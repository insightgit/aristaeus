#version 450

struct MaterialConfig {
    int diffuse_maps_active;
    int normal_maps_active;
    int specular_maps_active;
    int use_reflective_map;

    float shininess;
    float reflectivity;
    float refractive_index;
    float refractive_strength;

    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
};

// vertex-level
layout (location = 0) in vec3 position;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec2 texture_coords;
layout (location = 3) in vec4 diffuse_color;
layout (location = 4) in vec4 specular_color;
layout (location = 5) in vec3 tangent;

// instance-level
layout (location = 6) in mat4 instance_matrix;
/*layout (location = 6) in vec4 instance_matrix0;
layout (location = 7) in vec4 instance_matrix1;
layout (location = 8) in vec4 instance_matrix2;
layout (location = 9) in vec4 instance_matrix3;*/

layout (set = 0, binding = 0) uniform PerspectiveCameraUBO {
    mat4 projection;
    mat4 view;
    vec3 camera_front;
    vec3 camera_up;
    vec3 camera_right;
    vec3 camera_position;
} camera_ubo;

layout (set = 3, binding = 0) uniform MeshUBO {
    mat4 model;
    mat3 normal_transform;
    MaterialConfig material_config;
    mat4 light_space_matrix;
} mesh_ubo;

layout (location = 0) out VertexOut {
    vec4 diffuse_color_vector;
    vec3 normal_vector;
    mat3 tbn_matrix;
    vec2 texture_coords_out;
    vec3 world_position;
    float vertex_height;

    vec2 height_map_texture_coords;
} vertex_out;

void main() {
    //mat4 instance_matrix = mat4(instance_matrix0, instance_matrix1, instance_matrix2, instance_matrix3);

    //mat4 new_instance_matrix = transpose(instance_matrix);

    vec4 instanced_position = instance_matrix * vec4(position, 1.0);

    vertex_out.diffuse_color_vector = diffuse_color;

    vec3 bittangent_vector = cross(normal, tangent);
    vec3 tangent_vector_transformed = normalize(vec3(mesh_ubo.model * vec4(tangent, 0.0)));
    vec3 bittangent_vector_transformed = normalize(vec3(mesh_ubo.model * vec4(bittangent_vector, 0.0)));
    vec3 normal_vector_transformed = normalize(vec3(mesh_ubo.model * vec4(normal, 0.0)));

    mat3 tbn_matrix = mat3(tangent_vector_transformed, bittangent_vector_transformed, normal_vector_transformed);

    vertex_out.vertex_height = instanced_position.y;
    vertex_out.world_position = (instanced_position).xyz;
    vertex_out.normal_vector = mesh_ubo.normal_transform * normal;
    vertex_out.tbn_matrix = tbn_matrix;
    vertex_out.texture_coords_out = texture_coords;

    gl_Position = camera_ubo.projection * camera_ubo.view * vec4(vertex_out.world_position, 1.0);
}