#ifndef ARISTAEUS_PIPELINE_CACHE_HPP
#define ARISTAEUS_PIPELINE_CACHE_HPP

#include <mutex>

#include "vulkan_raii.hpp"

namespace aristaeus::gfx::vulkan {
    class CoreRenderer;

    class PipelineCache {
    public:
        // details nabbed from https://blog.roblox.com/2021/02/creating-a-robust-pipeline-cache-with-vulkan/
        // this is probably overkill for most scenarios but prevents crashes with invalid pipeline cache data
        // just in case
        struct PipelineCachePrefixHeader {
            uint32_t magic;
            size_t data_size;
            std::array<unsigned int, 4> data_hash;
            uint32_t vendor_id;
            uint32_t device_id;
            uint32_t driver_version;
            uint32_t driver_abi;
            std::array<uint8_t, VK_UUID_SIZE> uuid;
        };

        static constexpr uint32_t PIPELINE_CACHE_PREFIX_MAGIC = 0x23456FFF;

        PipelineCache(vk::raii::Device &logical_device, vk::PhysicalDeviceProperties device_properties,
                      const std::string &pipeline_cache_path);

        bool check_pipeline_cache_uuid_match(const PipelineCachePrefixHeader& cache_prefix,
            const vk::PhysicalDeviceProperties& physical_device_properties) const {
            for (unsigned int i = 0; VK_UUID_SIZE > i; ++i) {
                if (cache_prefix.uuid[i] != physical_device_properties.pipelineCacheUUID[i]) {
                    return false;
                }
            }

            return true;
        }

        std::reference_wrapper<vk::raii::PipelineCache> get_pipeline_cache() {
            return m_pipeline_cache;
        }

        void write_to_pipeline_cache(CoreRenderer &core_renderer);
    private:
        vk::raii::PipelineCache create_pipeline_cache(vk::raii::Device &logical_device);

        vk::PhysicalDeviceProperties m_device_properties;
        std::string m_pipeline_cache_path;
        vk::raii::PipelineCache m_pipeline_cache;
        std::mutex m_pipeline_cache_lock;
    };
}
#endif