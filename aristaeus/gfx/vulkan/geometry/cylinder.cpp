#include "cylinder.hpp"

#include "../core_renderer.hpp"

aristaeus::gfx::vulkan::Geometry aristaeus::gfx::vulkan::Cylinder::create_geometry(CoreRenderer &core_renderer, size_t sector_count, float radius, float height, const ModelInfo &model_info) {
	std::vector<ModelVertex> vertices;
	std::vector<uint32_t> indices;
	std::string mesh_generation_info = "cylinder-" + std::to_string(sector_count) + "-" + std::to_string(radius) + "-" + std::to_string(height);

	float sector_increment = (2.0f * PI) / sector_count;
	int current_index = 2;

	// draw circles at the top and bottom

	// top center of the cylinder
	vertices.push_back(ModelVertex{
		.position = glm::vec3{0.0f, height / 2, 0.0f},
		.normal = glm::vec3{0, -1, 0},
		.texture_coords = glm::vec3{0.5, 0.5, 0}
	});
	// bottom center of the cylinder
	vertices.push_back(ModelVertex{
		.position = glm::vec3{0.0f, -height / 2, 0.0f},
		.normal = glm::vec3{0, 1, 0},
		.texture_coords = glm::vec3{0.5, 0.5, 0},
	});

	for (float sector = 0.0f; 2.0f * PI > sector; sector += sector_increment) {
		ModelVertex vertex;

		// top circle
		vertex.position = glm::vec3{ cos(sector), height / 2, sin(sector) };
		vertex.texture_coords = glm::vec3((0.5f * cos(sector)) + 0.5f, (0.5f * sin(sector)) + 0.5f, 0.0f);
		vertex.normal = glm::normalize(glm::vec3(cos(sector), 0.0f, sin(sector)));

		vertices.push_back(vertex);

		uint32_t next_index = (sector < (2.0f * PI) - sector_increment) ? current_index + 2 : 2;

		indices.push_back(0);
		indices.push_back(next_index);
		indices.push_back(current_index);

		++current_index;

		// bottom circle
		vertex.position.y = -vertex.position.y;

		vertices.push_back(vertex);

		indices.push_back(1);
		indices.push_back(current_index);
		indices.push_back(next_index + 1);

		++current_index;
	}

	/*for (int i = 0; current_index - 5 > i; ++i) {
		glm::vec3 normal = glm::normalize(glm::cross(vertices[0].position - vertices[i + 2].position, vertices[i + 4].position - vertices[i + 2].position));
		vertices[i + 2].normal = normal;

		normal = glm::normalize(glm::cross(vertices[1].position - vertices[i + 3].position, vertices[i + 5].position - vertices[i + 3].position));
		vertices[i + 3].normal = normal;
	}*/

	// connect circles at the top and bottom (i = 2 because we are skipping the centers of the cylinders)
	for (int i = 2; current_index - 3 > i; ++i) {
		indices.push_back(i);
		indices.push_back(i + 1);
		indices.push_back(i + 2);

		indices.push_back(i + 2);
		indices.push_back(i + 1);
		indices.push_back(i + 3);
	}

	return { core_renderer, vertices, indices, mesh_generation_info, model_info };
}