//
// Created by bobby on 15/04/24.
//

#include "event_object.hpp"

#include "../core/class_db.hpp"

bool aristaeus::core::EventObject::add_callback_for_event_unsafe(const std::string &event_name,
                                   const std::string &callback_name,
                                   const std::function<void (EventObject&, void*)> &callback_function,
                                   core::StdTypeInfoRef argument_type_info, EventObject &from_object) {
    if(m_events.contains(event_name)) {
        EventInfo &event_info = m_events.at(event_name);

        if(event_info.argument_type == argument_type_info) {
            EventInfo::CallbackInfo callback_info {
                    .event_object = from_object,
                    .callback_function = callback_function
            };

            event_info.event_callbacks.insert({callback_name, callback_info});

            return true;
        } else {
            return false;
        }
    } else {
        return false;
    }
}

// disclaimer for add_callback_for_event_unsafe applies here as well
bool aristaeus::core::EventObject::call_event_unsafe(const std::string &event_name,
                                                     core::StdTypeInfoRef argument_type_info, void *argument) {
    if(m_events.contains(event_name)) {
        EventInfo &event_info = m_events.at(event_name);

        if(argument_type_info == event_info.argument_type) {
            for(std::pair<std::string, EventInfo::CallbackInfo> callback_func : event_info.event_callbacks) {
                callback_func.second.callback_function(callback_func.second.event_object, argument);
            }

            return true;
        } else {
            return false;
        }
    } else {
        return false;
    }
}