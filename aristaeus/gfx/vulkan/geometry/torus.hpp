#ifndef ARISTAEUS_TORUS_HPP
#define ARISTAEUS_TORUS_HPP

#include "geometry.hpp"

namespace aristaeus::gfx::vulkan {
	class Torus : public Geometry {
	public:
		Torus(CoreRenderer &core_renderer, size_t stack_count, size_t sector_count, float major_radius, float minor_radius, const ModelInfo &model_info) : 
			Geometry(create_geometry(core_renderer, stack_count, sector_count, major_radius, minor_radius, model_info)) {}
	private:
		Geometry create_geometry(CoreRenderer &core_renderer, size_t stack_count, size_t sector_count, float major_radius, float minor_radius, const ModelInfo &model_info);
	};
}

#endif