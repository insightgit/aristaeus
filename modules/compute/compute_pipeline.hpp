#ifndef ARISTAEUS_COMPUTE_PIPELINE_HPP
#define ARISTAEUS_COMPUTE_PIPELINE_HPP

#include <taskflow/taskflow.hpp>

#include <gfx/vulkan/pipeline_cache.hpp>
#include <gfx/vulkan/vulkan_pipeline.hpp>

namespace aristaeus::gfx::vulkan {
    class CoreRenderer;
}

namespace aristaeus::gfx::vulkan::compute {
	class ComputePipeline : public VulkanPipeline {
	public:
		ComputePipeline(ShaderInfo &compute_shader_info,
                        const std::vector<std::vector<vk::DescriptorSetLayoutBinding>> &descriptor_set_bindings,
                        const std::string &pipeline_cache_path,
						const std::vector<vk::PushConstantRange> &push_content_ranges = {},
                        bool start_constructing_pipeline = true, bool is_in_preload_stage = false);
	
		[[nodiscard]] vk::raii::PipelineLayout &get_pipeline_layout() {
			return *m_pipeline_data->pipeline_layout;
		}

		void bind_to_command_buffer(const vk::CommandBuffer &command_buffer);
	private:
		struct ComputePipelineData {
			std::unique_ptr<vk::raii::Pipeline> pipeline_object;
			std::unique_ptr<vk::raii::PipelineLayout> pipeline_layout;

			PipelineCache pipeline_cache;
			bool need_to_write_to_pipeline_cache = true;
			std::unique_ptr<std::mutex> pipeline_cache_lock;

			std::shared_ptr<vk::raii::ShaderModule> compute_shader_module;
			std::vector<vk::raii::DescriptorSetLayout> descriptor_set_layouts;

			tf::Taskflow pipeline_taskflow;
			std::optional<tf::Future<void>> pipeline_future{};
		};

		typedef std::unordered_map<ShaderInfo, std::weak_ptr<ComputePipelineData>, ShaderInfoHash> WeakComputePipelineCache;

		struct ComputePipelineConstructionInfo {
			std::reference_wrapper<vk::raii::Device> logical_device;
			std::vector<std::vector<vk::DescriptorSetLayoutBinding>> descriptor_set_bindings;
			std::vector<vk::PushConstantRange> push_content_ranges;
			ShaderInfo compute_shader_info;
			vk::PhysicalDeviceProperties device_properties;
		} m_pipeline_construction_info;

		static WeakComputePipelineCache compute_pipeline_cache;
		static std::mutex compute_pipeline_cache_mutex;

		std::unique_ptr<vk::raii::Pipeline> create_pipeline_object();
		void start_initial_pipeline_creation(bool is_in_preload_stage, const std::string &pipeline_cache_path);

		std::shared_ptr<ComputePipelineData> m_pipeline_data;
	};
}

#endif