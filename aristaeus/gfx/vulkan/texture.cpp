//
// Created by bobby on 5/29/22.
//

#include "core_renderer.hpp"

#include "texture.hpp"

std::unordered_map<std::string, std::weak_ptr<aristaeus::gfx::vulkan::Texture::TextureInfo>>
    aristaeus::gfx::vulkan::Texture::texture_cache;
std::mutex aristaeus::gfx::vulkan::Texture::texture_cache_mutex;

aristaeus::gfx::vulkan::Texture::Texture(const std::vector<unsigned char> &pixels_vector, vk::Format format, 
                                         const glm::uvec2 &image_size, const std::unordered_set<std::string> &texture_types, 
                                         CoreRenderer &core_renderer, std::optional<vk::CommandBuffer> create_command_buffer, 
                                         bool wait_for_creation, bool is_compute) :
    Texture(pixels_vector, format, image_size, texture_types, core_renderer,
        core_renderer.get_recommended_image_view_create_info(),
        core_renderer.get_recommended_sampler_create_info(), create_command_buffer, is_compute) {}

aristaeus::gfx::vulkan::Texture::Texture(const std::string &image_path, const std::unordered_set<std::string> &texture_types, CoreRenderer &core_renderer,
                                         std::optional<vk::CommandBuffer> create_command_buffer, bool wait_for_creation, bool is_compute) :
    Texture(image_path, texture_types, core_renderer,
        core_renderer.get_recommended_image_view_create_info(),
        core_renderer.get_recommended_sampler_create_info(), create_command_buffer, wait_for_creation, is_compute) {}

aristaeus::gfx::vulkan::Texture::Texture(const std::string &image_path, const std::unordered_set<std::string> &texture_types,
                                         CoreRenderer &core_renderer,
                                         const vk::ImageViewCreateInfo &image_view_create_info,
                                         const vk::SamplerCreateInfo &sampler_create_info,
                                         std::optional<vk::CommandBuffer> create_command_buffer,
                                         bool wait_for_creation, bool is_compute, bool delay_creation) :
                                            m_image_path(image_path),
                                            m_texture_types(texture_types),
                                            m_is_compute(is_compute),
                                            m_image_view_create_info(image_view_create_info),
                                            m_sampler_create_info(sampler_create_info),
                                            m_wait_for_creation(wait_for_creation),
                                            m_delay_creation(delay_creation) {
    assert(!image_path.empty());
    assert(!texture_types.empty());

    if(image_path == "../aristaeus/gfx/null_image.png") {
        std::cout << "inited with null image\n";
    }

    m_current_loading_data.create_command_buffer = create_command_buffer;

    if(ENABLE_CACHE) {
        std::lock_guard<std::mutex> texture_cache_lock_guard {texture_cache_mutex};

        if(texture_cache.contains(m_image_path.value())) {
            m_texture_info = texture_cache.at(m_image_path.value()).lock();

            if(m_texture_info != nullptr) {
                // we got the texture info, we don't need to create a texture
                return;
            }
        }

        if(m_image_path.has_value()) {
            m_texture_info = std::make_shared<TextureInfo>();

            std::pair<std::string, std::weak_ptr<aristaeus::gfx::vulkan::Texture::TextureInfo>> texture_cache_pair
                    {m_image_path.value(), std::weak_ptr<TextureInfo>(m_texture_info)};

            texture_cache.insert(texture_cache_pair);
        }
    } else {
        m_texture_info = std::make_shared<TextureInfo>();
    }

    m_current_loading_data.texture_format = vk::Format::eR8G8B8A8Srgb;

    if (!m_delay_creation) {
        create_image_texture();
    }
}

aristaeus::gfx::vulkan::Texture::Texture(const std::vector<unsigned char> &pixels_vector, vk::Format format,
                                         const glm::uvec2 &image_size,
                                         const std::unordered_set<std::string> &texture_types,
                                         aristaeus::gfx::vulkan::CoreRenderer &core_renderer,
                                         const vk::ImageViewCreateInfo &image_view_create_info,
                                         const vk::SamplerCreateInfo &sampler_create_info,
                                         std::optional<vk::CommandBuffer> create_command_buffer,
                                         bool wait_for_creation, bool is_compute, bool delay_creation) :
                                         m_texture_types(texture_types),
                                         m_is_compute(is_compute),
                                         m_image_view_create_info(image_view_create_info),
                                         m_sampler_create_info(sampler_create_info),
                                         m_wait_for_creation(wait_for_creation),
                                         m_delay_creation(delay_creation),
                                         m_texture_info(std::make_shared<TextureInfo>()) {
     m_current_loading_data.create_command_buffer = create_command_buffer;
     m_current_loading_data.pixels_vector = pixels_vector;
     m_current_loading_data.texture_format = format;
     m_current_loading_data.image_width = image_size.x;
     m_current_loading_data.image_height = image_size.y;
     
     if (!m_delay_creation) {
         create_image_texture();
     }
}


aristaeus::gfx::vulkan::Texture::Texture(ImageRaii &image_raii, const std::unordered_set<std::string> &texture_types,
                                         CoreRenderer &core_renderer,
                                         const vk::ImageViewCreateInfo &image_view_create_info,
                                         const vk::SamplerCreateInfo &sampler_create_info, bool is_compute) :
        m_image_path("internal"),
        m_texture_types(texture_types),
        m_is_compute(is_compute),
        m_image_view_create_info(image_view_create_info),
        m_texture_info(create_texture_info(core_renderer, std::move(image_raii), image_view_create_info,
                                           sampler_create_info)) {
    m_current_loading_data.mipmapping_levels = 1;

    vk::Format image_format = m_texture_info->image_texture->format;

    m_image_size_bytes_promise.set_value((image_format == vk::Format::eR16G16Sfloat ? 2 : 4) * m_texture_info->image_texture->image_width * 
                                          m_texture_info->image_texture->image_height * sizeof(char));
}

std::shared_ptr<aristaeus::gfx::vulkan::Texture::TextureInfo>
        aristaeus::gfx::vulkan::Texture::create_texture_info(CoreRenderer &core_renderer, ImageRaii image_raii,
                                                             const vk::ImageViewCreateInfo &image_view_create_info,
                                                             const vk::SamplerCreateInfo &sampler_create_info) {
    std::shared_ptr<TextureInfo> texture_info = std::make_shared<TextureInfo>(TextureInfo{
        .image_texture = std::move(image_raii),
        .texture_taskflow = {},
        .taskflow_launched = false
    });

    texture_info->image_texture_view = core_renderer.create_image_view_ptr(texture_info->image_texture, image_view_create_info);
    texture_info->texture_sampler = std::make_unique<vk::raii::Sampler>(core_renderer.create_sampler(sampler_create_info));

    return texture_info;
}

void aristaeus::gfx::vulkan::Texture::create_image_texture() {
    auto &core_renderer = utils::get_singleton_safe<CoreRenderer>();

    m_current_loading_data.core_renderer = &utils::get_singleton_safe<CoreRenderer>();

    m_texture_info->taskflow_launched = true;

    auto [image_data_load, allocate_buffer, copy_into_buffer, image_creation,
          buffer_image_copying, mipmap_generation, create_sampler] = m_texture_info->texture_taskflow.emplace(
            [this] () {
                if(m_image_path.has_value()) {
                    m_current_loading_data.pixels = stbi_load(m_image_path.value().c_str(),
                                                              &m_current_loading_data.image_width,
                                                              &m_current_loading_data.image_height,
                                                              &m_current_loading_data.image_channels, STBI_rgb_alpha);
                }

                m_current_loading_data.mipmapping_levels =
                        static_cast<uint32_t>(std::floor(std::log2(std::max(m_current_loading_data.image_width,
                                                                            m_current_loading_data.image_height))) + 1);
                m_image_view_create_info.subresourceRange.levelCount = m_current_loading_data.mipmapping_levels;
                m_image_view_create_info.format = m_current_loading_data.texture_format;
            },
            [this] () {
                if(m_image_path.has_value()) {
                    if(m_current_loading_data.pixels == nullptr) {
                        throw std::runtime_error("Couldn't load texture at " + m_image_path.value());
                    }
                }

                m_image_size_promise.set_value(glm::ivec2{ m_current_loading_data.image_width, m_current_loading_data.image_height });
                // TODO(Bobby): support multiple vulkan image formats whose color channels aren't bytes
                m_image_size_bytes_promise.set_value(m_current_loading_data.image_channels * m_current_loading_data.image_width * m_current_loading_data.image_height * sizeof(char));

                m_current_loading_data.image_size =
                        4u * static_cast<uint32_t>(m_current_loading_data.image_height) *
                        static_cast<uint32_t>(m_current_loading_data.image_width);

                if (!m_current_loading_data.pixels_vector.empty()) {
                    m_current_loading_data.image_size = m_current_loading_data.pixels_vector.size();
                }

                std::string buffer_purpose = "texture-" + (m_image_path.has_value() ? *m_image_path : "not_found");

                m_current_loading_data.staging_image_buffer =
                    m_current_loading_data.core_renderer->get_memory_manager().create_buffer(
                        *m_current_loading_data.core_renderer, m_current_loading_data.image_size,
                        vk::BufferUsageFlagBits::eTransferSrc, vk::SharingMode::eExclusive,
                        VMA_ALLOCATION_CREATE_HOST_ACCESS_SEQUENTIAL_WRITE_BIT, VMA_MEMORY_USAGE_AUTO, buffer_purpose);
            },
            [this] () {
                void *image_buffer_data;

                vmaMapMemory(m_current_loading_data.staging_image_buffer->allocator,
                             m_current_loading_data.staging_image_buffer->allocation, &image_buffer_data);

                unsigned char *pixels_data =
                    m_current_loading_data.pixels == nullptr ? m_current_loading_data.pixels_vector.data() : m_current_loading_data.pixels;

                std::memcpy(image_buffer_data, pixels_data, m_current_loading_data.image_size);

                vmaUnmapMemory(m_current_loading_data.staging_image_buffer->allocator,
                               m_current_loading_data.staging_image_buffer->allocation);

                if(m_current_loading_data.pixels != nullptr) {
                    stbi_image_free(m_current_loading_data.pixels);
                }
                else {
                    m_current_loading_data.pixels_vector.clear();
                }
            },
            [this] () {
                std::string texture_category;

                if (m_image_path.has_value()) {
                    texture_category = m_image_path.value();
                }
                else {
                    texture_category = "grayscale";

                    for(const std::string &texture_type : m_texture_types) {
                        texture_category += "-" + texture_type;
                    }
                }

                assert(m_texture_info->image_texture == nullptr);

                m_texture_info->image_texture =
                    m_current_loading_data.core_renderer->get_memory_manager().create_image(
                        *m_current_loading_data.core_renderer,
                        static_cast<uint32_t>(m_current_loading_data.image_width),
                        static_cast<uint32_t>(m_current_loading_data.image_height), m_current_loading_data.texture_format,
                        vk::ImageTiling::eOptimal,
                        vk::ImageUsageFlagBits::eTransferSrc | vk::ImageUsageFlagBits::eTransferDst |
                        vk::ImageUsageFlagBits::eSampled | vk::ImageUsageFlagBits::eInputAttachment, {},
                        VMA_MEMORY_USAGE_AUTO_PREFER_DEVICE, texture_category, 1, {},
                        m_current_loading_data.mipmapping_levels);

                m_texture_info->image_texture_view = m_current_loading_data.core_renderer->create_image_view_ptr(
                                                               m_texture_info->image_texture, m_image_view_create_info);
            },
            [this] () {
                if(m_current_loading_data.create_command_buffer.has_value()) {
                    m_current_loading_data.core_renderer->copy_buffer_to_image(
                            m_current_loading_data.create_command_buffer.value(),
                            m_current_loading_data.staging_image_buffer, m_texture_info->image_texture, 0, 1, 0, 0,
                            1, std::optional<vk::ImageLayout>{vk::ImageLayout::eShaderReadOnlyOptimal},
                            m_is_compute ? vk::PipelineBindPoint::eCompute : vk::PipelineBindPoint::eGraphics);
                } else {
                    m_current_loading_data.core_renderer->copy_buffer_to_image(
                            m_current_loading_data.staging_image_buffer, m_texture_info->image_texture,
                            0, 1, 0, 0, 1, vk::ImageLayout::eShaderReadOnlyOptimal,
                            m_is_compute ? vk::PipelineBindPoint::eCompute : vk::PipelineBindPoint::eGraphics);
                }

                std::cout << "\"Created\" image texture successfully!\n";

                // TODO(Bobby): potential race condition?
                #ifndef NDEBUG
                {
                    std::lock_guard<std::mutex> created_buffers_lock_guard {MemoryManager::created_buffers_lock};
                    MemoryManager::created_buffers.push_back(m_current_loading_data.staging_image_buffer->buffer);
                }
                #endif

                m_current_loading_data.core_renderer->transfer_staging_buffer_for_deletion(std::move(m_current_loading_data.staging_image_buffer));
            },
            [this] () {
                // TODO(Bobby): support using pre-generated mipmaps
                if(m_current_loading_data.create_command_buffer.has_value()) {
                    m_texture_info->image_texture->transition_image_layout(
                            m_current_loading_data.create_command_buffer.value(),
                            vk::ImageLayout::eTransferDstOptimal, m_is_compute ? vk::PipelineBindPoint::eCompute : vk::PipelineBindPoint::eGraphics,
                            {}, {}, {}, m_current_loading_data.mipmapping_levels);

                    if (!m_is_compute) {
                        m_current_loading_data.core_renderer->generate_mipmaps(
                            m_current_loading_data.create_command_buffer.value(),
                            m_texture_info->image_texture,
                            m_current_loading_data.mipmapping_levels);
                    }
                } else if(!m_is_compute) {
                    m_current_loading_data.core_renderer->generate_mipmaps(
                            m_texture_info->image_texture, 1,
                            m_current_loading_data.mipmapping_levels);
                }
            },
            [this] () {
                vk::SamplerCreateInfo new_sampler_create_info = m_sampler_create_info;

                new_sampler_create_info.mipmapMode = vk::SamplerMipmapMode::eLinear;
                new_sampler_create_info.maxLod = static_cast<float>(m_current_loading_data.mipmapping_levels);

                m_texture_info->texture_sampler = std::make_unique<vk::raii::Sampler>(
                                        m_current_loading_data.core_renderer->create_sampler(new_sampler_create_info));
            }
    );

    allocate_buffer.name("allocate_buffer");
    copy_into_buffer.name("copy_into_buffer");
    image_creation.name("image_creation");
    buffer_image_copying.name("buffer_image_copying");
    mipmap_generation.name("mipmap_generation");
    create_sampler.name("create_sampler");

    allocate_buffer.succeed(image_data_load);
    copy_into_buffer.succeed(allocate_buffer);
    image_creation.succeed(image_data_load);
    buffer_image_copying.succeed(copy_into_buffer, image_creation);
    mipmap_generation.succeed(buffer_image_copying);
    create_sampler.succeed(image_data_load);

    core_renderer.get_task_manager().execute_pre_render_taskflow(&m_texture_info->texture_taskflow,
                                                                 core_renderer.get_current_frame());

    if(m_current_loading_data.create_command_buffer.has_value() || m_wait_for_creation) {
        core_renderer.get_task_manager().wait_for_pre_render_taskflow(&m_texture_info->texture_taskflow,
                                                                      core_renderer.get_current_frame());
    }
}

vk::DescriptorImageInfo aristaeus::gfx::vulkan::Texture::get_descriptor_image_info(CoreRenderer &core_renderer) {
    // TODO(Bobby): race condition here from Terrain with this being null
    core_renderer.get_task_manager().wait_for_pre_render_taskflow(&m_texture_info->texture_taskflow,
        core_renderer.get_current_frame());

    assert(m_texture_info != nullptr &&
        m_texture_info->texture_sampler != nullptr && m_texture_info->image_texture_view != nullptr);

    return { **m_texture_info->texture_sampler, **m_texture_info->image_texture_view,
            vk::ImageLayout::eShaderReadOnlyOptimal };
}

void aristaeus::gfx::vulkan::Texture::transition_pre_compute(const vk::CommandBuffer &command_buffer) {
    if (!m_is_compute) {
        throw std::runtime_error("operation not supported on non compute textures");
    }
    
    assert(m_texture_info != nullptr);
    assert(m_texture_info->image_texture != nullptr);
    assert(m_is_compute);

    //core_renderer.get_task_manager().wait_for_pre_render_taskflow(&m_texture_info->texture_taskflow, core_renderer.get_current_frame());

    m_texture_info->image_texture->transition_image_layout(command_buffer, vk::ImageLayout::eGeneral,
                                                           vk::PipelineBindPoint::eCompute, 0, 1,
                                                           m_current_loading_data.mipmapping_levels);
}

void aristaeus::gfx::vulkan::Texture::transition_post_compute(const vk::CommandBuffer &command_buffer,
                                                              bool from_transfer_dst) {
    if (!m_is_compute) {
        throw std::runtime_error("operation not supported on non compute textures");
    }

    assert(m_texture_info != nullptr);
    assert(m_texture_info->image_texture != nullptr);
    assert(m_is_compute);

    m_texture_info->image_texture->transition_image_layout(command_buffer, vk::ImageLayout::eShaderReadOnlyOptimal,
                                                           vk::PipelineBindPoint::eCompute, {}, {}, {},
                                                           m_current_loading_data.mipmapping_levels);
}

void aristaeus::gfx::vulkan::Texture::write_hdr_image_save_buffer_to_file(BufferRaii &host_buffer, const std::string &image_path) {
    std::vector<float> image_data;
    void* image_buffer_data;

    // TODO(Bobby): Support mipmapping
    uint32_t image_size = 4 * static_cast<uint32_t>(m_texture_info->image_texture->image_width) * static_cast<uint32_t>(m_texture_info->image_texture->image_height);
    image_data.resize(image_size);

    vmaMapMemory(host_buffer->allocator, host_buffer->allocation, &image_buffer_data);

    std::memcpy(image_data.data(), image_buffer_data, image_size);

    vmaUnmapMemory(m_current_loading_data.staging_image_buffer->allocator,
        m_current_loading_data.staging_image_buffer->allocation);

    stbi_write_hdr(image_path.c_str(), m_texture_info->image_texture->image_width, m_texture_info->image_texture->image_height, 4, image_data.data());
}

void aristaeus::gfx::vulkan::Texture::wait_for_creation() {
    auto &core_renderer = utils::get_singleton_safe<CoreRenderer>();

    delayed_create_image();

    core_renderer.get_task_manager().wait_for_pre_render_taskflow(&m_texture_info->texture_taskflow,
                                                                  core_renderer.get_current_frame());
}
