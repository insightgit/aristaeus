//
// Created by bobby on 5/25/22.
//

#ifndef ARISTAEUS_VULKAN_RAII_HPP
#define ARISTAEUS_VULKAN_RAII_HPP

#define VULKAN_HPP_DISPATCH_LOADER_DYNAMIC 1
#include <vulkan/vulkan_raii.hpp>
#include <vulkan/vulkan_hash.hpp>

#endif //ARISTAEUS_VULKAN_RAII_HPP
