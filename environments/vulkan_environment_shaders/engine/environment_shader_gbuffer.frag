#version 450
#extension GL_EXT_nonuniform_qualifier : enable

struct PBRMaterialInfo {
    float roughness;
    float metallic;
    float ao;
    int using_gltf_metallicroughness_texture;

    ivec2 albedo_map_range;
    ivec2 normal_map_range;
    ivec2 metallic_map_range;
    ivec2 roughness_map_range;
    ivec2 ao_map_range;

    vec4 albedo;
    vec3 normal;
    vec3 world_position;
    vec3 tangent;
};

layout (location = 0) in VertexOut {
    vec3 normal_vector;
    mat3 tbn_matrix;
    vec2 texture_coords_out;
    vec3 world_position;

    float instance_index;
} from_vertex;

layout (location = 0) out vec4 position_out;
layout (location = 1) out vec4 normal_out;
layout (location = 2) out vec4 structure_out;
layout (location = 3) out vec4 albedo_out;
layout (location = 4) out vec3 roughness_metallic_ao_out;

struct Material {
    int override_texture;
    int object_type;
    vec3 light_box_color;

    PBRMaterialInfo pbr_material_info;
};

layout (set = 0, binding = 0) uniform PerspectiveCameraUBO {
    mat4 projection;
    mat4 view;
} camera_ubo;

layout (std430, set = 2, binding = 0) readonly buffer MaterialUBO {
    Material materials[];
} mat_ubo;

const int MATERIAL_SIZE = 10;

// TODO(Bobby): Convert this into a sampler array
layout(set = 2, binding = 1) uniform sampler2D texture_albedo[];
layout(set = 2, binding = 2) uniform sampler2D texture_normal[];
layout(set = 2, binding = 3) uniform sampler2D texture_metallic[];
layout(set = 2, binding = 4) uniform sampler2D texture_roughness[];
layout(set = 2, binding = 5) uniform sampler2D texture_ao[];

struct Instance {
    mat4 model;
    mat3 normal_transform;
    uint material_index;
};

layout (std430, set = 3, binding = 0) readonly buffer InstanceUBO {
    Instance instances[];
} instance_ubo;
// source: https://theorangeduck.com/page/avoiding-shader-conditionals
float when_greater_than(float x, float y) {
    return max(sign(x - y), 0.0);
}

float when_less_than(float x, float y) {
    return max(sign(y - x), 0.0);
}

float when_less_equal_than(float x, float y) {
    return 1.0 - when_greater_than(x, y);
}

float when_greater_equal_than(float x, float y) {
    return 1.0 - when_less_than(x, y);
}

float when_equal_to(float x, float y) {
    return 1.0 - abs(sign(x - y));
}

float when_not_equal_to(float x, float y) {
    return 1.0 - when_equal_to(x, y);
}

float when_less_equal_to(float x, float y) {
    return 1.0 - when_greater_than(x, y);
}

float or(float x, float y) {
    return min(x + y, 1.0);
}

PBRMaterialInfo get_pbr_material_info() {
    int instance_index_int = int(round(from_vertex.instance_index));
    Instance instance = instance_ubo.instances[instance_index_int];
    uint mat_index = instance.material_index;
    Material material = mat_ubo.materials[mat_index];

    PBRMaterialInfo pbr_material_info = material.pbr_material_info;

    if(material.pbr_material_info.normal.x <= 0.01 && material.pbr_material_info.normal.y <= 0.01 && 
        material.pbr_material_info.normal.z <= 0.01) {
        pbr_material_info.normal = from_vertex.normal_vector;
    }

    if(material.override_texture != 0) {
        return pbr_material_info;
    }
    
    pbr_material_info.albedo = vec4(0, 0, 0, 0);
    pbr_material_info.roughness = 0;
    pbr_material_info.metallic = 0;

    for(int i = material.pbr_material_info.albedo_map_range.x; material.pbr_material_info.albedo_map_range.x + material.pbr_material_info.albedo_map_range.y > i; ++i) {
        pbr_material_info.albedo += texture(texture_albedo[i], from_vertex.texture_coords_out);
    }

    for(int i = material.pbr_material_info.roughness_map_range.x; 
        material.pbr_material_info.roughness_map_range.x + material.pbr_material_info.roughness_map_range.y > i; ++i) {
        vec4 roughness_color = texture(texture_roughness[i], from_vertex.texture_coords_out);

        if(material.pbr_material_info.using_gltf_metallicroughness_texture != 0) {
            pbr_material_info.roughness += roughness_color.g;
        } else {
            pbr_material_info.roughness += roughness_color.r;
        }
    }

    for(int i = material.pbr_material_info.metallic_map_range.x; material.pbr_material_info.metallic_map_range.x + material.pbr_material_info.metallic_map_range.y > i; ++i) {
        vec4 metallic_color = texture(texture_metallic[i], from_vertex.texture_coords_out);

        if(material.pbr_material_info.using_gltf_metallicroughness_texture != 0) {
            pbr_material_info.metallic += metallic_color.b;
        } else {
            pbr_material_info.metallic += metallic_color.r;
        }
    }

    for(int i = material.pbr_material_info.ao_map_range.x; material.pbr_material_info.ao_map_range.x + material.pbr_material_info.ao_map_range.y > i; ++i) {
        pbr_material_info.ao += texture(texture_ao[i], from_vertex.texture_coords_out).r;
    }

    if(material.pbr_material_info.normal_map_range.y != 0) {
        // normal mapping is enabled
        vec3 normal_map_tangent_space = vec3(0.0f);

        for(int i = 0; material.pbr_material_info.normal_map_range.y > i; ++i) {
            normal_map_tangent_space += texture(texture_normal[material.pbr_material_info.normal_map_range.x + i], from_vertex.texture_coords_out).xyz;
        }

        normal_map_tangent_space = (normal_map_tangent_space * 2.0f) - 1.0f;

        vec3 world_space_normal = vec3(from_vertex.tbn_matrix * normal_map_tangent_space);

        pbr_material_info.normal = normalize(world_space_normal);
    } else {
        pbr_material_info.normal = from_vertex.normal_vector;
    }    

    //pbr_material_info.albedo = pow(pbr_material_info.albedo, vec3(2.2));

    // 0.0001 to prevent divide by zero
    pbr_material_info.roughness /= 0.0001 + material.pbr_material_info.roughness_map_range.y;
    pbr_material_info.metallic /= 0.0001 + material.pbr_material_info.metallic_map_range.y;
    pbr_material_info.ao /= 0.0001 + material.pbr_material_info.ao_map_range.y;

    pbr_material_info.ao = (when_less_equal_than(material.pbr_material_info.ao_map_range.y, 0) * 1) + 
                           (when_greater_than(material.pbr_material_info.ao_map_range.y, 0) * pbr_material_info.ao);

    return pbr_material_info;
}

void main() {
    PBRMaterialInfo pbr_material_info = get_pbr_material_info();
    int instance_index_int = int(round(from_vertex.instance_index));
    Instance instance = instance_ubo.instances[instance_index_int];

    albedo_out = pbr_material_info.albedo;
    normal_out = vec4(pbr_material_info.normal, pbr_material_info.albedo.a);

    vec3 camera_pos = (camera_ubo.view * vec4(from_vertex.world_position, 1)).xyz;
    vec3 view_space_normal = normalize(transpose(inverse(mat3(camera_ubo.view * instance.model))) * pbr_material_info.normal);

    structure_out = vec4(view_space_normal, pbr_material_info.albedo.a);
    position_out = vec4(from_vertex.world_position, pbr_material_info.albedo.a);
    roughness_metallic_ao_out = vec3(pbr_material_info.roughness, pbr_material_info.metallic, pbr_material_info.ao);
}
