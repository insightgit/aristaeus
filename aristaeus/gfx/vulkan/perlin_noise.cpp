//
// Created by bobby on 16/04/23.
//

#include "perlin_noise.hpp"
#include "assimp_model.hpp"


float aristaeus::gfx::vulkan::PerlinNoise::perlin_gradient_vector(int hash, const glm::vec2 &location_vector) {
    // z in this case is a constant (0)

    if (m_predetermined_gradient_vectors.contains(location_vector)) {
        //std::cout << "using predetermined at " << glm::to_string(location_vector) << "\n";

        glm::vec2 gradient_vector = m_predetermined_gradient_vectors.at(location_vector);
        float dot_value = glm::dot(gradient_vector, location_vector);

        return dot_value;
    }

    switch(hash & 0xF)
    {
        case 0x0:
            return location_vector.x + location_vector.y;
        case 0x1:
            return -location_vector.x + location_vector.y;
        case 0x2:
            return  location_vector.x - location_vector.y;
        case 0x3:
            return -location_vector.x - location_vector.y;
        case 0x4:
            return  location_vector.x + 0;
        case 0x5:
            return -location_vector.x + 0;
        case 0x6:
            return  location_vector.x - 0;
        case 0x7:
            return -location_vector.x - 0;
        case 0x8:
            return  location_vector.y + 0;
        case 0x9:
            return -location_vector.y + 0;
        case 0xA:
            return  location_vector.y - 0;
        case 0xB:
            return -location_vector.y - 0;
        case 0xC:
            return  location_vector.y + location_vector.x;
        case 0xD:
            return -location_vector.y + 0;
        case 0xE:
            return  location_vector.y - location_vector.x;
        case 0xF:
            return -location_vector.y - 0;
        default:
            return 0.0; // never happens
    }
}

float aristaeus::gfx::vulkan::PerlinNoise::perlin(float frequency, const glm::vec2 &coords) {
    glm::ivec2 bounded_pos = glm::ivec2((int)coords.x & 255, (int)coords.y & 255);
    glm::vec2 fade_pos;
    glm::vec2 unit_pos = glm::vec2(glm::mod(coords.x, 1.0f), glm::mod(coords.y, 1.0f));

    fade_pos.x = perlin_fade(unit_pos.x);
    fade_pos.y = perlin_fade(unit_pos.y);

    float x1_x2_lerp = glm::mix(perlin_gradient_vector(bounded_pos, unit_pos),
                                perlin_gradient_vector(bounded_pos + glm::ivec2(1, 0),
                                                       unit_pos - glm::vec2(1, 0)),
                                fade_pos.x);
    float x3_x4_lerp = glm::mix(perlin_gradient_vector(bounded_pos + glm::ivec2(0, 1),
                                                       unit_pos - glm::vec2(0, 1)),
                                perlin_gradient_vector(bounded_pos + glm::ivec2(1, 1),
                                                       unit_pos - glm::vec2(1, 1)),
                                fade_pos.x);

    float perlin_raw_val = glm::mix(x1_x2_lerp, x3_x4_lerp, fade_pos.y);

    return glm::max(perlin_raw_val, 0.0f);
}

float aristaeus::gfx::vulkan::PerlinNoise::perlin_with_octaves(float amplitude, float frequency, int number_of_octaves,
                          float persistence, glm::vec2 xy_coords) {
    float max_value = 0.0f;
    float total_result = 0.0f;

    for(int i = 0; number_of_octaves > i; ++i) {
        max_value += amplitude;

        total_result += amplitude * perlin(frequency, frequency * xy_coords);

        amplitude *= persistence;
        frequency *= 2;
    }

    return total_result / max_value;
}

glm::vec2 aristaeus::gfx::vulkan::PerlinNoise::get_perlin_gradient_vector(const glm::vec2 &coords) {
    glm::ivec2 bounded_pos = glm::ivec2((int)coords.x & 255, (int)coords.y & 255);

    switch(perlin_hash(bounded_pos) & 0xF)
    {
        case 0x2:
            return {1.0f, -1.0f};
        case 0x3:
            return {-1.0f, -1.0f};
        case 0x4:
        case 0x6:
            return {1.0f, 0.0f};
        case 0x5:
        case 0x7:
            return {-1.0f, 0.0f};
        case 0x8:
        case 0xA:
            return {0.0f, 1.0f};
        case 0x9:
        case 0xB:
        case 0xD:
        case 0xF:
            return {0.0f, -1.0f};
        case 0x0:
        case 0xC:
            return {1.0f, 1.0f};
        case 0x1:
        case 0xE:
            return {-1.0f, 1.0f};
        default:
            return {0.0f, 0.0f}; // never happens
    }
}

aristaeus::gfx::vulkan::Texture aristaeus::gfx::vulkan::PerlinNoise::to_perlin_noise_texture(
        float amplitude, float frequency, int number_of_octaves, float persistence,
        const glm::ivec2 &height_map_resolution, CoreRenderer &core_renderer,
        std::optional<vk::CommandBuffer> create_command_buffer, bool wait_for_creation) {
    // THIS NEEDS TO BE ADAPTED FOR std::vector<unsigned char> instead of std::vector<std::vector<float>>
    assert(false);
    std::vector<unsigned char> height_map;

    /*for (int y = 0; height_map_resolution.y > y; ++y) {
        std::vector<float> height_map_row;

        for(int x = 0; height_map_resolution.x > x; ++x) {
            glm::vec2 xy_coords{static_cast<float>(x), static_cast<float>(y)};

            height_map_row.push_back(perlin_with_octaves(amplitude, frequency, number_of_octaves, persistence,
                                                         xy_coords));
        }

        height_map.push_back(height_map_row);
    }*/
    vk::ImageViewCreateInfo image_view_create_info = AssimpModel::DEFAULT_IMAGE_VIEW_CREATE_INFO;
    vk::SamplerCreateInfo sampler_create_info = AssimpModel::DEFAULT_SAMPLER_CREATE_INFO;

    sampler_create_info.maxAnisotropy = core_renderer.get_max_sampler_anisotropy();

    return Texture{ height_map, vk::Format::eR32Sfloat, glm::uvec2{}, std::unordered_set<std::string>{"texture_perlin_noise"}, core_renderer, 
                    image_view_create_info, sampler_create_info, create_command_buffer, wait_for_creation};

}
