//
// Created by bobby on 5/25/22.
//

#include "glfw_create_window_surface.hpp"

void create_glfw_window_surface(VkInstance instance, GLFWwindow *window, VkSurfaceKHR *surface_pointer) {
    if(glfwCreateWindowSurface(instance, window, nullptr, surface_pointer) != VK_SUCCESS) {
        throw std::runtime_error("Couldn't create window surface!");
    }
}
