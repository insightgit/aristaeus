//
// Created by bobby on 16/04/23.
//

#include <gfx/vulkan/core_renderer.hpp>

#include "equirectangular_converter.hpp"

aristaeus::gfx::vulkan::compute::EquirectangularConverter::EquirectangularConverter(
                                                                const std::string &equirectangular_input_texture_path,
                                                                const std::optional<std::string> &cache_path) :
    CubeMapComputeTask(sizeof(ConversionInfo), 1, 1, 1, ComputePipelineInfo{ get_compute_shader_info(),
                       std::string("compute_equirectangular_converter"), {}, true, false},
                       "compute_equirectangular_converter", 6,
                       utils::get_image_size(equirectangular_input_texture_path), false, {}, cache_path,
                       std::vector<std::string>{equirectangular_input_texture_path}),
    m_equirectangular_converter_info(get_converter_info(equirectangular_input_texture_path)) {
}

aristaeus::gfx::vulkan::compute::EquirectangularConverter::EquirectangularOutputInfo
    aristaeus::gfx::vulkan::compute::EquirectangularConverter::create_output_data() {
    // TODO(Bobby): figure out a way to make all of these textures mapped close together in memory
    CoreRenderer &core_renderer = utils::get_singleton_safe<CoreRenderer>();
    std::vector<Texture> output_textures;

    ImageRaii output_image = core_renderer.get_memory_manager().create_image(core_renderer,
                                m_equirectangular_converter_info.image_size.x / 4,
                                m_equirectangular_converter_info.image_size.x / 4,
                                vk::Format::eR32G32B32A32Sfloat, vk::ImageTiling::eOptimal,
                                vk::ImageUsageFlagBits::eSampled | vk::ImageUsageFlagBits::eStorage,
                                {}, VMA_MEMORY_USAGE_AUTO_PREFER_DEVICE, "equirectangular_converter", 6,
                                vk::ImageCreateFlagBits::eCubeCompatible);
    std::vector<vk::raii::ImageView> image_views;
    vk::ImageViewCreateInfo image_view_create_info = AssimpModel::DEFAULT_IMAGE_VIEW_CREATE_INFO;
    vk::SamplerCreateInfo sampler_create_info = AssimpModel::DEFAULT_SAMPLER_CREATE_INFO;


    sampler_create_info.maxAnisotropy = core_renderer.get_max_sampler_anisotropy();
    image_view_create_info.format = vk::Format::eR32G32B32A32Sfloat;
    image_view_create_info.image = output_image->image;

    for (int i = 0; 6 > i; ++i) {
        image_view_create_info.subresourceRange.baseArrayLayer = i;

        image_views.emplace_back(core_renderer.get_logical_device_ref(), image_view_create_info);
    }

    return {
        .image = std::move(output_image),
        .image_views = std::move(image_views),
        .sampler = vk::raii::Sampler{core_renderer.get_logical_device_ref(), sampler_create_info}
    };
}


void aristaeus::gfx::vulkan::compute::EquirectangularConverter::update_descriptor_set(
                                                                             Texture &equirectangular_input_texture,
                                                                             const glm::ivec2 &image_size,
                                                                             uint32_t index) {
    m_conversion_info.image_size = image_size;
    m_conversion_info.view_transformation = get_current_view_transformation(index);

    vk::DescriptorImageInfo storage_image_info{ *m_output_info.sampler, *m_output_info.image_views.at(index),
                                                vk::ImageLayout::eGeneral };
    vk::DescriptorBufferInfo ubo_info{ m_ubos[index]->buffer, 0, sizeof(ConversionInfo)};

    vk::WriteDescriptorSet storage_image_set_write{ *m_descriptor_sets[index], 0, 0, vk::DescriptorType::eStorageImage,
                                                    storage_image_info};
    vk::WriteDescriptorSet ubo_descriptor_set_write{ *m_descriptor_sets[index], 1, 0, vk::DescriptorType::eUniformBuffer,
                                                     {}, ubo_info};

    void* ubo_data;

    vmaMapMemory(m_ubos[index]->allocator, m_ubos[index]->allocation, &ubo_data);

    std::memcpy(ubo_data, &m_conversion_info, sizeof(ConversionInfo));

    vmaUnmapMemory(m_ubos[index]->allocator, m_ubos[index]->allocation);

    CoreRenderer &core_renderer = utils::get_singleton_safe<CoreRenderer>();

    vk::DescriptorImageInfo sampler_image_info = equirectangular_input_texture.get_descriptor_image_info(core_renderer);

    vk::WriteDescriptorSet sampler_descriptor_set_write{ *m_descriptor_sets[index], 2, 0, vk::DescriptorType::eCombinedImageSampler, sampler_image_info, {}};

    core_renderer.get_logical_device_ref().updateDescriptorSets({ storage_image_set_write, ubo_descriptor_set_write, sampler_descriptor_set_write }, {});
}

void aristaeus::gfx::vulkan::compute::EquirectangularConverter::dispatch(vk::CommandBuffer &command_buffer) {
    //tf::Taskflow new_taskflow;
    m_output_info.image->transition_image_layout(command_buffer, vk::ImageLayout::eGeneral,
                                                 vk::PipelineBindPoint::eCompute);

    stbi_set_flip_vertically_on_load(true);

    CoreRenderer &core_renderer = utils::get_singleton_safe<CoreRenderer>();

    m_equirectangular_converter_texture = std::make_unique<Texture>(
                                            m_equirectangular_converter_info.equirectangular_input_texture_path,
                                            std::unordered_set<std::string>{"equirectangular_skybox"}, core_renderer,
                                            core_renderer.get_recommended_image_view_create_info(),
                                            core_renderer.get_recommended_sampler_create_info(), command_buffer, true,
                                            true);

    stbi_set_flip_vertically_on_load(false);

    // for some reason the image layout transition from TransferDstOptimal -> ShaderReadOnlyOptimal wasn't happening?
    // Investigate why we need this
    m_equirectangular_converter_texture->transition_post_compute(command_buffer, true);

    glm::ivec2 image_size = m_equirectangular_converter_texture->get_image_size().get();

    if (image_size.x % WORK_GROUP_X != 0 || image_size.y % WORK_GROUP_Y != 0) {
        throw std::runtime_error("equirectangular res must be a multiple of (" + std::to_string(WORK_GROUP_X) + "," +
                                 std::to_string(WORK_GROUP_Y) + ")");
    }

    for (int i = 0; 6 > i; ++i) {
        update_descriptor_set(*m_equirectangular_converter_texture, image_size, i);

        m_compute_pipeline.bind_to_command_buffer(command_buffer);
        command_buffer.bindDescriptorSets(vk::PipelineBindPoint::eCompute, *m_compute_pipeline.get_pipeline_layout(), 0,
                                          *m_descriptor_sets[i], {});

        command_buffer.dispatch(image_size.x / 4, image_size.x / 4, 1);
    }

    m_output_info.image->transition_image_layout(command_buffer, vk::ImageLayout::eShaderReadOnlyOptimal,
                                                 vk::PipelineBindPoint::eCompute);
}
