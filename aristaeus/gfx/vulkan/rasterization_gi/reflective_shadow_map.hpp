#ifndef ARISTAEUS_GI_REFLECTIVE_SHADOW_MAP_HPP
#define ARISTAEUS_GI_REFLECTIVE_SHADOW_MAP_HPP

#include <gfx/vulkan/lighting_node.hpp>
#include <gfx/vulkan/render_phase.hpp>

namespace aristaeus::gfx::gi {
	class ReflectiveShadowMap : public core::EventObject {
	public:
		static constexpr std::string_view REFLECTIVE_SHADOW_MAP_RENDER_PASS_NAME = "reflective_shadow_map";
		static constexpr std::string_view REFLECTIVE_SHADOW_MAP_RENDER_EXCLUSION_GROUP_NAME = "no_rsm_render";
		static constexpr uint32_t RSM_STRIDE = 4;

		ReflectiveShadowMap(uint32_t num_of_2d_maps, uint32_t num_of_cube_maps, const vk::Viewport &map_resolution,
							bool auto_find_light_nodes);

		std::vector<vk::DescriptorImageInfo> get_directional_rsm_descriptor_infos();
		std::vector<vk::DescriptorImageInfo> get_point_rsm_descriptor_infos();

		void set_scene_graph_node_root(const std::weak_ptr<SceneGraphNode> &root_node);
		void set_focus_light(uint32_t map_index, std::weak_ptr<vulkan::LightingNode> lighting_node);
	private:
		struct RSMPushConstantData {
			alignas(16) glm::vec3 point_light_pos;
			alignas(16) glm::vec3 light_color;
			alignas(4) int light_mode;
			alignas(4) int linear_depth_scaling;
		};

		vulkan::GraphicsPipeline::PipelineInfo get_rsm_pipeline_info();
		std::vector<vulkan::GraphicsPipeline::ShaderInfo> get_rsm_shader_infos();

		vulkan::RenderPhase &create_render_phase(const vk::Viewport &viewport);
		std::vector<vk::raii::ImageView> create_point_light_image_views();

		void on_new_node_added_to_tree(gfx::SceneGraphNode &node_added);
		void on_positionable_light_global_transform_changed(int map_index, int num_of_maps);

		static void render_callback_function(void *shadow_map_instance, bool predraw);

		std::vector<vk::PipelineColorBlendAttachmentState> m_blend_attachment_states;
		std::vector<std::weak_ptr<vulkan::LightingNode>> m_lighting_nodes;

		uint32_t m_num_of_2d_maps;
		uint32_t m_num_of_2d_maps_occupied = 0;

		uint32_t m_num_of_cube_maps;
		uint32_t m_num_of_cube_maps_occupied = 0;

		vk::Viewport m_render_phase_viewport;
		vulkan::RenderPhase &m_reflective_map_phase;

		vulkan::GraphicsPipeline::PipelineInfo m_reflective_shadow_map_pipeline_info;
		std::vector<vulkan::GraphicsPipeline::ShaderInfo> m_reflective_shadow_map_shader_infos;

		size_t m_current_map_index;
		vulkan::RenderContextID m_current_map_index_first_context;

		std::vector<vk::raii::ImageView> m_point_light_image_views;
		vk::raii::Sampler m_default_rsm_sampler;

		bool m_auto_find_light_nodes;
		std::weak_ptr<gfx::SceneGraphNode> m_root_node;

		glm::uvec2 m_past_rsm_frame_buffer_resolution;

		float m_last_frame_predraw;
		float m_last_frame_draw;
	};
}

#endif