#ifndef ARISTAEUS_CYLINDER_HPP
#define ARISTAEUS_CYLINDER_HPP

#include "geometry.hpp"

namespace aristaeus::gfx::vulkan {
	class Cylinder : public Geometry {
	public:
		Cylinder(CoreRenderer &core_renderer, size_t sector_count, float radius, float height, const ModelInfo &model_info) : Geometry(create_geometry(core_renderer, sector_count, radius, height, model_info)) {}
	private:
		Geometry create_geometry(CoreRenderer &core_renderer, size_t sector_count, float radius, float height, const ModelInfo &model_info);
	};
}

#endif