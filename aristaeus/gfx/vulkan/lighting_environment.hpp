#ifndef ARISTAEUS_LIGHTING_ENVIRONMENT_HPP
#define ARISTAEUS_LIGHTING_ENVIRONMENT_HPP

#include <core/event_object.hpp>

#include <gfx/vulkan/captured_sky_box.hpp>
#include <gfx/vulkan/command_buffer_manager.hpp>
#include <gfx/vulkan/cube_map_texture.hpp>
#include <gfx/vulkan/model_info.hpp>
#include <gfx/vulkan/point_light.hpp>
#include <gfx/vulkan/perspective_camera_environment.hpp>
#include <gfx/scene_graph_node.hpp>

namespace aristaeus::gfx::vulkan {
	// TODO(Bobby): make this more generalizable for ray tracing
	class LightingEnvironment : public core::EventObject {
	public:
		struct Fog {
			alignas(4) float constant_attenuation;
			alignas(4) float linear_attenuation;
			alignas(4) float quadratic_attenuation;

			alignas(16) glm::vec3 fog_color;

			alignas(4) int fog_active;

			// 0: light attenuation-based, 1: linear interporlation, 2: smoothstep
			alignas(4) int fog_mode;
			alignas(4) float fog_start;
			alignas(4) float fog_end;
		};

		static constexpr std::string_view ADDED_TO_CAMERA_ENVIRONMENT_EVENT = "added_to_camera_env";

		static bool NULL_CUBE_MAP_TEXTURE_INITED;
		static std::unique_ptr<CubeMapTexture> NULL_CUBE_MAP_TEXTURE;


		LightingEnvironment(const PBRMaterialInfo &default_pbr_material_info, const std::vector<glm::vec3> &initial_capture_points,
							vk::Extent2D initial_depth_capture_resolution, size_t initial_capture_point_capacity, size_t initial_max_num_of_point_lights,
							std::optional<vk::Extent2D> initial_capture_resolution);

		virtual ~LightingEnvironment();

		PBRMaterialInfo get_default_material_info() {
			return m_default_pbr_material_info;
		}

		void enable_lighting() {
			//m_environment_ubo_data.no_lighting = 0;
		}

		void disable_lighting() {
			//m_environment_ubo_data.no_lighting = 1;
		}

		void enable_point_lights() {
			//m_environment_ubo_data.point_lights_enabled = 1;
		}

		void disable_point_lights() {
			//m_environment_ubo_data.point_lights_enabled = 0;
		}

		void set_fog(const Fog &fog) {
			m_fog = fog;
		}

		void set_shadow_mode(int shadow_mode) {
			m_shadow_mode = shadow_mode;
		}

		virtual void input(Window &window);

		virtual std::string get_lighting_env_tag() const = 0;

		virtual bool is_captured() const = 0;

		virtual void capture_points(bool force_recapture) = 0;
		virtual void capture_point_light_shadows(bool force_recapture) = 0;

		virtual void activate_environment(bool predraw) = 0;

		virtual void reapply_graphics_pipeline(const vk::CommandBuffer &command_buffer) = 0;
		virtual void recreate_graphics_pipelines(const vk::Viewport &viewport, const vk::Rect2D &viewport_scissor) = 0;
	protected:
		static constexpr uint32_t MAX_NUM_OF_POINT_LIGHTS = 5;
		static constexpr uint32_t MAX_NUM_OF_DIRECTIONAL_LIGHTS = 5;

        friend class PerspectiveCameraEnvironment;

		std::weak_ptr<gfx::SceneGraphNode> m_scene_graph_node_root;

		PBRMaterialInfo m_default_pbr_material_info;

		uint32_t m_shadow_mode = 1;

		Fog m_fog;
	private:
		// meant to be called by PerspectiveCamera
		void set_perspective_camera_and_root(PerspectiveCameraEnvironment &camera_environment, const std::shared_ptr<SceneGraphNode> &root_scene_graph_node);
	};
}

#endif