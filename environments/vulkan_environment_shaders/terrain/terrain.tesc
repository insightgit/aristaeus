#version 450

layout (vertices=4) out;

layout (location = 0) in OutTerrainVertex {
    vec2 height_map_texture_coords;
    vec4 transformed_position;
} in_vertex[];

layout (location = 0) out vec2 height_map_texture_coords[];

const int TESSELLATION_MODE_MANUAL = 0;
const int TESSELLATION_MODE_CAMERA = 1;

struct TessellationSettings {
    int mode;
    int amount;

    int camera_max;
    int camera_min;
};

struct Wave {
    int wave_active;
    float amplitude;
    float birth_period;
    vec2 center_wave;
    float death_period;
    float sharpness;
    float wavelength;
    float t_time;
    vec2 padding;
};

const int MAX_NUM_OF_WAVES = 100;

layout (set = 0, binding = 0) uniform PerspectiveCameraUBO {
    mat4 projection;
    mat4 view;
    vec3 camera_front;
    vec3 camera_up;
    vec3 camera_right;
    vec3 camera_position;
    float camera_far_plane;
} camera_ubo;

layout (set = 2, binding = 0) uniform ModelUBO {
    float terrain_height_magnitude;

    int override_texture;
    int multi_texture_terrain_mode;
    int light_emitter;
    int object_type;
    vec3 light_box_color;

    Wave active_waves[MAX_NUM_OF_WAVES];
    Wave pending_waves[MAX_NUM_OF_WAVES];
    int active_wave_count;

    TessellationSettings tessellation_settings;
} model_ubo;

void main() {
    gl_out[gl_InvocationID].gl_Position = gl_in[gl_InvocationID].gl_Position;
    height_map_texture_coords[gl_InvocationID] = in_vertex[gl_InvocationID].height_map_texture_coords;

    if(model_ubo.tessellation_settings.mode == TESSELLATION_MODE_CAMERA) {
        float camera_distance =
            length(gl_in[gl_InvocationID].gl_Position.xyz - camera_ubo.camera_position) / camera_ubo.camera_far_plane;
        float tessellation_percent = pow(1 - camera_distance, 6.0);
        float tessellation_amount = model_ubo.tessellation_settings.camera_min +
            (model_ubo.tessellation_settings.camera_max - model_ubo.tessellation_settings.camera_min) *
            tessellation_percent;

        gl_TessLevelOuter[0] = tessellation_amount;
        gl_TessLevelOuter[1] = tessellation_amount;
        gl_TessLevelOuter[2] = tessellation_amount;
        gl_TessLevelOuter[3] = tessellation_amount;

        gl_TessLevelInner[0] = tessellation_amount;
        gl_TessLevelInner[1] = tessellation_amount;
    } else if(gl_InvocationID == 0) {
        // gl_InvocationID 0 can control all tesselation
        gl_TessLevelOuter[0] = model_ubo.tessellation_settings.amount;
        gl_TessLevelOuter[1] = model_ubo.tessellation_settings.amount;
        gl_TessLevelOuter[2] = model_ubo.tessellation_settings.amount;
        gl_TessLevelOuter[3] = model_ubo.tessellation_settings.amount;

        gl_TessLevelInner[0] = model_ubo.tessellation_settings.amount;
        gl_TessLevelInner[1] = model_ubo.tessellation_settings.amount;
    }
}
