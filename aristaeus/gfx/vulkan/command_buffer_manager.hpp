#ifndef ARISTAEUS_COMMAND_BUFFER_MANAGER
#define ARISTAEUS_COMMAND_BUFFER_MANAGER

#include "vulkan_raii.hpp"

#include "graphics_pipeline.hpp"
#include "render_phase.hpp"

#include "../../utils.hpp"

namespace aristaeus::gfx::vulkan {

    class CoreRenderer;

    // TODO(Bobby): Make this into a wrapper class eventually through using a script to copy all the function defintions from vulkan and 
    // then calling some callback function if in debug mode to keep track of commands executed on a cmd buffer
    class CommandBufferState {
    public:
        vk::CommandBuffer command_buffer{ nullptr };
        bool is_ready = false;
        bool is_ended = false;
        bool is_sky_box = false;
        bool has_blended_objects = false;
        RenderContextID current_render_context_id;
        uint32_t current_frame;
        vk::Framebuffer framebuffer_to_use;

        void begin_if_not_ready(RenderContextID render_context_id, uint32_t subpass_num, bool is_compute = false);
    };

	class CommandBufferManager {
	public:
        struct DescriptorSetBindInfo {
            uint32_t first_descriptor_set;
            std::vector<vk::DescriptorSet> descriptor_sets;
            std::vector<uint32_t> dynamic_offsets;
        };

        struct RenderPassKey {
            std::thread::id thread_id;
            RenderContextID render_context_id;
            uint32_t subpass_num;

            bool operator==(const RenderPassKey &other) const {
                return other.thread_id == thread_id && other.render_context_id == render_context_id && other.subpass_num == subpass_num;
            }
        };

        struct RenderPassKeyHash {
            size_t operator()(const RenderPassKey &render_pass_key) const {
                // adds the thread id to the GPU handle of the render pass
                size_t thread_id_hash = std::hash<std::thread::id>()(render_pass_key.thread_id);
                size_t render_context_id_hash = static_cast<size_t>(render_pass_key.render_context_id);
                size_t subpass_num_hash = static_cast<size_t>(render_pass_key.subpass_num);

                return (thread_id_hash << 56) | ((render_context_id_hash << 8) & ~0xFF00000000000000) | (subpass_num_hash & 0x00000000000000FF);
            }
        };

        CommandBufferManager(uint32_t queue_family_index, bool is_compute);

        void add_render_phase();
        
        void create_thread_command_pool_if_needed(RenderContextID render_context_id, uint32_t subpass_num);

        std::vector<vk::raii::CommandBuffer> create_command_buffers(const vk::CommandPool &command_pool,
            vk::CommandBufferLevel command_buffer_level, uint32_t count = utils::vulkan::MAX_FRAMES_IN_FLIGHT);

        std::vector<vk::raii::CommandBuffer> create_command_buffers(vk::raii::CommandPool &command_pool,
            vk::CommandBufferLevel command_buffer_level,
            uint32_t count = utils::vulkan::MAX_FRAMES_IN_FLIGHT) {
            return create_command_buffers(*command_pool, command_buffer_level, count);
        }

        CommandBufferState &get_command_buffer_state_for_thread(RenderContextID render_context_id,
            const std::optional<std::reference_wrapper<GraphicsPipeline>> &pipeline = {},
            const std::optional<DescriptorSetBindInfo> &descriptor_set_bind_info = {}, 
            std::optional<uint32_t> subpass_num = {});

        vk::CommandBuffer get_command_buffer_for_thread(RenderContextID render_context_id,
            const std::optional<std::reference_wrapper<GraphicsPipeline>> &pipeline = {},
            const std::optional<DescriptorSetBindInfo> &descriptor_set_bind_info = {},
            std::optional<uint32_t> subpass_num = {}) {
            return get_command_buffer_state_for_thread(render_context_id, pipeline, descriptor_set_bind_info, subpass_num).command_buffer;
        }

        void begin_secondary_command_buffers(uint32_t current_frame, RenderContextID render_context_id, uint32_t subpass_num);

        void bind_descriptor_sets_on_all_command_buffers(const vk::PipelineLayout &pipeline_layout, RenderContextID render_context_id, uint32_t subpass_num,
                                                         uint32_t first_descriptor_set, const std::vector<vk::DescriptorSet> &descriptor_sets, 
                                                         const std::vector<uint32_t> &dynamic_offsets, bool include_primary = false);

        void bind_pipeline_on_all_command_buffers(GraphicsPipeline &pipeline, RenderContextID render_context_id, bool include_primary = false);

        std::unordered_map<std::thread::id, vk::CommandPool> create_main_thread_command_pool();

        size_t record_all_secondary_command_buffers(RenderContextID render_context_id, uint32_t subpass_num, std::optional<vk::CommandBuffer> capture_command_buffer = {});


        void begin_command_buffer(uint32_t current_frame, RenderPhaseID render_phase_id);

        vk::CommandBuffer get_primary_command_buffer(uint32_t current_frame, RenderPhaseID render_phase_id);
        vk::CommandBuffer get_primary_command_buffer(uint32_t current_frame);
    private:
        uint32_t m_queue_family_index;
        uint32_t m_queue_index;

        bool m_is_compute;

        std::mutex m_command_pools_mutex;

        
        std::vector<vk::raii::CommandPool> m_command_pools_raii_owner;
        std::unordered_map<std::thread::id, vk::CommandPool> m_command_pools;
        std::unordered_map<RenderPassKey,
            std::array<CommandBufferState,
            utils::vulkan::MAX_FRAMES_IN_FLIGHT>,
            RenderPassKeyHash> m_persistent_secondary_command_buffers;
        std::unordered_map<RenderContextID, std::unordered_set<std::thread::id>> m_persistent_secondary_thread_ids;

        std::vector<vk::raii::CommandBuffer> m_command_buffers;
        std::vector<vk::raii::CommandBuffer> m_primary_command_buffers;

        std::mutex m_before_render_secondary_command_buffer_mutex;

        std::array<std::vector<vk::raii::CommandBuffer>, utils::vulkan::MAX_FRAMES_IN_FLIGHT>
            m_before_render_secondary_command_buffers;
        std::array<std::vector<vk::raii::CommandBuffer>, utils::vulkan::MAX_FRAMES_IN_FLIGHT>
            m_previous_before_render_secondary_command_buffers;
	};
}

#endif