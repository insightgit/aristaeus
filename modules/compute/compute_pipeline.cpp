#include "compute_pipeline.hpp"

#include <gfx/vulkan/core_renderer.hpp>

aristaeus::gfx::vulkan::compute::ComputePipeline::WeakComputePipelineCache aristaeus::gfx::vulkan::compute::ComputePipeline::compute_pipeline_cache{};
std::mutex aristaeus::gfx::vulkan::compute::ComputePipeline::compute_pipeline_cache_mutex{};

aristaeus::gfx::vulkan::compute::ComputePipeline::ComputePipeline(ShaderInfo &compute_shader_info,
                                const std::vector<std::vector<vk::DescriptorSetLayoutBinding>> &descriptor_set_bindings,
                                const std::string &pipeline_cache_path,
                                const std::vector<vk::PushConstantRange> &push_content_ranges,
                                bool start_constructing_pipeline, bool is_in_preload_stage) :
    m_pipeline_construction_info(utils::get_singleton_safe<CoreRenderer>().get_logical_device_ref(),
                                 descriptor_set_bindings, push_content_ranges, compute_shader_info,
                                 utils::get_singleton_safe<CoreRenderer>().get_selected_physical_device_properties()) {
    if (compute_pipeline_cache.find(compute_shader_info) != compute_pipeline_cache.end()) {
        m_pipeline_data = compute_pipeline_cache.at(compute_shader_info).lock();
    }

    if (m_pipeline_data == nullptr) {
        if (start_constructing_pipeline) {
            start_initial_pipeline_creation(is_in_preload_stage, pipeline_cache_path);
        }
    }
}

std::unique_ptr<vk::raii::Pipeline> aristaeus::gfx::vulkan::compute::ComputePipeline::create_pipeline_object() {
    vk::PipelineShaderStageCreateInfo shader_stage_create_info{ {}, vk::ShaderStageFlagBits::eCompute, **m_pipeline_data->compute_shader_module, "main"};
    vk::ComputePipelineCreateInfo compute_create_info { {}, shader_stage_create_info, **m_pipeline_data->pipeline_layout };

    return std::make_unique<vk::raii::Pipeline>(m_pipeline_construction_info.logical_device.get(), m_pipeline_data->pipeline_cache.get_pipeline_cache().get(), compute_create_info);
}

void aristaeus::gfx::vulkan::compute::ComputePipeline::start_initial_pipeline_creation(
                                                                               bool is_in_preload_stage,
                                                                               const std::string &pipeline_cache_path) {
    // TODO(Bobby): pipeline caching has a race condition somewhere
    // from validation layer (type: validation, severity: error):
    // Validation Error: [ UNASSIGNED-Threading-MultipleThreads ] Object 0: handle = 0x4ca22a00000005f2,
    // type = VK_OBJECT_TYPE_PIPELINE_CACHE; | MessageID = 0x141cb623 | THREADING ERROR :
    // vkGetPipelineCacheData(): object of type VkPipelineCache is simultaneously used in thread 140306842379968
    // and thread 140306817201856


    if (m_pipeline_data != nullptr) {
        // pipeline already created!
        return;
    }

    m_pipeline_data.reset(new ComputePipelineData{ .pipeline_cache = PipelineCache{m_pipeline_construction_info.logical_device, m_pipeline_construction_info.device_properties, pipeline_cache_path},
                                                   .pipeline_cache_lock = std::make_unique<std::mutex>() });

    auto [pipeline_layout_creation, compute_module_creation, pipeline_creation] =
        m_pipeline_data->pipeline_taskflow.emplace(
            [this]() {
                std::vector<std::vector<vk::DescriptorSetLayoutBinding>> descriptor_bindings =
                    m_pipeline_construction_info.descriptor_set_bindings;

                m_pipeline_data->descriptor_set_layouts = create_descriptor_set_layouts(m_pipeline_construction_info.logical_device.get(), descriptor_bindings);

                m_pipeline_data->pipeline_layout = create_pipeline_layout_object(
                    m_pipeline_construction_info.logical_device.get(), m_pipeline_construction_info.push_content_ranges,
                    m_pipeline_data->descriptor_set_layouts);
            },
            [this]() {
                auto code_ref = reinterpret_cast<const uint32_t*>(m_pipeline_construction_info.compute_shader_info.code.data());
                uint32_t code_ref_size = m_pipeline_construction_info.compute_shader_info.code.size();

                assert(m_pipeline_construction_info.compute_shader_info.stage == vk::ShaderStageFlagBits::eCompute);

                std::lock_guard<std::mutex> shader_module_lock_guard{ shader_module_cache_mutex };
                m_pipeline_data->compute_shader_module = nullptr;

                if (shader_module_cache.contains(m_pipeline_construction_info.compute_shader_info)) {
                    m_pipeline_data->compute_shader_module = VulkanPipeline::shader_module_cache.at(m_pipeline_construction_info.compute_shader_info).lock();
                }

                if (m_pipeline_data->compute_shader_module == nullptr) {
                    vk::ShaderModuleCreateInfo shader_module_create_info{ {}, code_ref_size, code_ref, nullptr };
                    m_pipeline_data->compute_shader_module =
                        std::make_shared<vk::raii::ShaderModule>(m_pipeline_construction_info.logical_device.get(),
                            shader_module_create_info);

                    shader_module_cache.insert({ m_pipeline_construction_info.compute_shader_info, m_pipeline_data->compute_shader_module });
                }
            },
            [this]() {
                m_pipeline_data->pipeline_object = create_pipeline_object();
            });

    pipeline_layout_creation.name("pipeline_layout_creation");
    compute_module_creation.name("compute_module_creation");
    pipeline_creation.name("pipeline_creation");

    pipeline_creation.succeed(pipeline_layout_creation, compute_module_creation);

    CoreRenderer &core_renderer = utils::get_singleton_safe<CoreRenderer>();

    if (is_in_preload_stage) {
        m_pipeline_data->pipeline_future =
            core_renderer.get_task_manager().execute_loading_taskflow(&m_pipeline_data->pipeline_taskflow);
    } else {
        core_renderer.get_task_manager().execute_pre_render_taskflow(&m_pipeline_data->pipeline_taskflow,
            core_renderer.get_current_frame());
    }

    {
        std::lock_guard<std::mutex> pipeline_lock_guard{ compute_pipeline_cache_mutex };

        compute_pipeline_cache.insert({ m_pipeline_construction_info.compute_shader_info, m_pipeline_data });
    }

    std::cout << "\"Created\" compute pipeline successfully!\n";
}

void aristaeus::gfx::vulkan::compute::ComputePipeline::bind_to_command_buffer(const vk::CommandBuffer &command_buffer) {
    if (m_pipeline_data->pipeline_future.has_value()) {
        m_pipeline_data->pipeline_future->wait();
    } else {
        CoreRenderer &core_renderer = utils::get_singleton_safe<CoreRenderer>();

        core_renderer.get_task_manager().wait_for_pre_render_taskflow(&m_pipeline_data->pipeline_taskflow,
            core_renderer.get_current_frame());
    }

    command_buffer.bindPipeline(vk::PipelineBindPoint::eCompute, **m_pipeline_data->pipeline_object);
}