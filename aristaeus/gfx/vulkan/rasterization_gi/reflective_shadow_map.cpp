#include "reflective_shadow_map.hpp"

#include <gfx/vulkan/rasterization_lighting_environment.hpp>
#include <gfx/vulkan/cube_map_texture.hpp>
#include <gfx/vulkan/directional_light.hpp>
#include <gfx/vulkan/point_light.hpp>

aristaeus::gfx::gi::ReflectiveShadowMap::ReflectiveShadowMap(uint32_t num_of_2d_maps, uint32_t num_of_cube_maps, const vk::Viewport &map_resolution,
                                                             bool auto_find_light_nodes) : 
                                                                                     m_num_of_2d_maps(num_of_2d_maps),
                                                                                     m_num_of_cube_maps(num_of_cube_maps),
                                                                                     m_render_phase_viewport(map_resolution),
                                                                                     m_reflective_map_phase(create_render_phase(m_render_phase_viewport)),
                                                                                     m_reflective_shadow_map_pipeline_info(get_rsm_pipeline_info()),
                                                                                     m_reflective_shadow_map_shader_infos(get_rsm_shader_infos()),
                                                                                     m_auto_find_light_nodes(auto_find_light_nodes),
                                                                                     m_point_light_image_views(create_point_light_image_views()),
                                                                                     m_default_rsm_sampler(utils::get_singleton_safe<vulkan::CoreRenderer>().create_default_sampler()) {
    m_lighting_nodes.resize(m_num_of_2d_maps + m_num_of_cube_maps);
}

void aristaeus::gfx::gi::ReflectiveShadowMap::set_focus_light(uint32_t map_index, std::weak_ptr<vulkan::LightingNode> lighting_node) {
    m_lighting_nodes[map_index] = lighting_node;
}

void aristaeus::gfx::gi::ReflectiveShadowMap::render_callback_function(void *shadow_map_instance, bool predraw) {
    auto &core_renderer = utils::get_singleton_safe<vulkan::CoreRenderer>();
    auto *reflective_shadow_map = reinterpret_cast<ReflectiveShadowMap*>(shadow_map_instance);

    uint32_t current_frame = core_renderer.get_current_frame();

    if ((predraw && reflective_shadow_map->m_last_frame_predraw != current_frame) || (!predraw && reflective_shadow_map->m_last_frame_draw != current_frame)) {
        reflective_shadow_map->m_current_map_index = 0;
        reflective_shadow_map->m_current_map_index_first_context = core_renderer.get_current_render_context_id();

        if (predraw) {
            reflective_shadow_map->m_last_frame_predraw = current_frame;
        }
        else {
            reflective_shadow_map->m_last_frame_draw = current_frame;
        }
    }

    vulkan::PerspectiveCameraEnvironment &perspective_camera_environment = core_renderer.get_perspective_camera_environment();
    vulkan::RenderContextID local_render_context_id = core_renderer.get_current_render_context_id() - reflective_shadow_map->m_current_map_index_first_context;
    std::shared_ptr<vulkan::LightingNode> current_lighting_node = reflective_shadow_map->m_lighting_nodes[reflective_shadow_map->m_current_map_index].lock();
    vulkan::PerspectiveCameraEnvironment::Capture capture;

    bool directional_light_expected = reflective_shadow_map->m_current_map_index < reflective_shadow_map->m_num_of_2d_maps;

    if (directional_light_expected) {
        assert(current_lighting_node == nullptr || current_lighting_node->get_light_node_ubo_data().light_type == gfx::vulkan::DirectionalLight::UBO_DIRECTIONAL_LIGHT_TYPE_ID);

        if (local_render_context_id >= 1) {
            local_render_context_id = 0;
            ++reflective_shadow_map->m_current_map_index;
            reflective_shadow_map->m_current_map_index_first_context = core_renderer.get_current_render_context_id();
        }
    }
    else {
        assert(current_lighting_node == nullptr || current_lighting_node->get_light_node_ubo_data().light_type == gfx::vulkan::PointLight::UBO_POINT_LIGHT_TYPE_ID);

        if (local_render_context_id >= 6) {
            local_render_context_id = 0;
            ++reflective_shadow_map->m_current_map_index;
            reflective_shadow_map->m_current_map_index_first_context = core_renderer.get_current_render_context_id();
        }
    }

    int base_num_of_rsms = reflective_shadow_map->m_num_of_cube_maps + reflective_shadow_map->m_num_of_2d_maps;
    reflective_shadow_map->m_current_map_index %= base_num_of_rsms;

    current_lighting_node = reflective_shadow_map->m_lighting_nodes[reflective_shadow_map->m_current_map_index].lock();

    if (current_lighting_node == nullptr) {
        return;
    }

    RSMPushConstantData rsm_data{};
    static constexpr int RSM_DATA_DIRECTIONAL_LIGHT_MODE = 1;
    static constexpr int RSM_DATA_POINT_LIGHT_MODE = 2;

    switch (current_lighting_node->get_light_node_ubo_data().light_type) {
    case gfx::vulkan::DirectionalLight::UBO_DIRECTIONAL_LIGHT_TYPE_ID: {
        auto directional_light = static_cast<gfx::vulkan::DirectionalLight*>(current_lighting_node.get());

        capture = directional_light->get_shadow_map_capture();

        rsm_data.light_mode = RSM_DATA_DIRECTIONAL_LIGHT_MODE;
        rsm_data.light_color = directional_light->get_light_node_ubo_data().diffuse;
        rsm_data.linear_depth_scaling = 1;

        break;
    }
    case gfx::vulkan::PointLight::UBO_POINT_LIGHT_TYPE_ID: {
        float target_pitch_degrees = vulkan::CapturedSkyBox::PITCH_DEGREES[local_render_context_id];
        float target_yaw_degrees = vulkan::CapturedSkyBox::YAW_DEGREES[local_render_context_id];

        glm::vec3 camera_up = glm::vec3{ 0, 1, 0 };

        if (target_pitch_degrees == 90) {
            camera_up = glm::vec3{ 0.0f, 0, 1.0f };
        }
        else if (target_pitch_degrees == -90) {
            camera_up = glm::vec3{ 0.0f, 0, -1.0 };
        }

        glm::vec3 global_pos = (current_lighting_node->get_transform() + current_lighting_node->get_parent_transform()).position;

        capture = vulkan::PerspectiveCameraEnvironment::Capture{
            .aspect_ratio = reflective_shadow_map->m_render_phase_viewport.width / reflective_shadow_map->m_render_phase_viewport.height,
            .fov = 90.0f,
            .pitch = target_pitch_degrees,
            .position = global_pos,
            .up = camera_up,
            .yaw = target_yaw_degrees,
            .z_far = 100.0f,
            .z_near = 1.0f
        };

        rsm_data.light_mode = RSM_DATA_POINT_LIGHT_MODE;
        rsm_data.light_color = current_lighting_node->get_light_node_ubo_data().diffuse;
        rsm_data.point_light_pos = global_pos;
        rsm_data.linear_depth_scaling = 1;

        break;
    }
    }

    if (predraw) {
        core_renderer.set_default_graphics_pipeline_info(reflective_shadow_map->m_reflective_shadow_map_pipeline_info);
        core_renderer.set_default_graphics_shader_infos(reflective_shadow_map->m_reflective_shadow_map_shader_infos);

        perspective_camera_environment.get_scene_graph_root_node()->draw_tree(SceneGraphNode::IDENTITY_TRANSFORM, {}, {}, {},
            { std::string{REFLECTIVE_SHADOW_MAP_RENDER_EXCLUSION_GROUP_NAME} });

        core_renderer.set_default_graphics_pipeline_info({});

        core_renderer.get_perspective_camera_environment().setup_capture(capture);
    }
    else {
        auto &mesh_manager = utils::get_singleton_safe<vulkan::MeshManager>();
        auto &material_manager = utils::get_singleton_safe<vulkan::MaterialManager>();

        vulkan::RenderContextID current_render_context_id = core_renderer.get_current_render_context_id();

        material_manager.call_upon_super_material_binds(current_render_context_id, [current_render_context_id, rsm_data](vulkan::MaterialID material_id) {
            auto &core_renderer = utils::get_singleton_safe<vulkan::CoreRenderer>();
            auto &material_manager = utils::get_singleton_safe<vulkan::MaterialManager>();

            vulkan::GraphicsPipeline &pipeline = material_manager.get_graphics_pipeline_for_material(material_id);

            vk::CommandBuffer command_buffer =
                core_renderer.get_graphics_command_buffer_manager().get_command_buffer_for_thread(current_render_context_id);

            command_buffer.pushConstants(*pipeline.get_pipeline_layout(), vk::ShaderStageFlagBits::eFragment, 0,
                                         sizeof(RSMPushConstantData), static_cast<const void *>(&rsm_data));
        });

        vk::CommandBuffer primary_command_buffer = core_renderer.get_graphics_command_buffer_manager().get_primary_command_buffer(current_frame);

        core_renderer.get_perspective_camera_environment().update_matrix_ubo(core_renderer, current_render_context_id);

        mesh_manager.draw(primary_command_buffer, current_render_context_id, 0);
    }
}

aristaeus::gfx::vulkan::RenderPhase &aristaeus::gfx::gi::ReflectiveShadowMap::create_render_phase(const vk::Viewport &viewport) {
    std::vector<aristaeus::gfx::vulkan::RenderPhase::AttachmentInfo> attachment_infos = vulkan::RasterizationLightingEnvironment::get_gbuffer_attachment_info();
    auto &core_renderer = utils::get_singleton_safe<vulkan::CoreRenderer>();

    // don't need the forward composition attachment, the tangent attachment, or the roughness/metallic/ao attachment
    attachment_infos.erase(attachment_infos.begin());
    attachment_infos.erase(attachment_infos.begin() + 4);
    attachment_infos.erase(attachment_infos.begin() + 2);

    m_blend_attachment_states.clear();

    for (int i = 0; attachment_infos.size() > i; ++i) {
        attachment_infos[i].usage_flags &= ~vk::ImageUsageFlagBits::eInputAttachment;
        attachment_infos[i].usage_flags |= vk::ImageUsageFlagBits::eSampled;

        if (attachment_infos[i].description.format != core_renderer.get_depth_image_format()) {
            m_blend_attachment_states.push_back(vulkan::RasterizationLightingEnvironment::FORWARD_COLOR_BLEND_ATTACHMENT);
        }

        attachment_infos[i].description.finalLayout = vk::ImageLayout::eShaderReadOnlyOptimal;
    }

    // based on the screen-space interpolation steps described in the paper, 
    // we want to have one set of render contexts for a low res render
    // and one set of render contexts for a higher res render.
    std::vector<glm::uvec2> resolutions;
    size_t base_num_of_render_contexts = (6 * m_num_of_cube_maps) + m_num_of_2d_maps;

    vulkan::RenderPhase::RenderPassInfo reflective_shadow_map_phase{
        .clear_values = {},
        .contextless_draw_function = [](void *ptr) {
            render_callback_function(ptr, true);
        },
        .contextless_draw_function_arg = this,
        .draw_function = [](void *ptr) {
            render_callback_function(ptr, false);
        },
        .draw_function_arg = this,
        .name = std::string{REFLECTIVE_SHADOW_MAP_RENDER_PASS_NAME},
        .one_shot = true,
        .fire_one_shot_on_creation = true,
        .phase_dependents = {{std::string{vulkan::RasterizationLightingEnvironment::DEFERRED_RENDER_PHASE_NAME}, vk::PipelineStageFlagBits::eColorAttachmentOutput}},
        .render_pass_attachments = attachment_infos,
        .resolution = glm::ivec2{768, 768},
        .subpass_info = {},
        .num_of_render_contexts = base_num_of_render_contexts,
        .viewport = viewport,
        .is_cube_map = m_num_of_cube_maps > 0,
        .show_attachments_in_imgui_window = false
    };

    vulkan::RenderPhase &render_phase_added = core_renderer.add_render_pass(reflective_shadow_map_phase);

    return render_phase_added;
}

std::vector<vk::raii::ImageView> aristaeus::gfx::gi::ReflectiveShadowMap::create_point_light_image_views() {
    const std::string render_pass_name{ REFLECTIVE_SHADOW_MAP_RENDER_PASS_NAME };

    auto &core_renderer = utils::get_singleton_safe<vulkan::CoreRenderer>();
    std::vector<vk::raii::ImageView> return_value;

    return_value.reserve(m_num_of_cube_maps);

    std::vector<std::reference_wrapper<vulkan::SharedImageRaii>> rsm_images;
    for (int i = 0; RSM_STRIDE > i; ++i) {
        rsm_images.push_back(m_reflective_map_phase.get_render_pass_image(render_pass_name, i));
    }

    for (int i = 0; m_num_of_cube_maps > i; ++i) {
        vk::ImageViewCreateInfo image_view_create_info = vulkan::CubeMapTexture::DEFAULT_IMAGE_VIEW_CREATE_INFO;

        image_view_create_info.subresourceRange.baseArrayLayer = m_num_of_2d_maps + (i * 6);

        for (std::reference_wrapper<vulkan::SharedImageRaii> rsm_image : rsm_images) {
            image_view_create_info.image = rsm_image.get()->image;
            image_view_create_info.format = rsm_image.get()->format;

            if (rsm_image.get()->is_depth_format()) {
                image_view_create_info.subresourceRange.aspectMask = vk::ImageAspectFlagBits::eDepth;
            }
            else {
                image_view_create_info.subresourceRange.aspectMask = vk::ImageAspectFlagBits::eColor;
            }

            m_point_light_image_views.emplace_back(core_renderer.get_logical_device_ref(), image_view_create_info);
        }
    }

    return return_value;
}

aristaeus::gfx::vulkan::GraphicsPipeline::PipelineInfo aristaeus::gfx::gi::ReflectiveShadowMap::get_rsm_pipeline_info() {
    auto &core_renderer = utils::get_singleton_safe<gfx::vulkan::CoreRenderer>();
    std::array<std::vector<vk::DescriptorSetLayoutBinding>, 2> gbuffer_descriptor_set_bindings = 
        vulkan::RasterizationLightingEnvironment::BASE_GBUFFER_DESCRIPTOR_SET_LAYOUT_BINDINGS;
    std::string pipeline_cache_suffix;
    vk::raii::RenderPass &render_pass = m_reflective_map_phase.get_render_pass(std::string{REFLECTIVE_SHADOW_MAP_RENDER_PASS_NAME});
    vk::PipelineVertexInputStateCreateInfo vertex_input_stage;

    vulkan::GraphicsPipeline::PipelineInfo pipeline_info{
            .vertex_input_stage = { {}, gfx::vulkan::AssimpModel::MODEL_BINDING_DESCRIPTION,
                                    gfx::vulkan::AssimpModel::MODEL_ATTRIBUTE_DESCRIPTIONS },
            .input_assembly_stage = {{}, vk::PrimitiveTopology::eTriangleList, VK_FALSE},
            .viewport_stage = {{}, 1, nullptr, 1, nullptr},
            .rasterization_stage = {{}, VK_FALSE, VK_FALSE, vk::PolygonMode::eFill, vk::CullModeFlagBits::eBack,
                                    vk::FrontFace::eCounterClockwise, {}, {}, {}, {}, 1.0f},
            .multisample_stage = {{}, vk::SampleCountFlagBits::e1, VK_FALSE, 1.0f, nullptr, VK_FALSE, VK_FALSE},
            .depth_stencil_stage = {{}, VK_TRUE, VK_TRUE, vk::CompareOp::eLessOrEqual, VK_FALSE, VK_FALSE, {}, {}, 0.0f, 1.0f},
            .blending_stage = {{}, VK_FALSE, vk::LogicOp::eCopy, m_blend_attachment_states, {}},

            .dynamic_stage = {{}, {}},
            .push_content_ranges = {vk::PushConstantRange{vk::ShaderStageFlagBits::eFragment, 0, sizeof(RSMPushConstantData)}},

            .descriptor_bindings = {gbuffer_descriptor_set_bindings.cbegin(), gbuffer_descriptor_set_bindings.cend()},

            .sub_pass = 0,
            .pipeline_cache_path = std::string("reflective_shadow_map"),
            .render_pass = render_pass
    };

    return pipeline_info;
}

std::vector<aristaeus::gfx::vulkan::GraphicsPipeline::ShaderInfo> aristaeus::gfx::gi::ReflectiveShadowMap::get_rsm_shader_infos() {
    return std::vector<aristaeus::gfx::vulkan::GraphicsPipeline::ShaderInfo>{
        {
                .code = utils::read_file("environments/vulkan_environment_shaders/engine/environment_shader.vert.spv"),
                .name = "environment_shader.vert",
                .stage = vk::ShaderStageFlagBits::eVertex
        },
        {
                .code = utils::read_file("environments/vulkan_environment_shaders/gi/reflective_shadow_map.frag.spv"),
                .name = "reflective_shadow_map.frag",
                .stage = vk::ShaderStageFlagBits::eFragment
        }
    };
}

void aristaeus::gfx::gi::ReflectiveShadowMap::on_new_node_added_to_tree(gfx::SceneGraphNode &node_added) {
    if (node_added.get_node_groups().contains(std::string{ gfx::vulkan::LightingNode::LIGHTING_NODE_GROUP })) {
        std::shared_ptr<gfx::vulkan::LightingNode> lighting_node =
            std::static_pointer_cast<gfx::vulkan::LightingNode, gfx::SceneGraphNode>(node_added.get_weak_ptr_to_this().lock());

        assert(lighting_node != nullptr);

        bool is_directional_light = lighting_node->get_light_node_ubo_data().light_type == vulkan::DirectionalLight::UBO_DIRECTIONAL_LIGHT_TYPE_ID;

        if ((is_directional_light && m_num_of_2d_maps_occupied < m_num_of_2d_maps) || m_num_of_cube_maps_occupied < m_num_of_cube_maps) {
            std::string event_name{ is_directional_light ? vulkan::DirectionalLight::SHADOW_MAP_CAPTURE_CHANGE_EVENT : SceneGraphNode::GLOBAL_TRANSFORM_CHANGED_EVENT };

            int current_map_index_value = (is_directional_light ? m_num_of_2d_maps_occupied : m_num_of_cube_maps_occupied)++;
            std::string callback_name{ (is_directional_light ? "rsm_shadow_map_capture_" : "rsm_transform_changed_") + std::to_string(current_map_index_value) };

            int num_of_maps = is_directional_light ? 1 : 6;

            lighting_node->add_callback_for_event<void *, ReflectiveShadowMap>(event_name, callback_name,
                [current_map_index_value, num_of_maps](ReflectiveShadowMap &rsm, void *) {
                    rsm.on_positionable_light_global_transform_changed(current_map_index_value, num_of_maps);
                }, *this);

            set_focus_light(is_directional_light ? current_map_index_value : current_map_index_value + m_num_of_2d_maps, lighting_node);
        }
    }
}

void aristaeus::gfx::gi::ReflectiveShadowMap::on_positionable_light_global_transform_changed(int map_index, int num_of_maps) {
    // TODO(Bobby): Have retoggable render contexts, so all lights don't get their rsms re-rendered
    m_reflective_map_phase.retoggle_one_shot_render_pass(std::string{REFLECTIVE_SHADOW_MAP_RENDER_PASS_NAME});
}

void aristaeus::gfx::gi::ReflectiveShadowMap::set_scene_graph_node_root(const std::weak_ptr<SceneGraphNode> &root_node) {
    m_root_node = root_node;

    if (m_auto_find_light_nodes) {
        std::queue<std::shared_ptr<SceneGraphNode>> queue;

        queue.push(root_node.lock());

        if (queue.front() == nullptr) {
            return;
        }

        queue.front()->add_callback_for_event<std::reference_wrapper<SceneGraphNode>, ReflectiveShadowMap>(std::string{gfx::SceneGraphNode::DESCENDENT_NODE_ADDED_EVENT}, 
                                                                          std::string{"reflective_shadow_map_descendent_node_added"}, 
                                                                          [](ReflectiveShadowMap &map, std::reference_wrapper<gfx::SceneGraphNode> node_added) {
                                                                              map.on_new_node_added_to_tree(node_added);
                                                                          }, *this);

        int current_map_index = 0;

        while(!queue.empty() && (current_map_index < m_num_of_2d_maps || current_map_index < m_num_of_cube_maps)) {
            if (queue.front() != nullptr) {
                gfx::SceneGraphNode &current_node = *queue.front();

                for(std::shared_ptr<gfx::SceneGraphNode> &child_node : current_node.get_children()) {
                    queue.push(child_node);
                }

                on_new_node_added_to_tree(current_node);
            }

            queue.pop();
        }
    }
}

std::vector<vk::DescriptorImageInfo> aristaeus::gfx::gi::ReflectiveShadowMap::get_directional_rsm_descriptor_infos() {
    std::vector<vk::DescriptorImageInfo> return_value;
    auto &core_renderer = utils::get_singleton_safe<vulkan::CoreRenderer>();

    for (int i = 0; m_lighting_nodes.size() > i; ++i) {
        std::shared_ptr<vulkan::LightingNode> lighting_node = m_lighting_nodes[i].lock();

        if (lighting_node != nullptr && lighting_node->get_light_node_ubo_data().light_type == vulkan::DirectionalLight::UBO_DIRECTIONAL_LIGHT_TYPE_ID) {
            const std::string render_pass_name{REFLECTIVE_SHADOW_MAP_RENDER_PASS_NAME};

            for (int i2 = 0; RSM_STRIDE > i2; ++i2) {
                // images are laid out like so
                // attachment 1 for #0, ..., attachment 1 for #n, attachment 2 for #0, ..., attachment 2 for #n and so on

                uint32_t current_image_view = (i * RSM_STRIDE) + (i2 * ((6 * m_num_of_cube_maps) + m_num_of_2d_maps));
                std::optional<size_t> last_rendered_frame_optional = m_reflective_map_phase.get_render_pass_one_shot_last_rendered_frame_num(render_pass_name);

                if(last_rendered_frame_optional.has_value()) {
                    uint32_t last_rendered_frame = (*last_rendered_frame_optional) % 2;

                    current_image_view += ((6 * m_num_of_cube_maps) + m_num_of_2d_maps) * RSM_STRIDE * last_rendered_frame;
                }

                vk::raii::ImageView &image_view = m_reflective_map_phase.get_render_pass_image_view(render_pass_name, current_image_view);

                return_value.emplace_back(*m_default_rsm_sampler, *image_view, vk::ImageLayout::eShaderReadOnlyOptimal);
            }
        }
    }

    return return_value;
}

std::vector<vk::DescriptorImageInfo> aristaeus::gfx::gi::ReflectiveShadowMap::get_point_rsm_descriptor_infos() {
    std::vector<vk::DescriptorImageInfo> return_value;
    auto &core_renderer = utils::get_singleton_safe<vulkan::CoreRenderer>();

    for (vk::raii::ImageView &image_view : m_point_light_image_views) {
        return_value.emplace_back(*m_default_rsm_sampler, *image_view, vk::ImageLayout::eShaderReadOnlyOptimal);
    }

    return return_value;
}
