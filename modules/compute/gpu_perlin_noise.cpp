//
// Created by bobby on 16/04/23.
//

#include "gpu_perlin_noise.hpp"

#include <gfx/vulkan/assimp_model.hpp>
#include <gfx/vulkan/core_renderer.hpp>

aristaeus::gfx::vulkan::compute::GPUPerlinNoise::GPUPerlinNoise(
                       const PerlinNoiseInfo &perlin_noise_input_info, 
                       const std::unordered_map<glm::vec2, glm::vec2, utils::Vec2Hash> &predetermined_gradient_vectors,
                       bool is_in_preload_stage, const std::optional<std::string> &cache_path) :
    SingleImageComputeTask(sizeof(PerlinNoiseUniformBuffer), 1u, 0u, get_compute_pipeline_info(is_in_preload_stage),
                           std::string("perlin_compute"), perlin_noise_input_info.resolution, cache_path,
                           vk::Format::eR32Sfloat),
    m_ubo_data(get_ubo_data(perlin_noise_input_info)),
    m_predetermined_gradient_vectors(predetermined_gradient_vectors) {
}

void aristaeus::gfx::vulkan::compute::GPUPerlinNoise::update_descriptor_set() {
    CoreRenderer &core_renderer = utils::get_singleton_safe<CoreRenderer>();

    vk::DescriptorImageInfo storage_image_info = m_output_texture.get_descriptor_image_info(core_renderer);
    vk::DescriptorBufferInfo ubo_info{ m_ubos[0]->buffer, 0, sizeof(PerlinNoiseUniformBuffer) };

    storage_image_info.imageLayout = vk::ImageLayout::eGeneral;
    
    vk::WriteDescriptorSet storage_image_set_write{ *m_descriptor_sets[0], 0, 0, vk::DescriptorType::eStorageImage,
                                                    storage_image_info };
    vk::WriteDescriptorSet ubo_descriptor_set_write{ *m_descriptor_sets[0], 1, 0, vk::DescriptorType::eUniformBuffer,
                                                     {}, ubo_info };

    void *ubo_data;

    vmaMapMemory(m_ubos[0]->allocator, m_ubos[0]->allocation, &ubo_data);

    std::memcpy(ubo_data, &m_ubo_data, sizeof(PerlinNoiseUniformBuffer));

    vmaUnmapMemory(m_ubos[0]->allocator, m_ubos[0]->allocation);

    core_renderer.get_logical_device_ref().updateDescriptorSets({storage_image_set_write, ubo_descriptor_set_write}, {});
}

void aristaeus::gfx::vulkan::compute::GPUPerlinNoise::dispatch(vk::CommandBuffer &command_buffer) {
    if (m_ubo_data.perlin_noise_info.resolution.x % WORK_GROUP_X != 0 || m_ubo_data.perlin_noise_info.resolution.y % WORK_GROUP_Y != 0) {
        throw std::runtime_error("perlin noise res must be a multiple of (" + std::to_string(WORK_GROUP_X) + "," +
                                 std::to_string(WORK_GROUP_Y) + ")");
    }

    m_output_texture.transition_pre_compute(command_buffer);

    update_descriptor_set();

    m_compute_pipeline.bind_to_command_buffer(command_buffer);
    command_buffer.bindDescriptorSets(vk::PipelineBindPoint::eCompute, *m_compute_pipeline.get_pipeline_layout(), 0,
                                      *m_descriptor_sets[0], {});

    command_buffer.dispatch(m_ubo_data.perlin_noise_info.resolution.x / WORK_GROUP_X,
                            m_ubo_data.perlin_noise_info.resolution.y / WORK_GROUP_Y, 1);
}

