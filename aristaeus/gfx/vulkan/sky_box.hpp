//
// Created by bobby on 1/3/22.
//

#ifndef ARISTAEUS_SKY_BOX_HPP
#define ARISTAEUS_SKY_BOX_HPP

#include <string>
#include <vector>

#define GLM_ENABLE_EXPERIMENTAL
#include <stb_image.h>
#include <glm/glm.hpp>

#include "core_renderer.hpp"
#include "cube_map_texture.hpp"
#include "graphics_pipeline.hpp"
#include "render_phase.hpp"
#include "../scene_graph_node.hpp"
#include "texture.hpp"

namespace aristaeus::gfx::vulkan {
    class SkyBox : public SceneGraphNode {
    ARISTAEUS_CLASS_HEADER("sky_box", SkyBox, false)
    public:
        static std::weak_ptr<SkyBox> active_sky_box;

        static constexpr std::string_view NODE_TYPE = "sky_box";

        virtual ~SkyBox() {
            --ALIVE_SKY_BOXES;

            if(ALIVE_SKY_BOXES <= 0) {
                VERTEX_BUFFER.reset(nullptr);
            }
        }

        [[nodiscard]] virtual std::vector<vk::DescriptorImageInfo> get_descriptor_image_infos() const {
            return m_cubemap_texture == nullptr ?
                std::vector<vk::DescriptorImageInfo>{} :
                std::vector<vk::DescriptorImageInfo>{m_cubemap_texture->get_descriptor_image_info()};
        }

        // we want to submit new requests for compute tasks in predraw_load so they get executed within this frame rather than waiting a
        // couple frames
        void draw()override;

        virtual void predraw_load(const vk::CommandBuffer &command_buffer, const glm::vec3 &camera_position)override {}

        void recreate_graphics_pipeline(CoreRenderer &core_renderer);
    protected:
        std::unique_ptr<CubeMapTexture> m_cubemap_texture;
        std::array<std::string, 6> m_cubemap_texture_faces;

        void set_child_create_cubemap_texture(bool child_creates_cubemap_texture) {
            if(m_child_creates_cubemap_texture != child_creates_cubemap_texture) {
                m_child_creates_cubemap_texture = child_creates_cubemap_texture;

                if(!m_child_creates_cubemap_texture) {
                    m_cubemap_texture = std::make_unique<CubeMapTexture>(m_cubemap_texture_faces);
                }
            }
        }
    private:
        static constexpr vk::PipelineColorBlendAttachmentState FORWARD_COLOR_BLEND_ATTACHMENT =
                {VK_FALSE, vk::BlendFactor::eOne, vk::BlendFactor::eZero, vk::BlendOp::eAdd,
                 vk::BlendFactor::eOne, vk::BlendFactor::eZero, vk::BlendOp::eAdd,
                 vk::ColorComponentFlagBits::eR | vk::ColorComponentFlagBits::eG | vk::ColorComponentFlagBits::eB};

        static constexpr vk::VertexInputBindingDescription SKY_BOX_BINDING_DESCRIPTION =
                {0, sizeof(glm::vec3), vk::VertexInputRate::eVertex};
        static constexpr vk::VertexInputAttributeDescription SKY_BOX_ATTRIBUTE_DESCRIPTION =
                {0, 0, vk::Format::eR32G32B32Sfloat, 0};

        static constexpr std::array<vk::DescriptorPoolSize, 2> POOL_SIZES {{
            {vk::DescriptorType::eUniformBuffer, static_cast<uint32_t>(utils::vulkan::MAX_FRAMES_IN_FLIGHT)},
            {vk::DescriptorType::eCombinedImageSampler, static_cast<uint32_t>(utils::vulkan::MAX_FRAMES_IN_FLIGHT)}
        }};

        static constexpr std::array<float, 108> SKYBOX_VERTICES = {
            -1.0f,  1.0f, -1.0f, // -z
            -1.0f, -1.0f, -1.0f,
            1.0f, -1.0f, -1.0f,
            1.0f, -1.0f, -1.0f,
            1.0f,  1.0f, -1.0f,
            -1.0f,  1.0f, -1.0f,

            -1.0f, -1.0f,  1.0f, // -x
            -1.0f, -1.0f, -1.0f,
            -1.0f,  1.0f, -1.0f,
            -1.0f,  1.0f, -1.0f,
            -1.0f,  1.0f,  1.0f,
            -1.0f, -1.0f,  1.0f,

            1.0f, -1.0f, -1.0f, // +x
            1.0f, -1.0f,  1.0f,
            1.0f,  1.0f,  1.0f,
            1.0f,  1.0f,  1.0f,
            1.0f,  1.0f, -1.0f,
            1.0f, -1.0f, -1.0f,

            -1.0f, -1.0f,  1.0f, // +z
            -1.0f,  1.0f,  1.0f,
            1.0f,  1.0f,  1.0f,
            1.0f,  1.0f,  1.0f,
            1.0f, -1.0f,  1.0f,
            -1.0f, -1.0f,  1.0f,

            -1.0f,  1.0f, -1.0f, // +y
            1.0f,  1.0f, -1.0f,
            1.0f,  1.0f,  1.0f,
            1.0f,  1.0f,  1.0f,
            -1.0f,  1.0f,  1.0f,
            -1.0f,  1.0f, -1.0f,

            -1.0f, -1.0f, -1.0f, // -y
            -1.0f, -1.0f,  1.0f,
            1.0f, -1.0f, -1.0f,
            1.0f, -1.0f, -1.0f,
            -1.0f, -1.0f,  1.0f,
            1.0f, -1.0f,  1.0f
        };

        static const std::vector<vk::DescriptorSetLayoutBinding> DESCRIPTOR_SET_LAYOUT_BINDINGS;

        static uint32_t ALIVE_SKY_BOXES;
        static BufferRaii VERTEX_BUFFER;

        void init_sky_box(CoreRenderer &core_renderer);

        GraphicsPipeline::PipelineInfo get_pipeline_info(vk::raii::RenderPass &render_pass);
        std::vector<GraphicsPipeline::ShaderInfo> get_pipeline_shader_infos(const std::string &shader_directory);

        vk::raii::DescriptorPool create_descriptor_pool(CoreRenderer &core_renderer) {
            return {core_renderer.get_logical_device_ref(), {{vk::DescriptorPoolCreateFlagBits::eFreeDescriptorSet},
                     utils::vulkan::MAX_FRAMES_IN_FLIGHT, POOL_SIZES}};
        }

        std::array<vk::raii::DescriptorSet, utils::vulkan::MAX_FRAMES_IN_FLIGHT> create_descriptor_sets(vk::raii::Device &logical_device, RenderContextID render_context_id);

        std::array<BufferRaii, utils::vulkan::MAX_FRAMES_IN_FLIGHT>
                create_camera_ubos(CoreRenderer &core_renderer) {
            std::array<BufferRaii, utils::vulkan::MAX_FRAMES_IN_FLIGHT> ubos;

            for(size_t i = 0; utils::vulkan::MAX_FRAMES_IN_FLIGHT > i; ++i) {
                ubos[i] = core_renderer.get_memory_manager().create_buffer(core_renderer,
                                                      2 * sizeof(glm::mat4),
                                                      vk::BufferUsageFlagBits::eUniformBuffer,
                                                      vk::SharingMode::eExclusive,
                                                      VMA_ALLOCATION_CREATE_HOST_ACCESS_SEQUENTIAL_WRITE_BIT,
                                                      VMA_MEMORY_USAGE_AUTO, "skybox-camera-ubo");
            }

            return ubos;
        }

        std::string m_shader_directory;

        std::unique_ptr<GraphicsPipeline> m_cubemap_pipeline;
        std::unique_ptr<vk::raii::DescriptorSetLayout> m_cubemap_descriptor_set_layout;
        std::unordered_map<RenderContextID,vk::raii::DescriptorPool> m_cubemap_descriptor_pools;
        std::unordered_map<RenderContextID,
            std::array<vk::raii::DescriptorSet, utils::vulkan::MAX_FRAMES_IN_FLIGHT>> m_cubemap_descriptor_sets;

        std::optional<vk::DescriptorImageInfo> m_shared_cubemap_texture_image_info;

        std::array<BufferRaii, utils::vulkan::MAX_FRAMES_IN_FLIGHT> m_camera_ubos;

        bool m_child_creates_cubemap_texture = false;
    };
}

#endif //ARISTAEUS_SKY_BOX_HPP
