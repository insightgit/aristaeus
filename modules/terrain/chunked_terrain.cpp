//
// Created by bobby on 23/02/23.
//

#include "chunked_terrain.hpp"
#include <core/scene_loader.hpp>

#include <gfx/vulkan/rasterization_lighting_environment.hpp>
#include "terrain.hpp"

#include <../libraries/include/imgui_stdlib.h>

#include <utility>

ARISTAEUS_CLASS_IMPL(aristaeus::gfx::vulkan::ChunkedTerrain, typeid(aristaeus::gfx::SceneGraphNode));

void aristaeus::gfx::vulkan::ChunkedTerrain::classdb_register() {
    assert(core::ClassDB::instance != nullptr);
    core::ClassDB &class_db = *core::ClassDB::instance;

    class_db.register_type<ProceduralTerrain::TerrainInfo>([](const nlohmann::json::value_type &json) {
        std::map<glm::vec4, std::vector<Image>, utils::Vec4Hash> texture_paths_height;
        std::unordered_map<Image, glm::vec4> texture_paths_slope;

        bool use_terrain_shader = true;
        int number_of_patches = 60;

        if (json.contains("use_terrain_shader")) {
            use_terrain_shader = json.at("use_terrain_shader").get<bool>();
        }

        if (json.contains("num_of_patches")) {
            number_of_patches = json["num_of_patches"].get<int>();
        }

        for (const nlohmann::json::value_type &texture_path_height_json : json.at("height_slope_ranges")) {
            if (!texture_path_height_json.contains("height_range") || !texture_path_height_json.contains("textures")) {
                std::cerr << "Skipping incomplete ChunkedTerrain texture height and slope range\n";
                continue;
            }

            glm::vec4 height_range = utils::get_vec4_json(texture_path_height_json.at("height_range"));
            std::vector<Image> terrain_images;

            for (const nlohmann::json::value_type &texture_json: texture_path_height_json.at("textures")) {
                if (!texture_json.contains("path") && !texture_json.contains("type")) {
                    std::cerr << "Skipping incomplete terrain Image\n";
                    continue;
                }

                terrain_images.push_back(Image{
                    .image_path = core::SceneLoader::instance->get_environment_path_for_file(texture_json.at("path").template get<std::string>()),
                    .image_type = "texture_" + texture_json.at("type").template get<std::string>()
                });
            }
            
            if (terrain_images.empty()) {
                std::cerr << "Skipping incomplete ChunkedTerrain with no complete textures\n";
                continue;
            }

            if (texture_paths_height.contains(height_range)) {
                texture_paths_height.at(height_range).insert(texture_paths_height.at(height_range).end(), terrain_images.begin(), terrain_images.end());
            }
            else {
                texture_paths_height.insert({ height_range, terrain_images });
            }

            if (texture_path_height_json.contains("slope_range")) {
                glm::vec4 slope_range = utils::get_vec4_json(texture_path_height_json.at("slope_range"));

                for (Image terrain_image : terrain_images) {
                    texture_paths_slope.insert({ terrain_image, slope_range });
                }
            }
        }

        return ProceduralTerrain::TerrainInfo{
            .height_magnitude = json.at("height_magnitude").template get<float>(),
            .height_map_pos_ratio = json.at("height_map_pos_ratio").template get<float>(),
            .number_of_perlin_octaves = json.at("number_of_perlin_octaves").template get<int>(),
            .number_of_textures = json.at("number_of_textures").template get<int>(),
            .multi_textured_terrain = json.at("multi_textured_terrain").template get<bool>(),
            .texture_paths_height = texture_paths_height,
            .texture_paths_slope = texture_paths_slope,
            .perlin_persistence = json.at("perlin_persistence").template get<float>(),
            .texture_path = "",
            .use_terrain_shader = use_terrain_shader,
            .vertex_resolution = json.at("vertex_resolution").template get<float>(),
            .number_of_patches = number_of_patches,
            .use_grass_shader = false,
            .prop_info = {.enabled = false}
            // TODO(Bobby): figure out how to parse prop_info;
        };
    },
    aristaeus::core::ClassDB::UiFunctions<ProceduralTerrain::TerrainInfo>{
        .parse_function = terrain_info_ui_parse,
        .constructor_function = [] {
            return ProceduralTerrain::TerrainInfo{};
        }
    });
    class_db.register_type<ProceduralTerrain::WaterInfo>([](const nlohmann::json::value_type &json) {
        static constexpr std::array<std::string_view, 14> REQUIRED_WATER_INFO{ "noise_amplitude",
                                                                           "number_of_textures", "texture_path",
                                                                           "y_spawn_threshold", "noise_influence",
                                                                           "noise_persistence",
                                                                           "number_of_perlin_octaves",
                                                                           "noise_frequency",
                                                                           "noise_frequency_increase",
                                                                           "shininess", "reflectivity",
                                                                           "refractive_index", "refractive_strength",
                                                                           "transmission" };

        ProceduralTerrain::WaterInfo water_info;

        if (json.contains("draw_water")) {
            water_info.draw_water = json.at("draw_water");

            if (water_info.draw_water) {
                for (const std::string_view &required_water_attribute : REQUIRED_WATER_INFO) {
                    if (!json.contains(required_water_attribute)) {
                        water_info.draw_water = false;

                        std::cerr << "[SCENE] Ignoring invalid water info "
                                     "(missing " << required_water_attribute << ")\n";

                        return water_info;
                    }
                }

                water_info.noise_amplitude = utils::get_vec3_json(json.at("noise_amplitude"));
                water_info.noise_frequency = utils::get_vec3_json(json.at("noise_frequency"));
                water_info.noise_frequency_increase = utils::get_vec3_json(json.at("noise_frequency_increase"));
                water_info.noise_influence = utils::get_vec3_json(json.at("noise_influence"));
                water_info.noise_persistence = utils::get_vec3_json(json.at("noise_persistence"));
                water_info.number_of_perlin_octaves = utils::get_vec3_json(json.at("number_of_perlin_octaves"));
                water_info.number_of_textures = json.at("number_of_textures").template get<int>();
                water_info.reflectivity = json.at("reflectivity").template get<float>();
                water_info.refractive_index = json.at("refractive_index").template get<float>();
                water_info.refractive_strength = json.at("refractive_strength").template get<float>();
                water_info.shininess = json.at("shininess").template get<float>();
                water_info.texture_path = core::SceneLoader::instance->get_environment_path_for_file(json.at("texture_path").template get<std::string>());
                water_info.transmission = json.at("transmission").template get<float>();
                water_info.y_spawn_threshold = json.at("y_spawn_threshold").template get<float>();

                // TODO(Bobby): include tessellation mode
                water_info.tessellation_amount = json.at("tessellation_amount").template get<int>();
                water_info.number_of_patches = json.at("num_of_patches").template get<int>();

                nlohmann::json::value_type wave_properties_json = json.at("wave_properties");

                water_info.wave_properties = {
                    .count = wave_properties_json.at("count"),
                    .median_wavelength = wave_properties_json.at("median_wavelength"),
                    .median_amplitude = wave_properties_json.at("median_amplitude"),
                    .median_sharpness = wave_properties_json.at("median_sharpness"),
                    .direction = utils::get_vec2_json(wave_properties_json.at("direction"))
                };
            }
        }
        else {
            water_info.draw_water = false;
        }

        return water_info;
    }, 
    aristaeus::core::ClassDB::UiFunctions<ProceduralTerrain::WaterInfo>{
        .parse_function = water_info_ui_parse,
        .constructor_function = [] {
            return ProceduralTerrain::WaterInfo{};
        }
    });

    class_db.register_property<ChunkedTerrain, ProceduralTerrain::TerrainInfo>("default_terrain_info", [](ChunkedTerrain &chunked_terrain) {return chunked_terrain.m_default_terrain_info; },
                                                                               [](ChunkedTerrain &chunked_terrain, ProceduralTerrain::TerrainInfo terrain_info) { chunked_terrain.m_default_terrain_info = terrain_info; }, {});
    class_db.register_property<ChunkedTerrain, ProceduralTerrain::WaterInfo>("default_water_info",
        [](ChunkedTerrain &chunked_terrain){return chunked_terrain.m_default_water_info;},
        [](ChunkedTerrain &chunked_terrain, ProceduralTerrain::WaterInfo water_info)
        {chunked_terrain.m_default_water_info = water_info;},
        [](){return ProceduralTerrain::WaterInfo{.draw_water = false};});
    class_db.register_property<ChunkedTerrain, glm::ivec2>("height_map_resolution",
                                                            [](ChunkedTerrain &chunked_terrain){ return chunked_terrain.m_height_map_resolution;},
                                                            [](ChunkedTerrain &chunked_terrain, glm::ivec2 height_map_resolution)
                                                            {chunked_terrain.m_height_map_resolution = height_map_resolution;}, {});
    class_db.register_property<ChunkedTerrain, uint32_t>("seed",
    [](ChunkedTerrain &chunked_terrain){return chunked_terrain.m_seed;},
    [](ChunkedTerrain &chunked_terrain, uint32_t seed) {
        chunked_terrain.m_seed = seed;
        chunked_terrain.m_rng = std::mt19937(chunked_terrain.m_seed);
    }, [](){return std::chrono::system_clock::now().time_since_epoch().count();});

    class_db.register_property<ChunkedTerrain, float>("draw_distance",
                               [](ChunkedTerrain &chunked_terrain) { return chunked_terrain.m_draw_distance;},
                               [](ChunkedTerrain &chunked_terrain, float draw_distance) { chunked_terrain.m_draw_distance = draw_distance; },
                               {});
    class_db.register_property<ChunkedTerrain, float>("smoothing_percent",
                               [](ChunkedTerrain &chunked_terrain) { return chunked_terrain.m_smoothing_percent;},
                               [](ChunkedTerrain &chunked_terrain, float smoothing_percent) { chunked_terrain.m_smoothing_percent = smoothing_percent; },
                               {});
    class_db.register_property<ChunkedTerrain, size_t>("max_loaded_terrains",
                               [](ChunkedTerrain &chunked_terrain) { return chunked_terrain.m_max_loaded_terrains;},
                               [](ChunkedTerrain &chunked_terrain, size_t max_loaded_terrains) {
                                   chunked_terrain.m_max_loaded_terrains = max_loaded_terrains;

                                   // TODO(Bobby): handle copying elements from one LRUCache to another
                                   // TODO(Bobby): if m_loaded terrains isn't initially null
                                   chunked_terrain.m_loaded_terrains =
                                       std::make_shared<LRUCacheNode<glm::vec2, ProceduralTerrain, utils::Vec2Hash>>("", max_loaded_terrains, true);

                                   chunked_terrain.m_loaded_terrains->rename_node("TerrainLRUCache");
                               },
                               {});

    class_db.register_event_callback_on_construction<ChunkedTerrain, void*>(std::string{SceneGraphNode::PARSING_DONE_EVENT},
                                                                            "chunked_terrain_on_parsing_done",
                                                                            [](ChunkedTerrain &chunked_terrain, void*) {
        chunked_terrain.on_parsing_done();
    });
}

void aristaeus::gfx::vulkan::ChunkedTerrain::on_parsing_done() {
    assert(m_loaded_terrains != nullptr);

    m_height_patch_pipeline.reset(new GraphicsPipeline{*CoreRenderer::instance,
                       RasterizationLightingEnvironment::get_forward_pipeline_shader_infos(RasterizationLightingEnvironment::ForwardPipelineType::RegularEnvironment),
                       get_height_patch_pipeline_pipeline_info(*utils::get_singleton_safe<CoreRenderer>().get_default_rasterization_render_pass())});

    std::uniform_int_distribution<int> random_int_generator_distribution(1000, 10000000);

    m_origin_random_start = glm::vec2{ random_int_generator_distribution(m_rng),
                                       random_int_generator_distribution(m_rng) };

    add_child(m_loaded_terrains);
}

aristaeus::gfx::vulkan::GraphicsPipeline::PipelineInfo
aristaeus::gfx::vulkan::ChunkedTerrain::get_height_patch_pipeline_pipeline_info(vk::raii::RenderPass &render_pass) {
    return {
            .vertex_input_stage = {{}, gfx::vulkan::AssimpMesh::MESH_BINDING_DESCRIPTION,
                                   gfx::vulkan::AssimpModel::MODEL_ATTRIBUTE_DESCRIPTIONS},
            .input_assembly_stage = {{}, vk::PrimitiveTopology::eTriangleList, VK_FALSE},
            .viewport_stage = {{}, 1, nullptr, 1, nullptr},
            .rasterization_stage = {{}, VK_FALSE, VK_FALSE, vk::PolygonMode::eFill, vk::CullModeFlagBits::eNone,
                                    vk::FrontFace::eCounterClockwise, {}, {}, {}, {}, 1.0f},
            .multisample_stage = {{}, vk::SampleCountFlagBits::e1, VK_FALSE, 1.0f, nullptr, VK_FALSE, VK_FALSE},
            .depth_stencil_stage = {{}, VK_TRUE, VK_TRUE, vk::CompareOp::eLessOrEqual, VK_FALSE, VK_FALSE, {}, {},
                                    0.0f, 1.0f},
            .blending_stage = {{}, VK_FALSE, vk::LogicOp::eCopy,
                               RasterizationLightingEnvironment::FORWARD_COLOR_BLEND_ATTACHMENT, {0, 0, 0, 0}},

            .dynamic_stage = {{}, RasterizationLightingEnvironment::FORWARD_DYNAMIC_STATE},
            .push_content_ranges = {},

            .descriptor_bindings = {RasterizationLightingEnvironment::FORWARD_DESCRIPTOR_SET_BINDINGS.cbegin(),
                                    RasterizationLightingEnvironment::FORWARD_DESCRIPTOR_SET_BINDINGS.cend()},

            .sub_pass = utils::get_singleton_safe<CoreRenderer>().get_default_rasterization_subpass(),
            .pipeline_cache_path = "chunked_height_patch_pipeline_cache",
            .render_pass = render_pass
    };
}

aristaeus::gfx::vulkan::ProceduralTerrain *aristaeus::gfx::vulkan::ChunkedTerrain::load_terrain_in(
        const glm::ivec2 &current_chunk_position) {
    // glm::vec2{1000, 1000} at least + random seed because chunks aren't seamless with negative numbers
    // TODO(Bobby): make all the random_start stuff in terms of glm::uvec2 instead
    CoreRenderer &core_renderer = utils::get_singleton_safe<CoreRenderer>();

    glm::vec2 random_start = m_origin_random_start + (glm::vec2{current_chunk_position.x, current_chunk_position.y} * glm::vec2{m_height_map_resolution});
    vk::CommandBuffer loading_command_buffer =
            core_renderer.get_graphics_command_buffer_manager().get_primary_command_buffer(core_renderer.get_current_frame());

    glm::vec3 terrain_mesh_position {1000 + (m_default_terrain_info.vertex_resolution - 1) * current_chunk_position.x,
                                     0,
                                     1000 + (m_default_terrain_info.vertex_resolution - 1) * current_chunk_position.y};

    ProceduralTerrain *terrain_ptr = new ProceduralTerrain(m_default_terrain_info, m_default_water_info,
                                                           m_rng, m_height_map_resolution, {}, {}, true, true,
                                                           {random_start}, {loading_command_buffer});

    terrain_ptr->rename_node("Terrain-(" +
        std::to_string(current_chunk_position.x) + "," + std::to_string(current_chunk_position.y) + ")");

    // do we need to pass in an environment pipeline?
    // why are terrains getting deleted here with multi-threading?
    return terrain_ptr;
}


void aristaeus::gfx::vulkan::ChunkedTerrain::predraw_load(const vk::CommandBuffer &command_buffer,
                                                          const glm::vec3 &camera_position) {
    glm::ivec2 chunk_camera_position = get_camera_chunk_position(camera_position);
    CoreRenderer &core_renderer = utils::get_singleton_safe<CoreRenderer>();

    m_loaded_terrains->delete_unused(core_renderer.get_current_frame());

    if(chunk_camera_position != m_previous_chunk_position[core_renderer.get_current_frame()]) {
        create_loading_tasks(chunk_camera_position);

        m_previous_chunk_position[core_renderer.get_current_frame()] = chunk_camera_position;
    }

    m_predraw_load_position = camera_position;
}

float aristaeus::gfx::vulkan::ChunkedTerrain::get_smoothed_height_map_value(glm::vec2 height_map_coords,
                                                                            glm::ivec2 current_chunk_position,
                                                                            uint32_t current_frame) {
    // TODO(Bobby): Do diagnols if smoothstep works

    glm::vec2 pixel_diff = glm::vec2(1.0) / glm::vec2(m_height_map_resolution);

    if(height_map_coords.x == 0) {
        height_map_coords.x += pixel_diff.x;
    } else if(height_map_coords.x == 1) {
        height_map_coords.x -= pixel_diff.x;
    }

    if(height_map_coords.y == 0) {
        height_map_coords.y += pixel_diff.y;
    } else if(height_map_coords.y == 1) {
        height_map_coords.y -= pixel_diff.y;
    }
    std::optional<std::shared_ptr<ProceduralTerrain>> terrain_info = m_loaded_terrains->get(glm::vec2(current_chunk_position), {current_frame});

    std::shared_ptr<ProceduralTerrain> original_terrain = terrain_info.value();

    float height_map_value =
            original_terrain->get_height_map_value_normalized(height_map_coords.x, height_map_coords.y);


    // top edge
    if(height_map_coords.y >= 1 - m_smoothing_percent) {
        glm::vec2 top_tex_coords = glm::vec2(height_map_coords.x, 0);
        float scaling_value =
                glm::mix(1 - m_smoothing_percent, 1.0f, height_map_coords.y);

        // is this height_map[7]?
        std::optional<std::shared_ptr<ProceduralTerrain>> bottom_terrain =
            m_loaded_terrains->get(current_chunk_position + glm::ivec2(0, 1), {current_frame});

        if(bottom_terrain.has_value()) {
            float bottom_height_value = bottom_terrain.value()->get_height_map_value_normalized(
                                                                                       top_tex_coords.x,
                                                                                       top_tex_coords.y);

            height_map_value = height_map_value * scaling_value + bottom_height_value * (1 - scaling_value);
        }
    }

    // right edge
    if(height_map_coords.x <= m_smoothing_percent) {
        glm::vec2 right_tex_coords = glm::vec2(1, height_map_coords.y);
        float scaling_value = glm::mix(0.0f, m_smoothing_percent, height_map_coords.x);

        std::optional<std::shared_ptr<ProceduralTerrain>> right_terrain =
                m_loaded_terrains->get(current_chunk_position + glm::ivec2(1, 0), current_frame);

        if(right_terrain.has_value()) {
            float right_height_value =
                    right_terrain.value()->get_height_map_value_normalized(right_tex_coords.x, right_tex_coords.y);

            height_map_value = height_map_value * (1 - scaling_value) + right_height_value * scaling_value;
        }
    }

    return height_map_value * m_default_terrain_info.height_magnitude;
}

float aristaeus::gfx::vulkan::ChunkedTerrain::get_smoothed_height_slope(glm::vec2 height_map_coords,
                                                                        glm::ivec2 current_chunk_position,
                                                                        uint32_t current_frame) {
    glm::vec2 pixel_diff = glm::vec2(1.0 / m_default_terrain_info.vertex_resolution);
    float original_height = get_smoothed_height_map_value(height_map_coords, current_chunk_position, current_frame);

    float greatest_slope = -1000.0f;

    if(height_map_coords.y - (3 * pixel_diff.y) >= 0) {
        float vertex_y_1 = abs(get_smoothed_height_map_value(height_map_coords + glm::vec2(0, -pixel_diff.y),
                                                             current_chunk_position, current_frame) - original_height);

        if(vertex_y_1 > greatest_slope) {
            greatest_slope = vertex_y_1;
        }
    }

    if(height_map_coords.x + (3 * pixel_diff.x) < 1) {
        float vertex_y_2 =
                get_smoothed_height_map_value(height_map_coords + glm::vec2(pixel_diff.x, 0),
                                                  current_chunk_position, current_frame) - original_height;

        if(vertex_y_2 > greatest_slope) {
            greatest_slope = vertex_y_2;
        }
    }

    if(height_map_coords.y + (3 * pixel_diff.y) < 1) {
        float vertex_y_3 =
                abs(get_smoothed_height_map_value(height_map_coords + glm::vec2(0, pixel_diff.y),
                                                  current_chunk_position, current_frame) - original_height);

        if(vertex_y_3 > greatest_slope) {
            greatest_slope = vertex_y_3;
        }
    }

    if(height_map_coords.x - (3 * pixel_diff.x) >= 0) {
        float vertex_y_4 = abs(get_smoothed_height_map_value(height_map_coords + glm::vec2(-pixel_diff.x, 0),
                                                             current_chunk_position, current_frame)
                                - original_height);

        if(vertex_y_4 > greatest_slope) {
            greatest_slope = vertex_y_4;
        }
    }

    return greatest_slope / m_default_terrain_info.height_magnitude;
}

void aristaeus::gfx::vulkan::ChunkedTerrain::draw(const vk::CommandBuffer &command_buffer,
                                                  vk::raii::PipelineLayout &pipeline_layout, size_t instance_number) {
    CoreRenderer &core_renderer = utils::get_singleton_safe<CoreRenderer>();

    glm::ivec2 chunk_camera_position = get_camera_chunk_position(m_predraw_load_position);

    uint32_t current_frame = core_renderer.get_current_frame();

    for(int x = chunk_camera_position.x - m_draw_distance;
        chunk_camera_position.x + m_draw_distance >= x; ++x) {
        for(int z = chunk_camera_position.y - m_draw_distance;
            chunk_camera_position.y + m_draw_distance >= z; ++z) {
            glm::vec2 current_chunk_position {x, z};
            std::optional<glm::vec3> test_color;

            glm::vec3 terrain_mesh_position {(m_default_terrain_info.vertex_resolution - 1) * current_chunk_position.x,
                                             0,
                                             (m_default_terrain_info.vertex_resolution - 1) * current_chunk_position.y};

            std::optional<std::shared_ptr<ProceduralTerrain>> loaded_terrain_optional =
                    m_loaded_terrains->get(current_chunk_position, current_frame);

            std::shared_ptr<ProceduralTerrain> loaded_terrain = loaded_terrain_optional.value();

            std::vector<std::shared_ptr<Texture>> height_map_textures = get_height_map_textures(current_frame,
                                                                                                current_chunk_position);

            if(m_test_mode) {
                if(x == -1 && z == -1) {
                    test_color = glm::vec3{1.0f, 0.0f, 0.0f};
                } else if(x == -1 && z == 0) {
                    test_color = glm::vec3{0.0f, 1.0f, 0.0f};
                } else if(x == -1 && z == 1) {
                    test_color = glm::vec3{0.0f, 0.0f, 1.0f};
                } else if(x == 0 && z == -1) {
                    test_color = glm::vec3{1.0f, 1.0f, 0.0f};
                } else if(x == 0 && z == 0) {
                    test_color = glm::vec3{0.0f, 0.0f, 0.0f};
                } else if(x == 0 && z == 1) {
                    test_color = glm::vec3{0.976f, 0.505f, 0.686f};
                } else if(x == 1 && z == -1) {
                    test_color = glm::vec3{0.749f, 0.0f, 1.0f};
                } else if(x == 1 && z == 0) {
                    test_color = glm::vec3{1.0f, 0.533f, 0.0f};
                } else if(x == 1 && z == 1) {
                    test_color = glm::vec3{0.0f, 1.0f, 1.0f};
                }
                else {
                    if (x % 2 == 0) {
                        test_color = glm::vec3{ 1.0f, 1.0f, 1.0f };
                    }
                    else {
                        test_color = glm::vec3{ 0.5f, 0.5f, 0.5f };
                    }
                }
            }

            loaded_terrain->set_test_color(test_color);

            loaded_terrain->set_position(terrain_mesh_position);
            loaded_terrain->set_chunked_settings(m_smoothing_percent, glm::ivec2{current_chunk_position});
            loaded_terrain->set_height_map_textures(height_map_textures);
        }
    }
}

glm::ivec2 aristaeus::gfx::vulkan::ChunkedTerrain::get_camera_chunk_position(const glm::vec3 &camera_position) {
    // TODO(Bobby): figure out why it's not showing the other 25 percent of the terrain
    float terrain_size = m_default_terrain_info.vertex_resolution - 1;
    glm::vec3 chunk_pos_with_y = (camera_position - get_position()) / terrain_size;
    glm::vec2 raw_camera_chunk_position {chunk_pos_with_y.x, chunk_pos_with_y.z};


    glm::vec2 float_chunk_camera_position {raw_camera_chunk_position.x, raw_camera_chunk_position.y};

    if (float_chunk_camera_position.x > 0.5f) {
        float_chunk_camera_position.x += 0.5f;
    }
    else if (float_chunk_camera_position.x < -0.5f) {
        float_chunk_camera_position.x -= 0.5f;
    }

    if (float_chunk_camera_position.y > 0.5f) {
        float_chunk_camera_position.y += 0.5f;
    }
    else if (float_chunk_camera_position.y < -0.5f) {
        float_chunk_camera_position.y -= 0.5f;
    }

    //std::cout << "current float chunk camera position: " << glm::to_string(float_chunk_camera_position) << "\n";

    return float_chunk_camera_position;
}

std::vector<std::shared_ptr<aristaeus::gfx::vulkan::Texture>>
    aristaeus::gfx::vulkan::ChunkedTerrain::get_height_map_textures(uint32_t current_frame_num,
                                                                    const glm::ivec2 &current_chunk_position) {
    std::vector<std::shared_ptr<Texture>> return_value;

    for(int x = current_chunk_position.x - 1;
        current_chunk_position.x + 1 >= x; ++x) {
        for (int z = current_chunk_position.y - 1;
             current_chunk_position.y + 1 >= z; ++z) {
            glm::vec2 chunk_neighbor_position {x, z};

            std::optional<std::shared_ptr<ProceduralTerrain>> chunk_neighbor_terrain =
                    m_loaded_terrains->get(chunk_neighbor_position, current_frame_num);

            // TODO(Bobby): Add partially loaded terrain smoothing
            if(x == current_chunk_position.x && z == current_chunk_position.y) {
                continue;
            } else if(!chunk_neighbor_terrain.has_value()) {
                return {};
            } else {
                std::shared_ptr<Texture> height_map_texture = chunk_neighbor_terrain.value()->get_terrain_height_map_texture();
                std::shared_ptr<Texture> &texture_to_use = height_map_texture == nullptr ? AssimpMesh::NULL_TEXTURE : height_map_texture;

                return_value.push_back(texture_to_use);
            }
        }
    }

    return return_value;
}

void aristaeus::gfx::vulkan::ChunkedTerrain::toggle_edge_debug_mode() {
    // TODO(Bobby): move this into a tese-acessible environment ubo attribute
    for(size_t i = 0; utils::vulkan::MAX_FRAMES_IN_FLIGHT > i; ++i) {
        for(auto loaded_terrain : m_loaded_terrains->get_underlying_time_list()) {
            assert(loaded_terrain.value != nullptr);

            loaded_terrain.value->toggle_edge_debug_mode();
        }
    }
}


void aristaeus::gfx::vulkan::ChunkedTerrain::create_loading_tasks(const glm::ivec2 &chunk_camera_position) {
    CoreRenderer &core_renderer = utils::get_singleton_safe<CoreRenderer>();

    m_terrain_loading_counter.store(0);

    m_terrain_loading_taskflows[core_renderer.get_current_frame()].clear();

    m_needed_chunks_task = m_terrain_loading_taskflows[core_renderer.get_current_frame()].emplace([this, &chunk_camera_position] (tf::Subflow &subflow) {
        // one way to optimize this: precompute all perlin data, send it to all of these loaded terrains to process afterward

        auto start = std::chrono::system_clock::now();

        CoreRenderer &core_renderer = utils::get_singleton_safe<CoreRenderer>();

        for(int x = chunk_camera_position.x - m_draw_distance;
            chunk_camera_position.x + m_draw_distance >= x; ++x) {
            for (int z = chunk_camera_position.y - m_draw_distance;
                 chunk_camera_position.y + m_draw_distance >= z; ++z) {
                glm::vec2 chunk_position {x, z};

                //subflow.emplace([this, chunk_position, &core_renderer]() {
                    if (!m_loaded_terrains->get(chunk_position, core_renderer.get_current_frame()).has_value()) {
                        std::shared_ptr<ProceduralTerrain> procedural_terrain {load_terrain_in(chunk_position)};

                        auto loaded_terrain_pair = std::pair<glm::vec2, std::shared_ptr<ProceduralTerrain>>(
                                                                                                    chunk_position,
                                                                                                    procedural_terrain);

                        m_loaded_terrains->put(std::move(loaded_terrain_pair), core_renderer.get_current_frame());
                        ++m_terrain_loading_counter;
                    }
                //});
            }
        }

        auto end = std::chrono::system_clock::now();

        std::chrono::duration<double> elapsed_seconds = end - start;

        std::cout << "needed_chunks took " << elapsed_seconds.count() << "s\n";
    });

    tf::Task generate_seam_connectors_task =
            m_terrain_loading_taskflows[core_renderer.get_current_frame()].emplace([this, &chunk_camera_position]
                                                                                           (tf::Subflow &subflow) {
                if(m_height_patch_model.contains(chunk_camera_position)) {
                    return;
                }

                auto start = std::chrono::system_clock::now();

                CoreRenderer &core_renderer = utils::get_singleton_safe<CoreRenderer>();
                glm::vec2 end_coords {1.0f - (1.0f / m_height_map_resolution.x), 1.0f - (1.0f / m_height_map_resolution.y)};

                // right edge
                glm::ivec2 working_chunk_position = chunk_camera_position + glm::ivec2(1, 0);
                std::optional<std::shared_ptr<ProceduralTerrain>> terrain = m_loaded_terrains->get(working_chunk_position, core_renderer.get_current_frame());

                std::vector<Vertex> vertices;
                std::vector<unsigned int> indices;
                unsigned int current_index = 0;
                float triangle_width = m_default_terrain_info.vertex_resolution * (1.0f / m_height_map_resolution.y);
                float x_value = m_default_terrain_info.vertex_resolution / 2;

                if(terrain.has_value()) {
                    //assert(terrain.value()->loaded_terrain != nullptr);

                    for(int y = 0; m_height_map_resolution.y > y; ++y) {
                        float y_coord_height_map = y * (1.0f / m_height_map_resolution.y);
                        glm::vec2 base_height_map_coords {1.0f - (1.0f / m_height_map_resolution.x), y_coord_height_map};
                        glm::vec2 smoothed_height_map_coords {0, y_coord_height_map};
                        float z_coord_model =
                                (-m_default_terrain_info.vertex_resolution / 2.0f) +
                                (m_default_terrain_info.vertex_resolution * (static_cast<float>(y) / m_height_map_resolution.y));

                        float base_height_map_value = get_smoothed_height_map_value(base_height_map_coords,
                                                                                    chunk_camera_position,
                                                                                    core_renderer.get_current_frame());
                        float height_map_value = get_smoothed_height_map_value(smoothed_height_map_coords,
                                                                               working_chunk_position,
                                                                               core_renderer.get_current_frame());

                        vertices.push_back({
                            .position = glm::vec3{x_value, -2 * height_map_value, z_coord_model}
                        });
                        vertices.push_back({
                            .position = glm::vec3{x_value, -2 * height_map_value, z_coord_model + triangle_width}
                        });
                        vertices.push_back({
                            .position = glm::vec3{x_value, -2 * base_height_map_value, z_coord_model}
                        });
                        vertices.push_back({
                            .position = glm::vec3{x_value, -2 * base_height_map_value, z_coord_model + triangle_width}
                        });

                        indices.push_back(current_index);

                        if(base_height_map_value > height_map_value) {
                            indices.push_back(current_index + 1);
                            indices.push_back(current_index + 2);

                            // second triangle
                            indices.push_back(current_index + 1);
                            indices.push_back(current_index + 3);
                            indices.push_back(current_index + 2);
                        } else {
                            indices.push_back(current_index + 2);
                            indices.push_back(current_index + 1);

                            // second triangle
                            indices.push_back(current_index + 1);
                            indices.push_back(current_index + 2);
                            indices.push_back(current_index + 3);
                        }

                        current_index += 4;
                    }
                    AssimpMesh assimp_mesh{vertices, indices,
                                           std::vector<std::shared_ptr<Texture>>{}, false, core_renderer};
                    glm::vec3 terrain_mesh_position {(m_default_terrain_info.vertex_resolution - 1) * chunk_camera_position.x,
                                                     0,
                                                     (m_default_terrain_info.vertex_resolution - 1) * chunk_camera_position.y};

                    m_height_patch_model.insert({chunk_camera_position, std::move(assimp_mesh)});

                    m_height_patch_model.at(chunk_camera_position).set_position(terrain_mesh_position);
                }

                auto end = std::chrono::system_clock::now();

                std::chrono::duration<double> elapsed_seconds = end - start;

                std::cout << "seam_connectors took " << elapsed_seconds.count() << "s\n";
            });

    if(m_default_terrain_info.prop_info.enabled) {
        assert(m_default_terrain_info.prop_info.instanced_mesh_pipeline != nullptr);

        tf::Task load_in_props_task =
        m_terrain_loading_taskflows[core_renderer.get_current_frame()].emplace([this, &chunk_camera_position,
                                                                                &core_renderer]
                        (tf::Subflow &subflow) {

            /*auto start = std::chrono::system_clock::now();
                    for (int x = chunk_camera_position.x - m_draw_distance;
                        chunk_camera_position.x + m_draw_distance >= x; ++x) {
                        for (int z = chunk_camera_position.y - m_draw_distance;
                             chunk_camera_position.y + m_draw_distance >= z; ++z) {
                            glm::vec2 chunk_position {x, z};
                            //if(chunk_position.x == 0 && chunk_position.y == 0) {
                                //
                                subflow.emplace([this, &core_renderer, chunk_position]() {
                                    std::optional<std::shared_ptr<ProceduralTerrain>> terrain =
                                            m_loaded_terrains->get(chunk_position, core_renderer.get_current_frame());

                                    if(terrain.value()->prop_loader != nullptr) {
                                        return;
                                    }

                                    GraphicsPipeline &pipeline_ref =
                                            *m_default_terrain_info.prop_info.instanced_mesh_pipeline;

                                    terrain.value()->prop_loader = std::make_shared<ChunkPropLoader>(pipeline_ref,
                                                             m_default_terrain_info.prop_info.prop_data,
                                                             m_default_terrain_info.prop_info.amount_of_props,
                                                             m_height_map_resolution, *this, core_renderer, m_rng,
                                                             chunk_position);
                                });
                            //}
                        }
                    }
                    auto end = std::chrono::system_clock::now();

                    std::chrono::duration<double> elapsed_seconds = end - start;

                    std::cout << "load_in_props took " << elapsed_seconds.count() << "s\n";*/
                });

        load_in_props_task.succeed(m_needed_chunks_task);
        generate_seam_connectors_task.succeed(m_needed_chunks_task, load_in_props_task);
    } else {
        generate_seam_connectors_task.succeed(m_needed_chunks_task);
    }

    if(m_terrain_taskflow_observer != nullptr) {
        core_renderer.get_task_manager().get_executor().remove_observer(m_terrain_taskflow_observer);
        m_terrain_loading_taskflow_future.cancel();
    }

    m_terrain_taskflow_observer =
            core_renderer.get_task_manager().get_executor().make_observer<TaskflowObserver>(m_needed_chunks_task,
                                                                                            m_optional_chunks_task);
    m_terrain_loading_taskflow_future =
            core_renderer.get_task_manager().execute_loading_taskflow(&m_terrain_loading_taskflows[core_renderer.get_current_frame()]);

    m_terrain_loading_taskflow_future.wait();
}

void aristaeus::gfx::vulkan::ChunkedTerrain::draw_imgui(CoreRenderer &core_renderer, const glm::vec3 &camera_position) {
    // TODO(Bobby): reimplement this: maybe as a supplemental child node?
}

#ifndef NDEBUG
void aristaeus::gfx::vulkan::ChunkedTerrain::toggle_normal_debug_mode() {
    /*for(std::pair<const glm::vec2, std::unique_ptr<ProceduralTerrain>> &loaded_terrain : m_loaded_terrains) {
        loaded_terrain.second->toggle_normal_debug_mode();
    }*/
}

#endif

nlohmann::json aristaeus::gfx::vulkan::ChunkedTerrain::terrain_info_ui_parse(ProceduralTerrain::TerrainInfo &persistent_data, bool display_gui, const std::optional<std::string> &label_name) {
    assert(core::ClassDB::instance != nullptr);
    core::ClassDB &class_db = *core::ClassDB::instance;
    
    nlohmann::json return_value;

    return_value["height_magnitude"] = class_db.get_already_registered_ui_functions<float>()->parse_function(persistent_data.height_magnitude, display_gui, "Height Magnitude");
    return_value["height_map_pos_ratio"] = class_db.get_already_registered_ui_functions<float>()->parse_function(persistent_data.height_map_pos_ratio, display_gui, "Height Map Position Ratio");
    return_value["number_of_perlin_octaves"] = class_db.get_already_registered_ui_functions<int>()->parse_function(persistent_data.number_of_perlin_octaves, display_gui, "Number of Perlin Octaves");
    return_value["number_of_textures"] = class_db.get_already_registered_ui_functions<int>()->parse_function(persistent_data.number_of_textures, display_gui, "Number of Textures");
    return_value["multi_textured_terrain"] = class_db.get_already_registered_ui_functions<bool>()->parse_function(persistent_data.multi_textured_terrain, display_gui, "Multi Textured Terrain");
    return_value["perlin_persistence"] = class_db.get_already_registered_ui_functions<float>()->parse_function(persistent_data.perlin_persistence, display_gui, "Perlin Persistence");
    return_value["use_terrain_shader"] = class_db.get_already_registered_ui_functions<bool>()->parse_function(persistent_data.use_terrain_shader, display_gui, "Use Terrain Shader");
    return_value["vertex_resolution"] = class_db.get_already_registered_ui_functions<float>()->parse_function(persistent_data.vertex_resolution, display_gui, "Vertex Resolution");
    return_value["number_of_patches"] = class_db.get_already_registered_ui_functions<int>()->parse_function(persistent_data.number_of_patches, display_gui, "Number of Patches");

    if (!display_gui || ImGui::TreeNode("Texture Paths")) {
        std::unordered_map<glm::vec4, std::vector<Image>, utils::Vec4Hash> images_new_height_ranges;

        for (std::pair<const glm::vec4, std::vector<Image>> &texture_path_height : persistent_data.texture_paths_height) {
            std::unordered_set<Image> images_to_delete;

            for (Image &image : texture_path_height.second) {
                glm::vec4 height_range = texture_path_height.first;
                std::optional<glm::vec4> slope_range;

                Image new_image = image;

                if (persistent_data.texture_paths_slope.contains(image)) {
                    slope_range = persistent_data.texture_paths_slope.at(image);
                }

                std::optional<glm::vec4> new_slope_range = slope_range;

                nlohmann::json height_range_json = class_db.get_already_registered_ui_functions<glm::vec4>()->parse_function(height_range, display_gui, "Height Range");
                nlohmann::json slope_range_json = class_db.get_already_registered_ui_functions<std::optional<glm::vec4>>()->parse_function(new_slope_range, display_gui, "Slope Range");

                ImGui::InputText("Texture Path", &new_image.image_path);
                ImGui::InputText("Texture Type", &new_image.image_type);

                if (new_image != image) {
                    if (persistent_data.texture_paths_slope.contains(image)) {
                        persistent_data.texture_paths_slope.erase(image);

                        if (new_slope_range.has_value()) {
                            persistent_data.texture_paths_slope.insert({ new_image, new_slope_range.value() });
                        }
                    }

                    image = new_image;
                }
                else if (new_slope_range != slope_range) {
                    new_slope_range = slope_range;

                    if (persistent_data.texture_paths_slope.contains(image)) {
                        if (new_slope_range.has_value()) {
                            persistent_data.texture_paths_slope.at(image) = new_slope_range.value();
                        }
                        else {
                            persistent_data.texture_paths_slope.erase(image);
                        }
                    }
                    else if (new_slope_range.has_value()) {
                        persistent_data.texture_paths_slope.insert({ image, new_slope_range.value() });
                    }
                }

                if (height_range != texture_path_height.first) {
                    images_new_height_ranges[height_range].push_back(image);

                    images_to_delete.insert(image);
                }
            }

            texture_path_height.second.erase(std::remove_if(texture_path_height.second.begin(), texture_path_height.second.end(), [&images_to_delete](Image image) {
                return images_to_delete.contains(image);
            }), texture_path_height.second.end());
        }

        for (const std::pair<const glm::vec4, std::vector<Image>> &image_height_pair : images_new_height_ranges) {
            std::vector<Image> &persistent_data_images = persistent_data.texture_paths_height[image_height_pair.first];

            persistent_data_images.insert(persistent_data_images.end(), image_height_pair.second.begin(), image_height_pair.second.end());
        }

        struct HeightSlopeHasher {
            size_t operator()(const std::pair<glm::vec4, std::optional<glm::vec4>> &path) const noexcept {
                return glm::length(path.first + (glm::vec4{ 1000 } * (path.second.has_value() ? path.second.value() : glm::vec4{0})));
           }
        };

        std::unordered_map<std::pair<glm::vec4, std::optional<glm::vec4>>, std::vector<Image>, HeightSlopeHasher> texture_paths_json_map;

        for (std::pair<const glm::vec4, std::vector<Image>> &texture_path_height : persistent_data.texture_paths_height) {
            for (const Image &image : texture_path_height.second) {
                std::pair<glm::vec4, std::optional<glm::vec4>> height_slope_pair{ texture_path_height.first, {} };

                if (persistent_data.texture_paths_slope.contains(image)) {
                    height_slope_pair.second = persistent_data.texture_paths_slope.at(image);
                }

                texture_paths_json_map[height_slope_pair].push_back(image);
            }
        }

        for (const std::pair<const std::pair<glm::vec4, std::optional<glm::vec4>>, std::vector<Image>> &texture_list_json : texture_paths_json_map) {
            nlohmann::json height_slope_range_json;

            height_slope_range_json["height_range"] = utils::get_glm_json<glm::vec4>(texture_list_json.first.first);

            if (texture_list_json.first.second.has_value()) {
                height_slope_range_json["slope_range"] = utils::get_glm_json<glm::vec4>(*texture_list_json.first.second);
            }

            for (const Image &image : texture_list_json.second) {
                nlohmann::json image_json;

                image_json["path"] = image.image_path;
                image_json["type"] = image.image_type.substr(8); // stripping out starting texture_

                height_slope_range_json.push_back(image_json);
            }

            return_value["height_slope_ranges"].push_back(height_slope_range_json);
        }

        if (display_gui) {
            ImGui::TreePop();
        }
    }

    //PropInfo prop_info;

    return return_value;
}

nlohmann::json aristaeus::gfx::vulkan::ChunkedTerrain::water_info_ui_parse(ProceduralTerrain::WaterInfo &persistent_data, bool display_gui, const std::optional<std::string> &label_name) {
    assert(core::ClassDB::instance != nullptr);
    core::ClassDB &class_db = *core::ClassDB::instance;
    
    nlohmann::json return_value;

    // TODO(Bobby): find someway to make this nicer with ClassDB, for instance putting this into a class that is not a node
    return_value["draw_water"] = class_db.get_already_registered_ui_functions<bool>()->parse_function(persistent_data.draw_water, display_gui, "Draw Water");

    return_value["number_of_patches"] = class_db.get_already_registered_ui_functions<int>()->parse_function(persistent_data.number_of_patches, display_gui, "Number of Patches");
    return_value["number_of_textures"] = class_db.get_already_registered_ui_functions<int>()->parse_function(persistent_data.number_of_textures, display_gui, "Number of Textures");
    return_value["reflectivity"] = class_db.get_already_registered_ui_functions<float>()->parse_function(persistent_data.reflectivity, display_gui, "Reflectivity");
    return_value["refractive_index"] = class_db.get_already_registered_ui_functions<float>()->parse_function(persistent_data.refractive_index, display_gui, "Refractive Index");
    return_value["refractive_strength"] = class_db.get_already_registered_ui_functions<float>()->parse_function(persistent_data.refractive_strength, display_gui, "Refractive Strength");
    return_value["shininess"] = class_db.get_already_registered_ui_functions<float>()->parse_function(persistent_data.shininess, display_gui, "Shininess");
    return_value["tessellation_amount"] = class_db.get_already_registered_ui_functions<int>()->parse_function(persistent_data.tessellation_amount, display_gui, "Tessellation Amount");
    return_value["texture_path"] = class_db.get_already_registered_ui_functions<std::string>()->parse_function(persistent_data.texture_path, display_gui, "Texture Path");
    return_value["transmission"] = class_db.get_already_registered_ui_functions<float>()->parse_function(persistent_data.transmission, display_gui, "Transmission");
    return_value["y_spawn_threshold"] = class_db.get_already_registered_ui_functions<float>()->parse_function(persistent_data.y_spawn_threshold, display_gui, "Y Spawn Threshold");
    return_value["wave_properties"]["count"] = class_db.get_already_registered_ui_functions<int>()->parse_function(persistent_data.wave_properties.count, display_gui, "Number of Waves");
    return_value["wave_properties"]["median_amplitude"] = class_db.get_already_registered_ui_functions<float>()->parse_function(persistent_data.wave_properties.median_amplitude, display_gui, "Median Amplitude of Waves");
    return_value["wave_properties"]["median_sharpness"] = class_db.get_already_registered_ui_functions<float>()->parse_function(persistent_data.wave_properties.median_sharpness, display_gui, "Median Sharpness of Waves");
    return_value["wave_properties"]["median_wavelength"] = class_db.get_already_registered_ui_functions<float>()->parse_function(persistent_data.wave_properties.median_sharpness, display_gui, "Median Wavelength of Waves");
    return_value["wave_properties"]["direction"] = class_db.get_already_registered_ui_functions<glm::vec2>()->parse_function(persistent_data.wave_properties.direction, display_gui, "Direction of Waves");

    return return_value;
}
