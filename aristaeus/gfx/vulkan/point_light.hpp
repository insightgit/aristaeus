//
// Created by bobby on 8/28/21.
//

#ifndef ARISTAEUS_POINT_LIGHT_HPP
#define ARISTAEUS_POINT_LIGHT_HPP

#include <memory>

#include "assimp_model.hpp"
#include "lighting_node.hpp"

#include "../../core/class_db.hpp"
#include "../../utils.hpp"

namespace aristaeus::gfx::vulkan {
    class PointLight : public LightingNode {
    public:
        ARISTAEUS_CLASS_HEADER("point_light", PointLight, true);
        struct LightColors {
            glm::vec3 diffuse;

            glm::vec3 light_box_color;
        };

        struct LightAttuention {
            float constant;
            float linear;
            float quadratic;
        };

        static LightAttuention DEFAULT_LIGHT_ATTENUATION;
        static LightColors DEFAULT_LIGHT_COLORS;

        static constexpr std::string_view NODE_TYPE = "point_light";
        static constexpr int UBO_POINT_LIGHT_TYPE_ID = 1;

        ~PointLight() = default;

        AssimpModel &get_loaded_model() {
            return *m_emitter_model;
        }

        void predraw_load(const vk::CommandBuffer &command_buffer, const glm::vec3 &camera_position)override {}

        void draw() override {}

        void on_parsing_done();
    private:
        void update_point_light_ubo();

        std::shared_ptr<AssimpModel> m_emitter_model;

        LightColors m_point_light_colors;
        LightAttuention m_point_light_attenuation;
    };
}


#endif //ARISTAEUS_POINT_LIGHT_HPP
