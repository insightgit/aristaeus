//
// Created by bobby on 1/3/22.
//

#include "sky_box.hpp"
#include "rasterization_gi/reflective_shadow_map.hpp"

#include "rasterization_lighting_environment.hpp"

#include <memory>

const std::vector<vk::DescriptorSetLayoutBinding> aristaeus::gfx::vulkan::SkyBox::DESCRIPTOR_SET_LAYOUT_BINDINGS = {
        {0, vk::DescriptorType::eUniformBuffer, 1, vk::ShaderStageFlagBits::eVertex},
        {1, vk::DescriptorType::eCombinedImageSampler, 1, vk::ShaderStageFlagBits::eFragment}
};

aristaeus::gfx::vulkan::BufferRaii aristaeus::gfx::vulkan::SkyBox::VERTEX_BUFFER = {};
uint32_t aristaeus::gfx::vulkan::SkyBox::ALIVE_SKY_BOXES = 0;

std::weak_ptr<aristaeus::gfx::vulkan::SkyBox> aristaeus::gfx::vulkan::SkyBox::active_sky_box{};

ARISTAEUS_CLASS_IMPL(aristaeus::gfx::vulkan::SkyBox, typeid(aristaeus::gfx::SceneGraphNode));

void aristaeus::gfx::vulkan::SkyBox::classdb_register() {
    core::ClassDB &class_db = utils::get_singleton_safe<core::ClassDB>();

    class_db.register_property<SkyBox, std::vector<std::string>>("cubemap_texture_faces",
    [](SkyBox &sky_box) {
        return std::vector<std::string>{sky_box.m_cubemap_texture_faces.begin(),
                                        sky_box.m_cubemap_texture_faces.end()};
    },
    [](SkyBox &sky_box, std::vector<std::string> texture_faces) {
        if(!sky_box.m_child_creates_cubemap_texture && texture_faces.size() != 6) {
            throw std::runtime_error("There must be six texture files for sky box " + sky_box.get_node_name());
        } else if(texture_faces.size() != 6) {
            return;
        }

        bool texture_changed = false;

        for(size_t i = 0; sky_box.m_cubemap_texture_faces.size() > i; ++i) {
            if(!texture_changed && sky_box.m_cubemap_texture_faces[i] != texture_faces[i]) {
                texture_changed = true;
            }

            sky_box.m_cubemap_texture_faces[i] = texture_faces[i];
        }

        if(texture_changed && !sky_box.m_child_creates_cubemap_texture) {
            sky_box.m_cubemap_texture = std::make_unique<CubeMapTexture>(sky_box.m_cubemap_texture_faces);
        }
    }, {});

    class_db.register_property<SkyBox, std::string>("shader_directory", [](SkyBox &sky_box) {
        return sky_box.m_shader_directory;
    }, [](SkyBox &sky_box, const std::string &new_shader_dir) {
        if(sky_box.m_shader_directory != new_shader_dir) {
            sky_box.m_shader_directory = new_shader_dir;

            sky_box.m_cubemap_pipeline = std::make_unique<GraphicsPipeline>(utils::get_singleton_safe<CoreRenderer>(),
                               sky_box.get_pipeline_shader_infos(sky_box.m_shader_directory),
                               sky_box.get_pipeline_info(*utils::get_singleton_safe<CoreRenderer>().get_default_rasterization_render_pass()));
        }
    }, []() {
        return "environments/vulkan_environment_shaders/engine";
    });

    class_db.register_event_callback_on_construction<SkyBox, void*>(std::string{SceneGraphNode::PARSING_DONE_EVENT},
                                                                    "sky_box_parsing_done",
                                                                    [](SkyBox &sky_box, void*) {
        sky_box.m_camera_ubos = sky_box.create_camera_ubos(utils::get_singleton_safe<CoreRenderer>());
        sky_box.m_cubemap_descriptor_set_layout = std::make_unique<vk::raii::DescriptorSetLayout>(
                utils::get_singleton_safe<CoreRenderer>().get_logical_device_ref(),
                vk::DescriptorSetLayoutCreateInfo{{}, DESCRIPTOR_SET_LAYOUT_BINDINGS});

        auto &core_renderer = utils::get_singleton_safe<CoreRenderer>();

        sky_box.init_sky_box(core_renderer);

        sky_box.add_node_group(std::string{ RasterizationLightingEnvironment::FORWARD_RENDER_NODE_GROUP });
        sky_box.add_node_group(std::string{ gi::ReflectiveShadowMap::REFLECTIVE_SHADOW_MAP_RENDER_EXCLUSION_GROUP_NAME });
    });

    class_db.register_event_callback_on_construction<SkyBox, void*>(std::string{SceneGraphNode::ENTER_TREE_EVENT},
                                                                    "sky_box_enter_tree", [](SkyBox &sky_box, void*) {
        if(active_sky_box.lock() == nullptr) {
            active_sky_box = std::static_pointer_cast<SkyBox>(sky_box.get_weak_ptr_to_this().lock());
        }
    });

    class_db.register_event_callback_on_construction<SkyBox, void*>(std::string{SceneGraphNode::EXIT_TREE_EVENT},
                                                                    "sky_box_exit_tree", [](SkyBox &sky_box, void*) {
        if(active_sky_box.lock() == sky_box.get_weak_ptr_to_this().lock()) {
            active_sky_box.reset();
        }
    });
}

void aristaeus::gfx::vulkan::SkyBox::init_sky_box(CoreRenderer &core_renderer) {
    if (ALIVE_SKY_BOXES <= 0) {
        size_t buffer_size = SKYBOX_VERTICES.size() * sizeof(float);

        BufferRaii staging_buffer = core_renderer.get_memory_manager().create_buffer(core_renderer,
            buffer_size, vk::BufferUsageFlagBits::eTransferSrc, vk::SharingMode::eExclusive,
            VMA_ALLOCATION_CREATE_HOST_ACCESS_SEQUENTIAL_WRITE_BIT, VMA_MEMORY_USAGE_AUTO,
            "sky-box-staging-vertex-buffer");
        VERTEX_BUFFER = core_renderer.get_memory_manager().create_buffer(core_renderer, buffer_size,
            vk::BufferUsageFlagBits::eTransferDst |
            vk::BufferUsageFlagBits::eVertexBuffer, vk::SharingMode::eExclusive,
            VMA_ALLOCATION_CREATE_HOST_ACCESS_SEQUENTIAL_WRITE_BIT,
            VMA_MEMORY_USAGE_AUTO_PREFER_DEVICE,
            "sky-box-vertex-buffer");

        void* buffer_data;

        vmaMapMemory(staging_buffer->allocator, staging_buffer->allocation, &buffer_data);

        std::memcpy(buffer_data, SKYBOX_VERTICES.data(), buffer_size);

        vmaUnmapMemory(staging_buffer->allocator, staging_buffer->allocation);

        core_renderer.copy_buffer(staging_buffer, VERTEX_BUFFER);

        core_renderer.transfer_staging_buffer_for_deletion(std::move(staging_buffer));

        ALIVE_SKY_BOXES = 1;
    }
    else {
        ++ALIVE_SKY_BOXES;
    }
}

aristaeus::gfx::vulkan::GraphicsPipeline::PipelineInfo
aristaeus::gfx::vulkan::SkyBox::get_pipeline_info(vk::raii::RenderPass &render_pass) {
    return {
            .vertex_input_stage = {{}, SKY_BOX_BINDING_DESCRIPTION,
                                   SKY_BOX_ATTRIBUTE_DESCRIPTION},
            .input_assembly_stage = {{}, vk::PrimitiveTopology::eTriangleList, VK_FALSE},
            .viewport_stage = {{}, 1, nullptr, 1, nullptr},
            .rasterization_stage = {{}, VK_FALSE, VK_FALSE, vk::PolygonMode::eFill, vk::CullModeFlagBits::eNone,
                                    vk::FrontFace::eClockwise, {}, {}, {}, {}, 1.0f},
            .multisample_stage = {{}, vk::SampleCountFlagBits::e1, VK_FALSE, 1.0f, nullptr, VK_FALSE, VK_FALSE},
            // depth writing is delibritley disabled for the sky box
            .depth_stencil_stage = {{}, VK_TRUE, VK_FALSE, vk::CompareOp::eLessOrEqual, VK_FALSE, VK_FALSE, {}, {}, 0.0f, 1.0f},
            .blending_stage = {{}, VK_FALSE, vk::LogicOp::eCopy, FORWARD_COLOR_BLEND_ATTACHMENT, {0, 0, 0, 0}},
            .push_content_ranges = {},

            .descriptor_bindings = {DESCRIPTOR_SET_LAYOUT_BINDINGS},

            .sub_pass = utils::get_singleton_safe<CoreRenderer>().get_default_rasterization_subpass(),
            .pipeline_cache_path = "sky_box_pipeline_cache",
            .render_pass = render_pass
    };
}

std::vector<aristaeus::gfx::vulkan::GraphicsPipeline::ShaderInfo>
aristaeus::gfx::vulkan::SkyBox::get_pipeline_shader_infos(const std::string &shader_directory) {
    return {
            {
                    .code = utils::read_file(shader_directory + "/sky_box.vert.spv"),
                    .name = "sky_box.vert",
                    .stage = vk::ShaderStageFlagBits::eVertex
            },
            {
                    .code = utils::read_file(shader_directory + "/sky_box.frag.spv"),
                    .name = "sky_box.frag",
                    .stage = vk::ShaderStageFlagBits::eFragment
            }
    };
}

void aristaeus::gfx::vulkan::SkyBox::draw() {
    // TODO
    CoreRenderer &core_renderer = utils::get_singleton_safe<CoreRenderer>();
    RenderPhase *render_phase = core_renderer.get_current_render_phase();

    assert(render_phase != nullptr);

    if(active_sky_box.lock() != get_weak_ptr_to_this().lock() ||
       render_phase->is_render_pass_depth_only(render_phase->get_current_render_pass())) {
        // sky box is not the active sky box!
        // or we are in a shadow mapping render phase!
        return;
    }

    // step 1: update descriptor sets with most recent data
    void *buffer_data;
    uint32_t current_frame = core_renderer.get_current_frame();
    RenderContextID render_context_id = core_renderer.get_current_render_context_id();

    if (m_cubemap_texture == nullptr && !m_shared_cubemap_texture_image_info.has_value()) {
        return;
    }

    vk::DescriptorImageInfo sampler_info = m_cubemap_texture == nullptr ? *m_shared_cubemap_texture_image_info :
                                           m_cubemap_texture->get_descriptor_image_info();
    vk::DescriptorBufferInfo ubo_info {m_camera_ubos[current_frame]->buffer, 0, 2 * sizeof(glm::mat4)};

    // get rid of the translation part of the view matrix
    PerspectiveCameraEnvironment &perspective_camera_environment = core_renderer.get_perspective_camera_environment();
    glm::mat4 projection_matrix = perspective_camera_environment.get_projection_matrix();
    glm::mat4 view_matrix = glm::mat4(glm::mat3(perspective_camera_environment.get_view_matrix()));

    vmaMapMemory(m_camera_ubos[current_frame]->allocator, m_camera_ubos[current_frame]->allocation, &buffer_data);

    std::memcpy(buffer_data, &projection_matrix, sizeof(glm::mat4));
    std::memcpy(reinterpret_cast<void*>(reinterpret_cast<uintptr_t>(buffer_data) + sizeof(glm::mat4)), &view_matrix,
                sizeof(glm::mat4));

    vmaUnmapMemory(m_camera_ubos[current_frame]->allocator, m_camera_ubos[current_frame]->allocation);

    assert(m_cubemap_descriptor_sets.contains(render_context_id) ==
           m_cubemap_descriptor_pools.contains(render_context_id));

    if(!m_cubemap_descriptor_sets.contains(render_context_id)) {
        m_cubemap_descriptor_pools.insert({ render_context_id, create_descriptor_pool(core_renderer)});
        m_cubemap_descriptor_sets.insert({ render_context_id,
                                          create_descriptor_sets(core_renderer.get_logical_device_ref(),
                                                                 render_context_id)});
    }

    render_phase->add_draw_next_frame([this, ubo_info, sampler_info] {
        auto &core_renderer = utils::get_singleton_safe<CoreRenderer>();
        RenderContextID render_context_id = core_renderer.get_current_render_context_id();
        uint32_t current_frame = core_renderer.get_current_frame();

        vk::CommandBuffer command_buffer = 
            core_renderer.get_graphics_command_buffer_manager().get_command_buffer_for_thread(render_context_id, {}, {}, 2);

        vk::WriteDescriptorSet ubo_descriptor_set_write {*m_cubemap_descriptor_sets.at(render_context_id)[current_frame], 0,
                                                     0, vk::DescriptorType::eUniformBuffer, {}, ubo_info};
        vk::WriteDescriptorSet samplers_descriptor_set_write {
            *m_cubemap_descriptor_sets.at(render_context_id)[current_frame], 1, 0,
            vk::DescriptorType::eCombinedImageSampler, sampler_info};
        std::array<vk::WriteDescriptorSet, 2> descriptor_set_writes {ubo_descriptor_set_write,
                                                                     samplers_descriptor_set_write};

        core_renderer.get_logical_device_ref().updateDescriptorSets(descriptor_set_writes, {});

        // step 2: bind skybox pipeline and descriptor set
        m_cubemap_pipeline->bind_to_command_buffer(command_buffer, core_renderer);
        command_buffer.bindDescriptorSets(vk::PipelineBindPoint::eGraphics, *m_cubemap_pipeline->get_pipeline_layout(), 0,
                                          *m_cubemap_descriptor_sets.at(render_context_id)[current_frame], {});

        // step 3: render skybox cube vertices

        command_buffer.bindVertexBuffers(0, VERTEX_BUFFER->buffer, static_cast<vk::DeviceSize>(0));
        command_buffer.draw(SKYBOX_VERTICES.size() / 3, 1, 0, 0);
    });

    
}

void aristaeus::gfx::vulkan::SkyBox::recreate_graphics_pipeline(CoreRenderer &core_renderer) {
    m_cubemap_pipeline->recreate_graphics_pipeline(get_pipeline_info(*core_renderer.get_default_rasterization_render_pass()));
}

std::array<vk::raii::DescriptorSet, utils::vulkan::MAX_FRAMES_IN_FLIGHT> aristaeus::gfx::vulkan::SkyBox::create_descriptor_sets(vk::raii::Device &logical_device, 
                                                                                                                                RenderContextID render_context_id) {
    std::array<vk::DescriptorSetLayout, utils::vulkan::MAX_FRAMES_IN_FLIGHT> descriptor_set_layouts
    { **m_cubemap_descriptor_set_layout, **m_cubemap_descriptor_set_layout };
    vk::DescriptorSetAllocateInfo set_allocate_info{ *m_cubemap_descriptor_pools.at(render_context_id),
                                                     descriptor_set_layouts };
    std::vector<vk::raii::DescriptorSet> new_sets_vector = logical_device.allocateDescriptorSets(set_allocate_info);

    return { std::move(new_sets_vector[0]), std::move(new_sets_vector[1]) };
}
