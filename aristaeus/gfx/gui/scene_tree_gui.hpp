//
// Created by bobby on 3/9/22.
//

#ifndef ARISTAEUS_PROCEDURAL_TERRAIN_GUI_HPP
#define ARISTAEUS_PROCEDURAL_TERRAIN_GUI_HPP

#include <chrono>
#include <queue>
#include <string_view>
#include <utility>

#include "../../core/class_db.hpp"

#include "../scene_graph_node.hpp"
#include "../vulkan/core_renderer.hpp"

#include "../vulkan/window.hpp"

namespace aristaeus::gfx::gui {
    class SceneTreeGui : public gfx::SceneGraphNode {
    ARISTAEUS_CLASS_HEADER("scene_tree_gui", SceneTreeGui, true);
    public:
        bool has_updates() const {
            return m_has_updates;
        }

        void predraw_load(const vk::CommandBuffer &command_buffer, const glm::vec3 &camera_position) override {
            for (ZOrderUpdate &update : m_z_order_updates) {
                std::shared_ptr<gfx::SceneGraphNode> scene_graph_node = update.scene_graph_node.lock();

                if (scene_graph_node != nullptr) {
                    scene_graph_node->set_z_order(update.z_order);
                }
            }

            m_z_order_updates.clear();
        };

        void draw()override;
    private:
        struct ZOrderUpdate {
            std::weak_ptr<gfx::SceneGraphNode> scene_graph_node;
            int z_order;
        };

        struct AddingNodeInfo {
            std::string_view node_type_info_name;
            core::StdTypeInfoRef node_type_info;
        };

        void refresh_node_list();

        void on_enter_tree();
        void on_exit_tree();

        void on_parsing_done();
        void on_core_renderer_frame_done();

        void refresh_font() {
            if (!m_font_path.empty()) {
                ImGuiIO &io = ImGui::GetIO();

                m_font = io.Fonts->AddFontFromFileTTF(m_font_path.c_str(), m_font_size);
            } else {
                m_font = nullptr;
            }
        }

        AddingNodeInfo get_adding_node_info(const std::string_view &node_type_info_name);

        void draw_add_node_panel(gfx::SceneGraphNode &scene_graph_node);

        void draw_vec3_panel(const std::string &label, glm::vec3 &vec3_to_modify);
        void draw_vec4_panel(const std::string &label, glm::vec4 &vec4_to_modify);

        void draw_perf_panel();

        void draw_scene_node_panel(const std::weak_ptr<gfx::SceneGraphNode> &scene_graph_node, vulkan::CoreRenderer &core_renderer);

        void display_ui_functions(const AddingNodeInfo &adding_node_info);

        bool m_has_updates = false;

        std::weak_ptr<gfx::SceneGraphNode> m_scene_tree;

        ImFont *m_font;
        uint16_t m_font_size;
        std::string m_font_path;

        std::vector<ZOrderUpdate> m_z_order_updates;
        std::weak_ptr<gfx::SceneGraphNode> m_node_being_added;

        bool m_adding_child = false;
        std::optional<AddingNodeInfo> m_ui_selected_class;
        nlohmann::json m_adding_node_working_json;

        int m_currently_selected_class = 0;

        std::vector<std::string_view> m_ui_initializable_classes;
        std::vector<const char*> m_ui_initializable_classes_c_ptr_wrapper;

        gfx::SceneGraphNode *m_current_guizmo_transform = nullptr;
        ImGuizmo::OPERATION m_current_guizmo_op;

        std::chrono::system_clock::time_point m_fps_timer = std::chrono::system_clock::now();
        std::chrono::system_clock::time_point m_fps_avg = m_fps_timer;

        float m_running_fps_sum = 0;
        int m_fps_counts = 0;
        int m_frame_counter = 0;

        float m_past_fps = 0;
        float m_past_10_sec_avg_fps = 0;
    };
}

#endif //ARISTAEUS_PROCEDURAL_TERRAIN_GUI_HPP
