//
// Created by bobby on 03/03/23.
//

#ifndef ARISTAEUS_MEMORY_MANAGER_HPP
#define ARISTAEUS_MEMORY_MANAGER_HPP

#include <mutex>

#include "image_buffer_raii.hpp"
#include "window.hpp"

#include "../../utils.hpp"

namespace aristaeus::gfx::vulkan {
    class MemoryManager {
    public:
        #ifndef NDEBUG
        static std::vector<VkBuffer> created_buffers;
        static std::mutex created_buffers_lock;
        #endif

        MemoryManager(Window &window, vk::raii::Device &logical_device);

        #ifndef NDEBUG
        void register_buffer_free(const VmaBuffer &vma_buffer) {
            std::lock_guard<std::mutex> lock_guard{ m_allocated_buffers_lock };

            m_alive_buffers_by_size[vma_buffer.buffer_size].erase(vma_buffer.buffer);
        }

        void register_image_free(const VmaImage &vma_image) {
            std::lock_guard<std::mutex> lock_guard{ m_allocated_images_lock };

            m_alive_images_by_size[glm::vec2{vma_image.image_width, vma_image.image_height}].erase(vma_image.image);
        }

        std::string get_buffer_purpose(VkBuffer buffer_handle);
        std::string get_image_purpose(VkImage image_handle);
        #endif

        BufferRaii create_buffer(CoreRenderer &core_renderer, vk::DeviceSize size,
                                 vk::BufferUsageFlags usage, vk::SharingMode sharing_mode,
                                 VmaAllocationCreateFlagBits allocation_flags,
                                 VmaMemoryUsage allocation_usage_flags, const std::string &allocation_category);

        ImageRaii create_image(CoreRenderer &core_renderer, uint32_t width, uint32_t height, vk::Format format,
                               vk::ImageTiling tiling, vk::ImageUsageFlags usage,
                               VmaAllocationCreateFlagBits allocation_flags, VmaMemoryUsage allocation_usage_flags, const std::string &allocation_category,
                               uint32_t array_layers = 1, vk::ImageCreateFlags flags = {}, uint32_t mip_levels = 1);
        SharedImageRaii create_shared_image(uint32_t width, uint32_t height, vk::Format format, vk::ImageTiling tiling, 
                                            vk::ImageUsageFlags usage, VmaAllocationCreateFlagBits allocation_flags, 
                                            VmaMemoryUsage allocation_usage_flags, const std::string &allocation_category,
                                            uint32_t array_layers = 1, vk::ImageCreateFlags flags = {}, uint32_t mip_levels = 1);
    private:
        Window::VmaAllocatorRaii m_allocator;

        VmaImage *create_vma_image(uint32_t width, uint32_t height, vk::Format format, vk::ImageTiling tiling, vk::ImageUsageFlags usage,
                                   VmaAllocationCreateFlagBits allocation_flags, VmaMemoryUsage allocation_usage_flags, 
                                   const std::string &allocation_category, uint32_t array_layers = 1, vk::ImageCreateFlags flags = {}, 
                                   uint32_t mip_levels = 1);

        #ifndef NDEBUG
        std::map<size_t, std::unordered_map<VkBuffer, std::string>> m_alive_buffers_by_size;
        std::map<glm::vec2, std::unordered_map<VkImage, std::string>, utils::Vec2Hash> m_alive_images_by_size;


        std::unordered_map<VkBuffer, std::string> m_allocated_buffers;
        std::unordered_map<VkImage, std::string> m_allocated_images;

        std::mutex m_allocated_buffers_lock;
        std::mutex m_allocated_images_lock;
        #endif
    };
}

#endif //ARISTAEUS_MEMORY_MANAGER_HPP
