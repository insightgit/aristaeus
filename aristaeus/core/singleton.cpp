//
// Created by bobby on 27/12/23.
//

#include "singleton.hpp"

template<typename SingletonType>
SingletonType *aristaeus::core::Singleton<SingletonType>::instance = nullptr;