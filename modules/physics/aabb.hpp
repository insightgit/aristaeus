//
// Created by bobby on 12/15/21.
//

#ifndef ARISTAEUS_AABB_HPP
#define ARISTAEUS_AABB_HPP

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/glm.hpp>
#include <glm/gtx/string_cast.hpp>

namespace aristaeus::physics {
    class AABB {
    public:
        enum class AABBFace {
            TOP,
            BOTTOM,
            LEFT,
            RIGHT,
            INSIDE,
            NONE
        };

        AABB(glm::vec3 center, glm::vec3 extents);

        AABB(glm::vec3 extents) : AABB(glm::vec3(0.0f), extents) {}

        AABB() : AABB(glm::vec3(0.0f)) {}

        [[nodiscard]] glm::vec3 get_center() const {
            return m_center;
        }

        void set_center(const glm::vec3 &center) {
            m_center = center;

            recalculate_min_max_extents();
        }

        [[nodiscard]] glm::vec3 get_extents() const {
            return m_extents;
        }

        void set_extents(const glm::vec3 &extents) {
            m_extents = extents;

            recalculate_min_max_extents();
        }

        [[nodiscard]] bool contains_point(const glm::vec3 &point) const {
            return (point.x >= m_min_extents.x && point.x <= m_max_extents.x) &&
                   (point.y >= m_min_extents.y && point.y <= m_max_extents.y) &&
                   (point.z >= m_min_extents.z && point.z <= m_max_extents.z);
        }

        [[nodiscard]] bool contains_AABB(const AABB &other) const {
            return (m_min_extents.x <= other.m_max_extents.x && m_max_extents.x >= other.m_min_extents.x) &&
                   (m_min_extents.y <= other.m_max_extents.y && m_max_extents.y >= other.m_min_extents.y) &&
                   (m_min_extents.z <= other.m_max_extents.z && m_max_extents.z >= other.m_min_extents.z);
        }

        [[nodiscard]] AABBFace get_first_colliding_face(const AABB &other) const;
    private:
        void recalculate_min_max_extents() {
            m_max_extents = m_center + m_extents;
            m_min_extents = m_center - m_extents;
        }

        glm::vec3 m_center;
        glm::vec3 m_extents;

        glm::vec3 m_max_extents;
        glm::vec3 m_min_extents;


        // aabb drawing
        const static unsigned int AABB_INDICES[];

        static unsigned int VAO;
        static unsigned int EBO;
        static bool EBO_VAO_inited;

        unsigned int m_VBO;
    };
}


#endif //ARISTAEUS_AABB_HPP
