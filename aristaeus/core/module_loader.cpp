//
// Created by bobby on 04/01/24.
//

#include "module_loader.hpp"
#include "../engine_module_entry_point.hpp"

#include <json.hpp>

bool aristaeus::core::ModuleLoader::find_and_add_module(const std::filesystem::path &path) {
    std::filesystem::path module_json_path = path / MODULE_JSON_NAME;
    std::string relative_path =
        std::filesystem::relative(path, std::filesystem::path(m_modules_path)).string();

    bool in_blacklist = m_module_loading_policy == ModuleLoadingPolicy::BLACKLIST &&
                        m_whitelist_blacklist.contains(relative_path);

    if(std::filesystem::is_regular_file(module_json_path) && !in_blacklist) {
        nlohmann::json module_json = nlohmann::json::parse(std::ifstream{module_json_path.string()});

        //if(module_json)
        const std::string dynamic_library_path_field = "dynlib_" + std::string(platform::get_platform_string());
        const std::string module_name_field = "name";
        const std::string entry_point_field = "entry_point";
        const std::string exit_point_field = "exit_point";
        const std::string required_vulkan_device_extensions_field = "required_vulkan_device_extensions";
        std::string module_path = module_json_path.string();

        if(!module_json.contains(dynamic_library_path_field) || !module_json.contains(module_name_field) ||
           !module_json.contains(entry_point_field)) {
            std::cerr << "[WARN] Skipping module at " << module_path << " due to broken " << MODULE_JSON_NAME << "\n";
            return false;
        }

        std::string module_name = module_json.at(module_name_field);

        if(STATIC_MODULE_LIST.contains(module_name)) {
            std::cerr << "[WARN] Skipping module at " << module_path << " due to module " << module_name <<
                         " already linked in as a static module\n";
            return false;
        }

        std::unordered_set<std::string> required_vulkan_device_extensions;

        if (module_json.contains(required_vulkan_device_extensions_field)) {
            for (const std::string &device_extension : module_json.at(required_vulkan_device_extensions_field)) {
                required_vulkan_device_extensions.insert(device_extension);
            }
        }

        Module module {
            .module_name = module_name,
            .module_path = module_path,

            .dynamic_library_path = module_json.at(dynamic_library_path_field),
            .entry_point_name = module_json.at(entry_point_field),
            .exit_point_name = module_json.contains(exit_point_field) ? module_json.at(exit_point_field) :
                               std::optional<std::string>{},

            .required_vulkan_device_extensions = required_vulkan_device_extensions,

            .loaded = false,
            .was_unloaded = false
        };

        m_discovered_modules.insert({module_path, module});

        return true;
    } else {
        return false;
    }
}

void aristaeus::core::ModuleLoader::refresh_module_search_recursively(const std::filesystem::path &path) {
    if(!std::filesystem::is_directory(path)) {
        // no modules could be found because the modules path doesn't exist or
        // is not a directory
        return;
    } else if(m_module_loading_policy == ModuleLoadingPolicy::WHITELIST) {
        for(const std::string &path : m_whitelist_blacklist) {
            find_and_add_module({path});
        }
    } else if(m_module_loading_policy != ModuleLoadingPolicy::NOTHING) {
        std::filesystem::directory_iterator end_iter;

        for(std::filesystem::directory_iterator iter(path); iter != end_iter; ++iter) {
            if(!find_and_add_module(iter->path())) {
                refresh_module_search_recursively(iter->path());
            }
        }
    }
}

bool aristaeus::core::ModuleLoader::load_module(const std::string &module_path, const std::unordered_set<std::string> &vulkan_device_extensions_loaded) {
    if(!m_discovered_modules.contains(module_path)) {
        return false;
    }

    Module &module = m_discovered_modules.at(module_path);

    #ifndef NDEBUG
    std::cout << "[MODULE_LOADER] loading " << module.module_name << " module (at: " << module.module_path << ")\n";
    #endif

    if(!module.loaded) {
        nlohmann::json module_options_json =
            m_module_options.contains(module.module_name) ? m_module_options.at(module.module_name) : nlohmann::json{};

        for(const std::string &required_vulkan_device_extension : module.required_vulkan_device_extensions) {
            if (!vulkan_device_extensions_loaded.contains(required_vulkan_device_extension)) {
                #ifndef NDEBUG
                std::cout << "[MODULE_LOADER] Couldn't load " << module.module_name << " module (at: " << module.module_path << ") due to selected device not supporting the " << required_vulkan_device_extension << " vulkan extension\n";
                #endif

                return false;
            }
        }

        call_function_from_dyn_lib<const nlohmann::json&>(module.dynamic_library_path, module.entry_point_name,
                                                          module_options_json);
        module.loaded = true;
        module.was_unloaded = false;
    }

    return true;
}

bool aristaeus::core::ModuleLoader::unload_module(const std::string &module_path) {
    if(!m_discovered_modules.contains(module_path)) {
        return false;
    }

    Module &module = m_discovered_modules.at(module_path);

    if(module.loaded) {
        #ifndef NDEBUG
        std::cout << "[MODULE_LOADER] unloading " << module.module_name << " module (at: " << module.module_path <<
                     ")\n";
        #endif

        if(module.exit_point_name.has_value()) {
            nlohmann::json module_options_json =
             m_module_options.contains(module.module_name) ? m_module_options.at(module.module_name) : nlohmann::json{};

            call_function_from_dyn_lib<const nlohmann::json&, void>(module.dynamic_library_path,
                                                                    module.exit_point_name.value(),
                                                                    module_options_json);
        }

        module.loaded = false;
        module.was_unloaded = true;
    } 

    return true;
}

template<typename Argument, typename ReturnType>
std::function<ReturnType(Argument)>
aristaeus::core::ModuleLoader::get_function_ptr_from_dyn_lib(const std::string &dynamic_library_path,
                                                             const std::string &function_name) {
    DynLibHandle dyn_lib_handle;
    if(!m_loaded_dyn_libs.contains(dynamic_library_path)) {
        #ifdef _WIN32
        dyn_lib_handle = LoadLibrary(dynamic_library_path.c_str());
        #else
        dyn_lib_handle = dlopen (dynamic_library_path.c_str(), RTLD_LAZY);
        #endif

        m_loaded_dyn_libs.insert({dynamic_library_path, dyn_lib_handle});
    } else {
        dyn_lib_handle = m_loaded_dyn_libs.at(dynamic_library_path);
    }

    if (dyn_lib_handle == nullptr) {
        std::string error_string = "Error when loading " + dynamic_library_path;

        #ifndef _WIN32
        error_string += std::string(":") + dlerror();
        #endif
        throw std::runtime_error(error_string);
    }
    #ifndef _WIN32
    dlerror();
    #endif

    void *raw_function_ptr;

    #ifdef _WIN32
    raw_function_ptr = GetProcAddress(dyn_lib_handle, function_name.c_str());
    #else
    raw_function_ptr = dlsym(dyn_lib_handle, function_name.c_str());
    #endif

    if(raw_function_ptr == nullptr) {
        std::string error_string = "Couldn't find function named " + function_name + " in " +
            dynamic_library_path;

        #ifndef _WIN32
        error_string += std::string(": ") + dlerror();
        #endif

        throw std::runtime_error(error_string);
    }

    // undefined behavior if there exists a function with this function name
    // but with diff return type/function signature than expected.
    return reinterpret_cast<ReturnType(*)(Argument)>(raw_function_ptr);
}

std::vector<std::string> aristaeus::core::ModuleLoader::load_module_candidates(const std::unordered_set<std::string> &vulkan_device_extensions_loaded) {
    std::vector<std::string> loaded_modules;

    for(const std::pair<const std::string, Module> &discovered_module : m_discovered_modules) {
        assert(!discovered_module.second.loaded || m_loaded_modules.contains(discovered_module.first));
        assert(discovered_module.first == discovered_module.second.module_path);

        if(!discovered_module.second.loaded && load_module(discovered_module.first, vulkan_device_extensions_loaded)) {
            loaded_modules.push_back(discovered_module.first);
        }
    }

    return loaded_modules;
}

void aristaeus::core::ModuleLoader::unload_all_loaded_modules() {
    for(const std::pair<const std::string, std::reference_wrapper<Module>> &module_pair : m_loaded_modules) {
        assert(module_pair.second.get().loaded);
        assert(module_pair.second.get().module_path == module_pair.first);

        unload_module(module_pair.first);
    }
}
