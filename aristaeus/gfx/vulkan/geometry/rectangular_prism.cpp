#include "../core_renderer.hpp"

#include "rectangular_prism.hpp"

aristaeus::gfx::vulkan::Geometry aristaeus::gfx::vulkan::RectangularPrism::create_geometry(CoreRenderer &core_renderer, const glm::vec3 &extents, const ModelInfo &model_info) {
	static_assert(BASE_VERTICES.size() % 3 == 0 && BASE_VERTICES.size() % 6 == 0);

	std::string mesh_generation_info = "rectangular-prism-" + std::to_string(extents.x) + "-" + std::to_string(extents.y) + "-" + std::to_string(extents.z);

	std::vector<uint32_t> indices;
	std::vector<ModelVertex> vertices;

	indices.reserve(36);
	vertices.reserve(36);

	glm::vec3 tangent;

	for (size_t i = 0; BASE_VERTICES.size() > i; ++i) {
		const glm::vec3 &vertex = BASE_VERTICES[i];

		if (i < BASE_VERTICES.size() - 1) {
			if (i % 6 == 0) {
				tangent = glm::normalize(BASE_VERTICES[i + 1] - BASE_VERTICES[i + 2]);
			}
			else if (i % 6 == 3) {
				tangent = glm::normalize(-tangent);
			}
		}

		ModelVertex current_vertex{
			.position = extents * vertex,
			.normal = BASE_NORMALS[i / 6],
			.texture_coords = glm::vec3{vertex.x == -1.0f ? 0.0f : 1.0f, vertex.z == -1.0f ? 0.0f : 1.0f, 0},
			.tangent = tangent,
		};

		vertices.push_back(current_vertex);
	}

	for (size_t i = 0; BASE_VERTICES.size() > i; ++i) {
		indices.push_back(i);
	}

	return { core_renderer, vertices, indices, mesh_generation_info, model_info	 };
}