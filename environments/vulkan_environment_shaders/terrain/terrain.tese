#version 460

layout (quads, fractional_odd_spacing, ccw) in;

layout (location = 0) in vec2 height_map_texture_coords[];

const int OBJECT_TYPE_OBJECT = 0;
const int OBJECT_TYPE_GRASS = 1;
const int OBJECT_TYPE_WATER = 2;
const int OBJECT_TYPE_WATER_GERSTNER = 3;

const float GRAVITY_CONSTANT = 9.8;
const float PI = 3.141592653589793238;

struct TessellationSettings {
    int mode;
    int amount;

    int camera_max;
    int camera_min;
};

struct Wave {
    int wave_active;
    float amplitude;
    float birth_period;
    vec2 center_wave;
    float death_period;
    float sharpness;
    float wavelength;
    float t_time;
    vec2 padding;
};

layout (location = 0) out VertexOut {
    vec4 diffuse_color_vector;
    vec3 normal_vector;
    mat3 tbn_matrix;
    vec2 texture_coords_out;
    vec3 world_position;
    float vertex_height;

    vec2 height_map_texture_coords;
} tessellation_out;

layout (set = 0, binding = 0) uniform PerspectiveCameraUBO {
    mat4 projection;
    mat4 view;
} camera_ubo;

const int PERLIN_LOOKUP_TABLE[256] = {
    151,160,137,91,90,15,
    131,13,201,95,96,53,194,233,7,225,140,36,103,30,69,142,8,99,37,240,21,10,23,
    190, 6,148,247,120,234,75,0,26,197,62,94,252,219,203,117,35,11,32,57,177,33,
    88,237,149,56,87,174,20,125,136,171,168, 68,175,74,165,71,134,139,48,27,166,
    77,146,158,231,83,111,229,122,60,211,133,230,220,105,92,41,55,46,245,40,244,
    102,143,54, 65,25,63,161, 1,216,80,73,209,76,132,187,208, 89,18,169,200,196,
    135,130,116,188,159,86,164,100,109,198,173,186, 3,64,52,217,226,250,124,123,
    5,202,38,147,118,126,255,82,85,212,207,206,59,227,47,16,58,17,182,189,28,42,
    223,183,170,213,119,248,152, 2,44,154,163, 70,221,153,101,155,167, 43,172,9,
    129,22,39,253, 19,98,108,110,79,113,224,232,178,185, 112,104,218,246,97,228,
    251,34,242,193,238,210,144,12,191,179,162,241, 81,51,145,235,249,14,239,107,
    49,192,214, 31,181,199,106,157,184, 84,204,176,115,121,50,45,127, 4,150,254,
    138,236,205,93,222,114,67,29,24,72,243,141,128,195,78,66,215,61,156,180
};

const int MAX_NUM_OF_WAVES = 100;

layout (set = 2, binding = 0) uniform ModelUBO {
    float terrain_height_magnitude;

    int override_texture;
    int multi_texture_terrain_mode;
    int light_emitter;
    int object_type;

    vec3 light_box_color;

    Wave active_waves[MAX_NUM_OF_WAVES];
    Wave pending_waves[MAX_NUM_OF_WAVES];
    vec3 wave_world_position;
    int active_wave_count;

    TessellationSettings tessellation_settings;
    float vertex_resolution;
    float terrain_num_of_textures;
    int is_chunked_terrain;
    float chunked_blend_threshold;
    ivec2 chunk_position;
} model_ubo;

layout (set = 3, binding = 0) uniform MeshUBO {
    // only needed variables declared
    mat4 model;
    mat3 normal_transform;
} mesh_ubo;
// origin is 4, others are the height maps surrounding the terrain
// TODO(Bobby): Move this to the model set
layout (set = 3, binding = 6) uniform sampler2D height_map[9];

float perlin_fade(float to_be_faded) {
    return to_be_faded * to_be_faded * to_be_faded * (to_be_faded * (to_be_faded * 6 - 15) + 10);
}

// source: https://theorangeduck.com/page/avoiding-shader-conditionals
float when_greater_than(float x, float y) {
    return max(sign(x - y), 0.0);
}

float when_less_than(float x, float y) {
    return max(sign(y - x), 0.0);
}

float when_greater_equal_than(float x, float y) {
    return 1.0 - when_less_than(x, y);
}

float when_less_equal_than(float x, float y) {
    return 1.0 - when_greater_than(x, y);
}

float when_equal_to(float x, float y) {
    return 1.0 - abs(sign(x - y));
}

float when_not_equal_to(float x, float y) {
    return 1.0 - when_equal_to(x, y);
}

float or(float x, float y) {
    return min(x + y, 1.0);
}

float perlin_gradient_dot(int hash, vec2 direction_vector) {
    int h = hash & 15;                      // CONVERT LO 4 BITS OF HASH CODE
    float u = (direction_vector.x * when_less_than(h, 8)) + (direction_vector.y * when_greater_equal_than(h, 8));
    //float u = h < 8 ? direction_vector.x : direction_vector.y;                 // INTO 12 GRADIENT DIRECTIONS.
    //float v = h < 4 ? direction_vector.y : h == 12||h==14 ? direction_vector.x : 0.0;

    float v = (when_less_than(h, 4) * direction_vector.y) + (or(when_equal_to(h, 12), when_equal_to(h, 14)) * direction_vector.x);

    float return_value = (when_equal_to(h&1, 0) * u) + (when_not_equal_to(h&1, 0) * -u);

    return_value += (when_equal_to(h&2, 0) * v) + (when_not_equal_to(h&2, 0) * -v);

    return return_value;

    //return ((h&1) == 0 ? u : -u) + ((h&2) == 0 ? v : -v);
}

int perlin_hash(ivec2 location_coords) {
    // z is a constant here (0)
    while(location_coords.x < 0) {
        location_coords.x += 255;
    }

    while(location_coords.y < 0) {
        location_coords.y += 255;
    }

    location_coords.x = location_coords.x % 255;
    location_coords.y = location_coords.y % 255;

    int x_y_index = (PERLIN_LOOKUP_TABLE[location_coords.x] + location_coords.y) % 255;

    return PERLIN_LOOKUP_TABLE[PERLIN_LOOKUP_TABLE[x_y_index] + 0];
}

float perlin(vec2 coords) {
    vec2 direction_pos = vec2(coords.x - floor(coords.x), coords.y - floor(coords.y));
    ivec2 location_pos = ivec2(int(coords.x) & 255, int(coords.y) & 255);
    vec2 fade_pos;

    fade_pos.x = perlin_fade(direction_pos.x);
    fade_pos.y = perlin_fade(direction_pos.y);

    float x1_x2_lerp = mix(perlin_gradient_dot(perlin_hash(location_pos), direction_pos),
    perlin_gradient_dot(perlin_hash(location_pos + ivec2(1, 0)),
    direction_pos - vec2(1, 0)), fade_pos.x);
    float x3_x4_lerp = mix(perlin_gradient_dot(perlin_hash(location_pos + ivec2(0, 1)), direction_pos - vec2(0, 1)),
    perlin_gradient_dot(perlin_hash(location_pos + ivec2(1, 1)), direction_pos - vec2(1, 1)),
    fade_pos.x);

    return max(mix(x1_x2_lerp, x3_x4_lerp, fade_pos.y), 0.0);
}

float perlin_octaves(vec2 coords, int octaves, float amplitude, float persistence, float frequency_increase) {
    float frequency = 1.0f;
    float max_value = 0.0f;
    float total_result = 0.0f;

    for(int i = 0; octaves > i; ++i) {
        max_value += amplitude;
        total_result += amplitude * perlin(coords * frequency);

        amplitude *= persistence;
        frequency *= frequency_increase;
    }

    return total_result / max_value;
}

vec2 interoplate_tex_height_map_coords() {
    vec2 texture_coord_first_u = mix(height_map_texture_coords[0], height_map_texture_coords[1], gl_TessCoord.x);
    vec2 texture_coord_bottom_u = mix(height_map_texture_coords[2], height_map_texture_coords[3], gl_TessCoord.x);

    return mix(texture_coord_first_u, texture_coord_bottom_u, gl_TessCoord.y);
}


vec2 interoplate_tex_coords(vec2 height_map_texture_coords) {
    vec2 tex_coords;

    vec2 texture_coord_base = vec2(model_ubo.vertex_resolution / model_ubo.terrain_num_of_textures,
                                   model_ubo.vertex_resolution / model_ubo.terrain_num_of_textures);

    vec2 current_coords = vec2((
        -model_ubo.vertex_resolution / 2.0f) + (model_ubo.vertex_resolution * height_map_texture_coords.x),
        (-model_ubo.vertex_resolution / 2.0f) + (model_ubo.vertex_resolution * height_map_texture_coords.y));
    tex_coords = current_coords / texture_coord_base;

    tex_coords.x = tex_coords.x - floor(tex_coords.x);
    tex_coords.y = tex_coords.y - floor(tex_coords.y);

    return tex_coords;
}

vec4 interoplate_position_coords() {
    vec4 position_first_u = mix(gl_in[0].gl_Position, gl_in[1].gl_Position, gl_TessCoord.x);
    vec4 position_bottom_u = mix(gl_in[2].gl_Position, gl_in[3].gl_Position, gl_TessCoord.x);

    vec4 position_v = mix(position_first_u, position_bottom_u, gl_TessCoord.y);

    position_v.w = 1.0;

    return position_v;
}

float get_smoothed_height_map_value(vec2 height_map_coords) {
    // TODO(Bobby): Do diagnols if smoothstep works

    // UNCOMMENT THIS IF IT IS NOT A CHUNKED TERRAIN!
    /*if(model_ubo.is_chunked_terrain == 0) {
        return height_map_value;
    }*/

    vec2 pixel_diff = vec2(1.0 / model_ubo.vertex_resolution);

    height_map_coords += when_equal_to(height_map_coords.x, 0) * vec2(pixel_diff.x, 0);
    height_map_coords += when_equal_to(height_map_coords.y, 0) * vec2(0, pixel_diff.y);

    // TODO(Bobby): this should really be pixel_diff.x instead of pixel_diff.y
    height_map_coords -= when_equal_to(height_map_coords.x, 1) * vec2(0, pixel_diff.x);
    height_map_coords -= when_equal_to(height_map_coords.y, 1) * vec2(0, pixel_diff.y);

    float height_map_value = texture(height_map[4], height_map_coords).r;

    float view_smoothing = when_equal_to(model_ubo.multi_texture_terrain_mode, 2);
    float dont_view_smoothing = 1 - view_smoothing;

    float and_term_1 =
        when_greater_equal_than(height_map_coords.y, 1 - model_ubo.chunked_blend_threshold);
    float and_term_2 =
        when_less_equal_than(height_map_coords.x, model_ubo.chunked_blend_threshold);
    float bilinear_interp_time = and_term_1 * and_term_2;
    float not_bilinear_interp_time = 1 - bilinear_interp_time;

    {
        // bilinear interpolation time
        //
        tessellation_out.diffuse_color_vector = vec4(1, 0, 0, 1) * bilinear_interp_time;


        float q_11 = texture(height_map[3], height_map_coords).r;
        float q_21 = texture(height_map[4], height_map_coords).r;
        float q_12 = (0.3333 * texture(height_map[0], height_map_coords).r) +
            (0.3333 * texture(height_map[3], height_map_coords).r) +
            (0.3333 * texture(height_map[1], height_map_coords).r);
        float q_22 = texture(height_map[1], height_map_coords).r;

        const float x_edge_one = 0;
        const float x_edge_two = model_ubo.chunked_blend_threshold;

        const float z_edge_one = 1 - model_ubo.chunked_blend_threshold;
        const float z_edge_two = 1;

        float x_edge_diff = x_edge_two - x_edge_one;
        float z_edge_diff = z_edge_two - z_edge_one;

        float y1_interp = ((x_edge_two - height_map_coords.x) / x_edge_diff) * q_11 +
            ((height_map_coords.x - x_edge_one) / x_edge_diff) * q_21;
        float y2_interp = ((x_edge_two - height_map_coords.x) / x_edge_diff) * q_12 +
            ((height_map_coords.x - x_edge_one) / x_edge_diff) * q_22;

        float z_interp = (((z_edge_two - height_map_coords.y) / z_edge_diff) * y1_interp) +
            (((height_map_coords.y - z_edge_one) / z_edge_diff) * y2_interp);

        //height_map_value += z_interp * bilinear_interp_time;
    }

    {
        // top edge
        vec2 top_tex_coords = vec2(height_map_coords.x, 0);
        float scaling_value =
            mix(1 - model_ubo.chunked_blend_threshold, 1.0f, height_map_coords.y);

        float top_edge_value = when_greater_equal_than(height_map_coords.y, 1 - model_ubo.chunked_blend_threshold);
        float not_top_edge_value = 1 - top_edge_value;
        vec4 height_map_diffuse_color_vector =
            (mix(vec4(0, 1, 0, 1), vec4(0), 1 - scaling_value) * view_smoothing) +
            (tessellation_out.diffuse_color_vector * dont_view_smoothing);
        float height_map_value_top_edge = height_map_value * scaling_value +
            texture(height_map[7], top_tex_coords).r * (1 - scaling_value);

        tessellation_out.diffuse_color_vector =
            (height_map_diffuse_color_vector * top_edge_value) +
            (tessellation_out.diffuse_color_vector * not_top_edge_value);


        float height_map_value_top_edge_total_value =
            (height_map_value_top_edge * top_edge_value) + (height_map_value * not_top_edge_value) - height_map_value;

        height_map_value += height_map_value_top_edge_total_value * not_bilinear_interp_time;
    }
    {
        // right edge
        float right_edge_value = when_less_equal_than(height_map_coords.x, model_ubo.chunked_blend_threshold);
        float not_right_edge_value = 1 - right_edge_value;

        vec2 right_tex_coords = vec2(1, height_map_coords.y);
        float scaling_value =
            mix(0, model_ubo.chunked_blend_threshold, height_map_coords.x);

        vec4 height_map_diffuse_color_vector =
            (mix(vec4(0, 1, 1, 1), vec4(0), 1 - scaling_value) * view_smoothing) +
            (tessellation_out.diffuse_color_vector * dont_view_smoothing);
        float height_map_value_right_value = height_map_value * (1 - scaling_value) +
            texture(height_map[5], right_tex_coords).r * scaling_value;

        tessellation_out.diffuse_color_vector =
            (height_map_diffuse_color_vector * right_edge_value) +
            (tessellation_out.diffuse_color_vector * not_right_edge_value);

        float height_map_value_right_edge =
            (height_map_value_right_value * right_edge_value) + (height_map_value * not_right_edge_value) - height_map_value;

        height_map_value += height_map_value_right_edge * not_bilinear_interp_time;
    }

    return height_map_value;
}

void main() {
    // need to differentiate noise texture coord from non noise texture coord!!!!!!

    // noise texture coord calculation
    // because gl_TessCoord is surface and not location specific, we are interpolating over 0 to 1
    // where 1 is the far edge and 0 is the near edge

    // normal calculation
    vec4 patch_u_vector = gl_in[1].gl_Position - gl_in[0].gl_Position;
    vec4 patch_v_vector = gl_in[2].gl_Position - gl_in[0].gl_Position;

    // position calculation
    tessellation_out.height_map_texture_coords = interoplate_tex_height_map_coords();
    tessellation_out.normal_vector = normalize(cross(patch_u_vector.xyz, patch_v_vector.xyz));

    vec4 position = interoplate_position_coords();

    tessellation_out.diffuse_color_vector = vec4(0.0f);

    float unit_height = get_smoothed_height_map_value(tessellation_out.height_map_texture_coords);
    float height = unit_height * model_ubo.terrain_height_magnitude;

    vec2 next_height_x = tessellation_out.height_map_texture_coords + vec2(1 / model_ubo.vertex_resolution, 0);
    //vec2 next_height_y = tessellation_out.height_map_texture_coords + vec2(0, 1 / model_ubo.vertex_resolution);

    float next_height_x_value = get_smoothed_height_map_value(next_height_x);
    //float next_height_y_value = get_smoothed_height_map_value(next_height_y);
    next_height_x_value = (next_height_x_value - 0.5) * 2;

    vec3 tangent = normalize(vec3(next_height_x_value - unit_height));
    vec3 bittangent = normalize(cross(tessellation_out.normal_vector, tangent));

    tessellation_out.tbn_matrix = mat3(tangent, bittangent, tessellation_out.normal_vector);

    position -= vec4(height * vec3(0, 1, 0), 0.0);

    tessellation_out.world_position = (mesh_ubo.model * position).xyz;
    tessellation_out.vertex_height = position.y;
    tessellation_out.texture_coords_out = interoplate_tex_coords(tessellation_out.height_map_texture_coords);

    gl_Position = camera_ubo.projection * camera_ubo.view * mesh_ubo.model * position;
}