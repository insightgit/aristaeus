//
// Created by bobby on 5/25/22.
//

#ifndef ARISTAEUS_GLFW_CREATE_WINDOW_SURFACE_HPP
#define ARISTAEUS_GLFW_CREATE_WINDOW_SURFACE_HPP

#include <stdexcept>

#include <vulkan/vulkan.h>

#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>

void create_glfw_window_surface(VkInstance instance, GLFWwindow *window, VkSurfaceKHR *surface_pointer);

#endif //ARISTAEUS_GLFW_CREATE_WINDOW_SURFACE_HPP
