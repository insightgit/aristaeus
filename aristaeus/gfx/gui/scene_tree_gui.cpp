//
// Created by bobby on 3/9/22.
//

#include "../vulkan/point_light.hpp"

#include "../vulkan/rasterization_lighting_environment.hpp"
#include "scene_tree_gui.hpp"

ARISTAEUS_CLASS_IMPL(aristaeus::gfx::gui::SceneTreeGui, typeid(aristaeus::gfx::SceneGraphNode));

void aristaeus::gfx::gui::SceneTreeGui::classdb_register() {
    // how to deal with const std::weak_ptr<gfx::SceneGraphNode> &scene_tree,
    auto &class_db = *aristaeus::core::ClassDB::instance;

    class_db.register_property<SceneTreeGui, std::string>("font_path",
                            [](SceneTreeGui &scene_tree_gui){return scene_tree_gui.m_font_path;},
                            [](SceneTreeGui &scene_tree_gui, std::string font_path){
                                scene_tree_gui.m_font_path = font_path; 
                                scene_tree_gui.refresh_font();
                            },
                            [](){return "";});
    class_db.register_property<SceneTreeGui, uint32_t>("font_size",
                            [](SceneTreeGui &scene_tree_gui){return scene_tree_gui.m_font_size;},
                            [](SceneTreeGui &scene_tree_gui, uint32_t font_size){
                                scene_tree_gui.m_font_size = font_size;
                                scene_tree_gui.refresh_font();
                            },
                            [](){return 16;});

    auto parsing_done_wrapper = [](SceneTreeGui &gui, void*) { gui.on_parsing_done(); };
    auto enter_tree_wrapper = [](SceneTreeGui &gui, void*) {gui.on_enter_tree(); };
    auto exit_tree_wrapper = [](SceneTreeGui &gui, void*) {gui.on_exit_tree(); };

    class_db.register_event_callback_on_construction<SceneTreeGui, void*>(std::string{SceneGraphNode::PARSING_DONE_EVENT},
                                                                          "scene_tree_gui_parsing_done",
                                                                          parsing_done_wrapper);
    class_db.register_event_callback_on_construction<SceneTreeGui, void*>(std::string{SceneGraphNode::ENTER_TREE_EVENT},
                                                                          "scene_tree_gui_enter_tree",
                                                                          enter_tree_wrapper);
    class_db.register_event_callback_on_construction<SceneTreeGui, void*>(std::string{SceneGraphNode::EXIT_TREE_EVENT},
                                                                          "scene_tree_gui_exit_tree",
                                                                          exit_tree_wrapper);
}


void aristaeus::gfx::gui::SceneTreeGui::on_enter_tree() {
    gfx::SceneGraphNode *rover = this;

    while (rover->get_parent() != nullptr) {
        rover = rover->get_parent();
    }

    if (rover != nullptr) {
        m_scene_tree = rover->get_weak_ptr_to_this();
    }
}

void aristaeus::gfx::gui::SceneTreeGui::on_exit_tree() {
    m_scene_tree = std::weak_ptr<SceneGraphNode>{};
}

void aristaeus::gfx::gui::SceneTreeGui::on_parsing_done() {
    auto &core_renderer = utils::get_singleton_safe<vulkan::CoreRenderer>();

    add_node_group(std::string{ gfx::vulkan::RasterizationLightingEnvironment::FORWARD_RENDER_NODE_GROUP });
    refresh_node_list();

    core_renderer.add_callback_for_event<void*, SceneTreeGui>(std::string{vulkan::CoreRenderer::FRAME_DONE_EVENT}, "scene_tree_gui_frame_done", 
                                                              [](SceneTreeGui &gui, void*){ gui.on_core_renderer_frame_done(); }, *this);
}

void aristaeus::gfx::gui::SceneTreeGui::on_core_renderer_frame_done() {
    ++m_frame_counter;

    if (std::chrono::duration<double>(std::chrono::system_clock::now() - m_fps_timer).count() >= 1) {
        m_past_fps = static_cast<float>(m_frame_counter) / std::chrono::duration<double>(std::chrono::system_clock::now() - m_fps_timer).count();

        m_running_fps_sum += m_frame_counter;
        ++m_fps_counts;

        m_fps_timer = std::chrono::system_clock::now();
        m_frame_counter = 0;
    }
     
    if (std::chrono::duration<double>(std::chrono::system_clock::now() - m_fps_avg).count() >= 10) {
        m_past_10_sec_avg_fps = m_running_fps_sum / m_fps_counts;

        m_fps_avg = std::chrono::system_clock::now();
        m_running_fps_sum = 0;
        m_fps_counts = 0;
    }
}

void aristaeus::gfx::gui::SceneTreeGui::draw() {
    vulkan::CoreRenderer &core_renderer = utils::get_singleton_safe<vulkan::CoreRenderer>();
    vulkan::RenderPhase &render_phase = *core_renderer.get_current_render_phase();
    bool in_default_rasterization_render_pass = core_renderer.in_default_rasterization_render_pass();

    if (!in_default_rasterization_render_pass) {
        return;
    }

    if (m_font != nullptr) {
        ImGui::PushFont(m_font);
    }

    std::shared_ptr<gfx::SceneGraphNode> scene_graph_node_strong = m_scene_tree.lock();

    glm::vec3 camera_pos = core_renderer.get_perspective_camera_environment().get_camera_position();

    draw_scene_node_panel(m_scene_tree, core_renderer);

    draw_perf_panel();

    if (m_adding_child) {
        std::shared_ptr<gfx::SceneGraphNode> node_being_added_strong = m_node_being_added.lock();

        if (node_being_added_strong == nullptr) {
            m_adding_child = false;
        }
        else {
            draw_add_node_panel(*node_being_added_strong);
        }
    }

    if (m_font != nullptr) {
        ImGui::PopFont();
    }
}

aristaeus::gfx::gui::SceneTreeGui::AddingNodeInfo aristaeus::gfx::gui::SceneTreeGui::get_adding_node_info(const std::string_view &node_type_info_name) {
    assert(core::ClassDB::instance != nullptr);
    core::ClassDB &class_db = *core::ClassDB::instance;

    AddingNodeInfo return_value{
        .node_type_info_name = node_type_info_name,
        .node_type_info = class_db.get_type_info_for_registered_class(node_type_info_name)
    };

    return return_value;
}

void aristaeus::gfx::gui::SceneTreeGui::display_ui_functions(const AddingNodeInfo &adding_node_info) {
    if (ImGui::TreeNode((std::string{ adding_node_info.node_type_info_name } + " properties").c_str())) {
        m_adding_node_working_json = core::ClassDB::instance->call_ui_functions("new_node", adding_node_info.node_type_info);

        ImGui::TreePop();
    }
}

void aristaeus::gfx::gui::SceneTreeGui::draw_add_node_panel(gfx::SceneGraphNode &scene_graph_node) {
    assert(core::ClassDB::instance != nullptr);
    core::ClassDB &class_db = *core::ClassDB::instance;

    ImGui::Begin(("Adding child to " + scene_graph_node.get_node_name()).c_str(), &m_adding_child);

    if (m_ui_selected_class.has_value()) {
        display_ui_functions(m_ui_selected_class.value());

        if (ImGui::Button("Confirm")) {
            m_adding_node_working_json = core::ClassDB::instance->call_ui_functions("new_node", m_ui_selected_class.value().node_type_info, true);

            m_adding_node_working_json["type"] = m_ui_selected_class.value().node_type_info_name;

            #ifndef NDEBUG
            std::ofstream tmp_file{ "last_node_addition.json" };

            tmp_file << m_adding_node_working_json.dump(4) << "\n";

            tmp_file.close();
            #endif

            scene_graph_node.add_child(std::shared_ptr<SceneGraphNode>(class_db.parse_node(m_adding_node_working_json)));

            m_adding_node_working_json.clear();
            m_ui_selected_class = {};
            m_adding_child = false;
        }
        else if (ImGui::Button("Back")) {
            m_ui_selected_class = {};
        }
    }
    else {
        ImGui::ListBox("Nodes available", &m_currently_selected_class, m_ui_initializable_classes_c_ptr_wrapper.data(), m_ui_initializable_classes_c_ptr_wrapper.size());
        
        if (ImGui::Button(("Create " + std::string{ m_ui_initializable_classes[m_currently_selected_class] }).c_str())) {
            m_ui_selected_class = get_adding_node_info(m_ui_initializable_classes[m_currently_selected_class]);
            
            m_adding_node_working_json.clear();
            m_adding_node_working_json["type"] = m_ui_selected_class.value().node_type_info_name;
        }
    }

    ImGui::End();
}

void aristaeus::gfx::gui::SceneTreeGui::draw_perf_panel() {
    auto &core_renderer = utils::get_singleton_safe<vulkan::CoreRenderer>();
    auto &mesh_manager = utils::get_singleton_safe<vulkan::MeshManager>();

    uint32_t past_frame;

    if (core_renderer.get_current_frame() == 0) {
        past_frame = utils::vulkan::MAX_FRAMES_IN_FLIGHT - 1;
    }
    else {
        past_frame = core_renderer.get_current_frame() - 1;
    }

    ImGui::Begin("Performance");

    ImGui::Text("FPS: %f", m_past_fps);
    ImGui::Text("FPS 10 sec avg: %f", m_past_10_sec_avg_fps);

    ImGui::Text("MeshManager Triangle Count: %d", mesh_manager.get_triangle_count(past_frame));
    ImGui::Text("MeshManager Drawcalls: %d", mesh_manager.get_draw_calls_count(past_frame));

    ImGui::Text("Num of render phases: %d", core_renderer.get_render_phase_dep_node_root().get_phases_last_rendered().size());
    ImGui::Text("Num of render contexts: %d", core_renderer.get_render_phase_dep_node_root().get_total_num_of_render_contexts_last_rendered());

    ImGui::End();
}

void aristaeus::gfx::gui::SceneTreeGui::draw_scene_node_panel(const std::weak_ptr<gfx::SceneGraphNode> &scene_graph_node, vulkan::CoreRenderer &core_renderer) {
    std::shared_ptr<gfx::SceneGraphNode> scene_graph_node_strong = scene_graph_node.lock();

    if (scene_graph_node_strong == nullptr) {
        ImGui::Text("Scene Tree has been destroyed/couldn't find Scene Tree root!");
        return;
    }

    if (scene_graph_node_strong != nullptr && scene_graph_node_strong->get_node_name() == gfx::SceneGraphNode::ROOT_NODE_NAME && ImGui::Button("Reset to empty root")) {
        scene_graph_node_strong->remove_all_children(true, core_renderer.get_current_frame());

        std::shared_ptr<gfx::gui::SceneTreeGui> new_scene_tree_gui = std::make_shared<gfx::gui::SceneTreeGui>();

        new_scene_tree_gui->rename_node("SceneTreeGui");
        new_scene_tree_gui->set_weak_ptr_to_this(new_scene_tree_gui);
        new_scene_tree_gui->m_font = m_font;
        new_scene_tree_gui->m_font_size = m_font_size;
        new_scene_tree_gui->m_font_path = m_font_path;
        new_scene_tree_gui->refresh_node_list();

        scene_graph_node_strong->add_child(new_scene_tree_gui);
    }

    if (ImGui::TreeNode((scene_graph_node_strong->get_node_name() + " (" +
                        std::string(scene_graph_node_strong->get_class_type()) + ")").c_str())) {
        int z_order = scene_graph_node_strong->get_z_order();

        if (ImGui::TreeNode("Transform")) {
            SceneGraphNode::Transform transform = scene_graph_node_strong->get_transform();
            glm::vec4 rotation_vec4{ transform.rotation_quat.x, transform.rotation_quat.y, transform.rotation_quat.z, transform.rotation_quat.w };

            ImGui::BeginGroup();

            draw_vec3_panel("Position", transform.position);
            ImGui::SameLine();
            if (ImGui::Button("adjust-t")) {
                m_current_guizmo_transform = scene_graph_node_strong.get();
                m_current_guizmo_op = ImGuizmo::TRANSLATE;
            }

            draw_vec3_panel("Scale", transform.scale);
            ImGui::SameLine();
            if (ImGui::Button("adjust-s")) {
                m_current_guizmo_transform = scene_graph_node_strong.get();
                m_current_guizmo_op = ImGuizmo::SCALE;
            }

            if (ImGui::TreeNode("Rotation")) {
                if (ImGui::Button("adjust-r")) {
                    m_current_guizmo_transform = scene_graph_node_strong.get();
                    m_current_guizmo_op = ImGuizmo::ROTATE;
                }
                static constexpr float PI = 3.141592653589793238f;

                glm::vec3 roll_pitch_yaw = glm::eulerAngles(transform.rotation_quat) * glm::vec3{ 180.0f / PI };
                glm::vec3 past_roll_pitch_yaw = roll_pitch_yaw;

                draw_vec4_panel("Quaternion", rotation_vec4);
                ImGui::InputFloat("Roll (x)", &roll_pitch_yaw.x, 1, 15);
                ImGui::InputFloat("Pitch (y)", &roll_pitch_yaw.y, 1, 15);
                ImGui::InputFloat("Yaw (z)", &roll_pitch_yaw.z, 1, 15);

                if (roll_pitch_yaw == past_roll_pitch_yaw) {
                    transform.rotation_quat = glm::quat{ rotation_vec4.w, rotation_vec4.x, rotation_vec4.y, rotation_vec4.z };
                }
                else {
                    transform.rotation_quat = glm::quat{ roll_pitch_yaw * glm::vec3{PI / 180.0f} };
                }

                ImGui::TreePop();
            }
                       

            ImGui::EndGroup();

            if (m_current_guizmo_transform == scene_graph_node_strong.get()) {
                ImGuiIO &io = ImGui::GetIO();
                float guizmo_matrix[16];

                glm::mat4 projection_matrix = gfx::vulkan::CoreRenderer::instance->get_projection_matrix();
                glm::mat4 view_matrix = gfx::vulkan::CoreRenderer::instance->get_view_matrix();
                SceneGraphNode::Transform parent_transform = scene_graph_node_strong->get_parent_transform();

                transform += parent_transform;

                ImGuizmo::RecomposeMatrixFromComponents(&transform.position[0], &transform.rotation_quat[0], &transform.scale[0], &guizmo_matrix[0]);

                ImGuizmo::SetRect(0, 0, io.DisplaySize.x, io.DisplaySize.y);
                ImGuizmo::SetDrawlist(ImGui::GetForegroundDrawList());
                ImGuizmo::Manipulate(&view_matrix[0][0], &projection_matrix[0][0], m_current_guizmo_op, ImGuizmo::LOCAL, &guizmo_matrix[0], nullptr, nullptr);

                glm::vec3 axis_angle_degrees;

                ImGuizmo::DecomposeMatrixToComponents(&guizmo_matrix[0], &transform.position[0], &axis_angle_degrees[0], &transform.scale[0]);

                static constexpr float PI = 3.1415926535f;

                glm::vec3 axis_angles_radians = glm::vec3{ PI } * (axis_angle_degrees / glm::vec3{ 180.0f });
                
                transform.rotation_quat = glm::quat{axis_angles_radians};

                if (transform.rotation_quat == glm::quat{1, 0, 0, 0}) {
                    transform.rotation_quat.w = 0;
                }

                transform -= parent_transform;
            }

            scene_graph_node_strong->set_transform(transform);

            ImGui::TreePop();
        }

        ImGui::InputInt("Z Order", &z_order);

        if (scene_graph_node_strong->get_z_order() != z_order) {
            m_z_order_updates.push_back({ scene_graph_node, z_order });
        }

        std::stringstream node_groups;

        node_groups << "Node Groups " << "(" << std::to_string(scene_graph_node_strong->get_node_groups().size()) << "): ";

        for (const std::string &node_group : scene_graph_node_strong->get_node_groups()) {
            node_groups << node_group << ", ";
        }

        ImGui::Text(node_groups.str().c_str());

        std::map<std::string, std::shared_ptr<gfx::SceneGraphNode>> children = scene_graph_node_strong->get_map();

        if (ImGui::TreeNode((std::to_string(children.size()) + std::string(" children")).c_str())) {
            if (!m_ui_initializable_classes.empty() && ImGui::Button("Add Child")) {
                m_node_being_added = scene_graph_node;
                m_adding_child = true;
            }

            for (const std::pair <const std::string, std::shared_ptr<gfx::SceneGraphNode>> &child : children) {
                draw_scene_node_panel(child.second, core_renderer);
            }

            ImGui::TreePop();
        }

        if (scene_graph_node_strong->get_node_name() != SceneGraphNode::ROOT_NODE_NAME && scene_graph_node_strong->get_class_type() != CLASS_TYPE &&
            ImGui::Button("Delete Node")) {
            std::string name = scene_graph_node_strong->get_node_name();
            bool ret = scene_graph_node_strong->get_parent()->remove_child(scene_graph_node_strong, true, core_renderer.get_current_frame());

            if (!ret) {
                std::cerr << "Node " << name << " couldn't be removed!" << std::endl;
            }
        }

        ImGui::TreePop();
    }
}

void aristaeus::gfx::gui::SceneTreeGui::draw_vec3_panel(const std::string &label, glm::vec3 &vec3_to_modify) {
    if (ImGui::TreeNode(label.data())) {
        ImGui::InputFloat("x", &vec3_to_modify.x, 0.001f, 0.5f, "%.3f");
        ImGui::InputFloat("y", &vec3_to_modify.y, 0.001f, 0.5f, "%.3f");
        ImGui::InputFloat("z", &vec3_to_modify.z, 0.001f, 0.5f, "%.3f");

        ImGui::TreePop();
    }
}

void aristaeus::gfx::gui::SceneTreeGui::draw_vec4_panel(const std::string &label, glm::vec4 &vec4_to_modify) {
    if(ImGui::TreeNode(label.data())) {
        ImGui::InputFloat("x", &vec4_to_modify.x, 0.001f, 0.5f, "%.3f");
        ImGui::InputFloat("y", &vec4_to_modify.y, 0.001f, 0.5f, "%.3f");
        ImGui::InputFloat("z", &vec4_to_modify.z, 0.001f, 0.5f, "%.3f");
        ImGui::InputFloat("w", &vec4_to_modify.w, 0.001f, 0.5f, "%.3f");

        ImGui::TreePop();
    }
}

void aristaeus::gfx::gui::SceneTreeGui::refresh_node_list() {
    core::ClassDB &class_db = *core::ClassDB::instance;

    m_ui_initializable_classes = class_db.get_ui_initializable_classes();
    m_ui_initializable_classes_c_ptr_wrapper.clear();

    for (const std::string_view &ui_initializable_class : m_ui_initializable_classes) {
        m_ui_initializable_classes_c_ptr_wrapper.push_back(ui_initializable_class.data());
    }
}
