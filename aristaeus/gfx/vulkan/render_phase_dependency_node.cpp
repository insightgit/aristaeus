#include "core_renderer.hpp"
#include "render_phase_dependency_node.hpp"

void aristaeus::gfx::vulkan::RenderPhaseDependencyNode::contextless_draw() {
	auto &core_renderer = utils::get_singleton_safe<CoreRenderer>();
	std::queue<std::reference_wrapper<RenderPhaseDependencyNode>> rover_queue;

	rover_queue.push(*this);

	while (!rover_queue.empty()) {
		RenderPhaseDependencyNode &current_node = rover_queue.front();

		core_renderer._set_current_render_phase(&current_node.m_render_phase);

		current_node.m_render_phase.contextless_draw();

		for (const std::shared_ptr<RenderPhaseDependencyNode> &dep_node : current_node.m_dependents) {
			if (dep_node != nullptr) {
				rover_queue.push(*dep_node);
			}
		}

		core_renderer._set_current_render_phase(nullptr);

		rover_queue.pop();
	}
}

void aristaeus::gfx::vulkan::RenderPhaseDependencyNode::get_submit_infos(std::vector<vk::SubmitInfo> &submit_info_vector) {
	m_render_contexts_last_rendered = 0;
	m_phases_last_rendered.clear();

	if (m_parents.empty()) {
		std::unordered_map<std::string, vk::PipelineStageFlags> dependencies = m_render_phase.get_dependencies();

		assert(dependencies.size() == 1 && dependencies.contains(std::string{ CoreRenderer::FRAME_AVAILABLE_DEP }));
	}

	for (std::weak_ptr<RenderPhaseDependencyNode> &parent_node : m_parents) {
		std::shared_ptr<RenderPhaseDependencyNode> parent_node_strong = parent_node.lock();

		if (parent_node_strong == nullptr) {
			continue;
		}

		// we need all our dependencies already executed before us before we execute
		std::string parent_phase_name = parent_node_strong->m_render_phase.get_render_phase_name();

		assert(m_cleared_dependencies.size() < m_render_phase.get_dependencies().size());
		assert(m_render_phase.get_dependencies().contains(parent_phase_name));
		assert(!m_cleared_dependencies.contains(parent_phase_name));

		m_cleared_dependencies.insert(parent_phase_name);

		if (m_cleared_dependencies.size() == m_render_phase.get_dependencies().size()) {
			break;
		}
	}

	if (m_cleared_dependencies.size() != m_render_phase.get_dependencies().size() && !m_parents.empty()) {
		assert(m_render_phase.get_dependencies().size() == m_parents.size());
		return;
	}
	else {
		m_cleared_dependencies.clear();
	}

	CoreRenderer &core_renderer = utils::get_singleton_safe<CoreRenderer>();

	core_renderer._set_current_render_phase(&m_render_phase);

	uint32_t current_frame = core_renderer.get_current_frame();
	std::optional<vk::SubmitInfo> phase_submit_info = m_render_phase.submit_render_phase();
	bool execute_phase = phase_submit_info.has_value();

	m_temp_semaphores[current_frame].clear();

	if (execute_phase) {
		for (std::weak_ptr<RenderPhaseDependencyNode> &parent_node : m_parents) {
			std::shared_ptr<RenderPhaseDependencyNode> parent_node_strong = parent_node.lock();

			if (parent_node_strong != nullptr) {
				// if we know that the previous parent phase didn't execute
				// due to not having anything to execute, we will just add the phases that
				// that previous phase depended on. If those phases didn't execute, we'll
				// move on to the previous phases and so on until we reach frame_available
				std::queue<std::reference_wrapper<RenderPhaseDependencyNode>> dep_node_queue;

				dep_node_queue.push(*parent_node_strong);

				while (!dep_node_queue.empty()) {
					RenderPhaseDependencyNode &dep_node_ref = dep_node_queue.front();

					if (dep_node_ref.m_executed) {
						m_temp_semaphores[current_frame].push_back(*dep_node_ref.m_render_phase.get_signal_semaphore(current_frame));
					}
					else {
						for (std::weak_ptr<RenderPhaseDependencyNode> &parent_of_parent : dep_node_ref.m_parents) {
							std::shared_ptr<RenderPhaseDependencyNode> parent_of_parent_strong = parent_of_parent.lock();

							if (parent_of_parent_strong != nullptr) {
								dep_node_queue.push(*parent_of_parent_strong);
							}
						}

						if (dep_node_ref.m_parents.empty()) {
							const std::string frame_available_string{ CoreRenderer::FRAME_AVAILABLE_DEP };

							assert(dep_node_ref.m_render_phase.m_dependencies.size() == 1 && 
								   dep_node_ref.m_render_phase.m_dependencies.contains(frame_available_string));

							m_temp_semaphores[current_frame].push_back(*core_renderer.get_render_phase_semaphore(frame_available_string, current_frame));
						}
					}

					dep_node_queue.pop();
				}
			}
		}

		if (!m_temp_semaphores[current_frame].empty()) {
			phase_submit_info->pWaitSemaphores = m_temp_semaphores[current_frame].data();
			phase_submit_info->waitSemaphoreCount = m_temp_semaphores[current_frame].size();
		}

		// TODO(Bobby): handle when the leaf chooses not to execute as a one-shot: right now the FRAME_DONE_DEP in that
		// TODO(Bobby): case won't get signaled.
		if (m_dependents.empty()) {
			bool wait_semaphores_using_temp = phase_submit_info->pWaitSemaphores == m_temp_semaphores[current_frame].data();

			m_temp_semaphores[current_frame].push_back(*core_renderer.get_render_phase_semaphore(std::string{ CoreRenderer::FRAME_DONE_DEP }, current_frame));

			phase_submit_info->pSignalSemaphores = &m_temp_semaphores[current_frame].back();
			phase_submit_info->signalSemaphoreCount = 1;

			if (wait_semaphores_using_temp) {
				phase_submit_info->pWaitSemaphores = m_temp_semaphores[current_frame].data();
			}
		}

		submit_info_vector.push_back(*phase_submit_info);

		m_phases_last_rendered.push_back(m_render_phase.get_render_phase_name());
		m_render_contexts_last_rendered += m_render_phase.get_num_of_render_contexts_last_rendered();

		m_executed = true;
	}
	else {
		m_executed = false;
	}

	for (const std::shared_ptr<RenderPhaseDependencyNode> &dep_node : m_dependents) {
		if (dep_node != nullptr) {
			dep_node->get_submit_infos(submit_info_vector);

			m_phases_last_rendered.insert(m_phases_last_rendered.end(), dep_node->m_phases_last_rendered.begin(), 
										  dep_node->m_phases_last_rendered.end());
			m_render_contexts_last_rendered += dep_node->m_render_contexts_last_rendered;
		}
	}

	m_cleared_dependencies.clear();
}

aristaeus::gfx::vulkan::RenderPhaseDependencyNode &aristaeus::gfx::vulkan::RenderPhaseDependencyNode::add_render_phase(RenderPhase render_phase) {
	std::unordered_set<std::string> accounted_for_dependencies;
	std::unordered_map<std::string, vk::PipelineStageFlags> render_phase_dependencies = render_phase.get_dependencies();
	std::queue<std::reference_wrapper<RenderPhaseDependencyNode>> render_phase_queue;

	if (render_phase_dependencies.empty()) {
		// the render phase passed in becomes the root of the tree and the
		// phase represented by the previous root node becomes a child of that new root node.
		std::shared_ptr<RenderPhaseDependencyNode> child_node = std::make_shared<RenderPhaseDependencyNode>(std::move(m_render_phase));
		
		child_node->set_weak_ptr_to_self(child_node);

		m_render_phase = std::move(render_phase);

		child_node->m_render_phase.m_dependencies.clear();
		child_node->m_render_phase.m_dependencies.insert({get_render_phase_name(), vk::PipelineStageFlagBits::eFragmentShader});

		child_node->m_dependents = m_dependents;

		for (const std::shared_ptr<RenderPhaseDependencyNode> &node : child_node->m_dependents) {
			if (node != nullptr) {
				bool found = false;

				for (std::weak_ptr<RenderPhaseDependencyNode> &node_parent : node->m_parents) {
					std::shared_ptr<RenderPhaseDependencyNode> node_parent_strong = node_parent.lock();

					if (node_parent_strong != nullptr && node_parent_strong->get_render_phase_name() == get_render_phase_name()) {
						node_parent = child_node;
						found = true;
						break;
					}
				}

				assert(found);
			}
		}

		m_dependents.clear();

		m_dependents.insert(child_node);

		bool found = false;

		for (std::weak_ptr<RenderPhaseDependencyNode> &existing_parent : child_node->m_parents) {
			if (existing_parent.lock() == m_weak_ptr_to_self.lock()) {
				found = true;
				break;
			}
		}

		if (!found) {
			child_node->m_parents.push_back(m_weak_ptr_to_self);
		}
		
		return *this;
	}


	render_phase_queue.push(*this);

	std::shared_ptr<RenderPhaseDependencyNode> working_node = std::make_shared<RenderPhaseDependencyNode>(std::move(render_phase));

	working_node->set_weak_ptr_to_self(working_node);

	while (!render_phase_queue.empty()) {
		RenderPhaseDependencyNode &render_phase_node = render_phase_queue.front().get();
		std::string render_phase_name = render_phase_node.m_render_phase.get_render_phase_name();

		if (render_phase_dependencies.contains(render_phase_name) && !accounted_for_dependencies.contains(render_phase_name)) {
			accounted_for_dependencies.insert(render_phase_name);

			render_phase_node.m_dependents.insert(working_node);

			working_node->m_parents.push_back(render_phase_node.m_weak_ptr_to_self);

			if (accounted_for_dependencies.size() == render_phase_dependencies.size()) {
				// we are resolving the tree such that RenderPhaseNodes that should be dependent on this new inserted node are updated accordingly
				// TODO(Bobby): Support out of order dependency resolution here
				for(const std::pair<std::string, vk::PipelineStageFlags> &dependent_node : working_node->m_render_phase.get_dependents()) {
					add_dependency(render_phase_node.get_render_phase_name(), dependent_node.first, dependent_node.second);
				}

				return *working_node;
			}
		}

		for(const std::shared_ptr<RenderPhaseDependencyNode> &dependent : render_phase_node.m_dependents) {
			if (dependent != nullptr) {
				render_phase_queue.push(*dependent);
			}
		}

		render_phase_queue.pop();
	}
	assert(accounted_for_dependencies.size() != render_phase_dependencies.size());

	// TODO(Bobby): allow for out of order insertion and resolving initially nonexistent dependencies
	throw std::runtime_error("out of order dependency insertion not supported yet");
}

std::unordered_map<std::string, vk::PipelineStageFlags> aristaeus::gfx::vulkan::RenderPhaseDependencyNode::get_all_dependencies() const {
	std::unordered_map<std::string, vk::PipelineStageFlags> dependencies;
	std::queue<std::shared_ptr<RenderPhaseDependencyNode>> rover_queue;

	rover_queue.push(m_weak_ptr_to_self.lock());

	while (!rover_queue.empty()) {
		std::shared_ptr<RenderPhaseDependencyNode> current_dep_node = rover_queue.front();

		if (current_dep_node == nullptr) {
			continue;
		}

		for (const std::pair<const std::string, vk::PipelineStageFlags> &dependency : current_dep_node->get_render_phase().m_dependencies) {
			if (dependencies.contains(dependency.first)) {
				dependencies.at(dependency.first) |= dependency.second;
			}
			else {
				dependencies.insert(dependency);
			}
		}

		for (std::weak_ptr<RenderPhaseDependencyNode> &parent_render_node : current_dep_node->m_parents) {
			std::shared_ptr<RenderPhaseDependencyNode> parent_render_node_strong = parent_render_node.lock();

			if (parent_render_node_strong == nullptr) {
				continue;
			}

			rover_queue.push(parent_render_node_strong);
		}

		rover_queue.pop();
	}

	return dependencies;
}

void aristaeus::gfx::vulkan::RenderPhaseDependencyNode::add_dependency(const std::string &dependency, const std::string &dependent, vk::PipelineStageFlags pipeline_flags) {
	// TODO(Bobby): refactor this entire function from the ground up
	std::shared_ptr<RenderPhaseDependencyNode> dependency_node = get_render_phase_node(dependency);
	std::shared_ptr<RenderPhaseDependencyNode> dependent_node = get_render_phase_node(dependent);

	struct RenderPhaseDepInfo {
		std::shared_ptr<RenderPhaseDependencyNode> render_phase_dep_node;
		vk::PipelineStageFlags dep_flags;
	};

	std::unordered_map<std::string, RenderPhaseDepInfo> new_dependency_node_parents;

	if (dependency_node == nullptr || dependent_node == nullptr || dependency_node == dependent_node || dependent_node->m_render_phase.m_dependencies.contains(dependency)) {
		return;
	}

	std::string dependency_node_render_phase_name = dependency_node->m_render_phase.get_render_phase_name();
	std::string dependent_node_render_phase_name = dependent_node->m_render_phase.get_render_phase_name();

	// add pre-existing parent nodes to the dependency node

	for (size_t i = 0; dependency_node->m_parents.size() > i; ++i) {
		std::shared_ptr<RenderPhaseDependencyNode> parent_strong = dependency_node->m_parents[i].lock();

		if (parent_strong == nullptr) {
			continue;
		}

		for (const std::pair<const std::string, vk::PipelineStageFlags> &parent_dep : parent_strong->get_all_dependencies()) {
			new_dependency_node_parents.insert({ parent_dep.first, RenderPhaseDepInfo{parent_strong, parent_dep.second} });
		}
	}

	for (std::weak_ptr<RenderPhaseDependencyNode> &parent : dependent_node->m_parents) {
		std::shared_ptr<RenderPhaseDependencyNode> parent_strong = parent.lock();

		if (parent_strong == nullptr) {
			continue;
		}

		for (std::pair<const std::string, vk::PipelineStageFlags> dependent : parent_strong->get_all_dependencies()) {
			if (new_dependency_node_parents.contains(dependent.first)) {
				if ((new_dependency_node_parents.at(dependent.first).dep_flags & dependent.second) != vk::PipelineStageFlags{}) {
					// this dependency is already covered by one of the dependent node's parents, we can therefore get
					// rid of this specific dependency from the dependency node.
					new_dependency_node_parents.erase(dependent.first);
				}
				else {
					new_dependency_node_parents.at(dependent.first).dep_flags |= dependent.second;
				}
			}
			else if(!new_dependency_node_parents.contains(dependent.first)) {
				new_dependency_node_parents.insert({ dependent.first, RenderPhaseDepInfo{ parent_strong, dependent.second }});
			}
		}
	}

	RenderPhase &dependency_render_phase = dependency_node->get_render_phase();
	RenderPhase &dependent_render_phase = dependent_node->get_render_phase();

	//dependency_node->m_parents.clear();

	dependency_render_phase.m_dependencies.clear();
	dependency_render_phase.m_dependencies.insert(dependent_render_phase.m_dependencies.begin(), dependent_render_phase.m_dependencies.end());

	dependent_render_phase.m_dependencies.clear();
	dependent_render_phase.m_dependencies.insert({dependency_render_phase.get_render_phase_name(), pipeline_flags});

	for (const std::pair<const std::string, RenderPhaseDepInfo> &new_dependency_node_pair : new_dependency_node_parents) {
		if (new_dependency_node_pair.second.render_phase_dep_node != nullptr) {
			bool found = false;

			for (std::weak_ptr<RenderPhaseDependencyNode> &existing_parent : dependency_node->m_parents) {
				if (existing_parent.lock() == new_dependency_node_pair.second.render_phase_dep_node) {
					found = true;
					break;
				}
			}

			if (!found) {
				dependency_node->m_parents.push_back(new_dependency_node_pair.second.render_phase_dep_node);
			}
			
			dependency_render_phase.m_dependencies[new_dependency_node_pair.first] |= new_dependency_node_pair.second.dep_flags;
		}
	}

	for (std::weak_ptr<RenderPhaseDependencyNode> &dependent_node_parent : dependent_node->m_parents) {
		std::shared_ptr<RenderPhaseDependencyNode> dependent_node_parent_strong = dependent_node_parent.lock();

		dependent_node_parent_strong->m_dependents.erase(dependent_node);
	}

	dependent_node->m_parents.clear();
	dependent_node->m_parents.push_back(dependency_node);

	dependency_node->m_dependents.insert(dependent_node);

}

std::shared_ptr<aristaeus::gfx::vulkan::RenderPhaseDependencyNode> aristaeus::gfx::vulkan::RenderPhaseDependencyNode::get_render_phase_node(const std::string &render_phase_name) {
	std::queue<std::shared_ptr<RenderPhaseDependencyNode>> dep_nodes;

	dep_nodes.push(m_weak_ptr_to_self.lock());

	assert(dep_nodes.front() != nullptr);

	while (!dep_nodes.empty()) {
		if (dep_nodes.front() == nullptr) {
			continue;
		}

		RenderPhaseDependencyNode &dep_node_ref = *dep_nodes.front();
		if (dep_node_ref.m_render_phase.get_render_phase_name() == render_phase_name) {
			return dep_nodes.front();
		}
		else {
			dep_nodes.pop();

			for (const std::shared_ptr<RenderPhaseDependencyNode> &new_dep_node : dep_node_ref.m_dependents) {
				dep_nodes.push(new_dep_node);
			}
		}
	}

	return nullptr;
}

std::unordered_map<std::string, vk::PipelineStageFlags> aristaeus::gfx::vulkan::RenderPhaseDependencyNode::get_all_dependents() const {
	std::unordered_map<std::string, vk::PipelineStageFlags> return_value;
	std::queue<std::reference_wrapper<const RenderPhaseDependencyNode>> children;

	children.push(*this);

	while (!children.empty()) {
		const RenderPhaseDependencyNode &rover = children.front().get();

		for (const std::pair<const std::string, vk::PipelineStageFlags> &dependent : rover.m_render_phase.m_dependents) {
			return_value[dependent.first] |= dependent.second;
		}

		for (const std::shared_ptr<RenderPhaseDependencyNode> &dep_node : rover.m_dependents) {
			children.push(*dep_node);
		}

		children.pop();
	}

	return return_value;
}

void aristaeus::gfx::vulkan::RenderPhaseDependencyNode::imgui_draw(vk::CommandBuffer &command_buffer) {
	auto &core_renderer = utils::get_singleton_safe<CoreRenderer>();
	std::queue<std::reference_wrapper<RenderPhaseDependencyNode>> rover_queue;

	rover_queue.push(*this);

	while (!rover_queue.empty()) {
		RenderPhaseDependencyNode &current_node = rover_queue.front();

		if (ImGui::TreeNode(current_node.m_render_phase.get_render_phase_name().c_str())) {
			current_node.m_render_phase.imgui_draw(command_buffer);

			ImGui::TreePop();
		}

		for (const std::shared_ptr<RenderPhaseDependencyNode> &dep_node : current_node.m_dependents) {
			if (dep_node != nullptr) {
				rover_queue.push(*dep_node);
			}
		}

		rover_queue.pop();
	}
}
