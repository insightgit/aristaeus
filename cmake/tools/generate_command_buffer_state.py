import sys

class ParsingState(Enum):
    InitialState,
    CommandBufferClassFound,
    CommandBufferCommandsFound


def main():
    if len(sys.argv) < 2:
        print(f"{sys.argv[0]} [vulkan_hpp_path]")
        sys.exit(1)

    vulkan_hpp_path = sys.argv[1]

    # TODO(Bobby): find a way to do this without reading the entire file in
    with open(f"{vulkan_hpp_path}/vulkan_handles.hpp", 'r') as vulkan_handles_file:
        lines = vulkan_handles_file.readlines()

    parsing_state = ParsingState.InitialState

    for line in lines:
        if parsing_state == ParsingState.InitialState and "class CommandBuffer" in line and ";" not in line:
            parsing_state = ParsingState.CommandBufferClassFound
        elif parsing_state == ParsingState.CommandBufferClassFound and "//=== VK_VERSION_1_0 ===" in line:
            pass


if __name__ == "__main__":
    main()