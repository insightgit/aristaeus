#include "core_renderer.hpp"
#include "image_buffer_raii.hpp"

aristaeus::gfx::vulkan::VmaImage::SourceAndDestinationImageInfo aristaeus::gfx::vulkan::VmaImage::get_default_src_and_dest_stage(vk::ImageLayout old_image_layout, vk::ImageLayout new_image_layout, 
                                                                                                                                 vk::PipelineBindPoint pipeline_bind_point) {
    SourceAndDestinationImageInfo src_and_dst_info;

    vk::PipelineStageFlags shader_stage = pipeline_bind_point == vk::PipelineBindPoint::eCompute ? vk::PipelineStageFlagBits::eComputeShader : vk::PipelineStageFlagBits::eFragmentShader;

    if (old_image_layout == vk::ImageLayout::eUndefined) {
        // when transferring from undefined, transfer writes don't have to wait for anything
        src_and_dst_info.source_stage = vk::PipelineStageFlagBits::eTopOfPipe;

        if (new_image_layout == vk::ImageLayout::eGeneral) {
            // usually just used for compute storage images
            src_and_dst_info.destination_access_flags = vk::AccessFlagBits::eShaderRead | vk::AccessFlagBits::eShaderWrite;
            src_and_dst_info.destination_stage = shader_stage;
        }
        else {
            src_and_dst_info.destination_access_flags = vk::AccessFlagBits::eTransferWrite;
            src_and_dst_info.destination_stage = vk::PipelineStageFlagBits::eTransfer;
        }
    }
    else {
        switch (old_image_layout) {
        case vk::ImageLayout::eColorAttachmentOptimal:
            src_and_dst_info.source_access_flags = vk::AccessFlagBits::eColorAttachmentWrite;
            src_and_dst_info.source_stage = vk::PipelineStageFlagBits::eColorAttachmentOutput;
            break;
        case vk::ImageLayout::eDepthStencilAttachmentOptimal:
            src_and_dst_info.source_access_flags = vk::AccessFlagBits::eDepthStencilAttachmentWrite;
            src_and_dst_info.source_stage = vk::PipelineStageFlagBits::eLateFragmentTests;
            break;
        case vk::ImageLayout::eTransferSrcOptimal:
            src_and_dst_info.source_access_flags = vk::AccessFlagBits::eTransferRead;
            src_and_dst_info.source_stage = vk::PipelineStageFlagBits::eTransfer;
            break;
        case vk::ImageLayout::eTransferDstOptimal:
            src_and_dst_info.source_access_flags = vk::AccessFlagBits::eTransferWrite;
            src_and_dst_info.source_stage = vk::PipelineStageFlagBits::eTransfer;
            break;
        case vk::ImageLayout::eShaderReadOnlyOptimal:
            src_and_dst_info.source_access_flags = vk::AccessFlagBits::eShaderRead;
            src_and_dst_info.source_stage = shader_stage;
            break;
        case vk::ImageLayout::eGeneral:
            src_and_dst_info.source_access_flags = vk::AccessFlagBits::eShaderRead | vk::AccessFlagBits::eShaderWrite;
            src_and_dst_info.source_stage = shader_stage;
            break;
        default:
            throw std::runtime_error("Unrecognized source stage image layout transition");
        }

        switch (new_image_layout) {
        case vk::ImageLayout::eColorAttachmentOptimal:
            src_and_dst_info.destination_access_flags = vk::AccessFlagBits::eColorAttachmentWrite;
            src_and_dst_info.destination_stage = vk::PipelineStageFlagBits::eColorAttachmentOutput;
            break;
        case vk::ImageLayout::eDepthStencilAttachmentOptimal:
            src_and_dst_info.destination_access_flags = vk::AccessFlagBits::eDepthStencilAttachmentWrite;
            src_and_dst_info.destination_stage = vk::PipelineStageFlagBits::eLateFragmentTests;
            break;
        case vk::ImageLayout::eTransferSrcOptimal:
            src_and_dst_info.destination_access_flags = vk::AccessFlagBits::eTransferRead;
            src_and_dst_info.destination_stage = vk::PipelineStageFlagBits::eTransfer;
            break;
        case vk::ImageLayout::eTransferDstOptimal:
            src_and_dst_info.destination_access_flags = vk::AccessFlagBits::eTransferWrite;
            src_and_dst_info.destination_stage = vk::PipelineStageFlagBits::eTransfer;
            break;
        case vk::ImageLayout::eShaderReadOnlyOptimal:
            src_and_dst_info.destination_access_flags = vk::AccessFlagBits::eShaderRead;
            src_and_dst_info.destination_stage = shader_stage;
            break;
        default:
            throw std::runtime_error("Unrecognized destination stage image layout transition");
        }
    }

    return src_and_dst_info;
}


void aristaeus::gfx::vulkan::VmaImage::transition_image_layout(const vk::CommandBuffer &command_buffer, const ImageLayoutRequest &image_layout_request) {
    uint32_t working_starting_array_layer = image_layout_request.starting_array_layer.has_value() ? std::min(*image_layout_request.starting_array_layer, array_layers - 1) : 0;
    uint32_t working_starting_mip_levels = image_layout_request.starting_mip_level.has_value() ? std::min(*image_layout_request.starting_mip_level, mip_levels - 1) : 0;

    uint32_t working_array_layers = image_layout_request.array_layers.has_value() ? std::min(*image_layout_request.array_layers, array_layers - working_starting_array_layer) : array_layers;
    uint32_t working_mip_levels = image_layout_request.mip_levels.has_value() ? std::min(*image_layout_request.mip_levels, mip_levels - working_starting_mip_levels) : mip_levels;

    std::vector<vk::ImageMemoryBarrier> image_memory_barriers;
    vk::PipelineStageFlags source_stage{};
    vk::PipelineStageFlags destination_stage{};

    for (size_t i = 0; working_array_layers > i; ++i) {
        for (size_t i2 = 0; working_mip_levels > i2; ++i2) {
            uint32_t current_array_layer = working_starting_array_layer + i;
            uint32_t current_mip_level = working_starting_mip_levels + i2;

            vk::ImageLayout &current_image_layout = image_layouts[current_mip_level + (current_array_layer * mip_levels)];

            if (current_image_layout == image_layout_request.desired_layout) {
                continue;
            }

            SourceAndDestinationImageInfo src_and_dest_image_info = get_default_src_and_dest_stage(
                                                                                    current_image_layout,
                                                                                    image_layout_request.desired_layout,
                                                                                    image_layout_request.pipeline_type);

            if (image_layout_request.src_stage.has_value()) {
                src_and_dest_image_info.source_stage = *image_layout_request.src_stage;
            }
            if (image_layout_request.src_access_flags.has_value()) {
                src_and_dest_image_info.source_access_flags = *image_layout_request.src_access_flags;
            }

            if (image_layout_request.dest_stage.has_value()) {
                src_and_dest_image_info.destination_stage = *image_layout_request.dest_stage;
            }
            if (image_layout_request.dest_access_flags.has_value()) {
                src_and_dest_image_info.destination_access_flags = *image_layout_request.dest_access_flags;
            }

            vk::ImageSubresourceRange subresource_range{ vk::ImageAspectFlagBits::eColor,
                static_cast<uint32_t>(working_starting_mip_levels + i2), 1,
                static_cast<uint32_t>(working_starting_array_layer + i), 1 };

            // if is depth format
            if (is_depth_format()) {
                subresource_range.aspectMask = vk::ImageAspectFlagBits::eDepth | vk::ImageAspectFlagBits::eStencil;
            }

            source_stage |= src_and_dest_image_info.source_stage;
            destination_stage |= src_and_dest_image_info.destination_stage;

            image_memory_barriers.push_back(vk::ImageMemoryBarrier{src_and_dest_image_info.source_access_flags, src_and_dest_image_info.destination_access_flags,
                                                                   current_image_layout, image_layout_request.desired_layout, VK_QUEUE_FAMILY_IGNORED, VK_QUEUE_FAMILY_IGNORED,
                                                                   image, subresource_range});
        }
    }

    if (!image_memory_barriers.empty()) {
        command_buffer.pipelineBarrier(source_stage, destination_stage, {}, {}, {}, image_memory_barriers);

        for (uint32_t i = 0; working_array_layers > i; ++i) {
            for (uint32_t i2 = 0; working_mip_levels > i2; ++i2) {
                uint32_t current_array_layer = working_starting_array_layer + i;
                uint32_t current_mip_level = working_starting_mip_levels + i2;

                image_layouts[current_mip_level + (current_array_layer * mip_levels)] = image_layout_request.desired_layout;
            }
        }
    }
}

void aristaeus::gfx::vulkan::VmaImage::transition_image_layouts(const vk::CommandBuffer &command_buffer, const std::vector<ImageLayoutRequest> &image_layout_requests) {
    for (const ImageLayoutRequest &image_layout_request : image_layout_requests) {
        transition_image_layout(command_buffer, image_layout_request);
    }
}

void aristaeus::gfx::vulkan::VmaBufferDeleter::operator()(VmaBuffer* buffer) {
    if (buffer != nullptr) {
        vmaDestroyBuffer(buffer->allocator, buffer->buffer, buffer->allocation);

        #ifndef NDEBUG
        CoreRenderer::instance->get_memory_manager().register_buffer_free(*buffer);
        #endif
    }
}

void aristaeus::gfx::vulkan::VmaImageDeleter::operator()(VmaImage* image) {
    if (image != nullptr) {
        vmaDestroyImage(image->allocator, image->image, image->allocation);

        #ifndef NDEBUG
        CoreRenderer::instance->get_memory_manager().register_image_free(*image);
        #endif
    }
};
