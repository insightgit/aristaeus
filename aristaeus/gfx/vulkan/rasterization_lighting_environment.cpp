#include "assimp_model.hpp"
#include "core_renderer.hpp"
#include "sky_box.hpp"
#include "perlin_noise.hpp"
#include "directional_light.hpp"

#include "rasterization_lighting_environment.hpp"


const std::array<std::vector<vk::DescriptorSetLayoutBinding>, 2>
aristaeus::gfx::vulkan::RasterizationLightingEnvironment::BASE_GBUFFER_DESCRIPTOR_SET_LAYOUT_BINDINGS = { {
        // perspective camera environment set
        {PerspectiveCameraEnvironment::DESCRIPTOR_SET_BINDING},

        // material environment set
        {}
} };

const std::array<std::vector<vk::DescriptorSetLayoutBinding>, 2>
aristaeus::gfx::vulkan::RasterizationLightingEnvironment::COMPOSITION_DESCRIPTOR_SET_LAYOUT_BINDINGS = { {
        // perspective camera environment set
        {PerspectiveCameraEnvironment::DESCRIPTOR_SET_BINDING},

        // material environment set
        {get_composition_env_layout_bindings()},
} };

const std::array<std::vector<vk::DescriptorSetLayoutBinding>, 2>
aristaeus::gfx::vulkan::RasterizationLightingEnvironment::BASE_FORWARD_DESCRIPTOR_SET_BINDINGS = { {
    // perspective camera environment set
    {PerspectiveCameraEnvironment::DESCRIPTOR_SET_BINDING},

    // material environment set
    {DEFAULT_PBR_BINDINGS.begin(), DEFAULT_PBR_BINDINGS.end()},
} };

const std::array<std::vector<vk::DescriptorSetLayoutBinding>, 2> 
aristaeus::gfx::vulkan::RasterizationLightingEnvironment::BASE_SCREEN_SPACE_INTERPOLATION_SET_BINDINGS = { {
    {PerspectiveCameraEnvironment::DESCRIPTOR_SET_BINDING},

    // material environment set
    {{0, vk::DescriptorType::eInputAttachment, 1, vk::ShaderStageFlagBits::eFragment},
     {1, vk::DescriptorType::eInputAttachment, 1, vk::ShaderStageFlagBits::eFragment},
     {2, vk::DescriptorType::eCombinedImageSampler, 1, vk::ShaderStageFlagBits::eFragment},
     {3, vk::DescriptorType::eCombinedImageSampler, 1, vk::ShaderStageFlagBits::eFragment},
     {4, vk::DescriptorType::eCombinedImageSampler, 1, vk::ShaderStageFlagBits::eFragment},
     {5, vk::DescriptorType::eStorageBuffer, 1, vk::ShaderStageFlagBits::eFragment}},
}};

const vk::DescriptorSetLayoutBinding
aristaeus::gfx::vulkan::RasterizationLightingEnvironment::BASE_DEPTH_MAP_DESCRIPTOR_SET_BINDING{0, vk::DescriptorType::eUniformBuffer, 1, 
                                            vk::ShaderStageFlagBits::eVertex | vk::ShaderStageFlagBits::eFragment
                                            | vk::ShaderStageFlagBits::eTessellationEvaluation |
                                            vk::ShaderStageFlagBits::eTessellationControl |
                                            vk::ShaderStageFlagBits::eGeometry};

const std::array<vk::DescriptorSetLayoutBinding, 4> aristaeus::gfx::vulkan::RasterizationLightingEnvironment::SSAO_DESCRIPTOR_SET_BINDINGS{{
                                        { 0, vk::DescriptorType::eCombinedImageSampler, 1, vk::ShaderStageFlagBits::eFragment },
                                        { 1, vk::DescriptorType::eCombinedImageSampler, 1, vk::ShaderStageFlagBits::eFragment },
                                        { 2, vk::DescriptorType::eCombinedImageSampler, 1, vk::ShaderStageFlagBits::eFragment },
                                        { 3, vk::DescriptorType::eUniformBuffer, 1, vk::ShaderStageFlagBits::eFragment }  }};
const vk::DescriptorSetLayoutBinding aristaeus::gfx::vulkan::RasterizationLightingEnvironment::SSAO_BLUR_DESCRIPTOR_SET_BINDING{ 0, vk::DescriptorType::eCombinedImageSampler, 1, vk::ShaderStageFlagBits::eFragment };


const aristaeus::gfx::vulkan::GraphicsPipeline::PipelineInfo aristaeus::gfx::vulkan::RasterizationLightingEnvironment::DEFAULT_PIPELINE_INFO {
    .vertex_input_stage = {},
    .input_assembly_stage = {{}, vk::PrimitiveTopology::eTriangleList, VK_FALSE},
    .viewport_stage = {{}, 1, nullptr, 1, nullptr},
    .rasterization_stage = {{}, VK_FALSE, VK_FALSE, vk::PolygonMode::eFill, vk::CullModeFlagBits::eBack,
                            vk::FrontFace::eCounterClockwise, {}, {}, {}, {}, 1.0f},
    .multisample_stage = {{}, vk::SampleCountFlagBits::e1, VK_FALSE, 1.0f, nullptr, VK_FALSE, VK_FALSE},
    .depth_stencil_stage = {{}, VK_TRUE, VK_TRUE, vk::CompareOp::eLessOrEqual, VK_FALSE, VK_FALSE, {}, {}, 0.0f, 1.0f},
    .blending_stage = {{}, VK_FALSE, vk::LogicOp::eCopy, FORWARD_COLOR_BLEND_ATTACHMENT, {}},

    .dynamic_stage = {{}, {}}
};

[[nodiscard]] std::vector<aristaeus::gfx::vulkan::RenderPhase::AttachmentInfo> aristaeus::gfx::vulkan::RasterizationLightingEnvironment::get_gbuffer_attachment_info() {
    auto &core_renderer = utils::get_singleton_safe<CoreRenderer>();
    vk::Format swap_chain_format = core_renderer.get_swap_chain_format();
    vk::Format depth_format = core_renderer.get_depth_image_format();

    std::array<vk::AttachmentDescription, GBUFFER_SIZE + 1> attachments = { {
            // lighting/composition attachment
            {{}, swap_chain_format, vk::SampleCountFlagBits::e1, vk::AttachmentLoadOp::eClear,
             vk::AttachmentStoreOp::eStore, vk::AttachmentLoadOp::eDontCare, vk::AttachmentStoreOp::eDontCare,
             vk::ImageLayout::eUndefined, vk::ImageLayout::ePresentSrcKHR},
            // positions attachment
            {{}, vk::Format::eR16G16B16A16Sfloat, vk::SampleCountFlagBits::e1, vk::AttachmentLoadOp::eClear,
            vk::AttachmentStoreOp::eStore, vk::AttachmentLoadOp::eDontCare, vk::AttachmentStoreOp::eDontCare,
            vk::ImageLayout::eUndefined, vk::ImageLayout::eShaderReadOnlyOptimal},
            // normals attachment
            {{}, vk::Format::eR16G16B16A16Sfloat, vk::SampleCountFlagBits::e1, vk::AttachmentLoadOp::eClear,
            vk::AttachmentStoreOp::eStore, vk::AttachmentLoadOp::eDontCare, vk::AttachmentStoreOp::eDontCare,
            vk::ImageLayout::eUndefined, vk::ImageLayout::eShaderReadOnlyOptimal},
            // structure buffer attachment
            {{}, vk::Format::eR16G16B16A16Sfloat, vk::SampleCountFlagBits::e1, vk::AttachmentLoadOp::eClear,
            vk::AttachmentStoreOp::eStore, vk::AttachmentLoadOp::eDontCare, vk::AttachmentStoreOp::eDontCare,
            vk::ImageLayout::eUndefined, vk::ImageLayout::eShaderReadOnlyOptimal},
            // albedo attachment
            {{}, vk::Format::eR8G8B8A8Unorm, vk::SampleCountFlagBits::e1, vk::AttachmentLoadOp::eClear,
            vk::AttachmentStoreOp::eStore, vk::AttachmentLoadOp::eDontCare, vk::AttachmentStoreOp::eDontCare,
            vk::ImageLayout::eUndefined, vk::ImageLayout::eShaderReadOnlyOptimal},
            // roughness/metallic/ao attachment
            {{}, vk::Format::eR16G16B16A16Sfloat, vk::SampleCountFlagBits::e1, vk::AttachmentLoadOp::eClear,
             vk::AttachmentStoreOp::eStore, vk::AttachmentLoadOp::eDontCare, vk::AttachmentStoreOp::eDontCare,
             vk::ImageLayout::eUndefined, vk::ImageLayout::eShaderReadOnlyOptimal},
            // depth/stencil attachment
            {{}, depth_format, vk::SampleCountFlagBits::e1, vk::AttachmentLoadOp::eClear,
            vk::AttachmentStoreOp::eStore, vk::AttachmentLoadOp::eClear, vk::AttachmentStoreOp::eDontCare,
            vk::ImageLayout::eUndefined, vk::ImageLayout::eDepthStencilAttachmentOptimal}
        } };

    std::vector<RenderPhase::AttachmentInfo> attachment_info;

    for (int i = 0; attachments.size() > i; ++i) {
        vk::ImageUsageFlags usage_flags = attachments[i].format == depth_format ? vk::ImageUsageFlagBits::eDepthStencilAttachment : vk::ImageUsageFlagBits::eColorAttachment;

        usage_flags |= vk::ImageUsageFlagBits::eInputAttachment;

        // TODO(Bobby): specialize thhis based on actual usages of each attachment for each render pass
        // TODO(Bobby): not as a blanket usage flag
        usage_flags |= vk::ImageUsageFlagBits::eSampled | vk::ImageUsageFlagBits::eTransferSrc | vk::ImageUsageFlagBits::eTransferDst;

        attachment_info.push_back({ attachments[i], usage_flags });
    }

    return attachment_info;
}

std::function<void(void*)> aristaeus::gfx::vulkan::RasterizationLightingEnvironment::get_full_render_function(
                                                                                        const std::unordered_set<RasterizationSubpass> &subpasses_to_enable) {
    return [&, subpasses_to_enable](void*) {
        auto &core_renderer = utils::get_singleton_safe<CoreRenderer>();
        vk::CommandBuffer command_buffer =
            core_renderer.get_graphics_command_buffer_manager().get_primary_command_buffer(core_renderer.get_current_frame());

        if (!subpasses_to_enable.empty()) {
            m_subpasses_enabled = subpasses_to_enable;
        }

        m_quads_to_compose.clear();
        m_quads_to_compose.push_back(QuadRenderPushConstants{ .quad_start = {0, 0}, .quad_extents = {1, 1} });

        core_renderer.get_perspective_camera_environment().draw(command_buffer, core_renderer, core_renderer.get_logical_device_ref());
    };
}

void aristaeus::gfx::vulkan::RasterizationLightingEnvironment::create_subpass_descriptions() {
    auto &core_renderer = utils::get_singleton_safe<CoreRenderer>();
    std::vector<RenderPhase::AttachmentInfo> gbuffer_attachments = get_gbuffer_attachment_info();

    assert(gbuffer_attachments[0].description.format == core_renderer.get_swap_chain_format());

    for (size_t i = 1; gbuffer_attachments.size() > i; ++i) {
        if (gbuffer_attachments[i].usage_flags & vk::ImageUsageFlagBits::eDepthStencilAttachment) {
            m_depth_gbuffer_compositionless_attachment = vk::AttachmentReference{ static_cast<uint32_t>(i - 1), vk::ImageLayout::eDepthStencilAttachmentOptimal };
        }
        else {
            vk::AttachmentReference attachment_ref{ static_cast<uint32_t>(i - 1), vk::ImageLayout::eColorAttachmentOptimal };

            m_color_gbuffer_attachments.push_back(attachment_ref);

            ++attachment_ref.attachment;
            attachment_ref.layout = vk::ImageLayout::eShaderReadOnlyOptimal;

            m_color_gbuffer_composition_input_attachments.push_back(attachment_ref);
        }
    }

    m_screen_space_interp_input_attachments.push_back({ POSITION_GBUFFER_ATTACHMENT_INDEX, vk::ImageLayout::eShaderReadOnlyOptimal });
    m_screen_space_interp_input_attachments.push_back({ NORMALS_GBUFFER_ATTACHMENT_INDEX, vk::ImageLayout::eShaderReadOnlyOptimal });

    m_ssao_blur_subpass = { {}, vk::PipelineBindPoint::eGraphics, {}, SSAO_BLUR_ATTACHMENT, {}, {} };
    m_deferred_subpass = { {}, vk::PipelineBindPoint::eGraphics, {}, m_color_gbuffer_attachments, {}, &m_depth_gbuffer_compositionless_attachment };
    m_composition_subpass = { {}, vk::PipelineBindPoint::eGraphics, SSAO_BLUR_INPUT_ATTACHMENT, COMPOSITION_ATTACHMENT, {}, {}};
    
    m_forward_subpass = { {}, vk::PipelineBindPoint::eGraphics, DEFERRED_POSITION_DEPTH_ATTACHMENT, COMPOSITION_ATTACHMENT, {}, &FORWARD_DEPTH_ATTACHMENT };
    m_screen_space_interp_subpass = { {}, vk::PipelineBindPoint::eGraphics, m_screen_space_interp_input_attachments, COMPOSITION_ATTACHMENT, {}, {} };
}

aristaeus::gfx::vulkan::RenderPhase &aristaeus::gfx::vulkan::RasterizationLightingEnvironment::create_ssao_render_phase() {
    auto &core_renderer = utils::get_singleton_safe<CoreRenderer>();

    RenderPhase::AttachmentInfo attachment_info{
        .description = {{}, core_renderer.get_swap_chain_format(), vk::SampleCountFlagBits::e1, vk::AttachmentLoadOp::eClear,
                        vk::AttachmentStoreOp::eStore, vk::AttachmentLoadOp::eDontCare, vk::AttachmentStoreOp::eDontCare,
                        vk::ImageLayout::eUndefined, vk::ImageLayout::eShaderReadOnlyOptimal},
        .usage_flags = vk::ImageUsageFlagBits::eSampled | vk::ImageUsageFlagBits::eInputAttachment | vk::ImageUsageFlagBits::eColorAttachment
    };

    RenderPhase::RenderPassInfo ssao_pass {
        .before_function = {},
        .clear_values = {{std::array<float, 4>{0.0f, 0.0f, 0.0f, 0.0f}}},
        .contextless_draw_function = [&](void *) {
            CoreRenderer &core_renderer = utils::get_singleton_safe<CoreRenderer>();

            m_subpasses_enabled = {RasterizationSubpass::SSAO};

            core_renderer.get_perspective_camera_environment().predraw();
        },
        .draw_function = get_full_render_function({RasterizationSubpass::SSAO}),
        .draw_function_arg = nullptr,
        .name = std::string{SSAO_RENDER_PASS_NAME},
        .one_shot = false,
        .phase_dependents = {},
        .phase_dependencies = std::unordered_map<std::string, vk::PipelineStageFlags>{{std::string{DEFERRED_RENDER_PHASE_NAME}, 
                                                                                      vk::PipelineStageFlagBits::eAllGraphics}},
        .render_pass_attachments = {attachment_info},
        .resolution = core_renderer.get_swap_chain_extent_uvec2(),
        .num_of_render_contexts = 1
    };

    return core_renderer.add_render_pass(ssao_pass, true, std::string{ SSAO_RENDER_PHASE_NAME });
}

aristaeus::gfx::vulkan::RenderPhase &aristaeus::gfx::vulkan::RasterizationLightingEnvironment::create_ssao_blur_composition_render_phase() {
    auto &core_renderer = utils::get_singleton_safe<CoreRenderer>();

    vk::PipelineStageFlags depth_stage_flags = vk::PipelineStageFlagBits::eEarlyFragmentTests | vk::PipelineStageFlagBits::eLateFragmentTests;

    vk::SubpassDependency ssao_blur_color_dep{ VK_SUBPASS_EXTERNAL, 0, vk::PipelineStageFlagBits::eColorAttachmentOutput,
                                               vk::PipelineStageFlagBits::eColorAttachmentOutput, {}, vk::AccessFlagBits::eColorAttachmentWrite };
    vk::SubpassDependency starting_depth_dep{ VK_SUBPASS_EXTERNAL, 1, depth_stage_flags, depth_stage_flags, {},
                                              vk::AccessFlagBits::eDepthStencilAttachmentWrite };

    vk::SubpassDependency ssao_subpass_to_composition_dep{ 0, 1, vk::PipelineStageFlagBits::eColorAttachmentOutput,
                                                               vk::PipelineStageFlagBits::eFragmentShader, vk::AccessFlagBits::eColorAttachmentWrite,
                                                               vk::AccessFlagBits::eInputAttachmentRead, vk::DependencyFlagBits::eByRegion };
    vk::SubpassDependency composition_subpass_to_forward_dep = ssao_subpass_to_composition_dep;

    composition_subpass_to_forward_dep.srcSubpass = 1;
    composition_subpass_to_forward_dep.dstSubpass = 2;

    vk::SubpassDependency forward_dep{ 2, VK_SUBPASS_EXTERNAL, vk::PipelineStageFlagBits::eColorAttachmentOutput, vk::PipelineStageFlagBits::eBottomOfPipe,
                                      vk::AccessFlagBits::eColorAttachmentRead | vk::AccessFlagBits::eColorAttachmentWrite, vk::AccessFlagBits::eMemoryRead,
                                      vk::DependencyFlagBits::eByRegion };

    std::unordered_set<RasterizationSubpass> subpasses_enabled = { RasterizationSubpass::SSAO_BLUR, RasterizationSubpass::COMPOSITION, RasterizationSubpass::FORWARD };

    std::vector<vk::SubpassDependency> subpass_dependencies{ ssao_blur_color_dep, starting_depth_dep, ssao_subpass_to_composition_dep, 
                                                             composition_subpass_to_forward_dep, forward_dep };
    std::vector<vk::SubpassDescription> subpasses{ m_ssao_blur_subpass, m_composition_subpass, m_forward_subpass };

    static const std::vector<vk::ClearValue> clear_values = {
        {{std::array<float, 4>{0.0f, 0.0f, 0.0f, 0.0f}}},
        {{std::array<float, 4>{0.0f, 0.0f, 0.0f, 0.0f}}},
        {{1.0f, 0}}
    };

    vk::Format depth_format = core_renderer.get_depth_image_format();
    std::vector<RenderPhase::AttachmentInfo> ssao_composition_attachments_infos;
    std::array<vk::AttachmentDescription, 3> ssao_composition_attachments{{
        {{}, core_renderer.get_swap_chain_format(), vk::SampleCountFlagBits::e1, vk::AttachmentLoadOp::eClear,
         vk::AttachmentStoreOp::eStore, vk::AttachmentLoadOp::eDontCare, vk::AttachmentStoreOp::eDontCare,
         vk::ImageLayout::eUndefined, vk::ImageLayout::ePresentSrcKHR},
        {{}, vk::Format::eR16Sfloat, vk::SampleCountFlagBits::e1, vk::AttachmentLoadOp::eClear,
         vk::AttachmentStoreOp::eStore, vk::AttachmentLoadOp::eDontCare, vk::AttachmentStoreOp::eDontCare,
         vk::ImageLayout::eUndefined, vk::ImageLayout::eColorAttachmentOptimal},
        {{}, depth_format, vk::SampleCountFlagBits::e1, vk::AttachmentLoadOp::eLoad,
         vk::AttachmentStoreOp::eStore, vk::AttachmentLoadOp::eClear, vk::AttachmentStoreOp::eDontCare,
         vk::ImageLayout::eDepthStencilAttachmentOptimal, vk::ImageLayout::eDepthStencilAttachmentOptimal}
    }};

    for (int i = 0; ssao_composition_attachments.size() > i; ++i) {
        bool is_depth_attachment = ssao_composition_attachments[i].format == depth_format;
        vk::ImageUsageFlags usage_flags = is_depth_attachment ? vk::ImageUsageFlagBits::eDepthStencilAttachment :
                                                                vk::ImageUsageFlagBits::eColorAttachment;

        // if we arent using the swap chain image
        if (i != 0) {
            usage_flags |= vk::ImageUsageFlagBits::eInputAttachment;
        }

        // TODO(Bobby): specialize this based on actual usages of each attachment for each render pass
        // TODO(Bobby): not as a blanket usage flag
        usage_flags |= vk::ImageUsageFlagBits::eSampled | vk::ImageUsageFlagBits::eTransferSrc | vk::ImageUsageFlagBits::eTransferDst;

        ssao_composition_attachments_infos.push_back({ ssao_composition_attachments[i], usage_flags });

        if (is_depth_attachment) {
            for (int i2 = 0; core_renderer.get_swap_chain_images().size() > i2; ++i2) {
                uint32_t frame_number = i2 % utils::vulkan::MAX_FRAMES_IN_FLIGHT;

                uint32_t depth_buffer_index = (frame_number * GBUFFER_SIZE) + (GBUFFER_SIZE - 1);
                SharedImageRaii &past_depth_buffer = m_gbuffer_render_phase.get_render_pass_image(std::string{DEFERRED_RENDER_PASS_NAME}, 
                                                                                                  depth_buffer_index);

                assert(past_depth_buffer->format == depth_format);

                ssao_composition_attachments_infos.back().existing_image.push_back(past_depth_buffer);
            }
        }
    }
    
    RenderPhase::RenderPassInfo ssao_composition_pass{
        .before_function = {},
        .clear_values = clear_values,
        .contextless_draw_function = [&, subpasses_enabled](void *) {
            CoreRenderer &core_renderer = utils::get_singleton_safe<CoreRenderer>();

            m_subpasses_enabled = subpasses_enabled;

            core_renderer.get_perspective_camera_environment().predraw();
        },
        .draw_function = get_full_render_function(subpasses_enabled),
        .draw_function_arg = nullptr,
        .name = std::string{SSAO_BLUR_COMPOSE_RENDER_PASS_NAME},
        .one_shot = false,
        .phase_dependents = {},
        .phase_dependencies = std::unordered_map<std::string, vk::PipelineStageFlags>{{std::string{SSAO_RENDER_PHASE_NAME}, 
                                                                                      vk::PipelineStageFlagBits::eAllGraphics}},
        .render_pass_attachments = ssao_composition_attachments_infos,
        .resolution = core_renderer.get_swap_chain_extent_uvec2(),
        .subpass_info = RenderPhase::SubpassInfo{.descriptions = subpasses, .dependencies = subpass_dependencies},
        .swap_chain_images = core_renderer.get_swap_chain_images(),
        .num_of_render_contexts = 1
    };

    return core_renderer.add_render_pass(ssao_composition_pass, true, std::string{ SSAO_BLUR_COMPOSE_RENDER_PHASE_NAME });
}

aristaeus::gfx::vulkan::RenderPhase &aristaeus::gfx::vulkan::RasterizationLightingEnvironment::create_gbuffer_render_phase(
                                                                                            const glm::uvec2 &resolution, 
                                                                                            const std::function<void(void*)> &draw_function,
                                                                                            const std::optional<std::string> &new_render_phase_name,
                                                                                            const std::unordered_map<std::string, vk::PipelineStageFlags> &dependencies,
                                                                                            const std::function<void(void*)> &before_function) {
    auto &core_renderer = utils::get_singleton_safe<CoreRenderer>();

    static const std::vector<vk::ClearValue> clear_values = {
        {{std::array<float, 4>{0.0f, 0.0f, 0.0f, 0.0f}}},
        {{std::array<float, 4>{0.0f, 0.0f, 0.0f, 0.0f}}},
        {{std::array<float, 4>{0.0f, 0.0f, 0.0f, 0.0f}}},
        {{std::array<float, 4>{0.0f, 0.0f, 0.0f, 0.0f}}},
        {{std::array<float, 4>{0.0f, 0.0f, 0.0f, 0.0f}}},
        {{1.0f, 0}}
    };

    if (!m_subpasses_created) {
        create_subpass_descriptions();
        m_subpasses_created = true;
    }

    vk::PipelineStageFlags depth_stage_flags = vk::PipelineStageFlagBits::eEarlyFragmentTests | vk::PipelineStageFlagBits::eLateFragmentTests;

    vk::SubpassDependency deferred_color_dep{ VK_SUBPASS_EXTERNAL, 0, vk::PipelineStageFlagBits::eColorAttachmentOutput,
                                              vk::PipelineStageFlagBits::eColorAttachmentOutput, {}, vk::AccessFlagBits::eColorAttachmentWrite};
    vk::SubpassDependency deferred_depth_dep{VK_SUBPASS_EXTERNAL, 0, depth_stage_flags, depth_stage_flags, {},
                                             vk::AccessFlagBits::eDepthStencilAttachmentWrite};

    vk::SubpassDependency deferred_end_dep{0, VK_SUBPASS_EXTERNAL, vk::PipelineStageFlagBits::eColorAttachmentOutput, vk::PipelineStageFlagBits::eBottomOfPipe, 
                                           vk::AccessFlagBits::eColorAttachmentRead | vk::AccessFlagBits::eColorAttachmentWrite, vk::AccessFlagBits::eMemoryRead, 
                                           vk::DependencyFlagBits::eByRegion};

    std::vector<vk::SubpassDependency> deferred_subpass_dependencies{ deferred_color_dep, deferred_depth_dep, deferred_end_dep };
    std::vector<vk::SubpassDescription> subpasses{ m_deferred_subpass };
    
    std::vector<RenderPhase::AttachmentInfo> attachment_infos = get_gbuffer_attachment_info();

    attachment_infos.erase(attachment_infos.begin());

    RenderPhase::RenderPassInfo deferred_pass{
        .before_function = before_function,
        .clear_values = clear_values,
        .contextless_draw_function = [&](void *) {
            CoreRenderer &core_renderer = utils::get_singleton_safe<CoreRenderer>();

            m_subpasses_enabled = { RasterizationSubpass::DEFERRED };

            core_renderer.get_perspective_camera_environment().predraw();
        },
        .draw_function = draw_function,
        .draw_function_arg = nullptr,
        .name = std::string{DEFERRED_RENDER_PASS_NAME},
        .one_shot = false,
        .phase_dependents = {},
        .phase_dependencies = dependencies,
        .render_pass_attachments = attachment_infos,
        .resolution = resolution,
        .subpass_info = RenderPhase::SubpassInfo{.descriptions = subpasses,
                                                 .dependencies = deferred_subpass_dependencies},
        .num_of_render_contexts = 1
    };

    return core_renderer.add_render_pass(deferred_pass, new_render_phase_name.has_value(), new_render_phase_name.has_value() ? *new_render_phase_name : "");
}

void aristaeus::gfx::vulkan::RasterizationLightingEnvironment::partial_occlusion_based_render_function(void*) {
    assert(m_occlusion_result_buffer != nullptr);

    auto &core_renderer = utils::get_singleton_safe<CoreRenderer>();
    uint32_t current_frame = core_renderer.get_current_frame();
    uint32_t past_frame = current_frame;

    assert(core_renderer.get_current_render_phase() != nullptr);

    if (past_frame == 0) {
        past_frame = utils::vulkan::MAX_FRAMES_IN_FLIGHT - 1;
    }
    else {
        past_frame -= 1;
    }

    void *buffer_data;

    if (core_renderer.get_total_frames_rendered() != 0) {
        vmaMapMemory(m_occlusion_result_buffer->allocator, m_occlusion_result_buffer->allocation, &buffer_data);
        int query_offset = past_frame * m_screen_space_interp_grid_size.x * m_screen_space_interp_grid_size.y;

        uint32_t *buffer_data_int = &reinterpret_cast<uint32_t*>(buffer_data)[query_offset];

        // reset occlusion result data buffer back to all fragments passing
        for (int i = 0; m_screen_space_interp_grid_size.x * m_screen_space_interp_grid_size.y > i; ++i) {
            m_occlusion_result_data[query_offset + i] = buffer_data_int[i];
            buffer_data_int[i] = 1;
        }

        vmaUnmapMemory(m_occlusion_result_buffer->allocator, m_occlusion_result_buffer->allocation);
    }
    else {
        assert(current_frame == 0);

        vmaMapMemory(m_occlusion_result_buffer->allocator, m_occlusion_result_buffer->allocation, &buffer_data);

        // initialize the buffer by zeroing it out
        std::memset(buffer_data, 0, m_occlusion_result_buffer->buffer_size);

        vmaUnmapMemory(m_occlusion_result_buffer->allocator, m_occlusion_result_buffer->allocation);
    }

    glm::vec2 grid_unit{1.0f / m_screen_space_interp_grid_size.x, 1.0f / m_screen_space_interp_grid_size.y };
    QuadRenderPushConstants quad_push_constants{ .quad_extents = grid_unit };

    vk::Viewport current_viewport = m_final_high_res_render_phase->get_current_viewport();
    int fragment_num = (current_viewport.width / m_screen_space_interp_grid_size.x) * (current_viewport.height / m_screen_space_interp_grid_size.x);

    m_subpasses_enabled = { RasterizationSubpass::COMPOSITION, RasterizationSubpass::FORWARD };

    m_quads_to_compose.clear();

    std::vector<int> quads_not_compose;

    int query_offset = past_frame * m_screen_space_interp_grid_size.x * m_screen_space_interp_grid_size.y;

    glm::vec3 current_camera_position = core_renderer.get_perspective_camera_environment().get_camera_position();

    for (int y = 0; m_screen_space_interp_grid_size.y > y; ++y) {
        for (int x = 0; m_screen_space_interp_grid_size.x > x; ++x) {
            int occlusion_query_index = query_offset + (y * m_screen_space_interp_grid_size.x) + x;

            if (m_occlusion_result_data[occlusion_query_index] == 0 || glm::length(current_camera_position - m_past_camera_position) > 0.1) {
                // we have some fragments which couldn't be screen space interpolated...
                quad_push_constants.quad_start = glm::vec2{ x * grid_unit.x,
                                                            y * grid_unit.y };
                m_quads_to_compose.push_back(quad_push_constants);
            }
            else {
                quads_not_compose.push_back((y * m_screen_space_interp_grid_size.x) + x);
            }
        }
    }

    m_past_camera_position = current_camera_position;

    //  render composition if any part needed re rendering, and then render forward
    vk::CommandBuffer command_buffer =
        core_renderer.get_graphics_command_buffer_manager().get_primary_command_buffer(core_renderer.get_current_frame());

    core_renderer.get_perspective_camera_environment().draw(command_buffer, core_renderer, core_renderer.get_logical_device_ref());
}

void aristaeus::gfx::vulkan::RasterizationLightingEnvironment::create_screen_space_interp_render_phases() {
    /*auto &core_renderer = utils::get_singleton_safe<CoreRenderer>();

    static const std::vector<vk::ClearValue> OCCLUSION_QUERY_CLEAR_VALS {
        {{std::array<float, 4>{0.0f, 0.0f, 0.0f, 0.0f}}},
        {{1.0f, 0}}
    };

    uint32_t num_of_queries = m_screen_space_interp_grid_size.x * m_screen_space_interp_grid_size.y;

    vk::PipelineStageFlags depth_stage_flags = vk::PipelineStageFlagBits::eEarlyFragmentTests | vk::PipelineStageFlagBits::eLateFragmentTests;


    // deferred -> occlude subpass dependencies
    vk::SubpassDependency deferred_color_dep{ VK_SUBPASS_EXTERNAL, 0, vk::PipelineStageFlagBits::eColorAttachmentOutput,
                                              vk::PipelineStageFlagBits::eColorAttachmentOutput, {}, vk::AccessFlagBits::eColorAttachmentWrite };
    vk::SubpassDependency deferred_depth_dep{ VK_SUBPASS_EXTERNAL, 0, depth_stage_flags, depth_stage_flags, {},
                                             vk::AccessFlagBits::eDepthStencilAttachmentWrite };

    vk::SubpassDependency deferred_subpass_to_occlude_dep{ 0, 1, vk::PipelineStageFlagBits::eColorAttachmentOutput,
                                                               vk::PipelineStageFlagBits::eFragmentShader, vk::AccessFlagBits::eColorAttachmentWrite,
                                                               vk::AccessFlagBits::eInputAttachmentRead, vk::DependencyFlagBits::eByRegion };
    vk::SubpassDependency occlude_dep{ 1, VK_SUBPASS_EXTERNAL, vk::PipelineStageFlagBits::eColorAttachmentOutput, vk::PipelineStageFlagBits::eBottomOfPipe,
                                      vk::AccessFlagBits::eColorAttachmentRead | vk::AccessFlagBits::eColorAttachmentWrite, vk::AccessFlagBits::eMemoryRead,
                                      vk::DependencyFlagBits::eByRegion };

    std::vector<vk::SubpassDependency> occlusion_query_dependencies{ deferred_color_dep, deferred_depth_dep, deferred_subpass_to_occlude_dep, occlude_dep };

    std::vector<RenderPhase::AttachmentInfo> occlusion_query_attachment_infos = get_gbuffer_attachment_info();

    occlusion_query_attachment_infos[0].description.finalLayout = vk::ImageLayout::eColorAttachmentOptimal;
    for (int i = 1; occlusion_query_attachment_infos.size() > i; ++i) {
        if (occlusion_query_attachment_infos[i].description.finalLayout == vk::ImageLayout::eColorAttachmentOptimal) {
            occlusion_query_attachment_infos[i].description.finalLayout = vk::ImageLayout::eShaderReadOnlyOptimal;
        }
    }

    RenderPhase::RenderPassInfo occlusion_query_render_pass_info {
        .clear_values = OCCLUSION_QUERY_CLEAR_VALS,
        .contextless_draw_function = [&](void *) {
            CoreRenderer &core_renderer = utils::get_singleton_safe<CoreRenderer>();

            m_subpasses_enabled = { RasterizationSubpass::DEFERRED, RasterizationSubpass::SCREEN_SPACE_INTERP };

            core_renderer.get_perspective_camera_environment().predraw();
        },
        .draw_function = [&](void *arg) {
            m_subpasses_enabled = { RasterizationSubpass::DEFERRED, RasterizationSubpass::SCREEN_SPACE_INTERP };

            get_full_render_function(false)(arg);
        },
        .name = std::string{OCCLUSION_QUERY_RENDER_PASS_NAME},
        .one_shot = false,
        .phase_dependents = {},
        .phase_dependencies = {{m_render_phase.get_render_phase_name(), vk::PipelineStageFlagBits::eAllGraphics}},
        .render_pass_attachments = occlusion_query_attachment_infos,
        .resolution = {},
        .subpass_info = RenderPhase::SubpassInfo{.descriptions = {m_deferred_subpass, m_screen_space_interp_subpass},
                                                 .dependencies = occlusion_query_dependencies},
        .swap_chain_images = core_renderer.get_swap_chain_images(),
        .num_of_render_contexts = 1
    };
    vk::QueryPoolCreateInfo query_poll_create_info{ {}, vk::QueryType::eOcclusion, num_of_queries};

    vk::Extent2D swap_chain_extent = core_renderer.get_swap_chain_extent();

    occlusion_query_render_pass_info.render_pass_attachments[6].description.storeOp = vk::AttachmentStoreOp::eStore;

    m_occlusion_query_render_phase = &core_renderer.add_render_pass(occlusion_query_render_pass_info, true, 
                                                                    std::string{ OCCLUSION_QUERY_RENDER_PHASE_NAME });

    uint32_t occlusion_num_of_frames = m_occlusion_query_render_phase->get_render_pass_frame_num(std::string{OCCLUSION_QUERY_RENDER_PASS_NAME});

    m_occlusion_result_data.resize(utils::vulkan::MAX_FRAMES_IN_FLIGHT * m_screen_space_interp_grid_size.x * m_screen_space_interp_grid_size.y);
    std::memset(m_occlusion_result_data.data(), 0, sizeof(uint32_t) * m_occlusion_result_data.size());

    for (int y = 0; m_screen_space_interp_grid_size.y > y; ++y) {
        for (int x = 0; m_screen_space_interp_grid_size.x > x; ++x) {
            m_occlusion_query_quad_indices.push_back((y * m_screen_space_interp_grid_size.x) + x);
        }
    }
    
    std::vector<RenderPhase::AttachmentInfo> final_high_res_attachment_infos = occlusion_query_attachment_infos;

    final_high_res_attachment_infos[0].description.loadOp = vk::AttachmentLoadOp::eLoad;
    final_high_res_attachment_infos[0].description.initialLayout = vk::ImageLayout::eColorAttachmentOptimal;
    final_high_res_attachment_infos[0].description.finalLayout = vk::ImageLayout::ePresentSrcKHR;

    for (int i = 0; core_renderer.get_swap_chain_images().size() > i; ++i) {
        // final_high_res_attachment_infos[0] will be all accounted for as a swap chain image
        for (int i2 = 0; final_high_res_attachment_infos.size() - 1 > i2; ++i2) {
            SharedImageRaii &existing_image = m_occlusion_query_render_phase->get_render_pass_image(occlusion_query_render_pass_info.name,
                                                                                                    ((GBUFFER_SIZE - 1) * i) + i2);

            final_high_res_attachment_infos[i2 + 1].description.loadOp = vk::AttachmentLoadOp::eLoad;
            final_high_res_attachment_infos[i2 + 1].description.initialLayout = 
                existing_image->is_depth_format() ? vk::ImageLayout::eDepthStencilAttachmentOptimal : vk::ImageLayout::eShaderReadOnlyOptimal;

            // GBUFFER_SIZE - 1 to account for not copying swap chain image
            final_high_res_attachment_infos[i2 + 1].existing_image.push_back(existing_image);
        }
    }
    
    m_final_high_res_render_phase = &create_render_phase(glm::uvec2{ swap_chain_extent.width, swap_chain_extent.height }, 
                                                         [&](void* ptr) {partial_occlusion_based_render_function(ptr); },
                                                         std::string{FINAL_RE_RENDER_RENDER_PHASE_NAME}, 
                                                         {{m_occlusion_query_render_phase->get_render_phase_name(), vk::PipelineStageFlagBits::eAllGraphics}}, false, true,
                                                         [&](void *ptr) {}, final_high_res_attachment_infos);

    // 4 for the minimum size of one bool/int per each screen quad
    std::string query_render_pass_name{OCCLUSION_QUERY_RENDER_PASS_NAME};
    uint32_t num_of_frames = m_occlusion_query_render_phase->get_render_pass_frame_num(query_render_pass_name);
    uint32_t occlusion_result_size = (4 * m_screen_space_interp_grid_size.x * m_screen_space_interp_grid_size.y) * num_of_frames;
    vk::BufferUsageFlags occlusion_buffer_usage_flags = vk::BufferUsageFlagBits::eStorageBuffer | vk::BufferUsageFlagBits::eTransferSrc;
    m_occlusion_result_buffer = core_renderer.get_memory_manager().create_buffer(core_renderer, occlusion_result_size, 
                                                                                 occlusion_buffer_usage_flags, vk::SharingMode::eExclusive, 
                                                                                 VMA_ALLOCATION_CREATE_HOST_ACCESS_RANDOM_BIT, VMA_MEMORY_USAGE_AUTO,
                                                                                 "occlusion_result_buffer");*/
}

aristaeus::gfx::vulkan::RasterizationLightingEnvironment::GraphicsPipelineMaterialInfo aristaeus::gfx::vulkan::RasterizationLightingEnvironment::create_gbuffer_pipeline(vk::raii::RenderPass &render_pass) {
    std::vector<GraphicsPipeline::ShaderInfo> shader_infos{ {.code = utils::read_file("environments/vulkan_environment_shaders/engine/environment_shader.vert.spv"), .name = "environment_shader.vert", .stage = vk::ShaderStageFlagBits::eVertex},
                                                            {.code = utils::read_file("environments/vulkan_environment_shaders/engine/environment_shader_gbuffer.frag.spv"), .name = "environment_shader_gbuffer.frag",
                                                             .stage = vk::ShaderStageFlagBits::eFragment}};

    std::vector<RenderPhase::AttachmentInfo> gbuffer_attachments = get_gbuffer_attachment_info();
    std::array<std::vector<vk::DescriptorSetLayoutBinding>, 2> base_gbuffer_descriptor_set_bindings = BASE_GBUFFER_DESCRIPTOR_SET_LAYOUT_BINDINGS;
    std::string pipeline_cache_suffix;

    auto &core_renderer = utils::get_singleton_safe<CoreRenderer>();

    m_blend_attachment_states.clear();

    for (int i = 0; gbuffer_attachments.size() > i; ++i) {
        if (i != 0 && gbuffer_attachments[i].description.format != core_renderer.get_depth_image_format()) {
            vk::PipelineColorBlendAttachmentState state{ VK_FALSE, vk::BlendFactor::eSrcColor, {}, {}, vk::BlendFactor::eSrcAlpha, {}, {}, 
                                                         vk::ColorComponentFlagBits::eR | vk::ColorComponentFlagBits::eG | vk::ColorComponentFlagBits::eB |
                                                         vk::ColorComponentFlagBits::eA };

            m_blend_attachment_states.push_back(state);
        }
    }

    GraphicsPipeline::PipelineInfo pipeline_info {
            .vertex_input_stage = { {}, gfx::vulkan::AssimpModel::MODEL_BINDING_DESCRIPTION, gfx::vulkan::AssimpModel::MODEL_ATTRIBUTE_DESCRIPTIONS },
            .input_assembly_stage = {{}, vk::PrimitiveTopology::eTriangleList, VK_FALSE},
            .viewport_stage = {{}, 1, nullptr, 1, nullptr},
            .rasterization_stage = {{}, VK_FALSE, VK_FALSE, vk::PolygonMode::eFill, vk::CullModeFlagBits::eBack,
                                    vk::FrontFace::eCounterClockwise, {}, {}, {}, {}, 1.0f},
            .multisample_stage = {{}, vk::SampleCountFlagBits::e1, VK_FALSE, 1.0f, nullptr, VK_FALSE, VK_FALSE},
            .depth_stencil_stage = {{}, VK_TRUE, VK_TRUE, vk::CompareOp::eLessOrEqual, VK_FALSE, VK_FALSE, {}, {}, 0.0f, 1.0f},
            .blending_stage = {{}, VK_FALSE, vk::LogicOp::eCopy, m_blend_attachment_states, {}},

            .dynamic_stage = {{}, {}},
            .push_content_ranges = {},

            .descriptor_bindings = {base_gbuffer_descriptor_set_bindings.cbegin(), base_gbuffer_descriptor_set_bindings.cend()},

            .sub_pass = 0,
            .pipeline_cache_path = std::string("gbuffer_pipeline_cache"),
            .render_pass = render_pass
    };

    return GraphicsPipelineMaterialInfo{
        .pipeline_info = pipeline_info,
        .shader_infos = shader_infos
    };
}

aristaeus::gfx::vulkan::GraphicsPipeline aristaeus::gfx::vulkan::RasterizationLightingEnvironment::create_composition_pipeline(vk::raii::RenderPass &render_pass, uint32_t subpass_num, bool start_constructing_pipeline) {
    std::vector<GraphicsPipeline::ShaderInfo> shader_infos{ {.code = utils::read_file("environments/vulkan_environment_shaders/engine/quad_render.vert.spv"), .name = "quad_render.vert", .stage = vk::ShaderStageFlagBits::eVertex},
                                                            {.code = utils::read_file("environments/vulkan_environment_shaders/engine/environment_shader.deferred.frag.spv"), .name = "environment_shader.deferred.frag",
                                                             .stage = vk::ShaderStageFlagBits::eFragment} };

    std::array<std::vector<vk::DescriptorSetLayoutBinding>, 2> composition_descriptor_set_bindings = COMPOSITION_DESCRIPTOR_SET_LAYOUT_BINDINGS;

    GraphicsPipeline::PipelineInfo pipeline_info{
            .vertex_input_stage = {},
            .input_assembly_stage = {{}, vk::PrimitiveTopology::eTriangleList, VK_FALSE},
            .viewport_stage = {{}, 1, nullptr, 1, nullptr},
            .rasterization_stage = {{}, VK_FALSE, VK_FALSE, vk::PolygonMode::eFill, vk::CullModeFlagBits::eBack,
                                    vk::FrontFace::eCounterClockwise, {}, {}, {}, {}, 1.0f},
            .multisample_stage = {{}, vk::SampleCountFlagBits::e1, VK_FALSE, 1.0f, nullptr, VK_FALSE, VK_FALSE},
            .depth_stencil_stage = {{}, VK_TRUE, VK_TRUE, vk::CompareOp::eLessOrEqual, VK_FALSE, VK_FALSE, {}, {}, 0.0f, 1.0f},
            .blending_stage = {{}, VK_FALSE, vk::LogicOp::eCopy, FORWARD_COLOR_BLEND_ATTACHMENT, {}},

            .dynamic_stage = {{}, {}},
            .push_content_ranges = {{vk::ShaderStageFlagBits::eVertex, 0, sizeof(QuadRenderPushConstants)}},

            .descriptor_bindings = {composition_descriptor_set_bindings.cbegin(), composition_descriptor_set_bindings.cend()},

            .sub_pass = subpass_num,
            .pipeline_cache_path = "environment_pipeline_cache_deferred_" + std::to_string(subpass_num),
            .render_pass = render_pass
    };

    return GraphicsPipeline(utils::get_singleton_safe<CoreRenderer>(), shader_infos, pipeline_info, start_constructing_pipeline);
}

aristaeus::gfx::vulkan::GraphicsPipeline aristaeus::gfx::vulkan::RasterizationLightingEnvironment::create_ssao_pipeline(vk::raii::RenderPass &render_pass, uint32_t subpass_num, bool start_constructing_pipeline) {
    std::vector<GraphicsPipeline::ShaderInfo> shader_infos{ {.code = utils::read_file("environments/vulkan_environment_shaders/engine/quad_render.vert.spv"), .name = "quad_render.vert", .stage = vk::ShaderStageFlagBits::eVertex},
                                                            {.code = utils::read_file("environments/vulkan_environment_shaders/gi/ssao.frag.spv"), .name = "ssao.frag",
                                                             .stage = vk::ShaderStageFlagBits::eFragment} };

    std::array<std::vector<vk::DescriptorSetLayoutBinding>, 2> composition_descriptor_set_bindings = COMPOSITION_DESCRIPTOR_SET_LAYOUT_BINDINGS;

    GraphicsPipeline::PipelineInfo pipeline_info = DEFAULT_PIPELINE_INFO;
    
    pipeline_info.push_content_ranges = {{vk::ShaderStageFlagBits::eVertex, 0, sizeof(QuadRenderPushConstants)}};
    pipeline_info.descriptor_bindings = {{SSAO_DESCRIPTOR_SET_BINDINGS.cbegin(), SSAO_DESCRIPTOR_SET_BINDINGS.cend()}};
    pipeline_info.sub_pass = subpass_num;
    pipeline_info.pipeline_cache_path = "ssao_pipeline_cache_" + std::to_string(subpass_num);
    pipeline_info.render_pass = render_pass;

    return GraphicsPipeline(utils::get_singleton_safe<CoreRenderer>(), shader_infos, pipeline_info, start_constructing_pipeline);
}

aristaeus::gfx::vulkan::GraphicsPipeline aristaeus::gfx::vulkan::RasterizationLightingEnvironment::create_ssao_blur_pipeline(vk::raii::RenderPass &render_pass, uint32_t subpass_num, bool start_constructing_pipeline) {
    std::vector<GraphicsPipeline::ShaderInfo> shader_infos{ {.code = utils::read_file("environments/vulkan_environment_shaders/engine/quad_render.vert.spv"), .name = "quad_render.vert", 
                                                             .stage = vk::ShaderStageFlagBits::eVertex},
                                                            {.code = utils::read_file("environments/vulkan_environment_shaders/gi/ssao_blur.frag.spv"), .name = "ssao_blur.frag",
                                                             .stage = vk::ShaderStageFlagBits::eFragment} };

    std::array<std::vector<vk::DescriptorSetLayoutBinding>, 2> composition_descriptor_set_bindings = COMPOSITION_DESCRIPTOR_SET_LAYOUT_BINDINGS;

    GraphicsPipeline::PipelineInfo pipeline_info = DEFAULT_PIPELINE_INFO;
    
    pipeline_info.push_content_ranges = {{vk::ShaderStageFlagBits::eVertex, 0, sizeof(QuadRenderPushConstants)}};
    pipeline_info.descriptor_bindings = { {SSAO_BLUR_DESCRIPTOR_SET_BINDING} };
    pipeline_info.sub_pass = subpass_num;
    pipeline_info.pipeline_cache_path = "ssao_blur_pipeline_cache_" + std::to_string(subpass_num);
    pipeline_info.render_pass = render_pass;

    return GraphicsPipeline(utils::get_singleton_safe<CoreRenderer>(), shader_infos, pipeline_info, start_constructing_pipeline);
}

aristaeus::gfx::vulkan::RasterizationLightingEnvironment::GraphicsPipelineMaterialInfo aristaeus::gfx::vulkan::RasterizationLightingEnvironment::create_screen_space_interp_pipeline() {
    create_screen_space_interp_render_phases();

    return GraphicsPipelineMaterialInfo{
        .pipeline_info = get_screen_space_interp_pipeline_pipeline_info(),
        .shader_infos = get_screen_space_interp_pipeline_shader_infos() 
    };
}

aristaeus::gfx::vulkan::RasterizationLightingEnvironment::RasterizationLightingEnvironment(const PBRMaterialInfo &default_pbr_material_info, const std::vector<glm::vec3> &initial_capture_points,
                                                                                 vk::Extent2D initial_depth_capture_resolution, size_t initial_capture_point_capacity,
                                                                                 size_t initial_max_num_of_point_lights, std::optional<vk::Extent2D> initial_capture_resolution, 
                                                                                 std::optional<vk::Viewport> viewport, bool prioritize_deferred, 
                                                                                 bool using_screen_space_interp, const glm::uvec2 &screen_space_interp_grid_size) :
    LightingEnvironment(default_pbr_material_info, initial_capture_points, initial_depth_capture_resolution, initial_capture_point_capacity, initial_max_num_of_point_lights, initial_capture_resolution),
    m_shadow_mapping_sampler(create_shadow_mapping_sampler()),
    m_environment_uniform_buffer(create_environment_uniform_buffer()),
    m_captured_sky_box(new CapturedSkyBox(initial_capture_points, std::max(initial_capture_points.size(), initial_capture_point_capacity),
        initial_capture_resolution.has_value() ? *initial_capture_resolution : utils::get_singleton_safe<CoreRenderer>().get_swap_chain_extent(), false)),
    m_captured_sky_box_point_light_shadows(new CapturedSkyBox({}, initial_max_num_of_point_lights, initial_depth_capture_resolution, true)),
    m_prioritizing_deferred(prioritize_deferred),
    m_composition_env_descriptor_set_layout(utils::get_singleton_safe<CoreRenderer>().get_logical_device_ref(),
                                            {{}, COMPOSITION_DESCRIPTOR_SET_LAYOUT_BINDINGS[1]}),
    m_ssao_env_descriptor_set_layout(utils::get_singleton_safe<CoreRenderer>().get_logical_device_ref(),
                                     {{}, SSAO_DESCRIPTOR_SET_BINDINGS }),
    m_ssao_blur_env_descriptor_set_layout(utils::get_singleton_safe<CoreRenderer>().get_logical_device_ref(),
                                          {{}, SSAO_BLUR_DESCRIPTOR_SET_BINDING }),
    m_forward_env_descriptor_set_layout(utils::get_singleton_safe<CoreRenderer>().get_logical_device_ref(), { {}, BASE_FORWARD_DESCRIPTOR_SET_BINDINGS[1] }),
    m_screen_space_interp_descriptor_set_layout(utils::get_singleton_safe<CoreRenderer>().get_logical_device_ref(), { {}, BASE_SCREEN_SPACE_INTERPOLATION_SET_BINDINGS[1]}),
    m_using_screen_space_interp(using_screen_space_interp),
    m_screen_space_interp_grid_size(screen_space_interp_grid_size),
    m_gbuffer_render_phase(create_gbuffer_render_phase(glm::uvec2{ utils::get_singleton_safe<CoreRenderer>().get_swap_chain_extent().width, 
                                                           utils::get_singleton_safe<CoreRenderer>().get_swap_chain_extent().height } / 
                                       glm::uvec2{ using_screen_space_interp ? 2u : 1u }, get_full_render_function({RasterizationSubpass::DEFERRED}), 
                                       std::string{DEFERRED_RENDER_PHASE_NAME}, 
                                       { {std::string{CoreRenderer::SHADOW_MAPPING_RENDER_PHASE}, vk::PipelineStageFlagBits::eAllGraphics} })),
    m_ssao_render_phase(create_ssao_render_phase()),
    m_composition_render_phase(create_ssao_blur_composition_render_phase()),
    m_gbuffer_pipeline_material_info(create_gbuffer_pipeline(m_gbuffer_render_phase.get_render_pass(std::string{DEFERRED_RENDER_PASS_NAME}))),
    m_depth_mapping_pipeline_material_info(GraphicsPipelineMaterialInfo{ get_depth_map_pipeline_info(utils::get_singleton_safe<CoreRenderer>().get_shadow_mapping_render_pass(),
                                                                        vk::CullModeFlagBits::eBack), get_depth_map_shader_infos() }),
    m_depth_mapping_pipeline_front_culling_material_info(GraphicsPipelineMaterialInfo{ get_depth_map_pipeline_info(
                                                        utils::get_singleton_safe<CoreRenderer>().
                                                        get_shadow_mapping_render_pass(),
                                                        vk::CullModeFlagBits::eFront),get_depth_map_shader_infos() }),
    m_forward_pipeline_material_info(GraphicsPipelineMaterialInfo{ get_forward_pipeline_pipeline_info(ForwardPipelineType::RegularEnvironment),
                                     get_forward_pipeline_shader_infos(ForwardPipelineType::RegularEnvironment) }),
    m_ssao_pipeline(create_ssao_pipeline(m_ssao_render_phase.get_render_pass(std::string{SSAO_RENDER_PASS_NAME}), 0)),
    m_ssao_blur_pipeline(create_ssao_blur_pipeline(m_composition_render_phase.get_render_pass(std::string{SSAO_BLUR_COMPOSE_RENDER_PASS_NAME}), 0)),
    m_composition_pipeline(create_composition_pipeline(m_composition_render_phase.get_render_pass(
                                                                 std::string{ SSAO_BLUR_COMPOSE_RENDER_PASS_NAME }), 1)),
    m_default_sampler(utils::get_singleton_safe<CoreRenderer>().get_logical_device_ref(),
                      utils::get_singleton_safe<CoreRenderer>().get_recommended_sampler_create_info()),
    m_white_noise(CoreRenderer::mersenne_twister_rng),
    m_white_noise_texture(m_white_noise.to_white_noise_texture(glm::ivec2{512, 512})) {

    auto &core_renderer = utils::get_singleton_safe<CoreRenderer>();
    PerspectiveCameraEnvironment &camera_env = core_renderer.get_perspective_camera_environment();

    m_environment_ubo_data.num_of_rsm_vpls = 50;
    m_environment_ubo_data.rsm_weighting = 0.5f;
    m_environment_ubo_data.sampling_radius_max = 0.03f;

    if (m_using_screen_space_interp) {
        m_screen_space_interpolation_pipeline = std::make_unique<GraphicsPipeline>(core_renderer, get_screen_space_interp_pipeline_shader_infos(),
                                                                                   get_screen_space_interp_pipeline_pipeline_info());
        vk::raii::RenderPass &occlusion_query_render_pass = m_occlusion_query_render_phase->get_render_pass(std::string{ OCCLUSION_QUERY_RENDER_PASS_NAME });
        vk::raii::RenderPass &final_re_render_pass = m_final_high_res_render_phase->get_render_pass(std::string{ DEFERRED_RENDER_PASS_NAME });

        // need to delay pipeline creation for these because of move going on
        m_gbuffer_screen_space_interp_pipeline_material_info = create_gbuffer_pipeline(occlusion_query_render_pass);
        m_composition_final_re_render_pipeline = std::make_unique<GraphicsPipeline>(create_composition_pipeline(final_re_render_pass, 0, false));

        m_composition_final_re_render_pipeline->start_initial_pipeline_creation(core_renderer, false);

        core_renderer.set_default_rasterization_render_pass(std::string{ FINAL_RE_RENDER_RENDER_PHASE_NAME }, std::string{ DEFERRED_RENDER_PASS_NAME });
        core_renderer.set_default_rasterization_subpass(1);
    }
    else {
        core_renderer.set_default_rasterization_render_pass(std::string{ SSAO_BLUR_COMPOSE_RENDER_PHASE_NAME }, 
                                                            std::string{ SSAO_BLUR_COMPOSE_RENDER_PASS_NAME });
        core_renderer.set_default_rasterization_subpass(2);
    }

    core_renderer.init_imgui();

    m_reflective_shadow_map = std::make_unique<gi::ReflectiveShadowMap>(1, 3, vk::Viewport{ 0, 0, 1000, 1000 }, true);

    add_callback_for_event<void*, RasterizationLightingEnvironment>(std::string{ LightingEnvironment::ADDED_TO_CAMERA_ENVIRONMENT_EVENT }, "forward_lighting_env_added_camera_env", 
                                                                    [](RasterizationLightingEnvironment &env, void*) {
        env.added_to_camera_environment();  
    }, *this);

    m_environment_ubo_data.gi_mode |= ALL_LIGHTING_BITS;
}

void aristaeus::gfx::vulkan::RasterizationLightingEnvironment::depth_only_subpass(std::shared_ptr<gfx::SceneGraphNode> &scene_graph_node_root_strong,
                                                                                  bool predraw) {
    auto &core_renderer = utils::get_singleton_safe<CoreRenderer>();

    if (predraw) {
        core_renderer.set_default_graphics_pipeline_info(m_depth_mapping_pipeline_material_info.pipeline_info);
        core_renderer.set_default_graphics_shader_infos(m_depth_mapping_pipeline_material_info.shader_infos);

        scene_graph_node_root_strong->draw_tree(SceneGraphNode::IDENTITY_TRANSFORM, {}, {}, { get_lighting_env_tag() });

        core_renderer.set_default_graphics_pipeline_info({});
    }
    else {
        uint32_t current_frame = core_renderer.get_current_frame();
        auto &mesh_manager = utils::get_singleton_safe<MeshManager>();
        RenderContextID render_context_id = core_renderer.get_current_render_context_id();

        vk::CommandBuffer primary_command_buffer =
            core_renderer.get_graphics_command_buffer_manager().get_primary_command_buffer(current_frame);

        mesh_manager.draw(primary_command_buffer, render_context_id, m_current_subpass_num);
    }
}

void aristaeus::gfx::vulkan::RasterizationLightingEnvironment::gbuffer_subpass(std::shared_ptr<gfx::SceneGraphNode> &scene_graph_node_root_strong,
                                                                               bool predraw) {
    auto &core_renderer = utils::get_singleton_safe<CoreRenderer>();
    uint32_t current_frame = core_renderer.get_current_frame();
    RenderContextID current_render_context_id = core_renderer.get_current_render_context_id();

    // subpass 0: we are rendering out our gbuffer

    std::unordered_set<std::string> ignore_render_groups{ std::string{FORWARD_RENDER_NODE_GROUP}, std::string{BOTH_RENDER_NODE_GROUP} };

    if (predraw) {
        core_renderer.set_default_graphics_pipeline_info(m_gbuffer_pipeline_material_info.pipeline_info);
        core_renderer.set_default_graphics_shader_infos(m_gbuffer_pipeline_material_info.shader_infos);

        // if we are prioritizing deferred, no DEFERRED_RENDER_NODE_GROUP tag is necessary. otherwise, one is necessary 
        if (m_prioritizing_deferred) {
            scene_graph_node_root_strong->draw_tree(SceneGraphNode::IDENTITY_TRANSFORM, {}, {}, { get_lighting_env_tag() }, ignore_render_groups);
        }
        else {
            scene_graph_node_root_strong->draw_tree(SceneGraphNode::IDENTITY_TRANSFORM, {}, {}, { get_lighting_env_tag(),
                                                    std::string{DEFERRED_RENDER_NODE_GROUP} }, ignore_render_groups);
        }

        ignore_render_groups.erase(std::string{ BOTH_RENDER_NODE_GROUP });

        scene_graph_node_root_strong->draw_tree(SceneGraphNode::IDENTITY_TRANSFORM, {}, {},
            { get_lighting_env_tag(), std::string{BOTH_RENDER_NODE_GROUP} }, ignore_render_groups);

        core_renderer.set_default_graphics_pipeline_info({});
    }
    else {
        vk::CommandBuffer primary_command_buffer = core_renderer.get_graphics_command_buffer_manager().get_primary_command_buffer(current_frame);

        auto &mesh_manager = utils::get_singleton_safe<MeshManager>();

        mesh_manager.draw(primary_command_buffer, current_render_context_id, 0);

        core_renderer.get_graphics_command_buffer_manager().record_all_secondary_command_buffers(current_render_context_id, m_current_subpass_num,
            { primary_command_buffer });
    }
}

std::array<aristaeus::gfx::vulkan::BufferRaii, utils::vulkan::MAX_FRAMES_IN_FLIGHT>
aristaeus::gfx::vulkan::RasterizationLightingEnvironment::create_environment_uniform_buffer() {
    std::array<BufferRaii, utils::vulkan::MAX_FRAMES_IN_FLIGHT> environment_uniform_buffer;
    auto& core_renderer = utils::get_singleton_safe<CoreRenderer>();

    for (size_t i = 0; environment_uniform_buffer.size() > i; ++i) {
        environment_uniform_buffer[i] = core_renderer.get_memory_manager().create_buffer(core_renderer,
            sizeof(EnvironmentUBOData), vk::BufferUsageFlagBits::eUniformBuffer,
            vk::SharingMode::eExclusive, VMA_ALLOCATION_CREATE_HOST_ACCESS_SEQUENTIAL_WRITE_BIT,
            VMA_MEMORY_USAGE_AUTO, "environment-ubo");
    }

    return environment_uniform_buffer;
}

std::array<vk::raii::DescriptorSet, utils::vulkan::MAX_FRAMES_IN_FLIGHT>
aristaeus::gfx::vulkan::RasterizationLightingEnvironment::create_environment_descriptor_sets(RenderContextID render_context_id,
                                                                                             vk::raii::DescriptorSetLayout &descriptor_set_layout) {
    vk::raii::Device &logical_device = utils::get_singleton_safe<CoreRenderer>().get_logical_device_ref();

    static_assert(utils::vulkan::MAX_FRAMES_IN_FLIGHT == 2);

    std::array<vk::DescriptorSetLayout, utils::vulkan::MAX_FRAMES_IN_FLIGHT> descriptor_set_layouts{ *descriptor_set_layout, *descriptor_set_layout };
    vk::DescriptorSetAllocateInfo set_allocate_info{ *m_environment_descriptor_pools.at(render_context_id),
                                                     descriptor_set_layouts };
    std::vector<vk::raii::DescriptorSet> new_sets_vector = logical_device.allocateDescriptorSets(set_allocate_info);

    return { std::move(new_sets_vector[0]), std::move(new_sets_vector[1]) };
}

vk::raii::DescriptorPool aristaeus::gfx::vulkan::RasterizationLightingEnvironment::create_environment_descriptor_pools(
    const std::vector<vk::DescriptorPoolSize>& pool_sizes) {
    return { utils::get_singleton_safe<CoreRenderer>().get_logical_device_ref(),
            {{vk::DescriptorPoolCreateFlagBits::eFreeDescriptorSet},
             2 * utils::vulkan::MAX_FRAMES_IN_FLIGHT, pool_sizes} };
}


std::unique_ptr<aristaeus::gfx::vulkan::CapturedSkyBox>
aristaeus::gfx::vulkan::RasterizationLightingEnvironment::create_captured_skybox(const std::vector<glm::vec3>& initial_capture_points, uint32_t capture_point_capacity,
    const vk::Extent2D& captured_sky_box_resolution, bool depth_only) {
    if (capture_point_capacity > 0) {
        std::unique_ptr<CapturedSkyBox> captured_sky_box = std::make_unique<CapturedSkyBox>(initial_capture_points,
            capture_point_capacity, captured_sky_box_resolution,
            depth_only);

        return captured_sky_box;
    }
    else {
        return {};
    }
}

std::vector<vk::WriteDescriptorSet> aristaeus::gfx::vulkan::RasterizationLightingEnvironment::get_rsm_texture_writes(RenderContextID render_context_id) {
    auto &core_renderer = utils::get_singleton_safe<CoreRenderer>();

    uint32_t current_frame = core_renderer.get_current_frame();
    std::vector<vk::WriteDescriptorSet> descriptor_set_writes;

    m_temp_image_infos.clear();

    if (m_environment_ubo_data.gi_mode | RSM_DIRECTIONAL_MODE) {
        std::vector<vk::DescriptorImageInfo> directional_rsm_descriptors = m_reflective_shadow_map->get_directional_rsm_descriptor_infos();

        m_temp_image_infos.insert(m_temp_image_infos.end(), directional_rsm_descriptors.begin(), directional_rsm_descriptors.end());
    }

    for (int i = m_temp_image_infos.size(); MAX_NUM_OF_DIRECTIONAL_LIGHTS * gi::ReflectiveShadowMap::RSM_STRIDE > i; ++i) {
        m_temp_image_infos.push_back(m_null_image_info);
    }

    size_t point_light_start = m_temp_image_infos.size();

    if (m_environment_ubo_data.gi_mode | RSM_POINT_MODE) {
        std::vector<vk::DescriptorImageInfo> point_rsm_descriptors = m_reflective_shadow_map->get_point_rsm_descriptor_infos();

        m_temp_image_infos.insert(m_temp_image_infos.end(), point_rsm_descriptors.begin(), point_rsm_descriptors.end());
    }

    for (int i = m_temp_image_infos.size() - point_light_start; MAX_NUM_OF_POINT_LIGHTS * gi::ReflectiveShadowMap::RSM_STRIDE > i; ++i) {
        m_temp_image_infos.push_back(m_null_cubemap_image_info);
    }

    descriptor_set_writes.emplace_back(
        *m_environment_descriptor_sets.at(render_context_id)[current_frame],
        7, 0, vk::DescriptorType::eCombinedImageSampler, m_null_image_info);

    descriptor_set_writes.emplace_back(
        *m_environment_descriptor_sets.at(render_context_id)[current_frame], 8, 0,
        vk::DescriptorType::eCombinedImageSampler, m_null_cubemap_image_info);

    descriptor_set_writes[descriptor_set_writes.size() - 2].pImageInfo = m_temp_image_infos.data();
    descriptor_set_writes[descriptor_set_writes.size() - 2].descriptorCount = point_light_start;

    descriptor_set_writes.back().pImageInfo = &m_temp_image_infos[point_light_start];
    descriptor_set_writes.back().descriptorCount = m_temp_image_infos.size() - point_light_start;

    return descriptor_set_writes;
}

std::vector<vk::WriteDescriptorSet> aristaeus::gfx::vulkan::RasterizationLightingEnvironment::get_default_texture_writes(RenderContextID render_context_id) {
    auto &core_renderer = utils::get_singleton_safe<CoreRenderer>();
    auto &material_manager = utils::get_singleton_safe<MaterialManager>();

    uint32_t current_frame = core_renderer.get_current_frame();

    m_environment_ubo_info = vk::DescriptorBufferInfo{ m_environment_uniform_buffer[current_frame]->buffer, 0, sizeof(EnvironmentUBOData) };
    vk::WriteDescriptorSet environment_ubo_descriptor_set_write{ *m_environment_descriptor_sets.at(render_context_id)[current_frame], 0, 0,
                                                                 vk::DescriptorType::eUniformBuffer, {},
                                                                 m_environment_ubo_info };
    vk::raii::ImageView& render_pass_image_view = core_renderer.get_render_phase_pass_image_view(std::string{ CoreRenderer::SHADOW_MAPPING_RENDER_PHASE },
        std::string{ CoreRenderer::SHADOW_MAPPING_RENDER_PHASE_DIRECTIONAL_RENDER_PASS });

    m_temp_shadow_map_sampler_info = { *m_shadow_mapping_sampler, *render_pass_image_view, vk::ImageLayout::eShaderReadOnlyOptimal };

    assert(NULL_CUBE_MAP_TEXTURE_INITED);

    assert(m_captured_sky_box_point_light_shadows == nullptr ||
        m_captured_sky_box_point_light_shadows->get_capture_point_capacity() <= MAX_NUM_OF_POINT_LIGHTS);

    size_t occupied_point_light_count = 0;

    if (m_captured_sky_box_point_light_shadows != nullptr) {
        std::vector<vk::DescriptorImageInfo> point_light_shadows = m_captured_sky_box_point_light_shadows->get_all_descriptor_image_infos();
        for (vk::DescriptorImageInfo descriptor_image_info : point_light_shadows) {
            if (occupied_point_light_count >= MAX_NUM_OF_POINT_LIGHTS) {
                break;
            }

            m_write_descriptor_set_point_light_temp_info[occupied_point_light_count] = descriptor_image_info;

            ++occupied_point_light_count;
        }
    }

    vk::DescriptorImageInfo null_cubemap_descriptor = NULL_CUBE_MAP_TEXTURE->get_descriptor_image_info();

    for (size_t i = occupied_point_light_count; MAX_NUM_OF_POINT_LIGHTS > i; ++i) {
        m_write_descriptor_set_point_light_temp_info[i] = null_cubemap_descriptor;
    }

    m_environment_ubo_data.shadows_enabled = m_shadow_mode;

    m_caustic_environment_map_sampler_temp_info = material_manager.get_null_image_info();

    if (m_caustic_environment_map.has_value()) {
        m_caustic_environment_map_sampler_temp_info = m_caustic_environment_map->get_descriptor_image_info(core_renderer);
    }

    vk::raii::DescriptorSet& current_descriptor_set = m_environment_descriptor_sets.at(render_context_id)[current_frame];

    vk::WriteDescriptorSet shadow_map_set_write{ *current_descriptor_set, 1, 0,
                                                 vk::DescriptorType::eCombinedImageSampler, m_null_image_info,
                                                 {} };
    vk::WriteDescriptorSet point_light_shadow_map_set_write{ *current_descriptor_set, 2, 0,
                                                             vk::DescriptorType::eCombinedImageSampler,
                                                             m_write_descriptor_set_point_light_temp_info, {} };
    vk::WriteDescriptorSet caustic_environment_map_shadow_map_set_write{ *current_descriptor_set, 3, 0,
                                                             vk::DescriptorType::eCombinedImageSampler,
                                                             m_caustic_environment_map_sampler_temp_info, {} };


    std::vector<vk::WriteDescriptorSet> descriptor_set_writes{ environment_ubo_descriptor_set_write,
                                                               shadow_map_set_write,
                                                               point_light_shadow_map_set_write,
                                                               caustic_environment_map_shadow_map_set_write };

    RenderPhase &current_render_phase = *core_renderer.get_current_render_phase();
    bool using_shadow_mapping = current_render_phase.is_render_pass_depth_only(current_render_phase.get_current_render_pass());

    m_null_image_info =
        material_manager.get_null_image_info();
    m_null_cubemap_image_info = NULL_CUBE_MAP_TEXTURE->get_descriptor_image_info();

    std::shared_ptr<SkyBox> active_sky_box = SkyBox::active_sky_box.lock();

    if (active_sky_box != nullptr) {
        m_temp_sky_box_descriptor_image_infos = active_sky_box->get_descriptor_image_infos();

        if (!m_temp_sky_box_descriptor_image_infos.empty()) {
            m_temp_sky_box_descriptor_image_infos.erase(m_temp_sky_box_descriptor_image_infos.begin());
        }
    }

    if (!using_shadow_mapping) {
        m_environment_ubo_data.num_of_point_lights = 0;

        for (size_t i = 0; m_point_lights.size() > i; ++i) {
            std::shared_ptr<LightingNode> point_light_strong = m_point_lights[i].lock();

            if (point_light_strong == nullptr) {
                m_environment_ubo_data.point_lights[i].light_active = 0;
            }
            else {
                m_environment_ubo_data.point_lights[i] = point_light_strong->get_light_node_ubo_data();

                ++m_environment_ubo_data.num_of_point_lights;
            }
        }

        m_environment_ubo_data.num_of_directional_lights = 0;

        for (size_t i = 0; m_directional_lights.size() > i; ++i) {
            std::shared_ptr<LightingNode> directional_light_strong = m_directional_lights[i].lock();

            if (directional_light_strong == nullptr) {
                m_environment_ubo_data.directional_lights[i].light_active = 0;
            }
            else {
                PerspectiveCameraEnvironment::Capture capture = 
                    static_cast<gfx::vulkan::DirectionalLight *>(directional_light_strong.get())->get_shadow_map_capture();
                m_environment_ubo_data.directional_lights[i] = directional_light_strong->get_light_node_ubo_data();

                m_environment_ubo_data.directional_light_matrices[i] = capture.get_projection_matrix() * capture.get_view_matrix();


                ++m_environment_ubo_data.num_of_directional_lights;
            }
        }

        void* buffer_data;

        vmaMapMemory(m_environment_uniform_buffer[current_frame]->allocator,
            m_environment_uniform_buffer[current_frame]->allocation, &buffer_data);

        std::memcpy(buffer_data, &m_environment_ubo_data, sizeof(EnvironmentUBOData));

        vmaUnmapMemory(m_environment_uniform_buffer[current_frame]->allocator,
            m_environment_uniform_buffer[current_frame]->allocation);
    }


    if (!using_shadow_mapping) {
        assert(NULL_CUBE_MAP_TEXTURE_INITED);

        descriptor_set_writes.emplace_back(
            *m_environment_descriptor_sets.at(render_context_id)[current_frame],
            4, 0, vk::DescriptorType::eCombinedImageSampler, m_null_cubemap_image_info);
        descriptor_set_writes.emplace_back(
            *m_environment_descriptor_sets.at(render_context_id)[current_frame], 5, 0,
            vk::DescriptorType::eCombinedImageSampler, m_null_cubemap_image_info);
        descriptor_set_writes.emplace_back(
            *m_environment_descriptor_sets.at(render_context_id)[current_frame],
            6, 0, vk::DescriptorType::eCombinedImageSampler, m_null_image_info);

        if (m_temp_sky_box_descriptor_image_infos.size() >= 3) {
            for (size_t i = 4; std::min(descriptor_set_writes.size(),
                4 + m_temp_sky_box_descriptor_image_infos.size()) > i; ++i) {
                descriptor_set_writes[i].pImageInfo = &m_temp_sky_box_descriptor_image_infos[i - 4];
            }
        }

        std::vector<vk::WriteDescriptorSet> rsm_writes = get_rsm_texture_writes(render_context_id);

        descriptor_set_writes.insert(descriptor_set_writes.end(), rsm_writes.begin(), rsm_writes.end());

        m_white_noise_image_info = m_white_noise_texture.get_descriptor_image_info(core_renderer);

        descriptor_set_writes.emplace_back(*m_environment_descriptor_sets.at(render_context_id)[current_frame],
                                           9, 0, vk::DescriptorType::eCombinedImageSampler, m_white_noise_image_info);
    }

    return descriptor_set_writes;
}

void aristaeus::gfx::vulkan::RasterizationLightingEnvironment::set_caustic_environment_map(const std::optional<std::string>& environment_map_path) {
    CoreRenderer& core_renderer = utils::get_singleton_safe<CoreRenderer>();

    assert(m_caustic_environment_map_path.has_value() == m_caustic_environment_map.has_value());

    m_caustic_environment_map_path = environment_map_path;

    if (environment_map_path.has_value() && m_caustic_environment_map_path != *environment_map_path) {
        if (environment_map_path.value() == "../environments//perlin") {
            PerlinNoise perlin_noise{ CoreRenderer::mersenne_twister_rng };

            m_caustic_environment_map = perlin_noise.to_perlin_noise_texture(1.0f, 0.5f, 8, 0.3f, glm::ivec2{ 128, 128 },
                core_renderer, {}, true);
        }
        else {
            vk::SamplerCreateInfo sampler_create_info = AssimpModel::DEFAULT_SAMPLER_CREATE_INFO;

            sampler_create_info.maxAnisotropy = core_renderer.get_max_sampler_anisotropy();

            Texture caustic_map_texture{ environment_map_path.value(), std::unordered_set<std::string>{"caustic_environment_map"}, core_renderer,
                                         AssimpModel::DEFAULT_IMAGE_VIEW_CREATE_INFO, sampler_create_info, {}, true };

            m_caustic_environment_map = std::move(caustic_map_texture);
        }
    }
    else {
        m_caustic_environment_map = {};
    }
}

void aristaeus::gfx::vulkan::RasterizationLightingEnvironment::add_lighting_node(
    const std::weak_ptr<gfx::vulkan::LightingNode>& lighting_node) {
    std::shared_ptr<gfx::vulkan::LightingNode> lighting_node_strong = lighting_node.lock();

    if (lighting_node_strong != nullptr) {
        LightingNode::LightNodeUBOData ubo_data = lighting_node_strong->get_light_node_ubo_data();

        switch (ubo_data.light_type) {
        case PointLight::UBO_POINT_LIGHT_TYPE_ID:
        {
            m_environment_ubo_data.num_of_point_lights = std::min(static_cast<uint32_t>(m_environment_ubo_data.num_of_point_lights + 1), MAX_NUM_OF_POINT_LIGHTS);

            uint32_t point_light_index = m_environment_ubo_data.num_of_point_lights - 1;

            m_environment_ubo_data.point_lights[point_light_index] = ubo_data;
            m_point_lights[point_light_index] = lighting_node;

            m_environment_ubo_data.gi_mode |= RSM_POINT_MODE;

            if (m_captured_sky_box_point_light_shadows != nullptr && m_captured_sky_box_point_light_shadows->get_capture_point_capacity() > m_environment_ubo_data.num_of_point_lights) {
                glm::vec3 global_position = lighting_node_strong->get_position() + lighting_node_strong->get_parent_transform().position;

                m_captured_sky_box_point_light_shadows->update_capture_point(global_position, point_light_index);
                m_captured_sky_box_point_light_shadows->attach_capture_point_to_node(global_position, *lighting_node_strong);
            }

            break;
        }
        case DirectionalLight::UBO_DIRECTIONAL_LIGHT_TYPE_ID:
            m_environment_ubo_data.num_of_directional_lights = std::min(static_cast<uint32_t>(m_environment_ubo_data.num_of_directional_lights + 1), MAX_NUM_OF_DIRECTIONAL_LIGHTS);
            m_environment_ubo_data.directional_lights[m_environment_ubo_data.num_of_directional_lights - 1] = ubo_data;

            PerspectiveCameraEnvironment::Capture capture = static_cast<gfx::vulkan::DirectionalLight*>(lighting_node_strong.get())->get_shadow_map_capture();

            m_environment_ubo_data.directional_light_matrices[m_environment_ubo_data.num_of_directional_lights - 1] = capture.get_projection_matrix() * capture.get_view_matrix();
            m_environment_ubo_data.gi_mode |= RSM_DIRECTIONAL_MODE;

            m_directional_lights[m_environment_ubo_data.num_of_directional_lights - 1] = lighting_node;

            break;
        }
    }
}

vk::DescriptorImageInfo aristaeus::gfx::vulkan::RasterizationLightingEnvironment::get_structure_buffer_image_info() {
    auto &core_renderer = utils::get_singleton_safe<CoreRenderer>();

    uint32_t structure_buffer_image_index = (core_renderer.get_current_frame() * GBUFFER_SIZE) + (STRUCTURE_BUFFER_GBUFFER_INDEX - 1);
    vk::raii::ImageView &structure_buffer_image_view = m_gbuffer_render_phase.get_render_pass_image_view(std::string{ DEFERRED_RENDER_PASS_NAME },
        structure_buffer_image_index);
    return { *m_default_sampler, *structure_buffer_image_view, vk::ImageLayout::eShaderReadOnlyOptimal };
}

void aristaeus::gfx::vulkan::RasterizationLightingEnvironment::create_sets_if_nonexistent(RenderContextID render_context_id, 
                                                                                          const std::vector<vk::DescriptorPoolSize> &pool_sizes,
                                                                                          vk::raii::DescriptorSetLayout &set_layout) {
    if (!m_environment_descriptor_sets.contains(render_context_id)) {
        const std::vector<vk::DescriptorPoolSize> pool_sizes{ COMPOSITION_ENVIRONMENT_POOL_SIZES.begin(),
                                                              COMPOSITION_ENVIRONMENT_POOL_SIZES.end() };

        m_environment_descriptor_pools.insert({ render_context_id,
                                                create_environment_descriptor_pools(pool_sizes) });
        m_environment_descriptor_sets.insert({ render_context_id,
                                                create_environment_descriptor_sets(render_context_id, set_layout) });
    }
}

void aristaeus::gfx::vulkan::RasterizationLightingEnvironment::ssao_blur_subpass() {
    auto &core_renderer = utils::get_singleton_safe<CoreRenderer>();
    uint32_t current_frame = core_renderer.get_current_frame();
    RenderContextID current_render_context_id = core_renderer.get_current_render_context_id();

    create_sets_if_nonexistent(current_render_context_id,
                               std::vector<vk::DescriptorPoolSize>{ SSAO_BLUR_POOL_SIZE },
                               m_ssao_blur_env_descriptor_set_layout);

    vk::raii::DescriptorSet &ssao_descriptor_set =
        m_environment_descriptor_sets.at(current_render_context_id)[current_frame];

    vk::CommandBuffer command_buffer =
        core_renderer.get_graphics_command_buffer_manager().get_command_buffer_for_thread(
            current_render_context_id, m_ssao_blur_pipeline, {});

    vk::raii::ImageView &unblurred_ao_image_view = 
        m_ssao_render_phase.get_render_pass_image_view(std::string{ SSAO_RENDER_PASS_NAME }, current_frame);

    vk::DescriptorImageInfo unblurred_ao_image_info{*m_default_sampler, *unblurred_ao_image_view, vk::ImageLayout::eShaderReadOnlyOptimal};

    std::vector<vk::WriteDescriptorSet> descriptor_set_writes;

    descriptor_set_writes.emplace_back(*ssao_descriptor_set, 0, 0, vk::DescriptorType::eCombinedImageSampler, unblurred_ao_image_info);

    core_renderer.get_logical_device_ref().updateDescriptorSets(descriptor_set_writes, {});

    core_renderer.set_default_graphics_pipeline(m_ssao_blur_pipeline);

    m_ssao_blur_pipeline.bind_to_command_buffer(command_buffer, core_renderer);

    for (const QuadRenderPushConstants &quad_render_push_constant : m_quads_to_compose) {
        m_quads_committed[current_frame].push_back(quad_render_push_constant);

        QuadRenderPushConstants &current_quad_constants = m_quads_committed[current_frame][m_quads_committed[current_frame].size() - 1];

        command_buffer.pushConstants(*m_ssao_blur_pipeline.get_pipeline_layout(), vk::ShaderStageFlagBits::eVertex, 0,
                                     sizeof(QuadRenderPushConstants), &current_quad_constants);

        command_buffer.bindDescriptorSets(vk::PipelineBindPoint::eGraphics, m_ssao_blur_pipeline.get_pipeline_layout(), 0, *ssao_descriptor_set, {});

        command_buffer.draw(6, 1, 0, 0);
    }

    vk::CommandBuffer primary_command_buffer = core_renderer.get_graphics_command_buffer_manager().get_primary_command_buffer(current_frame);

    core_renderer.get_graphics_command_buffer_manager().record_all_secondary_command_buffers(current_render_context_id, m_current_subpass_num,
        { primary_command_buffer });
}

void aristaeus::gfx::vulkan::RasterizationLightingEnvironment::ssao_subpass() {
    auto &core_renderer = utils::get_singleton_safe<CoreRenderer>();
    uint32_t current_frame = core_renderer.get_current_frame();
    RenderContextID current_render_context_id = core_renderer.get_current_render_context_id();

    if (!m_ssao_ubo_set_up) {
        setup_ssao_ubo();
        m_ssao_ubo_set_up = true;
    }

    m_ssao_ubo_data.view = core_renderer.get_perspective_camera_environment().get_view_matrix();
    void *ssao_ubo_data;

    vmaMapMemory(m_ssao_ubo_buffer->allocator, m_ssao_ubo_buffer->allocation, &ssao_ubo_data);

    std::memcpy(ssao_ubo_data, &m_ssao_ubo_data, sizeof(SSAOUboData));

    vmaUnmapMemory(m_ssao_ubo_buffer->allocator, m_ssao_ubo_buffer->allocation);

    create_sets_if_nonexistent(current_render_context_id, 
                               std::vector<vk::DescriptorPoolSize>{ COMPOSITION_ENVIRONMENT_POOL_SIZES.begin(), COMPOSITION_ENVIRONMENT_POOL_SIZES.end() },
                               m_ssao_env_descriptor_set_layout);

    vk::raii::DescriptorSet &ssao_descriptor_set =
        m_environment_descriptor_sets.at(current_render_context_id)[current_frame];

    vk::CommandBuffer command_buffer =
        core_renderer.get_graphics_command_buffer_manager().get_command_buffer_for_thread(
            current_render_context_id, m_ssao_pipeline, {});

    uint32_t position_gbuffer_index = (GBUFFER_SIZE * current_frame);
    uint32_t normal_gbuffer_index = (GBUFFER_SIZE * current_frame) + 2;

    vk::raii::ImageView &position_image_view = m_gbuffer_render_phase.get_render_pass_image_view(std::string{ DEFERRED_RENDER_PASS_NAME }, 
                                                                                                 position_gbuffer_index);
    vk::raii::ImageView &normal_image_view = m_gbuffer_render_phase.get_render_pass_image_view(std::string{ DEFERRED_RENDER_PASS_NAME },
                                                                                               normal_gbuffer_index);

    vk::DescriptorImageInfo position_image_info{ *m_default_sampler, position_image_view, vk::ImageLayout::eShaderReadOnlyOptimal };
    vk::DescriptorImageInfo normal_image_info{ *m_default_sampler, normal_image_view, vk::ImageLayout::eShaderReadOnlyOptimal };
    vk::DescriptorImageInfo noise_image_info = m_white_noise_texture.get_descriptor_image_info(core_renderer);
    vk::DescriptorBufferInfo ssao_ubo_info{m_ssao_ubo_buffer->buffer, 0, sizeof(SSAOUboData)};

    std::vector<vk::DescriptorImageInfo> empty_image_info_vector;
    std::vector<vk::WriteDescriptorSet> descriptor_set_writes;

    descriptor_set_writes.emplace_back(*ssao_descriptor_set, 0, 0, vk::DescriptorType::eCombinedImageSampler, position_image_info);
    descriptor_set_writes.emplace_back(*ssao_descriptor_set, 1, 0, vk::DescriptorType::eCombinedImageSampler, normal_image_info);
    descriptor_set_writes.emplace_back(*ssao_descriptor_set, 2, 0, vk::DescriptorType::eCombinedImageSampler, noise_image_info);
    descriptor_set_writes.emplace_back(*ssao_descriptor_set, 3, 0, vk::DescriptorType::eUniformBuffer, empty_image_info_vector, ssao_ubo_info);

    core_renderer.get_logical_device_ref().updateDescriptorSets(descriptor_set_writes, {});

    core_renderer.set_default_graphics_pipeline(m_ssao_pipeline);

    m_ssao_pipeline.bind_to_command_buffer(command_buffer, core_renderer);

    for (const QuadRenderPushConstants &quad_render_push_constant : m_quads_to_compose) {
        m_quads_committed[current_frame].push_back(quad_render_push_constant);

        QuadRenderPushConstants &current_quad_constants = m_quads_committed[current_frame][m_quads_committed[current_frame].size() - 1];

        command_buffer.pushConstants(*m_ssao_pipeline.get_pipeline_layout(), vk::ShaderStageFlagBits::eVertex, 0, 
                                     sizeof(QuadRenderPushConstants), &current_quad_constants);

        command_buffer.bindDescriptorSets(vk::PipelineBindPoint::eGraphics, m_ssao_pipeline.get_pipeline_layout(), 0, *ssao_descriptor_set, {});

        command_buffer.draw(6, 1, 0, 0);
    }

    vk::CommandBuffer primary_command_buffer = core_renderer.get_graphics_command_buffer_manager().get_primary_command_buffer(current_frame);

    core_renderer.get_graphics_command_buffer_manager().record_all_secondary_command_buffers(current_render_context_id, m_current_subpass_num,
        { primary_command_buffer });
}

void aristaeus::gfx::vulkan::RasterizationLightingEnvironment::composition_subpass() {
    // subpass 1: we are compositing our gbuffer together for our final result
    // (GBUFFER_SIZE * current_frame) + n where n is the positioning within the attachments given to the render pass

    auto &core_renderer = utils::get_singleton_safe<CoreRenderer>();
    uint32_t current_frame = core_renderer.get_current_frame();
    RenderContextID starting_render_context_id = m_composition_render_phase.get_render_pass_starting_render_context_id(m_composition_render_phase.get_current_render_pass());
    RenderContextID current_render_context_id = core_renderer.get_current_render_context_id();
    RenderContextID composition_render_context_id = current_render_context_id + 1000;
    std::vector<vk::DescriptorSet> descriptor_sets{ *core_renderer.get_perspective_camera_environment().get_camera_descriptor_set_to_bind() };

    if (!m_environment_descriptor_sets.contains(composition_render_context_id)) {
        const std::vector<vk::DescriptorPoolSize> pool_sizes{ COMPOSITION_ENVIRONMENT_POOL_SIZES.begin(),
                                                                COMPOSITION_ENVIRONMENT_POOL_SIZES.end() };

        m_environment_descriptor_pools.insert({ composition_render_context_id,
                                                create_environment_descriptor_pools(pool_sizes) });
        m_environment_descriptor_sets.insert({ composition_render_context_id,
                                                create_environment_descriptor_sets(
                                                                        composition_render_context_id,
                                                                        m_composition_env_descriptor_set_layout) });
    }

    vk::raii::DescriptorSet &environment_descriptor_set =
        m_environment_descriptor_sets.at(composition_render_context_id)[current_frame];

    descriptor_sets.push_back(*environment_descriptor_set);

    std::vector<vk::DescriptorImageInfo> additional_image_infos;

    bool in_final_high_res_render_phase = core_renderer.get_current_render_phase() == m_final_high_res_render_phase;
    std::reference_wrapper<GraphicsPipeline> pipeline_to_use = m_composition_pipeline;

    if (in_final_high_res_render_phase) {
        pipeline_to_use = *m_composition_final_re_render_pipeline;
    }

    vk::CommandBuffer command_buffer =
        core_renderer.get_graphics_command_buffer_manager().get_command_buffer_for_thread(
            current_render_context_id, pipeline_to_use, {});

    // GBUFFER_SIZE - 1 to account for the depth attachment
    for (int i = 0; GBUFFER_SIZE - 1 > i; ++i) {
        uint32_t render_pass_image_view_to_get = (GBUFFER_SIZE * current_frame) + i;
        std::reference_wrapper<vk::raii::ImageView> image_view =
            m_gbuffer_render_phase.get_render_pass_image_view(std::string{ DEFERRED_RENDER_PASS_NAME }, render_pass_image_view_to_get);

        if (in_final_high_res_render_phase) {
            image_view = 
                m_occlusion_query_render_phase->get_render_pass_image_view(std::string{OCCLUSION_QUERY_RENDER_PASS_NAME}, 
                                                                           render_pass_image_view_to_get);
        }

        additional_image_infos.emplace_back(*m_default_sampler, *image_view.get(), vk::ImageLayout::eShaderReadOnlyOptimal);
    }

    std::vector<vk::WriteDescriptorSet> composition_descriptor_set_writes = get_default_texture_writes(composition_render_context_id);

    int descriptor_index = 10;

    for (int i = 0; additional_image_infos.size() > i; ++i) {
        if (i == STRUCTURE_BUFFER_GBUFFER_INDEX - 1) {
            continue;
        }

        composition_descriptor_set_writes.emplace_back(*environment_descriptor_set, descriptor_index++, 0,
                                                       vk::DescriptorType::eCombinedImageSampler, additional_image_infos[i]);
    }
    
    vk::raii::ImageView &ao_image_view = m_composition_render_phase.get_render_pass_image_view(std::string{SSAO_BLUR_COMPOSE_RENDER_PASS_NAME}, 
                                                                                               (core_renderer.get_current_swap_chain_image() * 3) + 1);

    vk::DescriptorImageInfo image_info{m_default_sampler, ao_image_view, vk::ImageLayout::eShaderReadOnlyOptimal};

    composition_descriptor_set_writes.emplace_back(*environment_descriptor_set, 14, 0, vk::DescriptorType::eInputAttachment, image_info);

    core_renderer.get_logical_device_ref().updateDescriptorSets(composition_descriptor_set_writes, {});

    core_renderer.set_default_graphics_pipeline(pipeline_to_use);

    pipeline_to_use.get().bind_to_command_buffer(command_buffer, core_renderer);

    for (const QuadRenderPushConstants &quad_render_push_constant : m_quads_to_compose) {
        m_quads_committed[current_frame].push_back(quad_render_push_constant);

        QuadRenderPushConstants &current_quad_constants = m_quads_committed[current_frame][m_quads_committed[current_frame].size() - 1];

        command_buffer.pushConstants(*pipeline_to_use.get().get_pipeline_layout(),
                                     vk::ShaderStageFlagBits::eVertex, 0, sizeof(QuadRenderPushConstants),
                                     &current_quad_constants);

        command_buffer.bindDescriptorSets(vk::PipelineBindPoint::eGraphics, *pipeline_to_use.get().get_pipeline_layout(),
                                          0, descriptor_sets, {});

        command_buffer.draw(6, 1, 0, 0);
    }

    vk::CommandBuffer primary_command_buffer = core_renderer.get_graphics_command_buffer_manager().get_primary_command_buffer(current_frame);

    core_renderer.get_graphics_command_buffer_manager().record_all_secondary_command_buffers(current_render_context_id, m_current_subpass_num, 
                                                                                             { primary_command_buffer });
}

void aristaeus::gfx::vulkan::RasterizationLightingEnvironment::forward_subpass(std::shared_ptr<gfx::SceneGraphNode> &scene_graph_node_root_strong, bool predraw) {
    auto &core_renderer = utils::get_singleton_safe<CoreRenderer>();
    uint32_t current_frame = core_renderer.get_current_frame();
    RenderContextID render_context_id = core_renderer.get_current_render_context_id();
    RenderContextID forward_render_context_id = render_context_id + 2000;

    assert(m_environment_descriptor_sets.contains(forward_render_context_id) ==
           m_environment_descriptor_pools.contains(forward_render_context_id));

    if (predraw) {
        if (!m_environment_descriptor_sets.contains(forward_render_context_id)) {
            const std::vector<vk::DescriptorPoolSize> environment_pool_sizes{ DEFAULT_ENVIRONMENT_POOL_SIZES.begin(), DEFAULT_ENVIRONMENT_POOL_SIZES.end() };

            m_environment_descriptor_pools.insert({ forward_render_context_id,
                                                   create_environment_descriptor_pools(environment_pool_sizes) });
            m_environment_descriptor_sets.insert({ forward_render_context_id,
                                                  create_environment_descriptor_sets(forward_render_context_id, m_forward_env_descriptor_set_layout) });
        }

        tf::Taskflow active_environment_taskflow;
        RenderPhase &render_phase = *utils::get_singleton_safe<CoreRenderer>().get_current_render_phase();
        std::string current_render_pass = render_phase.get_current_render_pass();
        bool using_shadow_mapping = render_phase.is_render_pass_depth_only(current_render_pass);

        auto &material_manager = utils::get_singleton_safe<MaterialManager>();
        int forward_subpass_num = m_current_subpass_num;

        material_manager.call_upon_super_material_binds(render_context_id, [this, current_frame, render_context_id, forward_render_context_id, forward_subpass_num](MaterialID material_id) {
            auto &material_manager = utils::get_singleton_safe<MaterialManager>();
            GraphicsPipeline &pipeline = material_manager.get_graphics_pipeline_for_material(material_id);

            if (pipeline.get_pipeline_info().sub_pass != forward_subpass_num) {
                return;
            }

            std::vector<vk::WriteDescriptorSet> descriptor_set_writes = get_default_texture_writes(forward_render_context_id);
            auto &core_renderer = utils::get_singleton_safe<CoreRenderer>();

            vk::raii::DescriptorSet &camera_descriptor_set =
                core_renderer.get_perspective_camera_environment().get_camera_descriptor_set_to_bind();

            core_renderer.get_logical_device_ref().updateDescriptorSets(descriptor_set_writes, {});

            core_renderer.get_graphics_command_buffer_manager().bind_descriptor_sets_on_all_command_buffers(
                pipeline.get_pipeline_layout(), render_context_id, forward_subpass_num, 0,
                { *camera_descriptor_set,
                    *m_environment_descriptor_sets.at(forward_render_context_id)[current_frame] }, {});
        });

        std::map<float, std::shared_ptr<SceneGraphNode>> sorted_blended_models;

        for (unsigned int i = 0; m_blended_nodes.size() > i; ++i) {
            std::shared_ptr<SceneGraphNode> temp_ptr = m_blended_nodes[i].lock();

            if (temp_ptr != nullptr) {
                float camera_distance = glm::length(core_renderer.get_perspective_camera_environment().get_camera_position() -
                    temp_ptr->get_position());

                sorted_blended_models[camera_distance] = temp_ptr;
            }
        }

        float current_z_order = SceneGraphNode::BLENDED_OBJECT_Z_ORDER_RANGE_START;

        for (const std::pair<const float, std::shared_ptr<SceneGraphNode>> &scene_graph_node : sorted_blended_models) {
            assert(scene_graph_node.second != nullptr);

            scene_graph_node.second->set_z_order(current_z_order);

            ++current_z_order;
        }


        if (m_recapture_point_light_shadows_needed) {
            core_renderer.queue_recapture_point_light_shadows();
            m_recapture_point_light_shadows_needed = false;
        }

        std::unordered_set<std::string> ignore_render_groups{ std::string{DEFERRED_RENDER_NODE_GROUP}, std::string{ BOTH_RENDER_NODE_GROUP } };

        core_renderer.set_default_graphics_pipeline_info(m_forward_pipeline_material_info.pipeline_info);
        core_renderer.set_default_graphics_shader_infos(m_forward_pipeline_material_info.shader_infos);

        // if we aren't prioritizing deferred (and therefore prioritizing forward), 
        // no FORWARD_RENDER_NODE_GROUP tag is necessary. otherwise, one is necessary 
        if (m_prioritizing_deferred) {
            scene_graph_node_root_strong->draw_tree(SceneGraphNode::IDENTITY_TRANSFORM, {}, {},
                { get_lighting_env_tag(), std::string{FORWARD_RENDER_NODE_GROUP} }, ignore_render_groups);
        }
        else {
            scene_graph_node_root_strong->draw_tree(SceneGraphNode::IDENTITY_TRANSFORM, {}, {},
                { get_lighting_env_tag() }, ignore_render_groups);
        }

        ignore_render_groups.erase(std::string{ BOTH_RENDER_NODE_GROUP });

        scene_graph_node_root_strong->draw_tree(SceneGraphNode::IDENTITY_TRANSFORM, {}, {}, { get_lighting_env_tag(),
                                                std::string{BOTH_RENDER_NODE_GROUP} }, ignore_render_groups);

        core_renderer.set_default_graphics_pipeline_info({});
    }
    else {
        vk::CommandBuffer primary_command_buffer =
            core_renderer.get_graphics_command_buffer_manager().get_primary_command_buffer(current_frame);
        auto &mesh_manager = utils::get_singleton_safe<MeshManager>();

        mesh_manager.draw(primary_command_buffer, render_context_id, m_current_subpass_num);
    }
}

void aristaeus::gfx::vulkan::RasterizationLightingEnvironment::occlusion_query_subpass() {
    auto &core_renderer = utils::get_singleton_safe<CoreRenderer>();
    RenderContextID current_render_context_id = core_renderer.get_current_render_context_id();
    uint32_t frame = core_renderer.get_current_frame();

    glm::vec2 quad_size{ glm::vec2(1.0f / m_screen_space_interp_grid_size.x, 1.0f / m_screen_space_interp_grid_size.y) };

    QuadRenderPushConstants quad_push_constants{ .quad_extents = quad_size };

    CommandBufferState secondary_command_buffer =
        core_renderer.get_graphics_command_buffer_manager().get_command_buffer_state_for_thread(core_renderer.get_current_render_context_id(),
                                                                                                {}, {}, m_current_subpass_num);

    if (!m_environment_descriptor_sets.contains(current_render_context_id)) {
        const std::vector<vk::DescriptorPoolSize> pool_sizes{ SCREEN_SPACE_INTERP_POOL_SIZES.begin(),
                                                              SCREEN_SPACE_INTERP_POOL_SIZES.end() };

        m_environment_descriptor_pools.insert({ current_render_context_id,
                                                create_environment_descriptor_pools(pool_sizes) });
        m_environment_descriptor_sets.insert({ current_render_context_id,
                                                create_environment_descriptor_sets(current_render_context_id,
                                                                                   m_screen_space_interp_descriptor_set_layout) });
    }

    static const std::vector<vk::DescriptorImageInfo> empty_descriptor_image_infos;
    std::vector<vk::DescriptorImageInfo> descriptor_image_infos;

    size_t query_offset = frame * 4 * m_screen_space_interp_grid_size.x * m_screen_space_interp_grid_size.y;
    vk::DescriptorBufferInfo occlusion_buffer_info{ m_occlusion_result_buffer->buffer, query_offset, 
                                                    4 * m_screen_space_interp_grid_size.x * m_screen_space_interp_grid_size.y };

    std::vector<vk::WriteDescriptorSet> descriptor_set_writes;

    uint32_t image_frame_offset = get_gbuffer_attachment_info().size() * frame;

    descriptor_image_infos.push_back(create_default_image_info(
        m_occlusion_query_render_phase->get_render_pass_image_view(std::string{ OCCLUSION_QUERY_RENDER_PASS_NAME }, image_frame_offset + 1))); // position_screen_texture
    descriptor_image_infos.push_back(create_default_image_info(
        m_occlusion_query_render_phase->get_render_pass_image_view(std::string{ OCCLUSION_QUERY_RENDER_PASS_NAME }, image_frame_offset + 2))); // normal_screen_texture

    descriptor_image_infos.push_back(create_default_image_info(
        m_gbuffer_render_phase.get_render_pass_image_view(std::string{ DEFERRED_RENDER_PASS_NAME }, image_frame_offset + 1))); // low_res_position_screen_texture
    descriptor_image_infos.push_back(create_default_image_info(
        m_gbuffer_render_phase.get_render_pass_image_view(std::string{ DEFERRED_RENDER_PASS_NAME }, image_frame_offset + 2))); // low_res_normal_screen_texture
    descriptor_image_infos.push_back(create_default_image_info(
        m_gbuffer_render_phase.get_render_pass_image_view(std::string{ DEFERRED_RENDER_PASS_NAME }, image_frame_offset))); // low_res_composition_texture

    for (int i = 0; descriptor_image_infos.size() > i; ++i) {
        descriptor_set_writes.emplace_back(*m_environment_descriptor_sets.at(current_render_context_id)[frame],
                                           i, 0, i > 1 ? vk::DescriptorType::eCombinedImageSampler : vk::DescriptorType::eInputAttachment, 
                                           descriptor_image_infos[i]);
    }

    vk::WriteDescriptorSet buffer_write{ *m_environment_descriptor_sets.at(current_render_context_id)[frame], static_cast<uint32_t>(descriptor_image_infos.size()),
                                         0, vk::DescriptorType::eStorageBuffer, empty_descriptor_image_infos, occlusion_buffer_info };

    descriptor_set_writes.push_back(buffer_write); // for OcclusionTestBuffer

    core_renderer.get_logical_device_ref().updateDescriptorSets(descriptor_set_writes, {});

    secondary_command_buffer.command_buffer.bindDescriptorSets(vk::PipelineBindPoint::eGraphics, 
                                                               *m_screen_space_interpolation_pipeline->get_pipeline_layout(),
                                                               1, *m_environment_descriptor_sets.at(current_render_context_id)[frame], {});
    
    m_occlusion_query_screen_space_transforms[frame] = 
        core_renderer.get_perspective_camera_environment().get_projection_matrix() * 
        core_renderer.get_perspective_camera_environment().get_view_matrix();

    secondary_command_buffer.command_buffer.pushConstants(*m_screen_space_interpolation_pipeline->get_pipeline_layout(),
                                                        vk::ShaderStageFlagBits::eFragment, 
                                                        sizeof(QuadRenderPushConstants),
                                                        sizeof(glm::mat4), &m_occlusion_query_screen_space_transforms[frame]);

    for (int y = 0; m_screen_space_interp_grid_size.y > y; ++y) {
        for (int x = 0; m_screen_space_interp_grid_size.x > x; ++x) {
            int query_index = (y * m_screen_space_interp_grid_size.x) + x;

            quad_push_constants.quad_start = quad_size * glm::vec2{ x, y };

            assert(m_screen_space_interpolation_pipeline != nullptr);

            core_renderer.set_default_graphics_pipeline(*m_screen_space_interpolation_pipeline);

            m_screen_space_interpolation_pipeline->bind_to_command_buffer(secondary_command_buffer.command_buffer, core_renderer);

            m_quads_committed[frame].push_back(quad_push_constants);

            // if this static_assert fails, we need to double check our values in screen_space_interpolation
            // for push constant offsets
            static_assert(sizeof(QuadRenderPushConstants) == 16);

            secondary_command_buffer.command_buffer.pushConstants(*m_screen_space_interpolation_pipeline->get_pipeline_layout(), 
                                                                  vk::ShaderStageFlagBits::eVertex, 0, sizeof(QuadRenderPushConstants), 
                                                                  &m_quads_committed[frame][m_quads_committed[frame].size() - 1]);
            secondary_command_buffer.command_buffer.pushConstants(*m_screen_space_interpolation_pipeline->get_pipeline_layout(),
                                                                  vk::ShaderStageFlagBits::eFragment, sizeof(QuadRenderPushConstants) + sizeof(glm::mat4),
                                                                  sizeof(uint32_t), &m_occlusion_query_quad_indices[query_index]);

            secondary_command_buffer.command_buffer.draw(6, 1, 0, 0);
        }
    }
}

void aristaeus::gfx::vulkan::RasterizationLightingEnvironment::draw_tree_subpass(
                                                    std::shared_ptr<gfx::SceneGraphNode> &scene_graph_root, bool predraw, 
                                                    const std::function<void(RasterizationLightingEnvironment&, std::shared_ptr<gfx::SceneGraphNode>&, bool)> &draw_tree_func, 
                                                    RasterizationSubpass subpass_type) {
    if (m_subpasses_enabled.contains(subpass_type)) {
        auto &core_renderer = utils::get_singleton_safe<CoreRenderer>();

        if (m_current_subpass_num > 0 && !predraw) {
            vk::CommandBuffer primary_command_buffer =
                core_renderer.get_graphics_command_buffer_manager().get_primary_command_buffer(core_renderer.get_current_frame());

            primary_command_buffer.nextSubpass(vk::SubpassContents::eSecondaryCommandBuffers);
        }

        draw_tree_func(*this, scene_graph_root, predraw);

        ++m_current_subpass_num;
    }
}

void aristaeus::gfx::vulkan::RasterizationLightingEnvironment::draw_subpass(bool predraw, 
                                                                            const std::function<void(RasterizationLightingEnvironment&)> &draw_func, 
                                                                            RasterizationSubpass subpass_type) {
    if (m_subpasses_enabled.contains(subpass_type) && !predraw) {
        if (m_current_subpass_num > 0 && !predraw) {
            auto &core_renderer = utils::get_singleton_safe<CoreRenderer>();

            vk::CommandBuffer primary_command_buffer =
                core_renderer.get_graphics_command_buffer_manager().get_primary_command_buffer(core_renderer.get_current_frame());

            primary_command_buffer.nextSubpass(vk::SubpassContents::eSecondaryCommandBuffers);
        }

        draw_func(*this);

        ++m_current_subpass_num;
    }
}

void aristaeus::gfx::vulkan::RasterizationLightingEnvironment::setup_ssao_ubo() {
    auto &core_renderer = utils::get_singleton_safe<CoreRenderer>();
    PerspectiveCameraEnvironment &camera_env = core_renderer.get_perspective_camera_environment();

    m_ssao_ubo_data = {
        .projection = camera_env.get_projection_matrix(),
        .ssao_intensity = 3,
        .ssao_bias = 0.025,
        .ssao_radius = 0.3
    };

    std::uniform_real_distribution<float> random_float(0.0, 1.0);

    for (size_t i = 0; SSAO_KERNEL_SIZE > i; ++i) {
        glm::vec3 hemisphere_sample{ (random_float(CoreRenderer::mersenne_twister_rng) * 2) - 1,
                                     (random_float(CoreRenderer::mersenne_twister_rng) * 2) - 1,
                                     random_float(CoreRenderer::mersenne_twister_rng) };

        hemisphere_sample = glm::normalize(hemisphere_sample);
        hemisphere_sample *= random_float(CoreRenderer::mersenne_twister_rng);

        m_ssao_ubo_data.samples[i] = glm::vec4{ hemisphere_sample, 0 };
    }

    m_ssao_ubo_buffer = core_renderer.get_memory_manager().create_buffer(core_renderer, sizeof(SSAOUboData), 
                                                                         vk::BufferUsageFlagBits::eUniformBuffer,
                                                                         vk::SharingMode::eExclusive,
                                                                         VMA_ALLOCATION_CREATE_HOST_ACCESS_SEQUENTIAL_WRITE_BIT,
                                                                         VMA_MEMORY_USAGE_AUTO, "ssao-ubo");
}

void aristaeus::gfx::vulkan::RasterizationLightingEnvironment::draw_env_imgui() {
    auto &core_renderer = utils::get_singleton_safe<CoreRenderer>();
    PerspectiveCameraEnvironment &cam_env = core_renderer.get_perspective_camera_environment();

    ImGui::Begin("Environment Info");

    bool only_rsm_indirect = utils::check_for_exclusive_bit_on(m_environment_ubo_data.gi_mode, 
                                                               RSM_POINT_MODE | RSM_DIRECTIONAL_MODE, ALL_LIGHTING_BITS);
    bool only_ibl = utils::check_for_exclusive_bit_on(m_environment_ubo_data.gi_mode, IBL_ENABLED, ALL_LIGHTING_BITS);
    bool only_ssao = utils::check_for_exclusive_bit_on(m_environment_ubo_data.gi_mode, SSAO_ENABLED, ALL_LIGHTING_BITS);
    bool only_direct = utils::check_for_exclusive_bit_on(m_environment_ubo_data.gi_mode, 
                                                         DIRECT_POINT_ENABLED | DIRECT_DIRECTIONAL_ENABLED, ALL_LIGHTING_BITS);
    bool all_light = (m_environment_ubo_data.gi_mode & ALL_LIGHTING_BITS) == ALL_LIGHTING_BITS;
    bool past_all_light = all_light;

    bool point_rsms_enabled = (m_environment_ubo_data.gi_mode & RSM_POINT_MODE) != 0;
    bool directional_rsms_enabled = (m_environment_ubo_data.gi_mode & RSM_DIRECTIONAL_MODE) != 0;
    bool ibl_enabled = (m_environment_ubo_data.gi_mode & IBL_ENABLED) != 0;
    bool ssao_enabled = (m_environment_ubo_data.gi_mode & SSAO_ENABLED) != 0;
    bool direct_directional_illumination_enabled = (m_environment_ubo_data.gi_mode & DIRECT_DIRECTIONAL_ENABLED) != 0;
    bool direct_point_illumination_enabled = (m_environment_ubo_data.gi_mode & DIRECT_POINT_ENABLED) != 0;

    if (ImGui::BeginTabBar("LightingTabBar", ImGuiTabBarFlags_None)) {
        if (ImGui::BeginTabItem("General info")) {
            ImGui::Text("Current camera position: (%f, %f, %f)", cam_env.get_camera_position().x, cam_env.get_camera_position().y, cam_env.get_camera_position().z);
            ImGui::Text("Current camera front: (%f, %f, %f)", cam_env.get_camera_front().x, cam_env.get_camera_front().y, cam_env.get_camera_front().z);
            ImGui::Text("Current camera up: (%f, %f, %f)", cam_env.get_camera_up().x, cam_env.get_camera_up().y, cam_env.get_camera_up().z);

            ImGui::Checkbox("Turn on all lighting", &all_light);

            ImGui::EndTabItem();
        }

        if (ImGui::BeginTabItem("Direct")) {
            ImGui::Text("DL #0 direction: (%f, %f, %f)", m_environment_ubo_data.directional_lights[0].position_direction.x,
                m_environment_ubo_data.directional_lights[0].position_direction.y, m_environment_ubo_data.directional_lights[0].position_direction.z);

            std::shared_ptr<DirectionalLight> directional_light = std::static_pointer_cast<DirectionalLight>(m_directional_lights[0].lock());

            if (directional_light != nullptr) {
                PerspectiveCameraEnvironment::Capture capture = directional_light->get_shadow_map_capture();

                ImGui::Text("DL #0 shadow map position: (%f, %f, %f)", capture.position.x, capture.position.y, capture.position.z);

                if (ImGui::Button("Toggle day night rotation")) {
                    if (directional_light->is_day_night_rotation_paused()) {
                        directional_light->resume_day_night_rotation();
                    }
                    else {
                        directional_light->pause_day_night_rotation();
                    }
                }
            }

            ImGui::Checkbox("Only direct illumination", &only_direct);

            ImGui::Checkbox("Point Lights enabled", &direct_point_illumination_enabled);
            ImGui::Checkbox("Directional Lights enabled", &direct_directional_illumination_enabled);

            if (only_direct) {
                direct_directional_illumination_enabled = true;
                direct_point_illumination_enabled = true;
                point_rsms_enabled = false;
                directional_rsms_enabled = false;
                ibl_enabled = false;
                ssao_enabled = false;
                all_light = false;
            }

            ImGui::EndTabItem();
        }

        if (ImGui::BeginTabItem("RSMs")) {
            ImGui::Checkbox("Only show RSM lighting", &only_rsm_indirect);
            ImGui::Checkbox("Point RSMs enabled", &point_rsms_enabled);
            ImGui::Checkbox("Directional RSMs enabled", &directional_rsms_enabled);

            ImGui::SliderInt("Num of RSM VPLs", &m_environment_ubo_data.num_of_rsm_vpls, 1, 1000);
            ImGui::SliderFloat("RSM lighting weighting", &m_environment_ubo_data.rsm_weighting, 0, 1);
            ImGui::SliderFloat("RSM sampling radius max", &m_environment_ubo_data.sampling_radius_max, 0, 1);

            ImGui::EndTabItem();

            if (only_rsm_indirect) {
                point_rsms_enabled = true;
                directional_rsms_enabled = true;
                ibl_enabled = false;
                ssao_enabled = false;
                direct_directional_illumination_enabled = false;
                direct_point_illumination_enabled = false;
                all_light = false;
            }
        }

        if (ImGui::BeginTabItem("SSAO")) {
            ImGui::Checkbox("Only show SSAO", &only_ssao);
            ImGui::Checkbox("SSAO enabled", &ssao_enabled);

            ImGui::SliderFloat("SSAO intensity", &m_ssao_ubo_data.ssao_intensity, 0, 10);
            ImGui::SliderFloat("SSAO bias", &m_ssao_ubo_data.ssao_bias, 0, 0.5f);
            ImGui::SliderFloat("SSAO radius", &m_ssao_ubo_data.ssao_radius, 0, 3.0f);

            ImGui::EndTabItem();

            if (only_ssao) {
                ssao_enabled = true;
                point_rsms_enabled = false;
                directional_rsms_enabled = false;
                ibl_enabled = false;
                direct_directional_illumination_enabled = false;
                direct_point_illumination_enabled = false;
                all_light = false;
            }
        }

        if (ImGui::BeginTabItem("IBL")) {
            ImGui::Checkbox("Only show IBL", &only_ibl);
            ImGui::Checkbox("IBL enabled", &ibl_enabled);

            if (only_ibl) {
                ibl_enabled = true;
                ssao_enabled = false;
                point_rsms_enabled = false;
                directional_rsms_enabled = false;
                direct_directional_illumination_enabled = false;
                direct_point_illumination_enabled = false;
                all_light = false;
            }

            ImGui::EndTabItem();
        }

        ImGui::EndTabBar();
    }

    m_environment_ubo_data.gi_mode = 0;

    if (past_all_light != all_light && all_light) {
        m_environment_ubo_data.gi_mode |= ALL_LIGHTING_BITS;
    }
    else {
        const std::vector<bool> flags_enabled{ point_rsms_enabled, directional_rsms_enabled, ibl_enabled, ssao_enabled,
                                       direct_directional_illumination_enabled, direct_point_illumination_enabled };
        const std::vector<int> flag_values{ RSM_POINT_MODE, RSM_DIRECTIONAL_MODE, IBL_ENABLED, SSAO_ENABLED, DIRECT_DIRECTIONAL_ENABLED, DIRECT_POINT_ENABLED };

        assert(flags_enabled.size() == flag_values.size());

        for (int i = 0; flags_enabled.size() > i; ++i) {
            if (flags_enabled[i]) {
                m_environment_ubo_data.gi_mode |= flag_values[i];
            }
        }
    }

    ImGui::End();
}

void aristaeus::gfx::vulkan::RasterizationLightingEnvironment::activate_environment(bool predraw) {
    auto &core_renderer = utils::get_singleton_safe<CoreRenderer>();

    RenderPhase &render_phase = *core_renderer.get_current_render_phase();
    bool using_shadow_mapping = render_phase.is_render_pass_depth_only(render_phase.get_current_render_pass());

    std::shared_ptr<gfx::SceneGraphNode> scene_graph_node_root_strong = m_scene_graph_node_root.lock();

    if(scene_graph_node_root_strong == nullptr) {
        return;
    }

    if (!using_shadow_mapping && 
        &m_gbuffer_render_phase != core_renderer.get_current_render_phase() &&
        &m_composition_render_phase != core_renderer.get_current_render_phase() &&
        &m_ssao_render_phase != core_renderer.get_current_render_phase() &&
        m_occlusion_query_render_phase != core_renderer.get_current_render_phase() && 
        m_final_high_res_render_phase != core_renderer.get_current_render_phase()) {
        return;
    }

    m_current_subpass_num = 0;

    if (!predraw && &m_gbuffer_render_phase == core_renderer.get_current_render_phase()) {
        draw_env_imgui();
    }

    if (using_shadow_mapping) {
        depth_only_subpass(scene_graph_node_root_strong, predraw);
    } else {
        draw_tree_subpass(scene_graph_node_root_strong, predraw, 
                          [](RasterizationLightingEnvironment &env, std::shared_ptr<gfx::SceneGraphNode> &root, bool predraw) {
                              env.gbuffer_subpass(root, predraw);
                          }, RasterizationSubpass::DEFERRED);

        draw_subpass(predraw, 
                     [](RasterizationLightingEnvironment &env) {
                          env.occlusion_query_subpass();
                     }, RasterizationSubpass::SCREEN_SPACE_INTERP);
        draw_subpass(predraw, [](RasterizationLightingEnvironment &env) {
                          env.ssao_subpass();
                     }, RasterizationSubpass::SSAO);
        draw_subpass(predraw, [](RasterizationLightingEnvironment &env) {
                          env.ssao_blur_subpass();
                     }, RasterizationSubpass::SSAO_BLUR);
        draw_subpass(predraw, [](RasterizationLightingEnvironment &env) {
                          env.composition_subpass();
                     }, RasterizationSubpass::COMPOSITION);

        draw_tree_subpass(scene_graph_node_root_strong, predraw, 
                    [](RasterizationLightingEnvironment &env, std::shared_ptr<gfx::SceneGraphNode> &root, bool predraw) {
                        env.forward_subpass(root, predraw);
                    }, RasterizationSubpass::FORWARD);
    }
}


bool aristaeus::gfx::vulkan::RasterizationLightingEnvironment::is_captured() const {
    if (m_captured_sky_box != nullptr) {
        return m_captured_sky_box->has_been_captured();
    }
    else {
        return true;
    }
}

void aristaeus::gfx::vulkan::RasterizationLightingEnvironment::capture_points(bool force_recapture) {
    auto &core_renderer = utils::get_singleton_safe<CoreRenderer>();

    if (m_captured_sky_box != nullptr) {
        if (force_recapture) {
            m_captured_sky_box->capture_points();
        }
        else {
            m_captured_sky_box->capture_needed_points();
        }
    }
}

void aristaeus::gfx::vulkan::RasterizationLightingEnvironment::capture_point_light_shadows(bool force_recapture) {
    auto &core_renderer = utils::get_singleton_safe<CoreRenderer>();

    if (m_captured_sky_box_point_light_shadows != nullptr) {
        if (force_recapture) {
            m_captured_sky_box_point_light_shadows->capture_points();
        }
        else {
            m_captured_sky_box_point_light_shadows->capture_needed_points();
        }
    }
}

void aristaeus::gfx::vulkan::RasterizationLightingEnvironment::reapply_graphics_pipeline(const vk::CommandBuffer &command_buffer) {
    // TODO
}

void aristaeus::gfx::vulkan::RasterizationLightingEnvironment::recreate_graphics_pipelines(const vk::Viewport &viewport, const vk::Rect2D &viewport_scissor) {
    // TODO
}

aristaeus::gfx::vulkan::GraphicsPipeline::PipelineInfo
aristaeus::gfx::vulkan::RasterizationLightingEnvironment::get_forward_pipeline_pipeline_info(ForwardPipelineType pipeline_type) {
    std::array<std::vector<vk::DescriptorSetLayoutBinding>, 2> base_material_environment_descriptor_set_bindings = BASE_FORWARD_DESCRIPTOR_SET_BINDINGS;
    std::string pipeline_cache_suffix;

    switch (pipeline_type) {
    case ForwardPipelineType::InstancedEnvironment:
        pipeline_cache_suffix = "_instanced";
        break;
    #ifndef NDEBUG
    case ForwardPipelineType::DebugNormals:
        pipeline_cache_suffix = "_debugging_normals";
        break;
    #endif
    case ForwardPipelineType::RegularEnvironment:
        break;
    }

    std::reference_wrapper<vk::raii::RenderPass> render_pass = 
        m_composition_render_phase.get_render_pass(std::string{ SSAO_BLUR_COMPOSE_RENDER_PASS_NAME });

    if (m_using_screen_space_interp) {
        if (m_final_high_res_render_phase == nullptr) {
            assert(m_occlusion_query_render_phase == nullptr);

            create_screen_space_interp_render_phases();
        }

        render_pass = m_final_high_res_render_phase->get_render_pass(std::string{DEFERRED_RENDER_PASS_NAME});
    }

    return {
            .vertex_input_stage = { {}, gfx::vulkan::AssimpModel::MODEL_BINDING_DESCRIPTION,
                                    gfx::vulkan::AssimpModel::MODEL_ATTRIBUTE_DESCRIPTIONS },
            .input_assembly_stage = {{}, vk::PrimitiveTopology::eTriangleList, VK_FALSE},
            .viewport_stage = {{}, 1, nullptr, 1, nullptr},
            .rasterization_stage = {{}, VK_FALSE, VK_FALSE, vk::PolygonMode::eFill, vk::CullModeFlagBits::eBack,
                                    vk::FrontFace::eCounterClockwise, {}, {}, {}, {}, 1.0f},
            .multisample_stage = {{}, vk::SampleCountFlagBits::e1, VK_FALSE, 1.0f, nullptr, VK_FALSE, VK_FALSE},
            .depth_stencil_stage = {{}, VK_TRUE, VK_TRUE, vk::CompareOp::eLessOrEqual, VK_FALSE, VK_FALSE, {}, {}, 0.0f, 1.0f},
            .blending_stage = {{}, VK_FALSE, vk::LogicOp::eCopy, FORWARD_COLOR_BLEND_ATTACHMENT, {0, 0, 0, 0}},

            .dynamic_stage = {{}, FORWARD_DYNAMIC_STATE},
            .push_content_ranges = {},

            .descriptor_bindings = {base_material_environment_descriptor_set_bindings.cbegin(),
                                    base_material_environment_descriptor_set_bindings.cend()},

            .sub_pass = m_using_screen_space_interp ? 1u : 2u,
            .pipeline_cache_path = "environment_pipeline_cache" + pipeline_cache_suffix,
            .render_pass = render_pass.get()
    };
}

aristaeus::gfx::vulkan::GraphicsPipeline::PipelineInfo
aristaeus::gfx::vulkan::RasterizationLightingEnvironment::get_depth_map_pipeline_info(vk::raii::RenderPass &render_pass,
                                                                                 const vk::CullModeFlagBits &cull_mode) {
    return {
            .vertex_input_stage = { {}, gfx::vulkan::AssimpModel::MODEL_BINDING_DESCRIPTION,
                                    gfx::vulkan::AssimpModel::MODEL_ATTRIBUTE_DESCRIPTIONS },
            .input_assembly_stage = {{}, vk::PrimitiveTopology::eTriangleList, VK_FALSE},
            .viewport_stage = {{}, 1, nullptr, 1, nullptr},
            .rasterization_stage = {{}, VK_FALSE, VK_FALSE, vk::PolygonMode::eFill, cull_mode,
                                    vk::FrontFace::eCounterClockwise, VK_TRUE, {}, {}, {}, 1.0f},
            .multisample_stage = {{}, vk::SampleCountFlagBits::e1, VK_FALSE, 1.0f, nullptr, VK_FALSE, VK_FALSE},
            .depth_stencil_stage = {{}, VK_TRUE, VK_TRUE, vk::CompareOp::eLessOrEqual, VK_FALSE, VK_FALSE, {}, {}, 0.0f,
                                    1.0f},
            .blending_stage = {{}, VK_FALSE, vk::LogicOp::eCopy, FORWARD_COLOR_BLEND_ATTACHMENT, {0, 0, 0, 0}},

            .dynamic_stage = {{}, DEPTH_MAP_DYNAMIC_STATE },
            .push_content_ranges = {},

            .descriptor_bindings = {{BASE_DEPTH_MAP_DESCRIPTOR_SET_BINDING}, {}},

            .sub_pass = 0,
            .pipeline_cache_path = "depth_mapping_pipeline_cache_" + std::to_string(static_cast<uint32_t>(cull_mode)),

            .render_pass = render_pass
    };
}

std::vector<aristaeus::gfx::vulkan::GraphicsPipeline::ShaderInfo>
aristaeus::gfx::vulkan::RasterizationLightingEnvironment::get_depth_map_shader_infos() {
    return {
            {
                    .code = utils::read_file("environments/vulkan_environment_shaders/engine/depth_map.vert.spv"),
                    .name = "depth_map.vert",
                    .stage = vk::ShaderStageFlagBits::eVertex
            },
            {
                    .code = utils::read_file("environments/vulkan_environment_shaders/engine/depth_map.frag.spv"),
                    .name = "depth_map.frag",
                    .stage = vk::ShaderStageFlagBits::eFragment
            }
    };
}

std::vector<aristaeus::gfx::vulkan::GraphicsPipeline::ShaderInfo>
aristaeus::gfx::vulkan::RasterizationLightingEnvironment::get_forward_pipeline_shader_infos(ForwardPipelineType pipeline_type) {
#ifdef NDEBUG
    bool debugging_normals = false;
#else
    bool debugging_normals = pipeline_type == ForwardPipelineType::DebugNormals;
#endif
    bool instanced_rendering = pipeline_type == ForwardPipelineType::InstancedEnvironment;

    std::string fragment_shader_name = debugging_normals ? "debug/color_normals.frag" : "environment_shader.frag";
    std::string vertex_shader_name =
        debugging_normals ? "debug/color_normals.vert" : (instanced_rendering ? "environment_shader_instanced.vert" : "environment_shader.vert");

    std::vector<GraphicsPipeline::ShaderInfo> base_vector{
            {
                    .code = utils::read_file("environments/vulkan_environment_shaders/engine/" + vertex_shader_name + ".spv"),
                    .name = vertex_shader_name,
                    .stage = vk::ShaderStageFlagBits::eVertex
            },
            {
                    .code = utils::read_file("environments/vulkan_environment_shaders/engine/" + fragment_shader_name + ".spv"),
                    .name = fragment_shader_name,
                    .stage = vk::ShaderStageFlagBits::eFragment
            }
    };

    if (debugging_normals) {
        base_vector.push_back({
            .code = utils::read_file("environments/vulkan_environment_shaders/debug/color_normals.geom.spv"),
            .name = "color_normals.geom",
            .stage = vk::ShaderStageFlagBits::eGeometry
        });
    }

    return base_vector;
}

std::vector<aristaeus::gfx::vulkan::GraphicsPipeline::ShaderInfo> 
aristaeus::gfx::vulkan::RasterizationLightingEnvironment::get_screen_space_interp_pipeline_shader_infos() {
    return {
        {
                .code = utils::read_file("environments/vulkan_environment_shaders/engine/quad_render.vert.spv"),
                .name = "quad_render.vert",
                .stage = vk::ShaderStageFlagBits::eVertex
        },
        {
                .code = utils::read_file("environments/vulkan_environment_shaders/engine/screen_space_interpolation.frag.spv"),
                .name = "screen_space_interpolation.frag",
                .stage = vk::ShaderStageFlagBits::eFragment
        }
    };
}

aristaeus::gfx::vulkan::GraphicsPipeline::PipelineInfo 
aristaeus::gfx::vulkan::RasterizationLightingEnvironment::get_screen_space_interp_pipeline_pipeline_info() {
    std::vector<RenderPhase::AttachmentInfo> gbuffer_attachments = get_gbuffer_attachment_info();

    for (int i = 1; gbuffer_attachments.size() > i; ++i) {
        if (gbuffer_attachments[i].description.finalLayout == vk::ImageLayout::eColorAttachmentOptimal) {
            m_blend_attachment_states.push_back(FORWARD_COLOR_BLEND_ATTACHMENT);
        }
    }
    
    return {
            .vertex_input_stage = {{}, {}, {}},
            .input_assembly_stage = {{}, vk::PrimitiveTopology::eTriangleList, VK_FALSE},
            .viewport_stage = {{}, 1, nullptr, 1, nullptr},
            .rasterization_stage = {{}, VK_FALSE, VK_FALSE, vk::PolygonMode::eFill, vk::CullModeFlagBits::eNone,
                                    vk::FrontFace::eCounterClockwise, VK_TRUE, {}, {}, {}, 1.0f},
            .multisample_stage = {{}, vk::SampleCountFlagBits::e1, VK_FALSE, 1.0f, nullptr, VK_FALSE, VK_FALSE},
            .depth_stencil_stage = {{}, VK_TRUE, VK_TRUE, vk::CompareOp::eLessOrEqual, VK_FALSE, VK_FALSE, {}, {}, 0.0f,
                                    1.0f},
            .blending_stage = {{}, VK_FALSE, vk::LogicOp::eCopy, FORWARD_COLOR_BLEND_ATTACHMENT, {0, 0, 0, 0}},

            .dynamic_stage = {{}, {} },
            .push_content_ranges = {{vk::ShaderStageFlagBits::eVertex, 0, sizeof(QuadRenderPushConstants)},
                                    {vk::ShaderStageFlagBits::eFragment, sizeof(QuadRenderPushConstants), sizeof(glm::mat4) + sizeof(uint32_t)}},

            .descriptor_bindings = {BASE_SCREEN_SPACE_INTERPOLATION_SET_BINDINGS.cbegin(),
                                    BASE_SCREEN_SPACE_INTERPOLATION_SET_BINDINGS.cend()},

            .sub_pass = 1,
            .pipeline_cache_path = "screen_space_interp_pipeline_cache",

            .render_pass = m_occlusion_query_render_phase->get_render_pass(std::string{OCCLUSION_QUERY_RENDER_PASS_NAME})
    };
}

void aristaeus::gfx::vulkan::RasterizationLightingEnvironment::categorize_lighting_nodes_recursive(SceneGraphNode &current_node) {
    std::queue<std::shared_ptr<gfx::SceneGraphNode>> children;

    children.push(current_node.get_weak_ptr_to_this().lock());

    while (!children.empty()) {
        std::shared_ptr<gfx::SceneGraphNode> child = children.front();

        if (child->is_part_of_node_group(std::string{ SceneGraphNode::BLENDED_OBJECT_GROUP })) {
            m_blended_nodes.push_back(child);
        }

        if (child->is_part_of_node_group(std::string{ LightingNode::LIGHTING_NODE_GROUP })) {
            std::shared_ptr<LightingNode> lighting_node = std::static_pointer_cast<LightingNode>(child);

            add_lighting_node(lighting_node);
        }

        for (std::shared_ptr<gfx::SceneGraphNode> &second_level_child : child->get_children()) {
            children.push(second_level_child);
        }

        children.pop();
    }
}


void aristaeus::gfx::vulkan::RasterizationLightingEnvironment::added_to_camera_environment() {
    m_blended_nodes.clear();
    m_point_lights = {};
    m_directional_lights = {};

    std::shared_ptr<gfx::SceneGraphNode> scene_graph_node = m_scene_graph_node_root.lock();

    if (m_captured_sky_box != nullptr) {
        m_captured_sky_box->capture_points();
    }

    if (m_captured_sky_box_point_light_shadows != nullptr) {
        m_captured_sky_box_point_light_shadows->capture_points();
    }

    if (scene_graph_node != nullptr) {
        categorize_lighting_nodes_recursive(*scene_graph_node);
        m_reflective_shadow_map->set_scene_graph_node_root(scene_graph_node);

        /*scene_graph_node->add_callback_for_event<std::reference_wrapper<gfx::SceneGraphNode>, RasterizationLightingEnvironment>(std::string{SceneGraphNode::DESCENDENT_NODE_ADDED_EVENT}, "rasterization_lighting_env_descendent_node_added_event",
                                                                                                                                [](RasterizationLightingEnvironment &lighting_env, std::reference_wrapper<gfx::SceneGraphNode> node) {
                                                                                                                                    lighting_env.                                                                                                                    
                                                                                                                                }, *this);*/
    }
}

vk::raii::Sampler aristaeus::gfx::vulkan::RasterizationLightingEnvironment::create_shadow_mapping_sampler() {
    vk::SamplerCreateInfo sampler_create_info = SHADOW_MAPPING_SAMPLER_CREATE_INFO;

    sampler_create_info.addressModeU = vk::SamplerAddressMode::eClampToBorder;
    sampler_create_info.addressModeV = vk::SamplerAddressMode::eClampToBorder;
    sampler_create_info.borderColor = vk::BorderColor::eFloatOpaqueWhite;
    sampler_create_info.maxAnisotropy = utils::get_singleton_safe<CoreRenderer>().get_max_sampler_anisotropy();

    return { utils::get_singleton_safe<CoreRenderer>().get_logical_device_ref(), sampler_create_info, nullptr };
}

void aristaeus::gfx::vulkan::RasterizationLightingEnvironment::input(Window &window) {
    bool p_pressed = window.is_key_pressed_with_delay(GLFW_KEY_P, 500);
    bool l_pressed = window.is_key_pressed_with_delay(GLFW_KEY_L, 500);

    if (p_pressed) {
        if ((m_environment_ubo_data.gi_mode & RSM_DIRECTIONAL_MODE) != 0) {
            m_environment_ubo_data.gi_mode &= ~RSM_DIRECTIONAL_MODE;
        }
        else {
            m_environment_ubo_data.gi_mode |= RSM_DIRECTIONAL_MODE;
        }
    }

    if (l_pressed) {
        if ((m_environment_ubo_data.gi_mode & RSM_POINT_MODE) != 0) {
            m_environment_ubo_data.gi_mode &= ~RSM_POINT_MODE;
        }
        else {
            m_environment_ubo_data.gi_mode |= RSM_POINT_MODE;
        }
    }
}
