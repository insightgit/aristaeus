//
// Created by bobby on 8/26/21.
//

#ifndef ARISTAEUS_ASSIMP_MODEL_HPP
#define ARISTAEUS_ASSIMP_MODEL_HPP

#include <filesystem>
#include <iostream>
#include <string>
#include <unordered_set>
#include <vector>

#include <assimp/Importer.hpp>
#include <assimp/postprocess.h>
#include <assimp/scene.h>

#include <boost/algorithm/string.hpp>

#include <stb_image.h>

#include "vulkan_raii.hpp"

#include "assimp_mesh.hpp"
#include "model_info.hpp"
#include "render_phase.hpp"
#include "texture.hpp"
#include "uniform_buffer.hpp"

#include "geometry/geometry.hpp"

#include "../scene_graph_node.hpp"

namespace aristaeus::gfx::vulkan {
    class CoreRenderer;

    class AssimpModel : public SceneGraphNode {
    public:
        ARISTAEUS_CLASS_HEADER("assimp_model", AssimpModel, true);

        static constexpr std::string_view DIFFUSE_TEXTURE_STRING = "texture_diffuse";
        static constexpr std::string_view NORMAL_TEXTURE_STRING = "texture_normal";

        // PBR texture strings
        static constexpr std::string_view METALLIC_TEXTURE_STRING = "texture_metallic";
        static constexpr std::string_view ROUGHNESS_TEXTURE_STRING = "texture_roughness";
        static constexpr std::string_view AO_TEXTURE_STRING = "texture_ao";

        static constexpr std::string_view BLENDED_MODEL_PREFIX_STRING = "bl_";

        static constexpr std::array<std::string_view, 5> TEXTURE_TYPES{ DIFFUSE_TEXTURE_STRING, NORMAL_TEXTURE_STRING, METALLIC_TEXTURE_STRING,
                                                                        ROUGHNESS_TEXTURE_STRING, AO_TEXTURE_STRING };
        static constexpr std::array<aiTextureType, 5> ASSIMP_TEXTURE_TYPES{ aiTextureType_DIFFUSE, aiTextureType_NORMALS, aiTextureType_METALNESS,
                                                                                aiTextureType_DIFFUSE_ROUGHNESS, aiTextureType_AMBIENT_OCCLUSION };
        static constexpr std::array<uint32_t, 5> TEXTURE_BINDING_LOCATIONS{ 1, 2, 3, 4, 5 };
        static constexpr uint32_t MATERIAL_SSBO_BINDING_LOCATION = 0;

        static constexpr std::string_view NODE_TYPE = "assimp_model";

        static constexpr size_t BLENDING_NODE_RENDER_PRIORITY = 1000;

        static constexpr vk::PipelineColorBlendAttachmentState BLENDED_MODEL_STATE =
        { VK_TRUE, vk::BlendFactor::eSrcAlpha, vk::BlendFactor::eDstAlpha, vk::BlendOp::eAdd,
         vk::BlendFactor::eSrcAlpha, vk::BlendFactor::eDstAlpha, vk::BlendOp::eMax,
         vk::ColorComponentFlagBits::eR | vk::ColorComponentFlagBits::eG | vk::ColorComponentFlagBits::eB | vk::ColorComponentFlagBits::eA };

        struct MaterialUniformBuffer {
            alignas(4) int32_t override_texture;
            alignas(4) int32_t object_type;

            alignas(16) glm::vec3 light_box_color;

            alignas(16) PBRMaterialInfo pbr_material_info;
        };

        static const vk::ImageViewCreateInfo DEFAULT_IMAGE_VIEW_CREATE_INFO;
        static constexpr vk::SamplerCreateInfo DEFAULT_SAMPLER_CREATE_INFO {{}, vk::Filter::eLinear,
                                                                            vk::Filter::eLinear,
                                                                            vk::SamplerMipmapMode::eLinear,
                                                                            vk::SamplerAddressMode::eRepeat,
                                                                            vk::SamplerAddressMode::eRepeat,
                                                                            vk::SamplerAddressMode::eRepeat, 0.0f,
                                                                            VK_TRUE, {}, VK_FALSE,
                                                                            vk::CompareOp::eAlways, 0.0f, 0.0f,
                                                                            vk::BorderColor::eIntOpaqueBlack, VK_FALSE};

        static constexpr vk::VertexInputBindingDescription MODEL_BINDING_DESCRIPTION { 0, sizeof(ModelVertex), vk::VertexInputRate::eVertex };

        static constexpr std::array<vk::VertexInputAttributeDescription, 4> MODEL_ATTRIBUTE_DESCRIPTIONS {{
            {0, 0, vk::Format::eR32G32B32Sfloat, offsetof(ModelVertex, position)},
            {1, 0, vk::Format::eR32G32B32Sfloat, offsetof(ModelVertex, normal)},
            {2, 0, vk::Format::eR32G32Sfloat, offsetof(ModelVertex, texture_coords)},
            {3, 0, vk::Format::eR32G32B32Sfloat, offsetof(ModelVertex, tangent)},
        }};

        void on_parsing_done();

        bool should_debug_with_normal_shader() const {
            return m_debug_normals;
        }

        bool uses_blending() const {
            return m_uses_blending;
        }

        [[nodiscard]] std::string get_model_path() const {
            return m_model_path;
        }

        bool operator==(AssimpModel &other) const noexcept {
            return m_model_path == other.m_model_path;
        }

        [[nodiscard]] std::string get_model_name() const {
            return m_model_name;
        }

        [[nodiscard]] glm::vec3 get_light_box_color() const {
            return m_light_box_color;
        }

        [[nodiscard]] bool use_front_face_culling_shadow_mapping() {
            return m_use_front_culling_shadow_mapping;
        }

        [[nodiscard]] PBRMaterialInfo get_pbr_material_info() const {
            return m_custom_pbr_material_info;
        }

        // NOTE: this can be a very expensive operation, because we are creating
        // a copy of all the vertices and all the indices of every mesh in this AssimpModel.
        // You have been warned!
        [[nodiscard]] std::vector<AssimpMesh::MeshGeometryData> get_geometry_data_of_meshes() const;

        [[nodiscard]] bool is_light_emitter() const {
            return m_light_emitter;
        }

        void set_pbr_material_info(const PBRMaterialInfo &pbr_material_info) {
            m_custom_pbr_material_info = pbr_material_info;
        }

        void set_light_emitter(bool light_emitter) {
            m_light_emitter = light_emitter;
        }

        void set_light_box_color(const glm::vec3 &light_box_color) {
            m_light_box_color = light_box_color;
        }

        void predraw_load(const vk::CommandBuffer &command_buffer, const glm::vec3 &camera_position) override {};

        void draw()override;
    private:
        static constexpr std::string_view CYLINDER_GEOMETRY_TYPE = "cylinder";
        static constexpr std::string_view RECTANGULAR_PRISM_GEOMETRY_TYPE = "rectangular_prism";
        static constexpr std::string_view SPHERE_GEOMETRY_TYPE = "sphere";
        static constexpr std::string_view TORUS_GEOMETRY_TYPE = "torus";
        static constexpr std::string_view NO_PRIM_GEOMETRY_TYPE = "not_primitive";

        static const std::unordered_set<std::string_view> GEOMETRY_TYPES;

        struct GeometryUIData {
            std::string_view geom_type_selected;
            std::vector<float> supplemental_data;
            ModelInfo model_info;
        };

        struct ModelMaterial {
            Material material;
            PBRMaterialInfo pbr_info;
        };

        MaterialShaderData::GeneratorData get_material_mesh_ssbo_data(size_t material_mesh_index);

        std::vector<MaterialTexture> load_material_textures(aiMaterial &material, aiTextureType type, const std::string &type_name,
                                                            uint32_t binding_location);
        void process_material(aiMaterial *material);
        std::shared_ptr<AssimpMesh> process_mesh(aiMesh *mesh, const aiScene *scene, bool bake_physx_shapes, uint32_t mesh_index);

        void process_node(aiNode *node, const aiScene *scene, CoreRenderer &renderer, bool bake_physx_shapes);

        void register_pipeline_info_and_render_context_id(const GraphicsPipeline::PipelineInfo &pipeline_info, 
                                                          const std::vector<GraphicsPipeline::ShaderInfo> &shader_infos,
                                                          RenderContextID render_context_id);

        bool m_has_inited = false;
        glm::vec3 m_bottom_right_bound = glm::vec3(-10000.0f);
        glm::vec3 m_top_left_bound = glm::vec3(10000.0f);

        std::unique_ptr<Geometry> m_prim_geometry;

        std::vector<std::pair<std::shared_ptr<AssimpMesh>, std::string>> m_assimp_meshes;
        PBRMaterialInfo m_custom_pbr_material_info;
        // assimp mesh with its material name for material overrides
        bool m_debug_normals;
        bool m_flip = false;
        std::vector<std::shared_ptr<Texture>> m_loaded_textures;
        
        // TODO(Bobby): rethink implementation for MaterialOverrides
        std::unordered_map<std::string, std::vector<MaterialOverride>> m_material_overrides;
        std::vector<TextureOverride> m_texture_overrides;
        std::string m_model_directory;
        std::string m_model_name;
        std::string m_model_path;
        bool m_override_texture;
        std::optional<std::string> m_default_normal_map_texture;
        uint32_t m_last_frame_rendered = utils::vulkan::MAX_FRAMES_IN_FLIGHT + 1;
        RenderContextID m_last_render_context = -1;
        bool m_light_emitter = false;
        glm::vec3 m_light_box_color{ 0.0f };

        bool m_use_front_culling_shadow_mapping;
        bool m_bake_physx_shapes;
        bool m_uses_blending;

        std::vector<ModelMaterial> m_materials_per_mesh;
        std::unordered_map<RenderContextID, std::vector<MaterialID>> m_render_id_to_mat_ids_per_mesh;
        
        std::unordered_map<size_t, bool> m_ubo_written_render_mesh_id;
        MaterialUniformBuffer m_material_uniform_buffer;

        std::unordered_map<RenderContextID, std::vector<vk::PipelineColorBlendAttachmentState>> m_blended_object_attachment_states;
    };
}

#endif //ARISTAEUS_ASSIMP_MODEL_HPP
