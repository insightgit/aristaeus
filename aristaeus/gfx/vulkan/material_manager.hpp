#ifndef ARISTAEUS_MATERIAL_MANAGER_HPP
#define ARISTAEUS_MATERIAL_MANAGER_HPP

#include "core_renderer.hpp"
#include "texture.hpp"

#include "../../libraries/include/sha256.h"

namespace aristaeus::gfx::vulkan {
	struct MaterialShaderData {
		struct GeneratorData {
			size_t generated_data_size;
			void *generated_data;
		};

		size_t data_size;

		std::function<GeneratorData (void *)> data_generator; 
		// this function needs to spit out generated_data that
		// matches with the generated data it is spitting out for
		// the same argument for this frame.
		// data_generator function is responsible for freeing
		// any data under generated_data when its not used
		// (e.g. after the same frame gets recorded again)

		void *data_generator_arg;
		size_t data_generator_arg_size;

		uint32_t bind_location;
		vk::ShaderStageFlags used_by_shader_stages;

		size_t m_data_offset; 
	};

	struct MaterialTexture {
		std::string texture_path;
		bool null_image;

		std::optional<vk::SamplerCreateInfo> sampler_create_info;
		std::optional<vk::ImageViewCreateInfo> image_view_create_info;
		std::unordered_set<std::string> texture_types;

		uint32_t bind_location;
		SharedImageRaii manual_image; // not recommended, no batched loading
		std::shared_ptr<vk::raii::Sampler> manual_sampler; // only used if manual_image is used, ignored elsewise
		std::shared_ptr<vk::raii::ImageView> manual_image_view; // see above note

		vk::ShaderStageFlags used_by_shader_stages;
		bool dirty;

		bool operator==(const MaterialTexture &other) const noexcept {
			bool texture_bind_loc_checks = manual_image != nullptr ? true : texture_path == other.texture_path &&
										   bind_location == other.bind_location;
			bool manual_image_checks = manual_image == other.manual_image;

			return texture_bind_loc_checks && manual_image_checks;
		}
	};

	struct Material {
		std::optional<std::string> material_name;
		std::vector<MaterialShaderData> data;
		std::vector<MaterialTexture> textures;
		GraphicsPipeline::PipelineInfo pipeline_info;
		std::vector<GraphicsPipeline::ShaderInfo> shader_infos;
		std::optional<uint32_t> material_set_num;
		std::optional<uint32_t> material_render_priority; // if not filled in, we give it a default render property of zero

		bool operator==(const Material &other) const noexcept {
			return pipeline_info == other.pipeline_info && shader_infos == other.shader_infos;
		}
	};

	typedef uint32_t MaterialID;
}

template <>
struct std::hash<aristaeus::gfx::vulkan::Material> {
	std::size_t operator()(const aristaeus::gfx::vulkan::Material &material) const noexcept {
		aristaeus::gfx::vulkan::GraphicsPipeline::PipelineShaderHash pipeline_shader_hash{};

		return pipeline_shader_hash({ material.pipeline_info, material.shader_infos });
	}
};

namespace aristaeus::gfx::vulkan {
	class MaterialManager : public core::Singleton<MaterialManager> {
	public:
		typedef std::function<void(MaterialID)> SuperMaterialBindFunction;

		MaterialManager();

		void setup_null_texture() {
			if (m_null_texture == nullptr) {
				m_null_texture = std::make_unique<Texture>("../aristaeus/gfx/null_image.png",
					std::unordered_set<std::string>{"everything"},
					utils::get_singleton_safe<CoreRenderer>());
			}
		}

		[[nodiscard]] vk::DescriptorImageInfo get_null_image_info() {
			setup_null_texture();

			return m_null_texture->get_descriptor_image_info(utils::get_singleton_safe<CoreRenderer>());
		}

		[[nodiscard]] size_t get_super_material_index(MaterialID material_id) const {
			return m_material_id_to_data.at(material_id);
		}

		MaterialID create_material(Material material);

		[[nodiscard]] GraphicsPipeline &get_graphics_pipeline_for_material(MaterialID material);

		[[nodiscard]] size_t get_vertex_binding_size(MaterialID material, uint32_t binding = 0) const;
		[[nodiscard]] uint32_t get_first_image_index_within_binding(MaterialID material, uint32_t binding) const;

		[[nodiscard]] MaterialID get_current_binded_material_id() const {
			return m_current_binded_material_id;
		}

		[[nodiscard]] size_t get_material_render_priority(MaterialID material_id) const {
			const SuperMaterialData &material_data = m_material_datas.at(m_material_id_to_data.at(material_id));

			return material_data.initial_material.material_render_priority.has_value() ? 
				   *material_data.initial_material.material_render_priority : 0;
		}

		// useful for binding common environment sets such as camera or environment ubos
		void call_upon_super_material_binds(RenderContextID render_context_id, 
											const SuperMaterialBindFunction &super_material_bind_callback);
	private:
		friend class MeshManager;

		typedef uint32_t LocalMaterialID;

		struct SuperMaterialResources {
			vk::raii::DescriptorSetLayout descriptor_set_layout;
			vk::raii::DescriptorPool descriptor_pool;

			std::array<vk::raii::DescriptorSet, utils::vulkan::MAX_FRAMES_IN_FLIGHT> descriptor_sets;
			uint32_t last_frame_rendered;

			// maps a MaterialID to its binding spots with blank or full textures/buffers
			// if a texture in a binding slot is null, it will either be
			// a null image (if the corresponding material texture doesn't have a manual image)
			// or the manual image.
			std::vector<std::vector<std::unique_ptr<Texture>>> textures;
			BufferRaii storage_buffer;

			GraphicsPipeline graphics_pipeline;
		};

		struct SuperMaterialData {
			Material initial_material;

			std::vector<std::vector<MaterialShaderData>> data;
			std::vector<std::vector<MaterialTexture>> textures;
			std::unordered_map<MaterialID, LocalMaterialID> materials;
			std::vector<std::unordered_map<uint32_t, uint32_t>> first_image_binding_index; // the first array index for images bound to a certain binding location

			std::vector<MaterialID> added_materials;

			std::unique_ptr<SuperMaterialResources> resources;
		};

		// only called by MeshManager, the command buffer returned is
		// the command buffer that all future commands should go into
		// (creates a way of handling subpasses)
		vk::CommandBuffer bind_material(MaterialID material, RenderContextID render_context_id);

		[[nodiscard]] LocalMaterialID get_local_material_id_within_super_material(MaterialID material) const {
			return m_material_datas[m_material_id_to_data.at(material)].materials.at(material);
		}

		void initialize_super_material(MaterialID queued_material_id);

		void write_material_descriptor_set(SuperMaterialData &super_material_data);

		MaterialID m_current_material_id;
		MaterialID m_current_binded_material_id = ~0;

		std::unordered_map<Material, MaterialID> m_initial_materials; // keys aren't guranteed to be completely up to sync with new data 
																	  // aside from data which is hashed (pipeline and shader info)
		std::unordered_set<MaterialID> m_queued_material_ids; // queued for initialization and pipeline creation

		// multiple MaterialIDs can share the same SuperMaterialData 
		// (e.g. if multiple materials share the same pipeline)
		std::vector<SuperMaterialData> m_material_datas;
		std::unordered_map<MaterialID, size_t> m_material_id_to_data;
		
		std::unordered_map<RenderContextID, SuperMaterialBindFunction> m_on_super_material_bind_functions;

		std::unique_ptr<Texture> m_null_texture;
	};
}

#endif