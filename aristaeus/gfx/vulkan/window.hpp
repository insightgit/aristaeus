//
// Created by bobby on 5/23/22.
//

#ifndef ARISTAEUS_WINDOW_HPP
#define ARISTAEUS_WINDOW_HPP

#include <chrono>
#include <map>
#include <optional>
#include <limits>
#include <set>
#include <iostream>
#include <limits>
#include <unordered_map>
#include <unordered_set>
#include <vector>

#include "glfw_create_window_surface.hpp"

#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>
#include "vulkan_raii.hpp"

#include <imgui.h>
#include <imgui_impl_glfw.h>
#include <imgui_impl_vulkan.h>

#define VMA_STATIC_VULKAN_FUNCTIONS 0
#define VMA_DYNAMIC_VULKAN_FUNCTIONS 1
#ifdef _WIN32
#include <vma/vk_mem_alloc.h>
#else
#include <vk_mem_alloc.h>
#endif

namespace aristaeus::gfx::vulkan {
    class Window {
    public:
        #ifdef NDEBUG
                static constexpr bool VALIDATION_LAYERS_ENABLED = false;
                static constexpr bool PERFORMANCE_MARKERS_ENABLED = false;
        #else
                static constexpr bool VALIDATION_LAYERS_ENABLED = true;
                static constexpr bool PERFORMANCE_MARKERS_ENABLED = true;
        #endif

        struct QueueIndicesInfo {
            std::optional<uint32_t> graphics_family;
            std::optional<uint32_t> presentation_family;
            std::optional<uint32_t> compute_family;
            std::optional<uint32_t> compute_queue_index;

            bool is_complete() const {
                return graphics_family.has_value() && presentation_family.has_value() && compute_family.has_value() && compute_queue_index.has_value();
            }

            bool supports_async_compute() const {
                return compute_family.has_value() && compute_queue_index.has_value() && (compute_family.value() != 0 || compute_queue_index.value() != 0);
            }
        };

        struct VmaAllocatorDeleter {
            void operator()(VmaAllocator *allocator) {
                if(allocator != nullptr) {
                    vmaDestroyAllocator(*allocator);
                }
            }
        };

        typedef std::unique_ptr<VmaAllocator, VmaAllocatorDeleter> VmaAllocatorRaii;

        Window(bool prefer_integrated_gpu, bool prefer_sync_compute, const std::unordered_set<std::string> &desired_vulkan_extensions);

        vk::Format find_supported_image_format(const std::vector<vk::Format> &formats, vk::ImageTiling tiling,
                                               vk::FormatFeatureFlags features);

        vk::raii::Device create_logical_device();

        [[nodiscard]] vk::PhysicalDeviceProperties get_selected_physical_device_properties() const {
            return m_physical_device.getProperties();
        }

        VmaAllocatorRaii create_allocator(vk::raii::Device &logical_device);
        vk::raii::SwapchainKHR create_swap_chain(vk::raii::Device &logical_device, vk::SwapchainKHR old_swapchain = VK_NULL_HANDLE);

        [[nodiscard]] std::unordered_set<std::string> get_loaded_device_extensions() const {
            return m_loaded_device_extensions;
        }

        [[nodiscard]] vk::Extent2D get_last_created_swap_chain_extent() const {
            return m_swap_chain_extent;
        }

        [[nodiscard]] vk::Format get_last_created_swap_chain_image_format() const {
            return m_swap_chain_image_format;
        }

        [[nodiscard]] QueueIndicesInfo get_queue_indices_info() const {
            return m_queue_indices_info;
        }

        [[nodiscard]] bool should_frame_buffer_be_resized() {
            bool return_value = m_resize_frame_buffer;

            m_resize_frame_buffer = false;

            return return_value;
        }

        [[nodiscard]] bool should_close_window() {
            glfwPollEvents();

            return glfwWindowShouldClose(m_glfw_window.get());
        }

        void wait_until_un_minimized() {
            int height = 0;
            int width = 0;

            glfwGetFramebufferSize(m_glfw_window.get(), &width, &height);

            // wait until window is un-minimized
            while(width == 0 || height == 0) {
                glfwGetFramebufferSize(m_glfw_window.get(), &width, &height);
                glfwWaitEvents();
            }
        }

        [[nodiscard]] vk::raii::Instance &get_instance() {
            return m_vulkan_instance;
        }

        [[nodiscard]] vk::raii::PhysicalDevice &get_physical_device() {
            return m_physical_device;
        }

        bool is_capturing_mouse() const {
            return m_capture_mouse;
        }

        std::optional<GLFWcursorposfun> get_mouse_capture_callback_function() const {
            return m_capture_mouse_callback_function;
        }

        bool is_key_pressed(int key) const {
            return glfwGetKey(m_glfw_window.get(), key) == GLFW_PRESS;
        }

        bool is_key_pressed_with_delay(int key, long delay_milis) {
            std::chrono::time_point current_time_point = std::chrono::system_clock::now();
            long current_time = std::chrono::duration_cast<std::chrono::milliseconds>(
                    current_time_point.time_since_epoch()).count();

            if(is_key_pressed(key)) {
                if(m_last_time_pressed.contains(key) && current_time - m_last_time_pressed[key] < delay_milis) {
                    //std::cout << "key pressed but less millis\n";
                    return false;
                } else {
                    m_last_time_pressed[key] = current_time;

                    std::cout << "key pressed success\n";

                    return true;
                }
            } else {
                return false;
            }
        }

        void set_capturing_mouse(const bool capture_mouse, std::optional<GLFWcursorposfun> callback_function) {
            m_capture_mouse = capture_mouse;

            if(m_capture_mouse) {
                assert(callback_function.has_value());

                glfwSetInputMode(m_glfw_window.get(), GLFW_CURSOR, GLFW_CURSOR_DISABLED);
                glfwSetCursorPosCallback(m_glfw_window.get(), *callback_function);

                m_capture_mouse_callback_function = callback_function;
            } else {
                glfwSetInputMode(m_glfw_window.get(), GLFW_CURSOR, GLFW_CURSOR_NORMAL);
                glfwSetCursorPosCallback(m_glfw_window.get(), nullptr);
                m_capture_mouse_callback_function = {};
            }
        }

        void update_imgui_input();
    private:
        static PFN_vkVoidFunction vulkan_function_loader(const char* function_name, void* user_data);

        static VKAPI_ATTR VkBool32 VKAPI_CALL debug_callback(VkDebugUtilsMessageSeverityFlagBitsEXT message_severity,
                                                             VkDebugUtilsMessageTypeFlagsEXT message_type,
                                                             const VkDebugUtilsMessengerCallbackDataEXT *callback_data,
                                                             void *user_data);

        static constexpr VkDebugUtilsMessengerCreateInfoEXT DEBUG_CALLBACK_CREATION_INFO = {
                .sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT,

                .messageSeverity = VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT |
                                   VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT |
                                   VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT,
                .messageType = VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT |
                               VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT |
                               VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT,

                .pfnUserCallback = debug_callback,
                .pUserData = nullptr
        };

        static constexpr float COMPUTE_QUEUE_PRIORITY = 0.9f;
        static constexpr float GRAPHICS_QUEUE_PRIORITY = 1.0f;

        static const std::array<std::string, 2> DEVICE_EXTENSIONS;
        static const std::array<std::string, 1> VALIDATION_LAYERS;

        struct GLFWwindow_deleter {
            void operator()(GLFWwindow *window) {
                glfwTerminate();
            }
        };

        struct SwapChainSupportDetails {
            vk::SurfaceCapabilitiesKHR surface_capabilities;
            std::vector<vk::SurfaceFormatKHR> surface_formats;
            std::vector<vk::PresentModeKHR> presentation_modes;
        };

        [[nodiscard]] static std::vector<const char *> get_required_extensions() {
            std::vector<const char *> return_value;

            uint32_t glfw_extension_count = 0;
            const char **glfw_extensions;

            glfw_extensions = glfwGetRequiredInstanceExtensions(&glfw_extension_count);

            for(size_t i = 0; glfw_extension_count > i; ++i) {
                return_value.push_back(glfw_extensions[i]);
            }

            if(VALIDATION_LAYERS_ENABLED) {
                return_value.push_back(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);
            }

            return return_value;
        }

        static void frame_buffer_resize_callback(GLFWwindow *glfw_window, int width, int height) {
            auto window = reinterpret_cast<Window*>(glfwGetWindowUserPointer(glfw_window));

            if (static_cast<uint32_t>(width) != window->m_swap_chain_extent.width ||
                static_cast<uint32_t>(height) != window->m_swap_chain_extent.height) {
                window->m_resize_frame_buffer = true;
            }
        }

        bool check_validation_layer_support();

        int check_device_extension_support(const vk::raii::PhysicalDevice &physical_device, std::unordered_set<std::string> desired_extensions);
        QueueIndicesInfo find_queue_indices(const vk::raii::PhysicalDevice &physical_device, bool prefer_sync_compute);

        SwapChainSupportDetails get_swap_chain_support(const vk::raii::PhysicalDevice &physical_device);

        vk::Extent2D choose_swap_extent(const vk::SurfaceCapabilitiesKHR &capabilities);

        vk::PresentModeKHR choose_swap_presentation_mode(
                const std::vector<vk::PresentModeKHR> &available_presentation_modes) {
            // prefer triple-buffering as it will avoid tearing better (though we may want to V-SYNC if on mobile devices)

            for(const vk::PresentModeKHR &available_presentation_mode : available_presentation_modes) {
                if(available_presentation_mode == vk::PresentModeKHR::eMailbox) {
                    return available_presentation_mode;
                }
            }

            // guaranteed to be supported
            return vk::PresentModeKHR::eFifo;
        }

        vk::SurfaceFormatKHR choose_swap_surface_format(const std::vector<vk::SurfaceFormatKHR> &available_formats) {
            for(const vk::SurfaceFormatKHR &available_format : available_formats) {
                if(available_format.format == vk::Format::eB8G8R8A8Srgb &&
                   available_format.colorSpace == vk::ColorSpaceKHR::eSrgbNonlinear) {
                    return available_format;
                }
            }

            // usually good enough to pick the first color profile if sRGB not found
            // (according to vulkan-tutorial.com)
            return available_formats[0];
        }

        VmaVulkanFunctions get_vma_vulkan_functions();

        vk::raii::Instance create_vulkan_instance();
        std::unique_ptr<vk::raii::DebugUtilsMessengerEXT> create_debug_messenger();
        vk::raii::SurfaceKHR create_window_surface();
        vk::raii::PhysicalDevice select_physical_device(bool prefer_integrated_gpu, bool prefer_sync_compute, 
                                                        const std::unordered_set<std::string> &desired_vulkan_extensions);

        int physical_device_suitability_score(const vk::raii::PhysicalDevice &physical_device,
                                              bool prefer_integrated_gpu, bool prefer_sync_compute, 
                                              const std::unordered_set<std::string> &desired_vulkan_extensions);

        bool m_capture_mouse = false;
        std::optional<GLFWcursorposfun> m_capture_mouse_callback_function;

        vk::raii::Context m_raii_context;
        std::unique_ptr<GLFWwindow, GLFWwindow_deleter> m_glfw_window;
        vk::DynamicLoader m_dynamic_loader;

        bool m_prefer_sync_compute;

        vk::raii::Instance m_vulkan_instance;

        std::unique_ptr<vk::raii::DebugUtilsMessengerEXT> m_debug_messenger;

        vk::Extent2D m_swap_chain_extent;
        vk::Format m_swap_chain_image_format;

        vk::raii::SurfaceKHR m_window_surface;
        VkSurfaceKHR m_window_surface_ptr;

        std::unordered_set<std::string> m_loaded_device_extensions;
        vk::raii::PhysicalDevice m_physical_device;

        // for some reason we need to recreate the swap chain after initialization otherwise the depth texture will be binary???
        bool m_resize_frame_buffer = true;

        std::unordered_map<int, long> m_last_time_pressed;
        QueueIndicesInfo m_queue_indices_info;
    };
}

#endif //ARISTAEUS_WINDOW_HPP
