//
// Created by bobby on 16/04/23.
//

#ifndef ARISTAEUS_CHUNK_PROP_LOADER_HPP
#define ARISTAEUS_CHUNK_PROP_LOADER_HPP

#include <gfx/vulkan/assimp_model.hpp>
#include <gfx/vulkan/core_renderer.hpp>
#include <gfx/vulkan/graphics_pipeline.hpp>
#include <gfx/vulkan/perlin_noise.hpp>
#include <gfx/vulkan/image_buffer_raii.hpp>

#include <core/lru_cache.hpp>

namespace aristaeus::gfx::vulkan {
    class ChunkedTerrain;

    class ChunkPropLoader {
    public:
        struct PropGroup {
            std::optional<glm::vec2> scale_range;

            std::vector<glm::vec2> perlin_ranges;
            std::vector<glm::vec2> slope_ranges;
            std::vector<glm::vec2> height_ranges;

            bool self_group_override;
        };

        struct Prop {
            float weight;
            AssimpModel prop_model;
            std::optional<PropGroup> self_prop_group;
            std::optional<std::string> prop_group;
        };

        struct PropData {
            std::vector<Prop> props;
            std::unordered_map<std::string, PropGroup> prop_groups;
        };

        ChunkPropLoader(GraphicsPipeline &instanced_mesh_pipeline,
                        std::shared_ptr<PropData> &prop_datas, size_t amount_of_props,
                        const glm::ivec2 &height_map_resolution,
                        ChunkedTerrain &terrain, CoreRenderer &core_renderer,
                        const std::shared_ptr<std::mt19937> &random_num_generator,
                        const glm::vec2 &current_chunk_position);

        void draw(const vk::CommandBuffer &command_buffer,
                  const glm::mat4 &view_matrix, bool light_emitter,
                  vk::raii::PipelineLayout &pipeline_layout, const glm::mat4 &light_space_matrix);
    private:
        struct AssimpModelHash {
            size_t operator()(AssimpModel *model) const noexcept {
                if(model == nullptr) {
                    return 0;
                } else {
                    std::hash<std::string> hasher;

                    return hasher(model->get_model_path());
                }
            }

            bool operator()(AssimpModel *model_a, AssimpModel *model_b) const noexcept {
                if(model_a == nullptr || model_b == nullptr) {
                    return model_a == model_b;
                } else {
                    return model_a->get_model_path() == model_b->get_model_path();
                }
            }
        };

        struct PropRangeInfo {
            std::vector<std::reference_wrapper<Prop>> props;
            std::vector<glm::ivec2> grid_positions;
        };

        bool compare_ranges(float value, const std::vector<glm::vec2> &ranges);
        bool check_for_prop_group_compliance(const PropGroup &prop_group, float perlin, float height, float slope) {
            return compare_ranges(perlin, prop_group.perlin_ranges) &&
                   compare_ranges(height, prop_group.height_ranges) && compare_ranges(slope, prop_group.slope_ranges);
        }
        bool check_for_prop_compliance(const Prop &prop, float perlin, float height, float slope);

        GraphicsPipeline &m_instanced_mesh_pipeline;
        std::shared_ptr<PropData> m_prop_data;
        std::unordered_map<AssimpModel*, size_t, AssimpModelHash> m_prop_and_instance_numbers;
        std::shared_ptr<std::mt19937> m_random_num_generator;
        PerlinNoise m_prop_noise_generator;
        std::vector<std::vector<float>> m_prop_noise_map;
        std::unordered_map<PropGroup*, PropRangeInfo> m_prop_ranges{};
        std::unordered_map<Prop*, PropRangeInfo> m_self_prop_ranges{};

        glm::vec2 m_current_chunk_position;

        BufferRaii m_prop_staging_buffer;
        BufferRaii m_prop_buffer;
    };
}

#endif //ARISTAEUS_CHUNK_PROP_LOADER_HPP
