#include "brdf_convolution.hpp"

#include <gfx/vulkan/core_renderer.hpp>


aristaeus::gfx::vulkan::compute::BRDFConvolution::BRDFConvolution(const glm::ivec2 &image_size, int32_t sample_count,
                                                                  const std::optional<std::string> &cache_path) :
    SingleImageComputeTask(0, 0, 0, ComputePipelineInfo{ get_compute_shader_info(), std::string("compute_brdf_convolution"),
                                                         {vk::PushConstantRange{vk::ShaderStageFlagBits::eCompute, 0,
                                                         sizeof(ConvolutionPushConstants)}}, true, false },
                           "compute_brdf_convolution", image_size, {}, vk::Format::eR16G16Sfloat), // TODO(Bobby): actually parse in cache_path after
    // TODO(Bobby): caching single images are supported
    m_push_constants({ .image_size = image_size, .sample_count = sample_count }) {
}


void aristaeus::gfx::vulkan::compute::BRDFConvolution::update_descriptor_set() {
    CoreRenderer &core_renderer = utils::get_singleton_safe<CoreRenderer>();

    vk::DescriptorImageInfo storage_image_info = m_output_texture.get_descriptor_image_info(core_renderer);

    storage_image_info.imageLayout = vk::ImageLayout::eGeneral;

    vk::WriteDescriptorSet storage_image_set_write{ *m_descriptor_sets[0], 0, 0, vk::DescriptorType::eStorageImage, storage_image_info };

    core_renderer.get_logical_device_ref().updateDescriptorSets({ storage_image_set_write }, {});
}

void aristaeus::gfx::vulkan::compute::BRDFConvolution::dispatch(vk::CommandBuffer &command_buffer) {
    //tf::Taskflow new_taskflow;

    if (m_push_constants.image_size.x % WORK_GROUP_X != 0 || m_push_constants.image_size.y % WORK_GROUP_Y != 0) {
        throw std::runtime_error("convolution res must be a multiple of (" + std::to_string(WORK_GROUP_X) + "," + std::to_string(WORK_GROUP_Y) + ")");
    }

    m_output_texture.transition_pre_compute(command_buffer);

    command_buffer.pushConstants(*m_compute_pipeline.get_pipeline_layout(), vk::ShaderStageFlagBits::eCompute, 0,
                                 sizeof(ConvolutionPushConstants), static_cast<void*>(&m_push_constants));
    update_descriptor_set();

    m_compute_pipeline.bind_to_command_buffer(command_buffer);
    command_buffer.bindDescriptorSets(vk::PipelineBindPoint::eCompute, *m_compute_pipeline.get_pipeline_layout(), 0,
                                      *m_descriptor_sets[0], {});

    command_buffer.dispatch(m_push_constants.image_size.x / WORK_GROUP_X, m_push_constants.image_size.x / WORK_GROUP_Y, 1);

    if (!m_cache_path.has_value()) {
        m_output_texture.transition_post_compute(command_buffer);
    }
}