//
// Created by bobby on 6/8/22.
//

#include "core_renderer.hpp"
#include "perspective_camera_environment.hpp"
#include "lighting_environment.hpp"

aristaeus::gfx::vulkan::PerspectiveCameraEnvironment
    *aristaeus::gfx::vulkan::PerspectiveCameraEnvironment::current_camera = nullptr;

vk::ShaderStageFlags aristaeus::gfx::vulkan::PerspectiveCameraEnvironment::get_shader_usage() {
    vk::ShaderStageFlags shader_usage_flags = vk::ShaderStageFlagBits::eVertex | vk::ShaderStageFlagBits::eFragment |
                                              vk::ShaderStageFlagBits::eTessellationEvaluation |
                                              vk::ShaderStageFlagBits::eTessellationControl;

    #ifndef NDEBUG
    shader_usage_flags |= vk::ShaderStageFlagBits::eGeometry;
    #endif

    return shader_usage_flags;
}

aristaeus::gfx::vulkan::PerspectiveCameraEnvironment::PerspectiveCameraEnvironment(CoreRenderer &core_renderer,
                                                                                   float fov, float aspect_ratio,
                                                                                   float z_near, float z_far,
                                                                                   float mouse_sensitivity,
                                                                                   bool enable_up_down_movement,
                                                                                   const DirectionalLightShadowsInfo &shadows_info,
                                                                                   float camera_pitch_degrees,
                                                                                   float camera_yaw_degrees) :
        m_matrix_uniform_buffer(core_renderer, get_shader_usage(),
                                sizeof(MatrixUbo)),
        m_shadow_mapping_matrix_uniform_buffer(core_renderer, get_shader_usage(), sizeof(MatrixUbo)),
        m_mouse_sensitivity(mouse_sensitivity), m_up_down_movement(enable_up_down_movement),
        m_camera_front_target(0.068475f, 0.190809f, -0.979236f), 
        m_camera_position(-1.952644f, -1.789690f, 7.16568f),
        m_camera_up(0.0f, -1.0f, 0.0f),
        m_directional_light_shadows_info(shadows_info),
        m_core_renderer(core_renderer) {
    m_matrix_ubo_content.projection_matrix =
            reset_projection_matrix(fov, aspect_ratio, z_near, z_far, camera_pitch_degrees, camera_yaw_degrees);

    if (shadows_info.enabled) {
        m_shadow_mapping_matrix_ubo_content.projection_matrix = glm::ortho(shadows_info.orthographic_matrix[0], shadows_info.orthographic_matrix[1], shadows_info.orthographic_matrix[2],
            shadows_info.orthographic_matrix[3], shadows_info.orthographic_matrix[4], shadows_info.orthographic_matrix[5]);
        
        m_shadow_mapping_matrix_ubo_content.projection_matrix[1][1] *= -1;

        m_shadow_mapping_matrix_ubo_content.view_matrix = glm::lookAt(shadows_info.eye_position, shadows_info.target_position, glm::vec3(0, 1, 0));

        m_matrix_ubo_content.light_space_matrix = m_shadow_mapping_matrix_ubo_content.projection_matrix * m_shadow_mapping_matrix_ubo_content.view_matrix;
    }

    m_shadow_mapping_matrix_ubo_content.linear_distance_scaling = 0;
}

aristaeus::gfx::vulkan::PerspectiveCameraEnvironment::PerspectiveCameraEnvironment(PerspectiveCameraEnvironment &&other) :
    m_matrix_uniform_buffer(std::move(other.m_matrix_uniform_buffer)),
    m_shadow_mapping_matrix_uniform_buffer(std::move(other.m_shadow_mapping_matrix_uniform_buffer)),
    m_matrix_ubo_content(std::move(other.m_matrix_ubo_content)),
    m_shadow_mapping_matrix_ubo_content(std::move(other.m_shadow_mapping_matrix_ubo_content)),
    m_current_captures(std::move(other.m_current_captures)),
    m_aspect_ratio(std::move(other.m_aspect_ratio)),
    m_frame_delta(std::move(other.m_frame_delta)),
    m_fov(std::move(other.m_fov)),
    m_last_frame_time(std::move(other.m_last_frame_time)),
    m_mouse_sensitivity(std::move(other.m_mouse_sensitivity)),
    m_mouse_pitch(std::move(other.m_mouse_pitch)),
    m_mouse_yaw(std::move(other.m_mouse_yaw)),
    m_up_down_movement(std::move(other.m_up_down_movement)),
    m_z_far(std::move(other.m_z_far)),
    m_z_near(std::move(other.m_z_near)),
    m_camera_front_target(std::move(other.m_camera_front_target)),
    m_camera_position(std::move(other.m_camera_position)),
    m_camera_up(std::move(other.m_camera_up)),
    m_last_mouse_position(std::move(other.m_last_mouse_position)),
    m_lighting_environments(std::move(other.m_lighting_environments)),
    m_default_lighting_environment_tag(std::move(other.m_default_lighting_environment_tag)),
    m_lighting_environment_tags(std::move(other.m_lighting_environment_tags)),
    m_scene_graph_node_root(std::move(other.m_scene_graph_node_root)),
    m_directional_light_shadows_info(std::move(other.m_directional_light_shadows_info)),
    m_core_renderer(other.m_core_renderer),
    m_capturing_mouse(std::move(other.m_capturing_mouse)),
    m_use_orthographic(std::move(other.m_use_orthographic)),
    m_use_point_light_perspective(std::move(other.m_use_point_light_perspective))
{

    if (m_scene_graph_node_root != nullptr) {
        m_scene_graph_node_root->remove_callback_for_event<std::reference_wrapper<gfx::SceneGraphNode>>(std::string{ gfx::SceneGraphNode::DESCENDENT_NODE_ADDED_EVENT }, "perspective_camera_env_descendent_node_added_event");

        m_scene_graph_node_root->add_callback_for_event<std::reference_wrapper<gfx::SceneGraphNode>, gfx::SceneGraphNode>(std::string{ gfx::SceneGraphNode::DESCENDENT_NODE_ADDED_EVENT }, "perspective_camera_env_descendent_node_added_event",
            [this](gfx::SceneGraphNode &, std::reference_wrapper<gfx::SceneGraphNode> node_added) {
                assign_default_lighting_environment_to_node(node_added.get());
            }, *m_scene_graph_node_root);
    }
}

glm::vec3 aristaeus::gfx::vulkan::PerspectiveCameraEnvironment::Capture::get_view_direction() const {
    glm::vec3 direction;

    if (view_direction.has_value()) {
        direction = *view_direction;
    }
    else {
        direction.x = cosf(glm::radians(yaw)) * cosf(glm::radians(pitch));
        direction.y = sinf(glm::radians(pitch));
        direction.z = sinf(glm::radians(yaw)) * cosf(glm::radians(pitch));
    }

    return direction;
}

glm::mat4 aristaeus::gfx::vulkan::PerspectiveCameraEnvironment::Capture::get_view_matrix() const {
    return glm::lookAt(position, position + get_view_direction(), up);
}

glm::mat4 aristaeus::gfx::vulkan::PerspectiveCameraEnvironment::Capture::get_projection_matrix() const {
    glm::mat4 return_value;

    if (orthographic_capture.has_value()) {
        const OrthographicCapture &ortho_capture = *orthographic_capture;

        return_value = glm::ortho(ortho_capture.left, ortho_capture.right, ortho_capture.bottom, ortho_capture.top, z_near, z_far);
    }
    else {
        return_value = glm::perspective(glm::radians(fov), aspect_ratio, z_near, z_far);
    }
    return_value[1][1] *= -1;

    return return_value;
}

void aristaeus::gfx::vulkan::PerspectiveCameraEnvironment::update_matrix_ubo(CoreRenderer &core_renderer,
                                                                             RenderContextID render_context_id) {
    RenderPhase *current_render_phase = core_renderer.get_current_render_phase();
    assert(current_render_phase != nullptr);

    glm::uvec2 screen_resolution{ current_render_phase->get_current_viewport().width, current_render_phase->get_current_viewport().height };
    float aspect_ratio = static_cast<float>(screen_resolution.x) / screen_resolution.y;

    if (m_aspect_ratio != aspect_ratio) {
        m_aspect_ratio = aspect_ratio;

        m_matrix_ubo_content.projection_matrix = reset_projection_matrix(m_fov, m_aspect_ratio, m_z_near, m_z_far, m_mouse_pitch, m_mouse_yaw);
    }

    if(current_render_phase->get_render_phase_name() == CoreRenderer::SHADOW_MAPPING_RENDER_PHASE &&
       current_render_phase->get_current_render_pass() == CoreRenderer::SHADOW_MAPPING_RENDER_PHASE_DIRECTIONAL_RENDER_PASS) {
        m_shadow_mapping_matrix_uniform_buffer.write_to_descriptor(&m_shadow_mapping_matrix_ubo_content,
                                                                   sizeof(m_shadow_mapping_matrix_ubo_content),
                                                                   core_renderer.get_current_frame(),
                                                                   render_context_id,
                                                                   core_renderer);
    } else {
        glm::vec3 camera_up = m_camera_up;

        if(m_camera_front_target.y == -camera_up.y) {
            camera_up = {1.0f, 0.0f, 0.0f};
            m_matrix_ubo_content.flip_projection_matrix = 0;
        } else if(m_camera_front_target.y == camera_up.y) {
            camera_up = {-1.0f, 0.0f, 0.0f};
            m_matrix_ubo_content.flip_projection_matrix = 0;
        } else {
            m_matrix_ubo_content.flip_projection_matrix = 1;
        }

        bool used_capture = m_current_captures.contains(render_context_id);

        if (used_capture) {
            const Capture &capture = m_current_captures.at(render_context_id);
            glm::vec3 direction = capture.get_view_direction();

            m_matrix_ubo_content.camera_far_plane = capture.z_far;
            m_matrix_ubo_content.linear_distance_scaling = 1;
            m_matrix_ubo_content.camera_position = capture.position;
            m_matrix_ubo_content.camera_front = direction;
            m_matrix_ubo_content.camera_up = capture.up;
            m_matrix_ubo_content.camera_right = glm::cross(direction, capture.up);
            m_matrix_ubo_content.projection_matrix = capture.get_projection_matrix();
            m_matrix_ubo_content.view_matrix = capture.get_view_matrix();

            m_current_captures.erase(render_context_id);
        }
        else {
            m_matrix_ubo_content.view_matrix = glm::lookAt(m_camera_position,
                m_camera_position + m_camera_front_target,
                camera_up);
            m_matrix_ubo_content.camera_front = m_camera_front_target;
            m_matrix_ubo_content.camera_up = m_camera_up;
            m_matrix_ubo_content.camera_right = glm::cross(m_camera_front_target, m_camera_up);
            m_matrix_ubo_content.camera_position = m_camera_position;

            if (m_directional_light_shadows_info.follow_player_camera) {
                m_shadow_mapping_matrix_ubo_content.view_matrix = glm::lookAt(m_directional_light_shadows_info.eye_position + glm::vec3(m_camera_position.x, 0, m_camera_position.z),
                    m_directional_light_shadows_info.target_position + glm::vec3(m_camera_position.x, 0, m_camera_position.z), m_camera_up);

                m_matrix_ubo_content.light_space_matrix = m_shadow_mapping_matrix_ubo_content.projection_matrix * m_shadow_mapping_matrix_ubo_content.view_matrix;
            }
        }

        #ifndef NDEBUG
        //std::cout << "Current camera front: " << glm::to_string(m_matrix_ubo_content.camera_front) << "\n";
        //std::cout << "Current camera position: " << glm::to_string(m_camera_position) << ":" << m_mouse_pitch << "," << std::fmod(m_mouse_yaw, 360) << "\n";
        #endif

        if(m_use_orthographic || m_use_point_light_perspective >= 0) {
            MatrixUbo ubo;
            if(m_use_orthographic) {
                ubo.projection_matrix = m_shadow_mapping_matrix_ubo_content.projection_matrix;
                ubo.view_matrix = m_shadow_mapping_matrix_ubo_content.view_matrix;
                ubo.camera_position = m_camera_position;
            }

            m_matrix_uniform_buffer.write_to_descriptor(&ubo, sizeof(ubo), core_renderer.get_current_frame(),
                                                        render_context_id, core_renderer);
        } else {
            m_matrix_uniform_buffer.write_to_descriptor(&m_matrix_ubo_content, sizeof(m_matrix_ubo_content),
                                                        core_renderer.get_current_frame(),
                                                        render_context_id, core_renderer);
        }

        if (used_capture) {
            m_matrix_ubo_content.projection_matrix = reset_projection_matrix(m_fov, m_aspect_ratio, m_z_near, m_z_far, m_mouse_pitch, m_mouse_yaw);
        }
    }
}

vk::raii::DescriptorSet &aristaeus::gfx::vulkan::PerspectiveCameraEnvironment::get_camera_descriptor_set_to_bind() {
    auto &core_renderer = utils::get_singleton_safe<CoreRenderer>();

    std::reference_wrapper<vk::raii::DescriptorSet> descriptor_set =
        m_matrix_uniform_buffer.get_descriptor_to_bind(core_renderer, core_renderer.get_current_frame(), core_renderer.get_current_render_context_id());

    bool is_shadow_mapping = core_renderer.get_current_render_phase()->get_render_phase_name() == CoreRenderer::SHADOW_MAPPING_RENDER_PHASE &&
        core_renderer.get_current_render_phase()->get_current_render_pass() == CoreRenderer::SHADOW_MAPPING_RENDER_PHASE_DIRECTIONAL_RENDER_PASS;

    if (is_shadow_mapping) {
        descriptor_set =
            m_shadow_mapping_matrix_uniform_buffer.get_descriptor_to_bind(core_renderer,
                core_renderer.get_current_frame(),
                core_renderer.get_current_render_context_id());
    }

    return descriptor_set.get();
}

void aristaeus::gfx::vulkan::PerspectiveCameraEnvironment::predraw() {
    for (std::unique_ptr<LightingEnvironment> &lighting_environment : m_lighting_environments) {
        lighting_environment->activate_environment(true);
    }
}

void aristaeus::gfx::vulkan::PerspectiveCameraEnvironment::draw(vk::CommandBuffer primary_command_buffer,
                                                                CoreRenderer &core_renderer,
                                                                vk::raii::Device &logical_device) {
    float current_frame = glfwGetTime();
    RenderContextID render_context_id = core_renderer.get_current_render_context_id();

    m_frame_delta = current_frame - m_last_frame_time;
    m_last_frame_time = current_frame;

    update_matrix_ubo(core_renderer, render_context_id);

    bool is_shadow_mapping = core_renderer.get_current_render_phase()->get_render_phase_name() == CoreRenderer::SHADOW_MAPPING_RENDER_PHASE &&
        core_renderer.get_current_render_phase()->get_current_render_pass() == CoreRenderer::SHADOW_MAPPING_RENDER_PHASE_DIRECTIONAL_RENDER_PASS;

    glm::mat4 projection_matrix;
    glm::mat4 view_matrix;

    if (is_shadow_mapping) {
        projection_matrix = m_shadow_mapping_matrix_ubo_content.projection_matrix;
        view_matrix = m_shadow_mapping_matrix_ubo_content.view_matrix;
    }
    else {
        projection_matrix = m_matrix_ubo_content.projection_matrix;
        view_matrix = m_matrix_ubo_content.view_matrix;
    }

    glm::mat4 light_space_matrix = m_shadow_mapping_matrix_ubo_content.projection_matrix *
        m_shadow_mapping_matrix_ubo_content.view_matrix;

    for(std::unique_ptr<LightingEnvironment> &lighting_environment : m_lighting_environments) {
        lighting_environment->activate_environment(false);
    }
}

void aristaeus::gfx::vulkan::PerspectiveCameraEnvironment::mouse_callback(GLFWwindow *window, double x, double y) {
    glm::vec2 current_mouse_position = glm::vec2(x, y);
    glm::vec2 mouse_position_delta = current_mouse_position - current_camera->m_last_mouse_position;

    current_camera->m_last_mouse_position = current_mouse_position;

    current_camera->m_last_mouse_position *= glm::vec2(current_camera->m_mouse_sensitivity);

    current_camera->m_mouse_pitch -= mouse_position_delta.y;
    current_camera->m_mouse_yaw += mouse_position_delta.x;

    //std::cout << "yaw: " << current_camera->m_mouse_yaw << "\n";

    if(current_camera->m_mouse_pitch > 89.0f) {
        current_camera->m_mouse_pitch = 89.0f;
    } else if(current_camera->m_mouse_pitch < -89.0f) {
        current_camera->m_mouse_pitch = -89.0f;
    }

    glm::vec3 direction;

    // yaw is on xz axis, and pitch is on xy axis
    direction.x = cosf(glm::radians(current_camera->m_mouse_yaw)) * cosf(glm::radians(current_camera->m_mouse_pitch));
    direction.y = sinf(glm::radians(current_camera->m_mouse_pitch));
    direction.z = sinf(glm::radians(current_camera->m_mouse_yaw)) * cosf(glm::radians(current_camera->m_mouse_pitch));

    current_camera->m_camera_front_target = direction;

    //std::cout << "set to " << glm::to_string(direction) << ": " << current_camera->m_mouse_pitch << "," << fmod(current_camera->m_mouse_yaw, 360) << "\n";
}

void aristaeus::gfx::vulkan::PerspectiveCameraEnvironment::input(aristaeus::gfx::vulkan::Window &window) {
    float adjusted_frame_delta = m_frame_delta;
    glm::vec3 camera_position = m_camera_position;
    glm::vec3 camera_front_target = m_camera_front_target;

    if(window.is_key_pressed(GLFW_KEY_LEFT_SHIFT)) {
        adjusted_frame_delta *= 4;
    }

    if(window.is_key_pressed(GLFW_KEY_TAB)) {
        // get to directional light perspective
        camera_position = glm::vec3(1.21f, -10.72f, 4.13f);
        camera_front_target = glm::vec3(-1.21f, 10.72f, -4.13f);
    } else if(window.is_key_pressed(GLFW_KEY_R)) {
        camera_position = INITIAL_POSITION;
        camera_front_target = INITIAL_FRONT_TARGET;
    } else {
        if(!m_up_down_movement) {
            camera_front_target.y = 0;
        }

        if(window.is_key_pressed(GLFW_KEY_W)) {
            camera_position += 5.0f * camera_front_target * adjusted_frame_delta;
        } else if(window.is_key_pressed(GLFW_KEY_S)) {
            camera_position -= 5.0f * camera_front_target * adjusted_frame_delta;
        }

        if(window.is_key_pressed(GLFW_KEY_D)) {
            camera_position -= 5.0f * glm::normalize(glm::cross(camera_front_target, m_camera_up)) * adjusted_frame_delta;
        } else if(window.is_key_pressed(GLFW_KEY_A)) {
            camera_position += 5.0f * glm::normalize(glm::cross(camera_front_target, m_camera_up)) * adjusted_frame_delta;
        }

        if(window.is_key_pressed_with_delay(GLFW_KEY_PERIOD, 500)) {
            // toggle orthographic projection
            m_use_orthographic = !m_use_orthographic;
        }

        if(window.is_key_pressed_with_delay(GLFW_KEY_ESCAPE, 500)) {
            m_capturing_mouse = !m_capturing_mouse;

            current_camera = this;

            window.set_capturing_mouse(m_capturing_mouse, mouse_callback);
        }
    }

    m_camera_position = camera_position;
    m_camera_front_target = camera_front_target;

    for(std::unique_ptr<LightingEnvironment> &material_environment : m_lighting_environments) {
        material_environment->input(window);
    }
}

void aristaeus::gfx::vulkan::PerspectiveCameraEnvironment::recreate_graphics_pipeline(
        CoreRenderer &core_renderer, const vk::Viewport &viewport) {
    vk::Rect2D viewport_scissor {{0, 0}, {static_cast<uint32_t>(viewport.width),
                                          static_cast<uint32_t>(viewport.height)}};

    for(std::unique_ptr<LightingEnvironment> &material_environment : m_lighting_environments) {
        material_environment->recreate_graphics_pipelines(viewport, viewport_scissor);
    }
}

glm::mat4 aristaeus::gfx::vulkan::PerspectiveCameraEnvironment::reset_projection_matrix(float fov, float aspect_ratio,
                                                                                        float z_near, float z_far,
                                                                                        float camera_pitch_degrees,
                                                                                        float camera_yaw_degrees) {
    glm::mat4 projection_matrix = glm::perspective(fov, aspect_ratio, z_near, z_far);

    current_camera = this;

    m_aspect_ratio = aspect_ratio;
    m_mouse_pitch = camera_pitch_degrees;
    m_mouse_yaw = camera_yaw_degrees;
    m_fov = fov;
    m_z_far = z_far;
    m_z_near = z_near;

    m_matrix_ubo_content.camera_far_plane = m_z_far;

    projection_matrix[1][1] *= -1;

    glm::vec3 direction;

    // yaw is on xz axis, and pitch is on xy axis
    direction.x = cosf(glm::radians(m_mouse_yaw)) * cosf(glm::radians(m_mouse_pitch));
    direction.y = sinf(glm::radians(m_mouse_pitch));
    direction.z = sinf(glm::radians(m_mouse_yaw)) * cosf(glm::radians(m_mouse_pitch));

    m_camera_front_target = direction;

    return projection_matrix;
}

bool aristaeus::gfx::vulkan::PerspectiveCameraEnvironment::is_captured() const {
    for(const std::unique_ptr<LightingEnvironment> &material_environment : m_lighting_environments) {
        if(!material_environment->is_captured()) {
            return false;
        }
    }

    return true;
}

void aristaeus::gfx::vulkan::PerspectiveCameraEnvironment::predraw_load(vk::CommandBuffer command_buffer) {
    for(std::unique_ptr<LightingEnvironment> &material_environment : m_lighting_environments) {
        m_scene_graph_node_root->predraw_load_tree(command_buffer, m_camera_position);
    }
}

void aristaeus::gfx::vulkan::PerspectiveCameraEnvironment::capture_points(vk::CommandBuffer command_buffer, bool force_recapture) {
    for (std::unique_ptr<LightingEnvironment> &material_environment : m_lighting_environments) {
        material_environment->capture_points(force_recapture);
    }
}

void aristaeus::gfx::vulkan::PerspectiveCameraEnvironment::capture_point_light_shadows(vk::CommandBuffer command_buffer, bool force_recapture) {
    for (std::unique_ptr<LightingEnvironment> &material_environment : m_lighting_environments) {
        material_environment->capture_point_light_shadows(force_recapture);
    }
}

void aristaeus::gfx::vulkan::PerspectiveCameraEnvironment::attach_lighting_environment(
                                                           std::unique_ptr<LightingEnvironment> &lighting_environment) {
    m_lighting_environments.push_back(std::move(lighting_environment));

    std::string lighting_environmemnt_tag = m_lighting_environments.back()->get_lighting_env_tag();

    assert(!m_lighting_environment_tags.contains(lighting_environmemnt_tag));

    m_lighting_environment_tags.insert(lighting_environmemnt_tag);

    if(m_scene_graph_node_root != nullptr) {
        m_lighting_environments.back()->set_perspective_camera_and_root(*this, m_scene_graph_node_root);
    }
}

void aristaeus::gfx::vulkan::PerspectiveCameraEnvironment::set_scene_graph_root_node(const std::shared_ptr<gfx::SceneGraphNode> &loading_node_root) {
    if (m_scene_graph_node_root != loading_node_root) {
        m_scene_graph_node_root = loading_node_root;

        for (std::unique_ptr<gfx::vulkan::LightingEnvironment> &lighting_env : m_lighting_environments) {
            lighting_env->set_perspective_camera_and_root(*this, m_scene_graph_node_root);
        }

        // TODO(Bobby): make this work after std::move
        m_scene_graph_node_root->add_callback_for_event<std::reference_wrapper<gfx::SceneGraphNode>, gfx::SceneGraphNode>(std::string{gfx::SceneGraphNode::DESCENDENT_NODE_ADDED_EVENT}, "perspective_camera_env_descendent_node_added_event",
                                                                                                                          [this](gfx::SceneGraphNode&, std::reference_wrapper<gfx::SceneGraphNode> node_added) {
            assign_default_lighting_environment_to_node(node_added.get());
        }, *m_scene_graph_node_root);
    }
}

void aristaeus::gfx::vulkan::PerspectiveCameraEnvironment::setup_capture(const Capture &capture) {
    auto &core_renderer = utils::get_singleton_safe<CoreRenderer>();
    RenderContextID render_context_id = core_renderer.get_current_render_context_id();

    //assert(!m_current_captures.contains(render_context_id));

    m_current_captures.insert({render_context_id, capture});
}

void aristaeus::gfx::vulkan::PerspectiveCameraEnvironment::reapply_graphics_pipeline(
                                                const vk::CommandBuffer &command_buffer, int material_env_index) {
    m_lighting_environments[material_env_index]->reapply_graphics_pipeline(command_buffer);
}

void aristaeus::gfx::vulkan::PerspectiveCameraEnvironment::assign_default_lighting_environment_to_node(gfx::SceneGraphNode &scene_graph_node) {
    bool found_lighting_env = false;

    for (const std::string &node_group : scene_graph_node.get_node_groups()) {
        if (m_lighting_environment_tags.contains(node_group)) {
            found_lighting_env = true;
            break;
        }
    }

    if (!found_lighting_env) {
        scene_graph_node.add_node_group(m_default_lighting_environment_tag);
    }
}

void aristaeus::gfx::vulkan::PerspectiveCameraEnvironment::assign_default_lighting_environment_to_tree() {
    std::queue<gfx::SceneGraphNode*> tree_queue;

    tree_queue.push(m_scene_graph_node_root.get());

    while (!tree_queue.empty()) {
        gfx::SceneGraphNode *current_node = tree_queue.front();

        if (current_node != nullptr) {
            assign_default_lighting_environment_to_node(*current_node);

            for (std::shared_ptr<gfx::SceneGraphNode> &scene_graph_node : current_node->get_children()) {
                tree_queue.push(scene_graph_node.get());
            }
        }

        tree_queue.pop();
    }
}

float aristaeus::gfx::vulkan::PerspectiveCameraEnvironment::get_aspect_ratio() const {
    RenderContextID render_context_id = utils::get_singleton_safe<CoreRenderer>().get_current_render_context_id();

    return m_current_captures.contains(render_context_id) ? m_current_captures.at(render_context_id).aspect_ratio : m_aspect_ratio;
}

float aristaeus::gfx::vulkan::PerspectiveCameraEnvironment::get_pitch_degrees() const {
    RenderContextID render_context_id = utils::get_singleton_safe<CoreRenderer>().get_current_render_context_id();

    return m_current_captures.contains(render_context_id) ? m_current_captures.at(render_context_id).pitch : m_mouse_pitch;
}

float aristaeus::gfx::vulkan::PerspectiveCameraEnvironment::get_yaw_degrees() const {
    RenderContextID render_context_id = utils::get_singleton_safe<CoreRenderer>().get_current_render_context_id();

    return m_current_captures.contains(render_context_id) ? m_current_captures.at(render_context_id).yaw : m_mouse_yaw;
}

float aristaeus::gfx::vulkan::PerspectiveCameraEnvironment::get_fov_degrees() const {
    RenderContextID render_context_id = utils::get_singleton_safe<CoreRenderer>().get_current_render_context_id();

    return m_current_captures.contains(render_context_id) ? m_current_captures.at(render_context_id).fov : m_fov;
}

[[nodiscard]] float aristaeus::gfx::vulkan::PerspectiveCameraEnvironment::get_z_far() const {
    RenderContextID render_context_id = utils::get_singleton_safe<CoreRenderer>().get_current_render_context_id();

    return m_current_captures.contains(render_context_id) ? m_current_captures.at(render_context_id).z_far : m_z_far;
}

[[nodiscard]] float aristaeus::gfx::vulkan::PerspectiveCameraEnvironment::get_z_near() const {
    RenderContextID render_context_id = utils::get_singleton_safe<CoreRenderer>().get_current_render_context_id();

    return m_current_captures.contains(render_context_id) ? m_current_captures.at(render_context_id).z_near : m_z_near;
}

[[nodiscard]] glm::vec3 aristaeus::gfx::vulkan::PerspectiveCameraEnvironment::get_camera_position() const {
    RenderContextID render_context_id = utils::get_singleton_safe<CoreRenderer>().get_current_render_context_id();

    return m_current_captures.contains(render_context_id) ? m_current_captures.at(render_context_id).position : m_camera_position;
}