//
// Created by bobby on 5/23/22.
//
#include "core_renderer.hpp"

#include "assimp_mesh.hpp"
#include "perspective_camera_environment.hpp"
#include "../../core/scene_loader.hpp"

#include "material_manager.hpp"
#include "mesh_manager.hpp"

template<>
aristaeus::gfx::vulkan::CoreRenderer *aristaeus::core::Singleton<aristaeus::gfx::vulkan::CoreRenderer>::instance = nullptr;

std::random_device aristaeus::gfx::vulkan::CoreRenderer::random_device {};
std::mt19937 aristaeus::gfx::vulkan::CoreRenderer::mersenne_twister_rng {random_device()};

const vk::ImageViewCreateInfo aristaeus::gfx::vulkan::CoreRenderer::DEFAULT_IMAGE_VIEW_CREATE_INFO{ {}, {}, vk::ImageViewType::e2D,
                                                                                                    vk::Format::eR8G8B8A8Srgb,
                                                                                                    {}, {vk::ImageAspectFlagBits::eColor,
                                                                                                    0, 1, 0, 1} };

aristaeus::gfx::vulkan::CoreRenderer::CoreRenderer(
                                            aristaeus::gfx::vulkan::Window &window,
                                            const std::vector<glm::vec4> &clear_colors,
                                            vk::raii::Device &logical_device, vk::raii::SwapchainKHR &swap_chain,
                                            vk::Extent2D swap_chain_extent, vk::Format swap_chain_image_format, 
                                            EnvironmentLoadingInfo &environment_loading_info,
                                            const std::optional<std::string> &compute_info_path) :
                                            m_window(window), m_logical_device(std::move(logical_device)),
                                            m_swap_chain(std::move(swap_chain)),
                                            m_swap_chain_extent(swap_chain_extent),
                                            m_swap_chain_image_format(swap_chain_image_format),
                                            m_swap_chain_images(create_swap_chain_vma_images(m_swap_chain)),
                                            m_swap_chain_image_views(create_swap_image_views()),
                                            m_memory_manager(m_window, m_logical_device),
                                            m_depth_image_format(find_depth_image_format()),
                                            m_depth_image(create_depth_image(m_swap_chain_extent)),
                                            m_depth_image_view(create_depth_image_view(m_depth_image)),
                                            m_clear_color(clear_colors[0]),
                                            m_graphics_queue(m_logical_device.getQueue(
                                                    m_window.get_queue_indices_info().graphics_family.value(), 0)),
                                            m_presentation_queue(m_logical_device.getQueue(
                                                m_window.get_queue_indices_info().presentation_family.value(), 0)),
                                            m_graphics_command_buffer_manager(m_window.get_queue_indices_info().graphics_family.value(), false),
                                            m_frame_available_image(utils::create_semaphores(m_logical_device)),
                                            m_loading_task_semaphores(utils::create_semaphores(m_logical_device)),
                                            m_frame_finished_rendering(utils::create_semaphores(m_logical_device)),
                                            m_frame_in_flight_fences(utils::create_signaled_fences(m_logical_device)),
                                            m_imgui_descriptor_pool(create_descriptor_pool()),
                                            m_render_phases(init_render_phases()),
                                            m_perspective_camera_environment(
                                                std::move(environment_loading_info.scene_loader.init_environment_json(
                                                        environment_loading_info.aspect_ratio,
                                                        environment_loading_info.viewport,
                                                        environment_loading_info.viewport_scissor, window.get_loaded_device_extensions()))){
    m_perspective_camera_environment.set_scene_graph_root_node(environment_loading_info.scene_loader.get_scenegraph_node_root());

    environment_loading_info.scene_loader.get_scenegraph_node_root().reset(reinterpret_cast<SceneGraphNode*>(NULL));
}

void aristaeus::gfx::vulkan::CoreRenderer::init_imgui() {
    ImGui_ImplVulkan_InitInfo init_info{
        .Instance = *m_window.get_instance(),
        .PhysicalDevice = *m_window.get_physical_device(),
        .Device = *m_logical_device,
        .QueueFamily = m_window.get_queue_indices_info().graphics_family.value(),
        .Queue = *m_graphics_queue,
        // TODO: add init_info.PipelineCache
        .DescriptorPool = *m_imgui_descriptor_pool,
        .Subpass = get_default_rasterization_subpass(),
        .MinImageCount = 35,
        .ImageCount = 35,
        .MSAASamples = VK_SAMPLE_COUNT_1_BIT
    };

    ImGui_ImplVulkan_Init(&init_info, **get_default_rasterization_render_pass());

    ImGuiIO &io = ImGui::GetIO();
    io.Fonts->AddFontDefault();
    io.Fonts->Build();
}

std::vector<aristaeus::gfx::vulkan::VmaImage> aristaeus::gfx::vulkan::CoreRenderer::create_swap_chain_vma_images(
                                                                                   vk::raii::SwapchainKHR &swap_chain) {
    std::vector<VmaImage> return_value;

    vk::Extent2D swap_chain_extent = get_swap_chain_extent();

    for (vk::Image &image : swap_chain.getImages()) {
        return_value.push_back(VmaImage{
            .image = image,
            .image_height = swap_chain_extent.height,
            .image_width = swap_chain_extent.width,
            .allocation = nullptr,
            .allocator = nullptr,
            .array_layers = 1,
            .mip_levels = 1,
            .image_layouts = {vk::ImageLayout::eUndefined}
        });
    }

    return return_value;
}

aristaeus::gfx::vulkan::RenderPhase aristaeus::gfx::vulkan::CoreRenderer::create_shadow_render_phase() {
    // TODO(Bobby): perhaps move this to MaterialEnvironment?
    
    std::vector<vk::ClearValue> shadow_mapping_clear_values = {
        {vk::ClearDepthStencilValue{1.0f, 0}}
    };

    RenderPhase::RenderPassInfo shadow_render_pass_info{
        .after_function = after_shadow_phase,
        .after_function_arg = nullptr,
        .before_function = before_shadow_phase,
        .before_function_arg = nullptr,
        .clear_values = shadow_mapping_clear_values,
        .contextless_draw_function = before_context_shadow_phase,
        .contextless_draw_function_arg = nullptr,
        .draw_function = shadow_phase,
        .draw_function_arg = nullptr,
        .name = std::string{SHADOW_MAPPING_RENDER_PHASE_DIRECTIONAL_RENDER_PASS},
        .one_shot = true,
        .fire_one_shot_on_creation = false,
        .render_pass_attachments = get_default_depth_attachment_info(),
        .resolution = {}
    };

    return RenderPhase{ std::vector<RenderPhase::RenderPassInfo>{shadow_render_pass_info}, std::string{SHADOW_MAPPING_RENDER_PHASE} };
}

void aristaeus::gfx::vulkan::CoreRenderer::add_render_pass_dependencies(const RenderPhase::RenderPassInfo &render_pass_info, RenderPhaseDependencyNode &created_render_phase_node) {
    assert(m_render_phase_dep_tree != nullptr);

    std::string created_render_phase_name = created_render_phase_node.get_render_phase_name();

    for (const std::pair<const std::string, vk::PipelineStageFlags> &dependent : render_pass_info.phase_dependents) {
        std::string previous_root_render_phase_name = m_render_phase_dep_tree->get_render_phase_name();

        m_render_phase_dep_tree->add_dependency(created_render_phase_name, dependent.first, dependent.second);

        if (m_render_phase_dep_tree->get_render_phase_name() != previous_root_render_phase_name) {
            assert(m_render_phase_dep_tree->get_render_phase().get_render_phase_name() == created_render_phase_name);

            m_render_phases.at(previous_root_render_phase_name) = *m_render_phase_dep_tree->get_render_phase_node(previous_root_render_phase_name);
            m_render_phases.insert({ created_render_phase_name, created_render_phase_node });
        }
    }
}

void aristaeus::gfx::vulkan::CoreRenderer::refresh_render_phase_cache() {
    m_render_phases.clear();

    if (m_render_phase_dep_tree == nullptr) {
        return;
    }

    std::queue<std::reference_wrapper<gfx::vulkan::RenderPhaseDependencyNode>> node_queue;

    node_queue.push(*m_render_phase_dep_tree);

    while (!node_queue.empty()) {
        gfx::vulkan::RenderPhaseDependencyNode &dep_node = node_queue.front().get();

        m_render_phases.insert({dep_node.get_render_phase_name(), dep_node});

        for (const std::shared_ptr<gfx::vulkan::RenderPhaseDependencyNode> &child : dep_node.get_dependents()) {
            assert(child != nullptr);

            node_queue.push(*child);
        }

        node_queue.pop();
    }
}

aristaeus::gfx::vulkan::RenderPhase &aristaeus::gfx::vulkan::CoreRenderer::add_render_pass(const RenderPhase::RenderPassInfo &render_pass_info, bool create_render_phase, 
                                                                                           const std::optional<std::string> &render_phase_name) {
    for(std::pair<const std::string, std::reference_wrapper<RenderPhaseDependencyNode>> &render_phase_pair : m_render_phases) {
        RenderPhaseDependencyNode &render_phase_node_ref = render_phase_pair.second.get();
        RenderPhase &render_phase_ref = render_phase_node_ref.get_render_phase();

        assert(render_phase_pair.first == render_phase_ref.get_render_phase_name());

        if (render_pass_info.phase_dependents.contains(render_phase_ref.get_render_phase_name())) {
            continue;
        }

        bool compatible_with_render_phase = true;

        for (const std::pair<const std::string, vk::PipelineStageFlags> &current_render_phase_dep : render_phase_node_ref.get_all_dependencies()) {
            if (render_pass_info.phase_dependents.contains(current_render_phase_dep.first)) {
                compatible_with_render_phase = false;
                break;
            }
        }

        if (compatible_with_render_phase) {
            for (const std::pair<const std::string, vk::PipelineStageFlags>& dependency : render_pass_info.phase_dependencies) {
                // TODO(Bobby): add full dependency cycle checking
                if (!render_phase_ref.m_dependencies.contains(dependency.first) ||
                    render_phase_ref.m_dependencies.at(dependency.first) != dependency.second) {
                    compatible_with_render_phase = false;
                    break;
                }
            }
        }

        if (compatible_with_render_phase) {
            render_phase_ref.add_render_pass(render_pass_info, false, render_phase_node_ref.get_all_dependents());

            m_render_phase_render_context_id_map.at(render_phase_pair.first) =
                glm::uvec2{ m_last_assigned_render_context_id + 1, m_last_assigned_render_context_id + render_phase_ref.get_num_of_render_contexts() };
            m_last_assigned_render_context_id = m_render_phase_render_context_id_map.at(render_phase_pair.first).y - 1;

            render_phase_ref.m_render_context_id = m_render_phase_render_context_id_map.at(render_phase_pair.first).x;

            //add_render_pass_dependencies(render_pass_info, render_phase_node_ref);

            return render_phase_ref;
        }
    }

    if (!create_render_phase || !render_phase_name.has_value()) {
        throw std::runtime_error("Can't create new render phase for render pass " + render_pass_info.name + " which needs one");
    }
    else {
        if (m_render_phases.contains(*render_phase_name)) {
            throw std::runtime_error("Render phase with the name " + *render_phase_name + " already exists!");
        }

        RenderPhaseDependencyNode &render_phase_node = 
            m_render_phase_dep_tree->add_render_phase(RenderPhase{ std::vector<RenderPhase::RenderPassInfo>{render_pass_info}, *render_phase_name });

        register_render_phase(render_phase_node.get_render_phase());

        add_render_pass_dependencies(render_pass_info, render_phase_node);

        refresh_render_phase_cache();

        return render_phase_node.get_render_phase();
    }
}

vk::raii::DescriptorPool aristaeus::gfx::vulkan::CoreRenderer::create_descriptor_pool() {
    std::array<vk::DescriptorPoolSize, 1> pool_sizes {{
      {vk::DescriptorType::eCombinedImageSampler, 35u}
    }};

    return {m_logical_device, {{vk::DescriptorPoolCreateFlagBits::eFreeDescriptorSet}, 35, pool_sizes}};
}

void aristaeus::gfx::vulkan::CoreRenderer::render_render_phase_nodes() {
    std::vector<vk::SubmitInfo> submit_infos;

    vk::CommandBuffer loading_task_cmd_buffer = m_graphics_command_buffer_manager.get_primary_command_buffer(m_current_frame, RENDER_CONTEXT_ID_PRELOAD);
    vk::Semaphore frame_available_semaphore = *m_frame_available_image[m_current_frame];
    vk::Semaphore loading_task_semaphore = *m_loading_task_semaphores[m_current_frame];
    vk::PipelineStageFlags frame_available_stage_flags = vk::PipelineStageFlagBits::eColorAttachmentOutput;

    loading_task_cmd_buffer.reset({});
    loading_task_cmd_buffer.begin(vk::CommandBufferBeginInfo{});

    if(Window::PERFORMANCE_MARKERS_ENABLED) {
        vk::DebugUtilsLabelEXT pre_render_pass_debug_marker{ "Preload pass" };

        loading_task_cmd_buffer.beginDebugUtilsLabelEXT(pre_render_pass_debug_marker);
    }

    m_do_loading_tasks_exist =
            m_graphics_command_buffer_manager.record_all_secondary_command_buffers(RENDER_CONTEXT_ID_PRELOAD, 0) > 0 ||
            m_loading_tasks_added;

    if (m_do_loading_tasks_exist) {
        m_loading_tasks_added = false;

        if(Window::PERFORMANCE_MARKERS_ENABLED) {
            loading_task_cmd_buffer.endDebugUtilsLabelEXT();
        }
        loading_task_cmd_buffer.end();

        submit_infos.push_back(vk::SubmitInfo{ frame_available_semaphore, frame_available_stage_flags, loading_task_cmd_buffer, loading_task_semaphore });
    }

    std::unordered_map<vk::Semaphore, std::reference_wrapper<RenderPhase>> signal_semaphore_render_phases;

    m_render_phase_dep_tree->get_submit_infos(submit_infos);

    m_current_render_phase = nullptr;

    m_graphics_queue.submit(submit_infos, *m_frame_in_flight_fences[m_current_frame]);
}

void aristaeus::gfx::vulkan::CoreRenderer::before_default_render_phase() {
    CoreRenderer &core_renderer = utils::get_singleton_safe<CoreRenderer>();
    RenderPhaseID default_render_phase_id = core_renderer.get_render_phase(core_renderer.get_default_rasterization_render_phase_name())->get_render_phase_id().value();

    vk::CommandBuffer primary_command_buffer =
        core_renderer.m_graphics_command_buffer_manager.get_primary_command_buffer(core_renderer.m_current_frame, default_render_phase_id);

    if (!core_renderer.m_setup_imgui) {
        ImGui_ImplVulkan_CreateFontsTexture(primary_command_buffer);
        core_renderer.m_setup_imgui = true;
    }

    core_renderer.m_perspective_camera_environment.input(core_renderer.m_window);

    core_renderer.m_perspective_camera_environment.predraw_load(primary_command_buffer);
}

void aristaeus::gfx::vulkan::CoreRenderer::after_default_render_phase() {
    CoreRenderer &core_renderer = utils::get_singleton_safe<CoreRenderer>();
    vk::CommandBuffer primary_command_buffer =
        core_renderer.m_graphics_command_buffer_manager.get_primary_command_buffer(core_renderer.m_current_frame);
    uint32_t current_swap_chain_image = core_renderer.get_current_swap_chain_image();

    // force transition from eColorAttachmentOptimal to ePresentSrcKHR
    /*core_renderer.m_swap_chain_images[current_swap_chain_image].image_layouts[0] = vk::ImageLayout::eColorAttachmentOptimal;

    core_renderer.m_swap_chain_images[current_swap_chain_image].transition_image_layout(primary_command_buffer, vk::ImageLayout::ePresentSrcKHR);*/
}

void aristaeus::gfx::vulkan::CoreRenderer::draw_frame() {
    // TODO(Bobby): Move the captured skybox stage after the shadow mapping stage

    // step 1: wait for previous frame to finish

    if(m_logical_device.waitForFences(*m_frame_in_flight_fences[m_current_frame], VK_TRUE,
                                      UINT64_MAX) != vk::Result::eSuccess) {
        throw std::runtime_error("Failed to wait for frame in flight to finish!");
    }

    ImGui_ImplGlfw_NewFrame();
    ImGui_ImplVulkan_NewFrame();
    ImGui::NewFrame();
    ImGuizmo::BeginFrame();

    m_render_phase_dep_tree->contextless_draw();

    call_event<void *>(std::string{ DRAW_FRAME_EVENT }, nullptr);

    // step 1.5: call all post render callbacks and clear them

    std::vector<PostRenderCallbackInfo> current_post_render_callbacks = m_post_render_callback_infos[m_current_frame];

    m_post_render_callback_infos[m_current_frame].clear();

    for (PostRenderCallbackInfo &callback_info : current_post_render_callbacks) {
        callback_info.first(callback_info.second);
    }

    // step 2: wait for all pre-render tasks to finish and add their secondary command buffers in

    m_task_manager.wait_and_clear_pre_render_taskflows(m_current_frame);

    m_previous_temporary_buffers[m_current_frame].clear();

    for(size_t i = 0; m_temporary_buffers[m_current_frame].size() > i; ++i) {
        m_previous_temporary_buffers[m_current_frame].push_back(std::move(m_temporary_buffers[m_current_frame][i]));
    }

    // step 3: acquire image from swap chain
    std::pair<vk::Result, uint32_t> next_image = m_swap_chain.acquireNextImage(
                                                                            UINT64_MAX,
                                                                            *m_frame_available_image[m_current_frame],
                                                                            VK_NULL_HANDLE);

    vk::PresentInfoKHR present_info {*m_frame_finished_rendering[m_current_frame], *m_swap_chain,
                                      next_image.second };

    if(next_image.first == vk::Result::eErrorOutOfDateKHR) {
        //recreate_swap_chain();
    } else if(next_image.first != vk::Result::eSuccess && next_image.first != vk::Result::eSuboptimalKHR) {
        throw std::runtime_error("Failed to acquire swap chain image!");
    }

    m_logical_device.resetFences(*m_frame_in_flight_fences[m_current_frame]);

    // step 4: populate command buffers for all render phases,
    // and when image has been acquired and the previous frame has finished presenting, send render commands

    m_window.wait_until_un_minimized();

    m_current_swap_chain_image = next_image.second;

    call_event<void*>(std::string{ NEW_FRAME_EVENT }, nullptr);

    render_render_phase_nodes();

    m_window.update_imgui_input();

    // step 5: Submit result to swap chain to present on screen
    try {
        vk::Result present_result = m_presentation_queue.presentKHR(present_info);

        bool should_frame_buffer_be_resized = m_window.should_frame_buffer_be_resized();

        if (present_result == vk::Result::eSuboptimalKHR || should_frame_buffer_be_resized) {
            //recreate_swap_chain();
        } else if (present_result != vk::Result::eSuccess) {
            throw std::runtime_error("Unable to present image from swap chain!");
        }
    } catch(vk::OutOfDateKHRError &out_of_date_error) {
        recreate_swap_chain();
    } 

    m_current_frame = (m_current_frame + 1) % utils::vulkan::MAX_FRAMES_IN_FLIGHT;
    ++m_total_frames;

    call_event<void *>(std::string{ FRAME_DONE_EVENT }, nullptr);
}

void aristaeus::gfx::vulkan::CoreRenderer::before_shadow_phase(void*) {
    CoreRenderer &core_renderer = utils::get_singleton_safe<CoreRenderer>();

    vk::CommandBuffer primary_command_buffer =
        core_renderer.m_graphics_command_buffer_manager.get_primary_command_buffer(core_renderer.m_current_frame);

    core_renderer.m_perspective_camera_environment.predraw_load(primary_command_buffer);

    core_renderer.m_perspective_camera_environment.capture_point_light_shadows(primary_command_buffer, 
                                                                               !core_renderer.m_point_light_shadows_captured);

    if(!core_renderer.m_point_light_shadows_captured) {
        core_renderer.m_point_light_shadows_captured = true;
    }
}

void aristaeus::gfx::vulkan::CoreRenderer::before_context_shadow_phase(void*) {
    CoreRenderer &core_renderer = utils::get_singleton_safe<CoreRenderer>();

    core_renderer.m_perspective_camera_environment.predraw();
}

void aristaeus::gfx::vulkan::CoreRenderer::shadow_phase(void*) {
    CoreRenderer &core_renderer = utils::get_singleton_safe<CoreRenderer>();

    core_renderer.draw_vulkan_drawables();
}

void aristaeus::gfx::vulkan::CoreRenderer::after_shadow_phase(void*) {
    CoreRenderer &core_renderer = utils::get_singleton_safe<CoreRenderer>();
    vk::CommandBuffer primary_command_buffer =
        core_renderer.m_graphics_command_buffer_manager.get_primary_command_buffer(core_renderer.m_current_frame);
    const std::string shadow_mapping_string{ SHADOW_MAPPING_RENDER_PHASE };

    SharedImageRaii &shadow_mapping_image = 
        core_renderer.m_render_phases.at(shadow_mapping_string).get().get_render_phase().m_render_pass_data.at(std::string{SHADOW_MAPPING_RENDER_PHASE_DIRECTIONAL_RENDER_PASS})
        .resources.value().images[core_renderer.m_current_frame];

    shadow_mapping_image->transition_image_layout(primary_command_buffer, vk::ImageLayout::eShaderReadOnlyOptimal);
}

void aristaeus::gfx::vulkan::CoreRenderer::draw_vulkan_drawables() {
    vk::CommandBuffer command_buffer = 
        m_graphics_command_buffer_manager.get_primary_command_buffer(m_current_frame);

    m_perspective_camera_environment.draw(command_buffer, *this, m_logical_device);
}

void aristaeus::gfx::vulkan::CoreRenderer::recreate_swap_chain() {
    m_window.wait_until_un_minimized();
    m_logical_device.waitIdle();

    vk::raii::SwapchainKHR new_swap_chain = m_window.create_swap_chain(m_logical_device, *m_swap_chain);
    m_swap_chain_extent = m_window.get_last_created_swap_chain_extent();
    m_swap_chain_image_format = m_window.get_last_created_swap_chain_image_format();

    m_swap_chain_images = create_swap_chain_vma_images(new_swap_chain);
    m_swap_chain_image_views = create_swap_image_views();

    m_depth_image_format = find_depth_image_format();
    m_depth_image = create_depth_image(m_swap_chain_extent);
    m_depth_image_view = create_depth_image_view(m_depth_image);

    for (size_t i = 0; m_set_to_shader_read_only.size() > i; ++i) {
        m_set_to_shader_read_only[i] = false;
    }

    m_swap_chain = std::move(new_swap_chain);
    m_point_light_shadows_captured = false;

    for (std::pair<const std::string, std::reference_wrapper<RenderPhaseDependencyNode>> &render_phase : m_render_phases) {
        render_phase.second.get().get_render_phase().on_swap_chain_recreation(m_swap_chain.getImages());
    }

    m_perspective_camera_environment.recreate_graphics_pipeline(*this, vk::Viewport{ 0.0f, 0.0f, static_cast<float>(m_swap_chain_extent.width), static_cast<float>(m_swap_chain_extent.height), 
                                                                                     0.0f, 1.0f });
}

void aristaeus::gfx::vulkan::CoreRenderer::generate_mipmaps(vk::CommandBuffer command_buffer, ImageRaii &image_raii,
                                                            uint32_t mip_levels, uint32_t layer_count, vk::PipelineBindPoint pipeline_bind_point) {
    if(mip_levels <= 1) {
        return;
    }

    vk::ImageSubresourceRange image_subresource_range {vk::ImageAspectFlagBits::eColor, 0, 1, 0, layer_count};
    vk::ImageMemoryBarrier image_barrier {
        {}, {}, {}, {}, VK_QUEUE_FAMILY_IGNORED, VK_QUEUE_FAMILY_IGNORED, image_raii->image,
        image_subresource_range
    };

    auto mip_height = static_cast<int32_t>(image_raii->image_height);
    auto mip_width = static_cast<int32_t>(image_raii->image_width);

    for(uint32_t i = 1; mip_levels > i; ++i) {
        vk::ImageSubresourceLayers src_subresource_layers {
            vk::ImageAspectFlagBits::eColor, i - 1, 0, 1
        };
        std::array<vk::Offset3D, 2> src_offsets {{
            {0, 0, 0},
            {mip_width, mip_height, 1}
        }};

        int32_t new_mip_width = std::max(mip_width / 2, 1);
        int32_t new_mip_height = std::max(mip_height / 2, 1);

        vk::ImageSubresourceLayers dst_subresource_layers {
            vk::ImageAspectFlagBits::eColor, i, 0, 1
        };
        std::array<vk::Offset3D, 2> dst_offsets {{
            {0, 0, 0},
            {new_mip_width, new_mip_height, 1}
        }};

        vk::ImageBlit image_blit {
            src_subresource_layers, src_offsets, dst_subresource_layers, dst_offsets
        };

        image_barrier.subresourceRange.baseMipLevel = i - 1;
        image_barrier.oldLayout = vk::ImageLayout::eTransferDstOptimal;
        image_barrier.newLayout = vk::ImageLayout::eTransferSrcOptimal;
        image_barrier.srcAccessMask = vk::AccessFlagBits::eTransferWrite;
        image_barrier.dstAccessMask = vk::AccessFlagBits::eTransferRead;

        // getting previous image ready to be transferred
        command_buffer.pipelineBarrier(vk::PipelineStageFlagBits::eTransfer, vk::PipelineStageFlagBits::eTransfer, {},
                                       {}, {}, image_barrier);
        // blitting one level of the image to another level of the image
        command_buffer.blitImage(image_raii->image, vk::ImageLayout::eTransferSrcOptimal,
                                 image_raii->image, vk::ImageLayout::eTransferDstOptimal,
                                 image_blit, vk::Filter::eLinear);

        mip_width = new_mip_width;
        mip_height = new_mip_height;
    }

    // allow all mipmaps to be read by shaders now that we are done
    std::vector<vk::ImageMemoryBarrier> image_memory_barriers;

    image_barrier.newLayout = vk::ImageLayout::eShaderReadOnlyOptimal;
    image_barrier.dstAccessMask = vk::AccessFlagBits::eShaderRead;

    for(uint32_t i = 0; mip_levels > i; ++i) {
        if(i == mip_levels - 1) {
            image_barrier.oldLayout = vk::ImageLayout::eTransferDstOptimal;
            image_barrier.srcAccessMask = vk::AccessFlagBits::eTransferWrite;
        } else {
            image_barrier.oldLayout = vk::ImageLayout::eTransferSrcOptimal;
            image_barrier.srcAccessMask = vk::AccessFlagBits::eTransferRead;
        }

        image_barrier.subresourceRange.baseMipLevel = i;

        for (uint32_t i2 = 0; layer_count > i2; ++i2) {
            image_raii->image_layouts[i + (i2 * mip_levels)] = vk::ImageLayout::eShaderReadOnlyOptimal;
        }

        image_memory_barriers.push_back(image_barrier);
    }

    vk::PipelineStageFlags shader_stage = pipeline_bind_point == vk::PipelineBindPoint::eCompute ? vk::PipelineStageFlagBits::eComputeShader : vk::PipelineStageFlagBits::eVertexShader;

    command_buffer.pipelineBarrier(vk::PipelineStageFlagBits::eTransfer, shader_stage, {}, {}, {}, image_memory_barriers);
}

aristaeus::gfx::vulkan::CoreRenderer::~CoreRenderer() {
    ImGui_ImplVulkan_Shutdown();
}

void aristaeus::gfx::vulkan::CoreRenderer::copy_buffer_to_image(vk::CommandBuffer command_buffer, BufferRaii &src_buffer, ImageRaii &dst_image, 
                                                                uint32_t image_layer, uint32_t image_layer_count,
                                                                vk::DeviceSize buffer_offset, uint32_t mip_level, uint32_t mip_level_count,
                                                                std::optional<vk::ImageLayout> target_image_layout, vk::PipelineBindPoint pipeline_bind_point) {
    vk::BufferImageCopy image_region = {buffer_offset, 0, 0,
                                        {vk::ImageAspectFlagBits::eColor, 0, image_layer, image_layer_count}, {0, 0, 0},
                                        {dst_image->image_width, dst_image->image_height, 1}};

    dst_image->transition_image_layout(command_buffer, vk::ImageLayout::eTransferDstOptimal, pipeline_bind_point, image_layer, mip_level,
                                       image_layer_count, mip_level_count);

    for (size_t i = mip_level; mip_level_count + mip_level > i; ++i) {
        image_region.imageSubresource.mipLevel = i;

        command_buffer.copyBufferToImage(src_buffer->buffer, dst_image->image, vk::ImageLayout::eTransferDstOptimal, image_region);

        image_region.bufferOffset += image_layer_count * 4lu * image_region.imageExtent.width * image_region.imageExtent.height * sizeof(float);
        image_region.imageExtent.width /= 2;
        image_region.imageExtent.height /= 2;
    }

    if(target_image_layout.has_value()) {
        // used to be vk::ImageLayout::eShaderReadOnlyOptimal transition
        dst_image->transition_image_layout(command_buffer, *target_image_layout, pipeline_bind_point, image_layer, mip_level, image_layer_count, 
                                           mip_level_count);
    }
}

void aristaeus::gfx::vulkan::CoreRenderer::copy_image_to_buffer(vk::CommandBuffer command_buffer, ImageRaii &src_image, BufferRaii &dst_buffer, vk::ImageLayout current_image_layout,  
                                                                uint32_t image_layer, uint32_t image_layer_count, vk::DeviceSize buffer_offset, uint32_t mip_levels,
                                                                bool transition_after_copy, std::optional<vk::ImageLayout> target_image_layout, vk::PipelineBindPoint pipeline_bind_point) {
    glm::uvec2 working_src_image_size{src_image->image_width, src_image->image_height};

    vk::BufferImageCopy image_region { buffer_offset, 0, 0, {vk::ImageAspectFlagBits::eColor, 0, image_layer, image_layer_count}, {0, 0, 0},
                                       { working_src_image_size.x, working_src_image_size.y, 1} };

    bool in_transferrable_image_layout = current_image_layout == vk::ImageLayout::eGeneral || current_image_layout == vk::ImageLayout::eTransferSrcOptimal;

    if (!in_transferrable_image_layout) {
        src_image->transition_image_layout(command_buffer, vk::ImageLayout::eTransferSrcOptimal, pipeline_bind_point, image_layer, {}, image_layer_count, mip_levels);
    }

    vk::ImageLayout transitioned_image_layout = current_image_layout == vk::ImageLayout::eGeneral ? vk::ImageLayout::eGeneral : vk::ImageLayout::eTransferSrcOptimal;

    for (size_t i = 0; mip_levels > i; ++i) {
        image_region.imageSubresource.mipLevel = i;

        command_buffer.copyImageToBuffer(src_image->image, transitioned_image_layout, dst_buffer->buffer, image_region);

        image_region.bufferOffset += image_layer_count * 4lu * image_region.imageExtent.width * image_region.imageExtent.height * sizeof(float);
        image_region.imageExtent.width /= 2;
        image_region.imageExtent.height /= 2;
    }

    if (!in_transferrable_image_layout && transition_after_copy) {
        src_image->transition_image_layout(command_buffer, vk::ImageLayout::eTransferDstOptimal, pipeline_bind_point, image_layer, {}, image_layer_count, mip_levels);
    }
}

vk::raii::Semaphore &aristaeus::gfx::vulkan::CoreRenderer::get_render_phase_semaphore(const std::string &phase_name, uint32_t frame_num) {
    if (phase_name == FRAME_AVAILABLE_DEP) {
        if (m_do_loading_tasks_exist) {
            return m_loading_task_semaphores[frame_num];
        }
        else {
            return m_frame_available_image[frame_num];
        }
    }
    else if (phase_name == FRAME_DONE_DEP) {
        return m_frame_finished_rendering[frame_num];
    }
    else if (m_render_phases.contains(phase_name)) {
        return m_render_phases.at(phase_name).get().get_render_phase().get_signal_semaphore(frame_num);
    }
    else {
        throw std::runtime_error("Couldn't find render phase semaphore!");
    }
}

[[nodiscard]] std::vector<aristaeus::gfx::vulkan::RenderPhase::AttachmentInfo> aristaeus::gfx::vulkan::CoreRenderer::get_default_color_attachment_info() const {
    std::array<vk::AttachmentDescription, 2> attachments = { {
        // color attachment
        {{}, m_swap_chain_image_format, vk::SampleCountFlagBits::e1, vk::AttachmentLoadOp::eClear,
        vk::AttachmentStoreOp::eStore, vk::AttachmentLoadOp::eDontCare, vk::AttachmentStoreOp::eDontCare,
        vk::ImageLayout::eUndefined, vk::ImageLayout::eColorAttachmentOptimal},
        // depth/stencil attachment
        {{}, m_depth_image_format, vk::SampleCountFlagBits::e1, vk::AttachmentLoadOp::eClear,
        vk::AttachmentStoreOp::eDontCare, vk::AttachmentLoadOp::eClear, vk::AttachmentStoreOp::eDontCare,
        vk::ImageLayout::eUndefined, vk::ImageLayout::eDepthStencilAttachmentOptimal}
    }};
    return {{attachments[0], vk::ImageUsageFlagBits::eColorAttachment}, {attachments[1], vk::ImageUsageFlagBits::eDepthStencilAttachment } };
}

[[nodiscard]] std::vector<aristaeus::gfx::vulkan::RenderPhase::AttachmentInfo> aristaeus::gfx::vulkan::CoreRenderer::get_default_depth_attachment_info() const {
    vk::AttachmentDescription attachment_description{
        {}, m_depth_image_format, vk::SampleCountFlagBits::e1, vk::AttachmentLoadOp::eClear,
         vk::AttachmentStoreOp::eStore, vk::AttachmentLoadOp::eClear, vk::AttachmentStoreOp::eDontCare,
         vk::ImageLayout::eUndefined, vk::ImageLayout::eShaderReadOnlyOptimal
    };
    return {{attachment_description, vk::ImageUsageFlagBits::eDepthStencilAttachment | vk::ImageUsageFlagBits::eSampled } };
}

aristaeus::gfx::vulkan::RenderContextID aristaeus::gfx::vulkan::CoreRenderer::assign_render_context_id(RenderPhase &render_phase) {
    std::string render_phase_name = render_phase.get_render_phase_name();
    size_t render_context_num = render_phase.get_num_of_render_contexts();

    glm::uvec2 render_context_id_range{ m_last_assigned_render_context_id + 1, m_last_assigned_render_context_id + 1 + render_context_num };

    m_render_phase_render_context_id_map.insert({ render_phase_name, render_context_id_range });
    m_last_assigned_render_context_id = render_context_id_range.y - 1;

    render_phase.m_render_context_id = render_context_id_range.x;

    return render_context_id_range.x;
}

[[nodiscard]] aristaeus::gfx::vulkan::RenderPhase *aristaeus::gfx::vulkan::CoreRenderer::get_render_phase(RenderContextID render_context_id) {
    assert(m_render_phase_render_context_id_map.size() == m_render_phases.size());

    for (std::pair<const std::string, glm::uvec2> &render_phase : m_render_phase_render_context_id_map) {
        if (render_context_id >= render_phase.second.x && render_context_id < render_phase.second.y) {
            return &m_render_phases.at(render_phase.first).get().get_render_phase();
        }
    }

    return nullptr;
}

void aristaeus::gfx::vulkan::CoreRenderer::register_render_phase(RenderPhase &render_phase) {
    assign_render_context_id(render_phase);
    m_graphics_command_buffer_manager.add_render_phase();

    render_phase.m_phase_id = m_last_assigned_render_phase_id++;

    if (render_phase.m_dependencies.empty()) {
        vk::PipelineStageFlags wait_stage_mask = vk::PipelineStageFlagBits::eColorAttachmentOutput;

        render_phase.m_dependencies.insert({ std::string{ FRAME_AVAILABLE_DEP }, wait_stage_mask });
    }
}

std::unordered_map<std::string, std::reference_wrapper<aristaeus::gfx::vulkan::RenderPhaseDependencyNode>> aristaeus::gfx::vulkan::CoreRenderer::init_render_phases() {
    std::unordered_map<std::string, std::reference_wrapper<RenderPhaseDependencyNode>> return_value;

    RenderPhase shadow_render_phase = create_shadow_render_phase();

    register_render_phase(shadow_render_phase);

    m_render_phase_dep_tree = std::make_shared<RenderPhaseDependencyNode>(std::move(shadow_render_phase));

    m_render_phase_dep_tree->set_weak_ptr_to_self(m_render_phase_dep_tree);

    return_value.insert(std::pair<std::string, std::reference_wrapper<RenderPhaseDependencyNode>>{ m_render_phase_dep_tree->get_render_phase_name(), *m_render_phase_dep_tree });

    add_event<void*>(std::string{DRAW_FRAME_EVENT});
    add_event<void*>(std::string{NEW_FRAME_EVENT});
    add_event<void*>(std::string{FRAME_DONE_EVENT});
    add_event<void*>(std::string{WINDOW_DESTRUCTION_EVENT});
    add_event<void*>(std::string{DEFAULT_RENDER_PASS_DRAW_EVENT});

    return return_value;
}


void aristaeus::gfx::vulkan::CoreRenderer::predraw_load() {
    vk::CommandBuffer command_buffer = get_graphics_command_buffer_manager().get_primary_command_buffer(m_current_frame);

    m_perspective_camera_environment.predraw_load(command_buffer);
}

void aristaeus::gfx::vulkan::CoreRenderer::copy_buffer(BufferRaii &src_buffer, BufferRaii &dst_buffer) {
    if (src_buffer->buffer_size != dst_buffer->buffer_size) {
        throw std::runtime_error("Buffer sizes on copy buffer operation do not match!");
    }

    vk::BufferCopy buffer_copy{ 0, 0, src_buffer->buffer_size };
    vk::CommandBuffer command_buffer = m_graphics_command_buffer_manager.get_command_buffer_for_thread(RENDER_CONTEXT_ID_PRELOAD);

    command_buffer.copyBuffer(src_buffer->buffer, dst_buffer->buffer, buffer_copy);
}

void aristaeus::gfx::vulkan::CoreRenderer::copy_cubemap_buffer_to_image(BufferRaii &src_buffer, ImageRaii &dst_image,
    vk::DeviceSize buffer_image_offset, vk::PipelineBindPoint pipeline_bind_point) {
    vk::CommandBuffer command_buffer = m_graphics_command_buffer_manager.get_command_buffer_for_thread(RENDER_CONTEXT_ID_PRELOAD);
    std::array<vk::BufferImageCopy, 6> image_regions;

    dst_image->transition_image_layout(command_buffer, vk::ImageLayout::eTransferDstOptimal);

    for (uint32_t i = 0; image_regions.size() > i; ++i) {
        image_regions[i] = { i * buffer_image_offset, 0, 0,
                            {vk::ImageAspectFlagBits::eColor, 0, i, 1}, {0, 0, 0},
                            {dst_image->image_width, dst_image->image_height, 1} };
    }

    command_buffer.copyBufferToImage(src_buffer->buffer, dst_image->image, vk::ImageLayout::eTransferDstOptimal,
        image_regions);

    dst_image->transition_image_layout(command_buffer, vk::ImageLayout::eShaderReadOnlyOptimal, pipeline_bind_point);
}

void aristaeus::gfx::vulkan::CoreRenderer::copy_buffer_to_image(BufferRaii &src_buffer, ImageRaii &dst_image, uint32_t image_layer, uint32_t image_layer_count, 
                                                                vk::DeviceSize buffer_offset, uint32_t mip_levels, uint32_t mip_levels_copying,
                                                                std::optional<vk::ImageLayout> target_image_layout, vk::PipelineBindPoint pipeline_bind_point) {
    vk::CommandBuffer command_buffer = m_graphics_command_buffer_manager.get_command_buffer_for_thread(RENDER_CONTEXT_ID_PRELOAD);

    copy_buffer_to_image(command_buffer, src_buffer, dst_image, image_layer, image_layer_count, buffer_offset, 
                         mip_levels, mip_levels_copying, target_image_layout, pipeline_bind_point);
}

void aristaeus::gfx::vulkan::CoreRenderer::generate_mipmaps(ImageRaii &image_raii, uint32_t layer_count, uint32_t mip_levels, vk::PipelineBindPoint pipeline_bind_point) {
    vk::CommandBuffer command_buffer = m_graphics_command_buffer_manager.get_command_buffer_for_thread(RENDER_CONTEXT_ID_PRELOAD);

    // TODO(Bobby): look into optimizing this so it actually transitions all the images into the proper layout and not just into dst
    image_raii->transition_image_layout(command_buffer, vk::ImageLayout::eTransferDstOptimal, pipeline_bind_point, {}, {}, layer_count, mip_levels);

    generate_mipmaps(command_buffer, image_raii, mip_levels, layer_count);
}

vk::raii::Sampler aristaeus::gfx::vulkan::CoreRenderer::create_default_sampler() {
    vk::SamplerCreateInfo sampler_create_info = AssimpModel::DEFAULT_SAMPLER_CREATE_INFO;

    sampler_create_info.maxAnisotropy = get_max_sampler_anisotropy();

    return create_sampler(sampler_create_info);
}