template<aristaeus::core::AristaeusConcreteClass Class>
void aristaeus::core::ClassDB::register_class() {
    StdTypeInfoRef type_info_ref = typeid(Class);

    ClassInfo class_info {
        .class_type_name = Class::CLASS_TYPE,
        .class_type = type_info_ref,
        .parent_class_types = Class::parent_class_types,
        .is_node = std::is_base_of_v<gfx::SceneGraphNode, Class> || std::is_same_v<gfx::SceneGraphNode, Class>,
        .is_ui_initializable = true,
        .constructor_function = []{
            Class *class_ptr = new Class();

            return class_ptr;
        },
        .deleter_function = [](void* class_ptr) {delete reinterpret_cast<Class*>(class_ptr);}
    };

    m_class_ptr_info_aliases.insert({ typeid(Class*), type_info_ref });

    m_class_infos.insert({ type_info_ref, class_info });
    m_class_type_names.insert({ Class::CLASS_TYPE, type_info_ref });

    Class::classdb_register();
}

template<aristaeus::core::AristaeusAbstractClass Class>
void aristaeus::core::ClassDB::register_abstract_class() {
    StdTypeInfoRef type_info_ref = typeid(Class);

    ClassInfo class_info {
        .class_type_name = Class::CLASS_TYPE,
        .class_type = type_info_ref,
        .parent_class_types = Class::parent_class_types,
        .is_node = std::is_base_of_v<gfx::SceneGraphNode, Class> || std::is_same_v<gfx::SceneGraphNode, Class>,
        .constructor_function = []{return nullptr;},
        .deleter_function = [](void* class_ptr) {delete reinterpret_cast<Class*>(class_ptr);}
    };

    m_class_ptr_info_aliases.insert({ typeid(Class*), type_info_ref });

    m_class_infos.insert({ type_info_ref, class_info });
    m_class_type_names.insert({ Class::CLASS_TYPE, type_info_ref });

    Class::classdb_register();
}

template<aristaeus::core::AristaeusNode Node, typename ArgumentType>
void aristaeus::core::ClassDB::register_event_callback_on_construction(const std::string &event_name,
                                                          const std::string &callback_name,
                                                          const std::function<void(Node&, ArgumentType)>
                                                          &event_callback) {
    StdTypeInfoRef type_info_ref{typeid(Node)};

    EventCallbackInfo event_callback_info {
        .argument_type_info = typeid(ArgumentType),
        .event_name = event_name,
        .callback_name = callback_name,
        .callback_function = [event_callback](gfx::SceneGraphNode &scene_graph_node, void *argument) {
            Node &node = utils::compile_checked_down_cast<gfx::SceneGraphNode, Node>(&scene_graph_node);

            event_callback(node, *reinterpret_cast<ArgumentType*>(argument));
        }
    };

    if(m_event_callbacks.contains(type_info_ref)) {
        m_event_callbacks.at(type_info_ref).push_back(event_callback_info);
    } else {
        m_event_callbacks.insert({type_info_ref, std::vector<EventCallbackInfo>{event_callback_info}});
    }
}

template<aristaeus::core::AristaeusBaseClass ClassType, aristaeus::core::AristaeusClassPtr ClassPtr>
void aristaeus::core::ClassDB::register_property_class_ptr(const std::string &property_name, const std::function<ClassPtr(ClassType&)> &property_getter,
                                                           const std::function<void(ClassType&, ClassPtr)> &property_setter,
                                                           const std::optional<std::function<ClassPtr()>> &default_constructor_function) {
    ClassInfo &node_info = m_class_infos.at(typeid(ClassType));
    PropertyInfo property_info{
        .name = property_name,
        .type = StdTypeInfoRef{typeid(ClassPtr)},
        .getter = [property_getter](void *class_instance) {
            assert(class_instance != nullptr);

            return property_getter(*(reinterpret_cast<ClassType*>(class_instance)));
        },
        .setter = [property_setter](void *class_instance, void *type_instance) {
            assert(class_instance != nullptr);
            assert(type_instance != nullptr);

            property_setter(*(reinterpret_cast<ClassType*>(class_instance)),
                            reinterpret_cast<ClassPtr>(type_instance));
        }
    };

    if (default_constructor_function.has_value()) {
        property_info.default_constructor_function = [default_constructor_function] {
            return default_constructor_function.value()();
        };
    } else {
        node_info.required_properties.insert(property_name);
    }

    node_info.properties.insert({property_name, property_info});
}


template<aristaeus::core::AristaeusBaseClass ClassType, typename Type>
//requires(std::not_same_as)
void aristaeus::core::ClassDB::register_property(const std::string &property_name,
                                                 const std::function<Type (ClassType &)> &property_getter,
                                                 const std::function<void (ClassType &, Type)> &property_setter,
                                                 const std::optional<std::function<Type ()>> &default_constructor_function) {
    if (!m_class_infos.contains(typeid(ClassType))) {
        throw std::runtime_error("Node type is not registered");
    }

    ClassInfo &node_info = m_class_infos.at(typeid(ClassType));
    PropertyInfo property_info{
        .name = property_name,
        .type = StdTypeInfoRef{typeid(Type)},
        .getter = [property_getter](void *class_instance) {
            assert(class_instance != nullptr);

            return new Type(property_getter(*(reinterpret_cast<ClassType*>(class_instance))));
        },
        .setter = [property_setter](void *class_instance, void *type_instance) {
            assert(class_instance != nullptr);
            assert(type_instance != nullptr);

            property_setter(*(reinterpret_cast<ClassType*>(class_instance)),
                            std::move(*(reinterpret_cast<Type*>(type_instance))));
        }
    };

    if (!m_type_infos.contains(property_info.type)) {
        throw std::runtime_error("Property type is not registered");
    }

    if (!m_type_infos.at(property_info.type).ui_functions.has_value()) {
        node_info.is_ui_initializable = false;
    }

    if (default_constructor_function.has_value()) {
        property_info.default_constructor_function = [default_constructor_function] {
            return new Type(std::move(default_constructor_function.value()()));
        };
    } else {
        node_info.required_properties.insert(property_name);
    }

    node_info.properties.insert({property_name, property_info});
}

template<typename Type, typename UiFunctionsType>
void aristaeus::core::ClassDB::register_type_base_no_vector(std::function<Type(const nlohmann::json::value_type&)> parse_function,
                                                            std::optional<UiFunctions<UiFunctionsType>> ui_functions, bool register_supplemental_types) {
    std::optional<InternalUiFunctions> internal_ui_functions{};

    if (ui_functions.has_value()) {
        internal_ui_functions = convert_to_internal_ui_functions<UiFunctionsType>(ui_functions.value());
    }

    register_type_unsafe(typeid(Type), [parse_function](const nlohmann::json::value_type &json) {
        Type type_parsed = parse_function(json);

        return new Type(std::move(type_parsed));
    }, [](void* type_ptr) {delete reinterpret_cast<Type*>(type_ptr); }, internal_ui_functions);

    if (register_supplemental_types) {
        std::optional<InternalUiFunctions> optional_type_ui_functions{};
        std::optional<InternalUiFunctions> vector_type_ui_functions{};

        if (ui_functions.has_value()) {
            optional_type_ui_functions = InternalUiFunctions{
                .persistent_state_type = typeid(std::optional<UiFunctionsType>),
                .parse_function = [ui_functions](void* persistent_state, bool display_ui, const std::optional<std::string> &label_hint) {
                    std::optional<UiFunctionsType> &persistent_state_optional = *reinterpret_cast<std::optional<UiFunctionsType>*>(persistent_state);

                    if (display_ui && label_hint.has_value() && !ImGui::TreeNode(label_hint.value().c_str())) {
                        display_ui = false;
                    }

                    nlohmann::json return_value;

                    if (persistent_state_optional.has_value()) {
                        if (display_ui && ImGui::Button("De-init optional value")) {
                            persistent_state_optional.reset();
                            return_value = nlohmann::json{};
                        }
                        else {
                            return_value = ui_functions.value().parse_function(persistent_state_optional.value(), display_ui, "Value");
                        }
                    }
                    else {
                        if (display_ui && ImGui::Button("Init optional value")) {
                            delete &persistent_state_optional;

                            UiFunctionsType type_instance = ui_functions.value().constructor_function();

                            persistent_state = new std::optional<UiFunctionsType>(std::move(type_instance));
                        }

                        return_value = nlohmann::json{};
                    }

                    if (display_ui && label_hint.has_value()) {
                        ImGui::TreePop();
                    }

                    return return_value;
                },
                .constructor_function = [] {
                    return new std::optional<UiFunctionsType>();
                }
            };
        }

        register_type_unsafe(typeid(std::optional<Type>), [parse_function](const nlohmann::json::value_type &json) {
            if(json.empty()) {
                return new std::optional<Type>{};
            } else {
                Type type_parsed = parse_function(json);

                return new std::optional<Type>(std::move(type_parsed));
            }
        }, [](void* type_ptr) {delete reinterpret_cast<std::optional<Type>*>(type_ptr); }, optional_type_ui_functions);
    }
}

template<typename Type, typename UiFunctionsType>
void aristaeus::core::ClassDB::register_type_base(std::function<Type (const nlohmann::json::value_type&)> parse_function,
                                                  std::optional<UiFunctions<UiFunctionsType>> ui_functions, bool register_supplemental_types) {
    register_type_base_no_vector<Type, UiFunctionsType>(parse_function, ui_functions, register_supplemental_types);

    if(register_supplemental_types) {
        std::optional<InternalUiFunctions> vector_type_ui_functions{};

        if (ui_functions.has_value()) {
            vector_type_ui_functions = InternalUiFunctions{
                .persistent_state_type = typeid(std::vector<UiFunctionsType>),
                .parse_function = [ui_functions](void *persistent_state, bool display_ui, const std::optional<std::string> &label_hint) {
                    std::vector<UiFunctionsType> &persistent_state_vector = *reinterpret_cast<std::vector<UiFunctionsType>*>(persistent_state);
                    size_t vector_size = persistent_state_vector.size();

                    if (display_ui && label_hint.has_value() && !ImGui::TreeNode(label_hint.value().c_str())) {
                        display_ui = false;
                    }

                    if(display_ui) {
                        ImGui::InputScalar("Vector size", ImGuiDataType_U64, &vector_size, nullptr);

                        if (persistent_state_vector.size() > vector_size) {
                            persistent_state_vector.resize(vector_size);
                        }
                        else if (persistent_state_vector.size() < vector_size) {
                            persistent_state_vector.reserve(vector_size);

                            for (size_t i = 0; vector_size - persistent_state_vector.size() > i; ++i) {
                                UiFunctionsType type_instance = ui_functions.value().constructor_function();

                                persistent_state_vector.push_back(std::move(type_instance));
                            }
                        }
                    }

                    nlohmann::json return_value{};

                    for (size_t i = 0; persistent_state_vector.size() > i; ++i) {
                        return_value.push_back(ui_functions.value().parse_function(persistent_state_vector[i],
                                                                                   display_ui, "Element " + std::to_string(i)));
                    }

                    if (display_ui && label_hint.has_value()) {
                        ImGui::TreePop();
                    }

                    return return_value;
                },
                .constructor_function = [] {
                    return new std::vector<UiFunctionsType>();
                }
            };
        }

        register_type_unsafe(typeid(std::vector<Type>), [parse_function](const nlohmann::json::value_type &json) {
            auto return_type = new std::vector<Type>{};

            for(const nlohmann::json::value_type &json_element : json) {
                Type type_parsed = parse_function(json_element);

                return_type->push_back(std::move(type_parsed));
            }

            return return_type;
        }, [](void* type_ptr) {delete reinterpret_cast<std::vector<Type>*>(type_ptr); }, vector_type_ui_functions);
    }
}

// template specialization getting around the problem of std::vector<bool> by not registering it:
// most people won't want to use that type anyways
template <>
inline void aristaeus::core::ClassDB::register_type_base<bool>(std::function<bool(const nlohmann::json::value_type&)> parse_function,
                                                               std::optional<UiFunctions<bool>> ui_functions, bool register_supplemental_types) {
    register_type_base_no_vector<bool, bool>(parse_function, ui_functions, register_supplemental_types);
}

template<typename Type, typename UiFunctionsType>
void aristaeus::core::ClassDB::register_type(std::function<Type (const nlohmann::json::value_type&)> parse_function,
                                                                 std::optional<UiFunctions<UiFunctionsType>> ui_functions,
                                                                 bool register_supplemental_types) {
    register_type_base<Type, UiFunctionsType>(parse_function, ui_functions, register_supplemental_types);
}

template<utils::Hashable Type, utils::Hashable UiFunctionsType>
void aristaeus::core::ClassDB::register_type(std::function<Type (const nlohmann::json::value_type&)> parse_function,
                                             std::optional<UiFunctions<UiFunctionsType>> ui_functions, bool register_supplemental_types) {
    register_type_base<Type, UiFunctionsType>(parse_function, ui_functions, register_supplemental_types);

    if(register_supplemental_types) {
        std::optional<InternalUiFunctions> unordered_set_type_ui_functions{};

        if (ui_functions.has_value()) {
            unordered_set_type_ui_functions = InternalUiFunctions{
                .persistent_state_type = typeid(UnorderedSetUiHelper<UiFunctionsType>),
                .parse_function = [ui_functions](void *persistent_state, bool display_ui, const std::optional<std::string> &label_hint) {
                    // TODO(Bobby): make this soln not require an O(n) removal and insertion process between std::vector and std::unordered_set
                    UnorderedSetUiHelper<UiFunctionsType> &persistent_state_unordered_set = *reinterpret_cast<UnorderedSetUiHelper<UiFunctionsType>*>(persistent_state);

                    nlohmann::json return_value{};

                    std::vector<std::reference_wrapper<Type>> working_vector;

                    if (display_ui && ImGui::TreeNode("Element to add")) {
                        ui_functions.value().parse_function(persistent_state_unordered_set.item_being_added, display_ui, {});

                        if (ImGui::Button("Add Element")) {
                            persistent_state_unordered_set.unordered_set.insert(std::move(persistent_state_unordered_set.item_being_added));

                            persistent_state_unordered_set.item_being_added = ui_functions.value().constructor_function();
                        }

                        if(display_ui) {
                            ImGui::TreePop();
                        }
                    }

                    for (const UiFunctionsType &element : persistent_state_unordered_set.unordered_set) {
                        auto type_instance = persistent_state_unordered_set.unordered_set.extract(element);

                        if (!display_ui || ImGui::TreeNode("Element")) {
                            if (!display_ui || !ImGui::Button("Delete Element")) {
                                return_value.push_back(ui_functions.value().parse_function(type_instance.value(), display_ui, {}));
                                persistent_state_unordered_set.unordered_set.insert(std::move(type_instance.value()));
                            }

                            if(display_ui) {
                                ImGui::TreePop();
                            }
                        }
                    }

                    return return_value;
                },
                .constructor_function = [ui_functions] {
                    return new UnorderedSetUiHelper<UiFunctionsType>{
                        .unordered_set = {},
                        .item_being_added = ui_functions.value().constructor_function()
                    };
                }
            };
        }

        register_type_unsafe(typeid(std::unordered_set<Type>), [parse_function](const nlohmann::json::value_type &json) {
            auto return_type = new std::unordered_set<Type>{};

            for(const nlohmann::json::value_type &json_element : json) {
                Type type_parsed = parse_function(json_element);

                return_type->insert(std::move(type_parsed));
            }

            return return_type;
        }, [](void* type_ptr) {delete reinterpret_cast<std::unordered_set<Type>*>(type_ptr); }, unordered_set_type_ui_functions);
    }
}

template<typename Key, typename Value, typename Hasher>
void aristaeus::core::ClassDB::register_unordered_map_type() {
    std::optional<InternalUiFunctions> unordered_map_type_ui_functions{};

    if(!m_type_infos.contains(typeid(Key)) || !m_type_infos.contains(typeid(Value))) {
        throw std::runtime_error("Trying to register an unordered_map with an unregistered Key and/or Value type!");
    }

    const TypeInfo &key_type_info = m_type_infos.at(typeid(Key));
    const TypeInfo &value_type_info = m_type_infos.at(typeid(Value));

    if (key_type_info.ui_functions.has_value() && value_type_info.ui_functions.has_value()) {
        UiFunctions<Key> key_ui_functions = key_type_info.ui_functions.value().get_ui_functions<Key>().value();
        UiFunctions<Value> value_ui_functions = value_type_info.ui_functions.value().get_ui_functions<Value>().value();

        unordered_map_type_ui_functions = InternalUiFunctions{
            .persistent_state_type = typeid(UnorderedMapUiHelper<Key, Value, Hasher>),
            .parse_function = [key_ui_functions, value_ui_functions](void *persistent_state, bool display_ui, const std::optional<std::string> &label_hint) {
                // TODO(Bobby): make this soln not require an O(n) removal and insertion process between std::vector
                // TODO(Bobby): and std::unordered_map
                auto &persistent_state_unordered_map =
                    *reinterpret_cast<UnorderedMapUiHelper<Key, Value, Hasher>*>(persistent_state);

                nlohmann::json return_value{};

                if (display_ui && label_hint.has_value() && !ImGui::TreeNode(label_hint.value().c_str())) {
                    display_ui = false;
                }

                if (display_ui && ImGui::TreeNode("Element to add")) {
                    key_ui_functions.parse_function(persistent_state_unordered_map.item_being_added.first,
                                                    display_ui, "Key");

                    value_ui_functions.parse_function(persistent_state_unordered_map.item_being_added.second,
                                                        display_ui, "Value");

                    if (ImGui::Button("Add Element")) {
                        persistent_state_unordered_map.unordered_map.insert(
                            std::move(persistent_state_unordered_map.item_being_added));

                        persistent_state_unordered_map.item_being_added =
                                std::pair<Key, Value>(key_ui_functions.constructor_function(),
                                                      value_ui_functions.constructor_function());
                    }

                    ImGui::TreePop();
                }

                for (const std::pair<const Key, Value> &element : persistent_state_unordered_map.unordered_map) {
                    auto type_instance = persistent_state_unordered_map.unordered_map.extract(element.first);

                    if (!display_ui || ImGui::TreeNode("Element")) {
                        if (!display_ui || !ImGui::Button("Delete Element")) {
                            std::string parsed_key_json = key_ui_functions.parse_function(type_instance.key(), display_ui, "Key")[0].template get<std::string>();

                            return_value[parsed_key_json] = value_ui_functions.parse_function(type_instance.mapped(), display_ui, "Value");
                            persistent_state_unordered_map.unordered_map.insert(
                                std::pair<Key, Value>{std::move(type_instance.key()), std::move(type_instance.mapped())});
                        }

                        if(display_ui) {
                            ImGui::TreePop();
                        }
                    }
                }

                if (display_ui && label_hint.has_value()) {
                    ImGui::TreePop();
                }

                return return_value;
            },
            .constructor_function = [key_ui_functions, value_ui_functions] {
                std::pair<Key, Value> item_to_add{key_ui_functions.constructor_function(), value_ui_functions.constructor_function()};
                UnorderedMapUiHelper<Key, Value, Hasher> unordered_map_ui_helper{
                    .unordered_map = std::unordered_map<Key, Value, Hasher>{},
                    .item_being_added = item_to_add
                };

                return static_cast<void*>(new UnorderedMapUiHelper(unordered_map_ui_helper));
            }
        };
    }

    const ParseFunction &key_parse_func = m_type_infos.at(typeid(Key)).parse_function;
    const ParseFunction &value_parse_func = m_type_infos.at(typeid(Value)).parse_function;

    register_type_unsafe(typeid(std::unordered_map<Key, Value, Hasher>),
                         [key_parse_func, value_parse_func](const nlohmann::json &json) {
        auto return_type = new std::unordered_map<Key, Value, Hasher>{};

        for(const auto &json_element : json.items()) {
            Key *key_parsed = reinterpret_cast<Key*>(key_parse_func(json_element.key()));
            Value *value_parsed = reinterpret_cast<Value*>(value_parse_func(json_element.value()));

            return_type->insert(std::move(std::pair<Key, Value>{std::move(*key_parsed), std::move(*value_parsed)}));

            delete key_parsed;
            delete value_parsed;
        }

        return return_type;
    }, [](void* type_ptr) {delete reinterpret_cast<std::unordered_map<Key, Value, Hasher>*>(type_ptr);},
    unordered_map_type_ui_functions);
}

template<typename Type, typename UiFunctionsType>
        std::optional<aristaeus::core::ClassDB::UiFunctions<UiFunctionsType>> aristaeus::core::ClassDB::get_already_registered_ui_functions() {
    StdTypeInfoRef type_info = typeid(Type);

    if(m_type_infos.contains(type_info) && m_type_infos.at(type_info).ui_functions.has_value() &&
       m_type_infos.at(type_info).ui_functions->persistent_state_type == typeid(UiFunctionsType)) {
        InternalUiFunctions internal_ui_functions = *m_type_infos.at(type_info).ui_functions;

        return UiFunctions<UiFunctionsType> {
            .parse_function = [internal_ui_functions](UiFunctionsType &type, bool display_gui, const std::optional<std::string> &label_name) {
                return internal_ui_functions.parse_function(static_cast<void*>(&type), display_gui, label_name);
            },
            .constructor_function = [internal_ui_functions] {
                UiFunctionsType &ui_functions_type = *reinterpret_cast<UiFunctionsType*>(internal_ui_functions.constructor_function());

                return ui_functions_type;
            }
        };
    } else {
        return {};
    }
}

template<aristaeus::core::AristaeusNode Node>
void aristaeus::core::ClassDB::register_event_callbacks_for_class(Node &node_instance) {
    const ClassInfo &class_info = m_class_infos.at(m_class_type_names.at(node_instance.get_class_type()));

    for(const std::pair<const std::string, StdTypeInfoRef> &event_info : class_info.events) {
        node_instance.add_event(event_info.first, event_info.second);
    }

    std::vector<EventCallbackInfo> event_callback_infos;

    if (m_event_callbacks.contains(class_info.class_type)) {
        std::vector<EventCallbackInfo> my_event_callback_infos = m_event_callbacks.at(class_info.class_type);

        event_callback_infos.insert(event_callback_infos.end(), my_event_callback_infos.begin(),
                                    my_event_callback_infos.end());
    }

    for (StdTypeInfoRef parent_class_type : class_info.parent_class_types) {
        if (m_event_callbacks.contains(parent_class_type)) {
            std::vector<EventCallbackInfo> parent_event_callback_infos = m_event_callbacks.at(parent_class_type);

            event_callback_infos.insert(event_callback_infos.end(), parent_event_callback_infos.begin(), parent_event_callback_infos.end());
        }
    }

    for (const EventCallbackInfo &event_callback_info : event_callback_infos) {
        bool result = node_instance.add_callback_for_event_unsafe(event_callback_info.event_name,
                                                                  event_callback_info.callback_name,
                                                                  get_event_callback_wrapper(event_callback_info),
                                                                  event_callback_info.argument_type_info,
                                                                  node_instance);

        if (!result) {
            throw std::runtime_error("Couldn't add construction callback to event " + event_callback_info.event_name +
                "with argument type info name " + event_callback_info.argument_type_info.get().name());
        }
    }
}

template<aristaeus::core::AristaeusNode Node, typename ArgumentType>
void aristaeus::core::ClassDB::register_event(const std::string &event_name) {
    ClassInfo &class_info = m_class_infos.at(typeid(Node));

    class_info.events.insert({ event_name, typeid(ArgumentType) });
}