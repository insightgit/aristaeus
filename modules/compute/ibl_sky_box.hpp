//
// Created by bobby on 1/3/22.
//

#ifndef ARISTAEUS_IBL_SKY_BOX_HPP
#define ARISTAEUS_IBL_SKY_BOX_HPP

#include <string>
#include <vector>

#include <stb_image.h>
#include <glm/glm.hpp>

#include <gfx/vulkan/sky_box.hpp>

#include "brdf_convolution.hpp"
#include "diffuse_convolution.hpp"
#include "equirectangular_converter.hpp"

namespace aristaeus::gfx::vulkan::compute {
    class IblSkyBox : public SkyBox {
    ARISTAEUS_CLASS_HEADER("ibl_sky_box", IblSkyBox, true)
    public:
        [[nodiscard]] std::vector<vk::DescriptorImageInfo> get_descriptor_image_infos() const override;

        // we want to submit new requests for compute tasks in predraw_load so they get executed within this frame
        // rather than waiting a couple frames
        void predraw_load(const vk::CommandBuffer &command_buffer, const glm::vec3 &camera_position)override;
    private:
        std::unique_ptr<Texture> m_brdf_convolution_texture;
        std::unique_ptr<CubeMapTexture> m_diffuse_convolution_texture;
        std::unique_ptr<CubeMapTexture> m_specular_convolution_texture;

        std::optional<std::string> m_compute_pipeline_cache_path;

        std::shared_ptr<BRDFConvolution> m_brdf_convolution_job;
        std::shared_ptr<EquirectangularConverter> m_equirectangular_converter_job;
        std::shared_ptr<DiffuseConvolution> m_diffuse_convolution_job;
        std::shared_ptr<DiffuseConvolution> m_specular_reflection_convolution_job;

        glm::ivec2 m_pbr_samples;
        glm::ivec2 m_brdf_image_size;
        int32_t m_brdf_samples;
    };
}

#endif //ARISTAEUS_IBL_SKY_BOX_HPP
