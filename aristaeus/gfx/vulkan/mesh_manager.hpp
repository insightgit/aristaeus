#ifndef ARISTAEUS_MESH_MANAGER_HPP
#define ARISTAEUS_MESH_MANAGER_HPP

#include "graphics_pipeline.hpp"
#include "material_manager.hpp"

namespace aristaeus::gfx::vulkan {
	typedef uint32_t MeshID;
	typedef uint32_t InstanceID;

	class AssimpMesh;

	class MeshManager : public core::Singleton<MeshManager>, public core::EventObject {
	public:
		static constexpr vk::DescriptorSetLayoutBinding INSTANCE_STORAGE_BUFFER_BINDING{ 0, vk::DescriptorType::eStorageBuffer, 1, 
																						 vk::ShaderStageFlagBits::eFragment | vk::ShaderStageFlagBits::eVertex };
		
		struct LoadRequest {
			std::vector<glm::vec3> vertices;
			std::vector<uint32_t> indices;
			std::weak_ptr<AssimpMesh> assimp_mesh_in_tree;
			std::string mesh_generation_info;
			bool remain_unmerged; // this flag can be used for geometry which changes a lot,
								  // has its own merging behavior, or frequently pops in and out
								  // of the GPU.
		};

		struct LoadResponse {
			MeshID mesh_id;
			InstanceID instance_id;
		};

		MeshManager(bool use_gpu_driven_rendering) : m_use_gpu_driven_rendering(use_gpu_driven_rendering) {}

		std::optional<MeshID> get_mesh_id(const std::string &mesh_generation_info);

		LoadResponse load_mesh(const LoadRequest &load_request);
		InstanceID create_instance(MeshID mesh_id, std::weak_ptr<AssimpMesh> assimp_mesh_in_tree);

		const std::vector<glm::vec3> &get_mesh_vertices(MeshID mesh_id) const {
			return m_mesh_data.at(mesh_id).vertices;
		}

		const std::vector<uint32_t> &get_mesh_indices(MeshID mesh_id) const {
			return m_mesh_data.at(mesh_id).indices;
		}

		[[nodiscard]] size_t get_triangle_count(uint32_t current_frame) const {
			return m_triangle_count[current_frame];
		}

		[[nodiscard]] size_t get_draw_calls_count(uint32_t current_frame) const {
			return m_draw_calls[current_frame];
		}

		void setup_null_texture() {
			m_material_manager.setup_null_texture();
		}

		void prepare_for_render();

		// useful for switching order, say if tree order changes
		/*void swap_instance_id(InstanceID instance_id_one, InstanceID instance_id_two);

		InstanceID update_mesh(InstanceID existing_instance_id, const LoadRequest &load_request);*/

		void add_instance_to_draw_list(RenderContextID render_context_id, InstanceID instance_uuid, MaterialID material_uuid);
		void remove_instance_from_draw_list(RenderContextID render_context_id, InstanceID instance_uuid, MaterialID material_uuid);

		void draw(vk::CommandBuffer &primary_command_buffer, RenderContextID render_context_id, uint32_t subpass_index);
	private:
		struct MeshUBO {
			alignas(16) glm::mat4 model;
			alignas(16) glm::mat3 normal_transform;
			alignas(16) MaterialID local_material_id;
		};

		struct MeshData {
			std::vector<uint32_t> indices;
			std::vector<glm::vec3> vertices;

			vk::DeviceSize vertex_num_offset; // the vertex number offset for this mesh
			vk::DeviceSize vertex_buffer_offset; // the vertex byte offset for this mesh
			vk::DeviceSize index_buffer_offset;

			BufferRaii unmerged_buffer;
		};

		struct InstanceData {
			std::string transform_event_handle;
			std::string parent_transform_event_handle;
			std::string visibilty_event_handle;
			std::unordered_map<RenderContextID, vk::DeviceSize> instance_buffer_offset;
			std::unordered_map<RenderContextID, MaterialID> associated_material_ids;

			std::weak_ptr<AssimpMesh> assimp_mesh;
		};

		struct RenderObject {
			InstanceID instance;
			MeshID mesh;
			MaterialID material;

			bool operator==(const RenderObject &other) const noexcept {
				return instance == other.instance && mesh == other.mesh && material == other.material;
			}
		};

		struct RenderObjectHasher {
			size_t operator()(const RenderObject &render_object) const noexcept {
				return static_cast<size_t>(render_object.instance) << 40 | ((static_cast<size_t>(render_object.mesh) << 20) & utils::get_bit_mask(20, 20)) | 
					   (render_object.material & utils::get_bit_mask(0, 20));
			}
		};

		void refresh_dirty_instances(RenderContextID render_context_id, bool force_refresh = false);

		void create_mesh_vertex_index_buffer(std::unordered_map<RenderContextID, vk::DeviceSize> &instance_buffer_sizes);

		// TODO(Bobby): look into packing these into one texture
		std::unordered_map<std::string, Texture> m_texture_cache;

		MaterialManager m_material_manager;
		bool m_use_gpu_driven_rendering;

		MeshID m_current_mesh_id = 0;
		InstanceID m_current_instance_id = 0;

		std::unordered_set<RenderContextID> m_render_context_ids_registered;

		// the lists inside m_render_objects should always be ordered based on their material ID and mesh ID
		std::unordered_map<RenderContextID, std::unordered_set<RenderObject, RenderObjectHasher>> m_new_objects_to_render;
		std::unordered_map<RenderContextID, std::unordered_set<RenderObject, RenderObjectHasher>> m_old_objects_to_remove;
		std::unordered_map<RenderContextID, std::vector<RenderObject>> m_render_objects;
		std::unordered_set<RenderObject, RenderObjectHasher> m_render_object_set;

		// TODO(Bobby): adapt for dynamic geometry resizing while frames are in flight
		BufferRaii m_mesh_vertex_index_buffer;
		vk::DeviceSize m_vertex_buffer_size;
		std::unordered_map<RenderContextID, BufferRaii> m_mesh_instance_buffer;

		std::unordered_map<std::string, MeshID> m_mesh_id_generation_infos;
		std::unordered_map<uint64_t, MeshID> m_mesh_id_hashes;

		std::unordered_map<MeshID, MeshData> m_mesh_data;
		std::unordered_map<InstanceID, InstanceData> m_instance_data;

		std::unordered_map<InstanceID, MeshID> m_instances_to_mesh_ids;

		std::unordered_set<InstanceID> m_dirty_instances;

		std::unordered_set<MaterialID> m_inited_materials;
		std::unordered_set<MaterialID> m_queued_materials;

		std::array<std::vector<BufferRaii>, utils::vulkan::MAX_FRAMES_IN_FLIGHT> m_reaper_buffers;

		std::unique_ptr<vk::raii::DescriptorSetLayout> m_mesh_instance_ssbo_set_layout;
		std::unordered_map<RenderContextID, vk::raii::DescriptorPool> m_mesh_instance_ssbo_pools;
		std::unordered_map<RenderContextID, std::array<vk::raii::DescriptorSet, utils::vulkan::MAX_FRAMES_IN_FLIGHT>> m_mesh_instance_ssbo_set;

		uint32_t m_last_frame;

		std::array<size_t, utils::vulkan::MAX_FRAMES_IN_FLIGHT> m_triangle_count;
		std::array<size_t, utils::vulkan::MAX_FRAMES_IN_FLIGHT> m_draw_calls;
	};
}

#endif