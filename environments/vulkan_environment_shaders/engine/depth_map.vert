#version 450
layout (location = 0) in vec3 position;

layout (set = 0, binding = 0) uniform DepthMapUBO {
    mat4 projection_matrix;
    mat4 view_matrix;
    mat4 light_space_matrix;
    vec3 camera_front;
    vec3 camera_up;
    vec3 camera_right;
    vec3 camera_position;
    float camera_far_plane;
    int linear_distance_scaling;
    int flip_projection_matrix;
} depth_map_ubo;

struct Instance {
    mat4 model;
    mat3 normal_transform;
    uint material_index;
};

layout (std430, set = 3, binding = 0) readonly buffer InstanceUBO {
    Instance instances[];
} instance_ubo;

layout (location = 0) out vec3 world_position;

void main() {
    world_position = (instance_ubo.instances[gl_InstanceIndex].model * vec4(position, 1.0)).xyz;

    mat4 light_space_matrix = depth_map_ubo.projection_matrix * depth_map_ubo.view_matrix;

    gl_Position = depth_map_ubo.projection_matrix * depth_map_ubo.view_matrix * vec4(world_position, 1.0);
}
