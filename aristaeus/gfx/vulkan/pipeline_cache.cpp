#include "pipeline_cache.hpp"

#include <boost/algorithm/hex.hpp>
#include <boost/uuid/detail/md5.hpp>

#include "core_renderer.hpp"

aristaeus::gfx::vulkan::PipelineCache::PipelineCache(vk::raii::Device &logical_device, vk::PhysicalDeviceProperties device_properties,
                                                     const std::string &pipeline_cache_path) : m_device_properties(device_properties),
                                                                                               m_pipeline_cache_path(pipeline_cache_path),
                                                                                               m_pipeline_cache(create_pipeline_cache(logical_device)) {}


vk::raii::PipelineCache aristaeus::gfx::vulkan::PipelineCache::create_pipeline_cache(vk::raii::Device& logical_device) {
    std::ifstream file{ m_pipeline_cache_path, std::ios::ate | std::ios::binary };

    vk::PipelineCacheCreateInfo empty_pipeline_cache_create_info{ {}, 0, nullptr };
    PipelineCachePrefixHeader prefix_header{};
    size_t pipeline_cache_data_size;

    file.seekg(0, std::ios::end);

    pipeline_cache_data_size = file.tellg();

    if (pipeline_cache_data_size < sizeof(PipelineCachePrefixHeader)) {
        return { logical_device, {} };
    }

    pipeline_cache_data_size -= sizeof(PipelineCachePrefixHeader);

    file.seekg(0, std::ios::beg);

    file.read(reinterpret_cast<char*>(&prefix_header), sizeof(PipelineCachePrefixHeader));

    if (prefix_header.magic != PIPELINE_CACHE_PREFIX_MAGIC || prefix_header.data_size != pipeline_cache_data_size ||
        prefix_header.driver_abi != sizeof(void*) || prefix_header.vendor_id != m_device_properties.vendorID ||
        prefix_header.device_id != m_device_properties.deviceID ||
        prefix_header.driver_version != m_device_properties.driverVersion ||
        !check_pipeline_cache_uuid_match(prefix_header, m_device_properties)) {
        vk::raii::PipelineCache pipeline_cache{ logical_device, empty_pipeline_cache_create_info };

        return pipeline_cache;
    }

    std::vector<uint8_t> pipeline_cache_data(pipeline_cache_data_size);

    file.read(reinterpret_cast<char*>(pipeline_cache_data.data()), pipeline_cache_data_size);

    boost::uuids::detail::md5 hash;
    boost::uuids::detail::md5::digest_type hash_digest;

    hash.process_bytes(pipeline_cache_data.data(), pipeline_cache_data.size());
    hash.get_digest(hash_digest);

    for (int i = 0; 4 > i; ++i) {
        if (prefix_header.data_hash[i] != hash_digest[i]) {
            vk::raii::PipelineCache pipeline_cache{ logical_device, empty_pipeline_cache_create_info };

            return pipeline_cache;
        }
    }

    vk::PipelineCacheCreateInfo create_info{ {}, pipeline_cache_data.size(), pipeline_cache_data.data() };
    vk::raii::PipelineCache pipeline_cache{ logical_device, create_info };

    return pipeline_cache;
}

void
aristaeus::gfx::vulkan::PipelineCache::write_to_pipeline_cache(CoreRenderer &core_renderer) {
    std::vector<uint8_t> pipeline_cache_data;

    pipeline_cache_data = m_pipeline_cache.getData();

    boost::uuids::detail::md5 hash;
    boost::uuids::detail::md5::digest_type hash_digest;

    hash.process_bytes(pipeline_cache_data.data(), pipeline_cache_data.size());
    hash.get_digest(hash_digest);

    PipelineCachePrefixHeader prefix_header = {
        .magic = PIPELINE_CACHE_PREFIX_MAGIC,
        .data_size = pipeline_cache_data.size(),
        .data_hash = {hash_digest[0], hash_digest[1], hash_digest[2], hash_digest[3]},
        .vendor_id = m_device_properties.vendorID,
        .device_id = m_device_properties.deviceID,
        .driver_version = m_device_properties.driverVersion,
        .driver_abi = sizeof(void*),
        .uuid = m_device_properties.pipelineCacheUUID
    };

    std::ofstream file{ m_pipeline_cache_path, std::ios::ate | std::ios::binary };

    file.write(reinterpret_cast<const char*>(&prefix_header), sizeof(prefix_header));
    file.write(reinterpret_cast<const char*>(pipeline_cache_data.data()), pipeline_cache_data.size());

    file.close();
}
