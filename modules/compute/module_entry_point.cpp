//
// Created by bobby on 03/05/24.
//
#include "module_entry_point.hpp"

aristaeus::gfx::vulkan::compute::ComputeManager *_compute_manager_singleton = nullptr;

extern "C" void compute_module_entry_point(const nlohmann::json &module_json) {
    using aristaeus::gfx::vulkan::CoreRenderer;
    using aristaeus::gfx::vulkan::compute::ComputeManager;

    auto &class_db = utils::get_singleton_safe<aristaeus::core::ClassDB>();
    auto &core_renderer = utils::get_singleton_safe<CoreRenderer>();

    class_db.register_class<aristaeus::gfx::vulkan::compute::IblSkyBox>();

    assert(_compute_manager_singleton == nullptr);

    // initialize compute manager singleton
    _compute_manager_singleton = new ComputeManager(core_renderer.get_queue_indices_info().compute_family,
                                                    core_renderer.get_queue_indices_info().compute_queue_index,
                                                    module_json.contains("compute_info_path") ?
                                                    module_json.at("compute_info_path").get<std::string>() :
                                                    std::optional<std::string>{});
}

extern "C" void compute_module_exit_point(const nlohmann::json &module_json) {
    if(_compute_manager_singleton != nullptr) {
        delete _compute_manager_singleton;
        _compute_manager_singleton = nullptr;
    }
}