#ifndef ARISTAEUS_PHYSICS_ACTOR_NODE_HPP
#define ARISTAEUS_PHYSICS_ACTOR_NODE_HPP

#include <gfx/scene_graph_node.hpp>

#include <gfx/vulkan/assimp_model.hpp>

#include "physics_manager.hpp"

namespace aristaeus::physics {
	class PhysicsActorNode : public gfx::SceneGraphNode {
    ARISTAEUS_CLASS_HEADER("physics_actor", PhysicsActorNode, false);
	public:
		bool has_shape() {
			return m_collision_shapes.empty();
		}

		bool is_auto_adding_parent_mesh_shape() const {
			return m_auto_add_parent_mesh_shape;
		}

		void set_auto_adding_parent_mesh_shape(bool auto_add_parent_mesh_shape) {
			m_auto_add_parent_mesh_shape = auto_add_parent_mesh_shape;
		}

		std::weak_ptr<PhysxShape> add_shape(const GeometryInfo &geometry_info);

		bool remove_shape(const std::weak_ptr<PhysxShape> &shape);
	protected:
		static constexpr std::string_view ACTOR_UPDATED_EVENT = "actor_updated";

		physx::PxTransform scene_transform_to_physx_transform(const gfx::SceneGraphNode::Transform &transform);
		gfx::SceneGraphNode::Transform physx_transform_to_scene_transform(const physx::PxTransform &transform);

		physx::PxRigidActor *m_actor = nullptr;
	private:
		friend class PhysicsManager;
		
		void on_transform_changed() {
            if(m_actor != nullptr) {
                m_actor->setGlobalPose(scene_transform_to_physx_transform(get_parent_transform() + get_transform()));
            }
		}

		void refresh_attached_shapes_to_actor();
		void refresh_collision_shapes();

		void on_parsing_done();

		void on_physics_step_finished();

		void on_enter_tree();
		void on_exit_tree();

		bool m_in_tree = false;

		std::unordered_set<GeometryInfo> m_geometry_infos;
		std::unordered_set<std::shared_ptr<PhysxShape>> m_collision_shapes; // GeometryInfo is a persistent set of geometries, with shapes
																			// for those geometries only being generated upon being added to
																			// the scene tree (because some geometryinfos are tied directly to 
																			// the scene tree, and these shapes are only useful when they are
																			// attached to an actor which only happens when it's in the scene
																			// tree to begin with.

		std::vector<std::string> m_nodes_to_load_in;

		bool m_auto_add_parent_mesh_shape = true;
	};
}

#endif