#ifndef ARISTAEUS_RECTANGULAR_PRISM_HPP
#define ARISTAEUS_RECTANGULAR_PRISM_HPP

#include "geometry.hpp"

namespace aristaeus::gfx::vulkan {
	class RectangularPrism : public Geometry {
	public:
		RectangularPrism(CoreRenderer &core_renderer, const glm::vec3 &extents, const ModelInfo &model_info) : Geometry(create_geometry(core_renderer, extents, model_info)) {}
	private:
        static constexpr std::array<glm::vec3, 36> BASE_VERTICES = {{
            {-1.0f,  1.0f, -1.0f}, // -z
            {1.0f, -1.0f, -1.0f},
            {-1.0f, -1.0f, -1.0f},
 
            {1.0f, -1.0f, -1.0f},
            {-1.0f,  1.0f, -1.0f},
            {1.0f,  1.0f, -1.0f},
           

            {-1.0f, -1.0f,  1.0f}, // -x
            {-1.0f,  1.0f, -1.0f},
            {-1.0f, -1.0f, -1.0f},

            {-1.0f,  1.0f, -1.0f},
            {-1.0f, -1.0f,  1.0f},
            {-1.0f,  1.0f,  1.0f},

            {1.0f, -1.0f, -1.0f}, // +x
            {1.0f,  1.0f,  1.0f},
            {1.0f, -1.0f,  1.0f},

            {1.0f,  1.0f,  1.0f},
            {1.0f, -1.0f, -1.0f},
            {1.0f,  1.0f, -1.0f},

            {-1.0f, -1.0f,  1.0f}, // +z
            {1.0f,  1.0f,  1.0f},
            {-1.0f,  1.0f,  1.0f},

            {1.0f,  1.0f,  1.0f},
            {-1.0f, -1.0f,  1.0f},
            {1.0f, -1.0f,  1.0f},

            {-1.0f,  1.0f, -1.0f}, // +y
            {1.0f,  1.0f,  1.0f},
            {1.0f,  1.0f, -1.0f},

            {1.0f,  1.0f,  1.0f},
            {-1.0f,  1.0f, -1.0f},
            {-1.0f,  1.0f,  1.0f},

            {-1.0f, -1.0f, -1.0f}, // -y
            {1.0f, -1.0f, -1.0f},
            {-1.0f, -1.0f,  1.0f},

            {1.0f, -1.0f, -1.0f},
            {1.0f, -1.0f,  1.0f},
            {-1.0f, -1.0f,  1.0f},
        }};
        static constexpr std::array<glm::vec3, 6> BASE_NORMALS {{ {0, 0, -1}, { 1, 0, 0 }, { -1, 0, 0 }, { 0, 0, 1 }, { 0, -1, 0 }, { 0, 1, 0 } }};

		Geometry create_geometry(CoreRenderer &core_renderer, const glm::vec3 &extents, const ModelInfo &model_info);
	};
}

#endif