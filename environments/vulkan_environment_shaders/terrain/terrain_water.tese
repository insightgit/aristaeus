#version 460

layout (quads, fractional_odd_spacing, ccw) in;

layout (location = 0) in vec2 height_map_texture_coords[];

const int MAX_NUM_OF_WAVES = 100;

const float GRAVITY_CONSTANT = 9.8;
const float PI = 3.141592653589793238;

layout (set = 0, binding = 0) uniform PerspectiveCameraUBO {
    mat4 projection;
    mat4 view;
    vec3 camera_front;
    vec3 camera_up;
    vec3 camera_right;
    vec3 camera_position;
    float camera_far_plane;
} camera_ubo;

struct TessellationSettings {
    int mode;
    int amount;

    int camera_max;
    int camera_min;
};

struct Wave {
    int wave_active;
    float amplitude;
    float birth_period;
    vec2 center_wave;
    float death_period;
    float sharpness;
    float wavelength;
    float t_time;
    vec2 padding;
};

layout (location = 0) out VertexOut {
    vec4 diffuse_color_vector;
    vec3 normal_vector;
    mat3 tbn_matrix;
    vec2 texture_coords_out;
    vec3 world_position;
    float vertex_height;

    vec2 height_map_texture_coords;
} tessellation_out;

layout (set = 2, binding = 0) uniform ModelUBO {
    float terrain_height_magnitude;

    int override_texture;
    int multi_texture_terrain_mode;
    int light_emitter;
    int object_type;

    vec3 light_box_color;

    Wave active_waves[MAX_NUM_OF_WAVES];
    Wave pending_waves[MAX_NUM_OF_WAVES];
    vec3 wave_world_position;
    int active_wave_count;

    TessellationSettings tessellation_settings;
    float vertex_resolution;
    float terrain_num_of_textures;
    int is_chunked_terrain;
    float chunked_blend_threshold;
    ivec2 chunk_position;
} model_ubo;

layout (set = 3, binding = 0) uniform MeshUBO {
    mat4 model;
    mat3 normal_transform;
} mesh_ubo;

// gets a specific wave
vec3 get_gertsner_wave_change(Wave wave, vec2 original_position) {
    // TODO: add sharpness variation
    // "In fact, we can leave the specification of Q as a "steepness" parameter for the
    // production artist, allowing a range of 0 to 1, and using Qi = Q/(wi Ai x numWaves) to
    // vary from totally smooth waves to the sharpest waves we can produce." GPU Gems chap 1
    //float sharpness =

    // TODO: allow for circular direction waves
    vec2 direction = normalize(wave.center_wave);
    vec2 scaled_position = original_position * model_ubo.vertex_resolution;

    float speed = sqrt(GRAVITY_CONSTANT * (2 * PI) / wave.wavelength);
    float phase_constant = speed * (2 / wave.wavelength);

    float wave_periodic_term = dot(speed * direction, original_position) +
    (phase_constant * wave.t_time);

    float cosine_wave_term = cos(wave_periodic_term);
    float wave_term_x = (wave.sharpness * wave.amplitude) + direction.x * cosine_wave_term;
    float wave_term_y = (wave.sharpness * wave.amplitude) + direction.y * cosine_wave_term;

    float z_value_sin = sin(wave_periodic_term);
    float wave_term_z = wave.amplitude * z_value_sin;

    // TODO(Bobby): investigate wave term y giving strange flat results
    return vec3(wave_term_x, wave_term_z, wave_term_y);
}

vec2 calc_sin_cos_gertsner_wave_term(Wave wave, vec3 wave_position) {
    vec2 direction = normalize(wave.center_wave);
    vec3 direction_vec3 = vec3(direction.x, 0, direction.y);
    float frequency = 2 / wave.wavelength;

    float speed = sqrt(GRAVITY_CONSTANT * (2 * PI) / wave.wavelength);
    float phase_constant = speed * (2 / wave.wavelength);

    float sin_term = sin(dot(frequency * direction_vec3, wave_position + phase_constant * wave.t_time));
    float cos_term = cos(dot(frequency * direction_vec3, wave_position + phase_constant * wave.t_time));

    return vec2(sin_term, cos_term);
}

vec3 get_gertsner_wave_normal_change(Wave wave, float sin_term, float cos_term) {
    // TODO: allow for circular direction waves
    vec2 direction = normalize(wave.center_wave);
    vec3 direction_vec3 = vec3(direction.x, 0, direction.y);
    float frequency = 2 / wave.wavelength;
    float freq_amp_term = frequency * wave.amplitude;

    float speed = sqrt(GRAVITY_CONSTANT * (2 * PI) / wave.wavelength);
    float phase_constant = speed * (2 / wave.wavelength);

    float normal_term_x = -(direction.x * freq_amp_term * cos_term);
    float normal_term_y = -(direction.y * freq_amp_term * cos_term);
    float normal_term_z = 1 - (wave.sharpness * freq_amp_term * sin_term);

    // TODO(Bobby): investigate wave term y giving strange flat results
    return vec3(normal_term_x, normal_term_z, normal_term_y);
}

vec3 get_gertsner_wave_tangent_change(Wave wave, float sin_term, float cos_term) {
    vec2 direction = normalize(wave.center_wave);
    vec3 direction_vec3 = vec3(direction.x, 0, direction.y);
    float frequency = 2 / wave.wavelength;
    float freq_amp_term = frequency * wave.amplitude;

    float tangent_term_x = -wave.sharpness * direction_vec3.x * direction_vec3.y * freq_amp_term * sin_term;
    float tangent_term_y = -wave.sharpness * pow(direction_vec3.y, 2) * freq_amp_term * sin_term;
    float tangent_term_z = direction_vec3.y * freq_amp_term * cos_term;

    return vec3(tangent_term_x, tangent_term_y, tangent_term_z);
}

float get_wave_lifecycle(Wave current_wave) {
    float speed = sqrt(GRAVITY_CONSTANT * (2 * PI / current_wave.wavelength));
    float phase_constant = (speed * 2) / current_wave.wavelength;
    float wave_lifecycle = (2 * PI / phase_constant) + current_wave.birth_period;

    return wave_lifecycle;
}

// gets pending and active waves based on index
vec3 get_gertsner_wave_changes(vec2 original_position, uint wave_index) {
    Wave active_wave = model_ubo.active_waves[wave_index];
    vec3 active_wave_change = get_gertsner_wave_change(active_wave, original_position);
    float active_wave_lifecycle = get_wave_lifecycle(active_wave);

    if(active_wave.t_time > active_wave_lifecycle) {
        // assume that if death cycle has elapsed the CPU has dealed with it
        Wave pending_wave = model_ubo.pending_waves[wave_index];
        vec3 pending_wave_change = get_gertsner_wave_change(pending_wave, original_position);
        float death_period_t = active_wave.t_time - active_wave_lifecycle / active_wave.death_period;

        return mix(active_wave_change, pending_wave_change, death_period_t);
    } else {
        return active_wave_change;
    }
}

vec3 get_gertsner_wave_normal_changes(vec3 wave_position, uint wave_index, vec4 sin_cos_active_pending_wave_terms) {
    Wave active_wave = model_ubo.active_waves[wave_index];
    vec3 active_wave_change = get_gertsner_wave_normal_change(active_wave, sin_cos_active_pending_wave_terms.x,
                                                              sin_cos_active_pending_wave_terms.y);
    float active_wave_lifecycle = get_wave_lifecycle(active_wave);

    Wave pending_wave = model_ubo.pending_waves[wave_index];
    vec3 pending_wave_change = get_gertsner_wave_normal_change(pending_wave, sin_cos_active_pending_wave_terms.z,
                                                               sin_cos_active_pending_wave_terms.w);
    float death_period_t = active_wave.t_time - active_wave_lifecycle / active_wave.death_period;

    if(active_wave.t_time > active_wave_lifecycle) {
        // assume that if death cycle has elapsed the CPU has dealed with it
        return mix(active_wave_change, pending_wave_change, death_period_t);
    } else {
        return active_wave_change;
    }
}

vec3 get_gertsner_wave_tangent_changes(vec3 wave_position, uint wave_index, vec4 sin_cos_active_pending_wave_terms) {
    Wave active_wave = model_ubo.active_waves[wave_index];
    vec3 active_wave_change = get_gertsner_wave_normal_change(active_wave, sin_cos_active_pending_wave_terms.x,
                                                              sin_cos_active_pending_wave_terms.y);
    float active_wave_lifecycle = get_wave_lifecycle(active_wave);

    Wave pending_wave = model_ubo.pending_waves[wave_index];
    vec3 pending_wave_change = get_gertsner_wave_tangent_change(pending_wave, sin_cos_active_pending_wave_terms.z,
                                                                sin_cos_active_pending_wave_terms.w);
    float death_period_t = active_wave.t_time - active_wave_lifecycle / active_wave.death_period;

    if(active_wave.t_time > active_wave_lifecycle) {
        // assume that if death cycle has elapsed the CPU has dealed with it
        return mix(active_wave_change, pending_wave_change, death_period_t);
    } else {
        return active_wave_change;
    }
}

vec2 interoplate_tex_height_map_coords() {
    vec2 texture_coord_first_u = mix(height_map_texture_coords[0], height_map_texture_coords[1], gl_TessCoord.x);
    vec2 texture_coord_bottom_u = mix(height_map_texture_coords[2], height_map_texture_coords[3], gl_TessCoord.x);

    return mix(texture_coord_first_u, texture_coord_bottom_u, gl_TessCoord.y);
}

vec4 interoplate_position_coords() {
    vec4 position_first_u = mix(gl_in[0].gl_Position, gl_in[1].gl_Position, gl_TessCoord.x);
    vec4 position_bottom_u = mix(gl_in[2].gl_Position, gl_in[3].gl_Position, gl_TessCoord.x);

    vec4 position_v = mix(position_first_u, position_bottom_u, gl_TessCoord.y);

    position_v.w = 1.0;

    return position_v;
}

void main() {
    // normal calculation
    vec4 patch_u_vector = gl_in[1].gl_Position - gl_in[0].gl_Position;
    vec4 patch_v_vector = gl_in[3].gl_Position - gl_in[0].gl_Position;

    // position calculation
    tessellation_out.height_map_texture_coords = interoplate_tex_height_map_coords();
    tessellation_out.normal_vector = normalize(cross(patch_v_vector.xyz, patch_u_vector.xyz));

    vec4 position = interoplate_position_coords();

    vec2 gertsner_wave_coords =
        model_ubo.chunk_position * tessellation_out.height_map_texture_coords * vec2(2);/** vec2(model_ubo.terrain_num_of_textures)*/;
    vec3 tangent_vector = vec3(0.0f);

    tessellation_out.normal_vector = vec3(0.0f);

    for(uint i = 0; model_ubo.active_wave_count > i; ++i) {
        vec3 gertsner_wave_coords_output = get_gertsner_wave_changes(gertsner_wave_coords, i);

        vec4 gertsner_wave_sin_cos = vec4(
            calc_sin_cos_gertsner_wave_term(model_ubo.active_waves[i], gertsner_wave_coords_output),
            calc_sin_cos_gertsner_wave_term(model_ubo.pending_waves[i], gertsner_wave_coords_output));

        position += vec4(gertsner_wave_coords_output, 1.0f);
        tangent_vector += get_gertsner_wave_tangent_changes(gertsner_wave_coords_output, i, gertsner_wave_sin_cos);
        tessellation_out.normal_vector +=
            get_gertsner_wave_normal_changes(gertsner_wave_coords_output, i, gertsner_wave_sin_cos);
    }

    vec3 bitangent_vector = cross(tangent_vector, tessellation_out.normal_vector);

    tessellation_out.diffuse_color_vector = vec4(0.0f, 0.0f, 1.0f, 1.0f);
    tessellation_out.normal_vector = normalize(tessellation_out.normal_vector);

    tessellation_out.normal_vector.y = -tessellation_out.normal_vector.y;

    tessellation_out.world_position = (mesh_ubo.model * vec4(position.xyz, 1.0f)).xyz;
    tessellation_out.vertex_height = position.y;

    gl_Position = camera_ubo.projection * camera_ubo.view * mesh_ubo.model * position;
}
