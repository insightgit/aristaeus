#include "torus.hpp"

#include "../core_renderer.hpp"

aristaeus::gfx::vulkan::Geometry aristaeus::gfx::vulkan::Torus::create_geometry(CoreRenderer &core_renderer, size_t stack_count, size_t sector_count, float major_radius, float minor_radius, const ModelInfo &model_info) {
	std::vector<ModelVertex> vertices;
	std::vector<uint32_t> indices;

	std::string mesh_generation_info = "torus-" + std::to_string(stack_count) + "-" + std::to_string(sector_count) + "-" + std::to_string(major_radius) + "-" + std::to_string(minor_radius);

	float stack_increment = (2.0f * PI) / stack_count;
	float sector_increment = (2.0f * PI) / sector_count;
	int current_index = 0;

	for (float stack = 0.0f; 2.0f * PI > stack; stack += stack_increment) {
		for (float sector = 0.0f; 2.0f * PI > sector; sector += sector_increment) {
			ModelVertex vertex;

			float radius_term = major_radius + minor_radius * cos(stack);

			vertex.position = glm::vec3{ radius_term * cos(sector), minor_radius * sin(stack), radius_term * sin(sector) };

			vertex.texture_coords = glm::vec3((sector / sector_increment) / sector_count, (stack / stack_increment) / stack_count, 0.0);

			// take the two tangent vectors (from partial derivatives of sector and stack with respect to the torus shape) and take the cross product 
			// to get the normal
			glm::vec3 sector_tangent{-sin(sector), cos(sector), 0};
			glm::vec3 stack_tangent{cos(sector) * -sin(stack), sin(sector) * -sin(stack), cos(stack)};

			vertex.normal = glm::normalize(glm::cross(sector_tangent, stack_tangent));

			vertices.push_back(vertex);

			if (stack < (PI * 2.0f) - stack_increment) {
				indices.push_back(current_index);
				indices.push_back(current_index + sector_count);
				indices.push_back(current_index + sector_count + 1);

				indices.push_back(current_index);
				indices.push_back(current_index + sector_count + 1);
				indices.push_back(current_index + 1);
			}

			++current_index;
		}
	}

	return { core_renderer, vertices, indices, mesh_generation_info, model_info };
}