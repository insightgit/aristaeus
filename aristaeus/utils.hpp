//
// Created by bobby on 12/16/21.
//

#ifndef ARISTAEUS_UTILS_HPP
#define ARISTAEUS_UTILS_HPP

#include <array>
#include <string>
#include <chrono>
#include <iostream>
#include <optional>
#include <fstream>
#include <vector>

#include "gfx/vulkan/vulkan_raii.hpp"

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/glm.hpp>
#include <glm/gtx/quaternion.hpp>
#include <json.hpp>
#include <sha256.h>
#include <stb_image.h>

// based on https://stackoverflow.com/a/35832526
struct aligned_allocator_tag {};
static aligned_allocator_tag aligned_allocator;

inline void* operator new(size_t size_bytes, aligned_allocator_tag, size_t alignment) {
    // implementation ideas from https://johanmabille.github.io/blog/2014/12/06/aligned-memory-allocator/

#if defined(_WIN64) || defined(__APPLE__)
    // malloc already gives us 16-byte aligned allocations
    if (alignment == 16) {
        return malloc(size_bytes);
    }
#endif

#ifdef _MSC_VER
    // MS visual studio
    return _aligned_malloc(size_bytes, alignment);
#elif defined(unix) || defined(__unix__) || defined(__unix)
    // for POSIX systems
    void* res;
    const int failed = posix_memalign(&res, size_bytes, alignment);
    if (failed) res = 0;
    return res;
#endif
}
inline void* operator new[](size_t size_bytes, aligned_allocator_tag, size_t alignment) {
    return operator new(size_bytes, aligned_allocator, alignment);
}

namespace utils {
    namespace vulkan {
        static constexpr uint32_t MAX_FRAMES_IN_FLIGHT = 2;
    }

    static const std::vector<std::string> ENVIRONMENT_SHADER_VERTEX_ATTRIBUTES{"position", "normal", "texture_coords",
                                                                               "diffuse_color", "specular_color"};
    static constexpr int WINDOW_HEIGHT = 1080;
    static constexpr int WINDOW_WIDTH = 1920;
    static constexpr float PI = 3.141592653589793238;
    static constexpr float GRAVITY_CONSTANT = 9.8;

    static constexpr std::array<int, 256> PERLIN_LOOKUP_TABLE = {
            151,160,137,91,90,15,
            131,13,201,95,96,53,194,233,7,225,140,36,103,30,69,142,8,99,37,240,21,10,23,
            190, 6,148,247,120,234,75,0,26,197,62,94,252,219,203,117,35,11,32,57,177,33,
            88,237,149,56,87,174,20,125,136,171,168, 68,175,74,165,71,134,139,48,27,166,
            77,146,158,231,83,111,229,122,60,211,133,230,220,105,92,41,55,46,245,40,244,
            102,143,54, 65,25,63,161, 1,216,80,73,209,76,132,187,208, 89,18,169,200,196,
            135,130,116,188,159,86,164,100,109,198,173,186, 3,64,52,217,226,250,124,123,
            5,202,38,147,118,126,255,82,85,212,207,206,59,227,47,16,58,17,182,189,28,42,
            223,183,170,213,119,248,152, 2,44,154,163, 70,221,153,101,155,167, 43,172,9,
            129,22,39,253, 19,98,108,110,79,113,224,232,178,185, 112,104,218,246,97,228,
            251,34,242,193,238,210,144,12,191,179,162,241, 81,51,145,235,249,14,239,107,
            49,192,214, 31,181,199,106,157,184, 84,204,176,115,121,50,45,127, 4,150,254,
            138,236,205,93,222,114,67,29,24,72,243,141,128,195,78,66,215,61,156,180
    };

    typedef std::array<unsigned char, SHA256::DIGEST_SIZE> SHA256Digest;

    struct Vec2Hash {
        size_t operator()(const glm::vec2 &vec) const noexcept {
            return static_cast<size_t>((vec.x * 500000) + (vec.y * 300000));
        }

        bool operator()(const glm::vec2 &vec_a, const glm::vec2 &vec_b) const noexcept {
            if(vec_a.x == vec_b.x) {
                if(vec_a.y == vec_b.y) {
                    return false;
                } else {
                    return vec_a.y < vec_b.y;
                }
            } else {
                return vec_a.x < vec_b.x;
            }
        }
    };

    struct Vec3Hash {
        size_t operator()(const glm::vec3 &vec) const noexcept {
            return static_cast<size_t>((vec.x * 500000) + (vec.y * 300000) + (vec.z * 700000));
        }

        bool operator()(const glm::vec4 &vec_a, const glm::vec4 &vec_b) const noexcept {
            if(vec_a.x == vec_b.x) {
                if(vec_a.y == vec_b.y) {
                    if(vec_a.z == vec_b.z) {
                        return false;
                    } else {
                        return vec_a.z < vec_b.z;
                    }
                } else {
                    return vec_a.y < vec_b.y;
                }
            } else {
                return vec_a.x < vec_b.x;
            }
        }
    };

    struct Vec4Hash {
        size_t operator()(const glm::vec4 &vec) const noexcept {
            return static_cast<size_t>((vec.x * 500000) + (vec.y * 300000) + (vec.z * 700000) + (vec.w * 123456));
        }

        /*bool operator()(const glm::vec4 &vec_a, const glm::vec4 &vec_b) const noexcept {
            return vec_a.x == vec_b.x && vec_a.y == vec_b.y && vec_a.z == vec_b.z && vec_a.w == vec_b.w;
        }*/

        bool operator()(const glm::vec4 &vec_a, const glm::vec4 &vec_b) const noexcept {
            if(vec_a.x == vec_b.x) {
                if(vec_a.y == vec_b.y) {
                    if(vec_a.z == vec_b.z) {
                        if(vec_a.w == vec_b.w) {
                            return false;
                        } else {
                            return vec_a.w < vec_b.w;
                        }
                    } else {
                        return vec_a.z < vec_b.z;
                    }
                } else {
                    return vec_a.y < vec_b.y;
                }
            } else {
                return vec_a.x < vec_b.x;
            }
        }
    };

    struct RenderPassHash {
        size_t operator()(const vk::RenderPass &render_pass) const noexcept {
            return reinterpret_cast<uintptr_t>(static_cast<VkRenderPass>(render_pass));
        }

        /*bool operator()(const glm::vec4 &vec_a, const glm::vec4 &vec_b) const noexcept {
            return vec_a.x == vec_b.x && vec_a.y == vec_b.y && vec_a.z == vec_b.z && vec_a.w == vec_b.w;
        }*/

        bool operator()(const vk::RenderPass &render_pass_a, const vk::RenderPass &render_pass_b) const noexcept {
            return render_pass_a == render_pass_b;
        }
    };

    template <typename Container>
    concept GlmContainer = requires(Container container) {
        requires std::is_integral_v<typename Container::value_type> || std::is_floating_point_v<typename Container::value_type>;
        {container[0]} -> std::convertible_to<typename Container::value_type>;
        {Container::length()} -> std::integral;
    };

    #ifdef __GNUC__
    #pragma GCC diagnostic push
    #pragma GCC diagnostic ignored "-Wunused-function"
    #endif
    static std::vector<char> read_file(const std::string &file_name) {
        std::vector<char> buffer;
        std::ifstream file(file_name, std::ios::ate | std::ios::binary);
        size_t file_size = static_cast<size_t>(file.tellg());

        if(!file.is_open()) {
            throw std::runtime_error("Couldn't read shader file " + file_name);
        }

        buffer.resize(file_size);

        file.seekg(0);
        file.read(buffer.data(), file_size);

        file.close();

        return buffer;
    }

    [[nodiscard]] inline std::array<vk::raii::Semaphore, utils::vulkan::MAX_FRAMES_IN_FLIGHT> create_semaphores(vk::raii::Device &logical_device) {
        std::array<VkSemaphore, utils::vulkan::MAX_FRAMES_IN_FLIGHT> semaphores;
        vk::SemaphoreCreateInfo create_info{};

        for (size_t i = 0; semaphores.size() > i; ++i) {
            vk::Result result = static_cast<vk::Result>(logical_device.getDispatcher()->vkCreateSemaphore(
                *logical_device,
                reinterpret_cast<const VkSemaphoreCreateInfo*>(&create_info),
                nullptr, &semaphores[i]));

            if (result != vk::Result::eSuccess) {
                throw std::runtime_error("Couldn't create semaphore!");
            }
        }

        return { { {logical_device, semaphores[0], nullptr}, {logical_device, semaphores[1], nullptr} } };
    }


    [[nodiscard]] inline std::array<vk::raii::Fence, utils::vulkan::MAX_FRAMES_IN_FLIGHT> create_signaled_fences(vk::raii::Device &logical_device) {
        vk::FenceCreateInfo create_info{ vk::FenceCreateFlagBits::eSignaled };

        return { { {logical_device, create_info, nullptr}, {logical_device, create_info, nullptr} } };
    }

    [[nodiscard]] inline glm::ivec2 get_image_size(const std::string &image_path) {
        glm::ivec2 image_size;
        int garbage;

        int ok = stbi_info(image_path.c_str(), &image_size.x, &image_size.y, &garbage);

        if (!ok) {
            throw std::runtime_error("couldn't load " + image_path + "!");
        }

        return image_size;
    }

    [[nodiscard]] inline std::optional<std::string> add_string_to_optional(std::optional<std::string> optional, std::string string_to_add) {
        if (optional.has_value()) {
            return optional.value() + string_to_add;
        }
        else {
            return {};
        }
    }

    [[nodiscard]] inline SHA256Digest get_sha256_hash(unsigned char* data, size_t length) {
        SHA256 sha256{};
        SHA256Digest digest_buffer;

        sha256.init();

        sha256.update(data, length);

        sha256.final(digest_buffer.data());

        return digest_buffer;
    }

    [[nodiscard]] inline std::optional<std::pair<std::vector<unsigned char>, SHA256Digest>> get_sha256_hash_file(const std::string &file_path) {
        std::ifstream input_file(file_path, std::ios::binary);

        if (!input_file.is_open()) {
            return {};
        }

        std::vector<unsigned char> file_buffer(std::istreambuf_iterator<char>(input_file), {});

        return std::pair<std::vector<unsigned char>, SHA256Digest>{ file_buffer, get_sha256_hash(file_buffer.data(), file_buffer.size() * sizeof(unsigned char)) };
    }

    template<GlmContainer Container>
    [[nodiscard]] static nlohmann::json get_glm_json(const Container &container) {
        nlohmann::json return_value{};

        for (int i = 0; Container::length() > i; ++i) {
            return_value.push_back(container[i]);
        }

        return return_value;
    }


    // replace all of these get_*_json stuff with templates using ::value_type and ::length()
    [[nodiscard]] static glm::vec2 get_vec2_json(const nlohmann::json::value_type& vec2_json) {
        return { vec2_json[0], vec2_json[1] };
    }

    [[nodiscard]] static glm::ivec2 get_ivec2_json(const nlohmann::json::value_type& ivec2_json) {
        return get_vec2_json(ivec2_json);
    }

    [[nodiscard]] static glm::vec3 get_vec3_json(const nlohmann::json::value_type& vec3_json) {
        return { vec3_json[0], vec3_json[1], vec3_json[2] };
    }

    [[nodiscard]] static glm::ivec3 get_ivec3_json(const nlohmann::json::value_type& ivec3_json) {
        return get_vec3_json(ivec3_json);
    }

    [[nodiscard]] static glm::vec4 get_vec4_json(const nlohmann::json::value_type& vec4_json) {
        return { vec4_json[0], vec4_json[1], vec4_json[2], vec4_json[3] };
    }

    [[nodiscard]] static glm::ivec4 get_ivec4_json(const nlohmann::json::value_type& ivec4_json) {
        return get_vec4_json(ivec4_json);
    }

    [[nodiscard]] static glm::quat get_quat_json(const nlohmann::json::value_type& quat_json) {
        return { quat_json[0], quat_json[1], quat_json[2], quat_json[3] };
    }

    // TODO(Bobby): add a glm::mat constraint to this template
    template <typename GlmMatType>
    [[nodiscard]] static GlmMatType get_mat_json(const nlohmann::json::value_type& mat_json) {
        GlmMatType return_value;
        size_t dimension = sqrt(return_value.length());

        assert((dimension * dimension) == return_value.length());

        for (size_t i = 0; return_value.length() > i; ++i) {
            return_value[i / dimension][i % dimension] = mat_json[i];
        }

        return return_value;
    }

    template <typename DerivedType>
    static DerivedType *debug_dynamic_cast(void *base_ptr) {
        #ifndef NDEBUG
            return dynamic_cast<DerivedType*>(base_ptr);
        #else
            return static_cast<DerivedType*>(base_ptr);
        #endif
    }

    template<typename Type>
    concept Hashable = requires(Type type)
    {
        { std::hash<Type>{}(type) } -> std::convertible_to<std::size_t>;
    };

    template <typename BaseType, typename DerivedType>
    requires(std::is_base_of_v<BaseType, DerivedType>)
    static DerivedType &compile_checked_down_cast(BaseType *base_type) {
        return *static_cast<DerivedType*>(base_type);
    }

    static void _no_op_func() {}

    static std::string string_to_lower(std::string string) {
        for(size_t i = 0; string.size() > i; ++i) {
            if(string[i] >= 65 && string[i] < 90) {
                // 97 represents 'A' and 65 represents 'a'
                string[i] = (static_cast<char>(string[i]) - 65) + 97;
            }
        }

        return string;
    }

    static std::string string_to_upper(std::string string) {
        for(size_t i = 0; string.size() > i; ++i) {
            if(string[i] >= 97 && string[i] < 123) {
                // 97 represents 'A' and 65 represents 'a'
                string[i] = (static_cast<char>(string[i]) - 97) + 65;
            }
        }

        return string;
    }

    static bool check_for_exclusive_bit_on(size_t value, size_t exclusive_bit, size_t all_bits) {
        return (value & all_bits) == exclusive_bit;
    }

    template<typename SingletonType>
    SingletonType &get_singleton_safe() {
        assert(SingletonType::instance != nullptr);

        return *SingletonType::instance;
    }

    static constexpr size_t get_bit_mask(size_t start_bit, size_t len) {
        size_t return_value = 0;

        for (size_t i = 0; len > i; ++i) {
            return_value |= 1 << (start_bit + i);
        }

        return return_value;
    }

    template<typename JsonType>
    [[nodiscard]] JsonType get_or_default_json(const nlohmann::json &json, const std::string &json_field, JsonType default_data) {
        return json.contains(json_field) ? json.at(json_field).get<JsonType>() : default_data;
    }

    template<typename JsonType>
    [[nodiscard]] JsonType get_or_default_json(const nlohmann::json &json, const std::string &json_field, JsonType default_data, const std::function<JsonType(nlohmann::json)> &json_parse_function) {
        return json.contains(json_field) ? json_parse_function(json.at(json_field)) : default_data;
    }

    // credit to see on stack overflow: https://stackoverflow.com/a/72073933
    struct Uint32VectorHasher {
        std::size_t operator()(std::vector<uint32_t> const& vec) const {
            std::size_t seed = vec.size();
            for (auto x : vec) {
                x = ((x >> 16) ^ x) * 0x45d9f3b;
                x = ((x >> 16) ^ x) * 0x45d9f3b;
                x = (x >> 16) ^ x;
                seed ^= x + 0x9e3779b9 + (seed << 6) + (seed >> 2);
            }
            return seed;
        }
    };

#ifdef __GNUC__
#pragma GCC diagnostic pop
#endif
}

#endif //ARISTAEUS_UTILS_HPP
