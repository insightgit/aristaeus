//
// Created by bobby on 5/25/22.
//

#ifndef ARISTAEUS_VULKAN_SCENE_HPP
#define ARISTAEUS_VULKAN_SCENE_HPP

#include <fstream>

#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "../gfx/vulkan/assimp_model.hpp"
#include "../gfx/vulkan/perspective_camera_environment.hpp"
#include "../gfx/vulkan/core_renderer.hpp"
#include "../gfx/vulkan/texture.hpp"

#include "../utils.hpp"

#include "scene_loader.hpp"

namespace aristaeus::core {
    class Scene {
    public:
        Scene(int argc, char **argv);

        void render();
    private:
        static constexpr std::string_view DEFAULT_STARTING_ENVIRONMENT = "vulkan_environment_class_db";

        struct SwapChainInfo {
            vk::raii::Device logical_device;
            vk::raii::SwapchainKHR swap_chain;
            vk::Extent2D swap_chain_extent;
            vk::Format swap_chain_image_format;
        };


        static constexpr std::array<vk::DescriptorPoolSize, 2> POOL_SIZES {{
           {vk::DescriptorType::eCombinedImageSampler, static_cast<uint32_t>(utils::vulkan::MAX_FRAMES_IN_FLIGHT)},
           {vk::DescriptorType::eUniformBuffer, static_cast<uint32_t>(utils::vulkan::MAX_FRAMES_IN_FLIGHT)}
        }};

        static constexpr std::string_view PREFER_INTEGRATED_GPU_STRING = "use_integrated";
        static constexpr std::string_view PREFER_SYNC_COMPUTE_GPU_STRING = "use_sync_compute";
        static constexpr std::string_view CUSTOM_ENVIRONMENT_OPTION_STRING = "custom_environment=";

        SwapChainInfo create_swap_chain_info() {
            vk::raii::Device logical_device = m_window.create_logical_device();
            vk::raii::SwapchainKHR swap_chain = m_window.create_swap_chain(logical_device);

            return {
                .logical_device = std::move(logical_device),
                .swap_chain = std::move(swap_chain),
                .swap_chain_extent = m_window.get_last_created_swap_chain_extent(),
                .swap_chain_image_format = m_window.get_last_created_swap_chain_image_format()
            };
        }

        gfx::vulkan::Window construct_window(int argc, char **argv);

        gfx::vulkan::CoreRenderer::EnvironmentLoadingInfo get_environment_loading_info(int argc, char **argv);

        gfx::vulkan::CoreRenderer create_core_renderer(int argc, char **argv);

        SceneLoader m_scene_loader;
        gfx::vulkan::CoreRenderer::EnvironmentLoadingInfo m_environment_loading_info;

        gfx::vulkan::Window m_window;

        SwapChainInfo m_construction_swap_chain_info;
        vk::Viewport m_construction_viewport;
        vk::Rect2D m_construction_viewport_scissor;

        gfx::vulkan::MeshManager m_mesh_manager;

        gfx::vulkan::CoreRenderer m_renderer;
    };
}

#endif //ARISTAEUS_VULKAN_SCENE_HPP
