#version 460

layout (location = 0) in vec3 position;

layout (set = 0, binding = 0) uniform PerspectiveCameraUBO {
    mat4 projection;
    mat4 view;
    vec3 camera_front;
    vec3 camera_up;
    vec3 camera_right;
    vec3 camera_position;
} camera_ubo;

layout (location = 0) out VertexOut {
    vec3 color;
} vertex_out;

void main() {
    gl_Position = camera_ubo.projection * camera_ubo.view * vec4(position, 1.0);
    gl_PointSize = 2.0f;
}