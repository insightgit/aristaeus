
#include "core_renderer.hpp"

#include "sky_box.hpp"
#include "lighting_environment.hpp"
#include "perlin_noise.hpp"

#include "directional_light.hpp"

bool aristaeus::gfx::vulkan::LightingEnvironment::NULL_CUBE_MAP_TEXTURE_INITED = false;
std::unique_ptr<aristaeus::gfx::vulkan::CubeMapTexture>
aristaeus::gfx::vulkan::LightingEnvironment::NULL_CUBE_MAP_TEXTURE{};

aristaeus::gfx::vulkan::LightingEnvironment::LightingEnvironment(const PBRMaterialInfo &default_pbr_material_info, const std::vector<glm::vec3> &initial_capture_points,
							                                     vk::Extent2D initial_depth_capture_resolution, size_t initial_capture_point_capacity, size_t initial_max_num_of_point_lights, 
                                                                 std::optional<vk::Extent2D> initial_capture_resolution) :
    m_default_pbr_material_info(default_pbr_material_info) {
    if (!NULL_CUBE_MAP_TEXTURE_INITED) {
        std::array<std::string, 6> texture_faces{};

        for (size_t i = 0; texture_faces.size() > i; ++i) {
            texture_faces[i] = "../aristaeus/gfx/null_image.png";
        }

        NULL_CUBE_MAP_TEXTURE = std::make_unique<CubeMapTexture>(texture_faces);
        NULL_CUBE_MAP_TEXTURE_INITED = true;
    }

    add_event<void*>(std::string{ ADDED_TO_CAMERA_ENVIRONMENT_EVENT });
}

aristaeus::gfx::vulkan::LightingEnvironment::~LightingEnvironment() {
    // TODO(Bobby): actually Reference count this
    NULL_CUBE_MAP_TEXTURE_INITED = false;
    NULL_CUBE_MAP_TEXTURE.reset(nullptr);
}

void aristaeus::gfx::vulkan::LightingEnvironment::input(aristaeus::gfx::vulkan::Window& window) {
    bool minus_pressed = window.is_key_pressed_with_delay(GLFW_KEY_MINUS, 500);
    bool plus_pressed = window.is_key_pressed_with_delay(GLFW_KEY_EQUAL, 500);

    if (minus_pressed || plus_pressed) {
        if (minus_pressed) {
            m_shadow_mode = glm::max(m_shadow_mode - 1, 0u);
        }
        else {
            m_shadow_mode = glm::min(m_shadow_mode + 1, 3u);
        }

        std::cout << "shadow mode: " << m_shadow_mode << "\n";
    }
}

void aristaeus::gfx::vulkan::LightingEnvironment::set_perspective_camera_and_root(PerspectiveCameraEnvironment &camera_environment, const std::shared_ptr<SceneGraphNode> &scene_graph_node_root) {
    m_scene_graph_node_root = scene_graph_node_root;

    call_event<void*>(std::string{ ADDED_TO_CAMERA_ENVIRONMENT_EVENT }, nullptr);
}
