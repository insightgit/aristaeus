//
// Created by bobby on 16/04/23.
//

#ifndef ARISTAEUS_GPU_EQUIRECTANGULAR_CONVERTER_HPP
#define ARISTAEUS_GPU_EQUIRECTANGULAR_CONVERTER_HPP

#include <unordered_map>

#include <glm/glm.hpp>

#include <gfx/vulkan/assimp_model.hpp>
#include <gfx/vulkan/texture.hpp>

#include <utils.hpp>

#include "compute_pipeline.hpp"
#include "cube_map_compute_task.hpp"

namespace aristaeus::gfx::vulkan::compute {
    class EquirectangularConverter : public CubeMapComputeTask {
    public:
        // order should correspond with equirectangular_converter.comp
        enum class CubeMapOrder {
            BACK = 0,
            FRONT = 1,
            TOP = 2,
            BOTTOM = 3,
            LEFT = 4,
            RIGHT = 5
        };

        struct EquirectangularConverterInfo {
            std::string equirectangular_input_texture_path;
            glm::ivec2 image_size;
        };

        struct EquirectangularOutputInfo {
            ImageRaii image;
            std::vector<vk::raii::ImageView> image_views;
            vk::raii::Sampler sampler;
        };

        EquirectangularConverter(const std::string &equirectangular_input_texture_path,
                                 const std::optional<std::string> &cache_path = {});

        [[nodiscard]] std::string get_input_texture_path() const {
            return m_equirectangular_converter_info.equirectangular_input_texture_path;
        }

        void dispatch(vk::CommandBuffer &command_buffer)override;
    private:
        static constexpr uint32_t WORK_GROUP_X = 16;
        static constexpr uint32_t WORK_GROUP_Y = 16;
        static constexpr uint32_t WORK_GROUP_SIZE = WORK_GROUP_X * WORK_GROUP_Y * 1; // reference equirectangular_convert.comp

        struct ConversionInfo {
            alignas(8) glm::ivec2 image_size;
            alignas(16) glm::mat4 view_transformation;
        };

        [[nodiscard]] EquirectangularConverterInfo get_converter_info(const std::string &equirectangular_input_texture_path) {
            EquirectangularConverterInfo equirectangular_converter_info {.equirectangular_input_texture_path = equirectangular_input_texture_path};

            int trash;

            int ok = stbi_info(equirectangular_input_texture_path.c_str(), &equirectangular_converter_info.image_size.x, &equirectangular_converter_info.image_size.y, &trash);

            if (!ok) {
                throw std::runtime_error("couldn't get image info! is this a valid image?");
            }

            return equirectangular_converter_info;
        }

        VulkanPipeline::ShaderInfo get_compute_shader_info() {
            return {
                .code = utils::read_file("environments/vulkan_environment_shaders/compute/equirectangular_convert.comp.spv"),
                .name = "equirectangular_convert.comp",
                .stage = vk::ShaderStageFlagBits::eCompute
            };
        }

      
        void update_descriptor_set(Texture &equirectangular_input_texture, const glm::ivec2 &image_size,
                                   uint32_t index);

        EquirectangularOutputInfo create_output_data();

        EquirectangularConverterInfo m_equirectangular_converter_info;
        std::unique_ptr<Texture> m_equirectangular_converter_texture;

        ConversionInfo m_conversion_info;

        std::mutex m_descriptor_count_mutex;
        size_t previous_frame = -1;
        size_t current_descriptor_set_count = 0;
    };
}

#endif //ARISTAEUS_GPU_PERLIN_NOISE_HPP
