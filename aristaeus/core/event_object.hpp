//
// Created by bobby on 15/04/24.
//

#ifndef ARISTAEUS_EVENT_OBJECT_HPP
#define ARISTAEUS_EVENT_OBJECT_HPP

#include <string>

#include "std_type_info_ref.hpp"

namespace aristaeus::core {
    class EventObject;
    class ClassDB;

    template<class ObjectType>
    concept EventObjectType = std::is_base_of<EventObject, ObjectType>::value;

    class EventObject {
    public:
        template<typename ArgumentType, EventObjectType ObjectType>
        bool add_callback_for_event(const std::string &event_name, const std::string &callback_name,
                                    const std::function<void (ObjectType&, ArgumentType)> &callback_function,
                                    ObjectType &from_object);

        template<typename ArgumentType>
        bool remove_callback_for_event(const std::string &event_name, const std::string &callback_name);

        template<typename ArgumentType>
        bool call_event(const std::string& event_name, ArgumentType argument) {
            return call_event_unsafe(event_name, typeid(ArgumentType), static_cast<void*>(&argument));
        }
    protected:
        template<typename ArgumentType>
        void add_event(const std::string &event_name) {
            add_event(event_name, typeid(ArgumentType));
        }

        // adding events should mainly be done thru ClassDB's register_event function on registering classes with ClassDB,
        // unless this isn't a node.
        void add_event(const std::string &event_name, StdTypeInfoRef argument_type) {
            EventInfo event_info{ .argument_type = argument_type };

            m_events.insert({ event_name, event_info });
        }

        bool remove_event(const std::string &event_name) {
            if (m_events.contains(event_name)) {
                m_events.erase(event_name);
                return true;
            }
            else {
                return false;
            }
        }
    private:
        typedef std::function<void (EventObject &event_object, void*)> EventCallbackFunction;

        friend class core::ClassDB;

        // only use this when you only have a typeid for an argument type:
        // it doesn't verify that the void* in the function ptr represents
        // a ptr of the argument type represented in the typeid.
        bool add_callback_for_event_unsafe(const std::string &event_name,
                                           const std::string &callback_name,
                                           const std::function<void (EventObject&, void*)> &callback_function,
                                           core::StdTypeInfoRef argument_type_info, EventObject &from_object);

        // disclaimer for add_callback_for_event_unsafe applies here as well
        bool call_event_unsafe(const std::string &event_name, core::StdTypeInfoRef argument_type_info, void *argument);

        struct EventInfo {
            struct CallbackInfo {
                EventObject &event_object;
                EventCallbackFunction callback_function;
            };

            core::StdTypeInfoRef argument_type;
            std::unordered_map<std::string, CallbackInfo> event_callbacks;
        };

        std::unordered_map<std::string, EventInfo> m_events;
    };
}

#include "event_object.inl"

#endif //ARISTAEUS_EVENT_OBJECT_HPP
