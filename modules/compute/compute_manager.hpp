#ifndef ARISTAEUS_COMPUTE_MANAGER
#define ARISTAEUS_COMPUTE_MANAGER

#include <array>
#include <fstream>

#include <json.hpp>

#include <core/singleton.hpp>

#include <gfx/vulkan/command_buffer_manager.hpp>
#include <gfx/vulkan/vulkan_raii.hpp>

#include <utils.hpp>

#include "compute_task.hpp"

namespace aristaeus::gfx::vulkan {
    class CoreRenderer;
}

namespace aristaeus::gfx::vulkan::compute {
    class ComputeManager : public core::Singleton<ComputeManager> {
	public:
		ComputeManager(const std::optional<uint32_t> &compute_family_index, const std::optional<uint32_t> &queue_index,
					   const std::optional<std::string> &compute_info_path);

		[[nodiscard]] bool is_async() const {
			return m_async_compute_queue.has_value();
		}

		void save_compute_cache_info();
		
		size_t add_compute_task(std::shared_ptr<ComputeTask> compute_task);

		// if async, this will use m_compute_fences. if not, it is up to the caller to make sure the previous frame's fence has been waited for
		// because it will blindly update all of this frame's jobs to be completed.
		void update_submitted_jobs(std::optional<uint32_t> current_frame = {});

		// should only use this for async compute
		void execute_async_compute_jobs();

		// this should be executed 
		void execute_sync_compute(vk::CommandBuffer &command_buffer_to_record_into,
                                  bool allowed_with_async_queue = false);
	private:
		static constexpr size_t MAX_TASKS_PER_FRAME = 10000;

		std::unique_ptr<CommandBufferManager> get_command_buffer_manager(std::optional<uint32_t> compute_family_index);

		size_t m_current_job_index = 0;
		std::optional<vk::raii::Queue> m_async_compute_queue;

		std::array<vk::raii::Fence, utils::vulkan::MAX_FRAMES_IN_FLIGHT> m_compute_fences;
		// TODO(Bobby): this should be std::optional but was having issues with deleted copy constructors for some reason
		std::unique_ptr<CommandBufferManager> m_command_buffer_manager;

		std::optional<vk::raii::Queue> get_async_compute_queue(std::optional<uint32_t> compute_family_index,
                                                               std::optional<uint32_t> queue_index);

		void populate_command_buffers(vk::CommandBuffer &command_buffer_to_populate, bool is_sync_compute);

		nlohmann::json create_and_verify_compute_cache_json(const std::optional<std::string> &compute_cache_path);

		std::optional<std::string> m_compute_info_path;
		nlohmann::json m_compute_cache_info_json;

		// compute task ptr and the desired next compute task status
		std::array<std::vector<std::shared_ptr<ComputeTask>>, utils::vulkan::MAX_FRAMES_IN_FLIGHT> m_pending_jobs;
		std::array<std::mutex, utils::vulkan::MAX_FRAMES_IN_FLIGHT> m_pending_job_locks;

		std::array<std::vector<std::shared_ptr<ComputeTask>>, utils::vulkan::MAX_FRAMES_IN_FLIGHT> m_submitted_jobs;

		std::array<std::vector<std::shared_ptr<ComputeTask>>, utils::vulkan::MAX_FRAMES_IN_FLIGHT> m_saving_jobs;
		std::array<BufferRaii, utils::vulkan::MAX_FRAMES_IN_FLIGHT> m_saving_job_buffers;

		std::mutex m_pending_loading_jobs_lock;
		std::vector<std::shared_ptr<ComputeTask>> m_pending_loading_jobs;
		std::vector<std::vector<std::vector<unsigned char>>> m_pending_loading_images_data; // TODO(Bobby): make this not awful but still linked to each ComputeTask
		std::array<BufferRaii, utils::vulkan::MAX_FRAMES_IN_FLIGHT> m_loading_job_buffers;
	};
}

#endif