#version 450

const int MAX_NUM_OF_DIRECTIONAL_LIGHTS = 5;
const int MAX_NUM_OF_POINT_LIGHTS = 5;
const int MAX_NUM_OF_TERRAIN_TEXTURES = 4;

#extension GL_EXT_debug_printf : enable
#extension GL_EXT_nonuniform_qualifier : enable

const int OBJECT_TYPE_OBJECT = 0;
const int OBJECT_TYPE_LIGHT = 1;
const int OBJECT_TYPE_WATER = 2;
const int OBJECT_TYPE_WATER_GERSTNER = 3;

const float HEIGHT_SCALE = 0.1;

const float GRAVITY_CONSTANT = 9.8;
const float PI = 3.141592653589793238;

const float POINT_LIGHT_DEPTH_RESOLUTION = 100;
const float SHADOW_ALPHA = 0.9f;

struct LightingNode {
    float constant_attenuation;
    float linear_attenuation;
    float quadratic_attenuation;

    int light_active;
    int light_type;

    vec3 diffuse;

    vec3 position_direction;
};

struct Fog {
    float constant_attenuation;
    float linear_attenuation;
    float quadratic_attenuation;

    vec3 fog_color;

    int fog_active;

    // 0: light attenuation-based, 1: linear interporlation, 2: smoothstep
    int fog_mode;
    float fog_start;
    float fog_end;
};

struct PBRMaterialInfo {
    float roughness;
    float metallic;
    float ao;
    int using_gltf_metallicroughness_texture;

    ivec2 albedo_map_range;
    ivec2 normal_map_range;
    ivec2 metallic_map_range;
    ivec2 roughness_map_range;
    ivec2 ao_map_range;

    vec4 albedo;
    vec3 normal;
    vec3 world_position;
};


#ifdef DEFERRED_MODE
layout (location = 0) in vec2 uv_coords;
layout (set = 1, binding = 10) uniform sampler2D position_screen_texture;
layout (set = 1, binding = 11) uniform sampler2D normal_screen_texture;
layout (set = 1, binding = 12) uniform sampler2D albedo_screen_texture;
layout (set = 1, binding = 13) uniform sampler2D roughness_metallic_ao_screen_texture;
layout (input_attachment_index = 0, set = 1, binding = 14) uniform subpassInput ao_screen_texture;
#else
layout (location = 0) in VertexOut {
    vec3 normal_vector;
    mat3 tbn_matrix;
    vec2 texture_coords_out;
    vec3 world_position;

    float instance_index;
} from_vertex;
#endif

layout (location = 0) out vec4 color_out;

layout (set = 0, binding = 0) uniform PerspectiveCameraUBO {
    mat4 projection;
    mat4 view;
    mat4 light_space_matrix;
    vec3 camera_front;
    vec3 camera_up;
    vec3 camera_right;
    vec3 camera_position;
    float camera_far_plane;
} camera_ubo;

const int SHADOWS_ENABLED_NO_SHADOWS = 0;
const int SHADOWS_ENABLED_DIRECTIONAL_SHADOWS_ONLY = 1;
const int SHADOWS_ENABLED_POINT_SHADOWS_ONLY = 2;
const int SHADOWS_ENABLED_ALL_SHADOWS = 3;

const int MAX_NUM_OF_WAVES = 100;

layout (set = 1, binding = 0) uniform EnvironmentUBO {
    Fog fog;

    // If memory is an issue, we can convert all of these into one int
    int num_of_directional_lights;
    int num_of_point_lights;
    int show_normals;
    int shadows_enabled; // takes a SHADOWS_ENABLED parameter
    int gi_mode;
    int num_of_rsm_vpls;
    float rsm_weighting;
    float sampling_radius_max;

    mat4 directional_light_matrices[MAX_NUM_OF_DIRECTIONAL_LIGHTS];
    LightingNode directional_lights[MAX_NUM_OF_DIRECTIONAL_LIGHTS];
    LightingNode point_lights[MAX_NUM_OF_POINT_LIGHTS];

    float point_light_shadow_bias;
} environment_ubo;

layout(set = 1, binding = 1) uniform sampler2D depth_map;
layout(set = 1, binding = 2) uniform samplerCube point_light_depth_maps[MAX_NUM_OF_POINT_LIGHTS];
layout(set = 1, binding = 3) uniform sampler2D caustic_environment_map;
layout(set = 1, binding = 4) uniform samplerCube diffuse_convolution;
layout(set = 1, binding = 5) uniform samplerCube specular_convolution;
layout(set = 1, binding = 6) uniform sampler2D brdf_lut;

struct Material {
    int override_texture;
    int object_type;
    vec3 light_box_color;

    PBRMaterialInfo pbr_material_info;
};

#ifndef DEFERRED_MODE
layout (std430, set = 2, binding = 0) readonly buffer MaterialUBO {
    Material materials[];
} mat_ubo;

const int MATERIAL_SIZE = 10;

// TODO(Bobby): Convert this into a sampler array
layout(set = 2, binding = 1) uniform sampler2D texture_albedo[];
layout(set = 2, binding = 2) uniform sampler2D texture_normal[];
layout(set = 2, binding = 3) uniform sampler2D texture_metallic[];
layout(set = 2, binding = 4) uniform sampler2D texture_roughness[];
layout(set = 2, binding = 5) uniform sampler2D texture_ao[];

struct Instance {
    mat4 model;
    mat3 normal_transform;
    uint material_index;
};

layout (std430, set = 3, binding = 0) readonly buffer InstanceUBO {
    Instance instances[];
} instance_ubo;

#endif

const float EPSILON = 0.20f;
const float BIAS_MAX = 0.15f;
const float BIAS_MIN = 0.01f;

// source: https://theorangeduck.com/page/avoiding-shader-conditionals
float when_greater_than(float x, float y) {
    return max(sign(x - y), 0.0);
}

float when_less_than(float x, float y) {
    return max(sign(y - x), 0.0);
}

float when_less_equal_than(float x, float y) {
    return 1.0 - when_greater_than(x, y);
}

float when_greater_equal_than(float x, float y) {
    return 1.0 - when_less_than(x, y);
}

float when_equal_to(float x, float y) {
    return 1.0 - abs(sign(x - y));
}

float when_not_equal_to(float x, float y) {
    return 1.0 - when_equal_to(x, y);
}

float when_less_equal_to(float x, float y) {
    return 1.0 - when_greater_than(x, y);
}

float or(float x, float y) {
    return min(x + y, 1.0);
}

vec3 get_rid_of_neg(vec3 input_vector) {
    return vec3(max(input_vector.x, 0.0), max(input_vector.y, 0.0), max(input_vector.z, 0.0));
}

const int SSAO_ENABLED = 0x10;
const int IBL_ENABLED = 0x20;
const int DIRECT_POINT_ENABLED = 0x40;
const int DIRECT_DIRECTIONAL_ENABLED = 0x80;

// reflective shadow map code begin
const int RSM_STRIDE = 4; // albedo, normal, pos, depth

const int RSM_DIRECTIONAL_MODE = 0x01;
const int RSM_POINT_MODE = 0x02;

layout(set = 1, binding = 7) uniform sampler2D directional_rsms[MAX_NUM_OF_DIRECTIONAL_LIGHTS * RSM_STRIDE];
layout(set = 1, binding = 8) uniform samplerCube point_rsms[MAX_NUM_OF_POINT_LIGHTS * RSM_STRIDE];
layout(set = 1, binding = 9) uniform sampler2D white_noise;

struct HemisphereSampleInfo {
    float rand_1;
    float rand_2;
    vec3 hemisphere_sample;
};

HemisphereSampleInfo hemisphere_rand(vec2 rand_sample_1, vec2 rand_sample_2) {
    HemisphereSampleInfo sample_info;

    sample_info.rand_1 = texture(white_noise, rand_sample_1).r;
    sample_info.rand_2 = texture(white_noise, rand_sample_2).g;

    // we need to do a 3d sample on a hemisphere of possible light paths, not a 2d sample on a circle like in
    // directional rsm calculation.
    float latitude = 2 * PI * sample_info.rand_1;
    float longitude = (PI / 2) * sample_info.rand_2;

    sample_info.hemisphere_sample = vec3(cos(latitude) * sin(longitude), sin(latitude) * sin(longitude), cos(longitude));

    return sample_info;
}

HemisphereSampleInfo hemisphere_rand_one_sample(vec2 rand_sample) {
    return hemisphere_rand(rand_sample, rand_sample);
}

vec3 rsm_pixel_light_irradiance_directional(vec2 pixel_light, vec3 frag_pos, vec3 frag_normal, int light_index) {
    int light_index_offset = light_index * RSM_STRIDE;

    vec3 position = texture(directional_rsms[light_index_offset], pixel_light).rgb;
    vec3 normal = texture(directional_rsms[light_index_offset + 1], pixel_light).rgb;
    vec3 albedo = texture(directional_rsms[light_index_offset + 2], pixel_light).rgb;

    vec3 frag_pos_minus_pos = normalize(frag_pos - position);
    vec3 pos_minus_frag_pos = normalize(position - frag_pos);

    if(frag_pos == position) {
        // to prevent NaNs arising from zero vectors
        return vec3(0);
    }

    float frag_pos_minus_pos_dot = max(0, dot(frag_normal, pos_minus_frag_pos));
    float pos_minus_frag_pos_dot = max(0, dot(normal, frag_pos_minus_pos));

    vec3 irradiance = albedo * frag_pos_minus_pos_dot * pos_minus_frag_pos_dot;
    irradiance /= pow(length(frag_pos_minus_pos), 4) + 0.0001;

    return irradiance;
}

vec3 rsm_pixel_light_irradiance_point(vec3 pixel_light, vec3 frag_pos, vec3 frag_normal, int light_index) {
    int light_index_offset = light_index * RSM_STRIDE;

    vec3 position = texture(point_rsms[light_index_offset], pixel_light).rgb;
    vec3 normal = texture(point_rsms[light_index_offset + 1], pixel_light).rgb;
    vec3 albedo = texture(point_rsms[light_index_offset + 2], pixel_light).rgb;

    vec3 frag_pos_minus_pos = normalize(frag_pos - position);
    vec3 pos_minus_frag_pos = normalize(position - frag_pos);

    if(frag_pos == position) {
        // to prevent NaNs arising from zero vectors
        return vec3(0);
    }

    float frag_pos_minus_pos_dot = max(0, dot(frag_normal, pos_minus_frag_pos));
    float pos_minus_frag_pos_dot = max(0, dot(normal, frag_pos_minus_pos));

    vec3 irradiance = albedo * frag_pos_minus_pos_dot * pos_minus_frag_pos_dot;
    irradiance /= pow(length(frag_pos_minus_pos), 4) + 0.0001;

    return irradiance;
}

vec3 rsm_irradiance_directional(vec4 frag_pos, vec3 frag_normal, int directional_light_index) {
    int num_of_pixel_lights_per_light = environment_ubo.num_of_rsm_vpls / (environment_ubo.num_of_directional_lights + environment_ubo.num_of_point_lights);

    vec3 directional_light_irradiance = vec3(0.0f);

    vec4 directional_projection_coords = environment_ubo.directional_light_matrices[directional_light_index] * frag_pos;

    vec2 pixel_light_base = directional_projection_coords.xy / directional_projection_coords.w;
    pixel_light_base = (pixel_light_base * 0.5) + 0.5;

    float sum_of_weights = 0.0f;

    for(int i2 = 0; num_of_pixel_lights_per_light > i2; ++i2) {
        float pixel_light_index = (directional_light_index * num_of_pixel_lights_per_light) + i2;

        float rand_1 = texture(white_noise, vec2(pixel_light_index / num_of_pixel_lights_per_light, 
                                                 1.0f - (pixel_light_index / num_of_pixel_lights_per_light))).r;
        float rand_2 = texture(white_noise, vec2(1.0f - (pixel_light_index / num_of_pixel_lights_per_light), 
                                                 pixel_light_index / num_of_pixel_lights_per_light)).r;

        rand_1 = (2 * rand_1) + 1;
        rand_2 = (2 * rand_2) + 1;

        vec2 pixel_light_position = pixel_light_base + (environment_ubo.sampling_radius_max * vec2(rand_1 * sin(2 * PI * rand_2), rand_1 * cos(2 * PI * rand_2)));

        float pixel_light_weight = rand_1 * rand_1;
        vec3 pixel_light_radiance = rsm_pixel_light_irradiance_directional(pixel_light_position, frag_pos.xyz, frag_normal, directional_light_index);

        directional_light_irradiance += pixel_light_weight * pixel_light_radiance;
        sum_of_weights += pixel_light_weight;
    }

    directional_light_irradiance /= sum_of_weights;

    return directional_light_irradiance;
}

vec3 rsm_irradiance_point(vec4 frag_pos, vec3 frag_normal, int point_light_index) {
    int num_of_pixel_lights_per_light = environment_ubo.num_of_rsm_vpls / (environment_ubo.num_of_directional_lights + environment_ubo.num_of_point_lights);
    vec3 irradiance = vec3(0.0f);

    vec3 pixel_light_base = normalize(frag_pos.xyz - environment_ubo.point_lights[point_light_index].position_direction);

    float sum_of_weights = 0.0f;

    for(int i2 = 0; num_of_pixel_lights_per_light > i2; ++i2) {
        float pixel_light_index = (point_light_index * num_of_pixel_lights_per_light) + i2;
        vec2 sample_index = vec2(pixel_light_index / num_of_pixel_lights_per_light);

        HemisphereSampleInfo sample_info = hemisphere_rand_one_sample(sample_index);

        sample_info.hemisphere_sample *= environment_ubo.sampling_radius_max;

        sample_info.hemisphere_sample += vec3(0, 0, 1);

        ivec2 tangent_offset_size = textureSize(point_rsms[point_light_index], 0);
        vec3 tangent_frag_vector = frag_pos.xyz + vec3(1.0 / tangent_offset_size.x, 0, 0);

        vec3 tangent = normalize(sample_info.hemisphere_sample - pixel_light_base * dot(sample_info.hemisphere_sample, pixel_light_base));

        vec3 binormal = cross(tangent, pixel_light_base);
        mat3 frag_tbn_matrix = mat3(tangent, binormal, pixel_light_base);

        vec3 pixel_light_position = normalize(frag_tbn_matrix * sample_info.hemisphere_sample);

        float pixel_light_weight = sample_info.rand_1 * sample_info.rand_1;
        vec3 pixel_light_radiance = rsm_pixel_light_irradiance_point(pixel_light_position, frag_pos.xyz, frag_normal, point_light_index);

        irradiance += pixel_light_weight * pixel_light_radiance;
        sum_of_weights += pixel_light_weight;
    }

    irradiance /= sum_of_weights;

    return irradiance;
}

// rsm code end

float directional_light_shadow_calculation(vec4 frag_pos, PBRMaterialInfo pbr_material_info, int light_index) {
    if(environment_ubo.shadows_enabled == SHADOWS_ENABLED_NO_SHADOWS ||
       environment_ubo.shadows_enabled == SHADOWS_ENABLED_POINT_SHADOWS_ONLY) {
        return 0.0f;
    }

    vec3 directional_light_direction = normalize(environment_ubo.directional_lights[light_index].position_direction);
    vec4 frag_pos_light_space = environment_ubo.directional_light_matrices[light_index] * frag_pos;

    // transform light space coords to NDC coords from range [0,1]
    vec3 projection_coords = frag_pos_light_space.xyz / frag_pos_light_space.w;

    float directional_light_dot = dot(pbr_material_info.normal, directional_light_direction);

    float current_depth = projection_coords.z;

    projection_coords = (projection_coords * 0.5) + 0.5;

    if(current_depth > 1.0 || current_depth < -1.0 || frag_pos_light_space.w <= 0.0) {
        return 0.0;
    }

    float shadow = 0.0f;
    vec2 shadow_map_texel_size;

    shadow_map_texel_size = 1.0f / textureSize(directional_rsms[(light_index * RSM_STRIDE) + 3], 0);

    for (int x = -1; x <= 1; ++x) {
        for (int y = -1; y <= 1; ++y) {
            vec2 texel_offseted_position = projection_coords.xy + (vec2(x, y) * shadow_map_texel_size);

            float pcf_depth;
            
            pcf_depth = texture(directional_rsms[(light_index * RSM_STRIDE) + 3], texel_offseted_position).r;

            shadow += current_depth > pcf_depth ? 1.0f : 0.0f;
        }
    }
    shadow /= 9.0f;

    if(current_depth > 1.0) {
        return 0.0;
    } else {
        return shadow * SHADOW_ALPHA;
    }
}

float normal_distribution_function(float halfway_normal_dot, float roughness) {
    float roughness_squared = roughness * roughness;
    float halfway_normal_dot_squared = halfway_normal_dot * halfway_normal_dot;

    float roughness_halfway_normal_dot_term = (halfway_normal_dot_squared * (roughness_squared - 1)) + 1;

    float denominator = PI * roughness_halfway_normal_dot_term * roughness_halfway_normal_dot_term;

    return roughness_squared / denominator;
}

float geometry_distribution_function_ggx(float normal_view_dot, float roughness) {
    float a = roughness + 1;

    float roughness_k_value = (a * a) / 8;
    float denominator = (normal_view_dot * (1 - roughness_k_value)) + roughness_k_value;

    return normal_view_dot / denominator;
}

float geometry_distribution_function(float view_normal_dot, float light_normal_dot, float roughness) {
    return geometry_distribution_function_ggx(view_normal_dot, roughness) * geometry_distribution_function_ggx(light_normal_dot, roughness); 
}

vec3 fresnel_approximation_function(float halfway_normal_dot, vec3 base_reflectivity) {
    vec3 schlick_approximation = (1 - base_reflectivity) * pow(1 - halfway_normal_dot, 5);

    return schlick_approximation + base_reflectivity;
}

#ifdef DEFERRED_MODE
PBRMaterialInfo get_pbr_material_info_deferred() {
    PBRMaterialInfo material_info;

    material_info.albedo = texture(albedo_screen_texture, uv_coords);
    material_info.normal = texture(normal_screen_texture, uv_coords).rgb;

    vec3 roughness_metallic_ao = texture(roughness_metallic_ao_screen_texture, uv_coords).rgb;
    float ssao_result = subpassLoad(ao_screen_texture).r;

    material_info.roughness = roughness_metallic_ao.r;
    material_info.metallic = roughness_metallic_ao.g;

    material_info.ao = (1.0f * when_equal_to(environment_ubo.gi_mode & SSAO_ENABLED, 0)) + (ssao_result * when_not_equal_to(environment_ubo.gi_mode & SSAO_ENABLED, 0));
    material_info.world_position = texture(position_screen_texture, uv_coords).rgb;

    material_info.albedo_map_range = ivec2(0);
    material_info.normal_map_range = ivec2(0);
    material_info.metallic_map_range = ivec2(0);
    material_info.roughness_map_range = ivec2(0);
    material_info.ao_map_range = ivec2(0);

    return material_info;
}
#else
PBRMaterialInfo get_pbr_material_info_forward() {
    Instance instance = instance_ubo.instances[int(from_vertex.instance_index)];
    Material material = mat_ubo.materials[instance.material_index];

    PBRMaterialInfo pbr_material_info = material.pbr_material_info;

    if(material.override_texture != 0) {
        if(material.pbr_material_info.normal.x <= 0.01 && 
           material.pbr_material_info.normal.y <= 0.01 && 
           material.pbr_material_info.normal.z <= 0.01) {
            pbr_material_info.normal = from_vertex.normal_vector;
        }

        pbr_material_info.world_position = from_vertex.world_position;

        return pbr_material_info;
    }

    for(int i = 0; material.pbr_material_info.albedo_map_range.y > i; ++i) {
        pbr_material_info.albedo += texture(texture_albedo[material.pbr_material_info.albedo_map_range.x + i], from_vertex.texture_coords_out);
    }

    for(int i = 0; material.pbr_material_info.roughness_map_range.y > i; ++i) {
        vec4 roughness_color = texture(texture_metallic[material.pbr_material_info.roughness_map_range.x + i], from_vertex.texture_coords_out);

        if(material.pbr_material_info.using_gltf_metallicroughness_texture != 0) {
            pbr_material_info.roughness += roughness_color.g;
        } else {
            pbr_material_info.roughness += roughness_color.r;
        }
    }

    for(int i = 0; material.pbr_material_info.metallic_map_range.y > i; ++i) {
        vec4 metallic_color = texture(texture_metallic[material.pbr_material_info.metallic_map_range.x + i], from_vertex.texture_coords_out);

        if(material.pbr_material_info.using_gltf_metallicroughness_texture != 0) {
            pbr_material_info.metallic += metallic_color.b;
        } else {
            pbr_material_info.metallic += metallic_color.r;
        }
    }

    for(int i = 0; material.pbr_material_info.ao_map_range.y > i; ++i) {
        pbr_material_info.ao += texture(texture_ao[material.pbr_material_info.ao_map_range.x + i], from_vertex.texture_coords_out).r;
    }

    if(material.pbr_material_info.normal_map_range.y != 0) {
        // normal mapping is enabled
        vec3 normal_map_tangent_space = vec3(0.0f);

        for(int i = 0; material.pbr_material_info.normal_map_range.y > i; ++i) {
            normal_map_tangent_space += texture(texture_normal[material.pbr_material_info.normal_map_range.x + i], from_vertex.texture_coords_out).xyz;
        }

        normal_map_tangent_space = (normal_map_tangent_space * 2.0f) - 1.0f;

        vec3 world_space_normal = vec3(from_vertex.tbn_matrix * normal_map_tangent_space);

        pbr_material_info.normal = normalize(world_space_normal);
    } else {
        pbr_material_info.normal = from_vertex.normal_vector;
    }    

    pbr_material_info.albedo.rgb = pow(pbr_material_info.albedo.rgb, vec3(2.2));

    // 0.0001 to prevent divide by zero
    pbr_material_info.roughness /= 0.0001 + material.pbr_material_info.roughness_map_range.y;
    pbr_material_info.metallic /= 0.0001 + material.pbr_material_info.metallic_map_range.y;
    pbr_material_info.ao /= 0.0001 + material.pbr_material_info.ao_map_range.y;

    pbr_material_info.ao = (when_less_equal_than(material.pbr_material_info.ao_map_range.y, 0) * 1) + 
                           (when_greater_than(material.pbr_material_info.ao_map_range.y, 0) * pbr_material_info.ao);
    pbr_material_info.world_position = from_vertex.world_position;

    return pbr_material_info;
}
#endif

PBRMaterialInfo get_pbr_material_info() {
    #ifdef DEFERRED_MODE
        return get_pbr_material_info_deferred();
    #else
        return get_pbr_material_info_forward();
    #endif
}

vec3 cook_torrance_brdf(vec3 light_direction, PBRMaterialInfo pbr_material_info) {
    vec3 view_vector = normalize(camera_ubo.camera_position - pbr_material_info.world_position);
    vec3 halfway_vector = normalize(light_direction + view_vector);

    float light_normal_dot = max(dot(light_direction, pbr_material_info.normal), 0.0);
    float view_normal_dot = max(dot(view_vector, pbr_material_info.normal), 0.0);
    float halfway_normal_dot = max(dot(pbr_material_info.normal, halfway_vector), 0.0);

    // from LearnOpenGL: most dielectrics' (non-metals) base reflectivity at 0 degrees is 0.04
    vec3 base_reflectivity = vec3(0.04);
    base_reflectivity = mix(base_reflectivity, pbr_material_info.albedo.rgb, pbr_material_info.metallic);

    vec3 fresnel = fresnel_approximation_function(max(dot(view_vector, halfway_vector), 0.0), base_reflectivity);
    float normal_distribution = normal_distribution_function(halfway_normal_dot, pbr_material_info.roughness);
    float geometry_distribution = geometry_distribution_function(view_normal_dot, light_normal_dot, pbr_material_info.roughness);

    vec3 cook_torrance_specular_component_numerator = normal_distribution * geometry_distribution * fresnel;
    // +0.0001 to prevent divide-by-zero
    float cook_torrance_specular_component_denominator = (4 * light_normal_dot * view_normal_dot) + 0.0001;

    vec3 cook_torrance_specular_component = cook_torrance_specular_component_numerator / cook_torrance_specular_component_denominator;

    vec3 diffuse_term = vec3(1.0) - fresnel;

    vec3 diffuse_lambertian_component = (diffuse_term * pbr_material_info.albedo.rgb) / PI;

    return diffuse_lambertian_component + cook_torrance_specular_component;
}

float point_light_shadow_calculation(uint point_light_index, vec3 world_position) {
    if(environment_ubo.shadows_enabled == SHADOWS_ENABLED_NO_SHADOWS ||
       environment_ubo.shadows_enabled == SHADOWS_ENABLED_DIRECTIONAL_SHADOWS_ONLY) {
        return 0.0f;
    }

    vec3 point_light_ray = world_position - environment_ubo.point_lights[point_light_index].position_direction;

    vec3 point_light_direction_ray = normalize(point_light_ray);

    if(abs(point_light_direction_ray.x) > abs(point_light_direction_ray.y) && abs(point_light_direction_ray.x) > abs(point_light_direction_ray.z) || 
       abs(point_light_direction_ray.y) > abs(point_light_direction_ray.x) && abs(point_light_direction_ray.y) > abs(point_light_direction_ray.z)) {
        point_light_direction_ray.z = -point_light_direction_ray.z;
    } else if(abs(point_light_direction_ray.z) > abs(point_light_direction_ray.x) && abs(point_light_direction_ray.z) > abs(point_light_direction_ray.y)) {
        point_light_direction_ray.x = -point_light_direction_ray.x;
    }

    float raw_texture_depth;

    raw_texture_depth = texture(point_rsms[(point_light_index * RSM_STRIDE) + 3], point_light_direction_ray).r;

    float adjusted_texture_depth = raw_texture_depth * POINT_LIGHT_DEPTH_RESOLUTION;

    // maybe include EPSILON if needed?
    float current_depth = length(point_light_ray) - 0.25;

    if(current_depth > 100) {
        return SHADOW_ALPHA;
    }

    float shadow_contribution = current_depth > adjusted_texture_depth ? SHADOW_ALPHA : 0.0;

    return shadow_contribution;
}

float attenuation(float distance, LightingNode point_light) {
    return min(1.0 / (point_light.constant_attenuation + (point_light.linear_attenuation * distance) +
               (point_light.quadratic_attenuation * pow(distance, 2))), 1.0f);
}

vec4 point_light_output(int light_index, PBRMaterialInfo pbr_material_info) {
    LightingNode point_light = environment_ubo.point_lights[light_index];

    vec3 light_direction = point_light.position_direction - pbr_material_info.world_position;
    float light_length = length(light_direction);

    light_direction = normalize(light_direction);
    
    float radiant_flux_cosine = max(dot(pbr_material_info.normal, light_direction), 0.0);
    float attn = attenuation(light_length, point_light);
    vec3 radiance = radiant_flux_cosine * attn * point_light.diffuse; 

    vec3 direct_illumination = cook_torrance_brdf(light_direction, pbr_material_info);

    float point_light_shadow = 1 - point_light_shadow_calculation(light_index, pbr_material_info.world_position);

    radiance *= point_light_shadow;

    vec3 direct_illumination_cos = radiance * direct_illumination;
    vec4 return_value = vec4(vec3(0), pbr_material_info.albedo.a);

    if((environment_ubo.gi_mode & DIRECT_POINT_ENABLED) != 0) {
        return_value.rgb += direct_illumination_cos;
    }

    return return_value;
}

vec4 directional_light_output(PBRMaterialInfo pbr_material_info, int light_index) {
    LightingNode directional_light = environment_ubo.directional_lights[light_index];

    vec3 radiance_cosine = max(dot(pbr_material_info.normal, normalize(-directional_light.position_direction)), 0.0) * directional_light.diffuse;

    vec3 direct_illumination = cook_torrance_brdf(normalize(-directional_light.position_direction), pbr_material_info);

    vec3 result = vec3(0);
    float shadow = directional_light_shadow_calculation(vec4(pbr_material_info.world_position, 1.0), pbr_material_info, light_index);

    if((environment_ubo.gi_mode & DIRECT_DIRECTIONAL_ENABLED) != 0) {
        result += (1 - shadow) * direct_illumination;
    }

    return vec4((radiance_cosine * result), pbr_material_info.albedo.a);
}

vec3 get_ibl_result(PBRMaterialInfo pbr_material_info) {
    vec3 to_view_direction = -normalize(camera_ubo.camera_position - pbr_material_info.world_position);

    vec3 base_reflectivity = vec3(0.04);
    base_reflectivity = mix(base_reflectivity, pbr_material_info.albedo.rgb, pbr_material_info.metallic);
    
    float target_lod = pbr_material_info.roughness * 5;

    vec3 fresnel = fresnel_approximation_function(max(dot(pbr_material_info.normal, to_view_direction), 0.0), base_reflectivity);
    vec3 diffuse_convolution_result = texture(diffuse_convolution, pbr_material_info.normal).rgb;

    vec3 scene_ambient_light = pbr_material_info.albedo.rgb * diffuse_convolution_result;
    vec3 ambient_diffuse_term = (1 - fresnel) * scene_ambient_light;

    float normal_view_dot_direction = max(dot(to_view_direction, pbr_material_info.normal), 0.0f);
    vec3 reflected_vector = reflect(-to_view_direction, pbr_material_info.normal);

    vec3 specular_radiance = textureLod(specular_convolution, reflected_vector, target_lod).rgb;
    vec2 brdf_lut_result = texture(brdf_lut, vec2(normal_view_dot_direction, 1 - pbr_material_info.roughness)).rg;
    vec3 ambient_specular_term = specular_radiance * ((fresnel * brdf_lut_result.x) + brdf_lut_result.y);

    vec3 ibl_result = pbr_material_info.ao * (ambient_diffuse_term + ambient_specular_term);

    return ibl_result;
}

float get_fog_strength(vec3 world_position) {
    float fog_distance = length(world_position - camera_ubo.camera_position) - environment_ubo.fog.fog_start;

    if(fog_distance < 0) {
        return 0.0f;
    } else if(environment_ubo.fog.fog_end > environment_ubo.fog.fog_start && fog_distance + environment_ubo.fog.fog_start >= environment_ubo.fog.fog_end) {
        return 1.0f;
    }

    float smoothstep_fog = smoothstep(environment_ubo.fog.fog_start, environment_ubo.fog.fog_end, fog_distance + environment_ubo.fog.fog_start) * when_equal_to(environment_ubo.fog.fog_mode, 2);
    float mix_fog = mix(0.0f, 1.0f, fog_distance / (environment_ubo.fog.fog_end - environment_ubo.fog.fog_start)) * when_equal_to(environment_ubo.fog.fog_mode, 1);
    float light_attenuated_fog = 1.0f - min(1.0 / (environment_ubo.fog.constant_attenuation + (environment_ubo.fog.linear_attenuation * fog_distance) +
        (environment_ubo.fog.quadratic_attenuation * pow(fog_distance, 2))), 1.0f);

    return smoothstep_fog + mix_fog + light_attenuated_fog;
}

void main() {
    PBRMaterialInfo pbr_material_info = get_pbr_material_info();

    vec4 color_to_set = vec4(0.0);
    vec3 indirect_illumination = vec3(0.0);

    float fog_strength = get_fog_strength(pbr_material_info.world_position);

    if(environment_ubo.show_normals != 0) {
        if(pbr_material_info.normal.z == -1) {
            color_out = vec4(0, 0, 0, 1);
        } else {
            color_out = vec4(pbr_material_info.normal.xyz, 1);
        }

        return;
    }

    #ifndef DEFERRED_MODE
    Instance instance = instance_ubo.instances[int(from_vertex.instance_index)];
    Material material = mat_ubo.materials[instance.material_index];

    if(material.object_type == OBJECT_TYPE_LIGHT) {
        color_out = vec4(material.light_box_color, 1.0f);
        return;
    }
    #endif

    if(environment_ubo.fog.fog_active != 0 && fog_strength == 1) {
        color_out = vec4(environment_ubo.fog.fog_color, 1.0f);
        return;
    }

    for(int i = 0; environment_ubo.num_of_directional_lights > i; ++i) {
        if(environment_ubo.directional_lights[i].light_active != 0) {
            if((environment_ubo.gi_mode & DIRECT_DIRECTIONAL_ENABLED) != 0) {
                color_to_set += directional_light_output(pbr_material_info, i);
            }

            if((environment_ubo.gi_mode & RSM_DIRECTIONAL_MODE) != 0) { 
                indirect_illumination += pbr_material_info.ao * rsm_irradiance_directional(vec4(pbr_material_info.world_position, 1.0f), pbr_material_info.normal, i);
            }
        }
    }

    for(int i = 0; environment_ubo.num_of_point_lights > i; ++i) {
        if(environment_ubo.point_lights[i].light_active != 0) {
            if((environment_ubo.gi_mode & DIRECT_POINT_ENABLED) != 0) {
                color_to_set += point_light_output(i, pbr_material_info);
            }

            if((environment_ubo.gi_mode & RSM_POINT_MODE) != 0) {
                indirect_illumination += pbr_material_info.ao * rsm_irradiance_point(vec4(pbr_material_info.world_position, 1.0f), pbr_material_info.normal, i);
            }
        }
    }

    indirect_illumination *= environment_ubo.rsm_weighting;
    color_to_set.rgb += indirect_illumination;

    float ssao_term = (environment_ubo.gi_mode & SSAO_ENABLED) != 0 ? pbr_material_info.ao : 1; 
    color_to_set += vec4(pbr_material_info.albedo.rgb * 0.025 * ssao_term, 0); 

    if((environment_ubo.gi_mode & IBL_ENABLED) != 0) {
        color_to_set.rgb += 0.025 * get_ibl_result(pbr_material_info);
    }

    // HDR->LDR + gamma correction
    color_to_set.rgb = color_to_set.rgb / (color_to_set.rgb + vec3(1.0));
    color_to_set.rgb = pow(color_to_set.rgb, vec3(1.0/2.2));

    if(environment_ubo.fog.fog_active != 0) {
        float fog_strength = get_fog_strength(pbr_material_info.world_position);

        //color_out = (vec4(color_to_set, 1.0) * (1.0f - fog_strength)) + (environment_ubo.fog.fog_color * fog_strength);
        color_out = mix(color_to_set, vec4(environment_ubo.fog.fog_color, color_to_set.a), fog_strength);
    } else {
        color_out = color_to_set;
    }
}
