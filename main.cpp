#include <filesystem>
#include <iostream>

#include "aristaeus/core/scene.hpp"

int main(int argc, char** argv) {
    std::cout << "Current path is " << std::filesystem::current_path() << "\n";
    try {
        aristaeus::core::Scene vulkan_scene{ argc, argv };

        vulkan_scene.render();
    }
    catch (std::exception& e) {
        std::cout << e.what() << std::endl;
    }
}
