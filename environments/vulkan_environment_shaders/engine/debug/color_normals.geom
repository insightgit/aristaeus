#version 460

layout (triangles) in;
layout (line_strip, max_vertices = 18) out;

layout (set = 0, binding = 0) uniform PerspectiveCameraUBO {
    mat4 projection;
    mat4 view;
} camera_ubo;

layout (location = 0) in VertexOut {
    vec4 diffuse_color_vector;
    vec4 specular_color_vector;
    vec3 normal_vector;
    mat3 tbn_matrix;
    vec2 texture_coords_out;
    vec3 world_position;
    float vertex_height;

    vec2 height_map_texture_coords;
} geom_in[];

layout (location = 0) out GeomOut {
    int line_type;
} geom_out;

void main() {
    for(int i = 0; 3 > i; ++i) {
        vec4 pos = gl_in[i].gl_Position;
        //mat3 tbn_matrix = geom_in[i].tbn_matrix;
        //vec3 tangent = vec3(tbn_matrix[0][0], tbn_matrix[1][0], tbn_matrix[2][0]);
        //vec3 bittangent = vec3(tbn_matrix[0][1], tbn_matrix[1][1], tbn_matrix[2][1]);
        //vec3 normal = vec3(tbn_matrix[0][2], tbn_matrix[1][2], tbn_matrix[2][2]);

        gl_Position = camera_ubo.projection * camera_ubo.view * pos;
        geom_out.line_type = 0;
        EmitVertex();

        gl_Position = camera_ubo.projection * camera_ubo.view * (pos + vec4(normalize(geom_in[i].normal_vector) * 0.01, 0.0));
        geom_out.line_type = 0;
        EmitVertex();

        EndPrimitive();

        gl_Position = camera_ubo.projection * camera_ubo.view * pos;
        geom_out.line_type = 1;
        EmitVertex();

        /*gl_Position = camera_ubo.projection * camera_ubo.view * (pos + vec4(tangent, 0.0));
        geom_out.line_type = 1;
        EmitVertex();

        EndPrimitive();

        gl_Position = camera_ubo.projection * camera_ubo.view * pos;
        geom_out.line_type = 2;
        EmitVertex();

        gl_Position = camera_ubo.projection * camera_ubo.view * (pos + vec4(bittangent, 0.0));
        geom_out.line_type = 2;
        EmitVertex();

        EndPrimitive();*/
    }
}
