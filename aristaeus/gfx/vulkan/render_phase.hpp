#ifndef ARISTAEUS_RENDER_PHASE_HPP
#define ARISTAEUS_RENDER_PHASE_HPP

#include <concepts>
#include <map>

#include "image_buffer_raii.hpp"
#include "window.hpp"

#include "../../utils.hpp"

namespace aristaeus::gfx::vulkan {
    // one big question: how do we notify classes which use
    // RenderContextIDs when the IDs they expect to be using
    // have changed?
    static constexpr uint32_t RENDER_CONTEXT_ID_PRELOAD = ~0;

    typedef uint32_t RenderContextID;
    typedef uint32_t RenderPhaseID;

    class RenderPhase {
    public:
        struct SubpassInfo {
            std::vector<vk::SubpassDescription> descriptions;
            std::vector<vk::SubpassDependency> dependencies;
        };

        struct AttachmentInfo {
            vk::AttachmentDescription description;
            vk::ImageUsageFlags usage_flags;
            std::vector<SharedImageRaii> existing_image;

            bool operator==(const AttachmentInfo &other) const noexcept {
                return description == other.description && usage_flags == other.usage_flags && 
                       existing_image == other.existing_image;
            }
        };

        // TODO(Bobby): refactor these void* ptrs out with templates
        struct RenderPassInfo {
            std::optional<std::function<void(void*)>> after_function;
            void *after_function_arg;
            std::optional<std::function<void(void*)>> before_function;
            void *before_function_arg;

            std::vector<vk::ClearValue> clear_values;
            std::optional<std::function<void(void *)>> contextless_draw_function; 
                                                // this function is different from draw_function
                                                // in that when we run it we don't know which context
                                                // we are rendering in, so we can't do any commands
                                                // but we can add nodes to various manager classes.
            void *contextless_draw_function_arg;
            std::function<void(void*)> draw_function;
            void *draw_function_arg;
            std::string name;
            bool one_shot;
            bool fire_one_shot_on_creation;
            std::unordered_map<std::string, vk::PipelineStageFlags> phase_dependents;
            std::unordered_map<std::string, vk::PipelineStageFlags> phase_dependencies;
            std::vector<AttachmentInfo> render_pass_attachments;
            std::optional<glm::uvec2> resolution; // if no value, will use the current resolution of the swap chain
            std::vector<glm::uvec2> per_context_resolutions; // overrides resolution when # of elements >= num_of_render_contexts
                                                             // allows for different resolution render pass contexts.
            std::optional<SubpassInfo> subpass_info;
            std::vector<vk::Image> swap_chain_images;
            size_t num_of_render_contexts;
            std::optional<vk::Viewport> viewport; // if no value, will use a default viewport of the entire screen
            std::optional<vk::Rect2D> viewport_scissor; // see above
            bool is_cube_map;

            bool show_attachments_in_imgui_window;
        };

        RenderPhase(const std::vector<RenderPassInfo> &render_passes, const std::string &render_phase_name);

        template<typename T>
        void set_after_callback(const std::function<void(T*)> &callback, T *arg) {
            m_after_phase_function_calls.push_back([callback](void *arg) {
                callback(reinterpret_cast<T*>(arg));
            });
            m_after_function_args.push_back(arg);
        }

        template<typename T>
        void set_before_callback(const std::function<void(T*)> &callback, T *arg) {
            m_before_phase_function_calls.push_back([callback](void *arg) {
                callback(reinterpret_cast<T*>(arg));
            });
            m_before_function_args.push_back(arg);
        }

        void reset_after_callback() {
            m_after_phase_function_calls.clear();
            m_after_function_args.clear();
        }

        void reset_before_callback() {
            m_before_phase_function_calls.clear();
            m_before_function_args.clear();
        }

        [[nodiscard]] std::string get_render_phase_name() const {
            return m_render_phase_name;
        }

        // if there's no RenderPhaseID, then this RenderPhase hasn't been assigned one
        // (which should happen after it's added/created by CoreRenderer)
        [[nodiscard]] std::optional<RenderPhaseID> get_render_phase_id() const {
            return m_phase_id;
        }

        [[nodiscard]] vk::raii::Semaphore &get_signal_semaphore(uint32_t current_frame) {
            return m_signal_semaphores[current_frame];
        }

        [[nodiscard]] std::unordered_map<std::string, vk::PipelineStageFlags> get_dependencies() const {
            return m_dependencies;
        }

        [[nodiscard]] std::unordered_map<std::string, vk::PipelineStageFlags> get_dependents() const {
            return m_dependents;
        }

        [[nodiscard]] size_t get_total_num_of_render_contexts_last_rendered() const {
            return m_num_of_render_contexts_last_rendered;
        }

        [[nodiscard]] size_t get_num_of_render_contexts() const {
            return m_num_of_render_contexts;
        }

        [[nodiscard]] size_t get_num_of_render_passes() const {
            return m_render_pass_data.size();
        }

        [[nodiscard]] RenderContextID get_render_pass_starting_render_context_id(const std::string &render_pass_name) const;

        [[nodiscard]] RenderContextID get_render_phase_starting_render_context_id() const {
            return m_render_context_id;
        }

        [[nodiscard]] std::string get_current_render_pass() const {
            return m_current_render_pass;
        }

        [[nodiscard]] bool in_vulkan_render_pass_scope() const {
            return m_vulkan_render_pass_scope;
        }

        [[nodiscard]] bool uses_swap_chain(const std::string &render_pass_name) const {
            return !m_render_pass_data.at(render_pass_name).info.swap_chain_images.empty();
        }

        [[nodiscard]] bool is_render_pass_depth_only(const std::string &render_pass_name) const {
            const RenderPassInfo &render_pass_info = m_render_pass_data.at(render_pass_name).info;

            return render_pass_info.render_pass_attachments.size() == 1 && 
                   (render_pass_info.render_pass_attachments[0].usage_flags & vk::ImageUsageFlagBits::eDepthStencilAttachment);
        }

        [[nodiscard]] bool is_render_pass_in_vulkan_render_pass_scope(const std::string &render_pass_name) const {
            return m_render_pass_data.at(render_pass_name).resources.has_value();
        }

        [[nodiscard]] vk::Viewport get_current_viewport() const {
            return m_render_pass_data.at(get_current_render_pass()).info.viewport.value();
        }

        [[nodiscard]] vk::Rect2D get_current_viewport_scissor() const {
            return m_render_pass_data.at(get_current_render_pass()).info.viewport_scissor.value();
        }

        [[nodiscard]] AttachmentInfo get_attachment_info(const std::string &render_pass_name, size_t attachment_index) {
            return m_render_pass_data.at(render_pass_name).info.render_pass_attachments[attachment_index];
        }

        [[nodiscard]] uint32_t get_render_pass_frame_num(const std::string &render_pass_name) const {
            return m_render_pass_data.at(render_pass_name).info.swap_chain_images.empty() ? utils::vulkan::MAX_FRAMES_IN_FLIGHT : m_render_pass_data.at(render_pass_name).info.swap_chain_images.size();
        }

        [[nodiscard]] std::optional<size_t> get_render_pass_one_shot_last_rendered_frame_num(const std::string &render_pass_name) const {
            return m_render_pass_data.at(render_pass_name).info.one_shot ? m_render_pass_data.at(render_pass_name).one_shot_render_pass_fired_frame : 
                   std::optional<size_t>{};
        }

        [[nodiscard]] size_t get_num_of_render_contexts_last_rendered() const {
            return m_num_of_render_contexts_last_rendered;
        }

        [[nodiscard]] glm::uvec2 get_current_resolution() const;

        void retoggle_one_shot_render_pass(const std::string &render_pass_name);

        // NOTE: update_attachment will recreate the render pass
        void update_attachments(const std::string &render_pass_name, const std::vector<AttachmentInfo> &attachment_infos, const std::vector<size_t> &attachment_indices);

        [[nodiscard]] std::optional<vk::CommandBufferInheritanceInfo> get_current_cmd_inheritance_info();
        [[nodiscard]] std::optional<vk::CommandBufferInheritanceInfo> get_cmd_inheritance_info(RenderContextID render_context_id);

        [[nodiscard]] SharedImageRaii &get_render_pass_image(const std::string &render_pass_name, uint32_t current_frame);
        [[nodiscard]] vk::raii::ImageView &get_render_pass_image_view(const std::string &render_pass_name, uint32_t current_frame);
        [[nodiscard]] vk::raii::RenderPass &get_render_pass(const std::string &render_pass_name);

        [[nodiscard]] vk::raii::Sampler &get_default_sampler() {
            return m_default_sampler;
        }

        [[nodiscard]] bool in_contextless_draw() const {
            return m_contextless_draw;
        }
 
        bool add_render_pass(RenderPassInfo render_pass_info, bool add_dependencies,
                             const std::unordered_map<std::string, vk::PipelineStageFlags> &child_dependents);

        std::optional<vk::SubmitInfo> submit_render_phase(const std::vector<std::pair<vk::Semaphore, vk::PipelineStageFlags>> &additional_wait_semaphores = {});

        void contextless_draw();
        void imgui_draw(vk::CommandBuffer &command_buffer);

        void add_draw_next_frame(const std::function<void()> &draw_function);

        void on_swap_chain_recreation(const std::vector<vk::Image> &new_swap_chain_images);
    private:
        struct VulkanRenderPassResources {
            // TODO(Bobby): look into possible memory aliasing here with vk::raii::Image
            vk::raii::RenderPass render_pass;
            std::vector<SharedImageRaii> images;
            std::vector<vk::raii::ImageView> image_views;
            std::vector<vk::raii::Framebuffer> frame_buffers;
            size_t image_image_views_stride;

            std::vector<ImTextureID> texture_ids;
        };

        struct RenderPassData {
            RenderPassInfo info;
            std::optional<size_t> one_shot_render_pass_fired_frame;
            bool one_shot_contextless_draw_called;
            std::optional<VulkanRenderPassResources> resources;
        };

        struct DontCareImageInfo {
            AttachmentInfo attachment_info;
            glm::uvec2 image_resolution;

            bool operator==(const DontCareImageInfo &other) const noexcept {
                return other.attachment_info == attachment_info && other.image_resolution == image_resolution;
            }
        };

        struct DontCareImageInfoHasher {
            size_t operator()(const DontCareImageInfo &dont_care_info) const noexcept;
        };

        // get all dependents/dependencies, not just our immediate ones
        std::unordered_map<std::string, vk::PipelineStageFlags> get_global_dependents();
        std::unordered_map<std::string, vk::PipelineStageFlags> get_global_dependencies();
        
        friend class CoreRenderer;
        friend class RenderPhaseDependencyNode;

        // order matters here because of render context ids
        std::map<std::string, RenderPassData> m_render_pass_data;

        std::unordered_map<std::string, vk::PipelineStageFlags> m_dependencies;
        std::unordered_map<std::string, vk::PipelineStageFlags> m_dependents;

        std::array<vk::raii::Semaphore, utils::vulkan::MAX_FRAMES_IN_FLIGHT> m_signal_semaphores;
        std::string m_render_phase_name;

        std::string m_current_render_pass;

        RenderContextID m_render_context_id = 0;
        size_t m_num_of_render_contexts = 0;
        size_t m_num_of_render_contexts_last_rendered = 0;
        bool m_vulkan_render_pass_scope = false;

        vk::raii::Sampler m_default_sampler;

        [[nodiscard]] SubpassInfo get_default_subpass_info(const RenderPassData &render_pass_data);

        std::optional<RenderPhaseID> m_phase_id;

        std::vector<std::function<void(void*)>> m_before_phase_function_calls;
        std::vector<void*> m_before_function_args;
        std::vector<std::function<void(void*)>> m_after_phase_function_calls;
        std::vector<void*> m_after_function_args;

        std::unordered_map<DontCareImageInfo, SharedImageRaii, DontCareImageInfoHasher> m_dont_care_images;
        std::unordered_map<DontCareImageInfo, vk::raii::ImageView, DontCareImageInfoHasher> m_dont_care_image_views;

        std::unordered_map<std::string, std::vector<vk::AttachmentReference>> m_default_subpass_info_color_attachment_refs;
        std::unordered_map<std::string, vk::AttachmentReference> m_default_subpass_info_depth_stencil_attachment_ref;

        std::unordered_set<std::string> m_one_shot_render_passes_fired_last_frame;

        std::unordered_map<RenderContextID, std::vector<std::function<void()>>> m_draw_next_frame_funcs;

        struct SubmitData {
            std::vector<vk::Semaphore> wait_semaphores;
            std::vector<vk::PipelineStageFlags> wait_stage_masks;
            vk::CommandBuffer primary_command_buffer;
            vk::Semaphore signal_semaphore;

            void reset() {
                wait_semaphores.clear();
                wait_stage_masks.clear();
            }
        };

        std::array<SubmitData, utils::vulkan::MAX_FRAMES_IN_FLIGHT> m_submit_datas;

        bool m_render_phase_active = false;
        bool m_contextless_draw = false;

        void create_render_pass_attachments(RenderPassData &render_pass_data);
        void create_render_pass_attachment(RenderPassData &render_pass_data, const glm::ivec2 &image_resolution, 
                                           uint32_t num_of_render_contexts, const std::string &image_category, 
                                           vk::ImageAspectFlags image_aspect_flags, const AttachmentInfo &attachment_info,
                                           bool count_into_stride, int frame);

        void create_render_pass_attachments_for_context_resolution(RenderPassData &render_pass_data, int frame, 
                                                                   bool count_into_stride, const glm::uvec2 &context_resolution,
                                                                   uint32_t num_of_render_contexts, bool &used_swap_chain_image);
    };
}
#endif