//
// Created by bobby on 21/04/23.
//

#ifndef ARISTAEUS_WAVE_PROPERTIES_HPP
#define ARISTAEUS_WAVE_PROPERTIES_HPP

namespace aristaeus::gfx::vulkan {
    struct WaveProperties {
        int count;
        float median_wavelength;
        float median_amplitude;
        float median_sharpness;
        glm::vec2 direction;
        float t_spawn_interval;
    };
}

#endif //ARISTAEUS_WAVE_PROPERTIES_HPP
