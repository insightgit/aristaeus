//
// Created by bobby on 8/25/21.
//

#ifndef ARISTAEUS_ASSIMP_MESH_HPP
#define ARISTAEUS_ASSIMP_MESH_HPP

#include <algorithm>
#include <string>
#include <vector>

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/glm.hpp>
#include <glm/gtx/string_cast.hpp>
#include <glm/gtx/matrix_decompose.hpp>
#include <glm/gtx/transform.hpp>

#include "mesh_manager.hpp"
#include "model_info.hpp"
#include "render_phase.hpp"
#include "texture.hpp"
#include "uniform_buffer.hpp"
#include "../../utils.hpp"

#include "../scene_graph_node.hpp"

namespace aristaeus::gfx::vulkan {
    class AssimpMesh : public gfx::SceneGraphNode {
    public:
        struct MeshGeometryData {
            std::vector<glm::vec3> vertices;
            std::vector<uint32_t> indices;
        };

        AssimpMesh(const std::vector<glm::vec3> &vertices, const std::vector<uint32_t> &indices,
                   const std::string &mesh_generation_info, bool remain_unmerged = false);
        AssimpMesh(MeshID mesh_id);

        [[nodiscard]] MeshGeometryData get_mesh_geometry_data() const;

        void assign_material_id(MaterialID material_id, RenderContextID render_context_id);

        void predraw_load(const vk::CommandBuffer &command_buffer, const glm::vec3 &camera_position) override {};
        void draw()override;
    private:
        MeshManager::LoadRequest m_mesh_manager_load_request;

        std::optional<MeshID> m_mesh_id;
        std::optional<InstanceID> m_instance_id;
        std::unordered_map<RenderContextID, MaterialID> m_assigned_materials;
    };
}

#endif //ARISTAEUS_ASSIMP_MESH_HPP
