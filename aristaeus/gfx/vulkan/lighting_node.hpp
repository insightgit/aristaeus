#ifndef ARISTAEUS_LIGHTING_NODE
#define ARISTAEUS_LIGHTING_NODE

#include "../scene_graph_node.hpp"

namespace aristaeus::gfx::vulkan {
	class LightingNode : public gfx::SceneGraphNode {
    ARISTAEUS_CLASS_HEADER_LEAF("lighting_node", LightingNode)
	public:
        static constexpr std::string_view LIGHTING_NODE_GROUP = "_lighting_node";

        struct LightNodeUBOData {
            alignas(4) float constant_attenuation;
            alignas(4) float linear_attenuation;
            alignas(4) float quadratic_attenuation;

            alignas(4) int light_active;
            alignas(4) int light_type;

            alignas(16) glm::vec3 diffuse;

            alignas(16) glm::vec3 position_direction;
        };

        LightNodeUBOData get_light_node_ubo_data() const {
            return m_light_node_ubo_data;
        }
    protected:
        LightNodeUBOData m_light_node_ubo_data;
    private:
        void on_parsing_done();
	};
}

#endif