#version 450

#extension GL_EXT_nonuniform_qualifier : enable

struct PBRMaterialInfo {
    float roughness;
    float metallic;
    float ao;
    int using_gltf_metallicroughness_texture;

    ivec2 albedo_map_range;
    ivec2 normal_map_range;
    ivec2 metallic_map_range;
    ivec2 roughness_map_range;
    ivec2 ao_map_range;

    vec4 albedo;
    vec3 normal;
    vec3 world_position;
    vec3 tangent;
};

struct Material {
    int override_texture;
    int object_type;
    vec3 light_box_color;

    PBRMaterialInfo pbr_material_info;
};

layout (location = 0) in VertexOut {
    vec3 normal_vector;
    mat3 tbn_matrix;
    vec2 texture_coords_out;
    vec3 world_position;

    float instance_index;
} from_vertex;

const int RSM_DATA_DIRECTIONAL_LIGHT_MODE = 1;
const int RSM_DATA_POINT_LIGHT_MODE = 2;

layout (location = 0) out vec4 position_out;
layout (location = 1) out vec4 normal_out;
layout (location = 2) out vec4 albedo_out;

layout (set = 0, binding = 0) uniform PerspectiveCameraUBO {
    mat4 projection;
    mat4 view;
    mat4 light_space_matrix;
    vec3 camera_front;
    vec3 camera_up;
    vec3 camera_right;
    vec3 camera_position;
    float camera_far_plane;
} camera_ubo;

layout (std430, set = 2, binding = 0) readonly buffer MaterialUBO {
    Material materials[];
} mat_ubo;

const int MATERIAL_SIZE = 10;

// TODO(Bobby): Convert this into a sampler array
layout(set = 2, binding = 1) uniform sampler2D texture_albedo[];
layout(set = 2, binding = 2) uniform sampler2D texture_normal[];
layout(set = 2, binding = 3) uniform sampler2D texture_metallic[];
layout(set = 2, binding = 4) uniform sampler2D texture_roughness[];
layout(set = 2, binding = 5) uniform sampler2D texture_ao[];

struct Instance {
    mat4 model;
    mat3 normal_transform;
    uint material_index;
};

layout (std430, set = 3, binding = 0) readonly buffer InstanceUBO {
    Instance instances[];
} instance_ubo;

layout(push_constant) uniform RSMData {
    vec3 point_light_pos;
    vec3 light_color;
    int light_mode;
    int linear_depth_scaling;
} rsm_data;

// source: https://theorangeduck.com/page/avoiding-shader-conditionals
float when_greater_than(float x, float y) {
    return max(sign(x - y), 0.0);
}

float when_less_than(float x, float y) {
    return max(sign(y - x), 0.0);
}

float when_less_equal_than(float x, float y) {
    return 1.0 - when_greater_than(x, y);
}

float when_greater_equal_than(float x, float y) {
    return 1.0 - when_less_than(x, y);
}

float when_equal_to(float x, float y) {
    return 1.0 - abs(sign(x - y));
}

float when_not_equal_to(float x, float y) {
    return 1.0 - when_equal_to(x, y);
}

float when_less_equal_to(float x, float y) {
    return 1.0 - when_greater_than(x, y);
}

float or(float x, float y) {
    return min(x + y, 1.0);
}

PBRMaterialInfo get_pbr_material_info() {
    int instance_index = int(round(from_vertex.instance_index));

    Instance instance = instance_ubo.instances[instance_index];
    Material material = mat_ubo.materials[instance.material_index];

    PBRMaterialInfo pbr_material_info = material.pbr_material_info;

    if(material.override_texture != 0) {
        if(material.pbr_material_info.normal.x <= 0.01 && material.pbr_material_info.normal.y <= 0.01 && material.pbr_material_info.normal.z <= 0.01) {
            pbr_material_info.normal = from_vertex.normal_vector;
        }

        return pbr_material_info;
    }
    
    for(int i = material.pbr_material_info.albedo_map_range.x; material.pbr_material_info.albedo_map_range.x + material.pbr_material_info.albedo_map_range.y > i; ++i) {
        pbr_material_info.albedo += texture(texture_albedo[i], from_vertex.texture_coords_out);
    }

    if(material.pbr_material_info.normal_map_range.y != 0) {
        // normal mapping is enabled
        vec3 normal_map_tangent_space = vec3(0.0f);

        for(int i = material.pbr_material_info.normal_map_range.x; material.pbr_material_info.normal_map_range.x + material.pbr_material_info.normal_map_range.y > i; ++i) {
            normal_map_tangent_space += texture(texture_normal[i], from_vertex.texture_coords_out).xyz;
        }
        normal_map_tangent_space = (normal_map_tangent_space * 2.0f) - 1.0f;

        vec3 world_space_normal = vec3(from_vertex.tbn_matrix * normal_map_tangent_space);

        pbr_material_info.normal = normalize(world_space_normal);
    } else {
        pbr_material_info.normal = from_vertex.normal_vector;
    } 

    return pbr_material_info;
}

void main() {
    PBRMaterialInfo pbr_material_info = get_pbr_material_info();

    albedo_out = vec4(pbr_material_info.albedo.rgb, 1.0);
    normal_out = vec4(pbr_material_info.normal, 0);
    position_out = vec4(from_vertex.world_position, 1.0);

    albedo_out *= vec4(rsm_data.light_color, 1);

    if(rsm_data.light_mode == RSM_DATA_POINT_LIGHT_MODE) {
        // weight albedo/radiant flux based on inverse square attenuation
        float point_light_distance = length(rsm_data.point_light_pos - position_out.xyz);
        float inverse_square_attenuation = 1 / (1 + (0.1 * point_light_distance) + (0.01 * point_light_distance * point_light_distance));
        albedo_out *= inverse_square_attenuation;
    }

    albedo_out.rgb = albedo_out.rgb / (albedo_out.rgb + vec3(1.0));

    if(rsm_data.linear_depth_scaling != 0) {
        vec3 light_distance_vector = from_vertex.world_position - camera_ubo.camera_position;

        float distance = min(length(light_distance_vector) / 100.0f, 1.0f);

        gl_FragDepth = distance;
    } else {
        gl_FragDepth = gl_FragCoord.z;
    }
}
