//
// Created by bobby on 12/30/21.
//

#include <gfx/vulkan/lighting_environment.hpp>

#include "gpu_perlin_noise_terrain.hpp"

aristaeus::gfx::vulkan::compute::GPUPerlinNoiseTerrain::GPUPerlinNoiseTerrain(
    float height_magnitude, float vertex_resolution, GPUPerlinNoise &height_map_texture_job,
    const std::map<glm::vec4, std::vector<Image>, utils::Vec4Hash> &terrain_texture_paths_height,
    int number_of_textures, ObjectType object_type, PropInfo &prop_info, const std::string &node_name,
    const std::unordered_map<Image, glm::vec4> &terrain_texture_paths_slope,
    uint32_t number_of_patches, std::optional<vk::CommandBuffer> create_command_buffer) :
    Terrain(height_magnitude, vertex_resolution, height_map_texture_job.get_perlin_noise_info().resolution,
            terrain_texture_paths_height, number_of_textures, object_type,
            prop_info, node_name, terrain_texture_paths_slope, number_of_patches, create_command_buffer)
{
    //m_height_map_texture_job = std::move(height_map_texture_job);
}

void aristaeus::gfx::vulkan::compute::GPUPerlinNoiseTerrain::predraw_load(const vk::CommandBuffer &command_buffer,
                                                                          const glm::vec3 &camera_position) {
    std::optional<Texture> height_map_texture = m_height_map_texture_job->get_output_info();
    if (height_map_texture.has_value()) {
        set_height_map_texture(std::make_shared<Texture>(std::move(height_map_texture.value())));

        get_height_map_texture()->transition_post_compute(command_buffer);
    }
}