#include "core_renderer.hpp"

#include "mesh_manager.hpp"

#include "assimp_mesh.hpp"

#include "../libraries/include/MurmurHash2.h"

template<>
aristaeus::gfx::vulkan::MeshManager *aristaeus::core::Singleton<aristaeus::gfx::vulkan::MeshManager>::instance = nullptr;

aristaeus::gfx::vulkan::MeshManager::LoadResponse aristaeus::gfx::vulkan::MeshManager::load_mesh(const LoadRequest &load_request) {
	std::optional<MeshID> mesh_id;

	if (m_mesh_id_generation_infos.contains(load_request.mesh_generation_info)) {
		mesh_id = m_mesh_id_generation_infos.at(load_request.mesh_generation_info);
	}
	else {
		std::vector<uint32_t> data;
		size_t vertex_end_range = ((sizeof(glm::vec3) / sizeof(uint32_t)) * load_request.vertices.size());

		data.resize(vertex_end_range + load_request.indices.size());

		#ifndef NDEBUG
		for (uint32_t index : load_request.indices) {
			assert(index < load_request.vertices.size());
		}
		#endif

		std::memcpy(data.data(), load_request.vertices.data(), sizeof(glm::vec3) * load_request.vertices.size());
		std::memcpy(&data[vertex_end_range], load_request.indices.data(), sizeof(uint32_t) * load_request.indices.size());

		uint64_t mesh_hash = MurmurHash64A(data.data(), data.size() * sizeof(uint32_t), rand());

		if (m_mesh_id_hashes.contains(mesh_hash)) {
			MeshID hashed_mesh_id = m_mesh_id_hashes.at(mesh_hash);
			const MeshData &mesh_data = m_mesh_data.at(hashed_mesh_id);

			if (load_request.vertices == mesh_data.vertices && load_request.indices == mesh_data.indices) {
				mesh_id = hashed_mesh_id;
			}
		}
		
		if(!mesh_id.has_value()) {
			mesh_id = m_current_mesh_id++;

			m_mesh_data.insert({ mesh_id.value(), MeshData{load_request.indices, load_request.vertices}});

			m_mesh_id_hashes.insert({ mesh_hash, mesh_id.value()});
		}

		m_mesh_id_generation_infos.insert({ load_request.mesh_generation_info, mesh_id.value()});
	}

	InstanceID instance_id = create_instance(mesh_id.value(), load_request.assimp_mesh_in_tree);

	return LoadResponse{
		.mesh_id = mesh_id.value(),
		.instance_id = instance_id
	};
}

aristaeus::gfx::vulkan::InstanceID aristaeus::gfx::vulkan::MeshManager::create_instance(MeshID mesh_id, 
																						std::weak_ptr<AssimpMesh> assimp_mesh_in_tree) {
	auto &class_db = utils::get_singleton_safe<core::ClassDB>();

	std::shared_ptr<AssimpMesh> assimp_mesh_in_tree_strong = assimp_mesh_in_tree.lock();
	assert(assimp_mesh_in_tree_strong != nullptr);

	InstanceID instance_id = m_current_instance_id++;
	InstanceData instance_data{
		.transform_event_handle = "transform-event-" + std::to_string(instance_id),
		.parent_transform_event_handle = "parent-transform-event-" + std::to_string(instance_id),
		.assimp_mesh = assimp_mesh_in_tree_strong
	};

	assimp_mesh_in_tree_strong->add_callback_for_event<SceneGraphNode::Transform, MeshManager>(
		std::string{ SceneGraphNode::TRANSFORM_CHANGED_EVENT }, instance_data.transform_event_handle,
		[instance_id](MeshManager &mesh_manager, SceneGraphNode::Transform) {
			mesh_manager.m_dirty_instances.insert(instance_id);
	}, *this);

	assimp_mesh_in_tree_strong->add_callback_for_event<void*, MeshManager>(
		std::string{ SceneGraphNode::GLOBAL_TRANSFORM_CHANGED_EVENT }, instance_data.parent_transform_event_handle,
		[instance_id](MeshManager &mesh_manager, void*) {
			mesh_manager.m_dirty_instances.insert(instance_id);
	}, *this);

	m_dirty_instances.insert(instance_id);
	m_instance_data.insert({instance_id, instance_data});
	m_instances_to_mesh_ids.insert({instance_id, mesh_id});

	return instance_id;
}

void aristaeus::gfx::vulkan::MeshManager::add_instance_to_draw_list(RenderContextID render_context_id, InstanceID instance_id, 
																	MaterialID material_id) {
	RenderObject render_object{ .instance = instance_id, .material = material_id };

	render_object.mesh = m_instances_to_mesh_ids.at(instance_id);

	if (m_render_object_set.contains(render_object)) {
		return;
	}

	if(!m_new_objects_to_render[render_context_id].contains(render_object)) {
		m_new_objects_to_render[render_context_id].insert(render_object);
	}

	if (!m_inited_materials.contains(material_id)) {
		m_queued_materials.insert(material_id);
	}

	m_instance_data.at(instance_id).associated_material_ids.insert({ render_context_id, material_id });

	m_render_context_ids_registered.insert(render_context_id);
	m_render_object_set.insert(render_object);
}

void aristaeus::gfx::vulkan::MeshManager::remove_instance_from_draw_list(RenderContextID render_context_id, InstanceID instance_id, 
																		 MaterialID material_id) {
	RenderObject render_object{ .instance = instance_id, .material = material_id };

	render_object.mesh = m_instances_to_mesh_ids.at(instance_id);

	if (!m_old_objects_to_remove[render_context_id].contains(render_object)) {
		m_old_objects_to_remove[render_context_id].insert(render_object);
	}
}

void aristaeus::gfx::vulkan::MeshManager::refresh_dirty_instances(RenderContextID render_context_id, bool refresh) {
	void *buffer_data;
	auto &core_renderer = utils::get_singleton_safe<CoreRenderer>();
	auto &material_manager = utils::get_singleton_safe<MaterialManager>();
	std::vector<InstanceID> instance_id_refreshes;

	if (m_render_objects.at(render_context_id).empty()) {
		return;
	}

	for (const RenderObject &render_object : m_render_objects.at(render_context_id)) {
		if (refresh || m_dirty_instances.contains(render_object.instance)) {
			instance_id_refreshes.push_back(render_object.instance);
		}
	}

	vmaMapMemory(m_mesh_instance_buffer[render_context_id]->allocator, m_mesh_instance_buffer[render_context_id]->allocation, 
				 &buffer_data);

	char *buffer_data_bytes = reinterpret_cast<char *>(buffer_data);

	for(InstanceID instance_id : instance_id_refreshes) {
		InstanceData &instance_data = m_instance_data.at(instance_id);

		std::shared_ptr<AssimpMesh> assimp_mesh = instance_data.assimp_mesh.lock();

		if (assimp_mesh == nullptr) {
			// TODO(Bobby): remove mesh if mesh is null
			continue;
		}

		static_assert(offsetof(MeshUBO, local_material_id) == 112);

		MaterialID associated_material_id = instance_data.associated_material_ids.at(render_context_id);
		MaterialManager::LocalMaterialID local_material_id = material_manager.get_local_material_id_within_super_material(associated_material_id);

		SceneGraphNode::Transform mesh_transform = assimp_mesh->get_parent_transform() + assimp_mesh->get_transform();

		MeshUBO mesh_ubo{
			.model = assimp_mesh->get_global_model_matrix(),
			.normal_transform = glm::transpose(glm::inverse(assimp_mesh->get_global_model_matrix())),
			.local_material_id = local_material_id
		};

		size_t instance_buffer_offset = instance_data.instance_buffer_offset.at(render_context_id);

		std::memcpy(&buffer_data_bytes[instance_buffer_offset], &mesh_ubo, sizeof(MeshUBO));
	}

	vmaUnmapMemory(m_mesh_instance_buffer[render_context_id]->allocator, m_mesh_instance_buffer[render_context_id]->allocation);

	assert(m_mesh_instance_ssbo_set.contains(render_context_id) == m_mesh_instance_ssbo_pools.contains(render_context_id));

	if (!m_mesh_instance_ssbo_pools.contains(render_context_id)) {
		vk::DescriptorPoolSize pool_size{ vk::DescriptorType::eStorageBuffer, utils::vulkan::MAX_FRAMES_IN_FLIGHT };
		vk::DescriptorPoolCreateInfo pool_create_info{ { vk::DescriptorPoolCreateFlagBits::eFreeDescriptorSet }, utils::vulkan::MAX_FRAMES_IN_FLIGHT, pool_size };

		m_mesh_instance_ssbo_pools.insert({ render_context_id, vk::raii::DescriptorPool{core_renderer.get_logical_device_ref(), pool_create_info} });

		std::array<vk::DescriptorSetLayout, utils::vulkan::MAX_FRAMES_IN_FLIGHT> descriptor_set_layouts{ *m_mesh_instance_ssbo_set_layout, *m_mesh_instance_ssbo_set_layout };
		vk::DescriptorSetAllocateInfo set_allocate_info{ *m_mesh_instance_ssbo_pools.at(render_context_id),
														 descriptor_set_layouts };
		std::vector<vk::raii::DescriptorSet> new_sets_vector = core_renderer.get_logical_device_ref().allocateDescriptorSets(set_allocate_info);

		m_mesh_instance_ssbo_set.insert({ render_context_id, std::array<vk::raii::DescriptorSet, utils::vulkan::MAX_FRAMES_IN_FLIGHT>{std::move(new_sets_vector[0]), std::move(new_sets_vector[1])} });
	}

	vk::DescriptorBufferInfo buffer_info{ m_mesh_instance_buffer[render_context_id]->buffer, 0, m_mesh_instance_buffer[render_context_id]->buffer_size };
	vk::WriteDescriptorSet descriptor_set_write{ m_mesh_instance_ssbo_set.at(render_context_id)[core_renderer.get_current_frame()], 0, 0,
												 vk::DescriptorType::eStorageBuffer, {}, buffer_info };

	core_renderer.get_logical_device_ref().updateDescriptorSets(descriptor_set_write, {});
}

void aristaeus::gfx::vulkan::MeshManager::create_mesh_vertex_index_buffer(std::unordered_map<RenderContextID, vk::DeviceSize> &instance_buffer_sizes) {
	m_vertex_buffer_size = 0;
	vk::DeviceSize current_vertex_num = 0;

	vk::DeviceSize current_index_buffer_size = 0;
	std::unordered_set<MeshID> accounted_for_mesh_ids;

	for (RenderContextID render_context_id : m_render_context_ids_registered) {
		instance_buffer_sizes.insert({render_context_id, 0});

		for (const RenderObject &render_object : m_render_objects.at(render_context_id)) {
			if (!accounted_for_mesh_ids.contains(render_object.mesh)) {
				MeshData &mesh_data = m_mesh_data.at(render_object.mesh);
				size_t vertex_size = m_material_manager.get_vertex_binding_size(render_object.material) / sizeof(glm::vec3);

				if (mesh_data.unmerged_buffer == nullptr) {
					mesh_data.vertex_num_offset = current_vertex_num;
					mesh_data.vertex_buffer_offset = m_vertex_buffer_size;
					mesh_data.index_buffer_offset = current_index_buffer_size / sizeof(uint32_t);

					current_vertex_num += mesh_data.vertices.size() / vertex_size;
					m_vertex_buffer_size += sizeof(glm::vec3) * mesh_data.vertices.size();
					current_index_buffer_size += sizeof(uint32_t) * mesh_data.indices.size();
				}

				accounted_for_mesh_ids.insert(render_object.mesh);
			}

			InstanceData &instance_data = m_instance_data.at(render_object.instance);

			instance_data.instance_buffer_offset[render_context_id] = instance_buffer_sizes.at(render_context_id);

			instance_buffer_sizes.at(render_context_id) += sizeof(MeshUBO);
		}
	}

	auto &core_renderer = utils::get_singleton_safe<CoreRenderer>();
	MemoryManager &mem_manager = core_renderer.get_memory_manager();

	if (m_mesh_vertex_index_buffer == nullptr || m_mesh_vertex_index_buffer->buffer_size < m_vertex_buffer_size + current_index_buffer_size) {
		if (m_mesh_vertex_index_buffer != nullptr) {
			m_reaper_buffers[core_renderer.get_current_frame()].push_back(std::move(m_mesh_vertex_index_buffer));
		}

		m_mesh_vertex_index_buffer =
			mem_manager.create_buffer(core_renderer, m_vertex_buffer_size + current_index_buffer_size,
				vk::BufferUsageFlagBits::eIndexBuffer | vk::BufferUsageFlagBits::eVertexBuffer | vk::BufferUsageFlagBits::eTransferDst,
				vk::SharingMode::eExclusive, {}, VMA_MEMORY_USAGE_AUTO_PREFER_DEVICE,
				"shared-vertex-index-buffer");
	}

	BufferRaii staging_buffer = core_renderer.get_memory_manager().create_buffer(
		core_renderer, m_vertex_buffer_size + current_index_buffer_size,
		static_cast<vk::BufferUsageFlags>(vk::BufferUsageFlagBits::eTransferSrc),
		vk::SharingMode::eExclusive,
		VMA_ALLOCATION_CREATE_HOST_ACCESS_SEQUENTIAL_WRITE_BIT,
		VMA_MEMORY_USAGE_AUTO, "shared-mesh-vertex-staging-buffer");

	void *buffer_data;

	vmaMapMemory(staging_buffer->allocator, staging_buffer->allocation, &buffer_data);

	char *buffer_data_bytes = reinterpret_cast<char *>(buffer_data);

	uint32_t current_unused_vertex_base = 0;

	for (RenderContextID render_context_id : m_render_context_ids_registered) {
		for (const RenderObject &render_object : m_render_objects.at(render_context_id)) {
			MeshData &mesh_data = m_mesh_data.at(render_object.mesh);

			std::memcpy(&buffer_data_bytes[mesh_data.vertex_buffer_offset], mesh_data.vertices.data(),
				mesh_data.vertices.size() * sizeof(glm::vec3));
			std::memcpy(&buffer_data_bytes[m_vertex_buffer_size + (mesh_data.index_buffer_offset * sizeof(uint32_t))],
						mesh_data.indices.data(), mesh_data.indices.size() * sizeof(uint32_t));
		}
	}

	vmaUnmapMemory(staging_buffer->allocator, staging_buffer->allocation);

	core_renderer.copy_buffer(staging_buffer, m_mesh_vertex_index_buffer);
	core_renderer.transfer_staging_buffer_for_deletion(std::move(staging_buffer));
}

void aristaeus::gfx::vulkan::MeshManager::prepare_for_render() {
	auto &core_renderer = utils::get_singleton_safe<CoreRenderer>();
	MemoryManager &memory_manager = core_renderer.get_memory_manager();

	m_reaper_buffers[core_renderer.get_current_frame()].clear();

	if (m_mesh_instance_ssbo_set_layout == nullptr) {
		m_mesh_instance_ssbo_set_layout = std::make_unique<vk::raii::DescriptorSetLayout>(core_renderer.get_logical_device_ref(),
																						  vk::DescriptorSetLayoutCreateInfo{ {}, INSTANCE_STORAGE_BUFFER_BINDING });
	}

	if (m_render_context_ids_registered.empty()) {
		return;
	}

	bool update_needed = false;

	for (RenderContextID render_context_id : m_render_context_ids_registered) {
		std::vector<RenderObject> old_render_objects = m_render_objects[render_context_id];
		std::vector<RenderObject> &current_render_objects = m_render_objects.at(render_context_id);
		std::unordered_set<RenderObject, RenderObjectHasher> &new_render_objects_to_add = m_new_objects_to_render[render_context_id];
		std::unordered_set<RenderObject, RenderObjectHasher> &old_render_objects_to_remove = m_old_objects_to_remove[render_context_id];

		current_render_objects.insert(current_render_objects.end(), new_render_objects_to_add.begin(), new_render_objects_to_add.end());
		//current_render_objects.erase(std::remove(current_render_objects.begin(), current_render_objects.end(), old_render_objects_to_remove));

		new_render_objects_to_add.clear();
		old_render_objects_to_remove.clear();

		if (current_render_objects == old_render_objects) {
			refresh_dirty_instances(render_context_id);
			continue;
		}

		update_needed = true;
		// we want lower material IDs to come first because that is how we handle subpasses
		std::sort(current_render_objects.begin(), current_render_objects.end(), [](const RenderObject &a, const RenderObject &b) {
			auto &material_manager = utils::get_singleton_safe<MaterialManager>();

			bool id_comparison = ((static_cast<size_t>(a.material) << 32) | static_cast<uint32_t>(a.mesh)) <
								 ((static_cast<size_t>(b.material) << 32) | static_cast<uint32_t>(b.mesh));
			bool render_priority_comparison =
				material_manager.get_material_render_priority(a.material) <= material_manager.get_material_render_priority(b.material);

			return render_priority_comparison && id_comparison;
		});
	}

	m_dirty_instances.clear();

	if (!update_needed) {
		return;
	}

	core_renderer.get_graphics_command_buffer_manager().begin_secondary_command_buffers(core_renderer.get_current_frame(), RENDER_CONTEXT_ID_PRELOAD, 0);

	vk::DeviceSize current_instance_buffer_size = 0;
	std::unordered_map<RenderContextID, vk::DeviceSize> instance_buffer_sizes;

	create_mesh_vertex_index_buffer(instance_buffer_sizes);

	for (RenderContextID render_context_id : m_render_context_ids_registered) {
		size_t old_instance_buffer_size = m_mesh_instance_buffer[render_context_id] == nullptr ? 0 : m_mesh_instance_buffer[render_context_id]->buffer_size;

		if (m_mesh_instance_buffer[render_context_id] == nullptr || old_instance_buffer_size < instance_buffer_sizes.at(render_context_id)) {
			if (m_mesh_instance_buffer[render_context_id] != nullptr) {
				m_reaper_buffers[core_renderer.get_current_frame()].push_back(std::move(m_mesh_instance_buffer[render_context_id]));
			}

			// instance buffer stays shared between devices
			m_mesh_instance_buffer[render_context_id] =
				memory_manager.create_buffer(core_renderer, instance_buffer_sizes.at(render_context_id),
					vk::BufferUsageFlagBits::eStorageBuffer | vk::BufferUsageFlagBits::eTransferDst,
					vk::SharingMode::eExclusive, VMA_ALLOCATION_CREATE_HOST_ACCESS_SEQUENTIAL_WRITE_BIT, VMA_MEMORY_USAGE_AUTO,
					"context-" + std::to_string(render_context_id) + "-instance-buffer");
		}

		refresh_dirty_instances(render_context_id, true);
	}

	// initialize materials
	for (MaterialID queued_material : m_queued_materials) {
		auto &material_manager = utils::get_singleton_safe<MaterialManager>();

		material_manager.initialize_super_material(queued_material);
	}

	m_inited_materials.insert(m_queued_materials.begin(), m_queued_materials.end());
	m_queued_materials.clear();
}

void aristaeus::gfx::vulkan::MeshManager::draw(vk::CommandBuffer &primary_command_buffer, RenderContextID render_context_id,
											   uint32_t subpass_index) {
	size_t batch_size = 0;
	MeshID last_mesh_id = -1;
	bool last_mesh_used_merged_vertex_buffer = false;
	size_t last_super_material_id = -1;
	GraphicsPipeline *last_pipeline_bound = nullptr;

	auto &core_renderer = utils::get_singleton_safe<CoreRenderer>();
	vk::CommandBuffer command_buffer = 
		core_renderer.get_graphics_command_buffer_manager().get_command_buffer_for_thread(render_context_id, {}, {}, subpass_index);

	if (!m_render_context_ids_registered.contains(render_context_id) || !m_render_objects.contains(render_context_id) || 
		m_render_objects.at(render_context_id).empty()) {
		return;
	}

	vk::Buffer vertex_index_buffer = m_mesh_vertex_index_buffer->buffer;
	vk::DeviceSize base_vertex_buffer_offset = 0;

	vk::raii::DescriptorSet &camera_descriptor_set = core_renderer.get_perspective_camera_environment().get_camera_descriptor_set_to_bind();

	struct BatchDrawInfo {
		uint32_t index_count;
		uint32_t instance_count;
		uint32_t first_index;
		int32_t vertex_offset;
		uint32_t first_instance;
	};

	std::optional<BatchDrawInfo> last_batch_draw_info;

	if (m_last_frame != core_renderer.get_current_frame()) {
		m_triangle_count[core_renderer.get_current_frame()] = 0;
		m_draw_calls[core_renderer.get_current_frame()] = 0;
	}

	// TODO(Bobby): add GPU-based frustrum and occlusion culling
	for (int i = 0; m_render_objects.at(render_context_id).size() > i; ++i) {
		const RenderObject &render_object = m_render_objects.at(render_context_id)[i];

		size_t super_material_id = m_material_manager.get_super_material_index(render_object.material);

		bool in_same_batch = super_material_id == last_super_material_id && render_object.mesh == last_mesh_id;

		if (in_same_batch) {
			++last_batch_draw_info.value().instance_count;
			++batch_size;
			continue;
		}
		else {
			if (last_batch_draw_info.has_value()) {
				if (m_use_gpu_driven_rendering) {
					throw std::runtime_error("Not yet implemented!");
				}
				else {
					command_buffer.drawIndexed(last_batch_draw_info->index_count, last_batch_draw_info->instance_count, 
											   last_batch_draw_info->first_index, last_batch_draw_info->vertex_offset, 
											   last_batch_draw_info->first_instance);

					m_triangle_count[core_renderer.get_current_frame()] += last_batch_draw_info->index_count / 3;
					++m_draw_calls[core_renderer.get_current_frame()];
				}
			}

			MeshData &mesh_data = m_mesh_data.at(render_object.mesh);
			vk::DeviceSize index_buffer_offset = mesh_data.index_buffer_offset;
			vk::DeviceSize vertex_buffer_offset = mesh_data.vertex_buffer_offset;

			if (last_super_material_id != super_material_id) {
				if (subpass_index != m_material_manager.get_graphics_pipeline_for_material(render_object.material).get_pipeline_info().sub_pass) {
					// render object is in different subpass
					continue;
				}

				if ((last_pipeline_bound == nullptr ||
					*last_pipeline_bound != m_material_manager.get_graphics_pipeline_for_material(render_object.material))) {
					command_buffer = m_material_manager.bind_material(render_object.material, render_context_id);
				}
				
				last_super_material_id = super_material_id;
				last_pipeline_bound = &m_material_manager.get_graphics_pipeline_for_material(render_object.material);

				command_buffer.bindDescriptorSets(vk::PipelineBindPoint::eGraphics, *last_pipeline_bound->get_pipeline_layout(), 0, 
													*camera_descriptor_set, {});
				command_buffer.bindDescriptorSets(vk::PipelineBindPoint::eGraphics, *last_pipeline_bound->get_pipeline_layout(), 3,
													*m_mesh_instance_ssbo_set.at(render_context_id)[core_renderer.get_current_frame()], {});
			}

			if (last_mesh_id != render_object.mesh) {
				if (mesh_data.unmerged_buffer != nullptr) {
					command_buffer.bindVertexBuffers(0, mesh_data.unmerged_buffer->buffer, vertex_buffer_offset);
					command_buffer.bindIndexBuffer(mesh_data.unmerged_buffer->buffer, index_buffer_offset, vk::IndexType::eUint32);

					last_mesh_used_merged_vertex_buffer = false;
				}
				else if (!last_mesh_used_merged_vertex_buffer) {
					command_buffer.bindVertexBuffers(0, vertex_index_buffer, base_vertex_buffer_offset);
					command_buffer.bindIndexBuffer(vertex_index_buffer, m_vertex_buffer_size, vk::IndexType::eUint32);

					last_mesh_used_merged_vertex_buffer = true;
				}

				last_mesh_id = render_object.mesh;
			}

			vk::DeviceSize vertex_num_offset = mesh_data.vertex_num_offset;

			last_batch_draw_info = BatchDrawInfo{
				.index_count = static_cast<uint32_t>(mesh_data.indices.size()),
				.instance_count = 1,
				.first_index = static_cast<uint32_t>(index_buffer_offset),
				.vertex_offset = static_cast<int32_t>(vertex_num_offset),
				.first_instance = static_cast<uint32_t>(i),
			};
		}
	}

	m_last_frame = core_renderer.get_current_frame();
}
