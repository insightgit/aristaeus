# Aristaeus
A research renderer built to investigate real time rasterization and ray tracing.

![Bistro model rendered in rasterization pipeline](https://gitlab.com/insightgit/aristaeus/-/raw/master/images/bistro-raster.png?ref_type=heads)

![Sponza with indirect illumination in rasterization pipeline](https://gitlab.com/insightgit/aristaeus/-/raw/master/images/image.png?ref_type=heads)

### Features (so far)
- Physically based illumination model
- Bindless renderer through MeshManager and MaterialManager
- Omnidirectional and directional light shadow mapping
- Class reflection and module system loosely based on Godot's ClassDB system
- Rasterization and WIP ray tracing pipeline (RT pipeline currently in ray-tracing branch)
- Reflective shadow mapping-based GI in rasterization
- SSAO
- WIP IBL
- multi-threaded resource creation and management thru Taskflow
- Scene tree with hierarchical transforms and JSON based loader with prefab support
- (in an earlier version, currently broken) Chunked perlin noise-based terrain with Gertsner waves

### TODO list
- ReGIR lighting (work ongoing in ray-tracing branch)
- Seamless RT and raster hybrid switching
- GPU-based occlusion culling
- Cascaded shadow mapping in raster
- More robust and battle-tested render graph
- Investigate better and more performant raster-based GI solutions
- Switch support through deko3d (as a fun side project :) )

### Running on windows
Install [vcpkg](https://github.com/microsoft/vcpkg), the [Vulkan LunarG SDK](https://vulkan.lunarg.com/) and [CMake](https://cmake.org/download/).
This program has mainly been developed with Visual Studio 2022 and MSVC, but it will hopefully mostly work with gcc/clang.

then install the following vcpkg packages using the following command:
`vcpkg install glfw3:x64-windows boost-algorithm:x64-windows boost-uuid:x64-windows assimp:x64-windows tbb:x64-windows`

After that, set your `VCPKG_TOOLCHAIN` environment variable when generating this project's CMake files to point to `<your vcpkg dir>/scripts/buildsystems/vcpkg.cmake`.
If you are using Visual Studio, it will do this automatically for you if you modify line 3 of CMakeSettings.json appropriately. If the Vulkan SDK cannot be found,
verify that your `VULKAN_SDK` environment variable is set properly to the location of the install Vulkan SDK.