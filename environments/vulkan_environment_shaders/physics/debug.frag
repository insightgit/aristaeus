#version 460

layout (push_constant) uniform constants {
    vec3 color;
} push_constants;

layout (location = 0) out vec4 color_out;

void main() {
    color_out = vec4(push_constants.color, 1.0f);
}