//
// Created by bobby on 26/01/23.
//

#ifndef ARISTAEUS_CUBE_MAP_TEXTURE_HPP
#define ARISTAEUS_CUBE_MAP_TEXTURE_HPP

#include "core_renderer.hpp"

namespace aristaeus::gfx::vulkan {
    class CubeMapTexture {
    public:
        static const vk::ImageViewCreateInfo DEFAULT_IMAGE_VIEW_CREATE_INFO;

        CubeMapTexture(ImageRaii cubemap_image_raii, uint32_t number_of_mip_levels = 1,
                       const std::optional<vk::SamplerCreateInfo> &sampler_create_info = {},
                       vk::PipelineBindPoint pipeline_bind_point = vk::PipelineBindPoint::eGraphics);
        CubeMapTexture(const std::array<std::string, 6> &texture_faces,
                       const std::optional<vk::SamplerCreateInfo> &sampler_create_info = {},
                       vk::PipelineBindPoint pipeline_bind_point = vk::PipelineBindPoint::eGraphics);

        [[nodiscard]] glm::ivec2 get_image_size() const {
            return m_cubemap_image_size;
        }

        [[nodiscard]] vk::DescriptorImageInfo get_descriptor_image_info() const {
            return {*m_cubemap_sampler, *m_cubemap_image_view, vk::ImageLayout::eShaderReadOnlyOptimal};
        }

        [[nodiscard]] static vk::SamplerCreateInfo get_sampler_info(uint32_t number_of_mip_levels = 1);

        [[nodiscard]] std::array<std::pair<std::string, utils::SHA256Digest>, 6> get_hashes() {
            return m_file_hashes;
        }
    private:
        static constexpr vk::SamplerCreateInfo DEFAULT_SAMPLER_CREATE_INFO {{}, vk::Filter::eLinear,
                                                                            vk::Filter::eLinear,
                                                                            vk::SamplerMipmapMode::eLinear,
                                                                            vk::SamplerAddressMode::eRepeat,
                                                                            vk::SamplerAddressMode::eRepeat,
                                                                            vk::SamplerAddressMode::eRepeat, 0.0f,
                                                                            VK_TRUE, {}, VK_FALSE,
                                                                            vk::CompareOp::eNever, 0.0f, 0.0f,
                                                                            vk::BorderColor::eIntOpaqueBlack, VK_FALSE};


        struct CallbackData {
            std::string image_path;
            CubeMapTexture *texture;
        };

        static void postrender_saving_callback(void* data);

        vk::raii::ImageView create_image_view(uint32_t number_of_mip_levels = 1) {
            vk::ImageViewCreateInfo image_view_create_info = DEFAULT_IMAGE_VIEW_CREATE_INFO;

            image_view_create_info.format = m_cubemap_image->format;
            image_view_create_info.image = m_cubemap_image->image;

            image_view_create_info.subresourceRange.levelCount = number_of_mip_levels;

            return {utils::get_singleton_safe<CoreRenderer>().get_logical_device_ref(), image_view_create_info};
        }

        ImageRaii create_cubemap_image(const std::array<std::string, 6> &texture_faces);

        BufferRaii m_staging_image_buffer {};

        glm::ivec2 m_cubemap_image_size;

        std::array<std::pair<std::string, utils::SHA256Digest>, 6> m_file_hashes;

        vk::PipelineBindPoint m_pipeline_bind_point;

        ImageRaii m_cubemap_image;
        vk::raii::ImageView m_cubemap_image_view;
        vk::raii::Sampler m_cubemap_sampler;
        uint32_t m_number_of_mip_levels;

        BufferRaii m_save_image_staging_buffer;
    };
}

#endif //ARISTAEUS_CUBE_MAP_TEXTURE_HPP
