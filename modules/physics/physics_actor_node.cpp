//
// Created by bobby on 06/05/24.
//

#include "physics_actor_node.hpp"

ARISTAEUS_CLASS_IMPL(aristaeus::physics::PhysicsActorNode,
                     aristaeus::core::StdTypeInfoRef(typeid(aristaeus::gfx::SceneGraphNode)))

void aristaeus::physics::PhysicsActorNode::classdb_register() {
    auto &class_db = utils::get_singleton_safe<core::ClassDB>();

    class_db.register_property<PhysicsActorNode, std::unordered_set<GeometryInfo>>("geometry_infos", [](PhysicsActorNode &actor_node) { return actor_node.m_geometry_infos; },
                                                                                      [](PhysicsActorNode &actor_node, std::unordered_set<GeometryInfo> geometry_infos) {
                                                                                          if(actor_node.m_actor != nullptr) {
                                                                                              for (std::shared_ptr<PhysxShape> shape : actor_node.m_collision_shapes) {
                                                                                                  actor_node.m_actor->detachShape(*shape->shape);
                                                                                              }
                                                                                          }
                                                                                          
                                                                                          actor_node.m_collision_shapes.clear();

                                                                                          actor_node.m_geometry_infos = std::move(geometry_infos);

                                                                                          if (actor_node.m_in_tree) {
                                                                                              actor_node.refresh_collision_shapes();
                                                                                          }
                                                                                      }, []() { return std::unordered_set<GeometryInfo>(); });


    class_db.register_event_callback_on_construction<PhysicsActorNode, void*>(std::string{ SceneGraphNode::TREE_READY_EVENT },
                                                                              "physics_actor_tree_ready", [](PhysicsActorNode &actor_node, void*) {
                                                                                  actor_node.on_enter_tree();
                                                                              });
    class_db.register_event_callback_on_construction<PhysicsActorNode, void*>(std::string{ SceneGraphNode::EXIT_TREE_EVENT },
                                                                              "physics_actor_exit_tree", [](PhysicsActorNode &actor_node, void*) {
                                                                                  actor_node.on_exit_tree();
                                                                              });

    class_db.register_event_callback_on_construction<PhysicsActorNode, SceneGraphNode::Transform>(std::string{ SceneGraphNode::TRANSFORM_CHANGED_EVENT }, "physics_actor_transform_changed",
                                                                                                  [](PhysicsActorNode &actor_node, SceneGraphNode::Transform) {
                                                                                                      actor_node.on_transform_changed();
                                                                                                  });
    class_db.register_event_callback_on_construction<PhysicsActorNode, void*>(std::string{ SceneGraphNode::GLOBAL_TRANSFORM_CHANGED_EVENT }, "point_light_global_transform_changed", 
                                                                              [](PhysicsActorNode &actor_node, void*) {
                                                                                  actor_node.on_transform_changed();
                                                                              });

    class_db.register_event<PhysicsActorNode, void*>(std::string{ physics::PhysicsActorNode::ACTOR_UPDATED_EVENT });
    
    class_db.register_event_callback_on_construction<PhysicsActorNode, void*>(std::string{ PhysicsActorNode::ACTOR_UPDATED_EVENT }, "physics_actor_node_actor_updated", [](PhysicsActorNode &node, void*) {
        
        node.refresh_attached_shapes_to_actor();
    });
}

void aristaeus::physics::PhysicsActorNode::refresh_attached_shapes_to_actor() {
    assert(m_actor != nullptr);

    for (const std::shared_ptr<PhysxShape> &shape : m_collision_shapes) {
        // we know we can bypass the const cast here because we know that attaching the shape to this 
        // actor will NOT change the shape ptr itself in anyway (only changing the data at the shape ptr)
        // and because of this, it will not create an unexpected stale hash as that is the only thing
        // involved in the hash function of PhysxShape
        const physx::PxShape &const_shape_ref = *shape->shape;

        m_actor->attachShape(const_cast<physx::PxShape&>(const_shape_ref));
    }
}

void aristaeus::physics::PhysicsActorNode::refresh_collision_shapes() {
    assert(m_collision_shapes.empty());

    auto &physics_manager = utils::get_singleton_safe<PhysicsManager>();

    for(const GeometryInfo &geometry_info : m_geometry_infos) {
        m_collision_shapes.insert(physics_manager.create_cached_shape(geometry_info, this));
    }

    if (m_actor != nullptr) {
        refresh_attached_shapes_to_actor();
    }
}

void aristaeus::physics::PhysicsActorNode::on_enter_tree() {
    // ON_PARSING_DONE event should've been already called by this point
    auto &physics_manager = utils::get_singleton_safe<PhysicsManager>();

    m_in_tree = true;

    refresh_collision_shapes();

    physics_manager.add_actor(*m_actor, *this);
}

void aristaeus::physics::PhysicsActorNode::on_exit_tree() {
    m_collision_shapes.clear();

    m_in_tree = false;

    utils::get_singleton_safe<PhysicsManager>().remove_actor(*m_actor);
}

std::weak_ptr<aristaeus::physics::PhysxShape> aristaeus::physics::PhysicsActorNode::add_shape(const GeometryInfo &geometry_info) {
    std::shared_ptr<PhysxShape> cached_shape =
            utils::get_singleton_safe<PhysicsManager>().create_cached_shape(geometry_info);

    assert(cached_shape != nullptr);

    if (!m_collision_shapes.contains(cached_shape)) {
        if (m_actor != nullptr && m_actor->attachShape(*cached_shape->shape)) {
            throw std::runtime_error("Couldn't attach collision shape!");
        }

        m_collision_shapes.insert(cached_shape);
    }

    return cached_shape;
}

bool aristaeus::physics::PhysicsActorNode::remove_shape(const std::weak_ptr<PhysxShape> &shape) {
    std::shared_ptr<PhysxShape> shape_strong = shape.lock();

    if (m_collision_shapes.contains(shape_strong)) {
        m_actor->detachShape(*shape_strong->shape);

        m_collision_shapes.erase(shape_strong);

        return true;
    }
    else {
        return false;
    }
}

physx::PxTransform aristaeus::physics::PhysicsActorNode::scene_transform_to_physx_transform(
                                                                      const gfx::SceneGraphNode::Transform &transform) {
    physx::PxTransform actor_new_transform;

    actor_new_transform.q = physx::PxQuat(transform.rotation_quat.w, transform.rotation_quat.x,
                                          transform.rotation_quat.y, transform.rotation_quat.z);
    actor_new_transform.p = physx::PxVec3(transform.position.x, transform.position.y, transform.position.z);

    return actor_new_transform;
}

aristaeus::gfx::SceneGraphNode::Transform aristaeus::physics::PhysicsActorNode::physx_transform_to_scene_transform(const physx::PxTransform &transform) {
    return SceneGraphNode::Transform{
        .scale = {1, 1, 1},
        .position = {transform.p.x, transform.p.y, transform.p.z},
        .rotation_quat = {transform.q.w, transform.q.x, transform.q.y, transform.q.z}
    };
}

void aristaeus::physics::PhysicsActorNode::on_physics_step_finished() {
    physx::PxTransform actor_transform = m_actor->getGlobalPose();
    physx::PxQuat actor_normalized_quat = actor_transform.q.getNormalized();

    set_transform(Transform{
            .scale = glm::vec3{1, 1, 1},
            .position = glm::vec3{actor_transform.p.x, actor_transform.p.y, actor_transform.p.z},
            .rotation_quat = glm::quat{actor_normalized_quat.w, actor_normalized_quat.x, actor_normalized_quat.y,
                                       actor_normalized_quat.z}
    });
}
