//
// Created by bobby on 1/21/22.
//

#ifndef ARISTAEUS_GPU_PERLIN_NOISE_PROCEDURAL_TERRAIN_HPP
#define ARISTAEUS_GPU_PERLIN_NOISE_PROCEDURAL_TERRAIN_HPP

#include <cmath>

#include <chrono>
#include <map>
#include <memory>
#include <random>
#include <unordered_map>

#include <gfx/vulkan/point_light.hpp>
#include <gfx/vulkan/prop_info.hpp>
#include <gfx/vulkan/procedural_terrain.hpp>
#include <gfx/vulkan/core_renderer.hpp>
#include <gfx/vulkan/perlin_noise.hpp>
#include <gfx/vulkan/terrain_image.hpp>
#include <gfx/vulkan/wave_properties.hpp>

#include "compute_manager.hpp"
#include "gpu_perlin_noise_terrain.hpp"

namespace aristaeus::gfx::vulkan::compute {
    //class Terrain;

    class GPUPerlinNoiseProceduralTerrain : public ProceduralTerrain {
    public:
        GPUPerlinNoiseProceduralTerrain(const TerrainInfo &terrain_info, const WaterInfo &water_info,
                          std::mt19937 &rng, const glm::ivec2 &height_map_resolution,
                          const std::unordered_map<glm::vec2, glm::vec2, utils::Vec2Hash> &predetermined_gradient_vectors,
                          std::optional<std::reference_wrapper<GraphicsPipeline>> environment_pipeline,
                          bool write_noise_texture = false, bool draw_water_noise_texture = true,
                          std::optional<glm::vec2> perlin_noise_start = {},
                          std::optional<vk::CommandBuffer> create_command_buffer = {});

        [[nodiscard]] float get_height_map_value(int x, int y) const override {
            // TODO(Bobby): implement this for compute!
            return 0;
        }

        [[nodiscard]] float get_height_map_value_normalized(float x, float y) const override {
            // TODO(Bobby): implement this for compute!
            return 0;
        }

        [[nodiscard]] glm::vec2 get_perlin_gradient_vector(const glm::vec2 &coords) override {
            // TODO(Bobby): implement this for compute!
            return {};
        }

        Terrain *terrain_from_terrain_info(const ProceduralTerrain::TerrainInfo &terrain_info,
                                           std::optional<vk::CommandBuffer> create_command_buffer)override;

        void predraw_load(const vk::CommandBuffer &command_buffer, const glm::vec3& camera_position) {}
    private:
        std::shared_ptr<GPUPerlinNoise> generate_height_map_job(std::optional<glm::vec2> perlin_noise_start);

        // gpu perlin noise variables
        GPUPerlinNoise::PerlinNoiseInfo m_perlin_noise_info;
        std::shared_ptr<GPUPerlinNoise> m_height_map_job;
    };
}

#endif //ARISTAEUS_GPU_PERLIN_NOISE_PROCEDURAL_TERRAIN_HPP
