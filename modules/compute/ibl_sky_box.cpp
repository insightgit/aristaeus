//
// Created by bobby on 1/3/22.
//

#include "ibl_sky_box.hpp"

#include <utility>

#include "compute_manager.hpp"

ARISTAEUS_CLASS_IMPL(aristaeus::gfx::vulkan::compute::IblSkyBox, typeid(aristaeus::gfx::vulkan::SkyBox));

void aristaeus::gfx::vulkan::compute::IblSkyBox::classdb_register() {
    core::ClassDB &class_db = utils::get_singleton_safe<core::ClassDB>();

    class_db.register_property<IblSkyBox, glm::ivec2>("pbr_samples", [](IblSkyBox &ibl_sky_box) {
        return ibl_sky_box.m_pbr_samples;
    }, [](IblSkyBox &ibl_sky_box, glm::ivec2 pbr_samples) {
        ibl_sky_box.m_pbr_samples = pbr_samples;
    }, {});
    class_db.register_property<IblSkyBox, glm::ivec2>("brdf_image_size", [](IblSkyBox &ibl_sky_box) {
        return ibl_sky_box.m_brdf_image_size;
    }, [](IblSkyBox &ibl_sky_box, glm::ivec2 brdf_image_size) {
        ibl_sky_box.m_brdf_image_size = brdf_image_size;
    }, []() { return glm::ivec2{ 512, 512 }; });
    class_db.register_property<IblSkyBox, int32_t>("brdf_samples", [](IblSkyBox &ibl_sky_box) {
        return ibl_sky_box.m_brdf_samples;
    }, [](IblSkyBox &ibl_sky_box, int32_t brdf_samples) {
        ibl_sky_box.m_brdf_samples = brdf_samples;
    }, []() { return 1024; });

    class_db.register_property<IblSkyBox, std::optional<std::string>>("compute_cache_path",
    [](IblSkyBox &ibl_sky_box) {
        return ibl_sky_box.m_compute_pipeline_cache_path;
    }, [](IblSkyBox &ibl_sky_box, std::optional<std::string> compute_pipeline_cache_path) {
        ibl_sky_box.m_compute_pipeline_cache_path = std::move(compute_pipeline_cache_path);
    }, []{ return std::optional<std::string>{}; });

    class_db.register_property<IblSkyBox, std::optional<std::string>>("equirectangular_map",
    [](IblSkyBox &ibl_sky_box) {
        return ibl_sky_box.m_equirectangular_converter_job == nullptr ? std::optional<std::string>{} :
               ibl_sky_box.m_equirectangular_converter_job->get_input_texture_path();
    }, [](IblSkyBox &ibl_sky_box, std::optional<std::string> equirectangular_map) {
        if(equirectangular_map.has_value()) {
            if(ibl_sky_box.m_equirectangular_converter_job == nullptr ||
               ibl_sky_box.m_equirectangular_converter_job->get_input_texture_path() !=
               ibl_sky_box.m_compute_pipeline_cache_path) {
                ibl_sky_box.m_equirectangular_converter_job =
                        std::make_shared<EquirectangularConverter>(*equirectangular_map,
                                                                   utils::add_string_to_optional(
                                                                           ibl_sky_box.m_compute_pipeline_cache_path,
                                                                           "/equirectangular/"));

                utils::get_singleton_safe<ComputeManager>().add_compute_task(
                        std::static_pointer_cast<ComputeTask>(ibl_sky_box.m_equirectangular_converter_job));
            }
        } else {
            ibl_sky_box.m_equirectangular_converter_job = nullptr;

        }
    }, []{ return std::optional<std::string>{}; });

    class_db.register_event_callback_on_construction<IblSkyBox, void*>(
            std::string{gfx::SceneGraphNode::BEFORE_PARSING_EVENT}, "ibl_sky_box_before_parsing_event",
            [](IblSkyBox &sky_box, void*) {
        sky_box.set_child_create_cubemap_texture(true);
    });
}

void aristaeus::gfx::vulkan::compute::IblSkyBox::predraw_load(const vk::CommandBuffer &command_buffer,
                                                              const glm::vec3 &camera_position) {
    ComputeManager &compute_manager = utils::get_singleton_safe<ComputeManager>();

    bool have_equirectangular_texture = m_equirectangular_converter_job == nullptr && m_diffuse_convolution_job == nullptr;
    bool equirectangular_job_finished = m_equirectangular_converter_job != nullptr && m_cubemap_texture == nullptr &&
        m_equirectangular_converter_job->get_compute_job_status() == ComputeTask::JobStatus::Finished;

    if (have_equirectangular_texture || equirectangular_job_finished) {
        if (equirectangular_job_finished) {
            CubeMapComputeTask::OutputInfo output_info = m_equirectangular_converter_job->get_output_info().value();
            CubeMapTexture cubemap_texture{ std::move(output_info.image) };

            m_cubemap_texture = std::make_unique<CubeMapTexture>(std::move(cubemap_texture));
        }

        assert(m_specular_convolution_texture == nullptr && m_brdf_convolution_job == nullptr);

        m_brdf_convolution_job = std::make_shared<BRDFConvolution>(m_brdf_image_size, m_brdf_samples,
                                                                   utils::add_string_to_optional(m_compute_pipeline_cache_path, "/brdf/"));
        m_diffuse_convolution_job = std::make_shared<DiffuseConvolution>(
                                                        *m_cubemap_texture,
                                                         m_equirectangular_converter_job->get_output_filenames(),
                                                         m_pbr_samples, false,
                                                         utils::add_string_to_optional(m_compute_pipeline_cache_path,
                                                                                       "/irradiance/"));
        m_specular_reflection_convolution_job = std::make_shared<DiffuseConvolution>(
                                                        *m_cubemap_texture,
                                                        m_equirectangular_converter_job->get_output_filenames(),
                                                        m_pbr_samples, true,
                                                        utils::add_string_to_optional(m_compute_pipeline_cache_path,
                                                                                      "/radiance/"));

        compute_manager.add_compute_task(std::static_pointer_cast<ComputeTask>(m_brdf_convolution_job));
        compute_manager.add_compute_task(std::static_pointer_cast<ComputeTask>(m_diffuse_convolution_job));
        compute_manager.add_compute_task(std::static_pointer_cast<ComputeTask>(m_specular_reflection_convolution_job));
    }
    
    if (m_diffuse_convolution_job != nullptr && m_diffuse_convolution_texture == nullptr &&
        m_diffuse_convolution_job->get_compute_job_status() == ComputeTask::JobStatus::Finished) {

        CubeMapComputeTask::OutputInfo output_info = m_diffuse_convolution_job->get_output_info().value();
        m_diffuse_convolution_texture = std::make_unique<CubeMapTexture>(std::move(output_info.image));
    }
    
    if (m_specular_reflection_convolution_job != nullptr && m_specular_convolution_texture == nullptr &&
        m_specular_reflection_convolution_job->get_compute_job_status() == ComputeTask::JobStatus::Finished) {
        CubeMapComputeTask::OutputInfo output_info = m_specular_reflection_convolution_job->get_output_info().value();
        m_specular_convolution_texture = std::make_unique<CubeMapTexture>(std::move(output_info.image),
                                                                          output_info.image->mip_levels);
    }

    if (m_brdf_convolution_job != nullptr && m_brdf_convolution_texture == nullptr &&
        m_brdf_convolution_job->get_compute_job_status() == ComputeTask::JobStatus::Finished) {
        m_brdf_convolution_texture = std::make_unique<Texture>(std::move(m_brdf_convolution_job->get_output_info().value()));
    }
}

std::vector<vk::DescriptorImageInfo> aristaeus::gfx::vulkan::compute::IblSkyBox::get_descriptor_image_infos() const {
    std::vector<vk::DescriptorImageInfo> return_value;

    if(m_cubemap_texture != nullptr) {
        return_value.push_back(m_cubemap_texture->get_descriptor_image_info());
    }

    if(m_diffuse_convolution_texture != nullptr) {
        return_value.push_back(m_diffuse_convolution_texture->get_descriptor_image_info());
    }

    if(m_specular_convolution_texture != nullptr) {
        return_value.push_back(m_specular_convolution_texture->get_descriptor_image_info());
    }

    if (m_brdf_convolution_texture != nullptr) {
        return_value.push_back(m_brdf_convolution_texture->get_descriptor_image_info(utils::get_singleton_safe<CoreRenderer>()));
    }

    return return_value;
}
