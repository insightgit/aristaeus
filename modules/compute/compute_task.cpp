#include <gfx/vulkan/core_renderer.hpp>

#include "compute_task.hpp"

aristaeus::gfx::vulkan::compute::ComputeTask::ComputeTask(size_t ubo_size, uint32_t number_of_storage_images,
                                                          uint32_t number_of_ubos, uint32_t number_of_samplers,
                                                          ComputePipelineInfo compute_pipeline_info,
                                                          const std::string &compute_task_type,
                                                          uint32_t number_of_array_layers, uint32_t number_of_mipmaps,
                                                          uint32_t number_of_dispatches_done_per_job,
                                                          const std::optional<std::string> &cache_path,
                                                          const std::vector<std::string> &input_file_paths) :
    m_descriptor_set_info(generate_descriptor_set_info(number_of_storage_images, number_of_ubos, number_of_samplers,
                                                       number_of_dispatches_done_per_job)),
    m_compute_pipeline(compute_pipeline_info.compute_shader_info, { m_descriptor_set_info.first },
                       compute_pipeline_info.pipeline_cache_path, compute_pipeline_info.push_content_ranges,
                       compute_pipeline_info.start_constructing_pipeline, compute_pipeline_info.is_in_preload_stage),
    m_descriptor_set_layout(utils::get_singleton_safe<CoreRenderer>().get_logical_device_ref(),
                            { {}, m_descriptor_set_info.first }),
    m_descriptor_pool(create_descriptor_pool(utils::get_singleton_safe<CoreRenderer>().get_logical_device_ref(),
                                             number_of_dispatches_done_per_job)),
    m_descriptor_sets(create_descriptor_sets(utils::get_singleton_safe<CoreRenderer>().get_logical_device_ref(),
                                             number_of_dispatches_done_per_job)),
    m_ubos(create_ubos(ubo_size, number_of_dispatches_done_per_job * number_of_ubos)),
    m_cache_path(cache_path),
    m_input_file_paths(input_file_paths),
    m_number_of_array_layers(number_of_array_layers),
    m_number_of_mipmaps(number_of_mipmaps),
    m_compute_task_type(compute_task_type) {}


aristaeus::gfx::vulkan::compute::ComputeTask::DescriptorInfo
    aristaeus::gfx::vulkan::compute::ComputeTask::generate_descriptor_set_info(
                                                                           uint32_t number_of_storage_images,
                                                                           uint32_t number_of_ubos,
                                                                           uint32_t number_of_samplers,
                                                                           uint32_t number_of_dispatches_done_per_job) {
    std::vector<vk::DescriptorSetLayoutBinding> layout_bindings;
    std::vector<vk::DescriptorPoolSize> descriptor_pool_size;

    if (number_of_storage_images > 0) {
        layout_bindings.emplace_back(layout_bindings.size(), vk::DescriptorType::eStorageImage,
                                     number_of_storage_images, vk::ShaderStageFlagBits::eCompute);
        descriptor_pool_size.emplace_back(vk::DescriptorType::eStorageImage,
                                          number_of_storage_images * number_of_dispatches_done_per_job);
    }

    if (number_of_ubos > 0) {
        layout_bindings.emplace_back(layout_bindings.size(), vk::DescriptorType::eUniformBuffer,
                                     number_of_ubos, vk::ShaderStageFlagBits::eCompute);
        descriptor_pool_size.emplace_back(vk::DescriptorType::eUniformBuffer,
                                          number_of_ubos * number_of_dispatches_done_per_job);
    }

    if (number_of_samplers > 0) {
        layout_bindings.emplace_back(layout_bindings.size(), vk::DescriptorType::eCombinedImageSampler,
                                     number_of_samplers, vk::ShaderStageFlagBits::eCompute);
        descriptor_pool_size.emplace_back(vk::DescriptorType::eCombinedImageSampler,
                                          number_of_samplers * number_of_dispatches_done_per_job);
    }

    assert(!layout_bindings.empty());
    assert(layout_bindings.size() == descriptor_pool_size.size());

    return { layout_bindings, descriptor_pool_size };
}

std::vector<vk::raii::DescriptorSet> aristaeus::gfx::vulkan::compute::ComputeTask::create_descriptor_sets(
                                                                                vk::raii::Device &logical_device,
                                                                                uint32_t number_of_descriptor_sets) {
    std::vector<vk::DescriptorSetLayout> descriptor_set_layouts;

    for (size_t i = 0; number_of_descriptor_sets > i; ++i) {
        descriptor_set_layouts.push_back(*m_descriptor_set_layout);
    }

    vk::DescriptorSetAllocateInfo set_allocate_info{ *m_descriptor_pool, descriptor_set_layouts };
    std::vector<vk::raii::DescriptorSet> new_sets_vector = logical_device.allocateDescriptorSets(set_allocate_info);

    return new_sets_vector;
}

std::vector<aristaeus::gfx::vulkan::BufferRaii> aristaeus::gfx::vulkan::compute::ComputeTask::create_ubos(
                                                                             size_t ubo_size, uint32_t number_of_ubos) {
    std::vector<BufferRaii> ubo_buffers;
    for (size_t i = 0; number_of_ubos > i; ++i) {
        CoreRenderer &core_renderer = utils::get_singleton_safe<CoreRenderer>();

        ubo_buffers.push_back(core_renderer.get_memory_manager().create_buffer(core_renderer, ubo_size,
                                                   vk::BufferUsageFlagBits::eUniformBuffer, vk::SharingMode::eExclusive,
                                                   VMA_ALLOCATION_CREATE_HOST_ACCESS_SEQUENTIAL_WRITE_BIT,
                                                   VMA_MEMORY_USAGE_AUTO, "perlin-noise-ubo"));
    }

    return ubo_buffers;
}

vk::raii::DescriptorPool aristaeus::gfx::vulkan::compute::ComputeTask::create_descriptor_pool(vk::raii::Device &logical_device, uint32_t number_of_sets) {
    assert(number_of_sets > 0);
    return { logical_device, {{vk::DescriptorPoolCreateFlagBits::eFreeDescriptorSet}, number_of_sets, m_descriptor_set_info.second} };
}

aristaeus::gfx::vulkan::Texture aristaeus::gfx::vulkan::compute::ComputeTask::create_output_texture(
                                                        const glm::ivec2 &image_size, const vk::Format &image_format,
                                                        const std::string &image_description) {
    CoreRenderer &core_renderer = utils::get_singleton_safe<CoreRenderer>();

    ImageRaii output_image = core_renderer.get_memory_manager().create_image(
                                                core_renderer, image_size.x, image_size.y, image_format,
                                                vk::ImageTiling::eOptimal,
                                                vk::ImageUsageFlagBits::eSampled | vk::ImageUsageFlagBits::eStorage,
                                                {}, VMA_MEMORY_USAGE_AUTO_PREFER_DEVICE, image_description);
    vk::ImageViewCreateInfo image_view_create_info = AssimpModel::DEFAULT_IMAGE_VIEW_CREATE_INFO;
    vk::SamplerCreateInfo sampler_create_info = AssimpModel::DEFAULT_SAMPLER_CREATE_INFO;

    sampler_create_info.maxAnisotropy = core_renderer.get_max_sampler_anisotropy();
    image_view_create_info.format = image_format;

    Texture return_value{ output_image, std::unordered_set<std::string>{image_description}, core_renderer, image_view_create_info, sampler_create_info, true };

    return return_value;
}

void aristaeus::gfx::vulkan::compute::ComputeTask::postrender_saving_callback(void* data) {
    CallbackData *callback_data = reinterpret_cast<CallbackData*>(data);

    callback_data->compute_task.save_hdr_image(callback_data->image_path, callback_data->image_raii, {}, callback_data->image_staging_buffer, 
                                               false, callback_data->buffer_offset, callback_data->sha256_digest);

    delete callback_data;
}

void aristaeus::gfx::vulkan::compute::ComputeTask::save_hdr_image(const std::string &image_path, ImageRaii &image,
    const std::optional<vk::CommandBuffer> &command_buffer, BufferRaii &image_staging_buffer,    
    bool skip_image_staging_buffer_creation, std::optional<vk::DeviceSize> buffer_offset,
    const std::optional<std::reference_wrapper<utils::SHA256Digest>> &sha256_digest) {
    glm::ivec2 working_image_size {image->image_width, image->image_height};

    if (image_staging_buffer != nullptr && !skip_image_staging_buffer_creation) {
        void* image_buffer_data;

        vmaMapMemory(image_staging_buffer->allocator, image_staging_buffer->allocation, &image_buffer_data);

        size_t offset_value = buffer_offset.has_value() ? (buffer_offset.value() / sizeof(float)) : 0;
        float* image_buffer_float_data = reinterpret_cast<float*>(image_buffer_data) + offset_value;
        size_t image_copied = 0;

        for (uint32_t mip = 0; m_number_of_mipmaps > mip; ++mip) {
            for (uint32_t array_layer = 0; m_number_of_array_layers > array_layer; ++array_layer) {
                std::string complete_path = image_path + "/" + std::to_string(mip) + "-" + std::to_string(array_layer) + ".hdr";

                stbi_write_hdr(complete_path.c_str(), working_image_size.x, working_image_size.y, 4, &image_buffer_float_data[image_copied]);

                // TODO(Bobby): Optimize this so there's not two file writes/reads back-to-back
                if (sha256_digest.has_value()) {
                    std::memcpy(sha256_digest.value().get().data(), utils::get_sha256_hash_file(complete_path).value().second.data(), sha256_digest.value().get().size() * sizeof(unsigned char));
                }

                image_copied += 4lu * working_image_size.x * working_image_size.y;
            }

            working_image_size /= glm::ivec2{2, 2};
        }

        vmaUnmapMemory(image_staging_buffer->allocator, image_staging_buffer->allocation);

        std::function<void(void*)> saving_done_callback = [](void* arg) {
            ComputeTask *compute_task = reinterpret_cast<ComputeTask*>(arg);

            compute_task->m_job_status = JobStatus::Finished;
            compute_task->m_job_finished_promise.set_value();
        };

        utils::get_singleton_safe<CoreRenderer>().add_post_render_callback(CoreRenderer::PostRenderCallbackInfo{ saving_done_callback, this });
    }
    else {
        CoreRenderer &core_renderer = utils::get_singleton_safe<CoreRenderer>();

        if ((std::filesystem::exists(image_path) && !std::filesystem::is_directory(image_path)) || !std::filesystem::exists(image_path)) {
            std::filesystem::create_directories(image_path);
        }

        size_t save_image_size = 0;

        for (uint32_t i = 0; m_number_of_mipmaps > i; ++i) {
            save_image_size += 6lu * 4lu * working_image_size.x * working_image_size.y * sizeof(float);
            working_image_size /= glm::ivec2{2, 2};
        }

        vk::DeviceSize buffer_offset_to_use = buffer_offset.has_value() ? buffer_offset.value() : 0;

        if (!skip_image_staging_buffer_creation) {
            assert(!buffer_offset.has_value());

            image_staging_buffer = core_renderer.get_memory_manager().create_buffer(core_renderer, save_image_size,
                vk::BufferUsageFlagBits::eTransferDst, vk::SharingMode::eExclusive,
                VMA_ALLOCATION_CREATE_HOST_ACCESS_SEQUENTIAL_WRITE_BIT, VMA_MEMORY_USAGE_AUTO,
                "cube_map_saving_staging_buffer");
        }

        core_renderer.copy_image_to_buffer(command_buffer.value(), image, image_staging_buffer,
                                           vk::ImageLayout::eShaderReadOnlyOptimal, 0, m_number_of_array_layers, buffer_offset_to_use,
                                           m_number_of_mipmaps, true, {}, vk::PipelineBindPoint::eCompute);

        CoreRenderer::PostRenderCallbackInfo post_render_callback_info{ postrender_saving_callback,
            new CallbackData{ .image_path = image_path, .image_raii = image,
                              .image_staging_buffer = image_staging_buffer, .buffer_offset = buffer_offset,
                              .sha256_digest = sha256_digest, .compute_task = *this } };
        core_renderer.add_post_render_callback(post_render_callback_info);
    }
}

[[nodiscard]] std::vector<std::string> aristaeus::gfx::vulkan::compute::ComputeTask::get_output_filenames() const {
    if (!m_cache_path.has_value()) {
        return {};
    }

    std::vector<std::string> return_value;

    for (size_t mipmap = 0; m_number_of_mipmaps > mipmap; ++mipmap) {
        for (size_t layer = 0; m_number_of_array_layers > layer; ++layer) {
            return_value.push_back(m_cache_path.value() + "/" + std::to_string(mipmap) + "-" + std::to_string(layer) + ".hdr");
        }
    }

    return return_value;
}
