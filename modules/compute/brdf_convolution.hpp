#ifndef ARISTAEUS_GPU_BRDF_CONVOLUTION_HPP
#define ARISTAEUS_GPU_BRDF_CONVOLUTION_HPP

#include <glm/glm.hpp>
#include <unordered_map>

#include "compute_pipeline.hpp"
#include "single_image_compute_task.hpp"

#include <gfx/vulkan/texture.hpp>

namespace aristaeus::gfx::vulkan::compute {
	class BRDFConvolution : public SingleImageComputeTask {
	public:
		BRDFConvolution(const glm::ivec2 &image_size, int32_t sample_count,
                        const std::optional<std::string> &cache_path = {});

		void dispatch(vk::CommandBuffer &command_buffer)override;

		void update_descriptor_set();
	private:
		struct ConvolutionPushConstants {
			alignas(8) glm::ivec2 image_size;
			alignas(4) int32_t sample_count;
		} m_push_constants;

		static constexpr uint32_t WORK_GROUP_X = 16;
		static constexpr uint32_t WORK_GROUP_Y = 16;
		static constexpr uint32_t WORK_GROUP_SIZE = WORK_GROUP_X * WORK_GROUP_Y * 1;

		VulkanPipeline::ShaderInfo get_compute_shader_info() {
			return {
				.code = utils::read_file("environments/vulkan_environment_shaders/compute/brdf_convolution.comp.spv"),
				.name = "brdf_convolution.comp",
				.stage = vk::ShaderStageFlagBits::eCompute
			};
		}
	};
}

#endif