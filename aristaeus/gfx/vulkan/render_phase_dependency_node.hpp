#ifndef ARISTAEUS_RENDER_PHASE_DEPENDENCY_NODE
#define ARISTAEUS_RENDER_PHASE_DEPENDENCY_NODE

#include <optional>

#include "render_phase.hpp"

namespace aristaeus::gfx::vulkan {
	class RenderPhaseDependencyNode {
	public:
		RenderPhaseDependencyNode(RenderPhase render_phase) : m_render_phase(std::move(render_phase)) {}

		[[nodiscard]] RenderPhase &get_render_phase() {
			return m_render_phase;
		}

		[[nodiscard]] std::string get_render_phase_name() const {
			return m_render_phase.get_render_phase_name();
		}

		[[nodiscard]] std::unordered_set<std::shared_ptr<RenderPhaseDependencyNode>> get_dependents() const {
			return m_dependents;
		}

		[[nodiscard]] std::vector<std::string> get_phases_last_rendered() const {
			return m_phases_last_rendered;
		}

		[[nodiscard]] size_t get_total_num_of_render_contexts_last_rendered() const {
			return m_render_contexts_last_rendered;
		}

		void set_weak_ptr_to_self(const std::weak_ptr<RenderPhaseDependencyNode> &dep_node) {
			m_weak_ptr_to_self = dep_node;
		}

		RenderPhaseDependencyNode &add_render_phase(RenderPhase render_phase);

		void add_dependency(const std::string &dependency, const std::string &dependent, vk::PipelineStageFlags pipeline_flags);

		std::unordered_map<std::string, vk::PipelineStageFlags> get_all_dependencies() const;
		std::unordered_map<std::string, vk::PipelineStageFlags> get_all_dependents() const;
		std::shared_ptr<RenderPhaseDependencyNode> get_render_phase_node(const std::string &render_phase_name);

		void contextless_draw();

		void get_submit_infos(std::vector<vk::SubmitInfo> &submit_infos);

		void imgui_draw(vk::CommandBuffer &command_buffer);
	private:
		std::weak_ptr<RenderPhaseDependencyNode> m_weak_ptr_to_self;

		std::unordered_set<std::string> m_cleared_dependencies;
		std::unordered_set<std::shared_ptr<RenderPhaseDependencyNode>> m_dependents;
		std::vector<std::weak_ptr<RenderPhaseDependencyNode>> m_parents;
		RenderPhase m_render_phase;

		bool m_executed = false;

		std::array<std::vector<vk::Semaphore>, utils::vulkan::MAX_FRAMES_IN_FLIGHT> m_temp_semaphores;

		std::vector<std::string> m_phases_last_rendered;
		size_t m_render_contexts_last_rendered;
	};
}
#endif