#ifndef ARISTAEUS_RASTERIZATION_LIGHTING_ENVIRONMENT_HPP
#define ARISTAEUS_RASTERIZATION_LIGHTING_ENVIRONMENT_HPP

#include "graphics_pipeline.hpp"
#include "image_buffer_raii.hpp"
#include "memory_manager.hpp"
#include "lighting_environment.hpp"
#include "perspective_camera_environment.hpp"
#include "render_phase.hpp"

#include "rasterization_gi/reflective_shadow_map.hpp"

#include "white_noise.hpp"

namespace aristaeus::gfx::vulkan {
    class RasterizationLightingEnvironment : public LightingEnvironment {
    public:
        enum class ForwardPipelineType {
            RegularEnvironment,
            InstancedEnvironment,
            #ifndef NDEBUG
            DebugNormals
            #endif
        };

        static constexpr std::string_view DEFERRED_RENDER_PASS_NAME = "deferred_render_pass";
        static constexpr std::string_view DEFERRED_RENDER_PHASE_NAME = "deferred_render_phase";
        static constexpr std::string_view OCCLUSION_QUERY_RENDER_PASS_NAME = "occlusion_query_render_pass";
        static constexpr std::string_view OCCLUSION_QUERY_RENDER_PHASE_NAME = "occlusion_query_render_phase";
        static constexpr std::string_view RASTERIZATION_LIGHTING_ENVIRONMENT = "rasterization_lighting_env";

        static constexpr std::string_view SSAO_RENDER_PASS_NAME = "ssao_render_pass";
        static constexpr std::string_view SSAO_RENDER_PHASE_NAME = "ssao_render_phase";

        static constexpr std::string_view SSAO_BLUR_COMPOSE_RENDER_PASS_NAME = "ssao_blur_compose_render_pass";
        static constexpr std::string_view SSAO_BLUR_COMPOSE_RENDER_PHASE_NAME = "ssao_blur_compose_render_phase";

        static constexpr std::string_view FINAL_RE_RENDER_RENDER_PHASE_NAME = "final_re_render_phase";

        // only used when prioritize_deferred = false
        static constexpr std::string_view DEFERRED_RENDER_NODE_GROUP = "render_deferred";

        // only used when prioritize_deferred = true
        static constexpr std::string_view FORWARD_RENDER_NODE_GROUP = "render_forward";

        static constexpr std::string_view BOTH_RENDER_NODE_GROUP = "render_deferred_forward";

        static const std::array<std::vector<vk::DescriptorSetLayoutBinding>, 2>
            BASE_FORWARD_DESCRIPTOR_SET_BINDINGS;

        static const vk::DescriptorSetLayoutBinding BASE_DEPTH_MAP_DESCRIPTOR_SET_BINDING;
        static const std::array<vk::DescriptorSetLayoutBinding, 4> SSAO_DESCRIPTOR_SET_BINDINGS;

        static const vk::DescriptorSetLayoutBinding SSAO_BLUR_DESCRIPTOR_SET_BINDING;

        static const std::array<std::vector<vk::DescriptorSetLayoutBinding>, 2> BASE_SCREEN_SPACE_INTERPOLATION_SET_BINDINGS;

        static const std::array<std::vector<vk::DescriptorSetLayoutBinding>, 2> BASE_GBUFFER_DESCRIPTOR_SET_LAYOUT_BINDINGS;

        static constexpr vk::PipelineColorBlendAttachmentState FORWARD_COLOR_BLEND_ATTACHMENT =
        { VK_TRUE, vk::BlendFactor::eOneMinusConstantAlpha, vk::BlendFactor::eConstantAlpha, vk::BlendOp::eAdd,
         vk::BlendFactor::eOne, vk::BlendFactor::eZero, vk::BlendOp::eAdd,
         vk::ColorComponentFlagBits::eR | vk::ColorComponentFlagBits::eG | vk::ColorComponentFlagBits::eB };
        static constexpr std::array<vk::DynamicState, 1> FORWARD_DYNAMIC_STATE{ {
            vk::DynamicState::eBlendConstants} };
        static constexpr std::array<vk::DynamicState, 2> DEPTH_MAP_DYNAMIC_STATE{ vk::DynamicState::eBlendConstants, vk::DynamicState::eDepthBias };

        // normals, tangents, position, albedo, roughness/metallic/ambient, depth (lighting/composition is done later)
        static constexpr uint32_t GBUFFER_SIZE = 6;

        RasterizationLightingEnvironment(const PBRMaterialInfo &default_pbr_material_info = {}, const std::vector<glm::vec3> &initial_capture_points = {},
                                         vk::Extent2D initial_depth_capture_resolution = {1000, 1000}, size_t initial_capture_point_capacity = 0,
                                         size_t initial_max_num_of_point_lights = MAX_NUM_OF_POINT_LIGHTS, std::optional<vk::Extent2D> initial_capture_resolution = {}, 
                                         std::optional<vk::Viewport> viewport = {}, bool prioritize_deferred = true, bool using_screen_space_interp = true,
                                         const glm::uvec2 &screen_space_interp_grid_size = glm::uvec2{32, 32});

        ~RasterizationLightingEnvironment() = default;

        [[nodiscard]] static std::vector<aristaeus::gfx::vulkan::RenderPhase::AttachmentInfo> get_gbuffer_attachment_info();

        void activate_environment(bool predraw)override;

        static std::vector<GraphicsPipeline::ShaderInfo>
            get_forward_pipeline_shader_infos(ForwardPipelineType pipeline_type);

        GraphicsPipeline::PipelineInfo
            get_forward_pipeline_pipeline_info(ForwardPipelineType pipeline_type);

        static std::vector<GraphicsPipeline::ShaderInfo> get_screen_space_interp_pipeline_shader_infos();

        GraphicsPipeline::PipelineInfo get_screen_space_interp_pipeline_pipeline_info();

        void set_point_light_shadow_bias(int point_light_shadow_bias) {
            m_environment_ubo_data.point_light_shadow_bias = point_light_shadow_bias;
        }

        std::string get_lighting_env_tag() const override {
            return std::string{ RASTERIZATION_LIGHTING_ENVIRONMENT };
        }

        bool is_captured() const override;
        void capture_points(bool force_recapture) override;
        void capture_point_light_shadows(bool force_recapture) override;

        void input(Window &window) override;

        void set_caustic_environment_map(const std::optional<std::string>& environment_map_path);

        std::vector<vk::WriteDescriptorSet> get_default_texture_writes(RenderContextID render_context_id);

        void add_lighting_node(const std::weak_ptr<gfx::vulkan::LightingNode> &lighting_node);

        void reapply_graphics_pipeline(const vk::CommandBuffer &command_buffer) override;
        void recreate_graphics_pipelines(const vk::Viewport &viewport, const vk::Rect2D &viewport_scissor) override;
    private:
        static constexpr vk::Format ALBEDO_IMAGE_FORMAT = vk::Format::eR8G8B8A8Unorm;
        static constexpr vk::Format NORMALS_POSITION_IMAGE_FORMAT = vk::Format::eR16G16B16A16Sfloat;
        static constexpr vk::Format ROUGHNESS_METALLIC_AO_IMAGE_FORMAT = vk::Format::eR16G16B16Sfloat;

        static constexpr std::array<vk::DescriptorSetLayoutBinding, 10> DEFAULT_PBR_BINDINGS{{
			{0, vk::DescriptorType::eUniformBuffer, 1, vk::ShaderStageFlagBits::eFragment},
			{1, vk::DescriptorType::eCombinedImageSampler, 1, vk::ShaderStageFlagBits::eFragment},
			{2, vk::DescriptorType::eCombinedImageSampler, MAX_NUM_OF_POINT_LIGHTS, vk::ShaderStageFlagBits::eFragment},
			{3, vk::DescriptorType::eCombinedImageSampler, 1, vk::ShaderStageFlagBits::eFragment},
			{4, vk::DescriptorType::eCombinedImageSampler, 1, vk::ShaderStageFlagBits::eFragment},
			{5, vk::DescriptorType::eCombinedImageSampler, 1, vk::ShaderStageFlagBits::eFragment},
			{6, vk::DescriptorType::eCombinedImageSampler, 1, vk::ShaderStageFlagBits::eFragment},
            {7, vk::DescriptorType::eCombinedImageSampler, gi::ReflectiveShadowMap::RSM_STRIDE * MAX_NUM_OF_DIRECTIONAL_LIGHTS, vk::ShaderStageFlagBits::eFragment},
            {8, vk::DescriptorType::eCombinedImageSampler, gi::ReflectiveShadowMap::RSM_STRIDE * MAX_NUM_OF_POINT_LIGHTS, vk::ShaderStageFlagBits::eFragment},
            {9, vk::DescriptorType::eCombinedImageSampler, 1, vk::ShaderStageFlagBits::eFragment}
		}};

        static constexpr std::array<vk::DescriptorPoolSize, 2> DEFAULT_ENVIRONMENT_POOL_SIZES
        {{
            {vk::DescriptorType::eUniformBuffer, static_cast<uint32_t>(utils::vulkan::MAX_FRAMES_IN_FLIGHT)},
            {vk::DescriptorType::eCombinedImageSampler,
             static_cast<uint32_t>(((6 + MAX_NUM_OF_POINT_LIGHTS) + (gi::ReflectiveShadowMap::RSM_STRIDE * (MAX_NUM_OF_DIRECTIONAL_LIGHTS + MAX_NUM_OF_POINT_LIGHTS))) * utils::vulkan::MAX_FRAMES_IN_FLIGHT)}
        }};

        static constexpr vk::SamplerCreateInfo SHADOW_MAPPING_SAMPLER_CREATE_INFO{
                {}, vk::Filter::eLinear, vk::Filter::eLinear, vk::SamplerMipmapMode::eLinear,
                vk::SamplerAddressMode::eClampToBorder, vk::SamplerAddressMode::eClampToBorder,
                vk::SamplerAddressMode::eRepeat, 0.0f, VK_TRUE, {}, VK_FALSE,
                vk::CompareOp::eAlways, 0.0f, 0.0f, vk::BorderColor::eIntOpaqueBlack, VK_FALSE };

        static constexpr std::array<vk::DescriptorSetLayoutBinding, 5> EXTRA_COMPOSITION_BINDINGS{{
            {10, vk::DescriptorType::eCombinedImageSampler, 1, vk::ShaderStageFlagBits::eFragment},
            {11, vk::DescriptorType::eCombinedImageSampler, 1, vk::ShaderStageFlagBits::eFragment},
            {12, vk::DescriptorType::eCombinedImageSampler, 1, vk::ShaderStageFlagBits::eFragment},
            {13, vk::DescriptorType::eCombinedImageSampler, 1, vk::ShaderStageFlagBits::eFragment},
            {14, vk::DescriptorType::eInputAttachment, 1, vk::ShaderStageFlagBits::eFragment}
        }};

        static constexpr std::array<vk::DescriptorPoolSize, 3> COMPOSITION_ENVIRONMENT_POOL_SIZES
        {{
             DEFAULT_ENVIRONMENT_POOL_SIZES[0],
             DEFAULT_ENVIRONMENT_POOL_SIZES[1],
             {vk::DescriptorType::eInputAttachment,
              static_cast<uint32_t>(EXTRA_COMPOSITION_BINDINGS.size() * utils::vulkan::MAX_FRAMES_IN_FLIGHT)}
        }};

        static constexpr vk::DescriptorPoolSize SSAO_BLUR_POOL_SIZE{vk::DescriptorType::eCombinedImageSampler, 2 * utils::vulkan::MAX_FRAMES_IN_FLIGHT};

        static constexpr std::array<vk::DescriptorPoolSize, 3> SCREEN_SPACE_INTERP_POOL_SIZES{{
            {vk::DescriptorType::eCombinedImageSampler, 3 * utils::vulkan::MAX_FRAMES_IN_FLIGHT},
            {vk::DescriptorType::eInputAttachment, 2 * utils::vulkan::MAX_FRAMES_IN_FLIGHT},
            {vk::DescriptorType::eStorageBuffer, utils::vulkan::MAX_FRAMES_IN_FLIGHT}
        }};

        static constexpr vk::AttachmentReference SSAO_BLUR_ATTACHMENT{1, vk::ImageLayout::eColorAttachmentOptimal};
        static constexpr vk::AttachmentReference SSAO_BLUR_INPUT_ATTACHMENT{1, vk::ImageLayout::eShaderReadOnlyOptimal};

        static constexpr vk::AttachmentReference COMPOSITION_ATTACHMENT{ 0, vk::ImageLayout::eColorAttachmentOptimal };
        static constexpr vk::AttachmentReference DEFERRED_POSITION_DEPTH_ATTACHMENT{ 1, vk::ImageLayout::eShaderReadOnlyOptimal };
        static constexpr vk::AttachmentReference FORWARD_DEPTH_ATTACHMENT{ 2, vk::ImageLayout::eDepthStencilAttachmentOptimal };

        static constexpr uint32_t STRUCTURE_BUFFER_GBUFFER_INDEX = 3;

        static constexpr int POSITION_GBUFFER_ATTACHMENT_INDEX = 1;
        static constexpr int NORMALS_GBUFFER_ATTACHMENT_INDEX = 2;

        static constexpr int RSM_DIRECTIONAL_MODE = 0x01;
        static constexpr int RSM_POINT_MODE = 0x02;

        static constexpr int SSAO_ENABLED = 0x10;
        static constexpr int IBL_ENABLED = 0x20;
        static constexpr int DIRECT_POINT_ENABLED = 0x40;
        static constexpr int DIRECT_DIRECTIONAL_ENABLED = 0x80;

        static constexpr int ALL_LIGHTING_BITS = RSM_DIRECTIONAL_MODE | RSM_POINT_MODE | SSAO_ENABLED | IBL_ENABLED | DIRECT_POINT_ENABLED | DIRECT_DIRECTIONAL_ENABLED;

        static const std::array<std::vector<vk::DescriptorSetLayoutBinding>, 2> COMPOSITION_DESCRIPTOR_SET_LAYOUT_BINDINGS;
        static const GraphicsPipeline::PipelineInfo DEFAULT_PIPELINE_INFO;

        struct EnvironmentUBOData {
            alignas(16) Fog fog;

            alignas(4) int num_of_directional_lights;
            alignas(4) int num_of_point_lights;
            alignas(4) int show_normals;
            alignas(4) int shadows_enabled;
            alignas(4) int gi_mode;
            alignas(4) int num_of_rsm_vpls;
            alignas(4) float rsm_weighting;
            alignas(4) float sampling_radius_max;

            alignas(16) glm::mat4 directional_light_matrices[MAX_NUM_OF_DIRECTIONAL_LIGHTS];
            alignas(16) LightingNode::LightNodeUBOData directional_lights[MAX_NUM_OF_DIRECTIONAL_LIGHTS];
            alignas(16) LightingNode::LightNodeUBOData point_lights[MAX_NUM_OF_POINT_LIGHTS];

            alignas(4) float point_light_shadow_bias;
        };

        struct QuadRenderPushConstants {
            alignas(8) glm::vec2 quad_start;
            alignas(8) glm::vec2 quad_extents;
        };

        bool m_ssao_ubo_set_up = false;

        static constexpr size_t SSAO_KERNEL_SIZE = 16;
        struct SSAOUboData {
            alignas(16) glm::mat4 view;
            alignas(16) glm::mat4 projection;
            alignas(16) glm::vec4 samples[SSAO_KERNEL_SIZE];
            alignas(4) float ssao_intensity;
            alignas(4) float ssao_bias;
            alignas(4) float ssao_radius;
        } m_ssao_ubo_data;

        struct GraphicsPipelineMaterialInfo {
            GraphicsPipeline::PipelineInfo pipeline_info;
            std::vector<GraphicsPipeline::ShaderInfo> shader_infos;
        };

        enum class RasterizationSubpass {
            DEFERRED,
            SCREEN_SPACE_INTERP,
            SSAO,
            SSAO_BLUR,
            COMPOSITION,
            FORWARD
        };

        static std::vector<vk::DescriptorSetLayoutBinding> get_composition_env_layout_bindings() {
            std::vector<vk::DescriptorSetLayoutBinding> layout_bindings{ DEFAULT_PBR_BINDINGS.begin(), DEFAULT_PBR_BINDINGS.end() };

            layout_bindings.insert(layout_bindings.end(), EXTRA_COMPOSITION_BINDINGS.begin(), EXTRA_COMPOSITION_BINDINGS.end());

            return layout_bindings;
        }

        [[nodiscard]] vk::DescriptorImageInfo create_default_image_info(vk::raii::ImageView &image_view) {
            return { *m_default_sampler, *image_view, vk::ImageLayout::eShaderReadOnlyOptimal };
        }

        [[nodiscard]] vk::DescriptorImageInfo get_structure_buffer_image_info();

        void create_sets_if_nonexistent(RenderContextID render_context_id, const std::vector<vk::DescriptorPoolSize> &pool_sizes, 
                                        vk::raii::DescriptorSetLayout &set_layout);

        std::vector<GraphicsPipeline::ShaderInfo> get_depth_map_shader_infos();
        GraphicsPipeline::PipelineInfo get_depth_map_pipeline_info(vk::raii::RenderPass &render_pass, const vk::CullModeFlagBits &cull_mode);

        void depth_only_subpass(std::shared_ptr<gfx::SceneGraphNode> &scene_graph_node_root_strong, bool predraw);
        void gbuffer_subpass(std::shared_ptr<gfx::SceneGraphNode> &scene_graph_node_root_strong, bool predraw);
        void composition_subpass();
        void forward_subpass(std::shared_ptr<gfx::SceneGraphNode> &scene_graph_node_root_strong, bool predraw);
        void occlusion_query_subpass();
        void ssao_subpass();
        void ssao_blur_subpass();

        void draw_tree_subpass(std::shared_ptr<gfx::SceneGraphNode> &scene_graph_root, bool predraw, 
                               const std::function<void(RasterizationLightingEnvironment&, std::shared_ptr<gfx::SceneGraphNode>&, bool)> &draw_tree_func,
                               RasterizationSubpass subpass_type);
        void draw_subpass(bool predraw, const std::function<void(RasterizationLightingEnvironment&)> &draw_func, RasterizationSubpass subpass_type);

        void added_to_camera_environment();
        void categorize_lighting_nodes_recursive(SceneGraphNode &current_node);

        void setup_ssao_ubo();
        void draw_env_imgui();

        std::function<void(void*)> get_full_render_function(const std::unordered_set<RasterizationSubpass> &subpasses_to_enable);
        void partial_occlusion_based_render_function(void*);

        void create_subpass_descriptions();

        RenderPhase &create_gbuffer_render_phase(const glm::uvec2 &resolution, const std::function<void(void*)> &draw_function, 
                                                 const std::optional<std::string> &new_render_phase_name, 
                                                 const std::unordered_map<std::string, vk::PipelineStageFlags> &dependencies,
                                                 const std::function<void(void*)> &before_function = [](void*){});

        RenderPhase &create_ssao_render_phase();
        RenderPhase &create_ssao_blur_composition_render_phase();

        GraphicsPipelineMaterialInfo create_screen_space_interp_pipeline();

        void create_screen_space_interp_render_phases();

        GraphicsPipelineMaterialInfo create_gbuffer_pipeline(vk::raii::RenderPass &render_pass);
        GraphicsPipeline create_composition_pipeline(vk::raii::RenderPass &render_pass, uint32_t subpass_num,
                                                     bool start_constructing_pipeline = true);

        GraphicsPipeline create_ssao_pipeline(vk::raii::RenderPass &render_pass, uint32_t subpass_num,
                                              bool start_constructing_pipeline = true);
        GraphicsPipeline create_ssao_blur_pipeline(vk::raii::RenderPass &render_pass, uint32_t subpass_num,
                                                   bool start_constructing_pipeline = true);

        vk::raii::Sampler create_shadow_mapping_sampler();

        std::array<vk::raii::DescriptorSet, utils::vulkan::MAX_FRAMES_IN_FLIGHT>
            create_environment_descriptor_sets(RenderContextID render_context_id,
                vk::raii::DescriptorSetLayout& descriptor_set_layout);

        vk::raii::DescriptorPool create_environment_descriptor_pools(const std::vector<vk::DescriptorPoolSize>& pool_sizes);

        std::unique_ptr<CapturedSkyBox> create_captured_skybox(const std::vector<glm::vec3>& initial_capture_points,
            uint32_t capture_point_capacity, const vk::Extent2D& captured_sky_box_resolution,
            bool depth_only);

        std::array<BufferRaii, utils::vulkan::MAX_FRAMES_IN_FLIGHT> create_environment_uniform_buffer();

        std::vector<vk::WriteDescriptorSet> get_rsm_texture_writes(RenderContextID render_context_id);

        std::array<std::weak_ptr<LightingNode>, MAX_NUM_OF_DIRECTIONAL_LIGHTS> m_directional_lights;
        std::array<std::weak_ptr<LightingNode>, MAX_NUM_OF_POINT_LIGHTS> m_point_lights;

        vk::DescriptorImageInfo m_null_image_info;
        vk::DescriptorImageInfo m_null_cubemap_image_info;

        std::vector<vk::DescriptorImageInfo> m_temp_sky_box_descriptor_image_infos;
        vk::DescriptorImageInfo m_temp_shadow_map_sampler_info;

        std::array<vk::DescriptorImageInfo, MAX_NUM_OF_POINT_LIGHTS> m_write_descriptor_set_point_light_temp_info;
        vk::DescriptorImageInfo m_caustic_environment_map_sampler_temp_info;
        vk::DescriptorBufferInfo m_environment_ubo_info;

        vk::raii::Sampler m_shadow_mapping_sampler;
        std::array<BufferRaii, utils::vulkan::MAX_FRAMES_IN_FLIGHT> m_environment_uniform_buffer;

        std::unordered_map<RenderContextID, vk::raii::DescriptorPool> m_environment_descriptor_pools;
        std::unordered_map<RenderContextID,
            std::array<vk::raii::DescriptorSet, utils::vulkan::MAX_FRAMES_IN_FLIGHT>> m_environment_descriptor_sets;


        EnvironmentUBOData m_environment_ubo_data{};
        std::optional<Texture> m_caustic_environment_map;
        std::optional<std::string> m_caustic_environment_map_path;
        std::unique_ptr<CapturedSkyBox> m_captured_sky_box;
        std::unique_ptr<CapturedSkyBox> m_captured_sky_box_point_light_shadows;
        bool m_recapture_point_light_shadows_needed = false;
        bool m_point_light_front_culling_allowed = true;
        bool m_front_cull_everything = false;
        bool m_displaying_captured_sky_box = false;

        bool m_prioritizing_deferred;

        vk::raii::DescriptorSetLayout m_composition_env_descriptor_set_layout;
        vk::raii::DescriptorSetLayout m_ssao_env_descriptor_set_layout;
        vk::raii::DescriptorSetLayout m_ssao_blur_env_descriptor_set_layout;
        vk::raii::DescriptorSetLayout m_forward_env_descriptor_set_layout;
        vk::raii::DescriptorSetLayout m_screen_space_interp_descriptor_set_layout;

        std::vector<vk::PipelineColorBlendAttachmentState> m_blend_attachment_states;

        std::vector<vk::AttachmentReference> m_color_gbuffer_attachments;
        std::vector<vk::AttachmentReference> m_color_gbuffer_composition_input_attachments;
        std::vector<vk::AttachmentReference> m_screen_space_interp_input_attachments;

        vk::AttachmentReference m_depth_gbuffer_compositionless_attachment;

        bool m_subpasses_created = false;
        vk::SubpassDescription m_composition_subpass;
        vk::SubpassDescription m_deferred_subpass;
        vk::SubpassDescription m_forward_subpass;
        vk::SubpassDescription m_screen_space_interp_subpass;
        vk::SubpassDescription m_ssao_blur_subpass;

        bool m_using_screen_space_interp;
        glm::uvec2 m_screen_space_interp_grid_size;

        RenderPhase &m_gbuffer_render_phase;
        RenderPhase &m_ssao_render_phase;
        RenderPhase &m_composition_render_phase;

        BufferRaii m_ssao_ubo_buffer;

        RenderPhase *m_occlusion_query_render_phase = nullptr;
        RenderPhase *m_final_high_res_render_phase = nullptr;

        BufferRaii m_occlusion_result_buffer;
        std::vector<int> m_occlusion_result_data;
        std::vector<uint32_t> m_occlusion_query_quad_indices;
        std::array<glm::mat4, utils::vulkan::MAX_FRAMES_IN_FLIGHT> m_occlusion_query_screen_space_transforms;
        glm::vec3 m_past_camera_position;

        std::unordered_set<RasterizationSubpass> m_subpasses_enabled;
        std::vector<QuadRenderPushConstants> m_quads_to_compose;
        std::array<std::vector<QuadRenderPushConstants>, utils::vulkan::MAX_FRAMES_IN_FLIGHT> m_quads_committed;

        uint32_t m_current_subpass_num = 0;

        GraphicsPipelineMaterialInfo m_gbuffer_pipeline_material_info;
        GraphicsPipelineMaterialInfo m_depth_mapping_pipeline_material_info;
        GraphicsPipelineMaterialInfo m_depth_mapping_pipeline_front_culling_material_info;
        GraphicsPipelineMaterialInfo m_forward_pipeline_material_info;

        GraphicsPipeline m_ssao_pipeline;
        GraphicsPipeline m_ssao_blur_pipeline;
        GraphicsPipeline m_composition_pipeline;

        std::unique_ptr<GraphicsPipeline> m_screen_space_interpolation_pipeline;

        // TODO(Bobby): make one graphics pipeline support multiple render passes and
        // TODO(Bobby): subpasses
        GraphicsPipelineMaterialInfo m_gbuffer_screen_space_interp_pipeline_material_info;
        
        std::unique_ptr<GraphicsPipeline> m_composition_final_re_render_pipeline;

        vk::raii::Sampler m_default_sampler;

        std::vector<std::weak_ptr<SceneGraphNode>> m_blended_nodes;
        std::vector<vk::DescriptorImageInfo> m_temp_image_infos;

        std::unique_ptr<gi::ReflectiveShadowMap> m_reflective_shadow_map;
        WhiteNoise m_white_noise;
        Texture m_white_noise_texture;
        vk::DescriptorImageInfo m_white_noise_image_info;
    };
}

#endif