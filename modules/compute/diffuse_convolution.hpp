#ifndef ARISTAEUS_GPU_DIFFUSE_CONVOLUTION_HPP
#define ARISTAEUS_GPU_DIFFUSE_CONVOLUTION_HPP

#include <cmath>

#include <glm/glm.hpp>
#include <unordered_map>

#include "compute_pipeline.hpp"
#include "cube_map_compute_task.hpp"

#include <gfx/vulkan/cube_map_texture.hpp>

namespace aristaeus::gfx::vulkan::compute {
	class DiffuseConvolution : public CubeMapComputeTask {
	public:
		DiffuseConvolution(CubeMapTexture &cube_map_texture, const std::vector<std::string> &skybox_texture_paths,
						   const glm::ivec2 &samples, bool specular_reflection_mode,
                           const std::optional<std::string> &cache_path = {});

		void dispatch(vk::CommandBuffer &command_buffer)override;

		void update_descriptor_set(const glm::ivec2 &image_size, uint32_t index);
	private:
		struct ConvolutionPushConstants {
			alignas(8) glm::ivec2 image_size;
			alignas(8) glm::ivec2 samples;
			alignas(16) glm::vec3 face_up;
			alignas(16) glm::mat4 current_view_transformation;
			alignas(8) float importance_sample_roughness;
		} m_push_constants;

		static constexpr uint32_t WORK_GROUP_X = 16;
		static constexpr uint32_t WORK_GROUP_Y = 16;
		static constexpr uint32_t WORK_GROUP_SIZE = WORK_GROUP_X * WORK_GROUP_Y * 1;

		void send_dispatch_to_vulkan(vk::CommandBuffer &command_buffer, int index);

		static size_t get_number_of_mip_maps(CubeMapTexture &cube_map_texture) {
			return std::floor(std::log2(std::max(cube_map_texture.get_image_size().x, cube_map_texture.get_image_size().y))) + 1 - std::log2(std::max(WORK_GROUP_X, WORK_GROUP_Y));
		}

		VulkanPipeline::ShaderInfo get_compute_shader_info() {
			return {
				.code = utils::read_file("environments/vulkan_environment_shaders/compute/diffuse_convolution.comp.spv"),
				.name = "diffuse_convolution.comp",
				.stage = vk::ShaderStageFlagBits::eCompute
			};
		}

		CubeMapTexture &m_original_environment_map;
		bool m_specular_reflection_mode;
	};
}

#endif