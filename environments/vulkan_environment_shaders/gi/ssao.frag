#version 450

const int SSAO_KERNEL_SIZE = 16;


layout(set = 0, binding = 0) uniform sampler2D position_texture;
layout(set = 0, binding = 1) uniform sampler2D normal_texture;
layout(set = 0, binding = 2) uniform sampler2D noise_texture;

layout (set = 0, binding = 3) uniform SSAOUbo
{
    mat4 view;
	mat4 projection;
	vec3 samples[SSAO_KERNEL_SIZE];
	float ssao_intensity;
	float ssao_bias;
	float ssao_radius;
} ssao_ubo;

layout (location = 0) in vec2 uv_coords;
layout (location = 0) out vec4 color_out;

float ssao() {
	// in view-space
	vec3 gbuffer_position = (ssao_ubo.view * texture(position_texture, uv_coords)).xyz;
	vec3 gbuffer_normal = normalize(texture(normal_texture, uv_coords).rgb);

	ivec2 gbuffer_size = textureSize(position_texture, 0); 
	ivec2 noise_size = textureSize(noise_texture, 0);

	// mapping gbuffer uv coords to noise uv coords
	// (0.5, 0.75) -> (960, 810) / (512, 512) = (1.875, 1.58)
	// noise texture repeats
	vec2 gbuffer_pixel = gbuffer_size * uv_coords;
	vec2 noise_uv = gbuffer_pixel / vec2(4, 4);  

	// Get a random vector using a noise lookup
	// ([0, 1), [0, 1), [0, 1))
	vec3 random_vector = vec3(texture(noise_texture, noise_uv).xy, 0);

	// random vector is now ([-1, 1), [-1, 1))
	random_vector.xy = (2.0 * random_vector.xy) - 1;
	random_vector.xy = normalize(random_vector.xy);

	// construct a TBN matrix from our random_vector projecting our random vector onto the normal vector
	// using graham schdmit
	vec3 tangent = normalize(random_vector - (gbuffer_normal * dot(random_vector, gbuffer_normal)));
	vec3 bitangent = cross(tangent, gbuffer_normal);
	mat3 tbn_matrix = mat3(tangent, bitangent, gbuffer_normal);

	float occlusion_sum = 0.0f;
	for(int i = 0; SSAO_KERNEL_SIZE > i; ++i) {
		vec3 base_sample_pos = tbn_matrix * ssao_ubo.samples[i];
		vec3 sample_pos = gbuffer_position + (ssao_ubo.ssao_radius * base_sample_pos);
		
		// this should be a vector, oriented by some rotated normal
		vec4 texture_offset = vec4(sample_pos, 1.0);

		texture_offset = ssao_ubo.projection * texture_offset;
		texture_offset.xyz /= texture_offset.w;
		texture_offset.xyz = (texture_offset.xyz * 0.5) + 0.5;

		float sampled_depth = (ssao_ubo.view * texture(position_texture, texture_offset.xy)).z;
		float range_check = smoothstep(0, 1, ssao_ubo.ssao_radius / abs(gbuffer_position.z - sampled_depth));
		
		// if we see that our sample's depth is occluded, then we are occluded
		occlusion_sum += (sampled_depth >= (sample_pos.z + ssao_ubo.ssao_bias) ? 1 : 0) * range_check;
	}

	float weighted_occlusion = occlusion_sum / SSAO_KERNEL_SIZE;
	float visibility = max(1 - (ssao_ubo.ssao_intensity * weighted_occlusion), 0);

	return visibility;
}

void main() {
	color_out = vec4(ssao(), 0, 0, 0);
}