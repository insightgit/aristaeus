//
// Created by bobby on 04/01/24.
//

#ifndef PLATFORM_HPP
#define PLATFORM_HPP

#include <string_view>

namespace aristaeus::core::platform {
    static constexpr std::string_view LINUX = "linux";
    static constexpr std::string_view WINDOWS = "windows";

    #ifdef __GNUC__
    #pragma GCC diagnostic push
    #pragma GCC diagnostic ignored "-Wunused-function"
    #endif
    static std::string_view get_platform_string() {
        #ifdef _WIN32
        return WINDOWS;
        #elif __gnu_linux__
        return LINUX;
        #else
        #error "Unsupported platform! Platform supported are currently Windows and Linux"
        #endif
    }

    #ifdef __GNUC__
    #pragma GCC diagnostic pop
    #endif
}

#endif //PLATFORM_HPP
