#ifndef ARISTAEUS_RT_MODULE_RAY_TRACING_RENDERER
#define ARISTAEUS_RT_MODULE_RAY_TRACING_RENDERER

#include <aristaeus/core/singleton.hpp>

namespace aristaeus::rt {
	class RayTracingRenderer : public core::Singleton {
	public:
		RayTracingRenderer();
	private:
		void on_new_node_added();
	};
}

#endif