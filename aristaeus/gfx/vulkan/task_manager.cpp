//
// Created by bobby on 03/03/23.
//

#include "task_manager.hpp"

aristaeus::gfx::vulkan::TaskManager::TaskManager() : m_task_executor(), m_loader_executor() {}

void aristaeus::gfx::vulkan::TaskManager::wait_and_clear_pre_render_taskflows(uint32_t current_frame) {
    for(const std::pair<tf::Taskflow *const, tf::Future<void>> &pre_render_task : m_pre_render_tasks[current_frame]) {
        pre_render_task.second.wait();
    }

    // race condition here!
    m_pre_render_tasks[current_frame].clear();
}
