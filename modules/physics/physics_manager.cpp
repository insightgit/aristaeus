#include <gfx/vulkan/assimp_model.hpp>
#include <gfx/vulkan/core_renderer.hpp>
#include <gfx/vulkan/rasterization_lighting_environment.hpp>

#include "physics_manager.hpp"

#include <type_traits>

aristaeus::physics::PhysicsErrorCallback aristaeus::physics::PhysicsManager::error_callback{};
physx::PxDefaultAllocator aristaeus::physics::PhysicsManager::default_allocator_callback{};


template<>
aristaeus::physics::PhysicsManager
*aristaeus::core::Singleton<aristaeus::physics::PhysicsManager>::instance = nullptr;

aristaeus::gfx::vulkan::GraphicsPipeline::PipelineInfo aristaeus::physics::PhysicsManager::get_pipeline_info(bool is_point) {
	vk::PipelineVertexInputStateCreateInfo vertex_input_stage;
	std::string pipeline_cache_suffix;

	static constexpr vk::VertexInputBindingDescription DEBUG_BINDING_DESCRIPTION = { 0, sizeof(glm::vec3), vk::VertexInputRate::eVertex };
	static constexpr vk::VertexInputAttributeDescription DEBUG_ATTRIBUTE_DESCRIPTION = { 0, 0, vk::Format::eR32G32B32Sfloat, 0 };
	static constexpr vk::DescriptorSetLayoutBinding DESCRIPTOR_SET_LAYOUT_BINDING = { 0, vk::DescriptorType::eUniformBuffer, 1, vk::ShaderStageFlagBits::eAllGraphics };

	auto &core_renderer = utils::get_singleton_safe<gfx::vulkan::CoreRenderer>();

	vk::raii::RenderPass &main_render_pass = *core_renderer.get_default_rasterization_render_pass();

	return {
			.vertex_input_stage = {{}, DEBUG_BINDING_DESCRIPTION, DEBUG_ATTRIBUTE_DESCRIPTION},
			.input_assembly_stage = {{}, is_point ? vk::PrimitiveTopology::ePointList : vk::PrimitiveTopology::eLineList, VK_FALSE},
			.viewport_stage = {{}, 1, nullptr, 1, nullptr},
			.rasterization_stage = {{}, VK_FALSE, VK_FALSE, vk::PolygonMode::eFill, vk::CullModeFlagBits::eBack,
									vk::FrontFace::eCounterClockwise, {}, {}, {}, {}, 1.0f},
			.multisample_stage = {{}, vk::SampleCountFlagBits::e1, VK_FALSE, 1.0f, nullptr, VK_FALSE, VK_FALSE},
			.depth_stencil_stage = {{}, VK_TRUE, VK_TRUE, vk::CompareOp::eLessOrEqual, VK_FALSE, VK_FALSE, {}, {}, 0.0f, 1.0f},
			.blending_stage = {{}, VK_FALSE, vk::LogicOp::eCopy, gfx::vulkan::RasterizationLightingEnvironment::FORWARD_COLOR_BLEND_ATTACHMENT, {0, 0, 0, 0}},

			.dynamic_stage = {{}, {}},
			.push_content_ranges = {vk::PushConstantRange{vk::ShaderStageFlagBits::eFragment, 0, sizeof(glm::vec3)}},

			.descriptor_bindings = {{DESCRIPTOR_SET_LAYOUT_BINDING}},

			.sub_pass = core_renderer.get_default_rasterization_subpass(),
			.pipeline_cache_path = std::string("physics_manager_debug_pipeline_cache_") + (is_point ? "point" : "line"),
			.render_pass = main_render_pass
	};
}

std::vector<aristaeus::gfx::vulkan::GraphicsPipeline::ShaderInfo> aristaeus::physics::PhysicsManager::get_shader_infos() {
	return {
		{
				.code = utils::read_file("environments/vulkan_environment_shaders/physics/debug.vert.spv"),
				.name = "physics/debug.vert",
				.stage = vk::ShaderStageFlagBits::eVertex
		},
		{
				.code = utils::read_file("environments/vulkan_environment_shaders/physics/debug.frag.spv"),
				.name = "physics/debug.frag",
				.stage = vk::ShaderStageFlagBits::eFragment
		}
	};
}

aristaeus::physics::PhysicsManager::PhysicsManager(const nlohmann::json &physics_module_options) : m_time_step(physics_module_options.contains("time_step") ? physics_module_options.at("time_step").get<float>() : DEFAULT_TIME_STEP),
																								   m_visualization_mode(physics_module_options.contains("visualization_mode") ? physics_module_options.at("visualization_mode").get<bool>() : false),
																								   m_debug_point_pipeline(utils::get_singleton_safe<gfx::vulkan::CoreRenderer>(), get_shader_infos(), get_pipeline_info(true)),
																								   m_debug_line_pipeline(utils::get_singleton_safe<gfx::vulkan::CoreRenderer>(), get_shader_infos(), get_pipeline_info(false)){
	m_foundation.reset(PxCreateFoundation(PX_PHYSICS_VERSION, default_allocator_callback, error_callback));

	if (m_foundation == nullptr) {
		throw std::runtime_error("Couldn't create foundation!");
	}

#if !defined(NDEBUG) && defined(_WIN32)
	m_visual_debugger.reset(physx::PxCreatePvd(*m_foundation));
	m_visual_debugger_transport.reset(physx::PxDefaultPvdSocketTransportCreate("127.0.0.1", 5425, 10));

	m_visual_debugger->connect(*m_visual_debugger_transport, physx::PxPvdInstrumentationFlag::eALL);
#endif

	#if !defined(NDEBUG) && defined(_WIN32)
	m_physics.reset(PxCreatePhysics(PX_PHYSICS_VERSION, *m_foundation, physx::PxTolerancesScale(), true, m_visual_debugger.get()));
	#else
	m_physics.reset(PxCreatePhysics(PX_PHYSICS_VERSION, *m_foundation, physx::PxTolerancesScale(), true, nullptr));
	#endif
	if (m_physics == nullptr) {
		throw std::runtime_error("Couldn't create PhysX physics!");
	}

	physx::PxSceneDesc sceneDesc(m_physics->getTolerancesScale());
	sceneDesc.gravity = physx::PxVec3(0.0f, -9.81f, 0.0f);
	m_cpu_dispatcher.reset(physx::PxDefaultCpuDispatcherCreate(2));
	sceneDesc.cpuDispatcher = m_cpu_dispatcher.get();
	sceneDesc.filterShader = physx::PxDefaultSimulationFilterShader;

	m_scene.reset(m_physics->createScene(sceneDesc));

	m_default_material.reset(m_physics->createMaterial(0, 0, 0));

	if (m_scene == nullptr) {
		throw std::runtime_error("Couldn't create PhysX scene!");
	}

	if (m_visualization_mode) {
		m_scene->setVisualizationParameter(physx::PxVisualizationParameter::eSCALE, 1.0f);
		m_scene->setVisualizationParameter(physx::PxVisualizationParameter::eACTOR_AXES, 2.0f);
	}

	utils::get_singleton_safe<core::ClassDB>().register_type<GeometryInfo>(geometry_info_parse_function);

	utils::get_singleton_safe<gfx::vulkan::CoreRenderer>().add_callback_for_event<void*, PhysicsManager>(std::string{ gfx::vulkan::CoreRenderer::DRAW_FRAME_EVENT }, "draw_frame_physics_manager",
	[](PhysicsManager &physics_manager, void*) {
		physics_manager.step();
	}, *this);

	utils::get_singleton_safe<gfx::vulkan::CoreRenderer>().add_callback_for_event<void*, PhysicsManager>(std::string{ gfx::vulkan::CoreRenderer::NEW_FRAME_EVENT }, "new_frame_physics_manager",
	[](PhysicsManager &physics_manager, void*) {
		physics_manager.waitForStepResults();
		physics_manager.call_event<void*>(std::string{ PHYSICS_STEP_FINISHED_EVENT }, nullptr);
	}, *this);

	utils::get_singleton_safe<gfx::vulkan::CoreRenderer>().add_callback_for_event<void*, PhysicsManager>(std::string{ gfx::vulkan::CoreRenderer::DEFAULT_RENDER_PASS_DRAW_EVENT }, "forward_draw_physics_manager",
	[](PhysicsManager &physics_manager, void*) {
		if (physics_manager.m_visualization_mode) {
			physics_manager.debug_render();
		}
	}, *this);

	add_event<void*>(std::string{ PHYSICS_STEP_FINISHED_EVENT });
}

void aristaeus::physics::PhysicsManager::debug_render() {
	const physx::PxRenderBuffer &rb = m_scene->getRenderBuffer();

	gfx::vulkan::CoreRenderer &core_renderer = utils::get_singleton_safe<gfx::vulkan::CoreRenderer>();
	gfx::vulkan::RenderPhase *current_render_phase = core_renderer.get_current_render_phase();

	assert(current_render_phase != nullptr);

	vk::CommandBuffer command_buffer =
		core_renderer.get_graphics_command_buffer_manager().get_command_buffer_for_thread(core_renderer.get_current_render_context_id());
	uint32_t current_frame = core_renderer.get_current_frame();

	size_t required_buffer_size = (rb.getNbLines() + rb.getNbPoints()) * sizeof(glm::vec3);

	if (m_debug_render_buffers[current_frame] == nullptr || m_debug_render_buffers[current_frame]->buffer_size < required_buffer_size) {
		// we are not making a staging buffer because we will be updating this buffer every frame anyways
		m_debug_render_buffers[current_frame] = core_renderer.get_memory_manager().create_buffer(core_renderer, required_buffer_size, 
											static_cast<vk::BufferUsageFlags>(vk::BufferUsageFlagBits::eIndexBuffer | vk::BufferUsageFlagBits::eVertexBuffer), 
											vk::SharingMode::eExclusive, VMA_ALLOCATION_CREATE_HOST_ACCESS_SEQUENTIAL_WRITE_BIT,
											VMA_MEMORY_USAGE_AUTO, "physics-manager-render-buffer");
	}

	void *debug_render_buffer_data;
	float *debug_render_buffer_data_float;

	vmaMapMemory(m_debug_render_buffers[current_frame]->allocator, m_debug_render_buffers[current_frame]->allocation, &debug_render_buffer_data);
	
	debug_render_buffer_data_float = reinterpret_cast<float*>(debug_render_buffer_data);

	std::vector<float> points;
	std::vector<float> lines;

	for (physx::PxU32 i = 0; i < rb.getNbPoints(); i++)
	{
		const physx::PxDebugPoint &point = rb.getPoints()[i];
		
		points.push_back(point.pos.x);
		points.push_back(point.pos.y);
		points.push_back(point.pos.z);
	}

	for (physx::PxU32 i = 0; i < rb.getNbLines(); i++)
	{
		const physx::PxDebugLine &line = rb.getLines()[i];

		lines.push_back(line.pos0.x);
		lines.push_back(line.pos0.y);
		lines.push_back(line.pos0.z);

		lines.push_back(line.pos1.x);
		lines.push_back(line.pos1.y);
		lines.push_back(line.pos1.z);
	}

	std::memcpy(debug_render_buffer_data_float, points.data(), points.size());
	std::memcpy(&debug_render_buffer_data_float[points.size()], lines.data(), lines.size());

	vmaUnmapMemory(m_debug_render_buffers[current_frame]->allocator, m_debug_render_buffers[current_frame]->allocation);


	// point drawing

	vk::raii::DescriptorSet &camera_descriptor_set = core_renderer.get_perspective_camera_environment().get_camera_descriptor_set_to_bind();

	m_debug_point_pipeline.bind_to_command_buffer(command_buffer, core_renderer);

	command_buffer.bindDescriptorSets(vk::PipelineBindPoint::eGraphics, *m_debug_point_pipeline.get_pipeline_layout(), 0, *camera_descriptor_set, {});

	command_buffer.pushConstants(*m_debug_point_pipeline.get_pipeline_layout(), vk::ShaderStageFlagBits::eFragment, 0,
								 sizeof(glm::vec3), static_cast<const void*>(&VISUALIZATION_POINT_COLOR));

	command_buffer.bindVertexBuffers(0, m_debug_render_buffers[current_frame]->buffer, {0});

	command_buffer.draw(points.size() / 3, 1, 0, 0);

	// line drawing
	
	m_debug_line_pipeline.bind_to_command_buffer(command_buffer, core_renderer);

	command_buffer.bindDescriptorSets(vk::PipelineBindPoint::eGraphics, *m_debug_line_pipeline.get_pipeline_layout(), 0, *camera_descriptor_set, {});

	command_buffer.pushConstants(*m_debug_point_pipeline.get_pipeline_layout(), vk::ShaderStageFlagBits::eFragment, 0,
								 sizeof(glm::vec3), static_cast<const void*>(&VISUALIZATION_LINE_COLOR));

	command_buffer.bindVertexBuffers(0, m_debug_render_buffers[current_frame]->buffer, { points.size() });

	command_buffer.draw(lines.size() / 6, 1, 0, 0);
}

void aristaeus::physics::PhysicsManager::add_actor(physx::PxActor &physics_actor, gfx::SceneGraphNode &scene_graph_node) {
	m_scene->addActor(physics_actor);

	std::pair<physx::PxActor*, std::reference_wrapper<gfx::SceneGraphNode>> scene_graph_node_pair{ &physics_actor, scene_graph_node };

	m_scene_graph_nodes.insert(scene_graph_node_pair);
}

void aristaeus::physics::PhysicsManager::remove_actor(physx::PxActor &physics_actor) {
	m_scene->removeActor(physics_actor);
	m_scene_graph_nodes.erase(&physics_actor);
}

void aristaeus::physics::PhysicsManager::step() {
	std::chrono::system_clock::time_point now_time = std::chrono::system_clock::now();

	double microseconds_time_since_physics_step = std::chrono::duration_cast<std::chrono::microseconds>(now_time - m_last_physics_step_time).count() * (1e+6);
	
	if (microseconds_time_since_physics_step >= m_time_step) {
		m_scene->simulate(m_time_step);
		m_last_physics_step_time = now_time;
	}
}

void aristaeus::physics::PhysicsManager::waitForStepResults() {
	m_scene->fetchResults(true);
}

std::shared_ptr<physx::PxMaterial> aristaeus::physics::PhysicsManager::create_cached_material(const MaterialInfo &material_info) {
	if (m_materials_cache.contains(material_info)) {
		std::shared_ptr<physx::PxMaterial> cached_material = m_materials_cache.at(material_info).lock();

		if (cached_material != nullptr) {
			return cached_material;
		}
	}

	std::shared_ptr<physx::PxMaterial> new_cached_material = create_physx_shared_ptr<physx::PxMaterial>(m_physics->createMaterial(material_info.static_friction, 
																																  material_info.dynamic_friction, 
																																  material_info.restitution));

	m_materials_cache[material_info] = new_cached_material;

	return new_cached_material;
}

std::shared_ptr<aristaeus::physics::PhysxShape> aristaeus::physics::PhysicsManager::create_cached_shape(const GeometryInfo &geometry_info, gfx::SceneGraphNode *relative_node) {
	GeometryInfo cached_geometry_info = geometry_info;

	if (!cached_geometry_info.string_arg.empty()) {
		cached_geometry_info.string_arg = gfx::SceneGraphNode::concatenate_node_paths({ relative_node->get_node_path(), geometry_info.string_arg });
	}

	if (m_shape_cache.contains(cached_geometry_info)) {
		std::shared_ptr<PhysxShape> cached_shape = m_shape_cache.at(cached_geometry_info).lock();

		if (cached_shape != nullptr) {
			return cached_shape;
		}
	}

	std::shared_ptr<PhysxShape> new_physx_shape = std::make_shared<PhysxShape>();
	physx::PxMaterial *material_to_use = m_default_material.get();

	if (geometry_info.material_info.has_value()) {
		new_physx_shape->custom_material = create_cached_material(*geometry_info.material_info);
		new_physx_shape->material_info = *geometry_info.material_info;

		assert(new_physx_shape->custom_material != nullptr);

		material_to_use = new_physx_shape->custom_material.get();
	}

	std::unique_ptr<physx::PxGeometry> shape_geometry;
	std::optional<physx::PxSDFDesc> sdf_descriptor;

	if ((geometry_info.geom_type == GeometryType::TRIANGLE_MESH ||
         geometry_info.geom_type == GeometryType::TRIANGLE_MESH_NODE) &&
		geometry_info.numeric_args[0].uint32_arg) {
		sdf_descriptor = physx::PxSDFDesc();

		sdf_descriptor->spacing = geometry_info.numeric_args[1].float_arg;
		sdf_descriptor->subgridSize = geometry_info.numeric_args[2].uint32_arg;

		switch (geometry_info.numeric_args[3].uint32_arg) {
		case 16u:
			sdf_descriptor->bitsPerSubgridPixel = physx::PxSdfBitsPerSubgridPixel::e16_BIT_PER_PIXEL;
			break;
		case 32u:
			sdf_descriptor->bitsPerSubgridPixel = physx::PxSdfBitsPerSubgridPixel::e32_BIT_PER_PIXEL;
			break;
		default:
			sdf_descriptor->bitsPerSubgridPixel = physx::PxSdfBitsPerSubgridPixel::e8_BIT_PER_PIXEL;
		}

        sdf_descriptor->numThreadsForSdfConstruction = std::thread::hardware_concurrency();
	}

	static_assert(std::is_same_v<physx::PxReal, float> && sizeof(float) == sizeof(uint32_t));

	switch (geometry_info.geom_type) {
	case GeometryType::BOX:
		shape_geometry.reset(new physx::PxBoxGeometry(geometry_info.numeric_args[0].float_arg, 
													  geometry_info.numeric_args[1].float_arg, 
													  geometry_info.numeric_args[2].float_arg));
		break;
	case GeometryType::SPHERE:
		shape_geometry.reset(new physx::PxSphereGeometry(geometry_info.numeric_args[0].float_arg));
		break;
	case GeometryType::CAPSULE:
		shape_geometry.reset(new physx::PxCapsuleGeometry(geometry_info.numeric_args[0].float_arg,
														  geometry_info.numeric_args[1].float_arg));
		break;
	case GeometryType::PLANE:
		shape_geometry.reset(new physx::PxPlaneGeometry());
		break;
	case GeometryType::TRIANGLE_MESH: {
		gfx::vulkan::AssimpMesh::MeshGeometryData mesh_geometry_data;

		size_t number_of_vertex_datas = 3 * geometry_info.numeric_args[4].uint32_arg;

		for (size_t i = 5; number_of_vertex_datas + 1 > i; i += 3) {
			mesh_geometry_data.vertices.emplace_back(geometry_info.numeric_args[i].float_arg,
                                                     geometry_info.numeric_args[i + 1].float_arg,
				                                     geometry_info.numeric_args[i + 2].float_arg);
		}

		for (size_t i = number_of_vertex_datas + 1; geometry_info.numeric_args.size() > i; ++i) {
			mesh_geometry_data.indices.push_back(geometry_info.numeric_args[i].float_arg);
		}

		shape_geometry.reset(create_mesh_geometry(mesh_geometry_data, sdf_descriptor));
		break;
	}	
	case GeometryType::TRIANGLE_MESH_NODE:
		bool is_node_path_relative = static_cast<bool>(geometry_info.numeric_args[4].uint32_arg);
		uint32_t mesh_index = geometry_info.numeric_args[5].uint32_arg;

		if (is_node_path_relative) {
			if (relative_node == nullptr) {
				throw new std::runtime_error("We need a valid relative node to deal with geometry infos with relative node paths!");
			}

			std::optional<std::reference_wrapper<gfx::SceneGraphNode>> tri_mesh_node =
                    relative_node->get_node_with_path(geometry_info.string_arg, false);

			if (!tri_mesh_node.has_value()) {
				throw new std::runtime_error("Couldn't find " + geometry_info.string_arg + " relative node as referenced by a geometry info in " + relative_node->get_node_path());
			}
			else {
				// AssimpModel: get_geometry_data_of_meshes
				auto *assimp_model = dynamic_cast<gfx::vulkan::AssimpModel*>(&tri_mesh_node->get());

				if (assimp_model == nullptr) {
					throw new std::runtime_error("Node " + tri_mesh_node->get().get_node_path() + " is not an AssimpModel!");
				}

				gfx::vulkan::AssimpMesh::MeshGeometryData mesh_geometry_data = assimp_model->get_geometry_data_of_meshes()[mesh_index];

				shape_geometry.reset(create_mesh_geometry(mesh_geometry_data, sdf_descriptor));
			}
		}
		else {
			throw new std::runtime_error("absolute paths not implemented yet!");
		}

		break;
	}

	if (new_physx_shape->shape == nullptr) {
		assert(shape_geometry != nullptr);
		new_physx_shape->shape = create_physx_shared_ptr<physx::PxShape>(m_physics->createShape(*shape_geometry, *material_to_use, false));
	}

	m_shape_cache[cached_geometry_info] = new_physx_shape;

	return new_physx_shape;
}

physx::PxTriangleMeshGeometry *aristaeus::physics::PhysicsManager::create_mesh_geometry(const gfx::vulkan::AssimpMesh::MeshGeometryData &mesh_geo_data,
																						std::optional<physx::PxSDFDesc> sdf_info) {
    auto &physics_manager = utils::get_singleton_safe<PhysicsManager>();

	static_assert(sizeof(glm::vec3) == sizeof(physx::PxVec3));
	static_assert(sizeof(uint32_t) == sizeof(physx::PxU32));

	assert(mesh_geo_data.indices.size() % 3 == 0);

    physx::PxTriangleMeshDesc meshDesc;
    meshDesc.points.count = mesh_geo_data.vertices.size();
    meshDesc.points.stride = sizeof(glm::vec3);
    meshDesc.points.data = &mesh_geo_data.vertices[0];

	meshDesc.triangles.count = mesh_geo_data.indices.size() / 3;
	meshDesc.triangles.stride = 3 * sizeof(uint32_t);
	meshDesc.triangles.data = &mesh_geo_data.indices[0];

	
	if (sdf_info.has_value()) {
		meshDesc.sdfDesc = &*sdf_info;
		assert(sdf_info->isValid());
	}

    physx::PxTolerancesScale scale;
    physx::PxCookingParams params(scale);

    physx::PxDefaultMemoryOutputStream writeBuffer;
    physx::PxTriangleMeshCookingResult::Enum result;

    bool status = PxCookTriangleMesh(params, meshDesc, writeBuffer, &result);
    if (!status) {
        throw std::runtime_error("Couldn't cook triangle mesh physx shapes!");
    }

	// TODO(Bobby): save this cooked data
    physx::PxDefaultMemoryInputData readBuffer(writeBuffer.getData(), writeBuffer.getSize());

	physx::PxTriangleMesh *triangle_mesh = physics_manager.get_physics().createTriangleMesh(readBuffer);
	physx::PxTriangleMeshGeometry *triangle_mesh_geometry = new physx::PxTriangleMeshGeometry(triangle_mesh);

	// TODO(Bobby): make the lifetime of this physx::PxTriangleMesh track with the lifetime of PxTriangleMeshGeometry
	//triangle_mesh->release();

    return triangle_mesh_geometry;
}

aristaeus::physics::GeometryInfo aristaeus::physics::PhysicsManager::geometry_info_parse_function(const nlohmann::json &geometry_json) {
	std::string shape_type = utils::string_to_lower(geometry_json.at("shape_type").get<std::string>());

	GeometryInfo geometry_info;

	if (geometry_json.contains("custom_material")) {
		const nlohmann::json &custom_material_json = geometry_json.at("custom_material");

		geometry_info.material_info = MaterialInfo{
			.dynamic_friction = custom_material_json.at("dynamic_friction").get<physx::PxReal>(),
			.static_friction = custom_material_json.at("static_friction").get<physx::PxReal>(),
			.restitution = custom_material_json.at("static_friction").get<physx::PxReal>(),
		};
	}

	// TODO(Bobby): find a way to cache shapes
	if (shape_type == "box") {
		glm::vec3 box_extents = utils::get_vec3_json(geometry_json.at("box_extents"));

		geometry_info.geom_type = GeometryType::BOX;
		
		for (int i = 0; box_extents.length() > i; ++i) {
			geometry_info.numeric_args.push_back(GeometryInfo::NumericArg{ .float_arg = box_extents[i] });
		}
	}
	else if (shape_type == "sphere") {
		geometry_info.geom_type = GeometryType::SPHERE;
		geometry_info.numeric_args.push_back(GeometryInfo::NumericArg{ .float_arg = geometry_json.at("radius").get<float>() });
	}
	else if (shape_type == "capsule") {
		geometry_info.geom_type = GeometryType::CAPSULE;
		geometry_info.numeric_args.push_back(GeometryInfo::NumericArg{ .float_arg = geometry_json.at("radius").get<float>() });
		geometry_info.numeric_args.push_back(GeometryInfo::NumericArg{ .float_arg = geometry_json.at("half_height").get<float>() });
	}
	else if (shape_type == "plane") {
		geometry_info.geom_type = GeometryType::PLANE;
	}
	else if (shape_type == "triangle_mesh") {
		bool sdf_cooking = false;
		float sdf_spacing = SDF_DEFAULT_SPACING;
		uint32_t sdf_subgrid_size = SDF_DEFAULT_SUBGRID_SIZE;
		uint32_t sdf_bits_per_subgrid_pixel = SDF_BITS_PER_SUBGRID_PIXEL;

		if (geometry_json.contains("sdf_cooking") && geometry_json.at("sdf_cooking").contains("enabled") && 
			geometry_json.at("sdf_cooking").at("enabled").get<bool>()) {
			const nlohmann::json &sdf_json = geometry_json.at("sdf_cooking");

			sdf_cooking = true;

			if (sdf_json.contains("spacing")) {
				sdf_spacing = sdf_json.at("spacing").get<float>();
			}

			if (sdf_json.contains("subgrid_size")) {
				sdf_spacing = sdf_json.at("subgrid_size").get<uint32_t>();
			}

			if (sdf_json.contains("bits_per_subgrid_pixel")) {
				sdf_spacing = sdf_json.at("bits_per_subgrid_pixel").get<uint32_t>();
			}
		}

		geometry_info.numeric_args.push_back(GeometryInfo::NumericArg{.uint32_arg = sdf_cooking});
		geometry_info.numeric_args.push_back(GeometryInfo::NumericArg{.float_arg = sdf_spacing});
		geometry_info.numeric_args.push_back(GeometryInfo::NumericArg{.uint32_arg = sdf_subgrid_size});
		geometry_info.numeric_args.push_back(GeometryInfo::NumericArg{.uint32_arg = sdf_bits_per_subgrid_pixel});

		if (geometry_json.contains("model_node_path")) {
			bool is_node_path_relative = true;
			uint32_t mesh_index = 0;

			if (geometry_json.contains("model_node_path_relative")) {
				is_node_path_relative = geometry_json.at("model_node_path_relative").get<bool>();
			}

			if (geometry_json.contains("mesh_index")) {
				mesh_index = geometry_json.at("mesh_index").get<uint32_t>();
			}

			geometry_info.geom_type = GeometryType::TRIANGLE_MESH_NODE;

			geometry_info.numeric_args.push_back(GeometryInfo::NumericArg{ .uint32_arg = is_node_path_relative });
			geometry_info.numeric_args.push_back(GeometryInfo::NumericArg{ .uint32_arg = mesh_index });

			geometry_info.string_arg = geometry_json.at("model_node_path").get<std::string>();
		}
		else {
			geometry_info.geom_type = GeometryType::TRIANGLE_MESH;

			geometry_info.numeric_args.push_back(GeometryInfo::NumericArg{ .uint32_arg = static_cast<uint32_t>(geometry_json.at("vertices").size()) });

			for (const nlohmann::json &vertex : geometry_json.at("vertices")) {
				for (int i = 0; 3 > i; ++i) {
					geometry_info.numeric_args.push_back(GeometryInfo::NumericArg{ .float_arg = vertex.at(i).get<float>() });
				}
			}

			for (const nlohmann::json &index : geometry_json.at("indices")) {
				geometry_info.numeric_args.push_back(GeometryInfo::NumericArg{ .uint32_arg = index.get<uint32_t>() });
			}
		}
	}
	else {
		throw std::runtime_error("Couldn't recognize shape type " + shape_type);
	}

	return geometry_info;
}