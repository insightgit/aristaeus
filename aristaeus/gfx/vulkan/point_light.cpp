//
// Created by bobby on 8/28/21.
//

#include "point_light.hpp"
#include "rasterization_lighting_environment.hpp"

#include <utility>

ARISTAEUS_CLASS_IMPL(aristaeus::gfx::vulkan::PointLight, aristaeus::core::StdTypeInfoRef(typeid(aristaeus::gfx::vulkan::LightingNode)));

aristaeus::gfx::vulkan::PointLight::LightColors aristaeus::gfx::vulkan::PointLight::DEFAULT_LIGHT_COLORS = {
    .diffuse = glm::vec3(0.8f, 0.8f, 0.8f),
    .light_box_color = glm::vec3(1.0f, 1.0f, 1.0f)
};

aristaeus::gfx::vulkan::PointLight::LightAttuention aristaeus::gfx::vulkan::PointLight::DEFAULT_LIGHT_ATTENUATION = {
    .constant = 1.0f,
    .linear = 0.07f,
    .quadratic = 0.017f
};

void aristaeus::gfx::vulkan::PointLight::classdb_register() {
    assert(aristaeus::core::ClassDB::instance != nullptr);
    auto &class_db = *aristaeus::core::ClassDB::instance;

    class_db.register_type<LightAttuention>([](const nlohmann::json::value_type &json) {
        LightAttuention light_attuention = DEFAULT_LIGHT_ATTENUATION;

        if(json.contains("constant")) {
            light_attuention.constant = json.at("constant").get<float>();
        }

        if(json.contains("linear")) {
            light_attuention.linear = json.at("linear").get<float>();
        }

        if(json.contains("quadratic")) {
            light_attuention.quadratic = json.at("quadratic").get<float>();
        }

        return light_attuention;
    });
    class_db.register_type<LightColors>([](const nlohmann::json::value_type &json) {
        LightColors light_colors = DEFAULT_LIGHT_COLORS;

        if(json.contains("light_box")) {
            light_colors.light_box_color = utils::get_vec3_json(json.at("light_box"));
        }

        if(json.contains("generate_colors_from_box_color")) {
            auto generation_scale = glm::vec3(1.0f);

            if(json.contains("box_color_generation_scale")) {
                generation_scale = glm::vec3(json.at("box_color_generation_scale"));
            }
            light_colors.diffuse *= light_colors.light_box_color * generation_scale;
        } else if(json.contains("diffuse")) {
            light_colors.diffuse = utils::get_vec3_json(json.at("diffuse"));
        }


        return light_colors;
    });

    class_db.register_property_class_ptr<PointLight, AssimpModel*>("light_emitter_model", [](PointLight &point_light) { return point_light.m_emitter_model.get(); },
                                                                   [](PointLight& point_light, AssimpModel* model) {
                                                                         point_light.m_emitter_model.reset(model);
                                                                   }, {});

    class_db.register_property<PointLight, LightColors>("colors", [](PointLight &point_light) {return point_light.m_point_light_colors; },
    [](PointLight &point_light, LightColors light_colors) {
        point_light.m_point_light_colors = light_colors;
        if (point_light.m_emitter_model != nullptr) {
            point_light.m_emitter_model->set_light_box_color(point_light.m_point_light_colors.light_box_color);
        }
        point_light.update_point_light_ubo();
    }, [](){return DEFAULT_LIGHT_COLORS;});
    class_db.register_property<PointLight, LightAttuention>("attenuation", [](PointLight& point_light) {return point_light.m_point_light_attenuation; },
        [](PointLight& point_light, LightAttuention point_light_attuention) {
            point_light.m_point_light_attenuation = point_light_attuention;
            point_light.update_point_light_ubo();
        }, []() {return DEFAULT_LIGHT_ATTENUATION; });

    class_db.register_event_callback_on_construction<PointLight, void*>(std::string{SceneGraphNode::ENTER_TREE_EVENT}, "point_light_enter_tree", [](PointLight &point_light, void*) {
        point_light.on_parsing_done();
    });

    class_db.register_event_callback_on_construction<PointLight, SceneGraphNode::Transform>(std::string{SceneGraphNode::TRANSFORM_CHANGED_EVENT}, "point_light_transform_changed", [](PointLight& point_light, SceneGraphNode::Transform) {
        point_light.update_point_light_ubo();
    });

    class_db.register_event_callback_on_construction<PointLight, void*>(std::string{SceneGraphNode::GLOBAL_TRANSFORM_CHANGED_EVENT}, "point_light_global_transform_changed", [](PointLight& point_light, void*) {
        point_light.update_point_light_ubo();
    });
}

void aristaeus::gfx::vulkan::PointLight::on_parsing_done() {
    assert(m_emitter_model != nullptr);

    m_emitter_model->set_light_emitter(true);
    m_emitter_model->set_light_box_color(m_point_light_colors.light_box_color);

    add_child(m_emitter_model);

    add_node_group(std::string{ gfx::vulkan::RasterizationLightingEnvironment::FORWARD_RENDER_NODE_GROUP });
    m_emitter_model->add_node_group(std::string{ gi::ReflectiveShadowMap::REFLECTIVE_SHADOW_MAP_RENDER_EXCLUSION_GROUP_NAME });
}

void aristaeus::gfx::vulkan::PointLight::update_point_light_ubo() {
    glm::vec3 parent_position = get_parent_transform().position;

    m_light_node_ubo_data = LightingNode::LightNodeUBOData {
        .constant_attenuation = m_point_light_attenuation.constant,
        .linear_attenuation = m_point_light_attenuation.linear,
        .quadratic_attenuation = m_point_light_attenuation.quadratic,

        .light_active = 1,
        .light_type = UBO_POINT_LIGHT_TYPE_ID,

        .diffuse = m_point_light_colors.diffuse,

        .position_direction = parent_position + get_position()
    };
}
