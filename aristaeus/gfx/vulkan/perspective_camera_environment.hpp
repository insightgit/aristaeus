//
// Created by bobby on 6/8/22.
//

#ifndef ARISTAEUS_PERSPECTIVE_CAMERA_ENVIRONMENT_HPP
#define ARISTAEUS_PERSPECTIVE_CAMERA_ENVIRONMENT_HPP

#include "window.hpp"
#include "uniform_buffer.hpp"

namespace aristaeus::gfx {
    class SceneGraphNode;
}

namespace aristaeus::gfx::vulkan {
    class CoreRenderer;
    class LightingEnvironment;

    class PerspectiveCameraEnvironment {
    public:
        struct DirectionalLightShadowsInfo {
            std::array<float, 6> orthographic_matrix;
            glm::vec3 target_position;
            glm::vec3 eye_position;
            bool follow_player_camera;
            bool enabled;
        };

        static constexpr vk::DescriptorSetLayoutBinding DESCRIPTOR_SET_BINDING {
            0, vk::DescriptorType::eUniformBuffer, 1, 
            vk::ShaderStageFlagBits::eFragment |
            vk::ShaderStageFlagBits::eVertex |
            vk::ShaderStageFlagBits::eTessellationEvaluation |
            vk::ShaderStageFlagBits::eTessellationControl |
            vk::ShaderStageFlagBits::eGeometry
        };

        struct OrthographicCapture {
            float left;
            float right;
            float bottom;
            float top;
        };

        struct Capture {
            float aspect_ratio;
            float fov;
            float pitch;
            glm::vec3 position;
            glm::vec3 up;
            float yaw;
            float z_far;
            float z_near;

            // if view_direction is set, yaw and pitch are disregarded
            std::optional<glm::vec3> view_direction;

            std::optional<OrthographicCapture> orthographic_capture;

            [[nodiscard]] glm::vec3 get_view_direction() const;

            [[nodiscard]] glm::mat4 get_view_matrix() const;
            [[nodiscard]] glm::mat4 get_projection_matrix() const;
        };

        PerspectiveCameraEnvironment(CoreRenderer &core_renderer, float fov, float aspect_ratio, float z_near,
                                     float z_far, float mouse_sensitivity, bool enable_up_down_movement, const DirectionalLightShadowsInfo &shadows_info,
                                     float camera_pitch_degrees = 0.0f, float camera_yaw_degrees = 180.0f);

        PerspectiveCameraEnvironment(PerspectiveCameraEnvironment &&other);

        [[nodiscard]] float get_aspect_ratio() const;
        [[nodiscard]] float get_pitch_degrees() const;
        [[nodiscard]] float get_yaw_degrees() const;
        [[nodiscard]] float get_fov_degrees() const;

        [[nodiscard]] float get_z_far() const;
        [[nodiscard]] float get_z_near() const;

        [[nodiscard]] glm::mat4 get_view_matrix() const {
            return m_matrix_ubo_content.view_matrix;
        }

        [[nodiscard]] glm::mat4 get_projection_matrix() const {
            return m_matrix_ubo_content.projection_matrix;
        }

        [[nodiscard]] glm::vec2 get_last_mouse_position() const {
            return m_last_mouse_position;
        }

        [[nodiscard]] glm::vec3 get_camera_position() const;
        [[nodiscard]] glm::vec3 get_camera_front() const {
            return m_camera_front_target;
        }
        [[nodiscard]] glm::vec3 get_camera_up() const {
            return m_camera_up;
        }

        [[nodiscard]] LightingEnvironment &get_lighting_env(size_t index) {
            return *m_lighting_environments[index];
        }

        [[nodiscard]] std::string get_default_lighting_env_tag() const {
            return m_default_lighting_environment_tag;
        }

        [[nodiscard]] std::shared_ptr<gfx::SceneGraphNode> get_scene_graph_root_node() {
            return m_scene_graph_node_root;
        }

        void set_default_lighting_env_tag(const std::string &lighting_env_tag) {
            if (m_default_lighting_environment_tag != lighting_env_tag) {
                m_default_lighting_environment_tag = lighting_env_tag;

                assert(m_lighting_environment_tags.contains(m_default_lighting_environment_tag));

                assign_default_lighting_environment_to_tree();
            }
        }

        [[nodiscard]] vk::raii::DescriptorSet &get_camera_descriptor_set_to_bind();

        void set_pitch_degrees(float camera_pitch_degrees) {
            m_mouse_pitch = camera_pitch_degrees;

            m_matrix_ubo_content.projection_matrix = reset_projection_matrix(m_fov, m_aspect_ratio, m_z_near, m_z_far,
                                                                             m_mouse_pitch, m_mouse_yaw);
        }

        void set_yaw_degrees(float camera_yaw_degrees) {
            m_mouse_yaw = camera_yaw_degrees;

            m_matrix_ubo_content.projection_matrix = reset_projection_matrix(m_fov, m_aspect_ratio, m_z_near, m_z_far,
                                                                             m_mouse_pitch, m_mouse_yaw);
        }

        void set_fov_degrees(float fov_degrees) {
            m_fov = fov_degrees;

            m_matrix_ubo_content.projection_matrix = reset_projection_matrix(m_fov, m_aspect_ratio, m_z_near, m_z_far,
                                                                             m_mouse_pitch, m_mouse_yaw);
        }

        void set_aspect_ratio(float aspect_ratio) {
            m_aspect_ratio = aspect_ratio;

            m_matrix_ubo_content.projection_matrix = reset_projection_matrix(m_fov, m_aspect_ratio, m_z_near, m_z_far,
                                                                             m_mouse_pitch, m_mouse_yaw);
        }

        void set_last_mouse_position(const glm::vec2 &last_mouse_position) {
            m_last_mouse_position = last_mouse_position;
        }

        void set_camera_position(const glm::vec3 &camera_position) {
            m_camera_position = camera_position;
        }

        void setup_capture(const Capture &capture);

        void set_scene_graph_root_node(const std::shared_ptr<gfx::SceneGraphNode> &loading_node_root);

        vk::ShaderStageFlags get_shader_usage();

        bool is_captured() const;

        void capture_points(vk::CommandBuffer command_buffer, bool force_recapture);

        void capture_point_light_shadows(vk::CommandBuffer command_buffer, bool force_recapture);

        void recreate_graphics_pipeline(CoreRenderer &core_renderer, const vk::Viewport &viewport);

        void input(Window &window);

        void predraw_load(vk::CommandBuffer command_buffer); // predraw_load called before every render pass
        void predraw(); // predraw called every render context

        void draw(vk::CommandBuffer command_buffer, CoreRenderer &core_renderer, vk::raii::Device &logical_device);

        void attach_lighting_environment(std::unique_ptr<LightingEnvironment> &lighting_environment);

        void reapply_graphics_pipeline(const vk::CommandBuffer &command_buffer, int material_env_index = 0);

        void update_matrix_ubo(CoreRenderer &core_renderer, RenderContextID render_context_id);
    private:
        static void mouse_callback(GLFWwindow *window, double x, double y);

        static PerspectiveCameraEnvironment *current_camera;

        static constexpr glm::vec3 INITIAL_FRONT_TARGET {0.068475f, 0.190809f, -0.979236f};
        static constexpr glm::vec3 INITIAL_POSITION {-1.952644f, -1.789690f, 7.16568f};

        struct MatrixUbo {
            alignas(16) glm::mat4 projection_matrix;
            alignas(16) glm::mat4 view_matrix;
            alignas(16) glm::mat4 light_space_matrix;
            alignas(16) glm::vec3 camera_front;
            alignas(16) glm::vec3 camera_up;
            alignas(16) glm::vec3 camera_right;
            alignas(16) glm::vec3 camera_position;
            alignas(4) float camera_far_plane;
            alignas(4) int linear_distance_scaling;
            alignas(4) int flip_projection_matrix;
        };

        glm::mat4 reset_projection_matrix(float fov, float aspect_ratio, float z_near, float z_far,
                                          float camera_pitch_degrees, float camera_yaw_degrees);

        void assign_default_lighting_environment_to_node(gfx::SceneGraphNode &scene_graph_node);
        
        void assign_default_lighting_environment_to_tree();

        UniformBuffer m_matrix_uniform_buffer;
        UniformBuffer m_shadow_mapping_matrix_uniform_buffer;

        MatrixUbo m_matrix_ubo_content {glm::mat4(1.0f), glm::mat4(1.0f)};
        MatrixUbo m_shadow_mapping_matrix_ubo_content {glm::mat4(1.0f), glm::mat4(1.0f)};
        std::unordered_map<RenderContextID, Capture> m_current_captures;

        float m_aspect_ratio;
        float m_frame_delta;
        float m_fov;
        float m_last_frame_time = 0.0f;
        float m_mouse_sensitivity;
        float m_mouse_pitch = 0.0f;
        float m_mouse_yaw = -90.0f;
        bool m_up_down_movement;
        float m_z_far;
        float m_z_near;

        glm::vec3 m_camera_front_target;
        glm::vec3 m_camera_position;
        glm::vec3 m_camera_up;

        glm::vec2 m_last_mouse_position {0.0f, 0.0f};

        std::array<glm::vec3, 6> m_current_frustrum_planes;

        std::vector<std::unique_ptr<LightingEnvironment>> m_lighting_environments;
        std::string m_default_lighting_environment_tag;
        std::unordered_set<std::string> m_lighting_environment_tags;
        std::shared_ptr<gfx::SceneGraphNode> m_scene_graph_node_root;
        DirectionalLightShadowsInfo m_directional_light_shadows_info;

        CoreRenderer &m_core_renderer;

        bool m_capturing_mouse = false;

        bool m_use_orthographic = false;
        int m_use_point_light_perspective = -1;
    };
}

#endif //ARISTAEUS_PERSPECTIVE_CAMERA_ENVIRONMENT_HPP
