//
// Created by bobby on 8/25/21.
//

#include <memory>

#include "assimp_mesh.hpp"

#include "core_renderer.hpp"
#include "assimp_model.hpp"

// TODO(Bobby): make this a fully fledged node
/*void aristaeus::gfx::vulkan::AssimpMesh::classdb_register() {
    core::ClassDB &class_db = utils::get_singleton_safe<core::ClassDB>();
    
    class_db.register_property<AssimpMesh, std::string>
}*/

aristaeus::gfx::vulkan::AssimpMesh::AssimpMesh(const std::vector<glm::vec3> &vertices, const std::vector<uint32_t> &indices,
                                               const std::string &mesh_generation_info, bool remain_unmerged) : 
    m_mesh_manager_load_request(MeshManager::LoadRequest{
        .vertices = vertices, 
        .indices = indices,
        .mesh_generation_info = mesh_generation_info,
        .remain_unmerged = remain_unmerged
    }) {
}

aristaeus::gfx::vulkan::AssimpMesh::AssimpMesh(MeshID mesh_id) : m_mesh_id(mesh_id) {
}

void aristaeus::gfx::vulkan::AssimpMesh::draw() {
    RenderContextID render_context_id = utils::get_singleton_safe<CoreRenderer>().get_current_render_context_id();

    auto &mesh_manager = utils::get_singleton_safe<MeshManager>();
    std::shared_ptr<SceneGraphNode> mesh_shared_ptr = get_weak_ptr_to_this().lock();

    assert(mesh_shared_ptr != nullptr);

    if (!m_mesh_id.has_value() && !m_instance_id.has_value()) {
        m_mesh_manager_load_request.assimp_mesh_in_tree = std::static_pointer_cast<AssimpMesh>(mesh_shared_ptr);

        MeshManager::LoadResponse load_response = mesh_manager.load_mesh(m_mesh_manager_load_request);

        m_mesh_id = load_response.mesh_id;
        m_instance_id = load_response.instance_id;
    }
    else if (!m_instance_id.has_value()) {
        m_instance_id = mesh_manager.create_instance(m_mesh_id.value(), std::static_pointer_cast<AssimpMesh>(mesh_shared_ptr));
    }

    if (m_assigned_materials.contains(render_context_id)) {
        MaterialID mat_id = m_assigned_materials.at(render_context_id);

        mesh_manager.add_instance_to_draw_list(render_context_id, m_instance_id.value(), mat_id);
    }
}

[[nodiscard]] aristaeus::gfx::vulkan::AssimpMesh::MeshGeometryData aristaeus::gfx::vulkan::AssimpMesh::get_mesh_geometry_data() const {
    auto &mesh_manager = utils::get_singleton_safe<MeshManager>();

    MeshGeometryData mesh_geometry_data;

    return MeshGeometryData{
        .vertices = mesh_manager.get_mesh_vertices(m_mesh_id.value()),
        .indices = mesh_manager.get_mesh_indices(m_mesh_id.value())
    };
}

void aristaeus::gfx::vulkan::AssimpMesh::assign_material_id(MaterialID material_id, RenderContextID render_context_id) {
    auto &mesh_manager = utils::get_singleton_safe<MeshManager>();

    if (m_assigned_materials.contains(render_context_id)) {
        /*for (RenderContextID render_c) {
            mesh_manager.update_mesh_material()
        }*/
        throw std::runtime_error("Not yet implemented");
    }
    else {
        m_assigned_materials.insert({ render_context_id, material_id });
    }
}
