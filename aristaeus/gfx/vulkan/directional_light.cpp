#include "directional_light.hpp"

ARISTAEUS_CLASS_IMPL(aristaeus::gfx::vulkan::DirectionalLight, aristaeus::core::StdTypeInfoRef(typeid(aristaeus::gfx::vulkan::LightingNode)));

void aristaeus::gfx::vulkan::DirectionalLight::classdb_register() {
    assert(aristaeus::core::ClassDB::instance != nullptr);
    auto& class_db = *aristaeus::core::ClassDB::instance;

    class_db.register_type<PerspectiveCameraEnvironment::Capture>(get_capture_json);

    class_db.register_property<DirectionalLight, glm::vec3>("diffuse", [](DirectionalLight &directional_light) {return directional_light.m_diffuse;},
        [](DirectionalLight &directional_light, glm::vec3 diffuse_color) {
            directional_light.m_diffuse = diffuse_color;
            directional_light.update_point_light_ubo();
        }, []() {return glm::vec3{1.0f}; });
    class_db.register_property<DirectionalLight, glm::vec3>("direction", [](DirectionalLight &directional_light) {return directional_light.m_direction;},
        [](DirectionalLight &directional_light, glm::vec3 direction) {
            directional_light.m_direction = direction;
            directional_light.m_base_direction = directional_light.m_direction;
            directional_light.update_point_light_ubo();
        }, []() {return glm::vec3{ 1.0f }; });

    class_db.register_property<DirectionalLight, PerspectiveCameraEnvironment::Capture>("shadow_map_capture", [](DirectionalLight &directional_light) {return directional_light.m_shadow_map_capture; },
        [](DirectionalLight &directional_light, PerspectiveCameraEnvironment::Capture capture) {
            directional_light.m_shadow_map_capture = capture;
            directional_light.m_base_shadow_map_capture = directional_light.m_shadow_map_capture;

            directional_light.call_event<void *>(std::string{ SHADOW_MAP_CAPTURE_CHANGE_EVENT }, nullptr);
        }, []() {return DEFAULT_SHADOW_MAP_CAPTURE; });


    class_db.register_property<DirectionalLight, size_t>("day_night_rotation_time_frames", [](DirectionalLight &directional_light) {return directional_light.m_day_night_rotation_time_frames; },
                                                        [](DirectionalLight &directional_light, float day_night_rotation_frames) {directional_light.day_night_rotation_time_updated(day_night_rotation_frames);},
                                                        []() {return 0.0f;});

    class_db.register_event_callback_on_construction<DirectionalLight, void*>(std::string{SceneGraphNode::GLOBAL_TRANSFORM_CHANGED_EVENT}, 
                                                                              "directional_light_global_transform_changed", [](DirectionalLight &light, void*) {
        if (light.m_shadow_map_capture.position == USE_CURRENT_STATE) {
            light.call_event<void *>(std::string{ SHADOW_MAP_CAPTURE_CHANGE_EVENT }, nullptr);
        }
    });

    class_db.register_event<DirectionalLight, void*>(std::string{SHADOW_MAP_CAPTURE_CHANGE_EVENT});
}

void aristaeus::gfx::vulkan::DirectionalLight::update_point_light_ubo() {
    glm::vec3 parent_position = get_parent_transform().position;

    m_light_node_ubo_data = LightingNode::LightNodeUBOData{
        .light_active = 1,
        .light_type = UBO_DIRECTIONAL_LIGHT_TYPE_ID,

        .diffuse = m_diffuse,
        .position_direction = m_direction
    };

    if (!m_day_night_rotation_paused) {
        if (m_direction != m_base_direction) {
            float t_term = (m_current_rotation_frame_count * glm::pi<float>()) / (0.5 * m_day_night_rotation_time_frames);

            m_shadow_map_capture.position = m_base_shadow_map_capture.position + glm::vec3{ 0, 15 * glm::cos(t_term), (15 * glm::sin(t_term)) };
            call_event<void *>(std::string{ vulkan::DirectionalLight::SHADOW_MAP_CAPTURE_CHANGE_EVENT }, nullptr);
        }
        else if (m_direction == m_base_direction && m_shadow_map_capture.position != m_base_shadow_map_capture.position) {
            m_shadow_map_capture = m_base_shadow_map_capture;
            call_event<void *>(std::string{ vulkan::DirectionalLight::SHADOW_MAP_CAPTURE_CHANGE_EVENT }, nullptr);
        }
    }
}

aristaeus::gfx::vulkan::PerspectiveCameraEnvironment::Capture aristaeus::gfx::vulkan::DirectionalLight::get_capture_json(const nlohmann::json::value_type &capture_json) {
    PerspectiveCameraEnvironment::Capture capture{
        .aspect_ratio = utils::get_or_default_json<float>(capture_json, "aspect_ratio", DEFAULT_SHADOW_MAP_CAPTURE.aspect_ratio),
        .fov = utils::get_or_default_json<float>(capture_json, "fov", DEFAULT_SHADOW_MAP_CAPTURE.fov),
        .position = utils::get_or_default_json<glm::vec3>(capture_json, "position", DEFAULT_SHADOW_MAP_CAPTURE.position, utils::get_vec3_json),
        .up = utils::get_or_default_json<glm::vec3>(capture_json, "up", DEFAULT_SHADOW_MAP_CAPTURE.up, utils::get_vec3_json),
        .z_far = utils::get_or_default_json<float>(capture_json, "z_far", DEFAULT_SHADOW_MAP_CAPTURE.z_far),
        .z_near = utils::get_or_default_json<float>(capture_json, "z_near", DEFAULT_SHADOW_MAP_CAPTURE.z_near),
        .orthographic_capture = {}
    };

    if (capture_json.contains("orthographic")) {
        const nlohmann::json &orthographic_capture_json = capture_json.at("orthographic");

        capture.orthographic_capture = PerspectiveCameraEnvironment::OrthographicCapture{
            .left = orthographic_capture_json.at("left").get<float>(),
            .right = orthographic_capture_json.at("right").get<float>(),
            .bottom = orthographic_capture_json.at("bottom").get<float>(),
            .top = orthographic_capture_json.at("top").get<float>(),
        };
    }

    if (capture_json.contains("view_direction")) {
        capture.view_direction = utils::get_vec3_json(capture_json.at("view_direction"));
    }

    return capture;
}

void aristaeus::gfx::vulkan::DirectionalLight::day_night_rotation_time_updated(float new_time_frames) {
    m_day_night_rotation_time_frames = new_time_frames;

    if (m_day_night_rotation_time_frames == 0 && !m_day_night_rotation_paused) {
        m_direction = m_base_direction;

        update_point_light_ubo();
    }
}

void aristaeus::gfx::vulkan::DirectionalLight::draw() {
    if (m_day_night_rotation_time_frames != 0 && !m_day_night_rotation_paused) {
        m_direction = glm::normalize(-m_shadow_map_capture.position);
        ++m_current_rotation_frame_count;

        set_position(m_shadow_map_capture.position);

        if (m_current_rotation_frame_count >= m_day_night_rotation_time_frames) {
            m_current_rotation_frame_count = 0;
        }

        update_point_light_ubo();
    }
}