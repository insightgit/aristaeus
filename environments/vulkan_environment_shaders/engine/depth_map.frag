#version 450

layout (set = 0, binding = 0) uniform DepthMapUBO {
    mat4 projection_matrix;
    mat4 view_matrix;
    mat4 light_space_matrix;
    vec3 camera_front;
    vec3 camera_up;
    vec3 camera_right;
    vec3 camera_position;
    float camera_far_plane;
    int linear_distance_scaling;
} depth_map_ubo;

layout (location = 0) in vec3 world_position;

void main() {
    //gl_Position = vec4(vec3(0.0), 1.0);
    // TODO(Bobby): optimize this

    if(depth_map_ubo.linear_distance_scaling != 0) {
        vec3 light_distance_vector = world_position - depth_map_ubo.camera_position;

        float distance = min(length(light_distance_vector) / depth_map_ubo.camera_far_plane, 1.0f);

        gl_FragDepth = distance;
    } else {
        //float z = gl_FragCoord.z * 2.0 - 1.0; // Back to NDC 
        //z = (2.0 * 0.1 * 100) / (100 + 0.1 - z * (100 - 0.1));

        gl_FragDepth = gl_FragCoord.z;
    }
}
