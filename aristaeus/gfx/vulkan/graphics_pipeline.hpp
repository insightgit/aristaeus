//
// Created by bobby on 5/23/22.
//

#ifndef ARISTAEUS_GRAPHICS_PIPELINE_HPP
#define ARISTAEUS_GRAPHICS_PIPELINE_HPP

#include <algorithm>
#include <execution>
#include <fstream>
#include <functional>
#include <iostream>

#include <boost/algorithm/hex.hpp>
#include <boost/uuid/detail/md5.hpp>
#include <taskflow/taskflow.hpp>

#include "pipeline_cache.hpp"
#include "vulkan_pipeline.hpp"

#include "vulkan_raii.hpp"

namespace aristaeus::gfx::vulkan {
    class CoreRenderer;

    class GraphicsPipeline : public VulkanPipeline {
    public:
        struct PipelineInfo {
            vk::PipelineVertexInputStateCreateInfo vertex_input_stage;
            vk::PipelineInputAssemblyStateCreateInfo input_assembly_stage;
            vk::PipelineTessellationStateCreateInfo tessellation_stage;
            vk::PipelineViewportStateCreateInfo viewport_stage;
            vk::PipelineRasterizationStateCreateInfo rasterization_stage;
            vk::PipelineMultisampleStateCreateInfo multisample_stage;
            vk::PipelineDepthStencilStateCreateInfo depth_stencil_stage;
            vk::PipelineColorBlendStateCreateInfo blending_stage;
            vk::PipelineDynamicStateCreateInfo dynamic_stage;

            std::vector<vk::PushConstantRange> push_content_ranges;

            std::vector<std::vector<vk::DescriptorSetLayoutBinding>> descriptor_bindings;

            uint32_t sub_pass;

            std::string pipeline_cache_path;
            vk::RenderPass render_pass;

            bool operator==(const PipelineInfo &pipeline_info) const noexcept {
                // TODO(Bobby): check more than these attributes
                return this->pipeline_cache_path == pipeline_info.pipeline_cache_path && this->render_pass == pipeline_info.render_pass &&
                       this->sub_pass == pipeline_info.sub_pass;
            }
        };

        struct PipelineShaderHash {
            size_t operator()(const std::pair<PipelineInfo, std::vector<ShaderInfo>> &pipeline_shader_info)
                            const noexcept {
                std::hash<std::string> string_hash;
                size_t current_hash = 0;

                for(const ShaderInfo &shader_info : pipeline_shader_info.second) {
                    for (char shader_info_byte : shader_info.code) {
                        current_hash += shader_info_byte;
                    }
                }

                return string_hash(pipeline_shader_info.first.pipeline_cache_path) + current_hash;
            }

            bool operator()(const std::pair<PipelineInfo, std::vector<ShaderInfo>> &pipeline_shader_info_a,
                            const std::pair<PipelineInfo, std::vector<ShaderInfo>> &pipeline_shader_info_b)
                           const noexcept {
                if(pipeline_shader_info_a.first.pipeline_cache_path ==
                   pipeline_shader_info_b.first.pipeline_cache_path) {
                    if(pipeline_shader_info_a.second.size() == pipeline_shader_info_b.second.size()) {
                        for(size_t i = 0; pipeline_shader_info_a.second.size() > i; ++i) {
                            if(pipeline_shader_info_a.second[i].code != pipeline_shader_info_b.second[i].code) {
                                return false;
                            }
                        }

                        return true;
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            }
        };

        GraphicsPipeline(CoreRenderer &core_renderer,
                         const std::vector<ShaderInfo> &shader_infos,
                         const PipelineInfo &pipeline_info, bool start_constructing_pipeline = true,
                         bool is_in_preload_stage = false);

        virtual ~GraphicsPipeline();

        bool operator==(const GraphicsPipeline &other) const noexcept {
            return m_pipeline_data == other.m_pipeline_data;
        }

        [[nodiscard]] std::vector<vk::DynamicState> get_dynamic_states() const {
            return m_viewport_dynamic_states;
        }

        [[nodiscard]] vk::raii::PipelineLayout &get_pipeline_layout() {
            return *m_pipeline_data->pipeline_layout;
        }

        [[nodiscard]] PipelineInfo get_pipeline_info() {
            return m_pipeline_info;
        }

        [[nodiscard]] std::vector<ShaderInfo> get_shader_infos() {
            return m_shader_infos;
        }

        [[nodiscard]] bool has_dynamic_state(const vk::DynamicState &dynamic_state);

        void recreate_graphics_pipeline(const PipelineInfo &pipeline_info) {
            m_pipeline_info = pipeline_info;

            m_pipeline_data->pipeline_object = create_pipeline_object();
        }

        void start_initial_pipeline_creation(CoreRenderer &core_renderer, bool is_in_preload_stage);

        void bind_to_command_buffer(const vk::CommandBuffer &command_buffer, CoreRenderer &core_renderer);

        void bind_to_command_buffer(vk::raii::CommandBuffer &command_buffer, CoreRenderer &core_renderer) {
            bind_to_command_buffer(*command_buffer, core_renderer);
        }
    private:
        // TODO(Bobby): Make these unique_ptrs go on the stack
        struct VulkanPipelineData {
            std::unique_ptr<vk::raii::PipelineLayout> pipeline_layout;
            std::unique_ptr<vk::raii::Pipeline> pipeline_object;

            PipelineCache pipeline_cache;
            bool need_to_write_to_pipeline_cache = true;
            std::unique_ptr<std::mutex> pipeline_cache_lock;

            std::vector<std::shared_ptr<vk::raii::ShaderModule>> shader_modules_in_use;
            std::vector<vk::raii::DescriptorSetLayout> descriptor_set_layouts;

            std::unique_ptr<std::mutex> shader_modules_mutex;

            PipelineInfo pipeline_info;
            std::vector<ShaderInfo> shader_infos;
            tf::Taskflow pipeline_taskflow;

            std::optional<tf::Future<void>> pipeline_future{};
        };

        struct PipelineConstructionInfo {
            std::reference_wrapper<vk::raii::Device> logical_device;
            std::vector<vk::PipelineShaderStageCreateInfo> shader_stages;
            vk::PhysicalDeviceProperties device_properties;
        } m_pipeline_construction_info;

        PipelineInfo m_pipeline_info;
        std::vector<ShaderInfo> m_shader_infos;

        typedef std::unordered_map<std::pair<PipelineInfo, std::vector<ShaderInfo>>,
                                   std::weak_ptr<VulkanPipelineData>, PipelineShaderHash> WeakGraphicsPipelineCache;

        static WeakGraphicsPipelineCache pipeline_cache;
        static std::mutex pipeline_cache_mutex;

        std::unique_ptr<vk::raii::Pipeline> create_pipeline_object();

        CoreRenderer &m_core_renderer;

        std::vector<vk::DynamicState> m_viewport_dynamic_states
            {{vk::DynamicState::eViewport, vk::DynamicState::eScissor}};

        std::shared_ptr<VulkanPipelineData> m_pipeline_data;
    };
}

#endif //ARISTAEUS_GRAPHICS_PIPELINE_HPP
