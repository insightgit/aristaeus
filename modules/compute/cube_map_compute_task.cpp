#include <gfx/vulkan/core_renderer.hpp>
#include <gfx/vulkan/assimp_model.hpp>

#include "cube_map_compute_task.hpp"

const glm::mat4 aristaeus::gfx::vulkan::compute::CubeMapComputeTask::PROJECTION_MATRIX =
        glm::perspective(glm::radians(90.0f), 1.0f, 0.001f, 1.0f);

aristaeus::gfx::vulkan::compute::CubeMapComputeTask::CubeMapComputeTask(size_t ubo_size,
                                                               uint32_t number_of_storage_images,
                                                               uint32_t number_of_ubos, uint32_t number_of_samplers,
						                                       ComputePipelineInfo compute_pipeline_info,
                                                               const std::string &compute_task_type,
                                                               uint32_t number_of_dispatches_done_per_job,
                                                               const glm::ivec2 &image_size,
                                                               bool mipmapping_enabled,
                                                               std::optional<uint32_t> max_num_of_mipmaps,
                                                               const std::optional<std::string> &cache_path,
                                                               const std::vector<std::string> &input_file_paths) : 
    ComputeTask(ubo_size, number_of_storage_images, number_of_ubos, number_of_samplers, compute_pipeline_info,
                compute_task_type, 6, max_num_of_mipmaps.has_value() ? max_num_of_mipmaps.value() : 1,
                number_of_dispatches_done_per_job, cache_path, input_file_paths),
    m_output_info(create_output_data(image_size, compute_task_type, mipmapping_enabled, max_num_of_mipmaps)) {
}

aristaeus::gfx::vulkan::compute::CubeMapComputeTask::OutputInfo
    aristaeus::gfx::vulkan::compute::CubeMapComputeTask::create_output_data(
                                                                        const glm::ivec2 &image_size,
                                                                        const std::string &output_texture_name,
                                                                        bool mipmapping_enabled,
                                                                        std::optional<uint32_t> max_num_of_mipmaps) {
    uint32_t mipmapping_levels = mipmapping_enabled ?
            std::floor(std::log2(std::max(image_size.x, image_size.y))) + 1 : 1;

    if (max_num_of_mipmaps.has_value()) {
        mipmapping_levels = std::min(mipmapping_levels, max_num_of_mipmaps.value());
    }

    vk::ImageUsageFlags image_usage_flags = vk::ImageUsageFlagBits::eSampled | vk::ImageUsageFlagBits::eStorage;

    if (m_cache_path.has_value()) {
        image_usage_flags |= vk::ImageUsageFlagBits::eTransferSrc | vk::ImageUsageFlagBits::eTransferDst;
    }

    CoreRenderer &core_renderer = utils::get_singleton_safe<CoreRenderer>();

    ImageRaii output_image = core_renderer.get_memory_manager().create_image(
                                                core_renderer, image_size.x / 4, image_size.x / 4,
                                                vk::Format::eR32G32B32A32Sfloat, vk::ImageTiling::eOptimal,
                                                image_usage_flags, {}, VMA_MEMORY_USAGE_AUTO_PREFER_DEVICE,
                                                output_texture_name, 6, vk::ImageCreateFlagBits::eCubeCompatible,
                                                mipmapping_levels);
    std::vector<vk::raii::ImageView> image_views;
    vk::ImageViewCreateInfo image_view_create_info = AssimpModel::DEFAULT_IMAGE_VIEW_CREATE_INFO;
    vk::SamplerCreateInfo sampler_create_info = AssimpModel::DEFAULT_SAMPLER_CREATE_INFO;

    sampler_create_info.maxAnisotropy = core_renderer.get_max_sampler_anisotropy();
    image_view_create_info.format = vk::Format::eR32G32B32A32Sfloat;
    image_view_create_info.image = output_image->image;

    for (uint32_t array_layer = 0; 6 > array_layer; ++array_layer) {
        for (uint32_t mipmap = 0; mipmapping_levels > mipmap; ++mipmap) {
            image_view_create_info.subresourceRange.baseArrayLayer = array_layer;
            image_view_create_info.subresourceRange.baseMipLevel = mipmap;

            image_views.emplace_back(core_renderer.get_logical_device_ref(), image_view_create_info);
        }
    }

    return {
        .image = std::move(output_image),
        .image_views = std::move(image_views),
        .sampler = vk::raii::Sampler{core_renderer.get_logical_device_ref(), sampler_create_info},
    };
}

glm::mat4 aristaeus::gfx::vulkan::compute::CubeMapComputeTask::get_current_view_transformation(int index) {
    // glm::mat4 constructor is COLUMN-MAJOR
    std::array<glm::mat4, 6> capture_views =
    { {
       glm::lookAt(glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(1.0f,  0.0f,  0.0f),
                   glm::vec3(0.0f, -1.0f,  0.0f)) * glm::mat4({0, 0, 1, 0}, {0, 1, 0, 0}, {0, 0, 0, 0}, {1, -1, -1, 1}),
                   // right (+x)
       glm::lookAt(glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(1.0f,  0.0f,  0.0f),
                   glm::vec3(0.0f, -1.0f,  0.0f)) * glm::mat4({0, 0, -1, 0}, {0, 1, 0, 0}, {0, 0, 0, 0}, {-1, -1, 1, 1}),
                   // left (-x)

       glm::lookAt(glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(1.0f,  0.0f,  0.0f),
                   glm::vec3(0.0f, -1.0f,  0.0f)) * glm::mat4({1, 0, 0, 0}, {0, 0, -1, 0}, {0, 0, 0, 0}, {-1, -1, 1, 1}),
                   // down (-y)
       glm::lookAt(glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(1.0f,  0.0f,  0.0f),
                   glm::vec3(0.0f, -1.0f,  0.0f)) * glm::mat4({1, 0, 0, 0}, {0, 0, 1, 0}, {0, 0, 0, 0}, {-1, 1, -1, 1}),
                   // up (+y)

       glm::lookAt(glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(1.0f,  0.0f,  0.0f),
                   glm::vec3(0.0f, -1.0f,  0.0f)) * glm::mat4({1, 0, 0, 0}, {0, 1, 0, 0}, {0, 0, 0, 0}, {-1, -1, -1, 1}),
                   // front (+z)
       glm::lookAt(glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(1.0f,  0.0f,  0.0f),
                   glm::vec3(0.0f, -1.0f,  0.0f)) * glm::mat4({-1, 0, 0, 0}, {0, 1, 0, 0}, {0, 0, 0, 0}, {1, -1, 1, 1})
                   // back (-z)
    } };

    return PROJECTION_MATRIX * capture_views[index];
}

vk::DeviceSize aristaeus::gfx::vulkan::compute::CubeMapComputeTask::get_output_size() {
    vk::DeviceSize return_value = 0;

    glm::ivec2 working_image_size{m_output_info.image->image_width, m_output_info.image->image_height};

    for (size_t mip = 0; m_output_info.image->mip_levels > mip; ++mip) {
        return_value += 6lu * 16lu * working_image_size.x * working_image_size.y;

        working_image_size /= glm::ivec2{2, 2};
    }

    return return_value;
}

bool aristaeus::gfx::vulkan::compute::CubeMapComputeTask::load_from_saved_output(
                                                                     vk::CommandBuffer &command_buffer,
                                                                     BufferRaii &buffer,
                                                                     vk::DeviceSize buffer_offset,
                                                                     const std::vector<glm::ivec3> &file_image_data) {
    if (file_image_data.size() != 6 * m_output_info.image->mip_levels) {
        // need six faces (and six files) per mipmap for cube maps
        return false;
    }

    glm::ivec2 working_image_size {m_output_info.image->image_width, m_output_info.image->image_height};

    for (size_t i = 0; file_image_data.size() > i; ++i) {
        if (i > 0 && i % 6 == 0) {
            working_image_size /= 2;
        }

        if (file_image_data[i].x != working_image_size.x || file_image_data[i].y != working_image_size.y) {
            // invalid saved image size
            return false;
        }
    }

    CoreRenderer &core_renderer = utils::get_singleton_safe<CoreRenderer>();
    std::optional<vk::PipelineStageFlagBits> from_stage;

    m_output_info.image->transition_image_layout(command_buffer, vk::ImageLayout::eTransferDstOptimal,
                                                 vk::PipelineBindPoint::eCompute);
    core_renderer.copy_buffer_to_image(command_buffer, buffer, m_output_info.image, 0, 6, buffer_offset, 
                                       0, m_output_info.image->mip_levels, std::optional<vk::ImageLayout>{},
                                       vk::PipelineBindPoint::eCompute);
    m_output_info.image->transition_image_layout(command_buffer, vk::ImageLayout::eShaderReadOnlyOptimal,
                                                 vk::PipelineBindPoint::eCompute);

    return true;
}

void aristaeus::gfx::vulkan::compute::CubeMapComputeTask::save_output(vk::CommandBuffer &command_buffer,
                                                                      BufferRaii &buffer,
                                                                      vk::DeviceSize buffer_offset) {
    if (m_cache_path.has_value()) {
        std::string cache_path = m_cache_path.value();

        save_hdr_image(cache_path, m_output_info.image, command_buffer, buffer, true, buffer_offset);
        
        m_output_info.image->transition_image_layout(command_buffer, vk::ImageLayout::eShaderReadOnlyOptimal, vk::PipelineBindPoint::eCompute);
    }
}