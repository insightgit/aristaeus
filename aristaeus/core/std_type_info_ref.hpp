//
// Created by bobby on 15/04/24.
//

#ifndef ARISTAEUS_STD_TYPE_INFO_REF_HPP
#define ARISTAEUS_STD_TYPE_INFO_REF_HPP

#include <functional>

namespace aristaeus::core {
    class StdTypeInfoRef : public std::reference_wrapper<const std::type_info> {
    public:
        StdTypeInfoRef(const std::type_info &type_info) : std::reference_wrapper<const std::type_info>(type_info) {}

        bool operator==(const StdTypeInfoRef &type_info_ref_b) const noexcept {
            return get() == type_info_ref_b.get();
        }
        bool operator==(const std::type_info &type_info) const noexcept {
            return get() == type_info;
        }
    };
}

#endif //ARISTAEUS_STD_TYPE_INFO_REF_HPP
