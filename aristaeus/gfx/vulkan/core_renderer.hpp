//
// Created by bobby on 5/23/22.
//

#ifndef ARISTAEUS_CORE_RENDERER_HPP
#define ARISTAEUS_CORE_RENDERER_HPP

#include <cmath>
#include <functional>
#include <mutex>
#include <thread>

#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "../../utils.hpp"

#include "../../core/singleton.hpp"
#include "../../core/event_object.hpp"

#include "graphics_pipeline.hpp"
#include "image_buffer_raii.hpp"
#include "memory_manager.hpp"
#include "task_manager.hpp"
#include "window.hpp"
#include "render_phase.hpp"
#include "render_phase_dependency_node.hpp"
#include "perspective_camera_environment.hpp"
#include "command_buffer_manager.hpp"

namespace aristaeus::core {
    class SceneLoader;
}

namespace aristaeus::gfx::vulkan {
    // Here multiple inheritance of two non-interfaces is justified because the two classes
    // serve 2 disjoint purposes: singleton handles the singleton pattern of this class,
    // whereas EventObject allows for this class to have events.
    class CoreRenderer : public aristaeus::core::Singleton<CoreRenderer>, public core::EventObject {
    public:
        struct EnvironmentLoadingInfo {
            core::SceneLoader &scene_loader;
            std::string environment_path;
            std::string environment_name;
            float aspect_ratio;
            vk::Viewport viewport;
            vk::Rect2D viewport_scissor;
        };

        static constexpr std::string_view FRAME_AVAILABLE_DEP = "_frame_available";
        static constexpr std::string_view FRAME_DONE_DEP = "_frame_done";
        static constexpr std::string_view SHADOW_MAPPING_RENDER_PHASE = "shadow";
        static constexpr std::string_view SHADOW_MAPPING_RENDER_PHASE_DIRECTIONAL_RENDER_PASS = "shadow_render_pass";

        static constexpr std::string_view DRAW_FRAME_EVENT = "on_draw_frame";
        static constexpr std::string_view NEW_FRAME_EVENT = "on_new_frame";
        static constexpr std::string_view FRAME_DONE_EVENT = "on_frame_done";
        static constexpr std::string_view WINDOW_DESTRUCTION_EVENT = "on_window_destruction";
        static constexpr std::string_view DEFAULT_RENDER_PASS_DRAW_EVENT = "on_default_pass_draw";

        static std::random_device random_device;
        static std::mt19937 mersenne_twister_rng;

        typedef std::pair<std::function<void(void *)>, void *> PostRenderCallbackInfo;

        explicit CoreRenderer(Window &window,
            const std::vector<glm::vec4> &clear_colors, vk::raii::Device &logical_device,
            vk::raii::SwapchainKHR &swap_chain, vk::Extent2D swap_chain_extent,
            vk::Format swap_chain_image_format, EnvironmentLoadingInfo &environment_loading_info,
            const std::optional<std::string> &compute_info_path);

        virtual ~CoreRenderer();

        [[nodiscard]] glm::vec4 get_clear_color() const {
            return m_clear_color;
        }

        [[nodiscard]] vk::PhysicalDeviceProperties get_selected_physical_device_properties() const {
            return m_window.get_selected_physical_device_properties();
        }

        [[nodiscard]] float get_max_sampler_anisotropy() const {
            return 1;//m_window.get_selected_physical_device_properties().limits.maxSamplerAnisotropy;
        }


        void copy_buffer(BufferRaii &src_buffer, BufferRaii &dst_buffer);

        // intentional raii copy for transferring ownership
        void transfer_staging_buffer_for_deletion(BufferRaii buffer_raii) {
            std::lock_guard<std::mutex> temporary_buffers_lock_guard{ m_temporary_buffers_lock };

            m_temporary_buffers[m_current_frame].push_back(std::move(buffer_raii));
        }

        void copy_cubemap_buffer_to_image(BufferRaii &src_buffer, ImageRaii &dst_image,
            vk::DeviceSize buffer_image_offset, vk::PipelineBindPoint pipeline_bind_point = vk::PipelineBindPoint::eGraphics);

        void copy_buffer_to_image(vk::CommandBuffer command_buffer, BufferRaii &src_buffer,
            ImageRaii &dst_image, uint32_t image_layer = 0, uint32_t image_layer_count = 1,
            vk::DeviceSize buffer_offset = 0, uint32_t mip_level = 0, uint32_t mip_level_count = 1,
            std::optional<vk::ImageLayout> target_image_layout = vk::ImageLayout::eShaderReadOnlyOptimal, vk::PipelineBindPoint pipeline_bind_point = vk::PipelineBindPoint::eGraphics);

        void copy_buffer_to_image(vk::raii::CommandBuffer &command_buffer, BufferRaii &src_buffer, ImageRaii &dst_image,
            uint32_t image_layer = 0, uint32_t image_layer_count = 1, vk::DeviceSize buffer_offset = 0,
            uint32_t mip_level = 0, uint32_t mip_level_count = 1,
            std::optional<vk::ImageLayout> target_image_layout = vk::ImageLayout::eShaderReadOnlyOptimal, vk::PipelineBindPoint pipeline_bind_point = vk::PipelineBindPoint::eGraphics) {
            copy_buffer_to_image(*command_buffer, src_buffer, dst_image, image_layer, image_layer_count, buffer_offset, mip_level, mip_level_count,
                target_image_layout, pipeline_bind_point);
        }

        void copy_buffer_to_image(BufferRaii &src_buffer, ImageRaii &dst_image,
            uint32_t image_layer = 0, uint32_t image_layer_count = 1, vk::DeviceSize buffer_offset = 0,
            uint32_t mip_level = 0, uint32_t mip_level_count = 1,
            std::optional<vk::ImageLayout> target_image_layout = vk::ImageLayout::eShaderReadOnlyOptimal,
            vk::PipelineBindPoint pipeline_bind_point = vk::PipelineBindPoint::eGraphics);


        void copy_image_to_buffer(vk::CommandBuffer command_buffer, ImageRaii &src_image, BufferRaii &dst_buffer, vk::ImageLayout current_image_layout,
            uint32_t image_layer = 1, uint32_t image_layer_count = 1, vk::DeviceSize buffer_offset = 0, uint32_t mip_levels = 1,
            bool transition_after_copy = true, std::optional<vk::ImageLayout> target_image_layout = vk::ImageLayout::eShaderReadOnlyOptimal, vk::PipelineBindPoint pipeline_bind_point = vk::PipelineBindPoint::eGraphics);


        vk::raii::ImageView create_image_view(ImageRaii &image, vk::ImageViewCreateInfo image_view_create_info) {
            image_view_create_info.image = image->image;

            return { m_logical_device, image_view_create_info };
        }

        [[nodiscard]] std::optional<std::reference_wrapper<GraphicsPipeline>> get_default_graphics_pipeline() const {
            return m_default_graphics_pipeline;
        }

        [[nodiscard]] glm::vec3 get_default_clear_color() const {
            return m_clear_color;
        }

        void set_default_graphics_pipeline(const std::optional<std::reference_wrapper<GraphicsPipeline>> &graphics_pipeline) {
            m_default_graphics_pipeline = graphics_pipeline;
        }

        std::unique_ptr<vk::raii::ImageView> create_image_view_ptr(ImageRaii &image,
            vk::ImageViewCreateInfo image_view_create_info) {
            image_view_create_info.image = image->image;

            return std::make_unique<vk::raii::ImageView>(m_logical_device, image_view_create_info);
        }

        vk::raii::ImageView create_image_view(vk::Image image, vk::Format format,
            vk::ImageAspectFlags aspect_flags,
            uint32_t layer_num = 0u) {
            vk::ImageViewCreateInfo image_view_create_info(static_cast<vk::ImageViewCreateFlags>(0), image,
                vk::ImageViewType::e2D, format, {},
                vk::ImageSubresourceRange(aspect_flags, 0, 1, layer_num, 1));

            return { m_logical_device, image_view_create_info, nullptr };
        }

        vk::raii::ImageView create_image_view(ImageRaii &image, vk::Format format,
            vk::ImageAspectFlags aspect_flags) {
            return create_image_view(image->image, format, aspect_flags);
        }

        vk::raii::Sampler create_sampler(const vk::SamplerCreateInfo &sampler_create_info) {
            return { m_logical_device, sampler_create_info, nullptr };
        }

        vk::raii::Sampler create_default_sampler();

        std::array<BufferRaii, utils::vulkan::MAX_FRAMES_IN_FLIGHT> create_uniform_buffer(vk::DeviceSize uniform_buffer_size) {
            std::array<BufferRaii, utils::vulkan::MAX_FRAMES_IN_FLIGHT> ubo_buffers;

            for (size_t i = 0; ubo_buffers.size() > i; ++i) {
                ubo_buffers[i] = m_memory_manager.create_buffer(*this, uniform_buffer_size,
                    vk::BufferUsageFlagBits::eUniformBuffer,
                    vk::SharingMode::eExclusive,
                    VMA_ALLOCATION_CREATE_HOST_ACCESS_SEQUENTIAL_WRITE_BIT,
                    VMA_MEMORY_USAGE_AUTO, "ubo");
            }

            return ubo_buffers;
        }

        [[nodiscard]] std::vector<RenderPhase::AttachmentInfo> get_default_color_attachment_info() const;
        [[nodiscard]] std::vector<RenderPhase::AttachmentInfo> get_default_depth_attachment_info() const;

        [[nodiscard]] uint32_t get_current_frame() const {
            return m_current_frame;
        }

        [[nodiscard]] vk::Extent2D get_swap_chain_extent() const {
            return m_swap_chain_extent;
        }

        [[nodiscard]] glm::uvec2 get_swap_chain_extent_uvec2() const {
            return { get_swap_chain_extent().width, get_swap_chain_extent().height };
        }

        [[nodiscard]] vk::Format get_swap_chain_format() const {
            return m_swap_chain_image_format;
        }

        [[nodiscard]] bool should_close_window() {
            bool return_value = m_window.should_close_window();

            if (return_value) {
                call_event<void *>(std::string{ WINDOW_DESTRUCTION_EVENT }, nullptr);
                m_logical_device.waitIdle();
            }

            return return_value;
        }

        [[nodiscard]] vk::raii::Device &get_logical_device_ref() {
            return m_logical_device;
        }

        [[nodiscard]] RenderContextID get_current_render_context_id() const {
            return m_current_render_context_id;
        }

        void set_current_render_context_id(RenderContextID render_context_id) {
            m_current_render_context_id = render_context_id;
        }

        void added_loading_tasks() {
            m_loading_tasks_added = true;
        }

        [[nodiscard]] bool is_capturing_mouse() const {
            return m_window.is_capturing_mouse();
        }

        void set_capturing_mouse(const bool capture_mouse, GLFWcursorposfun callback_function) const {
            m_window.set_capturing_mouse(capture_mouse, callback_function);
        }

        [[nodiscard]] vk::ImageViewCreateInfo get_recommended_image_view_create_info() const {
            return DEFAULT_IMAGE_VIEW_CREATE_INFO;
        }

        [[nodiscard]] vk::SamplerCreateInfo get_recommended_sampler_create_info() const {
            vk::SamplerCreateInfo sampler_create_info = DEFAULT_SAMPLER_CREATE_INFO;

            sampler_create_info.maxAnisotropy = get_selected_physical_device_properties().limits.maxSamplerAnisotropy;

            return sampler_create_info;
        }

        vk::raii::Semaphore &get_render_phase_semaphore(const std::string &phase_name, uint32_t frame_num);

        [[nodiscard]] vk::raii::ImageView &get_render_phase_pass_image_view(const std::string &render_phase_name, const std::string &render_pass_name) {
            return m_render_phases.at(render_phase_name).get().get_render_phase().get_render_pass_image_view(render_pass_name, m_current_frame);
        }

        [[nodiscard]] vk::raii::RenderPass &get_render_phase_render_pass(const std::string &render_phase_name, const std::string &render_pass_name) {
            return m_render_phases.at(render_phase_name).get().get_render_phase().get_render_pass(render_pass_name);
        }

        [[nodiscard]] vk::raii::RenderPass *get_default_rasterization_render_pass() const {
            return m_default_rasterization_render_pass;
        }

        [[nodiscard]] uint32_t get_default_rasterization_subpass() const {
            return m_default_rasterization_subpass;
        }

        [[nodiscard]] bool in_default_rasterization_render_pass() const {
            return m_current_render_phase != nullptr && m_current_render_phase->get_render_phase_name() == m_default_rasterization_render_phase_name &&
                m_current_render_phase->get_current_render_pass() == m_default_rasterization_render_pass_name;
        }

        [[nodiscard]] std::string get_default_rasterization_render_phase_name() const {
            return m_default_rasterization_render_phase_name;
        }

        [[nodiscard]] std::string get_default_rasterization_render_pass_name() const {
            return m_default_rasterization_render_pass_name;
        }

        void set_default_rasterization_render_pass(const std::string &render_phase_name, const std::string &render_pass_name) {
            m_default_rasterization_render_pass = &get_render_phase_render_pass(render_phase_name, render_pass_name);
            m_default_rasterization_render_phase_name = render_phase_name;
            m_default_rasterization_render_pass_name = render_pass_name;
        }

        void set_default_rasterization_subpass(uint32_t default_rasterization_subpass) {
            m_default_rasterization_subpass = default_rasterization_subpass;
        }

        void set_default_graphics_pipeline_info(const std::optional<GraphicsPipeline::PipelineInfo> &graphics_pipeline_info) {
            m_default_graphics_pipeline_info = graphics_pipeline_info;
        }

        void set_default_graphics_shader_infos(const std::vector<GraphicsPipeline::ShaderInfo> &shader_infos) {
            m_default_graphics_shader_infos = shader_infos;
        }

        [[nodiscard]] std::optional<GraphicsPipeline::PipelineInfo> get_default_graphics_pipeline_info() {
            return m_default_graphics_pipeline_info;
        }

        [[nodiscard]] std::vector<GraphicsPipeline::ShaderInfo> get_default_graphics_shader_infos() {
            return m_default_graphics_shader_infos;
        }

        [[nodiscard]] vk::raii::RenderPass &get_shadow_mapping_render_pass() {
            return get_render_phase_render_pass(std::string{ SHADOW_MAPPING_RENDER_PHASE }, std::string{ SHADOW_MAPPING_RENDER_PHASE_DIRECTIONAL_RENDER_PASS });
        }

        [[nodiscard]] RenderPhase *get_render_phase(const std::string &render_phase_name) {
            if (m_render_phases.contains(render_phase_name)) {
                return &m_render_phases.at(render_phase_name).get().get_render_phase();
            }
            else {
                return nullptr;
            }
        }

        [[nodiscard]] RenderPhase *get_render_phase(RenderContextID render_context_id);

        [[nodiscard]] RenderPhase *get_current_render_phase() {
            return m_current_render_phase;
        }

        [[nodiscard]] vk::raii::RenderPass *get_current_render_pass() {
            auto &core_renderer = utils::get_singleton_safe<CoreRenderer>();
            RenderPhase *render_phase = core_renderer.get_current_render_phase();

            if (render_phase == nullptr && !render_phase->get_current_render_pass().empty()) {
                return nullptr;
            }
            else {
                return &render_phase->get_render_pass(render_phase->get_current_render_pass());
            }
        }

        [[nodiscard]] RenderPhaseDependencyNode &get_render_phase_dep_node_root() {
            return *m_render_phase_dep_tree;
        }

        // (!) THIS SHOULD ONLY BE USED BY RenderPhaseDependencyNode
        void _set_current_render_phase(RenderPhase *render_phase) {
            m_current_render_phase = render_phase;
        }

        [[nodiscard]] uint32_t get_current_swap_chain_image() const {
            return m_current_swap_chain_image;
        }

        void queue_recapture_point_light_shadows() {
            m_point_light_shadows_captured = false;
        }

        ImageRaii create_depth_image(vk::Extent2D depth_image_extent,
                                     std::optional<vk::ImageUsageFlags> extra_usage_bits = {}) {
            vk::ImageUsageFlags flags = vk::ImageUsageFlagBits::eDepthStencilAttachment;

            if(extra_usage_bits.has_value()) {
                flags |= extra_usage_bits.value();
            }

            return m_memory_manager.create_image(*this, depth_image_extent.width, depth_image_extent.height,
                                                 m_depth_image_format, vk::ImageTiling::eOptimal, flags,
                                                 {}, VMA_MEMORY_USAGE_AUTO, "core_renderer_depth");
        }

        ImageRaii create_depth_image(std::optional<vk::ImageUsageFlags> extra_usage_bits = {}) {
            return create_depth_image(m_swap_chain_extent, extra_usage_bits);
        }

        vk::raii::ImageView create_depth_image_view(ImageRaii &image_raii, uint32_t layer_num = 0) {
            vk::raii::ImageView return_value = create_image_view(image_raii->image, m_depth_image_format,
                                                                 vk::ImageAspectFlagBits::eDepth,
                                                                 layer_num);

            std::cout << "Created depth resources successfully!\n";

            return return_value;
        }


        vk::Format get_depth_image_format() const {
            return m_depth_image_format;
        }

        [[nodiscard]] TaskManager &get_task_manager() {
            return m_task_manager;
        }

        [[nodiscard]] MemoryManager &get_memory_manager()  {
            return m_memory_manager;
        }

        [[nodiscard]] CommandBufferManager &get_graphics_command_buffer_manager() {
            if (m_disallow_command_recording) {
                throw std::runtime_error("Command recording is not allowed at this time!");
            }
            return m_graphics_command_buffer_manager;
        }

        [[nodiscard]] PerspectiveCameraEnvironment &get_perspective_camera_environment() {
            return m_perspective_camera_environment;
        }

        [[nodiscard]] Window::QueueIndicesInfo get_queue_indices_info() const {
            return m_window.get_queue_indices_info();
        }

        [[nodiscard]] uint32_t get_graphics_queue_family_index() const {
            return m_window.get_queue_indices_info().graphics_family.value();
        }

        [[nodiscard]] glm::mat4 get_view_matrix() const {
            return m_perspective_camera_environment.get_view_matrix();
        }

        [[nodiscard]] glm::mat4 get_projection_matrix() const {
            return m_perspective_camera_environment.get_projection_matrix();
        }

        [[nodiscard]] std::vector<vk::Image> get_swap_chain_images() const {
            return m_swap_chain.getImages();
        }

        [[nodiscard]] uint64_t get_total_frames_rendered() const {
            return m_total_frames;
        }

        void generate_mipmaps(vk::CommandBuffer command_buffer, ImageRaii &image_raii,
                              uint32_t layer_count = 1, uint32_t mip_levels = 1, vk::PipelineBindPoint pipeline_bind_point = vk::PipelineBindPoint::eGraphics);

        void generate_mipmaps(ImageRaii &image_raii, uint32_t layer_count = 1,
                              uint32_t mip_levels = 1, vk::PipelineBindPoint pipeline_bind_point = vk::PipelineBindPoint::eGraphics);

        void add_post_render_callback(const PostRenderCallbackInfo &post_render_callback_info) {
            m_post_render_callback_infos[m_current_frame].push_back(post_render_callback_info);
        }

        void init_imgui();

        void predraw_load();

        RenderPhase &add_render_pass(const RenderPhase::RenderPassInfo &render_pass_info, bool create_render_phase = false, 
                                     const std::optional<std::string> &render_phase_info = {});

        void draw_frame();
    private:
        static const vk::ImageViewCreateInfo DEFAULT_IMAGE_VIEW_CREATE_INFO;
        static constexpr vk::SamplerCreateInfo DEFAULT_SAMPLER_CREATE_INFO {{}, vk::Filter::eLinear,
                                                                            vk::Filter::eLinear,
                                                                            vk::SamplerMipmapMode::eLinear,
                                                                            vk::SamplerAddressMode::eRepeat,
                                                                            vk::SamplerAddressMode::eRepeat,
                                                                            vk::SamplerAddressMode::eRepeat, 0.0f,
                                                                            VK_TRUE, {}, VK_FALSE,
                                                                            vk::CompareOp::eAlways, 0.0f, 0.0f,
                                                                            vk::BorderColor::eIntOpaqueBlack, VK_FALSE};

        // so it can access before_default_render_phase and after_default_render_phase
        friend class RenderPhase;

        vk::Format find_depth_image_format() {
            return m_window.find_supported_image_format(
                    {vk::Format::eD24UnormS8Uint, vk::Format::eD32SfloatS8Uint, vk::Format::eD32Sfloat},
                    vk::ImageTiling::eOptimal, vk::FormatFeatureFlagBits::eDepthStencilAttachment);
        }

        std::vector<VmaImage> create_swap_chain_vma_images(vk::raii::SwapchainKHR& swap_chain);

        std::vector<vk::raii::ImageView> create_swap_image_views() {
            std::vector<vk::raii::ImageView> return_value;

            return_value.reserve(m_swap_chain_images.size());

            for(size_t i = 0; m_swap_chain_images.size() > i; ++i) {
                return_value.push_back(create_image_view(m_swap_chain_images[i].image, m_swap_chain_image_format,
                                                         vk::ImageAspectFlagBits::eColor));
            }

            return return_value;
        }

        void add_render_pass_dependencies(const RenderPhase::RenderPassInfo &render_pass_info, RenderPhaseDependencyNode &created_render_phase);

        void refresh_render_phase_cache();

        void register_render_phase(RenderPhase &render_phase);
        std::unordered_map<std::string, std::reference_wrapper<RenderPhaseDependencyNode>> init_render_phases();

        void render_render_phase_nodes();

        RenderPhase create_shadow_render_phase();

        void before_default_render_phase();
        void after_default_render_phase();

        static void before_context_shadow_phase(void*);
        static void before_shadow_phase(void*);
        static void shadow_phase(void*);
        static void after_shadow_phase(void*);

        // TODO
        //void create_shared_descriptor_set();

        void recreate_swap_chain();

        void draw_vulkan_drawables();

        RenderContextID assign_render_context_id(RenderPhase &render_phase);

        vk::raii::DescriptorPool create_descriptor_pool();

        uint32_t m_current_frame = 0;

        std::optional<std::reference_wrapper<GraphicsPipeline>> m_default_graphics_pipeline;
        std::optional<GraphicsPipeline::PipelineInfo> m_default_graphics_pipeline_info;
        std::vector<GraphicsPipeline::ShaderInfo> m_default_graphics_shader_infos;

        Window &m_window;

        vk::raii::Device m_logical_device;

        vk::raii::SwapchainKHR m_swap_chain;
        vk::Extent2D m_swap_chain_extent;
        vk::Format m_swap_chain_image_format;
        std::vector<VmaImage> m_swap_chain_images;
        std::vector<vk::raii::ImageView> m_swap_chain_image_views;

        TaskManager m_task_manager;
        MemoryManager m_memory_manager;

        vk::Format m_depth_image_format;
        ImageRaii m_depth_image;
        vk::raii::ImageView m_depth_image_view;

        glm::vec4 m_clear_color;

        vk::raii::Queue m_graphics_queue;
        vk::raii::Queue m_presentation_queue;

        vk::raii::RenderPass *m_default_rasterization_render_pass = nullptr;
        uint32_t m_default_rasterization_subpass = 0;
        std::string m_default_rasterization_render_phase_name;
        std::string m_default_rasterization_render_pass_name;

        CommandBufferManager m_graphics_command_buffer_manager;

        std::array<vk::raii::Semaphore, utils::vulkan::MAX_FRAMES_IN_FLIGHT> m_frame_available_image;
        std::array<vk::raii::Semaphore, utils::vulkan::MAX_FRAMES_IN_FLIGHT> m_loading_task_semaphores;
        bool m_do_loading_tasks_exist = false;
        std::array<vk::raii::Semaphore, utils::vulkan::MAX_FRAMES_IN_FLIGHT> m_frame_finished_rendering;
        std::array<vk::raii::Fence, utils::vulkan::MAX_FRAMES_IN_FLIGHT> m_frame_in_flight_fences;

        std::array<std::vector<BufferRaii>, utils::vulkan::MAX_FRAMES_IN_FLIGHT> m_temporary_buffers;
        std::mutex m_temporary_buffers_lock;
        std::array<std::vector<BufferRaii>, utils::vulkan::MAX_FRAMES_IN_FLIGHT> m_previous_temporary_buffers;

        uint32_t m_current_swap_chain_image = 0;

        std::array<bool, utils::vulkan::MAX_FRAMES_IN_FLIGHT> m_set_to_shader_read_only {false, false};

        uint64_t m_total_frames = 0;

        vk::raii::DescriptorPool m_imgui_descriptor_pool;
        bool m_setup_imgui = false;

        // for static point light shadows
        bool m_point_light_shadows_captured = false;

        std::array<std::vector<PostRenderCallbackInfo>, utils::vulkan::MAX_FRAMES_IN_FLIGHT> m_post_render_callback_infos;

        RenderContextID m_last_assigned_render_context_id = 0;
        RenderPhaseID m_last_assigned_render_phase_id = 0;
        std::unordered_map<std::string, glm::uvec2> m_render_phase_render_context_id_map;
        std::shared_ptr<RenderPhaseDependencyNode> m_render_phase_dep_tree; // this dependency "tree" right now is made with the assumption that
                                                           // RenderPhases are unique: that is no two RenderPhases have the exact
                                                           // same dependencies or parents. Therefore, this "tree" right now is
                                                           // more like a linked list, but could become a tree if there's ever
                                                           // a need for more than two RenderPhases per level/dependencies.
        std::unordered_map<std::string, std::reference_wrapper<RenderPhaseDependencyNode>> m_render_phases;

        RenderPhase *m_current_render_phase = nullptr;

        PerspectiveCameraEnvironment m_perspective_camera_environment;
        RenderContextID m_current_render_context_id;

        bool m_disallow_command_recording = false;

        bool m_loading_tasks_added = false;
    };
}

#endif //ARISTAEUS_CORE_RENDERER_HPP
