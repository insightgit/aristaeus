#version 450

layout (location = 0) in vec3 texture_coords;

layout (location = 0) out vec4 output_color;

layout (set = 0, binding = 1) uniform samplerCube sky_box;

void main() {
    output_color = texture(sky_box, texture_coords);

    if(output_color.g == output_color.b && output_color.g == 0) {
        output_color.g = output_color.r;
        output_color.b = output_color.r;
    }
}
