#include <memory>

namespace aristaeus::physics {
	template<class Class>
	concept PhysxClass = requires(Class *obj) {
		{obj->release()};
	};

	template<PhysxClass T>
	struct PhysxObjDeleter {
		void operator()(T *obj) {
			if (obj != nullptr) {
				obj->release();
			}
		}
	};

	template<PhysxClass T>
	using PhysxUniquePtr = std::unique_ptr<T, PhysxObjDeleter<T>>;

	template<PhysxClass T>
	std::shared_ptr<T> create_physx_shared_ptr(T *raw_ptr_object) {
		return std::shared_ptr<T>(raw_ptr_object, PhysxObjDeleter<T>());
	}
}
