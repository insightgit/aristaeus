# DO NOT change this value after add_module has been called once with a
# runtime module unless you know what you're doing!
set(RUNTIME_MODULE_LOAD_PATH "${CMAKE_CURRENT_BINARY_DIR}/runtime_modules/")

function(get_platform_string)
    if(CMAKE_SYSTEM_NAME STREQUAL "Windows")
        set(PLATFORM_STRING "windows" PARENT_SCOPE)
    elseif(CMAKE_SYSTEM_NAME STREQUAL "Linux")
        set(PLATFORM_STRING "linux" PARENT_SCOPE)
    else()
        message(FATAL_ERROR "Unsupported platform!")
    endif()
endfunction()

function(add_module module_name entry_point entry_exit_point_header exit_point is_static source_files_list
         create_json_file include_dirs link_libs lib_dirs parent_target_name)
    set(MODULE_TARGET_NAME "Aristaeus-m-${module_name}")
    set(MODULE_TARGET_NAME ${MODULE_TARGET_NAME} PARENT_SCOPE)

    set(entry_point_file_path ${CMAKE_CURRENT_BINARY_DIR}/static_module_entry_point.cpp)

    set(STATIC_MODULE_INSERT_INDICATOR "//!PUT_STATIC_MODULE_LIST_HERE!")
    set(STATIC_MODULE_ARRAY_PREFIX "const std::unordered_set<std::string>")
    string(APPEND static_module_array_string ${STATIC_MODULE_ARRAY_PREFIX} " STATIC_MODULE_LIST {")

    get_property(_copied_static_module GLOBAL PROPERTY _copied_static_module_property)

    if(NOT _copied_static_module)
        message(STATUS "not copied static module")

        file(COPY_FILE ${CMAKE_CURRENT_SOURCE_DIR}/cmake/tools/base_static_module_entry_point.cpp
                ${entry_point_file_path})
        set_property(GLOBAL PROPERTY _copied_static_module_property 1)

        if(NOT is_static)
            file(READ ${entry_point_file_path} base_entry_point_cpp)
            string(APPEND static_module_array_string "};")
            string(REPLACE ${STATIC_MODULE_INSERT_INDICATOR} "${static_module_array_string}" base_entry_point_cpp "${base_entry_point_cpp}")

            file(WRITE ${entry_point_file_path} "${base_entry_point_cpp}")
        endif()
    endif()

    if(is_static)
        add_library(${MODULE_TARGET_NAME} STATIC ${${source_files_list}})

        file(READ ${entry_point_file_path} base_entry_point_cpp)

        set(INCLUDES_INSERT_INDICATOR "//!PUT_INCLUDES_HERE!")
        set(ENTRY_POINTS_INSERT_INDICATOR "//!PUT_ENTRY_POINTS_HERE!")
        set(EXIT_POINTS_INSERT_INDICATOR "//!PUT_EXIT_POINTS_HERE!")

        string(APPEND json_field "\"" ${module_name} "\"")

        string(APPEND module_point_argument "(module_options_json.contains(" ${json_field} ") ? module_options_json.at("
                ${json_field} ") : nlohmann::json{});\n")

        string(APPEND include_string "#include \"" ${entry_exit_point_header} "\"\n" ${INCLUDES_INSERT_INDICATOR})
        string(APPEND entry_point_call ${entry_point} "${module_point_argument}" ${ENTRY_POINTS_INSERT_INDICATOR})

        string(REPLACE ${INCLUDES_INSERT_INDICATOR} ${include_string} base_entry_point_cpp "${base_entry_point_cpp}")
        string(REPLACE ${ENTRY_POINTS_INSERT_INDICATOR} "${entry_point_call}" base_entry_point_cpp "${base_entry_point_cpp}")

        get_property(_module_list GLOBAL PROPERTY _module_list_property)

        list(APPEND _module_list ${module_name})

        list(LENGTH _module_list module_list_len)

        foreach(module ${_module_list})
            string(APPEND static_module_array_string "\"" ${module} "\",")
        endforeach()

        set_property(GLOBAL PROPERTY _module_list_property ${_module_list})

        string(APPEND static_module_array_string "};\n}\n")
        string(FIND "${base_entry_point_cpp}" ${STATIC_MODULE_INSERT_INDICATOR} static_module_insert_location)

        if(${static_module_insert_location} EQUAL -1)
            string(FIND "${base_entry_point_cpp}" ${STATIC_MODULE_ARRAY_PREFIX} static_module_array_prefix_location)

            string(SUBSTRING "${base_entry_point_cpp}" 0 ${static_module_array_prefix_location} base_entry_point_cpp)
            string(APPEND base_entry_point_cpp "${static_module_array_string}")
        else()
            string(REPLACE ${STATIC_MODULE_INSERT_INDICATOR} "${static_module_array_string}" base_entry_point_cpp "${base_entry_point_cpp}")
        endif()

        if(NOT ${exit_point} STREQUAL "" AND NOT ${exit_point} STREQUAL "exit_point-NOTFOUND")
            string(APPEND exit_point_call "${exit_point}" "${module_point_argument};" ${EXIT_POINTS_INSERT_INDICATOR})
            string(REPLACE ${EXIT_POINTS_INSERT_INDICATOR} "${exit_point_call}" base_entry_point_cpp "${base_entry_point_cpp}")
        endif()

        file(WRITE ${entry_point_file_path} "${base_entry_point_cpp}")

        if(DEFINED ${lib_dirs})
            target_link_directories(${parent_target_name} PRIVATE ${${lib_dirs}})
        endif()
        
        if(DEFINED ${link_libs})
            target_link_libraries(${parent_target_name} PRIVATE ${${link_libs}})
        endif()

        target_link_libraries(${parent_target_name} PRIVATE ${MODULE_TARGET_NAME})
    else()
        add_library(${MODULE_TARGET_NAME} MODULE ${${source_files_list}})

        set(MODULE_RUNTIME_FILE_PATH
                "${RUNTIME_MODULE_LOAD_PATH}/${module_name}/${MODULE_TARGET_NAME}${CMAKE_SHARED_LIBRARY_SUFFIX}")

        if(${create_json_file})
            # TODO: implement this!
            string(JSON module_json SET "{}" "name" "\"${module_name}\"")
            string(JSON module_json SET ${module_json} "entry_point" "\"${entry_point}\"")
            string(JSON module_json SET ${module_json} "entry_exit_point_header" "\"${entry_exit_point_header}\"")

            get_platform_string()
            string(JSON module_json SET ${module_json} "dynlib_${PLATFORM_STRING}" "\"${MODULE_RUNTIME_FILE_PATH}\"")

            string(LENGTH "${exit_point}" exit_point_length)

            if(NOT exit_point_length EQUAL 0)
                string(JSON module_json SET ${module_json} "exit_point" "${exit_point}")
            endif()

            file(WRITE "${MODULE_RUNTIME_FILE_PATH}/arista_module.json" ${module_json})
        endif()

        add_custom_command(TARGET ${MODULE_TARGET_NAME} POST_BUILD
            COMMAND ${CMAKE_COMMAND} -E copy $<TARGET_FILE:${MODULE_TARGET_NAME}> ${MODULE_RUNTIME_FILE_PATH}
        )

        if(DEFINED ${lib_dirs})
            target_link_directories(${MODULE_TARGET_NAME} PRIVATE ${${lib_dirs}})
        endif()

        if(DEFINED ${link_libs})
            target_link_libraries(${MODULE_TARGET_NAME} PRIVATE ${${link_libs}})
        endif()
    endif()

    target_include_directories(${MODULE_TARGET_NAME} PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/aristaeus ${${include_dirs}})
endfunction()

function(add_module_with_json module_directory is_static source_files_list include_dirs link_libs lib_dirs parent_target_name)
    file(READ ${module_directory}/arista_module.json module_json_body)

    string(JSON module_name GET ${module_json_body} name)
    string(JSON entry_point GET ${module_json_body} entry_point)
    string(JSON exit_point ERROR_VARIABLE error_message GET ${module_json_body} exit_point)
    string(JSON entry_exit_point_header GET ${module_json_body} entry_exit_point_header)

    set(entry_exit_point_header ${module_directory}/${entry_exit_point_header})

    add_module(${module_name} ${entry_point} ${entry_exit_point_header} ${exit_point} ${is_static}
               ${source_files_list} FALSE ${include_dirs} ${link_libs} ${lib_dirs} ${parent_target_name})

    if(NOT is_static)
        get_platform_string()
        string(JSON module_json_body SET "${module_json_body}" "dynlib_${PLATFORM_STRING}"
                "\"${RUNTIME_MODULE_LOAD_PATH}/${module_name}/${MODULE_TARGET_NAME}${CMAKE_SHARED_LIBRARY_SUFFIX}\"")
        file(WRITE "${RUNTIME_MODULE_LOAD_PATH}/${module_name}/arista_module.json" ${module_json_body})
    endif()

    set(MODULE_TARGET_NAME ${MODULE_TARGET_NAME} PARENT_SCOPE)
endfunction()