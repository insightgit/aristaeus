#ifndef ARISTAEUS_PHYSICS_MODULE_ENTRY_POINT_HPP
#define ARISTAEUS_PHYSICS_MODULE_ENTRY_POINT_HPP

#include <core/class_db.hpp>

extern "C" void physics_module_entry_point(const nlohmann::json &module_json);

extern "C" void physics_module_exit_point(const nlohmann::json &module_json);

#endif //ARISTAEUS_PHYSICS_MODULE_ENTRY_POINT_HPP
