//
// Created by bobby on 5/23/22.
//

#include "core_renderer.hpp"

#include "graphics_pipeline.hpp"

std::unordered_map<std::pair<aristaeus::gfx::vulkan::GraphicsPipeline::PipelineInfo,
                   std::vector<aristaeus::gfx::vulkan::GraphicsPipeline::ShaderInfo>>,
        std::weak_ptr<aristaeus::gfx::vulkan::GraphicsPipeline::VulkanPipelineData>,
        aristaeus::gfx::vulkan::GraphicsPipeline::PipelineShaderHash>
        aristaeus::gfx::vulkan::GraphicsPipeline::pipeline_cache {};
std::mutex aristaeus::gfx::vulkan::GraphicsPipeline::pipeline_cache_mutex {};

aristaeus::gfx::vulkan::GraphicsPipeline::GraphicsPipeline(CoreRenderer &core_renderer,
                                                           const std::vector<ShaderInfo> &shader_infos,
                                                           const PipelineInfo &pipeline_info,
                                                           bool start_constructing_pipeline,
                                                           bool is_in_preload_stage) :
        m_pipeline_construction_info({core_renderer.get_logical_device_ref(), {},
                                      core_renderer.get_selected_physical_device_properties()}),
        m_pipeline_info(pipeline_info),
        m_shader_infos(shader_infos),
        m_core_renderer(core_renderer) {
    std::pair<PipelineInfo, std::vector<ShaderInfo>> cache_key {m_pipeline_info, m_shader_infos};

    std::shared_ptr<VulkanPipelineData> pipeline_cache_data {};

    if(pipeline_cache.contains(cache_key)) {
        pipeline_cache_data = pipeline_cache.at(cache_key).lock();
    }

    if(pipeline_cache_data != nullptr) {
        m_pipeline_data = pipeline_cache_data;
    } else {
        m_pipeline_data = nullptr;

        if(start_constructing_pipeline) {
            start_initial_pipeline_creation(core_renderer, is_in_preload_stage);
        }
    }
}

aristaeus::gfx::vulkan::GraphicsPipeline::~GraphicsPipeline() {
    if (m_pipeline_data != nullptr) {
        assert(m_pipeline_data->pipeline_cache_lock != nullptr);

        std::lock_guard<std::mutex> pipeline_cache_lock_guard{ *m_pipeline_data->pipeline_cache_lock };

        if (m_pipeline_data->need_to_write_to_pipeline_cache &&
            !m_pipeline_data->pipeline_info.pipeline_cache_path.empty()) {
            m_pipeline_data->pipeline_cache.write_to_pipeline_cache(m_core_renderer);

            m_pipeline_data->need_to_write_to_pipeline_cache = false;
        }
    }
}

std::unique_ptr<vk::raii::Pipeline> aristaeus::gfx::vulkan::GraphicsPipeline::create_pipeline_object() {
    // TODO(Bobby): Combine this with pipeline_info.dynamic_stage
    assert(m_pipeline_data != nullptr);

    if(m_pipeline_info.dynamic_stage.dynamicStateCount > 0) {
        for(uint32_t i = 0; m_pipeline_info.dynamic_stage.dynamicStateCount > i; ++i) {
            vk::DynamicState current_dynamic_state = m_pipeline_info.dynamic_stage.pDynamicStates[i];
            bool found = false;

            for (size_t i2 = 0; m_viewport_dynamic_states.size() > i2; ++i2) {
                if (m_viewport_dynamic_states[i2] == current_dynamic_state) {
                    found = true;
                    break;
                }
            }

            if (!found) {
                m_viewport_dynamic_states.push_back(current_dynamic_state);
            }
        }
    }

    if (m_pipeline_construction_info.shader_stages.empty()) {
        std::pair<PipelineInfo, std::vector<ShaderInfo>> cache_key{ m_pipeline_info, m_shader_infos };

        std::shared_ptr<VulkanPipelineData> pipeline_cache_data{};

        std::lock_guard<std::mutex> lock_guard{ shader_module_cache_mutex };

        for (const ShaderInfo &shader_info : m_shader_infos) {
            m_pipeline_construction_info.shader_stages.push_back({ {}, shader_info.stage,
                                                                   **shader_module_cache.at(shader_info).lock(),
                                                                   "main" });
        }
    }

    vk::PipelineDynamicStateCreateInfo dynamic_state {{}, m_viewport_dynamic_states};

    vk::GraphicsPipelineCreateInfo pipeline_create_info {{},
                                                         static_cast<uint32_t>(
                                                                 m_pipeline_construction_info.shader_stages.size()),
                                                         m_pipeline_construction_info.shader_stages.data(),
                                                         &m_pipeline_info.vertex_input_stage,
                                                         &m_pipeline_info.input_assembly_stage,
                                                         &m_pipeline_info.tessellation_stage,
                                                         &m_pipeline_info.viewport_stage,
                                                         &m_pipeline_info.rasterization_stage,
                                                         &m_pipeline_info.multisample_stage,
                                                         &m_pipeline_info.depth_stencil_stage,
                                                         &m_pipeline_info.blending_stage,
                                                         &dynamic_state,
                                                         **m_pipeline_data->pipeline_layout,
                                                         m_pipeline_info.render_pass,
                                                         m_pipeline_info.sub_pass};

    return std::make_unique<vk::raii::Pipeline>(m_pipeline_construction_info.logical_device.get(),
                                                m_pipeline_data->pipeline_cache.get_pipeline_cache().get(),
                                                pipeline_create_info, nullptr);
}

void aristaeus::gfx::vulkan::GraphicsPipeline::bind_to_command_buffer(const vk::CommandBuffer &command_buffer,
                                                                      CoreRenderer &core_renderer) {
    if(m_pipeline_data->pipeline_future.has_value()) {
        m_pipeline_data->pipeline_future->wait();
    } else {
        core_renderer.get_task_manager().wait_for_pre_render_taskflow(&m_pipeline_data->pipeline_taskflow,
                                                                      core_renderer.get_current_frame());
    }

    command_buffer.bindPipeline(vk::PipelineBindPoint::eGraphics, **m_pipeline_data->pipeline_object);
}

void aristaeus::gfx::vulkan::GraphicsPipeline::start_initial_pipeline_creation(CoreRenderer &core_renderer,
                                                                               bool is_in_preload_stage) {
    // TODO(Bobby): pipeline caching has a race condition somewhere
    // from validation layer (type: validation, severity: error):
    // Validation Error: [ UNASSIGNED-Threading-MultipleThreads ] Object 0: handle = 0x4ca22a00000005f2,
    // type = VK_OBJECT_TYPE_PIPELINE_CACHE; | MessageID = 0x141cb623 | THREADING ERROR :
    // vkGetPipelineCacheData(): object of type VkPipelineCache is simultaneously used in thread 140306842379968
    // and thread 140306817201856


    if (m_pipeline_data != nullptr) {
        // pipeline already created!
        return;
    }

    m_pipeline_data.reset(new VulkanPipelineData{ .pipeline_cache = PipelineCache{m_pipeline_construction_info.logical_device, m_pipeline_construction_info.device_properties, m_pipeline_info.pipeline_cache_path},
                                                  .pipeline_cache_lock = std::make_unique<std::mutex>(), .shader_modules_mutex = std::make_unique<std::mutex>(), .pipeline_info = m_pipeline_info});

    auto [pipeline_layout_creation, shader_stage_setup, pipeline_creation] =
        m_pipeline_data->pipeline_taskflow.emplace(
            [this] () {
                std::vector<std::vector<vk::DescriptorSetLayoutBinding>> descriptor_bindings = 
                    m_pipeline_info.descriptor_bindings;

                m_pipeline_data->descriptor_set_layouts = create_descriptor_set_layouts(m_pipeline_construction_info.logical_device.get(), descriptor_bindings);

                m_pipeline_data->pipeline_layout = create_pipeline_layout_object(
                        m_pipeline_construction_info.logical_device.get(), m_pipeline_data->pipeline_info.push_content_ranges, 
                        m_pipeline_data->descriptor_set_layouts);
            },
            [this] () {
                m_pipeline_construction_info.shader_stages.reserve(m_shader_infos.size());
            },
            [this] () {
                m_pipeline_data->pipeline_object = create_pipeline_object();
            }
    );

    pipeline_layout_creation.name("pipeline_layout_creation");
    shader_stage_setup.name("shader_stage_setup");
    pipeline_creation.name("pipeline_creation");

    std::string pipeline_cache_path = m_pipeline_info.pipeline_cache_path;

    tf::Task shader_stage_creation =
                      m_pipeline_data->pipeline_taskflow.for_each(m_shader_infos.begin(), m_shader_infos.end(),
                                                                  [&, pipeline_cache_path] (const ShaderInfo &shader_info) {
          auto code_ref = reinterpret_cast<const uint32_t*>(shader_info.code.data());
          uint32_t code_ref_size = shader_info.code.size();

          std::lock_guard<std::mutex> shader_module_lock_guard {shader_module_cache_mutex};
          std::shared_ptr<vk::raii::ShaderModule> shader_module = nullptr;

          if(shader_module_cache.contains(shader_info)) {
              shader_module = shader_module_cache.at(shader_info).lock();
          }

          if(shader_module != nullptr) {
              m_pipeline_construction_info.shader_stages.push_back({{}, shader_info.stage, **shader_module,
                                                                    "main"});
          } else {
              vk::ShaderModuleCreateInfo shader_module_create_info {{}, code_ref_size, code_ref, nullptr};
              std::shared_ptr<vk::raii::ShaderModule> shader_module_shared =
                      std::make_shared<vk::raii::ShaderModule>(m_pipeline_construction_info.logical_device.get(),
                                                               shader_module_create_info);

              (void)pipeline_cache_path;

              shader_module_cache.insert({shader_info, shader_module_shared});

              m_pipeline_data->shader_modules_in_use.push_back(shader_module_shared);
              m_pipeline_construction_info.shader_stages.push_back({{}, shader_info.stage,
                                                                    **shader_module_shared, "main"});
          }
    });

    shader_stage_creation.name("shader_stage_creation");

    shader_stage_creation.succeed(shader_stage_setup);
    pipeline_creation.succeed(pipeline_layout_creation, shader_stage_creation);

    if(is_in_preload_stage) {
        m_pipeline_data->pipeline_future =
                core_renderer.get_task_manager().execute_loading_taskflow(&m_pipeline_data->pipeline_taskflow);
    } else {
        core_renderer.get_task_manager().execute_pre_render_taskflow(&m_pipeline_data->pipeline_taskflow,
                                                                     core_renderer.get_current_frame());
    }

    std::pair<PipelineInfo, std::vector<ShaderInfo>> cache_key{ m_pipeline_info, m_shader_infos };

    {
        std::lock_guard<std::mutex> pipeline_lock_guard{ pipeline_cache_mutex };

        pipeline_cache.insert({ cache_key, m_pipeline_data });
    }

    std::cout << "\"Created\" graphics pipeline successfully!\n";
}

bool aristaeus::gfx::vulkan::GraphicsPipeline::has_dynamic_state(const vk::DynamicState &dynamic_state) {
    for (vk::DynamicState viewport_dynamic_state : m_viewport_dynamic_states) {
        if (viewport_dynamic_state == dynamic_state) {
            return true;
        }
    }

    for (int i = 0; m_pipeline_info.dynamic_stage.dynamicStateCount > i; ++i) {
        if (m_pipeline_info.dynamic_stage.pDynamicStates[i] == dynamic_state) {
            return true;
        }
    }

    return false;
}
