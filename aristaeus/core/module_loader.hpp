//
// Created by bobby on 04/01/24.
//

#ifndef MODULE_LOADER_HPP
#define MODULE_LOADER_HPP

#ifdef _WIN32
#define NOMINMAX
#include <windows.h>
#else
#include <dlfcn.h>
#endif

#include <filesystem>
#include <fstream>
#include <functional>
#include <iostream>
#include <optional>
#include <string>
#include <unordered_set>
#include <unordered_map>

#include <json.hpp>

#include "platform.hpp"
#include "uncopyable.hpp"

namespace aristaeus::core {
    class ModuleLoader : public Uncopyable {
    public:
        static constexpr std::string_view MODULE_JSON_NAME = "arista_module.json";

        struct Module {
            std::string module_name;
            std::string module_path;

            std::string dynamic_library_path;
            std::string entry_point_name;
            std::optional<std::string> exit_point_name;

            std::unordered_set<std::string> required_vulkan_device_extensions;

            bool loaded;
            bool was_unloaded;
        };
        enum class ModuleLoadingPolicy {
            NOTHING,
            WHITELIST,
            BLACKLIST,
            EVERYTHING
        };

        ModuleLoader(const std::string &modules_path) : m_modules_path(modules_path) {
            if(!m_modules_path.empty()) {
                refresh_module_search();
            }
        }

        virtual ~ModuleLoader() {
            unload_all_loaded_modules();
        }

        std::unordered_map<std::string, Module> get_module_candidates() const {
            return m_discovered_modules;
        }

        [[nodiscard]] nlohmann::json get_module_options() const {
            return m_module_options;
        }

        void set_module_options(const nlohmann::json &module_options) {
            m_module_options = module_options;
        }

        // returns all the modules just loaded
        std::vector<std::string> load_module_candidates(const std::unordered_set<std::string> &vulkan_device_extensions_loaded);
        void unload_all_loaded_modules();

        bool load_module(const std::string &module_path, const std::unordered_set<std::string> &vulkan_device_extensions_loaded);
        bool unload_module(const std::string &module_path);

        [[nodiscard]] ModuleLoadingPolicy get_module_loading_policy() const {
            return m_module_loading_policy;
        }

        void set_module_loading_policy(const ModuleLoadingPolicy &module_loading_policy) {
            m_module_loading_policy = module_loading_policy;
        }

        [[nodiscard]] const std::unordered_set<std::string> &get_whitelist_blacklist() const {
            return m_whitelist_blacklist;
        }

        void set_whitelist_blacklist(const std::unordered_set<std::string> &whitelist_blacklist) {
            m_whitelist_blacklist = whitelist_blacklist;
        }

        [[nodiscard]] std::string get_modules_path() const {
            return m_modules_path;
        }

        void set_modules_path(const std::string &modules_path) {
            m_modules_path = modules_path;

            refresh_module_search();
        }

        void refresh_module_search() {
            m_discovered_modules.clear();
            refresh_module_search_recursively({m_modules_path});
        }
    private:
        #ifdef _WIN32
        typedef HINSTANCE DynLibHandle;
        #else
        typedef void* DynLibHandle;
        #endif

        template<typename Argument, typename ReturnType>
        ReturnType call_function_from_dyn_lib(const std::string &dynamic_library_path, const std::string &function_name,
                                              Argument arg) {
            return get_function_ptr_from_dyn_lib<Argument, ReturnType>(dynamic_library_path, function_name)(arg);
        }

        template<typename Argument>
        void call_function_from_dyn_lib(const std::string &dynamic_library_path, const std::string &function_name,
                                        Argument arg) {
            get_function_ptr_from_dyn_lib<Argument, void>(dynamic_library_path, function_name)(arg);
        }

        bool find_and_add_module(const std::filesystem::path &path);
        void refresh_module_search_recursively(const std::filesystem::path &path);

        ModuleLoadingPolicy m_module_loading_policy;
        std::unordered_set<std::string> m_whitelist_blacklist;

        template<typename Argument, typename ReturnType>
        std::function<ReturnType(Argument)>
        get_function_ptr_from_dyn_lib(const std::string &dynamic_library_path, const std::string &function_name);

        std::string m_modules_path;
        std::unordered_map<std::string, Module> m_discovered_modules;
        std::unordered_map<std::string, std::reference_wrapper<Module>> m_loaded_modules;

        std::unordered_map<std::string, DynLibHandle> m_loaded_dyn_libs;
        nlohmann::json m_module_options;
    };
}



#endif //ENGINE_MODULE_LOADER_HPP
