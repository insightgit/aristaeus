//
// Created by bobby on 1/21/22.
//

#include "procedural_terrain.hpp"
#include "terrain.hpp"

#include <gfx/vulkan/rasterization_lighting_environment.hpp>

#define STB_IMAGE_WRITE_IMPLEMENTATION
#include <stb_image_write.h>

void aristaeus::gfx::vulkan::ProceduralTerrain::setup_water_terrain() {
    AssimpModel::ModelUniformBuffer water_model_ubo_content {
        .override_texture = 1,
        .multi_texture_terrain_mode = 0,
        .light_emitter = 0,
        .object_type = static_cast<int>(Terrain::ObjectType::Water_Gerstner),
        .light_box_color = glm::vec3(0.0f),
        .tessellation_settings = {
                .mode = 0,
                .amount = m_water_info.tessellation_amount,
                .camera_max = 128,
                .camera_min = 4,
        },
        .vertex_resolution = 11 * m_terrain_info.vertex_resolution,
        .terrain_num_of_textures = static_cast<float>(m_terrain_info.number_of_textures)
    };

    if (m_water_info.draw_water) {
        m_water_terrain->set_model_ubo(water_model_ubo_content);

        m_water_terrain->set_shininess(m_water_info.shininess);
        m_water_terrain->set_reflectivity(m_water_info.reflectivity);
        m_water_terrain->set_refractive_index(m_water_info.refractive_index);
        m_water_terrain->set_refractive_strength(m_water_info.refractive_strength);
        m_water_terrain->set_transmission(m_water_info.transmission);

        m_water_terrain->use_custom_graphics_pipeline(m_water_pipeline);
        m_water_terrain->set_using_terrain_pipeline(false);

        m_water_terrain->set_position(glm::vec3{ 0, 10, 0 });
        m_water_terrain->set_z_order(BLENDED_OBJECT_Z_ORDER_RANGE_START);

        m_water_terrain->rename_node("WaterTerrain");

        m_terrain->add_child(m_water_terrain);
    }
}


// if no compute manager is provided, perlin noise will run on the cpu
aristaeus::gfx::vulkan::ProceduralTerrain::ProceduralTerrain(const TerrainInfo &terrain_info, const WaterInfo &water_info,
                                                             std::mt19937 &rng, const glm::ivec2 &height_map_resolution,
                                                             const std::unordered_map<glm::vec2, glm::vec2, utils::Vec2Hash> &predetermined_gradient_vectors,
                                                             std::optional<std::reference_wrapper<GraphicsPipeline>> environment_pipeline,
                                                             bool write_noise_texture, bool draw_water_noise_texture,
                                                             std::optional<glm::vec2> perlin_noise_start,
                                                             std::optional<vk::CommandBuffer> create_command_buffer) :
        m_random_num_generator(rng),
        m_draw_water_noise_texture(draw_water_noise_texture),
        m_height_map_resolution(height_map_resolution),
        m_terrain_info(terrain_info),
        m_water_info(water_info),
        m_write_noise_texture(write_noise_texture),
        m_predetermined_gradient_vectors(predetermined_gradient_vectors),
        m_water_pipeline(utils::get_singleton_safe<CoreRenderer>(), get_water_pipeline_shader_infos(),
                         get_water_pipeline_info(*utils::get_singleton_safe<CoreRenderer>().get_default_rasterization_render_pass())),
        m_cpu_perlin_noise(PerlinNoise{m_random_num_generator, m_predetermined_gradient_vectors}),
        m_cpu_generated_height_map(generate_height_map_cpu_perlin_noise(m_height_map_resolution, perlin_noise_start)),
        m_terrain_node_path("Terrain"),
        m_water_terrain_node_path("WaterTerrain"),
        m_terrain(terrain_from_terrain_info(terrain_info, create_command_buffer)),
        m_water_terrain(generate_water_terrain(utils::get_singleton_safe<CoreRenderer>(), create_command_buffer)),
        m_environment_pipeline(environment_pipeline),
        m_current_render_pass(**utils::get_singleton_safe<CoreRenderer>().get_default_rasterization_render_pass()){
    m_terrain->rename_node("Terrain");

    add_child(m_terrain);

    if (m_height_map_textures.empty()) {
        set_height_map_textures({});
    }

    setup_water_terrain();

    add_node_group(std::string{ gfx::vulkan::RasterizationLightingEnvironment::FORWARD_RENDER_NODE_GROUP });
}

aristaeus::gfx::vulkan::ProceduralTerrain::ProceduralTerrain(const TerrainInfo &terrain_info, const WaterInfo &water_info,
                                                             const glm::ivec2 &height_map_resolution,
                                                             const std::vector<std::vector<float>> &perlin_noise_map,
                                                             std::optional<std::reference_wrapper<GraphicsPipeline>> environment_pipeline,
                                                             std::optional<vk::CommandBuffer> create_command_buffer) :
    m_height_map_resolution(height_map_resolution),
    m_terrain_info(terrain_info),
    m_water_info(water_info),
    m_water_pipeline(utils::get_singleton_safe<CoreRenderer>(), get_water_pipeline_shader_infos(),
                     get_water_pipeline_info(*utils::get_singleton_safe<CoreRenderer>().get_default_rasterization_render_pass())),
    m_cpu_generated_height_map(perlin_noise_map),
    m_terrain(terrain_from_terrain_info(terrain_info, create_command_buffer)),
    m_water_terrain(generate_water_terrain(utils::get_singleton_safe<CoreRenderer>(), create_command_buffer)),
    m_environment_pipeline(environment_pipeline),
    m_current_render_pass(**utils::get_singleton_safe<CoreRenderer>().get_default_rasterization_render_pass()) {
    add_child(m_terrain);

    if (m_height_map_textures.empty()) {
        set_height_map_textures({});
    }

    setup_water_terrain();
}

[[nodiscard]] aristaeus::gfx::vulkan::AssimpModel::TessellationSettings
    aristaeus::gfx::vulkan::ProceduralTerrain::get_tessellation_settings() const {
    return m_terrain->get_tessellation_settings();
}

[[nodiscard]] int aristaeus::gfx::vulkan::ProceduralTerrain::get_num_of_patches() const {
    return m_terrain->get_num_of_patches();
}

void aristaeus::gfx::vulkan::ProceduralTerrain::set_tessellation_settings(
        const AssimpModel::TessellationSettings &tessellation_settings) {
    m_terrain->set_tessellation_settings(tessellation_settings);
}

void aristaeus::gfx::vulkan::ProceduralTerrain::set_num_of_patches(int num_of_patches, CoreRenderer &core_renderer) {
    m_terrain->set_num_of_patches(num_of_patches, core_renderer);
}

bool aristaeus::gfx::vulkan::ProceduralTerrain::using_terrain_shader() const {
    return m_terrain->using_terrain_shader();
}

std::vector<std::vector<float>>
aristaeus::gfx::vulkan::ProceduralTerrain::generate_height_map_cpu_perlin_noise(const glm::ivec2& height_map_resolution, std::optional<glm::ivec2> perlin_noise_start) {
    if (!m_cpu_perlin_noise.has_value()) {
        return {};
    }

    std::vector<std::vector<float>> height_map;
    glm::ivec2 random_start;

    if (perlin_noise_start.has_value()) {
        random_start = perlin_noise_start.value();
    }
    else {
        std::uniform_real_distribution<float> random_num_generator_distribution(0.0f, 1.0f);

        std::uniform_int_distribution<int> random_int_generator_distribution(0, 1000000);

        random_start = glm::ivec2{ random_int_generator_distribution(m_random_num_generator) };
    }

    for (int y = random_start.y; random_start.y + height_map_resolution.y > y; ++y) {
        std::vector<float> row;

        for (int x = random_start.x; random_start.x + height_map_resolution.x > x; ++x) {
            glm::vec2 base_xy = { float(x) / height_map_resolution.x, float(y) / height_map_resolution.y };

            base_xy *= m_terrain_info.height_map_pos_ratio;

            float perlin_output =
                m_cpu_perlin_noise.value().perlin_with_octaves(1.0f, 0.5f, m_terrain_info.number_of_perlin_octaves,
                                                               m_terrain_info.perlin_persistence, base_xy);

            // -2 instead of 2 because vulkan y coord is flipped compared to ogl
            row.push_back(2 * perlin_output);
        }


        height_map.push_back(row);
    }

    return height_map;
}

std::shared_ptr<aristaeus::gfx::vulkan::Terrain> aristaeus::gfx::vulkan::ProceduralTerrain::generate_water_terrain(
        CoreRenderer &core_renderer,
        std::optional<vk::CommandBuffer> create_command_buffer = {}) {
    if(m_water_info.draw_water) {
        std::vector<std::vector<float>> height_map;

        for(int y = 0; m_height_map_resolution.y > y; ++y) {
            std::vector<float> row;

            for(int x = 0; m_height_map_resolution.x > x; ++x) {
                row.push_back(m_water_info.y_spawn_threshold);
            }

            height_map.push_back(row);
        }
        PropInfo prop_info {.enabled = false};

        auto terrain_ptr = std::make_shared<Terrain>(m_terrain_info.height_magnitude, m_terrain_info.vertex_resolution,
                                         height_map, m_water_info.texture_path,
                                         m_water_info.number_of_textures, Terrain::ObjectType::Water_Gerstner,
                                         prop_info, m_water_terrain_node_path, m_terrain_info.specular_map_path, m_water_info.number_of_patches,
                                         create_command_buffer);
        terrain_ptr->set_position(m_terrain->get_position());

        return terrain_ptr;
    }

    return {};
}

void aristaeus::gfx::vulkan::ProceduralTerrain::draw(const vk::CommandBuffer &command_buffer,
                                                     vk::raii::PipelineLayout &pipeline_layout, size_t instance_number) {
    // the drawing is handled by the scene tree and our children terrain objects for us
    m_water_terrain->update_waves(m_water_info.wave_properties);
    m_terrain->set_wave_info(m_water_terrain->get_wave_info());
}

float aristaeus::gfx::vulkan::ProceduralTerrain::bilinearly_interperlate(const glm::vec2 &coords,
                                                                         const std::vector<std::vector<float>> &height_map) const {
    std::array<glm::vec2, 4> corners = {
            coords + glm::vec2(-1, -1),
            coords + glm::vec2(1, -1),
            coords + glm::vec2(-1, 1),
            coords + glm::vec2(1, 1)
    };

    float first_x_lerp = (((corners[1].x - coords.x) / (corners[1].x - corners[0].x)) *
                          height_map[static_cast<unsigned int>(corners[0].y)]
                          [static_cast<unsigned int>(corners[0].x)]) +
                         (((coords.x - corners[0].x) / (corners[1].x - corners[0].x)) *
                          height_map[static_cast<unsigned int>(corners[1].y)]
                          [static_cast<unsigned int>(corners[1].x)]);
    float second_x_lerp = (((corners[1].x - coords.x) / (corners[1].x - corners[0].x)) *
                           height_map[static_cast<unsigned int>(corners[2].y)]
                           [static_cast<unsigned int>(corners[2].x)]) +
                          (((coords.x - corners[0].x) / (corners[1].x - corners[0].x)) *
                           height_map[static_cast<unsigned int>(corners[3].y)]
                           [static_cast<unsigned int>(corners[3].x)]);
    float y_lerp = (((corners[3].y - coords.y) / (corners[3].y - corners[0].y)) * first_x_lerp) +
                   (((coords.y - corners[0].y) / (corners[3].y - corners[0].y)) * second_x_lerp);

    return y_lerp;
}

aristaeus::gfx::vulkan::Terrain *aristaeus::gfx::vulkan::ProceduralTerrain::terrain_from_terrain_info(
        const ProceduralTerrain::TerrainInfo &terrain_info, std::optional<vk::CommandBuffer> create_command_buffer) {
    if (terrain_info.multi_textured_terrain) {
        return new Terrain(m_terrain_info.height_magnitude, m_terrain_info.vertex_resolution,
            m_cpu_generated_height_map, m_terrain_info.texture_paths_height,
            m_terrain_info.number_of_textures, Terrain::ObjectType::Object, m_terrain_info.prop_info,
            m_terrain_node_path, m_terrain_info.texture_paths_slope, m_terrain_info.number_of_patches,
            create_command_buffer);
    }
    else {
        return new Terrain(m_terrain_info.height_magnitude, m_terrain_info.vertex_resolution,
            m_cpu_generated_height_map, m_terrain_info.texture_path,
            m_terrain_info.number_of_textures, Terrain::ObjectType::Object, m_terrain_info.prop_info,
            m_terrain_node_path, "", m_terrain_info.number_of_patches, create_command_buffer);
    }
}

void aristaeus::gfx::vulkan::ProceduralTerrain::refresh_with_new_terrain_info(
                              const TerrainInfo &new_terrain_info, const int seed,
                              const glm::ivec2 &height_map_resolution,
                              std::optional<vk::CommandBuffer> create_command_buffer,
                              std::optional<glm::vec2> perlin_noise_start) {
    m_height_map_resolution = height_map_resolution;
    m_seed = seed;

    m_terrain_info = new_terrain_info;

    if (!m_use_cpu_perlin_noise) {
        //m_perlin_noise = {m_seed, m_predetermined_gradient_vectors};
        m_cpu_generated_height_map = generate_height_map_cpu_perlin_noise(m_height_map_resolution);
    }

    // TODO(Bobby): make Terrain use stack instead of heap
    m_terrain = std::unique_ptr<Terrain>(terrain_from_terrain_info(m_terrain_info, create_command_buffer));
}

aristaeus::gfx::vulkan::GraphicsPipeline::PipelineInfo
  aristaeus::gfx::vulkan::ProceduralTerrain::get_water_pipeline_info(vk::raii::RenderPass &render_pass) {
    return {
            .vertex_input_stage = {{}, Terrain::TERRAIN_BINDING_DESCRIPTION,
                                   Terrain::TERRAIN_ATTRIBUTE_DESCRIPTIONS},
            .input_assembly_stage = {{}, vk::PrimitiveTopology::ePatchList, VK_FALSE},
            .tessellation_stage = {{}, 4},
            .viewport_stage = {{}, 1, nullptr, 1, nullptr},
            .rasterization_stage = {{}, VK_FALSE, VK_FALSE, vk::PolygonMode::eFill, vk::CullModeFlagBits::eNone,
                                    vk::FrontFace::eCounterClockwise, {}, {}, {}, {}, 1.0f},
            .multisample_stage = {{}, vk::SampleCountFlagBits::e1, VK_FALSE, 1.0f, nullptr, VK_FALSE, VK_FALSE},
            .depth_stencil_stage = {{}, VK_TRUE, VK_TRUE, vk::CompareOp::eLessOrEqual, VK_FALSE, VK_FALSE, {}, {}, 0.0f, 1.0f},
            .blending_stage = {{}, VK_FALSE, vk::LogicOp::eCopy,
                               WATER_COLOR_BLEND_ATTACHMENT, {0, 0, 0, 0}},

            .dynamic_stage = {{}, RasterizationLightingEnvironment::FORWARD_DYNAMIC_STATE},
            .push_content_ranges = {},

            .descriptor_bindings = {RasterizationLightingEnvironment::FORWARD_DESCRIPTOR_SET_BINDINGS.cbegin(),
                                    RasterizationLightingEnvironment::FORWARD_DESCRIPTOR_SET_BINDINGS.cend()},

            .sub_pass = utils::get_singleton_safe<CoreRenderer>().get_default_rasterization_subpass(),
            .pipeline_cache_path = "procedural_water_pipeline_cache",
            .render_pass = render_pass
    };
}

void aristaeus::gfx::vulkan::ProceduralTerrain::draw_imgui(aristaeus::gfx::vulkan::CoreRenderer &core_renderer) {
    /*ImGui::Begin("Procedural Terrain");

    ImGui::Text("Water");
    ImGui::Checkbox("Draw water", &m_water_info.draw_water);
    ImGui::SliderFloat("Median amplitude", &m_water_info.wave_properties.median_amplitude, 0.5f, 50.0f);
    ImGui::SliderFloat("Median sharpness", &m_water_info.wave_properties.median_sharpness, 0.5f, 100.0f);
    ImGui::SliderFloat("Median wavelength", &m_water_info.wave_properties.median_wavelength, 0.5f, 100.0f);


    ImGui::Text("Terrain textures");
    ImGui::BeginChild("Scrolling");

    bool dirty = false;

    std::vector<std::pair<glm::vec4, glm::vec4>> height_range_changes;

    for(std::pair<const glm::vec4, Image> &height_pair : m_terrain_info.texture_paths_height) {
        std::pair<glm::vec4, Image> height_pair_write = height_pair;

        std::string base_filename =
                height_pair.second.image_path.substr(height_pair.second.image_path.find_last_of("/\\") + 1);

        ImGui::Text("Image %s", base_filename.c_str());

        imgui_vec4("Height", height_pair_write.first);

        if(m_terrain_info.texture_paths_slope.contains(height_pair.second)) {
            glm::vec4 init_slope = m_terrain_info.texture_paths_slope.at(height_pair.second);
            glm::vec4 &slope = m_terrain_info.texture_paths_slope.at(height_pair.second);

            imgui_vec4("Slope", slope);

            if(height_pair.first != height_pair_write.first || init_slope != slope) {
                // TODO(Bobby): make it only replace the changed terrain info
                height_range_changes.push_back({height_pair.first, height_pair_write.first});
                dirty = true;
            }
        } else if(height_pair.first != height_pair_write.first) {
            dirty = true;
        }
    }

    if(dirty) {
        for(std::pair<glm::vec4, glm::vec4> &height_range_change : height_range_changes) {
            Image terrain_image = m_terrain_info.texture_paths_height.at(height_range_change.first);

            m_terrain_info.texture_paths_height.erase(height_range_change.first);
            m_terrain_info.texture_paths_height.insert({height_range_change.second, terrain_image});
        }

        m_terrain->setup_terrain_infos(m_terrain_info.texture_paths_height, m_terrain_info.texture_paths_slope);
    }

    ImGui::EndChild();

    ImGui::End();*/
}

void aristaeus::gfx::vulkan::ProceduralTerrain::imgui_vec4(const std::string &base_label, glm::vec4 &target_vec4) {
    ImGui::InputFloat((base_label + ", x").c_str(), &target_vec4.x);
    ImGui::InputFloat((base_label + ", y").c_str(), &target_vec4.y);
    ImGui::InputFloat((base_label + ", z").c_str(), &target_vec4.z);
    ImGui::InputFloat((base_label + ", w").c_str(), &target_vec4.w);
}

void aristaeus::gfx::vulkan::ProceduralTerrain::set_test_color(std::optional<glm::vec3> test_color) {
    m_terrain->set_test_color(test_color);
}

std::shared_ptr<aristaeus::gfx::vulkan::Texture> aristaeus::gfx::vulkan::ProceduralTerrain::get_terrain_height_map_texture() {
    return m_terrain->get_height_map_texture();
}

void aristaeus::gfx::vulkan::ProceduralTerrain::set_chunked_settings(
        const std::optional<float> &chunked_smoothing_percent, const std::optional<glm::ivec2> &chunked_position) {
    m_terrain->set_chunked_settings(chunked_smoothing_percent, chunked_position);
    if(m_water_terrain != nullptr) {
        m_water_terrain->set_chunked_settings(chunked_smoothing_percent, chunked_position);
    }

}

void aristaeus::gfx::vulkan::ProceduralTerrain::toggle_edge_debug_mode() {
    m_terrain->toggle_edge_debug_mode();

    if(m_water_terrain != nullptr) {
        m_water_terrain->toggle_edge_debug_mode();
    }
}

#ifndef NDEBUG
void aristaeus::gfx::vulkan::ProceduralTerrain::toggle_normal_debug_mode() {
    m_terrain->toggle_normal_debug_mode();
}
#endif
