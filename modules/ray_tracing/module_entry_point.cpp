#include "module_entry_point.hpp"

#include "chunked_terrain.hpp"
#include "procedural_terrain.hpp"

extern "C" void ray_tracing_module_entry_point(const nlohmann::json &module_json) {
	auto &class_db = *aristaeus::core::ClassDB::instance;

	class_db.register_class<aristaeus::gfx::vulkan::ChunkedTerrain>();
}