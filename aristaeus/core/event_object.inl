template<typename ArgumentType, aristaeus::core::EventObjectType ObjectType>
bool aristaeus::core::EventObject::add_callback_for_event(
        const std::string &event_name,
        const std::string &callback_name,
        const std::function<void (ObjectType&, ArgumentType)> &callback_function,
        ObjectType &from_object) {
    EventCallbackFunction callback_function_wrapper = [callback_function](EventObject &from_object,
                                                                          void *argument_data) {
        callback_function(static_cast<ObjectType&>(from_object),
                          *reinterpret_cast<ArgumentType*>(argument_data));
    };

    return add_callback_for_event_unsafe(event_name, callback_name, callback_function_wrapper,
                                         typeid(ArgumentType), from_object);
}

template<typename ArgumentType>
bool aristaeus::core::EventObject::remove_callback_for_event(const std::string &event_name,
                                                             const std::string &callback_name) {
    if(m_events.contains(event_name)) {
        core::StdTypeInfoRef argument_type_info = typeid(ArgumentType);
        EventInfo &event_info = m_events.at(event_name);

        if(event_info.argument_type == argument_type_info &&
           event_info.event_callbacks.contains(callback_name)) {
            event_info.event_callbacks.erase(callback_name);

            return true;
        } else {
            return false;
        }
    } else {
        return false;
    }
}