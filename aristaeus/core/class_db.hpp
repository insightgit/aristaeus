#ifndef ARISTAEUS_CLASS_DB_HPP
#define ARISTAEUS_CLASS_DB_HPP

#include <concepts>
#include <exception>
#include <fstream>
#include <functional>
#include <initializer_list>
#include <memory>
#include <optional>
#include <string>
#include <typeinfo>
#include <unordered_map>
#include <unordered_set>

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/glm.hpp>
#include <glm/gtx/quaternion.hpp>
#include <json.hpp>

#include <imgui.h>

#include "singleton.hpp"
#include "std_type_info_ref.hpp"
#include "../utils.hpp"

#include "event_object.hpp"

namespace aristaeus::gfx {
	class SceneGraphNode;
}

namespace aristaeus::core {
	template<class Class>
	concept AristaeusBaseClass = requires(Class) {
		{Class::CLASS_TYPE} -> std::common_with<std::string_view>;
		{Class::parent_class_types} -> std::same_as<const std::vector<StdTypeInfoRef>&>;
		//{Class::MyType} -> std::same_as<Class>; "this expression is invalid": why?
		{Class::classdb_register()};
	};

	template <typename> struct ptr_traits {};
	template <typename T> struct ptr_traits<T*>
	{
		typedef T MyType;
	};
	
	// from Alexandre C. on https://stackoverflow.com/a/3236398
	template<class ClassPtr>
	concept AristaeusClassPtr = std::is_pointer_v<ClassPtr> && requires(ClassPtr) {
		{ptr_traits<ClassPtr>::MyType::CLASS_TYPE} -> std::common_with<std::string_view>;
		{ptr_traits<ClassPtr>::MyType::parent_class_types} -> std::same_as<const std::vector<StdTypeInfoRef>&>;
		//{Class::MyType} -> std::same_as<Class>; "this expression is invalid": why?
		{ptr_traits<ClassPtr>::MyType::classdb_register()};
	};

	template<class Class>
	concept AristaeusAbstractClass = AristaeusBaseClass<Class> && std::is_abstract_v<Class>;

	template<class Class>
	concept AristaeusConcreteClass = AristaeusBaseClass<Class> && !std::is_abstract_v<Class>;

	template<class Node>
	concept AristaeusNode = AristaeusBaseClass<Node> && std::is_base_of<gfx::SceneGraphNode, Node>::value;

	struct StdTypeInfoRefHasher {
		size_t operator()(const StdTypeInfoRef& type_info_ref) const noexcept {
			return type_info_ref.get().hash_code();
		}
	};

	// leaf node means that the node can be parsed in as the ultimate most specific type of the 
	// node (i.e. is it abstract or not?)
	#define ARISTAEUS_CLASS_HEADER_LEAF(class_type_string, class_type) ARISTAEUS_CLASS_HEADER(class_type_string, class_type, true)
	#define ARISTAEUS_CLASS_HEADER(class_type_string, class_type, leaf_node) ARISTAEUS_CLASS_HEADER_ENTRY_POINT( \
																								    class_type_string,\
																									class_type, \
																									leaf_node, \
																									utils::_no_op_func)
	#define ARISTAEUS_CLASS_HEADER_ENTRY_POINT(class_type_string, class_type, leaf_node, constructor_entry_point) \
	public: \
	static constexpr bool LEAF_NODE = leaf_node; \
	static constexpr std::string_view CLASS_TYPE = class_type_string; \
	typedef class_type MyType; \
	static const std::vector<aristaeus::core::StdTypeInfoRef> parent_class_types; \
	class_type() { constructor_entry_point(); assert(aristaeus::core::ClassDB::instance != nullptr); aristaeus::core::ClassDB::instance->register_event_callbacks_for_class<class_type>(*this); } \
	static void classdb_register(); \
	virtual std::string_view get_class_type() { return CLASS_TYPE; } \
	virtual aristaeus::core::StdTypeInfoRef get_class_type_info() { return typeid(MyType); }

	#define ARISTAEUS_CLASS_IMPL(class_type, ...) \
	const std::vector<aristaeus::core::StdTypeInfoRef> class_type::parent_class_types{ __VA_ARGS__ }; \

	class ClassDB : public Singleton<ClassDB> {
	public:
		static const std::unordered_set<std::string> IGNORED_PROPERTY_NAMES;

		template <typename Type>
		struct UiFunctions {
			// Type& is the persistent state, whereas bool says whether to display the ui or not
			// If we aren't displaying the ui, we still need to return our complete json result
			// with all properties. If we are displaying the ui, nonvisible properties can have their
			// json be omitted. Finally, const std::optional<std::string>& specifies an optional label
			// (usually a property name) that will be reflected in the ui.
			std::function<nlohmann::json::value_type (Type&, bool, const std::optional<std::string>&)> parse_function;
			std::function<Type ()> constructor_function; // used only to initalize a basic persistent state that can be mainuplated by the UI
		};

        class UnrecognizedNodeTypeException : public std::runtime_error {
        public:
            UnrecognizedNodeTypeException(const std::string &what) : std::runtime_error(what) {}
        };

		template <typename Class, typename ReturnType, typename Argument>
		static std::function<ReturnType (Class&, Argument)> member_func_wrapper(
			const std::function<ReturnType (Class*, Argument)> &function) {
			return [function](Class &class_instance, Argument argument) {
				if(std::is_void_v<ReturnType>()) {
					function(&class_instance, argument);
				} else {
					return function(&class_instance, argument);
				}
			};
		}

		template <typename Class, typename ReturnType>
		static std::function<ReturnType (Class&)> member_func_wrapper(
			const std::function<ReturnType (Class*)> &function) {
			return [function](Class &class_instance) {
				std::function<ReturnType (Class*)> wrapping_func = function;

				return wrapping_func(&class_instance);
			};
		}

		ClassDB() {
			register_base_types();
		};

		template<AristaeusAbstractClass Class>
		void register_abstract_class();

		template<AristaeusConcreteClass Class>
		void register_class();

		[[nodiscard]] std::vector<std::string_view> get_ui_initializable_classes() const;

		[[nodiscard]] StdTypeInfoRef get_type_info_for_registered_class(const std::string_view &class_type_name) const {
			return m_class_type_names.at(class_type_name);
		}

		template<typename Type, typename UiFunctionsType>
		void register_type(std::function<Type (const nlohmann::json::value_type&)> parse_function,
						   std::optional<UiFunctions<UiFunctionsType>> ui_functions = {},
						   bool register_supplemental_types = true);

		template<utils::Hashable Type, utils::Hashable UiFunctionsType>
		void register_type(std::function<Type (const nlohmann::json::value_type&)> parse_function,
						   std::optional<UiFunctions<UiFunctionsType>> ui_function = {}, bool register_supplemental_types = true);

		template<typename Type>
		void register_type(std::function<Type(const nlohmann::json::value_type&)> parse_function,
						   std::optional<UiFunctions<Type>> ui_functions = {},
						   bool register_supplemental_types = true) {
			register_type<Type, Type>(parse_function, ui_functions, register_supplemental_types);
		}

		template<utils::Hashable Type>
		void register_type(std::function<Type(const nlohmann::json::value_type&)> parse_function,
			std::optional<UiFunctions<Type>> ui_functions = {}, bool register_supplemental_types = true) {
			register_type<Type, Type>(parse_function, ui_functions, register_supplemental_types);
		}

		template<typename Key, typename Value, typename Hasher>
		void register_unordered_map_type();

		template<utils::Hashable Key, typename Value>
		void register_unordered_map_type() {
			return register_unordered_map_type<Key, Value, std::hash<Key>>();
		}

		template<typename Type, typename UiFunctionsType>
		std::optional<UiFunctions<UiFunctionsType>> get_already_registered_ui_functions();

		template<typename Type>
		std::optional<UiFunctions<Type>> get_already_registered_ui_functions() {
			return get_already_registered_ui_functions<Type, Type>();
		}

		// should only be called by gfx::SceneGraphNode to register event callbacks
		template<AristaeusNode Node>
		void register_event_callbacks_for_class(Node &node_instance);

		template<AristaeusNode Node, typename ArgumentType>
		void register_event(const std::string &event_name);

		template<AristaeusNode Node, typename ArgumentType>
		void register_event_callback_on_construction(const std::string &event_name, const std::string &callback_name,
													 const std::function<void(Node&, ArgumentType)> &event_callback);

		// properties must be of already registered types
		// if the default constructor function doesn't have a value, this property is treated as a required property
		// which requires an explicit parse on node creation
		template<AristaeusBaseClass ClassType, typename Type>
		void register_property(const std::string &property_name, const std::function<Type (ClassType &)> &property_getter,
							   const std::function<void (ClassType&, Type)> &property_setter,
							   const std::optional<std::function<Type ()>> &default_constructor_function);

		template<AristaeusBaseClass ClassType, AristaeusClassPtr ClassPtr>
		void register_property_class_ptr(const std::string &property_name, const std::function<ClassPtr(ClassType &)> &property_getter,
										 const std::function<void (ClassType&, ClassPtr)> &property_setter,
										 const std::optional<std::function<ClassPtr()>> &default_constructor_function);
		
		// TODO(Bobby): impl a get persistent state function from ui funcs maybe?
		nlohmann::json::value_type call_ui_functions(const std::string &node_path, StdTypeInfoRef type_info, bool get_complete_json = false);

		gfx::SceneGraphNode *parse_node(const nlohmann::json::value_type &node_json);
	private:
		typedef std::function<void *(void*)> PropertyGetter; // takes in a ptr to the object instance
		typedef std::function<void (void*, void*)> PropertySetter; // takes in a ptr to the object instance, and then a ptr to the actual property instance
		typedef std::function<void *(const nlohmann::json::value_type&)> ParseFunction;
		typedef std::function<void(void*)> DeleterFunction;

		// the general idea is to
		// (a) get json from ui (if required then see if we can initialize it to some blank value that has to be filled in)
		//    (i) ui needs a persistent working state to display, which can be created via ui_constructor_function
		// (b) pass json to parse_node function
		// (c) set property on class
		struct InternalUiFunctions {
			StdTypeInfoRef persistent_state_type;
			std::function<nlohmann::json::value_type(void*, bool, const std::optional<std::string>&)> parse_function;
			std::function<void* ()> constructor_function; // used only to initalize a basic persistent state that can be mainuplated by the UI

			template<typename Type>
			std::optional<UiFunctions<Type>> get_ui_functions() const {
				if (typeid(Type) == persistent_state_type) {
					return UiFunctions<Type>{
						.parse_function = [&internal_ui_parse_function = parse_function](Type &persistent_data, bool display_ui, const std::optional<std::string> &label_hint) {
							return internal_ui_parse_function(static_cast<void*>(&persistent_data), display_ui, label_hint);
						},
						.constructor_function = [&internal_ui_constructor_function = constructor_function] {
							auto type_ptr = reinterpret_cast<Type*>(internal_ui_constructor_function());
							Type return_value{ std::move(*type_ptr) };

							delete type_ptr;

							return return_value;
						}
					};
				}
				else {
					return {};
				}
			}
		};

		struct TypeInfo {
			ParseFunction parse_function;
			DeleterFunction deleter_function;
			std::optional<InternalUiFunctions> ui_functions;
		};

		struct PropertyInfo {
			std::string name;
			StdTypeInfoRef type;

			PropertyGetter getter;
			PropertySetter setter;

			// if we don't have a value for the default constructor, this property
			// is required.
			std::optional<std::function<void* ()>> default_constructor_function;
		};

		struct EventInfo {
			StdTypeInfoRef argument_type;
			std::string event_name;
		};

		struct ClassInfo {
			std::string_view class_type_name;
			StdTypeInfoRef class_type;
			std::vector<StdTypeInfoRef> parent_class_types;
			bool is_node;
			bool is_ui_initializable;

			std::unordered_map<std::string, PropertyInfo> properties;
			std::unordered_set<std::string> required_properties;

			// <event name, event argument type>
			std::unordered_map<std::string, StdTypeInfoRef> events;

			std::function<void* ()> constructor_function;
			DeleterFunction deleter_function;
		};

		struct EventCallbackInfo {
			StdTypeInfoRef argument_type_info;
			std::string event_name;
			std::string callback_name;
			std::function<void(gfx::SceneGraphNode&, void*)> callback_function;
		};

		struct PersistentData {
			void *data;
			StdTypeInfoRef data_type;
			std::string name;
		};

		template <typename Type>
		struct UnorderedSetUiHelper {
			std::unordered_set<Type> unordered_set;
			Type item_being_added;
		};

		template <typename Key, typename Value, typename Hasher>
		struct UnorderedMapUiHelper {
			std::unordered_map<Key, Value, Hasher> unordered_map;
			std::pair<Key, Value> item_being_added;
		};

        std::function<void(core::EventObject&, void*)> get_event_callback_wrapper(
                                                                          const EventCallbackInfo &event_callback_info);

		void register_base_types();

		template<typename Type>
		InternalUiFunctions convert_to_internal_ui_functions(UiFunctions<Type> ui_functions) {
			return InternalUiFunctions{
				.persistent_state_type = typeid(Type),
				.parse_function = [ui_functions](void *persistent_state, bool display_ui, const std::optional<std::string> &label_hint) {
					return ui_functions.parse_function(*reinterpret_cast<Type*>(persistent_state), display_ui, label_hint);
				},
				.constructor_function = [ui_functions]() {
					return static_cast<void*>(new Type(std::move(ui_functions.constructor_function())));
				}
			};
		}

		template <typename Type>
		static std::function<Type(const nlohmann::json::value_type&)> basic_type_json_parse_function() {
			return [](const nlohmann::json::value_type &json) {
				if (json.is_array()) {
					return Type(json[0].get<Type>());
				}
				else {
					return Type(json.get<Type>());
				}
			};
		}

		template <typename Type>
		requires(std::is_integral_v<Type> || std::is_floating_point_v<Type>)
		static UiFunctions<Type> get_integer_ui_functions(ImGuiDataType data_type) {
			return {
				.parse_function = [data_type](Type &persistent_data, bool display_ui, const std::optional<std::string> &label_hint) {
					if(display_ui) {
						std::string label = label_hint.has_value() ? label_hint.value() : "value";

						ImGui::InputScalar(label.c_str(), data_type, &persistent_data, nullptr, nullptr);
					}

					nlohmann::json integer_json{};

					integer_json.push_back(persistent_data);

					return integer_json;
				},
				.constructor_function = [] { return static_cast<Type>(0); }
			};
		}

		template <typename Type>
		static UiFunctions<Type> get_glm_vector_ui_functions(ImGuiDataType data_type, int components) {
			return {
				.parse_function = [data_type, components](Type &persistent_data, bool display_ui, const std::optional<std::string> &label_hint) {
					if(display_ui) {
						std::string label = label_hint.has_value() ? label_hint.value() : "value";

						ImGui::InputScalarN(label.c_str(), data_type, &persistent_data[0], components, nullptr,
											nullptr);
					}

					nlohmann::json glm_vector_json{};

					for (int i = 0; components > i; ++i) {
						glm_vector_json.push_back(persistent_data[i]);
					}

					return glm_vector_json;
				},
				.constructor_function = [] { return Type{};}
			};
		}
		
		template <typename Type>
		static ParseFunction parse_function_wrapper(const std::function<Type(const nlohmann::json::value_type&)> &parse_function) {
			return [parse_function] (const nlohmann::json::value_type &json) {
				Type parse_type = parse_function(json);

				return new Type(parse_type);
			};
		}

		template<typename Type, typename UiFunctionsType>
		void register_type_base(std::function<Type (const nlohmann::json::value_type&)> parse_function, std::optional<UiFunctions<UiFunctionsType>> ui_functions = {},
								bool register_supplemental_types = true);

		template<typename Type, typename UiFunctionsType>
		void register_type_base_no_vector(std::function<Type(const nlohmann::json::value_type&)> parse_function,
                                          std::optional<UiFunctions<UiFunctionsType>> ui_functions, bool register_supplemental_types);

		void parse_properties_on_type(void *type_instance, const ClassInfo &class_info,
									  const nlohmann::json::value_type &node_json);

		void register_type_unsafe(StdTypeInfoRef type_info, ParseFunction parse_function, DeleterFunction deleter_function, std::optional<InternalUiFunctions> ui_functions = {});

		// basic types which form properties for parsing of registered types
		std::unordered_map<StdTypeInfoRef, TypeInfo, StdTypeInfoRefHasher> m_type_infos;

		// classes which contain properties to parse and serve a purpose.
		// they can also serve as types for properties of classes, say if you have a node within a node
		std::unordered_map<StdTypeInfoRef, ClassInfo, StdTypeInfoRefHasher> m_class_infos;
		std::unordered_map<StdTypeInfoRef, StdTypeInfoRef, StdTypeInfoRefHasher> m_class_ptr_info_aliases;

		std::unordered_map<std::string_view, StdTypeInfoRef> m_class_type_names;

		std::unordered_map<StdTypeInfoRef, std::vector<EventCallbackInfo>, StdTypeInfoRefHasher> m_event_callbacks;

		std::unordered_map<std::string, std::vector<PersistentData>> m_node_ui_persistent_data;
	};
}

#include "class_db.inl"

#endif