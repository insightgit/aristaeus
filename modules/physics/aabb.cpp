//
// Created by bobby on 12/19/21.
//

#include "aabb.hpp"

const unsigned int aristaeus::physics::AABB::AABB_INDICES[38] = {
    // top square
    2, 0, 1,
    2, 1, 3,

    // north rectangle side
    4, 0, 1,
    4, 1, 5,

    // east rectangle side
    5, 1, 3,
    5, 3, 7,

    // south rectangle side
    6, 2, 3,
    6, 3, 7,

    // west rectangle side
    4, 0, 2,
    4, 2, 6,

    // bottom square
    6, 4, 5,
    6, 5, 7,
     0, 6
};
bool aristaeus::physics::AABB::EBO_VAO_inited = false;
unsigned int aristaeus::physics::AABB::EBO = 0;
unsigned int aristaeus::physics::AABB::VAO = 0;

aristaeus::physics::AABB::AABB(glm::vec3 center, glm::vec3 extents) {
    set_center(center);
    set_extents(extents);
}

aristaeus::physics::AABB::AABBFace
aristaeus::physics::AABB::get_first_colliding_face(const aristaeus::physics::AABB &other) const {
    if(!contains_AABB(other)) {
        return AABBFace::NONE;
    }

    AABB large_x = *this;
    AABB small_x = other;

    if(m_extents.x < other.m_extents.x) {
        large_x = other;
        small_x = *this;
    }

    if(small_x.m_min_extents.x >= large_x.m_min_extents.x &&
       small_x.m_max_extents.x <= large_x.m_max_extents.x) {
        AABB large_y = *this;
        AABB small_y = other;

        if (m_extents.y < other.m_extents.y) {
            large_y = other;
            small_y = *this;
        }

        if (small_y.m_min_extents.y >= large_y.m_min_extents.y &&
            small_y.m_max_extents.y <= large_y.m_max_extents.y) {
            return AABBFace::INSIDE;
        } else if (small_y.m_min_extents.y < large_y.m_min_extents.y) {
            assert(small_y.m_max_extents.y <= large_y.m_max_extents.y);

            return AABBFace::TOP;
        } else if (small_y.m_max_extents.y > large_y.m_max_extents.y) {
            assert(small_y.m_min_extents.y >= large_y.m_min_extents.y);

            return AABBFace::BOTTOM;
        }
    } else if(small_x.m_min_extents.x < large_x.m_min_extents.x) {
        assert(small_x.m_max_extents.x <= large_x.m_max_extents.x);

        return AABBFace::LEFT;
    } else if(small_x.m_max_extents.x > large_x.m_max_extents.x) {
        assert(small_x.m_min_extents.x >= large_x.m_min_extents.x);

        return AABBFace::RIGHT;
    }

    return AABBFace::NONE;
}
