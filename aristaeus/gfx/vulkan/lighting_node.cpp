#include "lighting_node.hpp"

ARISTAEUS_CLASS_IMPL(aristaeus::gfx::vulkan::LightingNode, aristaeus::core::StdTypeInfoRef{ typeid(aristaeus::gfx::SceneGraphNode) })

void aristaeus::gfx::vulkan::LightingNode::classdb_register() {
	core::ClassDB &class_db = *core::ClassDB::instance;

	class_db.register_event_callback_on_construction<LightingNode, void*>(std::string{SceneGraphNode::PARSING_DONE_EVENT}, "lighting_node_parsing_done", [](LightingNode &node, void*) {node.on_parsing_done();});
}

void aristaeus::gfx::vulkan::LightingNode::on_parsing_done() {
	add_node_group(std::string{ LIGHTING_NODE_GROUP });
}