//
// Created by bobby on 16/04/23.
//

#include "chunk_prop_loader.hpp"

#include "chunked_terrain.hpp"

aristaeus::gfx::vulkan::ChunkPropLoader::ChunkPropLoader(
        aristaeus::gfx::vulkan::GraphicsPipeline &instanced_mesh_pipeline,
        std::shared_ptr<PropData> &props, size_t amount_of_props, const glm::ivec2 &height_map_resolution,
        ChunkedTerrain &chunked_terrain, CoreRenderer &core_renderer, const std::shared_ptr<std::mt19937> &random_num_generator,
        const glm::vec2 &current_chunk_position) :
      m_instanced_mesh_pipeline(instanced_mesh_pipeline), m_prop_data(props),
      m_random_num_generator(random_num_generator), m_prop_noise_generator(*m_random_num_generator),
      m_current_chunk_position(current_chunk_position) {
    std::vector<InstanceInput> instance_inputs;
    assert(m_prop_data != nullptr);

    float total_weights = 0.0f;

    for(Prop &prop : m_prop_data->props) {
        total_weights += prop.weight;
    }

    std::uniform_real_distribution<double> prop_selecting_distribution {0, total_weights};

    m_prop_noise_map.reserve(height_map_resolution.y);

    for(int y = 0; height_map_resolution.y > y; ++y) {
        std::vector<float> height_map_row;

        height_map_row.reserve(height_map_resolution.x);

        for(int x = 0; height_map_resolution.x > x; ++x) {
            glm::vec2 xy_coords {static_cast<float>(x) / height_map_resolution.x,
                                 static_cast<float>(y) / height_map_resolution.y};

            height_map_row.push_back(m_prop_noise_generator.perlin_with_octaves(1.0f, 0.5f, 8, 0.3f, xy_coords));
        }

        m_prop_noise_map.push_back(height_map_row);
    }

    for(std::pair<const std::string, PropGroup> &prop_group : m_prop_data->prop_groups) {
        PropRangeInfo prop_range_info;

        for(size_t y = 0; m_prop_noise_map.size() > y; ++y) {
            for(size_t x = 0; m_prop_noise_map[0].size() > x; ++x) {
                assert(m_prop_noise_map[0].size() == m_prop_noise_map[x].size());
                glm::vec2 xy_coords {static_cast<float>(x) / height_map_resolution.x,
                                     static_cast<float>(y) / height_map_resolution.y};
                float height = chunked_terrain.get_smoothed_height_map_value(xy_coords, current_chunk_position,
                                                                             core_renderer.get_current_frame());
                float slope = chunked_terrain.get_smoothed_height_slope(xy_coords, current_chunk_position,
                                                                        core_renderer.get_current_frame());

                if(check_for_prop_group_compliance(prop_group.second, m_prop_noise_map[y][x], height, slope)) {
                    prop_range_info.grid_positions.push_back({x, y});
                }
            }
        }

        m_prop_ranges.insert({&prop_group.second, prop_range_info});
    }

    for(Prop &prop : m_prop_data->props) {
        if(prop.self_prop_group.has_value()) {
            for(size_t y = 0; m_prop_noise_map.size() > y; ++y) {
                for (size_t x = 0; m_prop_noise_map[0].size() > x; ++x) {
                    glm::vec2 xy_coords {static_cast<float>(x) / height_map_resolution.x,
                                         static_cast<float>(y) / height_map_resolution.y};
                    PropRangeInfo prop_range_info;
                    float height = chunked_terrain.get_smoothed_height_map_value(xy_coords, current_chunk_position,
                                                                                 core_renderer.get_current_frame());
                    float slope = chunked_terrain.get_smoothed_height_slope(xy_coords, current_chunk_position,
                                                                            core_renderer.get_current_frame());

                    if(check_for_prop_compliance(prop, m_prop_noise_map[y][x], height, slope)) {
                        prop_range_info.grid_positions.push_back({x, y});
                    }
                }
            }
        } else {
            assert(prop.prop_group.has_value());

            PropGroup *prop_group = &m_prop_data->prop_groups.at(prop.prop_group.value());

            m_prop_ranges.at(prop_group).props.push_back(prop);
        }
    }

    std::unordered_set<glm::vec2, utils::Vec2Hash> grid_positions_used;

    for(size_t i = 0; amount_of_props > i; ++i) {
        std::reference_wrapper<Prop> selected_prop = m_prop_data->props.back();
        float selected_prop_probability = prop_selecting_distribution(CoreRenderer::mersenne_twister_rng);
        float running_sum = 0.0f;

        for(Prop &prop : m_prop_data->props) {
            running_sum += prop.weight;

            if (selected_prop_probability >= running_sum) {
                selected_prop = prop;
            }
        }

        size_t grid_i = 0;
        // TODO(Bobby): make this work with self prop groups
        PropGroup *prop_group = &m_prop_data->prop_groups.at(selected_prop.get().prop_group.value());
        PropRangeInfo &prop_range_info = m_prop_ranges.at(prop_group);

        std::vector<glm::ivec2> shuffled_positions = prop_range_info.grid_positions;

        if(shuffled_positions.empty()) {
            continue;
        }

        for(size_t i2 = shuffled_positions.size() - 1; 0 < i2; --i2) {
            std::uniform_int_distribution<size_t> shuffle_dist{0, i2};
            size_t shuffle_item = shuffle_dist(*random_num_generator);

            glm::ivec2 temp = shuffled_positions[shuffle_item];

            shuffled_positions[shuffle_item] = shuffled_positions[i2];
            shuffled_positions[i2] = temp;
        }

        while(grid_i < shuffled_positions.size() &&
              grid_positions_used.find(shuffled_positions[grid_i]) != grid_positions_used.end()) {
            ++grid_i;
        }

        if(grid_i < shuffled_positions.size() &&
           grid_positions_used.find(shuffled_positions[grid_i]) == grid_positions_used.end()) {
            const glm::vec3 normal{0, -1.0f, 0};

            float vertex_resolution = chunked_terrain.get_vertex_resolution();

            glm::ivec2 noise_grid_position = shuffled_positions[grid_i];
            glm::vec2 noise_grid_pos_normalized = glm::vec2(noise_grid_position) / glm::vec2(height_map_resolution);
            glm::vec2 actual_grid_offset_pos = glm::vec2(vertex_resolution) * noise_grid_pos_normalized;
            glm::vec2 actual_grid_position = (-vertex_resolution / 2.0f) + actual_grid_offset_pos;

            float height_value = chunked_terrain.get_smoothed_height_map_value(noise_grid_pos_normalized,
                                                                               m_current_chunk_position,
                                                                               core_renderer.get_current_frame());

                glm::vec3 offset_position =
                        glm::vec3(actual_grid_position.x, 0, actual_grid_position.y) +
                        chunked_terrain.get_chunk_world_position(current_chunk_position);
                glm::mat4 instance_matrix{1.0f};

            offset_position += normal * height_value;

            // another tangent-bitangent-normal matrix for prop terrain alignment
            glm::ivec2 next_x_height_vector = (noise_grid_position + glm::ivec2(1, 0)) / height_map_resolution;
            //glm::ivec2 next_y_height_vector = noise_grid_position + glm::ivec2(0, 1);

            glm::vec3 next_height_value {chunked_terrain.get_smoothed_height_map_value(next_x_height_vector,
                                                                                       m_current_chunk_position,
                                                                                       core_renderer.get_current_frame())};

            next_height_value *= normal;
            //float next_height_y_value = chunked_terrain.get_height_value(next_y_height_vector)  * -chunked_terrain.get_height_magnitude();

            glm::vec3 tangent = glm::normalize(next_height_value - offset_position);
            glm::vec3 bitangent = glm::normalize(glm::cross(normal, tangent));
            //glm::vec3 bitangent = glm::normalize(glm::vec3(next_height_y_value - offset_position.y));
            //glm::vec3 normal = glm::normalize(glm::cross(tangent, bitangent));

            //float degrees = 0;

            /*float slope = chunked_terrain.get_smoothed_height_slope(noise_grid_pos_normalized,
                                                                    m_current_chunk_position,
                                                                    core_renderer.get_current_frame());*/

                //std::cout << "slope: " << slope << "\n";


            //glm::mat4 rotation_matrix = glm::rotate(glm::mat4{1.0f}, glm::radians(), glm::vec3(0, 1, 0));

            instance_matrix = glm::translate(glm::mat4{1.0f}, offset_position);

            tangent = normalize(glm::vec3(instance_matrix * glm::vec4(tangent, 0.0f)));
            bitangent = normalize(glm::vec3(instance_matrix * glm::vec4(bitangent, 0.0f)));

            glm::mat4 tbn_matrix = glm::mat4(glm::mat3(tangent, bitangent, normal));

            instance_matrix = instance_matrix * tbn_matrix;

            // probably is buggy on chunk transition
            instance_inputs.push_back({.instance_matrix_column0 = instance_matrix[0],
                                       .instance_matrix_column1 = instance_matrix[1],
                                       .instance_matrix_column2 = instance_matrix[2],
                                       .instance_matrix_column3 = instance_matrix[3]});

            if(m_prop_and_instance_numbers.contains(&selected_prop.get().prop_model)) {
                ++m_prop_and_instance_numbers.at(&selected_prop.get().prop_model);
            } else {
                m_prop_and_instance_numbers.insert({&selected_prop.get().prop_model, 1});
            }

            grid_positions_used.insert(noise_grid_position);
        }
    }
    size_t instance_inputs_byte_size = sizeof(InstanceInput) * instance_inputs.size();


    if(instance_inputs_byte_size > 0) {
        m_prop_staging_buffer = core_renderer.get_memory_manager().
            create_buffer(core_renderer, instance_inputs_byte_size, vk::BufferUsageFlagBits::eTransferSrc,
                vk::SharingMode::eExclusive, VMA_ALLOCATION_CREATE_HOST_ACCESS_SEQUENTIAL_WRITE_BIT,
                VMA_MEMORY_USAGE_AUTO, "prop-staging-buffer");

        void* buffer_data;

        vmaMapMemory(m_prop_staging_buffer->allocator, m_prop_staging_buffer->allocation, &buffer_data);

        std::memcpy(buffer_data, instance_inputs.data(), instance_inputs_byte_size);

        vmaUnmapMemory(m_prop_staging_buffer->allocator, m_prop_staging_buffer->allocation);

        // TODO(Bobby): integrate with assimp model for better cache locality
        m_prop_buffer = core_renderer.get_memory_manager().
            create_buffer(core_renderer, instance_inputs_byte_size,
                vk::BufferUsageFlagBits::eVertexBuffer | vk::BufferUsageFlagBits::eTransferDst,
                vk::SharingMode::eExclusive, {}, VMA_MEMORY_USAGE_AUTO_PREFER_DEVICE,
                std::string("prop-device-buffer"));

        core_renderer.copy_buffer(m_prop_staging_buffer, m_prop_buffer);

        core_renderer.transfer_staging_buffer_for_deletion(std::move(m_prop_staging_buffer));
    }
}

void aristaeus::gfx::vulkan::ChunkPropLoader::draw(const vk::CommandBuffer &command_buffer,
                                                   const glm::mat4 &view_matrix, bool light_emitter,
                                                   vk::raii::PipelineLayout &pipeline_layout,
                                                   const glm::mat4 &light_space_matrix) {
    if (m_prop_staging_buffer == nullptr && m_prop_buffer == nullptr) {
        return;
    }

    assert(m_prop_buffer == nullptr);
    CoreRenderer &core_renderer = utils::get_singleton_safe<CoreRenderer>();

    m_instanced_mesh_pipeline.bind_to_command_buffer(command_buffer, core_renderer);

    command_buffer.bindVertexBuffers(1, m_prop_buffer->buffer, static_cast<vk::DeviceSize>(0));

    for(std::pair<AssimpModel* const, size_t> &prop_and_count : m_prop_and_instance_numbers) {
        //TODO(Bobby): light_emitter, light_space_matrix
        prop_and_count.first->draw(command_buffer, pipeline_layout, prop_and_count.second);
    }
}

bool aristaeus::gfx::vulkan::ChunkPropLoader::compare_ranges(float value, const std::vector<glm::vec2> &ranges) {
    if(ranges.empty()) {
        return true;
    }

    for(const glm::vec2 &current_range : ranges) {
        if(value >= current_range.x && value < current_range.y) {
            return true;
        }
    }

    return false;
}

bool aristaeus::gfx::vulkan::ChunkPropLoader::check_for_prop_compliance(const Prop &prop, float perlin, float height,
                                                                        float slope) {
    std::vector<glm::vec2> height_ranges;
    std::vector<glm::vec2> perlin_ranges;
    std::vector<glm::vec2> slope_ranges;

    if(prop.prop_group.has_value()) {
        PropGroup &prop_group = m_prop_data->prop_groups.at(prop.prop_group.value());

        height_ranges = prop_group.height_ranges;
        perlin_ranges = prop_group.perlin_ranges;
        slope_ranges = prop_group.slope_ranges;
    }

    if(prop.self_prop_group.has_value()) {
        if(prop.self_prop_group->self_group_override) {
            height_ranges = prop.self_prop_group->height_ranges;
            perlin_ranges = prop.self_prop_group->perlin_ranges;
            slope_ranges = prop.self_prop_group->slope_ranges;
        } else {
            perlin_ranges.insert(perlin_ranges.end(), prop.self_prop_group->perlin_ranges.begin(),
                                 prop.self_prop_group->perlin_ranges.end());
            height_ranges.insert(height_ranges.end(), prop.self_prop_group->height_ranges.begin(),
                                 prop.self_prop_group->height_ranges.end());
            slope_ranges.insert(slope_ranges.end(), prop.self_prop_group->slope_ranges.begin(),
                                prop.self_prop_group->slope_ranges.end());
        }
    }


    return compare_ranges(perlin, perlin_ranges) && compare_ranges(height, height_ranges) &&
           compare_ranges(slope, slope_ranges);
}
