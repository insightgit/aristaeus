#ifndef ARISTAEUS_SINGLETON_HPP
#define ARISTAEUS_SINGLETON_HPP

#include "uncopyable.hpp"

namespace aristaeus::core {
	template<typename SingletonType>
	class Singleton : public Uncopyable {
	public:
		static SingletonType *instance;

		Singleton() {
			assert(instance == nullptr);

			instance = reinterpret_cast<SingletonType*>(this);
		}

		virtual ~Singleton() {
			assert(instance != nullptr);
			instance = nullptr;
		}
	};
}

#endif