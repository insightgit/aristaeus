//
// Created by bobby on 6/4/22.
//

#include "uniform_buffer.hpp"

#include "core_renderer.hpp"
//#include "graphics_pipeline.hpp"

aristaeus::gfx::vulkan::UniformBuffer::UniformBuffer(CoreRenderer &core_renderer,
                                                     vk::ShaderStageFlags active_shader_stages, size_t buffer_size) :
                     m_buffer_size(buffer_size),
                     m_descriptor_set_layout(create_descriptor_set_layout(core_renderer.get_logical_device_ref(),
                                                                          active_shader_stages)){
}

vk::raii::DescriptorSetLayout aristaeus::gfx::vulkan::UniformBuffer::create_descriptor_set_layout(
        vk::raii::Device &logical_device, vk::ShaderStageFlags active_shader_stages) {
    vk::DescriptorSetLayoutBinding set_layout_info {0, vk::DescriptorType::eUniformBuffer, 1, active_shader_stages};

    return {logical_device, {{}, set_layout_info}};
}

std::array<vk::raii::DescriptorSet, utils::vulkan::MAX_FRAMES_IN_FLIGHT>
aristaeus::gfx::vulkan::UniformBuffer::create_descriptor_sets(vk::raii::Device &logical_device,
                                                              RenderContextID render_context_id) {
    if(!m_descriptor_pools.contains(render_context_id)) {
        m_descriptor_pools.insert({render_context_id, create_descriptor_pool(logical_device)});
    }

    std::array<vk::DescriptorSetLayout, utils::vulkan::MAX_FRAMES_IN_FLIGHT> descriptor_set_layouts
            {*m_descriptor_set_layout,
             *m_descriptor_set_layout};
    vk::DescriptorSetAllocateInfo set_allocate_info {*m_descriptor_pools.at(render_context_id), descriptor_set_layouts};
    std::vector<vk::raii::DescriptorSet> new_sets_vector = logical_device.allocateDescriptorSets(set_allocate_info);

    return {std::move(new_sets_vector[0]), std::move(new_sets_vector[1])};
}

void
aristaeus::gfx::vulkan::UniformBuffer::write_to_descriptor(const void *data, size_t data_size,
                                                           uint32_t frame_number,
                                                           RenderContextID render_context_id,
                                                           CoreRenderer &core_renderer) {
    if(!m_descriptor_sets.contains(render_context_id)) {
        get_ready_for_descriptor_write(render_context_id, core_renderer);
    }

    copy_data_to_buffer(data, data_size, core_renderer);

    vk::DescriptorBufferInfo buffer_info {m_pending_descriptor_write_buffers[render_context_id][frame_number]->buffer, 0,
                                          m_buffer_size};

    vk::WriteDescriptorSet descriptor_set_write {*m_descriptor_sets.at(render_context_id)[frame_number], 0,
                                                 0, vk::DescriptorType::eUniformBuffer, {}};

    m_pending_descriptor_writes[render_context_id].push_back({descriptor_set_write, buffer_info});

    m_pending_descriptor_writes[render_context_id].back().first.pBufferInfo =
            &m_pending_descriptor_writes[render_context_id].back().second;
    m_pending_descriptor_writes[render_context_id].back().first.descriptorCount = 1;
}

vk::raii::DescriptorSet &aristaeus::gfx::vulkan::UniformBuffer::get_descriptor_to_bind(
                                                                                CoreRenderer &core_renderer,
                                                                                uint32_t frame_number,
                                                                                RenderContextID render_context_id) {
    vk::raii::Device &logical_device = core_renderer.get_logical_device_ref();

    if(m_pending_descriptor_writes.contains(render_context_id) &&
       !m_pending_descriptor_writes[render_context_id].empty()) {
        assert(m_descriptor_sets.contains(render_context_id));

        std::vector<vk::WriteDescriptorSet> pending_descriptor_writes;

        for(std::pair<vk::WriteDescriptorSet, vk::DescriptorBufferInfo> &pending_write :
            m_pending_descriptor_writes.at(render_context_id)) {
            pending_descriptor_writes.push_back(pending_write.first);

            pending_descriptor_writes.back().pBufferInfo = &pending_write.second;
        }

        logical_device.updateDescriptorSets(pending_descriptor_writes, {});

        m_pending_descriptor_writes.at(render_context_id).clear();
    } else if(!m_descriptor_sets.contains(render_context_id)) {
        get_ready_for_descriptor_write(render_context_id, core_renderer);
    }

    return m_descriptor_sets.at(render_context_id)[frame_number];
}

void aristaeus::gfx::vulkan::UniformBuffer::write_to_all_descriptors(const void *data, size_t data_size,
                                                                     RenderContextID render_context_id,
                                                                     CoreRenderer &core_renderer) {
    if(!m_descriptor_sets.contains(render_context_id)) {
        get_ready_for_descriptor_write(render_context_id, core_renderer);
    }

    copy_data_to_buffer(data, data_size, core_renderer);

    for(size_t i = 0; m_descriptor_sets.at(render_context_id).size() > i; ++i) {
        BufferRaii &buffer_raii = m_pending_descriptor_write_buffers[render_context_id][core_renderer.get_current_frame()];

        vk::raii::DescriptorSet &descriptor_set = m_descriptor_sets.at(render_context_id)[i];

        vk::DescriptorBufferInfo buffer_info {buffer_raii->buffer, 0, m_buffer_size};
        vk::WriteDescriptorSet descriptor_set_write {*descriptor_set, 0, 0, vk::DescriptorType::eUniformBuffer, {},
                                                     buffer_info};

        m_pending_descriptor_writes[render_context_id].push_back({descriptor_set_write, buffer_info});

        m_pending_descriptor_writes[render_context_id].back().first.pBufferInfo =
                &m_pending_descriptor_writes[render_context_id].back().second;
        m_pending_descriptor_writes[render_context_id].back().first.descriptorCount = 1;
    }
}

void aristaeus::gfx::vulkan::UniformBuffer::copy_data_to_buffer(const void *data, size_t data_size,
                                                                CoreRenderer &core_renderer) {
    uint32_t current_frame = core_renderer.get_current_frame();
    RenderContextID render_context_id = core_renderer.get_current_render_context_id();

    BufferRaii &buffer_raii = m_pending_descriptor_write_buffers[render_context_id][current_frame];

    void *buffer_data;

    vmaMapMemory(buffer_raii->allocator, buffer_raii->allocation, &buffer_data);

    std::memcpy(buffer_data, data, std::min(m_buffer_size, data_size));

    vmaUnmapMemory(buffer_raii->allocator, buffer_raii->allocation);
}

void aristaeus::gfx::vulkan::UniformBuffer::get_ready_for_descriptor_write(
        aristaeus::gfx::vulkan::RenderContextID render_context_id, CoreRenderer &core_renderer) {
    vk::raii::Device &logical_device = core_renderer.get_logical_device_ref();

    assert(!m_pending_descriptor_writes.contains(render_context_id));

    std::array<BufferRaii, utils::vulkan::MAX_FRAMES_IN_FLIGHT> buffer_array {{
        create_buffer_ubo(core_renderer), create_buffer_ubo(core_renderer)
    }};
    std::pair<RenderContextID, std::array<BufferRaii, utils::vulkan::MAX_FRAMES_IN_FLIGHT>> write_buffers_init_pair {
        render_context_id, std::move(buffer_array)
    };

    m_descriptor_sets.insert({render_context_id, create_descriptor_sets(logical_device, render_context_id)});
    m_pending_descriptor_writes.insert({render_context_id, {}});
    m_pending_descriptor_write_buffers.insert(std::move(write_buffers_init_pair));
}

aristaeus::gfx::vulkan::BufferRaii aristaeus::gfx::vulkan::UniformBuffer::create_buffer_ubo(CoreRenderer& core_renderer) {
    return core_renderer.get_memory_manager().create_buffer(core_renderer, m_buffer_size,
        vk::BufferUsageFlagBits::eUniformBuffer,
        vk::SharingMode::eExclusive,
        VMA_ALLOCATION_CREATE_HOST_ACCESS_SEQUENTIAL_WRITE_BIT,
        VMA_MEMORY_USAGE_AUTO, "generic-ubo");
}
