//
// Created by bobby on 19/02/23.
//

#ifndef ARISTAEUS_TERRAIN_IMAGE_HPP
#define ARISTAEUS_TERRAIN_IMAGE_HPP

#include <string>

namespace aristaeus::gfx::vulkan {
    struct Image {
        std::string image_path;
        std::string image_type;

        bool operator==(const Image &image) const noexcept {
            return image_path == image.image_path && image_type == image.image_type;
        }
    };
}

template<>
struct std::hash<aristaeus::gfx::vulkan::Image> {
    size_t operator()(const aristaeus::gfx::vulkan::Image &image) const noexcept {
        std::hash<std::string> hasher;

        return hasher(image.image_type) + hasher(image.image_path);
    }
};

#endif //ARISTAEUS_TERRAIN_IMAGE_HPP
