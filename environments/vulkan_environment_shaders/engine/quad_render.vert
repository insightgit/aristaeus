#version 450

layout (location = 0) out vec2 uv_coords;

const vec2 QUAD_UV_COORDS[6] = vec2[](vec2(0, 1), vec2(1, 0), vec2(0, 0), vec2(0, 1), vec2(1, 1), vec2(1, 0));

layout (push_constant) uniform QuadConstants {
	vec2 quad_start;
	vec2 quad_extents;
} constants;

void main() {
	int vertex_index = int(gl_VertexIndex);

    uv_coords = constants.quad_start + (constants.quad_extents * QUAD_UV_COORDS[vertex_index]);

	gl_Position = vec4(uv_coords * 2.0f - 1.0f, 0.0f, 1.0f);
}