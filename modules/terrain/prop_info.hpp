//
// Created by bobby on 17/04/23.
//

#ifndef ARISTAEUS_PROP_INFO_HPP
#define ARISTAEUS_PROP_INFO_HPP

#include "chunk_prop_loader.hpp"

namespace aristaeus::gfx::vulkan {
    struct PropInfo {
        bool enabled;
        GraphicsPipeline *instanced_mesh_pipeline;
        std::shared_ptr<ChunkPropLoader::PropData> prop_data;
        size_t amount_of_props;
    };
}

#endif //ARISTAEUS_PROP_INFO_HPP
