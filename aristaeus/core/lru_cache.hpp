//
// Created by bobby on 04/03/23.
//

#ifndef ARISTAEUS_LRU_CACHE_HPP
#define ARISTAEUS_LRU_CACHE_HPP

#include <cassert>

#include <chrono>
#include <list>
#include <mutex>
#include <optional>
#include <unordered_map>
#include <unordered_set>

namespace aristaeus {
    template <typename K, typename V, typename Hash = std::hash<K>>
    class LRUCache {
    public:
        using DependencyType = uint32_t;

        struct LRUItem {
            K key;
            V value;
            std::unordered_set<DependencyType> dependencies;
        };

        using TimeList = std::list<LRUItem>;

        explicit LRUCache(size_t capacity, bool delay_deletion) : m_capacity(capacity),
                                                                  m_delay_deletion(delay_deletion) {}

        // TODO(Bobby): implement this, also convert data structure to non ptr stuff
        ~LRUCache() {}

        // when we mean "reserve", we mean assigning responsibility to one given thread to put in a terrain to an LRU,
        // so we don't accidentally have duplicate terrains from different threads and dependencies we can't predict.
        bool reserve_atomic(const K &key_to_reserve) {
            std::lock_guard<std::mutex> lock_guard{m_mutex};

            if (m_reserved_keys.contains(key_to_reserve)) {
                m_reserved_keys.insert(key_to_reserve);
                return true;
            }
            else {
                return false;
            }
        }

        // bool return type: did we have to evict something or not?
        bool put(std::pair<K, V> key_value_pair, std::optional<DependencyType> dependency) {
            // TODO(Bobby): Implement inserting the same object twice
            std::lock_guard<std::mutex> lock_guard {m_mutex};

            if (m_reserved_keys.contains(key_value_pair.first)) {
                m_reserved_keys.erase(key_value_pair.first);
            }

            assert(m_time_map.size() == m_time_list.size());
            assert(m_time_list.size() <= m_capacity);
            if(m_time_map.contains(key_value_pair.first)) {
                m_time_map.at(key_value_pair.first)->value = key_value_pair.second;

                std::cout << "";

                get_without_locking(key_value_pair.first, dependency);

                return false;
            } else {
                LRUItem lru_item {key_value_pair.first, key_value_pair.second};

                m_time_list.push_front(lru_item);

                added_obj(m_time_list.front().value);

                if(dependency.has_value()) {
                    m_time_list.back().dependencies.insert(dependency.value());
                }

                std::pair<K, typename TimeList::iterator> time_map_pair {m_time_list.front().key, m_time_list.begin()};

                m_time_map.insert(time_map_pair);

                if(m_time_list.size() > m_capacity) {
                    std::cout << "evicted item from lru cache\n";   

                    queued_for_deletion(m_time_list.back().value);

                    if(m_delay_deletion) {
                        m_reaper_map.insert({m_time_list.back().key, {nullptr, m_time_list.back().dependencies}});

                        m_reaper_map[m_time_list.back().key].first.swap(m_time_list.back().value);
                    }

                    m_time_map.erase(m_time_list.back().key);
                    m_time_list.pop_back();

                    return true;
                } else {
                    return false;
                }
            }
        }

        std::optional<V> peek(K key) {
            if (m_time_map.contains(key)) {
                return (*m_time_map.at(key))->value;
            } else {
                return {};
            }
        }

        std::optional<V> get(K key, std::optional<DependencyType> dependency) {
            // time list size and time map size should both be constant in C++11
            std::lock_guard<std::mutex> lock_guard {m_mutex};
            return get_without_locking(key, dependency);
        }

        size_t get_capacity() const {
            return m_capacity;
        }

        bool at_capacity() const {
            return get_capacity() == m_time_list.size();
        }

        // accesses here DO NOT update the cache status
        const TimeList &get_underlying_time_list() const {
            return m_time_list;
        }

        void delete_unused(std::optional<DependencyType> dependency) {
            using ReaperItem = std::pair<const K, std::pair<V, std::unordered_set<DependencyType>>>;

            if(m_delay_deletion) {
                if(dependency.has_value()) {
                    for(ReaperItem &reaper_item : m_reaper_map) {
                        reaper_item.second.second.erase(dependency.value());
                    }
                }

                if(!m_reaper_map.empty()) {
                    std::cout << "getting rid of " << m_reaper_map.size() << " objects on the reaper list\n";
                }

                std::vector<K> keys_to_erase;

                for(ReaperItem &reaper_item : m_reaper_map) {
                    if (reaper_item.second.second.empty()) {
                        keys_to_erase.push_back(reaper_item.first);
                    }
                }

                for(K &key : keys_to_erase) {
                    m_reaper_map.erase(key);
                }
            }
        }
    protected:
        void update_all_with_dependency(const DependencyType &dependency_type) {
            for (auto lru_item : m_time_list) {
                lru_item.dependencies.insert(dependency_type);
            }
        }

        virtual void added_obj(V &lru_value) {}

        virtual void queued_for_deletion(V &lru_value) {}
    private:
        std::optional<V> get_without_locking(K key, std::optional<DependencyType> dependency) {
            // time list size and time map size should both be constant in C++11
            assert(m_time_map.size() == m_time_list.size());
            assert(m_time_list.size() <= m_capacity);

            if(m_time_map.contains(key)) {
                typename TimeList::iterator &map_iterator = m_time_map.at(key);
                LRUItem map_value = *map_iterator;

                m_time_list.erase(map_iterator);
                m_time_list.push_front(map_value);

                map_iterator = m_time_list.begin();

                if(dependency.has_value()) {
                    m_time_list.front().dependencies.insert(dependency.value());
                }

                return m_time_list.front().value;
            } else {
                return {};
            }
        }

        std::unordered_set<K, Hash> m_reserved_keys;

        size_t m_capacity;
        std::mutex m_mutex;
        std::unordered_map<K, typename TimeList::iterator, Hash> m_time_map;

        std::unordered_map<K, std::pair<V, std::unordered_set<DependencyType>>, Hash> m_reaper_map;

        TimeList m_time_list;
        bool m_delay_deletion;
    };
}

#endif //ARISTAEUS_LRU_CACHE_HPP
