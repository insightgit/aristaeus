//
// Created by bobby on 9/11/21.
//

#ifndef ARISTAEUS_UNCOPYABLE_HPP
#define ARISTAEUS_UNCOPYABLE_HPP

namespace aristaeus {
    class Uncopyable {
    public:
        Uncopyable() = default;

        Uncopyable(const Uncopyable&) = delete;

        Uncopyable& operator= (const Uncopyable&) = delete;
    };
}

#endif //ARISTAEUS_UNCOPYABLE_HPP
