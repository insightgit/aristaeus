//
// Created by bobby on 8/26/21.
//

#include "core_renderer.hpp"
#include "../../core/scene_loader.hpp"

#include "assimp_model.hpp"
#include "geometry/geometry.hpp"

#include "../../libraries/include/imgui_stdlib.h"

ARISTAEUS_CLASS_IMPL(aristaeus::gfx::vulkan::AssimpModel, aristaeus::core::StdTypeInfoRef(typeid(aristaeus::gfx::SceneGraphNode)));

const vk::ImageViewCreateInfo aristaeus::gfx::vulkan::AssimpModel::DEFAULT_IMAGE_VIEW_CREATE_INFO{ {}, {}, vk::ImageViewType::e2D, vk::Format::eR8G8B8A8Srgb,
                                                                                                  {}, {vk::ImageAspectFlagBits::eColor, 0, 1, 0, 1} };

const std::unordered_set<std::string_view> aristaeus::gfx::vulkan::AssimpModel::GEOMETRY_TYPES{ CYLINDER_GEOMETRY_TYPE, RECTANGULAR_PRISM_GEOMETRY_TYPE, SPHERE_GEOMETRY_TYPE, TORUS_GEOMETRY_TYPE, NO_PRIM_GEOMETRY_TYPE };

void aristaeus::gfx::vulkan::AssimpModel::classdb_register() {
    assert(aristaeus::core::ClassDB::instance != nullptr);

    auto &class_db = *aristaeus::core::ClassDB::instance;

    class_db.register_type<PBRMaterialInfo>([](const nlohmann::json::value_type &json) {
        return PBRMaterialInfo{
            .roughness = json.contains("roughness") ? json.at("roughness").template get<float>() : 0.0f,
            .metallic = json.contains("metallic") ? json.at("metallic").template get<float>() : 0.0f,
            .ao = json.contains("ambient") ? json.at("ambient").template get<float>() : 0.0f,
            .using_gltf_metallicroughness_texture = json.contains("using_gltf_metallicroughness_texture") ? json.at("using_gltf_metallicroughness_texture").template get<int>() : -1,
            .albedo = json.contains("albedo") ? utils::get_vec4_json(json.at("albedo")) : glm::vec4{0, 0, 0, 1},
            .normal = json.contains("normal") ? utils::get_vec3_json(json.at("normal")) : glm::vec3{},
        };
    }, core::ClassDB::UiFunctions<PBRMaterialInfo>{
        .parse_function = [](PBRMaterialInfo &persistent_state, bool display_gui, const std::optional<std::string> &label_hint) {
            nlohmann::json return_value{};

            if(display_gui && (!label_hint.has_value() || ImGui::TreeNode(label_hint.value().c_str()))) {
                ImGui::InputFloat3("albedo", &persistent_state.albedo[0]);
                ImGui::InputFloat3("normal", &persistent_state.normal[0]);

                ImGui::InputFloat("roughness", &persistent_state.roughness);
                ImGui::InputFloat("metallic", &persistent_state.metallic);
                ImGui::InputFloat("ambient", &persistent_state.ao);
                ImGui::Checkbox("using_gltf_metallicroughness_texture", reinterpret_cast<bool*>(&persistent_state.using_gltf_metallicroughness_texture));

                if (label_hint.has_value()) {
                    ImGui::TreePop();
                }
            }

            return_value["albedo"] = utils::get_glm_json<glm::vec3>(persistent_state.albedo);
            return_value["normal"] = utils::get_glm_json<glm::vec3>(persistent_state.normal);
            return_value["roughness"] = persistent_state.roughness;
            return_value["metallic"] = persistent_state.metallic;
            return_value["ambient"] = persistent_state.ao;
            return_value["using_gltf_metallicroughness_texture"] = persistent_state.using_gltf_metallicroughness_texture;

            return return_value;
        },
        .constructor_function = [] {
            return PBRMaterialInfo{.using_gltf_metallicroughness_texture = -1};
        }
    });

    class_db.register_type<std::unique_ptr<Geometry>, GeometryUIData>([](const nlohmann::json::value_type &json) {
        assert(CoreRenderer::instance != nullptr);

        CoreRenderer &core_renderer = *CoreRenderer::instance;
        std::unique_ptr<Geometry> geom_ptr;
        std::string geometry_type = json.at("geometry_type");

        if (geometry_type == CYLINDER_GEOMETRY_TYPE) {
            geom_ptr.reset(new Cylinder{core_renderer, json.at("sector_count"), json.at("radius"), json.at("height"), DEFAULT_MODEL_INFO});
        } else if(geometry_type == RECTANGULAR_PRISM_GEOMETRY_TYPE) {
            geom_ptr.reset(new RectangularPrism{ core_renderer, utils::get_vec3_json(json.at("extents")), DEFAULT_MODEL_INFO });
        } else if (geometry_type == SPHERE_GEOMETRY_TYPE) {
            geom_ptr.reset(new Sphere{core_renderer, json.at("stack_count"), json.at("sector_count"), json.at("radius"), DEFAULT_MODEL_INFO });
        }
        else if (geometry_type == TORUS_GEOMETRY_TYPE) {
            geom_ptr.reset(new Torus{core_renderer, json.at("stack_count"), json.at("sector_count"), json.at("major_radius"), json.at("minor_radius"), DEFAULT_MODEL_INFO });
        }
        else if(geometry_type != NO_PRIM_GEOMETRY_TYPE) {
            throw std::runtime_error("Unrecognized geometry type!");
        }

        return geom_ptr;
    },
    core::ClassDB::UiFunctions<GeometryUIData>{
        .parse_function = [](GeometryUIData &persistent_state, bool display_gui, const std::optional<std::string> &label_hint) {
            if (label_hint.has_value() && !ImGui::TreeNode(label_hint.value().c_str())) {
                display_gui = false;
            }

            if(display_gui) {
                if (ImGui::BeginCombo("Geometry type", persistent_state.geom_type_selected.data()))
                {
                    for (const std::string_view &geom_type : GEOMETRY_TYPES) {
                        bool is_selected = persistent_state.geom_type_selected == geom_type;

                        if (ImGui::Selectable(geom_type.data(), is_selected)) {
                            if (!is_selected) {
                                persistent_state.supplemental_data.clear();
                            }

                            persistent_state.geom_type_selected = geom_type;
                        }

                        if (is_selected) {
                            ImGui::SetItemDefaultFocus();
                        }
                    }

                    ImGui::EndCombo();
                }
            }

            std::string_view current_geom_type = persistent_state.geom_type_selected;
            nlohmann::json return_value{};

            return_value["geometry_type"] = current_geom_type;

            if (current_geom_type == CYLINDER_GEOMETRY_TYPE) {
                if (persistent_state.supplemental_data.empty()) {
                    persistent_state.supplemental_data.resize(3);
                }

                if(display_gui) {
                    int sector_count = static_cast<int>(persistent_state.supplemental_data[0]);

                    ImGui::InputInt("Sector Count", &sector_count);

                    persistent_state.supplemental_data[0] = static_cast<float>(sector_count);

                    ImGui::InputFloat("Radius", &persistent_state.supplemental_data[1]);
                    ImGui::InputFloat("Height", &persistent_state.supplemental_data[2]);
                }
            }
            else if (current_geom_type == RECTANGULAR_PRISM_GEOMETRY_TYPE) {
                if (persistent_state.supplemental_data.empty()) {
                    persistent_state.supplemental_data.resize(3);
                }

                if(display_gui) {
                    ImGui::InputFloat3("Extents", persistent_state.supplemental_data.data());
                }
            }
            else if (current_geom_type == SPHERE_GEOMETRY_TYPE) {
                if (persistent_state.supplemental_data.empty()) {
                    persistent_state.supplemental_data.resize(3);
                }

                if(display_gui) {
                    int sector_count = static_cast<int>(persistent_state.supplemental_data[0]);
                    int stack_count = static_cast<int>(persistent_state.supplemental_data[1]);

                    ImGui::InputInt("Sector Count", &sector_count);
                    ImGui::InputInt("Stack Count", &stack_count);
                    ImGui::InputFloat("Radius", &persistent_state.supplemental_data[2]);

                    persistent_state.supplemental_data[0] = static_cast<float>(sector_count);
                    persistent_state.supplemental_data[1] = static_cast<float>(stack_count);
                }
            }
            else if (current_geom_type == TORUS_GEOMETRY_TYPE) {
                if (persistent_state.supplemental_data.empty()) {
                    persistent_state.supplemental_data.resize(4);
                }

                if(display_gui) {
                    int sector_count = static_cast<int>(persistent_state.supplemental_data[0]);
                    int stack_count = static_cast<int>(persistent_state.supplemental_data[1]);

                    ImGui::InputInt("Sector Count", &sector_count);
                    ImGui::InputInt("Stack Count", &stack_count);
                    ImGui::InputFloat("Major Radius", &persistent_state.supplemental_data[2]);
                    ImGui::InputFloat("Minor Radius", &persistent_state.supplemental_data[3]);

                    persistent_state.supplemental_data[0] = static_cast<float>(sector_count);
                    persistent_state.supplemental_data[1] = static_cast<float>(stack_count);
                }
            }
            else if(current_geom_type != NO_PRIM_GEOMETRY_TYPE) {
                throw std::runtime_error("Unrecognized geometry type!");
            }

            if (label_hint.has_value() && display_gui) {
                ImGui::TreePop();
            }

            return return_value;
        },
        .constructor_function = [] {
            return GeometryUIData{
                .geom_type_selected = NO_PRIM_GEOMETRY_TYPE
            };
        }
    });
    class_db.register_type<TextureOverride>([](const nlohmann::json::value_type &json) {
        std::unordered_set<std::string> texture_types;

        for (const nlohmann::json::value_type &type_json : json.at("types")) {
            texture_types.insert("texture_" + type_json.get<std::string>());
        }

        return TextureOverride{
            .texture_types = texture_types,
            .texture_path = core::SceneLoader::instance->get_environment_path_for_file(json.at("path"))
        };
    }, core::ClassDB::UiFunctions<TextureOverride>{
        .parse_function = [](TextureOverride &texture_override, bool display_gui, const std::optional<std::string> &label_hint) {
            core::ClassDB &class_db = *core::ClassDB::instance;

            nlohmann::json return_value{};

            if (display_gui && (!label_hint.has_value() || ImGui::TreeNode(label_hint.value().c_str()))) {
                class_db.get_already_registered_ui_functions<std::string>()->parse_function(texture_override.texture_path,
                                                                                            display_gui, "Texture Path");
                class_db.get_already_registered_ui_functions<std::unordered_set<std::string>>()->
                    parse_function(texture_override.texture_types, display_gui, "Texture Types");

                if (label_hint.has_value()) {
                    ImGui::TreePop();
                }
            }

            return return_value;
        }, .constructor_function = [] {
            return TextureOverride{};
        },
    });


    typedef std::unordered_map<std::string, std::vector<MaterialOverride>> MaterialOverridesType;

    class_db.register_type<MaterialOverride>([](const nlohmann::json &json) {
        if (!json.contains("type") && !json.contains("value")) {
            std::cerr << "Skipping incomplete material override for material\n";

            return MaterialOverride{};
        } else {
            return MaterialOverride{
                .override_name = json.at("type").get<std::string>(),
                .override_value = json.at("value").get<float>()
            };
        }
    }, core::ClassDB::UiFunctions<MaterialOverride>{
        .parse_function = [](MaterialOverride &material_override, bool display_ui, const std::optional<std::string> &label_hint){
            if(display_ui) {
                core::ClassDB::instance->get_already_registered_ui_functions<std::string>()->
                    parse_function(material_override.override_name, true, "Override Name");
                core::ClassDB::instance->get_already_registered_ui_functions<float>()->
                    parse_function(material_override.override_value, true, "Override Value");
            }

            nlohmann::json return_type;

            return_type["type"] = material_override.override_name;
            return_type["value"] = material_override.override_value;

            return return_type;
        },
        .constructor_function = []{
            return MaterialOverride{};
        }
    });

    class_db.register_unordered_map_type<std::string, std::vector<MaterialOverride>>();

    class_db.register_property<AssimpModel, PBRMaterialInfo>("custom_pbr_material", [](AssimpModel& model) {return model.m_custom_pbr_material_info; },
                                                              [](AssimpModel &model, PBRMaterialInfo pbr_material_info) {
        model.m_custom_pbr_material_info = pbr_material_info;

        if (model.m_custom_pbr_material_info.using_gltf_metallicroughness_texture == -1 && !model.m_model_path.empty()) {
            model.m_custom_pbr_material_info.using_gltf_metallicroughness_texture = 
                std::filesystem::path{ model.m_model_path }.extension() == ".gltf";
        }
    }, [](){return PBRMaterialInfo{.using_gltf_metallicroughness_texture = -1};});

    class_db.register_property<AssimpModel, bool>("debug_normals", [](AssimpModel& model) {return model.m_debug_normals; },
                                                   [](AssimpModel &model, bool debug_normals) {model.m_debug_normals = debug_normals;}, 
                                                   [](){return false;});

    class_db.register_property<AssimpModel, std::optional<std::string>>("default_normal_map_texture", [](AssimpModel& model) {return model.m_default_normal_map_texture; },
        [](AssimpModel& model, std::optional<std::string> default_normal_map_texture) {model.m_default_normal_map_texture = default_normal_map_texture; },
        []() {return std::optional<std::string>{}; });
    class_db.register_property<AssimpModel, MaterialOverridesType>("material_overrides",
        [](AssimpModel &model) {return model.m_material_overrides; },
        [](AssimpModel &model, MaterialOverridesType material_overrides) {model.m_material_overrides = material_overrides; },
        []() {return MaterialOverridesType{}; });

    class_db.register_property<AssimpModel, std::string>("model_path", [](AssimpModel &model) {return model.m_model_path; },
        [](AssimpModel &model, std::string model_path) {
            assert(core::SceneLoader::instance != nullptr);

            if (model_path.empty()) {
                model.m_model_directory = "";
                model.m_model_name = "";
                model.m_model_path = "";
            }
            else {
                std::string env_model_path = core::SceneLoader::instance->get_environment_path_for_file(model_path);

                model.m_model_directory = std::filesystem::path(env_model_path).remove_filename().string();
                model.m_model_name = std::filesystem::path(env_model_path).stem().string();
                model.m_model_path = env_model_path;

                if (model.m_custom_pbr_material_info.using_gltf_metallicroughness_texture == -1) {
                    model.m_custom_pbr_material_info.using_gltf_metallicroughness_texture =
                        std::filesystem::path{ model.m_model_path }.extension() == ".gltf";
                }
            }

        }, []{ return ""; });

    class_db.register_property<AssimpModel, bool>("material_color_override_textures", [](AssimpModel &model) {return model.m_override_texture;},
                                                  [](AssimpModel &model, bool override_texture){model.m_override_texture = override_texture;},
                                                  []() {return false;});
    class_db.register_property<AssimpModel, bool>("use_front_culling_shadow_mapping", [](AssimpModel &model) {return model.m_use_front_culling_shadow_mapping;},
                                                  [](AssimpModel &model, bool use_front_culling_shadow_mapping){model.m_use_front_culling_shadow_mapping = use_front_culling_shadow_mapping;},
                                                  []() {return false;});

    class_db.register_property<AssimpModel, bool>("bake_physx_shapes", [](AssimpModel &model) {return model.m_bake_physx_shapes;},
                                                  [](AssimpModel &model, bool bake_physx_shapes){model.m_bake_physx_shapes = bake_physx_shapes;},
                                                  []() {return false;});
    class_db.register_property<AssimpModel, std::unique_ptr<Geometry>>("prim_geometry", [](AssimpModel&) {return std::unique_ptr<Geometry>{}; }, [](AssimpModel &model, std::unique_ptr<Geometry> geom_ptr) {model.m_prim_geometry = std::move(geom_ptr); },
                                                                       []() {return std::unique_ptr<Geometry>{};});

    class_db.register_property<AssimpModel, std::vector<TextureOverride>>("texture_overrides", [](AssimpModel &model) {return model.m_texture_overrides;},
                                                  [](AssimpModel &model, std::vector<TextureOverride> texture_overrides){model.m_texture_overrides = texture_overrides;},
                                                  []() {return std::vector<TextureOverride>{};});

    auto enter_tree_wrapper = [](AssimpModel &model, void*){model.on_parsing_done();};

    class_db.register_event_callback_on_construction<AssimpModel, void*>(std::string{SceneGraphNode::ENTER_TREE_EVENT},
                                                                         "assimp_model_enter_tree",
                                                                         enter_tree_wrapper);
}

void aristaeus::gfx::vulkan::AssimpModel::on_parsing_done() {
    assert(CoreRenderer::instance != nullptr);

    if (m_prim_geometry != nullptr) {
        m_assimp_meshes.push_back({ std::move(m_prim_geometry->get_produced_assimp_mesh()), "" });

        //m_override_texture = m_prim_geometry->get_model_info().material_color_override_textures;
        //m_custom_pbr_material_info = m_prim_geometry->get_model_info().pbr_material_info;

        process_material(nullptr);
    }
    else if(!m_model_path.empty()) {
        stbi_set_flip_vertically_on_load(true);

        Assimp::Importer assimp_importer;

        const aiScene* scene = assimp_importer.ReadFile(m_model_path,
            aiProcess_Triangulate | aiProcess_GenNormals |
            aiProcess_CalcTangentSpace | aiProcess_OptimizeMeshes | aiProcess_OptimizeGraph);

        std::filesystem::path model_fs_path(m_model_path);

        if (scene == nullptr || scene->mFlags & AI_SCENE_FLAGS_INCOMPLETE || scene->mRootNode == nullptr) {
            std::string error = assimp_importer.GetErrorString();

            throw std::runtime_error(error);
        }

        for (std::pair<std::string, std::vector<MaterialOverride>> material : m_material_overrides) {
            for (MaterialOverride material_override : material.second) {
                if (material_override.override_name == "transmission" && material_override.override_value > 0.0f) {
                    m_uses_blending = true;
                    break;
                }
            }

            if (m_uses_blending) {
                break;
            }
        }

        process_node(scene->mRootNode, scene, *CoreRenderer::instance, m_bake_physx_shapes);
    }
    else {
        throw std::runtime_error("No primitive geometry or model path for AssimpModel to use!");
    }

    int i = 0;

    for (const std::pair<std::shared_ptr<AssimpMesh>, std::string> &assimp_mesh : m_assimp_meshes) {
        assimp_mesh.first->set_weak_ptr_to_this(assimp_mesh.first);
        assimp_mesh.first->rename_node(assimp_mesh.second + "-" + std::to_string(i));

        add_child(assimp_mesh.first);

        ++i;
    }
}

void aristaeus::gfx::vulkan::AssimpModel::draw() {
    auto &core_renderer = utils::get_singleton_safe<CoreRenderer>();
    RenderContextID current_render_context_id = core_renderer.get_current_render_context_id();

    if (!m_render_id_to_mat_ids_per_mesh.contains(current_render_context_id)) {
        register_pipeline_info_and_render_context_id(core_renderer.get_default_graphics_pipeline_info().value(),
                                                     core_renderer.get_default_graphics_shader_infos(),
                                                     current_render_context_id);
    }

    m_last_frame_rendered = core_renderer.get_current_frame();
    m_last_render_context = current_render_context_id;
}

void aristaeus::gfx::vulkan::AssimpModel::process_node(aiNode *node, const aiScene *scene, CoreRenderer &renderer,
                                                       bool bake_physx_shapes) {
    // TODO(Bobby): optimize already loaded meshes through mesh_generation_info
    for(uint32_t i = 0; i < node->mNumMeshes; ++i) {
        aiMesh *mesh = scene->mMeshes[node->mMeshes[i]];
        aiMaterial *material = scene->mMaterials[mesh->mMaterialIndex];

        assert(mesh != nullptr);

        m_assimp_meshes.emplace_back(process_mesh(mesh, scene, bake_physx_shapes, i),
                                     material == nullptr ? "" : std::string(material->GetName().C_Str()));

        process_material(material);
    }

    for (uint32_t i = 0; i < node->mNumChildren; ++i) {
        process_node(node->mChildren[i], scene, renderer, bake_physx_shapes);
    }
}

void aristaeus::gfx::vulkan::AssimpModel::process_material(aiMaterial *material) {
    auto &core_renderer = utils::get_singleton_safe<CoreRenderer>();
    std::string material_name = material != nullptr ? material->GetName().C_Str() : "";

    size_t material_mesh_num = m_materials_per_mesh.size();

    MaterialShaderData material_shader_data{
        .data_size = sizeof(MaterialUniformBuffer),
        .data_generator = [this, material_mesh_num](void *) {
            return get_material_mesh_ssbo_data(material_mesh_num);
        },
        .data_generator_arg = nullptr,
        .data_generator_arg_size = 0,
        .bind_location = MATERIAL_SSBO_BINDING_LOCATION,
        .used_by_shader_stages = vk::ShaderStageFlagBits::eFragment
    };

    // pipeline_info and shader_infos will be determined as
    // graphics pipelines register themselves with us
    ModelMaterial model_material{
        .material = Material{
            .material_name = material_name,
            .data = {material_shader_data},
            .textures = {}
        },
        .pbr_info = m_custom_pbr_material_info
    };

    if (material != nullptr) {
        static_assert(TEXTURE_TYPES.size() == ASSIMP_TEXTURE_TYPES.size());

        auto &material_manager = utils::get_singleton_safe<MaterialManager>();

        std::unordered_map<aiTextureType, size_t> texture_sizes;

        for (int i = 0; 20 > i; ++i) {
            texture_sizes.insert({ static_cast<aiTextureType>(i), material->GetTextureCount(static_cast<aiTextureType>(i)) });
        }

        std::vector<std::string> strings;
        bool contains_specular_property;

        for (int i = 0; material->mNumProperties > i; ++i) {
            strings.push_back(material->mProperties[i]->mKey.C_Str());
        }

        bool albedo_null_image = false;

        aiString blend_func;

        aiGetMaterialString(material, "$mat.gltf.alphaMode", 0, 0, &blend_func);

        if (std::string(blend_func.C_Str()) == "BLEND") {
            model_material.material.material_name = std::string{ BLENDED_MODEL_PREFIX_STRING } + model_material.material.material_name.value();
        }

        for (size_t i = 0; TEXTURE_TYPES.size() > i; ++i) {
            std::vector<MaterialTexture> material_textures = load_material_textures(*material, ASSIMP_TEXTURE_TYPES[i], std::string(TEXTURE_TYPES[i]),
                TEXTURE_BINDING_LOCATIONS[i]);

            if (material_textures.empty()) {
                vk::SamplerCreateInfo sampler_create_info = DEFAULT_SAMPLER_CREATE_INFO;

                sampler_create_info.maxAnisotropy = core_renderer.get_max_sampler_anisotropy();

                if (TEXTURE_TYPES[i] == NORMAL_TEXTURE_STRING && m_default_normal_map_texture.has_value()) {
                    MaterialTexture material_texture{
                        .texture_path = *m_default_normal_map_texture,
                        .sampler_create_info = sampler_create_info,
                        .image_view_create_info = DEFAULT_IMAGE_VIEW_CREATE_INFO,
                        .texture_types = std::unordered_set<std::string>{ std::string(NORMAL_TEXTURE_STRING) },
                        .bind_location = TEXTURE_BINDING_LOCATIONS[i],
                        .used_by_shader_stages = vk::ShaderStageFlagBits::eFragment,
                        .dirty = true
                    };

                    material_textures.push_back(material_texture);
                }
                else {
                    aiColor4D color;

                    switch (ASSIMP_TEXTURE_TYPES[i]) {
                    case aiTextureType_DIFFUSE:
                        if (aiGetMaterialColor(material, AI_MATKEY_COLOR_DIFFUSE, &color) == AI_SUCCESS ||
                            aiGetMaterialColor(material, AI_MATKEY_BASE_COLOR, &color) == AI_SUCCESS) {
                            model_material.pbr_info.albedo = glm::vec4(color.r, color.g, color.b, color.a);
                        }
                        break;
                    case aiTextureType_NORMALS:
                        break;
                    case aiTextureType_METALNESS:
                        aiGetMaterialFloat(material, AI_MATKEY_METALLIC_FACTOR, &model_material.pbr_info.metallic);
                        break;
                    case aiTextureType_DIFFUSE_ROUGHNESS:
                        if (aiGetMaterialFloat(material, AI_MATKEY_ROUGHNESS_FACTOR, &model_material.pbr_info.roughness) != AI_SUCCESS) {
                            // unable to find roughness factor, let's try specuclariity
                            aiColor4D color;

                            if (aiGetMaterialColor(material, AI_MATKEY_COLOR_SPECULAR, &color) == AI_SUCCESS) {
                                model_material.pbr_info.roughness = 1 - color.r;
                            }
                        }
                        break;
                    case aiTextureType_AMBIENT_OCCLUSION:
                        //aiGetMaterialFloat(material, AI_MATKEY_COLOR, &m_custom_pbr_material_info.metallic);
                        break;
                    }

                    if (ASSIMP_TEXTURE_TYPES[i] == aiTextureType_DIFFUSE) {
                        albedo_null_image = true;
                    }
                }
            }

            switch (i) {
            case 0:
                model_material.pbr_info.albedo_map_range.y = material_textures.size();
                break;
            case 1:
                model_material.pbr_info.normal_map_range.y = material_textures.size();
                break;
            case 2:
                model_material.pbr_info.metallic_map_range.y = material_textures.size();
                break;
            case 3:
                model_material.pbr_info.roughness_map_range.y = material_textures.size();
                break;
            case 4:
                model_material.pbr_info.ao_map_range.y = material_textures.size();
                break;
            }

            if ((std::string(blend_func.C_Str()) == "BLEND" || m_materials_per_mesh.empty()) && material_textures.empty()) {
                material_textures.push_back(MaterialTexture{
                    .null_image = true,
                    .texture_types = {std::string{TEXTURE_TYPES[i]}},
                    .bind_location = TEXTURE_BINDING_LOCATIONS[i],
                    .used_by_shader_stages = vk::ShaderStageFlagBits::eFragment
                });
            }

            model_material.material.textures.insert(model_material.material.textures.end(), material_textures.begin(), material_textures.end());
        }
    }

    m_materials_per_mesh.push_back(model_material);
}

std::shared_ptr<aristaeus::gfx::vulkan::AssimpMesh> aristaeus::gfx::vulkan::AssimpModel::process_mesh(aiMesh *mesh, const aiScene *scene, 
                                                                                                      bool bake_physx_shapes, uint32_t mesh_index) {
    std::vector<uint32_t> indices;
    std::vector<std::shared_ptr<Texture>> textures;
    std::vector<glm::vec3> vertices;

    for (unsigned int i = 0; i < mesh->mNumVertices; ++i) {
        ModelVertex vertex{
            .position = glm::vec3(mesh->mVertices[i].x, mesh->mVertices[i].y, mesh->mVertices[i].z),
            .normal = glm::vec3(mesh->mNormals[i].x, mesh->mNormals[i].y, mesh->mNormals[i].z)
        };

        if (mesh->mTextureCoords[0] != nullptr) {
            vertex.tangent = glm::vec3(mesh->mTangents[i].x, mesh->mTangents[i].y, mesh->mTangents[i].z);
            vertex.texture_coords = glm::vec3(mesh->mTextureCoords[0][i].x, mesh->mTextureCoords[0][i].y, 0.0f);
        }
        else {
            aiVector3D next_vertex = mesh->mVertices[i >= mesh->mNumVertices - 1 ? 0 : (i + 1)];

            vertex.tangent = glm::normalize(glm::vec3(next_vertex.x, next_vertex.y, next_vertex.z) - vertex.position);
            vertex.texture_coords = glm::vec3(0.0f);
        }

        glm::vec3 world_position = (glm::vec4(vertex.position, 1.0f) * get_model_matrix());

        if(world_position.x < m_top_left_bound.x) {
            m_top_left_bound.x = world_position.x;
        } else if(world_position.x > m_bottom_right_bound.x) {
            m_bottom_right_bound.x = world_position.x;
        }

        if(world_position.y < m_top_left_bound.y) {
            m_top_left_bound.y = world_position.y;
        } else if(world_position.y > m_bottom_right_bound.y) {
            m_bottom_right_bound.y = world_position.y;
        }

        if(world_position.z < m_top_left_bound.z) {
            m_top_left_bound.z = world_position.z;
        } else if(world_position.z > m_bottom_right_bound.z) {
            m_bottom_right_bound.z = world_position.z;
        }

        vertex.pack_model_vertex(vertices);
    }

    for (unsigned int i = 0; i < mesh->mNumFaces; ++i) {
        aiFace face = mesh->mFaces[i];

        for (unsigned int i2 = 0; i2 < face.mNumIndices; ++i2) {
            indices.push_back(face.mIndices[i2]);
        }
    }

    std::shared_ptr<AssimpMesh> assimp_mesh = std::make_shared<AssimpMesh>(vertices, indices, m_model_path + "/" + std::to_string(mesh_index));

    return assimp_mesh;
}

std::vector<aristaeus::gfx::vulkan::MaterialTexture>
    aristaeus::gfx::vulkan::AssimpModel::load_material_textures(aiMaterial &material, aiTextureType type,
                                                                const std::string &type_name, uint32_t binding_location) {
    auto &core_renderer = utils::get_singleton_safe<CoreRenderer>();

    std::vector<MaterialTexture> return_value;
    vk::SamplerCreateInfo sampler_create_info = DEFAULT_SAMPLER_CREATE_INFO;

    sampler_create_info.maxAnisotropy = core_renderer.get_max_sampler_anisotropy();

    size_t texture_count = material.GetTextureCount(type);

    if (texture_count == 0) {
        switch (type) {
        case aiTextureType_NORMALS:
            type = aiTextureType_NORMAL_CAMERA;
            break;
        case aiTextureType_DIFFUSE:
            type = aiTextureType_BASE_COLOR;
            break;
        }

        texture_count = material.GetTextureCount(type);
    }

    for(size_t i = 0; i < texture_count; ++i) {
        bool already_loaded = false;
        aiString texture_string;

        material.GetTexture(type, i, &texture_string);

        std::filesystem::path model_texture_path = m_model_directory;

        model_texture_path += std::filesystem::path{ texture_string.C_Str() };

        model_texture_path = std::filesystem::weakly_canonical(model_texture_path).make_preferred();

        MaterialTexture material_texture{
            .texture_path = model_texture_path.string(),
            .sampler_create_info = sampler_create_info,
            .image_view_create_info = DEFAULT_IMAGE_VIEW_CREATE_INFO,
            .texture_types = {type_name},
            .bind_location = binding_location,
            .used_by_shader_stages = vk::ShaderStageFlagBits::eFragment
        };

        return_value.push_back(std::move(material_texture));
    }

    return return_value;
}

[[nodiscard]] std::vector<aristaeus::gfx::vulkan::AssimpMesh::MeshGeometryData> aristaeus::gfx::vulkan::AssimpModel::get_geometry_data_of_meshes() const {
    std::vector<AssimpMesh::MeshGeometryData> return_value;

    for (const std::pair<std::shared_ptr<AssimpMesh>, std::string> &assimp_mesh : m_assimp_meshes) {
        return_value.push_back(assimp_mesh.first->get_mesh_geometry_data());
    }

    return return_value;
}



aristaeus::gfx::vulkan::MaterialShaderData::GeneratorData aristaeus::gfx::vulkan::AssimpModel::get_material_mesh_ssbo_data(size_t material_mesh_index) {
    auto &core_renderer = utils::get_singleton_safe<CoreRenderer>();
    RenderContextID render_context_id = core_renderer.get_current_render_context_id();
    size_t render_mesh_id = (static_cast<size_t>(render_context_id) << 32) | (material_mesh_index & 0x00000000FFFFFFFF) + core_renderer.get_current_frame();

    //if (!m_ubo_written_render_mesh_id.contains(render_mesh_id) || !m_ubo_written_render_mesh_id.at(render_mesh_id)) {
        // this needs to be true in order to comply with std430 padding rules
        static_assert(sizeof(MaterialUniformBuffer) % 16 == 0);

        m_material_uniform_buffer = MaterialUniformBuffer{
            .override_texture = m_override_texture,
            .object_type = m_light_emitter ? 1 : 0,
            .light_box_color = m_light_box_color,
            .pbr_material_info = m_materials_per_mesh[material_mesh_index].pbr_info
        };

        auto &material_manager = utils::get_singleton_safe<MaterialManager>();
        MaterialID material_id = material_manager.get_current_binded_material_id();

        m_material_uniform_buffer.pbr_material_info.albedo_map_range.x = material_manager.get_first_image_index_within_binding(material_id, 1);
        m_material_uniform_buffer.pbr_material_info.normal_map_range.x = material_manager.get_first_image_index_within_binding(material_id, 2);
        m_material_uniform_buffer.pbr_material_info.metallic_map_range.x = material_manager.get_first_image_index_within_binding(material_id, 3);
        m_material_uniform_buffer.pbr_material_info.roughness_map_range.x = material_manager.get_first_image_index_within_binding(material_id, 4);
        m_material_uniform_buffer.pbr_material_info.ao_map_range.x = material_manager.get_first_image_index_within_binding(material_id, 5);

        m_ubo_written_render_mesh_id[render_mesh_id] = true;

        return MaterialShaderData::GeneratorData{
            .generated_data_size = sizeof(MaterialUniformBuffer),
            .generated_data = &m_material_uniform_buffer,
        };
    /*}
    else {
        return MaterialShaderData::GeneratorData{
            .generated_data_size = 0
        };
    }*/
}

void aristaeus::gfx::vulkan::AssimpModel::register_pipeline_info_and_render_context_id(const GraphicsPipeline::PipelineInfo &pipeline_info, 
                                                                                       const std::vector<GraphicsPipeline::ShaderInfo> &shader_infos,
                                                                                       RenderContextID render_context_id) {
    assert(!m_render_id_to_mat_ids_per_mesh.contains(render_context_id));
    assert(m_materials_per_mesh.size() == m_assimp_meshes.size());

    std::vector<MaterialID> material_ids;

    for (int i = 0; m_materials_per_mesh.size() > i; ++i) {
        Material finished_material = m_materials_per_mesh[i].material;

        finished_material.pipeline_info = pipeline_info;
        finished_material.shader_infos = shader_infos;

        auto &material_manager = utils::get_singleton_safe<MaterialManager>();

        bool blended_object = finished_material.material_name.has_value();

        auto &core_renderer = utils::get_singleton_safe<CoreRenderer>();
        RenderPhase &current_render_phase = *core_renderer.get_current_render_phase();
        std::string current_render_pass = current_render_phase.get_current_render_pass();

        if (blended_object && !current_render_phase.is_render_pass_depth_only(current_render_pass)) {
            for (int i = 0; BLENDED_MODEL_PREFIX_STRING.size() > i; ++i) {
                if (finished_material.material_name->size() <= i || BLENDED_MODEL_PREFIX_STRING[i] != (*finished_material.material_name)[i]) {
                    blended_object = false;
                    break;
                }
            }

            if (blended_object) {
                finished_material.pipeline_info.pipeline_cache_path = "bl_" + finished_material.pipeline_info.pipeline_cache_path;

                size_t expected_attachment_count = finished_material.pipeline_info.blending_stage.attachmentCount;

                if (!m_blended_object_attachment_states.contains(render_context_id)) {
                    std::vector<vk::PipelineColorBlendAttachmentState> attachment_states;

                    for (int i = 0; expected_attachment_count > i; ++i) {
                        attachment_states.push_back(BLENDED_MODEL_STATE);
                    }

                    m_blended_object_attachment_states.insert({ render_context_id, attachment_states });
                }

                assert(m_blended_object_attachment_states.at(render_context_id).size() >= finished_material.pipeline_info.blending_stage.attachmentCount);

                finished_material.pipeline_info.blending_stage.pAttachments = m_blended_object_attachment_states.at(render_context_id).data();

                finished_material.material_render_priority = BLENDING_NODE_RENDER_PRIORITY;
            }
        }

        MaterialID new_material_id = material_manager.create_material(finished_material);

        material_ids.push_back(new_material_id);

        m_assimp_meshes[i].first->assign_material_id(new_material_id, render_context_id);
    }

    m_render_id_to_mat_ids_per_mesh.insert({ render_context_id, material_ids });
}