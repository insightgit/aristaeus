#ifndef ARISTAEUS_SINGLE_IMAGE_COMPUTE_TASK
#define ARISTAEUS_SINGLE_IMAGE_COMPUTE_TASK

#include "compute_task.hpp"

namespace aristaeus::gfx::vulkan::compute {
	class SingleImageComputeTask : public ComputeTask {
	public:
		struct OutputInfo {
			ImageRaii image;
			std::vector<vk::raii::ImageView> image_views;
			vk::raii::Sampler sampler;
			uint32_t number_of_mipmaps;
		};

		[[nodiscard]] std::optional<Texture> get_output_info() {
			if (get_compute_job_status() == JobStatus::Finished) {
				return std::move(m_output_texture);
			}
			else {
				return {};
			}
		}

		vk::DeviceSize get_output_size() override {
			if (!m_image_size_bytes.has_value()) {
				std::future<size_t> image_size_bytes = m_output_texture.get_image_size_bytes();

				image_size_bytes.wait();

				m_image_size_bytes = image_size_bytes.get();
			}

			return *m_image_size_bytes;
			
		}

		[[nodiscard]] glm::uvec2 get_image_size() const {
			return m_output_texture.get_image_texture_size();
		}

		void save_output(vk::CommandBuffer& command_buffer, BufferRaii& buffer, vk::DeviceSize buffer_offset)override {
			if (m_cache_path.has_value()) {
				auto &core_renderer = utils::get_singleton_safe<CoreRenderer>();

				ComputeTask::save_hdr_image(m_cache_path.value(), m_output_texture.get_image_texture(), command_buffer, buffer);
			}
		}

		bool load_from_saved_output(vk::CommandBuffer &command_buffer, BufferRaii &buffer, vk::DeviceSize buffer_offset,
                                    const std::vector<glm::ivec3> &file_image_data)override {
			throw std::runtime_error("not yet implemented!");
		}
	protected:
		static const glm::mat4 PROJECTION_MATRIX;

		SingleImageComputeTask(size_t ubo_size, uint32_t number_of_ubos, uint32_t number_of_samplers,
			ComputePipelineInfo compute_pipeline_info, const std::string& compute_task_type, const glm::ivec2& image_size, const std::optional<std::string>& cache_path,
			const vk::Format &format) :
			ComputeTask(ubo_size, 1, number_of_ubos, number_of_samplers, compute_pipeline_info, compute_task_type, 1, 1, 1, cache_path),
			m_output_texture(create_output_texture(image_size, format, compute_task_type)) {}

		OutputInfo create_output_data(const glm::ivec2& image_size, const std::string &output_texture_name,
                                      bool mipmapping_enabled, std::optional<uint32_t> max_num_of_mipmaps = {});

		glm::mat4 get_current_view_transformation(int index);

		Texture m_output_texture;
		std::optional<size_t> m_image_size_bytes;
	};
}

#endif