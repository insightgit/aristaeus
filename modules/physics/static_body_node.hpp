#ifndef ARISTAEUS_STATIC_BODY_NODE_HPP
#define ARISTAEUS_STATIC_BODY_NODE_HPP

#include "physics_actor_node.hpp"
#include "physics_manager.hpp"

namespace aristaeus::physics {
	class StaticBodyNode : public PhysicsActorNode {
		ARISTAEUS_CLASS_HEADER("static_body", StaticBodyNode, true);
	private:
		void draw() {};

		void predraw_load(const vk::CommandBuffer& command_buffer, const glm::vec3& camera_position) {};

		void on_parsing_done();

		physx::PxRigidStatic *m_rigid_static;
	};
}

#endif