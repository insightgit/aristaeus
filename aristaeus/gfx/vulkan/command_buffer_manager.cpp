#include "command_buffer_manager.hpp"
#include "core_renderer.hpp"


aristaeus::gfx::vulkan::CommandBufferManager::CommandBufferManager(uint32_t queue_family_index, bool is_compute) : 
                                                m_queue_family_index(queue_family_index),
                                                m_is_compute(is_compute),
                                                m_command_pools(create_main_thread_command_pool()),
                                                m_primary_command_buffers(create_command_buffers(m_command_pools[std::this_thread::get_id()], vk::CommandBufferLevel::ePrimary)) {}


void aristaeus::gfx::vulkan::CommandBufferState::begin_if_not_ready(RenderContextID render_context_id, uint32_t subpass_num, bool is_compute) {
    if (!is_ready && !is_ended) {
        vk::CommandBufferInheritanceInfo secondary_buffer_inheritance_info{ {}, subpass_num, {} };
        vk::CommandBufferUsageFlags usage_flags{};

        CoreRenderer &core_renderer = utils::get_singleton_safe<CoreRenderer>();
        RenderPhase *render_phase = core_renderer.get_current_render_phase();

        if (!is_compute && render_phase != nullptr && render_phase->in_vulkan_render_pass_scope()) {
            if (render_phase->is_render_pass_depth_only(render_phase->get_current_render_pass())) {
                current_frame = core_renderer.get_current_frame();
            } else {
                current_frame = core_renderer.get_current_swap_chain_image();
            }

            secondary_buffer_inheritance_info = render_phase->get_current_cmd_inheritance_info().value();
            secondary_buffer_inheritance_info.subpass = subpass_num;
            usage_flags = { vk::CommandBufferUsageFlagBits::eRenderPassContinue };
        }

        vk::CommandBufferBeginInfo secondary_command_buffer_begin_info{ usage_flags,
                                                                        &secondary_buffer_inheritance_info };

        command_buffer.begin(secondary_command_buffer_begin_info);

        current_render_context_id = render_context_id;
        is_ready = true;
    } else if (is_ended && is_ready) {
        assert(false);
    }
}

void aristaeus::gfx::vulkan::CommandBufferManager::create_thread_command_pool_if_needed(RenderContextID render_context_id, uint32_t subpass_num) {
    CoreRenderer &core_renderer = utils::get_singleton_safe<CoreRenderer>();
    RenderPassKey render_pass_key { std::this_thread::get_id(), render_context_id, subpass_num };

    if (!m_command_pools.contains(std::this_thread::get_id())) {
        vk::CommandPoolCreateInfo command_pool_info{
                vk::CommandPoolCreateFlagBits::eResetCommandBuffer,
                m_queue_family_index
        };
        vk::raii::CommandPool command_pool{ core_renderer.get_logical_device_ref(), command_pool_info};

        std::lock_guard<std::mutex> command_pool_lock_guard{ m_command_pools_mutex };

        m_command_pools_raii_owner.push_back(std::move(command_pool));
        m_command_pools.insert({ std::this_thread::get_id(), *m_command_pools_raii_owner.back() });

        std::vector<vk::raii::CommandBuffer> command_buffers = create_command_buffers(m_command_pools_raii_owner.back(), vk::CommandBufferLevel::eSecondary);

        for (vk::raii::CommandBuffer& command_buffer : command_buffers) {
            m_command_buffers.push_back(std::move(command_buffer));
        }

        std::array<CommandBufferState, utils::vulkan::MAX_FRAMES_IN_FLIGHT> command_buffers_array{ {
            {*m_command_buffers[m_command_buffers.size() - 2]},
            {*m_command_buffers[m_command_buffers.size() - 1]}
        } };

        m_persistent_secondary_command_buffers.insert({ render_pass_key,
                                                       command_buffers_array });

        m_persistent_secondary_thread_ids[render_context_id].insert(std::this_thread::get_id());
    }
    else if (!m_persistent_secondary_command_buffers.contains(render_pass_key)) {
        std::lock_guard<std::mutex> command_pool_lock_guard{ m_command_pools_mutex };

        std::vector<vk::raii::CommandBuffer> command_buffers = create_command_buffers(m_command_pools[std::this_thread::get_id()], vk::CommandBufferLevel::eSecondary);

        // TODO(Bobby): something here is causing a double free
        for (vk::raii::CommandBuffer& command_buffer : command_buffers) {
            m_command_buffers.push_back(std::move(command_buffer));
        }

        std::array<CommandBufferState, utils::vulkan::MAX_FRAMES_IN_FLIGHT> command_buffers_array{ {
            {*m_command_buffers[m_command_buffers.size() - 2]},
            {*m_command_buffers[m_command_buffers.size() - 1]}
        } };

        m_persistent_secondary_command_buffers.insert({ render_pass_key, command_buffers_array });
        m_persistent_secondary_thread_ids[render_context_id].insert(std::this_thread::get_id());
    }
}


aristaeus::gfx::vulkan::CommandBufferState&
aristaeus::gfx::vulkan::CommandBufferManager::get_command_buffer_state_for_thread(RenderContextID render_context_id, 
    const std::optional<std::reference_wrapper<GraphicsPipeline>> &pipeline,
    const std::optional<DescriptorSetBindInfo> &descriptor_set_bind_info, std::optional<uint32_t> subpass_num) {
    // TODO(Bobby): main thread detection
    CoreRenderer &core_renderer = utils::get_singleton_safe<CoreRenderer>();
    uint32_t current_frame = core_renderer.get_current_frame();
    std::thread::id thread_id = std::this_thread::get_id();
    bool new_command_buffer = !m_command_pools.contains(thread_id) ||
        !m_persistent_secondary_command_buffers.contains({ thread_id, render_context_id });

    if (!subpass_num.has_value()) {
        if (pipeline.has_value()) {
            subpass_num = pipeline->get().get_pipeline_info().sub_pass;
        }
        else {
            subpass_num = 0;
        }
    }

    create_thread_command_pool_if_needed(render_context_id, *subpass_num);

    RenderPassKey render_pass_key{ std::this_thread::get_id(), render_context_id, *subpass_num };

    CommandBufferState &command_buffer_state = m_persistent_secondary_command_buffers[render_pass_key][current_frame];

    command_buffer_state.begin_if_not_ready(render_context_id, *subpass_num, m_is_compute);

    if (new_command_buffer) {
        vk::CommandBuffer command_buffer_to_use = command_buffer_state.command_buffer;

        if (pipeline.has_value()) {
            pipeline->get().bind_to_command_buffer(command_buffer_to_use, core_renderer);
        }
    }
    RenderPhase *current_render_phase = core_renderer.get_current_render_phase();

    // current render phase should only be null for compute
    if (current_render_phase != nullptr && current_render_phase->in_vulkan_render_pass_scope()) {
        std::string current_render_pass = current_render_phase->get_current_render_pass();

        vk::Viewport current_viewport = current_render_phase->get_current_viewport();
        vk::Rect2D current_viewport_scissor = current_render_phase->get_current_viewport_scissor();
        std::array<float, 4> default_blend_constants{ {0.0f, 0.0f, 0.0f, 0.0f} };

        std::vector<vk::DynamicState> dynamic_states;

        if (pipeline.has_value()) {
            dynamic_states = pipeline->get().get_dynamic_states();
            pipeline->get().bind_to_command_buffer(command_buffer_state.command_buffer, core_renderer);
        }

        if (current_render_phase->in_vulkan_render_pass_scope()) {
            command_buffer_state.command_buffer.setViewport(0, current_viewport);
            command_buffer_state.command_buffer.setScissor(0, current_viewport_scissor);

            if (current_render_phase->is_render_pass_depth_only(current_render_phase->get_current_render_pass())) {
                command_buffer_state.command_buffer.setDepthBias(1.25f, 0.0f, 1.75f);
            }
        }

        if (std::find(dynamic_states.begin(), dynamic_states.end(), vk::DynamicState::eBlendConstants) !=
            dynamic_states.end()) {
            command_buffer_state.command_buffer.setBlendConstants(default_blend_constants.data());
        }
    }

    if (descriptor_set_bind_info.has_value()) {
        assert(pipeline.has_value());

        const DescriptorSetBindInfo& bind_info = descriptor_set_bind_info.value();

        // TODO(Bobby): for some reason two different threads are binding the same descriptor set here?
        command_buffer_state.command_buffer.bindDescriptorSets(vk::PipelineBindPoint::eGraphics,
            *(pipeline->get().get_pipeline_layout()),
            bind_info.first_descriptor_set,
            bind_info.descriptor_sets, bind_info.dynamic_offsets);
    }

    return command_buffer_state;
}

size_t aristaeus::gfx::vulkan::CommandBufferManager::record_all_secondary_command_buffers(
    RenderContextID render_context_id, uint32_t subpass_num,
    std::optional<vk::CommandBuffer> capture_command_buffer) {
    CoreRenderer &core_renderer = utils::get_singleton_safe<CoreRenderer>();
    std::vector<vk::CommandBuffer> command_buffers;
    std::vector<CommandBufferState> command_buffer_states;
    uint32_t current_frame = core_renderer.get_current_frame();

    std::optional<vk::CommandBuffer> blended_objects_command_buffer;

    RenderPhase *current_render_phase = core_renderer.get_current_render_phase();
    std::string render_phase_name;
    std::string render_pass_name;
    bool has_render_phase = !m_is_compute && render_context_id != RENDER_CONTEXT_ID_PRELOAD;

    if (has_render_phase) {
        assert(current_render_phase != nullptr);
        render_phase_name = current_render_phase->get_render_phase_name();
        render_pass_name = current_render_phase->get_current_render_pass();
    }

    for (std::pair<const RenderPassKey,
        std::array<CommandBufferState, utils::vulkan::MAX_FRAMES_IN_FLIGHT>> &command_buffer :
        m_persistent_secondary_command_buffers) {
        bool check_sky_box = command_buffer.first.render_context_id == render_context_id && 
            command_buffer.first.subpass_num == subpass_num && command_buffer.second[current_frame].is_sky_box;

        if ((m_is_compute || check_sky_box) && command_buffer.second[current_frame].is_ready) {
            assert(!command_buffer.second[current_frame].has_blended_objects);
            command_buffer.second[current_frame].is_ended = true;
            command_buffer.second[current_frame].is_ready = false;
            command_buffer.second[current_frame].is_sky_box = false;

            command_buffers.push_back(command_buffer.second[current_frame].command_buffer);

            break;
        }
    }

    for (std::pair<const RenderPassKey,
        std::array<CommandBufferState, utils::vulkan::MAX_FRAMES_IN_FLIGHT>> &command_buffer :
        m_persistent_secondary_command_buffers) {
        if ((m_is_compute || (command_buffer.first.render_context_id == render_context_id &&
            command_buffer.first.subpass_num == subpass_num)) &&
            command_buffer.second[current_frame].is_ready) {
            command_buffer.second[current_frame].is_ended = true;
            command_buffer.second[current_frame].is_ready = false;

            if (!m_is_compute && command_buffer.second[current_frame].has_blended_objects) {
                assert(!blended_objects_command_buffer.has_value());

                blended_objects_command_buffer = { command_buffer.second[current_frame].command_buffer };
                command_buffer.second[current_frame].has_blended_objects = false;
            } else {
                // sky box command buffers should've already been taken care of
                assert(m_is_compute || !command_buffer.second[current_frame].is_sky_box);

                command_buffers.push_back(command_buffer.second[current_frame].command_buffer);
                command_buffer_states.push_back(command_buffer.second[current_frame]);
            }
        }
    }

    if (blended_objects_command_buffer.has_value()) {
        assert(!m_is_compute);
        command_buffers.push_back(*blended_objects_command_buffer);
    }

    if (has_render_phase && core_renderer.in_default_rasterization_render_pass() && subpass_num == core_renderer.get_default_rasterization_subpass()) {
        if (!command_buffers.empty()) {
            ImGui::Begin("Render Phases");
            core_renderer.get_render_phase_dep_node_root().imgui_draw(command_buffers.back());
            ImGui::End();
        }

        ImGui::Render();

        if(!command_buffers.empty()) {
            if (ImGui::GetDrawData() != nullptr) {
                ImGui_ImplVulkan_RenderDrawData(ImGui::GetDrawData(), command_buffers.back());
            }
        }
    }

    for (int i = 0; command_buffers.size() > i; ++i) {
        command_buffers[i].end();
    }

    if (!command_buffers.empty()) {
        if (!has_render_phase) {
            m_primary_command_buffers[current_frame].executeCommands(command_buffers);
        } else {
            RenderPhaseID render_phase_id = current_render_phase->get_render_phase_id().value();

            m_primary_command_buffers[utils::vulkan::MAX_FRAMES_IN_FLIGHT + (render_phase_id * utils::vulkan::MAX_FRAMES_IN_FLIGHT) + current_frame].executeCommands(command_buffers);
        }
    }

    return command_buffers.size();
}

std::unordered_map<std::thread::id, vk::CommandPool> aristaeus::gfx::vulkan::CommandBufferManager::create_main_thread_command_pool() {
    vk::CommandPoolCreateInfo command_pool_info{
        vk::CommandPoolCreateFlagBits::eResetCommandBuffer,
        m_queue_family_index
    };
    vk::raii::CommandPool command_pool{ utils::get_singleton_safe<CoreRenderer>().get_logical_device_ref(), command_pool_info};

    std::unordered_map<std::thread::id, vk::CommandPool> return_value;

    m_command_pools_mutex.lock();

    m_command_pools_raii_owner.push_back(std::move(command_pool));
    return_value.insert({ std::this_thread::get_id(), *m_command_pools_raii_owner.back() });

    m_command_pools_mutex.unlock();

    std::cout << "Created main thread command pool successfully!\n";

    return return_value;
}

std::vector<vk::raii::CommandBuffer> aristaeus::gfx::vulkan::CommandBufferManager::create_command_buffers(const vk::CommandPool& command_pool,
    vk::CommandBufferLevel command_buffer_level, uint32_t count) {
    CoreRenderer &core_renderer = utils::get_singleton_safe<CoreRenderer>();

    vk::CommandBufferAllocateInfo command_buffer_info{ command_pool, command_buffer_level, count };
    std::vector<vk::raii::CommandBuffer> command_buffers = core_renderer.get_logical_device_ref().allocateCommandBuffers(
        command_buffer_info);

    std::cout << "Created command buffers!\n";

    return command_buffers;
}

void aristaeus::gfx::vulkan::CommandBufferManager::bind_descriptor_sets_on_all_command_buffers(const vk::PipelineLayout &pipeline_layout, RenderContextID render_context_id, uint32_t subpass_num,
                                                                                               uint32_t first_descriptor_set, const std::vector<vk::DescriptorSet> &descriptor_sets,
                                                                                               const std::vector<uint32_t> &dynamic_offsets, bool include_primary) {
    CoreRenderer &core_renderer = utils::get_singleton_safe<CoreRenderer>();
    size_t current_frame = core_renderer.get_current_frame();

    if (include_primary) {
        m_command_buffers[current_frame].bindDescriptorSets(vk::PipelineBindPoint::eGraphics,
            pipeline_layout, first_descriptor_set,
            descriptor_sets, dynamic_offsets);
    }

    for (std::pair<const RenderPassKey,
        std::array<CommandBufferState, 2>> &command_buffer :
        m_persistent_secondary_command_buffers) {
        if (command_buffer.first.render_context_id == render_context_id && command_buffer.first.subpass_num == subpass_num) {
            command_buffer.second[current_frame].begin_if_not_ready(command_buffer.first.render_context_id, command_buffer.first.subpass_num, m_is_compute);

            command_buffer.second[current_frame].command_buffer.
                bindDescriptorSets(vk::PipelineBindPoint::eGraphics, pipeline_layout, first_descriptor_set,
                    descriptor_sets, dynamic_offsets);
        }
    }
}

void aristaeus::gfx::vulkan::CommandBufferManager::bind_pipeline_on_all_command_buffers(GraphicsPipeline &pipeline, RenderContextID render_context_id, bool include_primary) {
    CoreRenderer &core_renderer = utils::get_singleton_safe<CoreRenderer>();
    size_t current_frame = core_renderer.get_current_frame();

    if (include_primary) {
        pipeline.bind_to_command_buffer(m_command_buffers[current_frame], core_renderer);
    }

    for (std::pair<const RenderPassKey,
        std::array<CommandBufferState, 2>> &command_buffer :
        m_persistent_secondary_command_buffers) {
        if (command_buffer.first.render_context_id == render_context_id && command_buffer.first.subpass_num == pipeline.get_pipeline_info().sub_pass) {
            command_buffer.second[current_frame].begin_if_not_ready(command_buffer.first.render_context_id, command_buffer.first.subpass_num, m_is_compute);

            pipeline.bind_to_command_buffer(command_buffer.second[current_frame].command_buffer, core_renderer);
        }
    }
}

void aristaeus::gfx::vulkan::CommandBufferManager::begin_command_buffer(uint32_t current_frame, RenderPhaseID render_phase_id) {
    vk::CommandBuffer command_buffer = get_primary_command_buffer(current_frame, render_phase_id);

    command_buffer.reset({});
    command_buffer.begin(vk::CommandBufferBeginInfo{});
}

vk::CommandBuffer aristaeus::gfx::vulkan::CommandBufferManager::get_primary_command_buffer(uint32_t current_frame, RenderPhaseID render_phase_id) {
    if (m_is_compute || render_phase_id == RENDER_CONTEXT_ID_PRELOAD) {
        return *m_primary_command_buffers[current_frame];
    }
    else {
        return *m_primary_command_buffers[utils::vulkan::MAX_FRAMES_IN_FLIGHT + current_frame + (render_phase_id * utils::vulkan::MAX_FRAMES_IN_FLIGHT)];
    }
}

vk::CommandBuffer aristaeus::gfx::vulkan::CommandBufferManager::get_primary_command_buffer(uint32_t current_frame) {
    if (m_is_compute) {
        return get_primary_command_buffer(current_frame, 0);
    }
    else {
        return get_primary_command_buffer(current_frame, utils::get_singleton_safe<CoreRenderer>().get_current_render_phase()->get_render_phase_id().value());
    }
}

void aristaeus::gfx::vulkan::CommandBufferManager::begin_secondary_command_buffers(uint32_t current_frame, RenderContextID render_context_id, uint32_t subpass_num) {
    if (!m_persistent_secondary_thread_ids.contains(render_context_id)) {
        return;
    }

    for (std::thread::id thread_id : m_persistent_secondary_thread_ids.at(render_context_id)) {
        RenderPassKey render_pass_key{ thread_id, render_context_id, subpass_num };
        
        if (!m_persistent_secondary_command_buffers.contains(render_pass_key)) {
            continue;
        }

        std::array<CommandBufferState, utils::vulkan::MAX_FRAMES_IN_FLIGHT> &command_buffers = m_persistent_secondary_command_buffers.at(render_pass_key);
        CommandBufferState &command_buffer_state = command_buffers[current_frame];

        if (!command_buffer_state.is_ready) {
            command_buffer_state.is_ended = false;

            command_buffer_state.command_buffer.reset({});

            command_buffer_state.begin_if_not_ready(render_context_id, subpass_num, m_is_compute);
        }
        else {
            assert(!command_buffer_state.is_ended);
        }
    }
}

void aristaeus::gfx::vulkan::CommandBufferManager::add_render_phase() {
    std::vector<vk::raii::CommandBuffer> new_command_buffers = create_command_buffers(m_command_pools.at(std::this_thread::get_id()), vk::CommandBufferLevel::ePrimary);

    for (vk::raii::CommandBuffer &command_buffer : new_command_buffers) {
        m_primary_command_buffers.emplace_back(std::move(command_buffer));
    }
}