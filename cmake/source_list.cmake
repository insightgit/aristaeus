set(SOURCE_FILES libraries/src/stb_image.cpp
        libraries/src/stb_image_write.cpp
        libraries/src/imgui.cpp
        libraries/src/imgui_draw.cpp
        libraries/src/imgui_demo.cpp
        libraries/src/imgui_impl_glfw.cpp
        libraries/src/imgui_impl_vulkan.cpp
        libraries/src/imgui_tables.cpp
        libraries/src/imgui_widgets.cpp
        libraries/src/imgui_stdlib.cpp
        libraries/src/sha256.cpp
        libraries/src/MurmurHash2.cpp
        libraries/src/ImGuizmo.cpp
        aristaeus/gfx/vulkan/graphics_pipeline.cpp
        aristaeus/gfx/vulkan/graphics_pipeline.hpp
        aristaeus/gfx/vulkan/core_renderer.cpp
        aristaeus/gfx/vulkan/core_renderer.hpp
        aristaeus/gfx/vulkan/window.cpp
        aristaeus/gfx/vulkan/window.hpp
        aristaeus/core/scene.cpp
        aristaeus/core/scene.hpp
        aristaeus/gfx/vulkan/vulkan_raii.cpp
        aristaeus/gfx/vulkan/vulkan_raii.hpp
        aristaeus/gfx/vulkan/glfw_create_window_surface.hpp
        aristaeus/gfx/vulkan/glfw_create_window_surface.cpp
        aristaeus/gfx/vulkan/vma.cpp
        aristaeus/gfx/vulkan/assimp_mesh.hpp
        aristaeus/gfx/vulkan/assimp_mesh.cpp
        aristaeus/gfx/vulkan/assimp_model.hpp
        aristaeus/gfx/vulkan/assimp_model.cpp
        aristaeus/gfx/vulkan/texture.cpp
        aristaeus/gfx/vulkan/texture.hpp
        aristaeus/gfx/vulkan/uniform_buffer.cpp
        aristaeus/gfx/vulkan/uniform_buffer.hpp
        aristaeus/gfx/vulkan/perspective_camera_environment.cpp
        aristaeus/gfx/vulkan/perspective_camera_environment.hpp
        aristaeus/gfx/vulkan/point_light.cpp
        aristaeus/gfx/vulkan/point_light.hpp
        aristaeus/gfx/vulkan/directional_light.cpp
        aristaeus/gfx/vulkan/directional_light.hpp
        aristaeus/gfx/vulkan/sky_box.cpp
        aristaeus/gfx/vulkan/sky_box.hpp
        aristaeus/core/scene_loader.cpp
        aristaeus/core/scene_loader.hpp
        aristaeus/gfx/gui/scene_tree_gui.cpp
        aristaeus/gfx/vulkan/captured_sky_box.cpp
        aristaeus/gfx/vulkan/captured_sky_box.hpp
        aristaeus/gfx/vulkan/cube_map_texture.cpp
        aristaeus/gfx/vulkan/cube_map_texture.hpp
        aristaeus/gfx/vulkan/task_manager.cpp
        aristaeus/gfx/vulkan/task_manager.hpp
        aristaeus/gfx/vulkan/memory_manager.cpp
        aristaeus/gfx/vulkan/memory_manager.hpp
        aristaeus/gfx/vulkan/render_phase_dependency_node.cpp
        aristaeus/gfx/vulkan/render_phase_dependency_node.hpp
        aristaeus/core/lru_cache.hpp
        aristaeus/gfx/vulkan/perlin_noise.cpp
        aristaeus/gfx/vulkan/perlin_noise.hpp
        aristaeus/gfx/vulkan/pipeline_cache.hpp
        aristaeus/gfx/vulkan/pipeline_cache.cpp
        aristaeus/gfx/vulkan/command_buffer_manager.hpp
        aristaeus/gfx/vulkan/command_buffer_manager.cpp
        aristaeus/gfx/vulkan/geometry/geometry.hpp
        aristaeus/gfx/vulkan/geometry/sphere.hpp
        aristaeus/gfx/vulkan/geometry/sphere.cpp
        aristaeus/gfx/vulkan/geometry/cylinder.hpp
        aristaeus/gfx/vulkan/geometry/cylinder.cpp
        aristaeus/gfx/vulkan/geometry/torus.hpp
        aristaeus/gfx/vulkan/geometry/torus.cpp
        aristaeus/gfx/vulkan/geometry/rectangular_prism.hpp
        aristaeus/gfx/vulkan/geometry/rectangular_prism.cpp
        aristaeus/gfx/vulkan/vulkan_pipeline.hpp
        aristaeus/gfx/vulkan/vulkan_pipeline.cpp
        aristaeus/gfx/vulkan/image_buffer_raii.cpp
        aristaeus/gfx/vulkan/model_info.hpp
        aristaeus/gfx/vulkan/model_info.cpp
        aristaeus/gfx/vulkan/lighting_node.hpp
        aristaeus/gfx/vulkan/lighting_node.cpp
        aristaeus/gfx/vulkan/render_phase.hpp
        aristaeus/gfx/vulkan/render_phase.cpp
        aristaeus/engine_module_entry_point.hpp
        aristaeus/gfx/scene_graph_node.hpp
        aristaeus/gfx/scene_graph_node.cpp
        aristaeus/core/class_db.hpp
        aristaeus/core/class_db.cpp
        aristaeus/core/event_object.hpp
        aristaeus/core/event_object.cpp
        aristaeus/core/singleton.hpp
        aristaeus/core/singleton.cpp
        aristaeus/core/module_loader.hpp
        aristaeus/core/module_loader.cpp
        aristaeus/core/platform.hpp
        aristaeus/gfx/vulkan/lighting_environment.cpp
        aristaeus/gfx/vulkan/lighting_environment.hpp
        aristaeus/gfx/vulkan/rasterization_lighting_environment.cpp
        aristaeus/gfx/vulkan/rasterization_lighting_environment.hpp
        aristaeus/gfx/vulkan/rasterization_gi/reflective_shadow_map.cpp
        aristaeus/gfx/vulkan/rasterization_gi/reflective_shadow_map.hpp
        aristaeus/gfx/vulkan/mesh_manager.hpp
        aristaeus/gfx/vulkan/mesh_manager.cpp
        aristaeus/gfx/vulkan/material_manager.hpp
        aristaeus/gfx/vulkan/material_manager.cpp
        main.cpp)

set(GLSL_SHADERS environments/vulkan_environment_shaders/engine/environment_shader.frag
        environments/vulkan_environment_shaders/engine/environment_shader_gbuffer.frag
        environments/vulkan_environment_shaders/engine/screen_space_interpolation.frag
        environments/vulkan_environment_shaders/engine/quad_render.vert
        environments/vulkan_environment_shaders/engine/environment_shader.vert
        environments/vulkan_environment_shaders/engine/environment_shader_instanced.vert
        environments/vulkan_environment_shaders/engine/sky_box.frag
        environments/vulkan_environment_shaders/engine/sky_box.vert
        environments/vulkan_environment_shaders/engine/depth_map.frag
        environments/vulkan_environment_shaders/engine/depth_map.vert
        environments/vulkan_environment_shaders/terrain/terrain_water.vert
        environments/vulkan_environment_shaders/terrain/terrain_water.tese
        environments/vulkan_environment_shaders/terrain/terrain.tesc
        environments/vulkan_environment_shaders/terrain/terrain.tese
        environments/vulkan_environment_shaders/terrain/terrain.vert
        environments/vulkan_environment_shaders/terrain/debug/terrain_normals.tese
        environments/vulkan_environment_shaders/engine/debug/color_normals.geom
        environments/vulkan_environment_shaders/engine/debug/color_normals.frag
        environments/vulkan_environment_shaders/engine/debug/color_normals.vert
        environments/vulkan_environment_shaders/compute/perlin_noise.comp
        environments/vulkan_environment_shaders/compute/equirectangular_convert.comp
        environments/vulkan_environment_shaders/compute/diffuse_convolution.comp
        environments/vulkan_environment_shaders/compute/brdf_convolution.comp
        environments/vulkan_environment_shaders/physics/debug.vert
        environments/vulkan_environment_shaders/physics/debug.frag
        environments/vulkan_environment_shaders/gi/reflective_shadow_map.frag
        environments/vulkan_environment_shaders/gi/ssao.frag
        environments/vulkan_environment_shaders/gi/ssao_blur.frag)

set(GLSL_SHADERS_WITH_OPTIONS 
    environments/vulkan_environment_shaders/engine/environment_shader.frag DEFERRED_MODE "${CMAKE_CURRENT_BINARY_DIR}/environments/vulkan_environment_shaders/engine/environment_shader.deferred.frag.spv")

set(COMPUTE_MODULE_SOURCE_FILES
    modules/compute/gpu_perlin_noise.hpp
    modules/compute/gpu_perlin_noise.cpp
    modules/compute/equirectangular_converter.cpp
    modules/compute/equirectangular_converter.hpp
    modules/compute/diffuse_convolution.cpp
    modules/compute/diffuse_convolution.hpp
    modules/compute/brdf_convolution.cpp
    modules/compute/brdf_convolution.hpp
    modules/compute/cube_map_compute_task.cpp
    modules/compute/cube_map_compute_task.hpp
    modules/compute/ibl_sky_box.cpp
    modules/compute/ibl_sky_box.hpp
    modules/compute/module_entry_point.cpp
    modules/compute/compute_pipeline.hpp
    modules/compute/compute_pipeline.cpp
    modules/compute/compute_manager.hpp
    modules/compute/compute_manager.cpp
    modules/compute/compute_task.cpp
    modules/compute/compute_task.hpp)

set(PHYSICS_MODULE_SOURCE_FILES
    modules/physics/aabb.cpp
    modules/physics/aabb.hpp
    modules/physics/physics_manager.hpp
    modules/physics/physics_manager.cpp
    modules/physics/rigid_body_node.hpp
    modules/physics/rigid_body_node.cpp
    modules/physics/physics_actor_node.hpp
    modules/physics/physics_actor_node.cpp
    modules/physics/module_entry_point.hpp
    modules/physics/module_entry_point.cpp
    modules/physics/static_body_node.hpp
    modules/physics/static_body_node.cpp
)

set(TERRAIN_MODULE_SOURCE_FILES
    modules/terrain/procedural_terrain.hpp
    modules/terrain/procedural_terrain.cpp
    modules/terrain/terrain_image.hpp
    modules/terrain/chunked_terrain.cpp
    modules/terrain/chunked_terrain.hpp
    modules/terrain/terrain.cpp
    modules/terrain/terrain.hpp
    modules/terrain/chunk_prop_loader.cpp
    modules/terrain/chunk_prop_loader.hpp
    modules/terrain/module_entry_point.cpp)

set(COMPUTE_TERRAIN_MODULE_SOURCE_FILES 
    modules/compute/gpu_perlin_noise_procedural_terrain.hpp
    modules/compute/gpu_perlin_noise_procedural_terrain.cpp
    modules/compute/gpu_perlin_noise_terrain.hpp
    modules/compute/gpu_perlin_noise_terrain.cpp)