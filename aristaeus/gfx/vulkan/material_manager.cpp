#include "material_manager.hpp"

#include "assimp_model.hpp"

template<>
aristaeus::gfx::vulkan::MaterialManager *aristaeus::core::Singleton<aristaeus::gfx::vulkan::MaterialManager>::instance = nullptr;

aristaeus::gfx::vulkan::MaterialManager::MaterialManager() : m_current_material_id(0) {}


aristaeus::gfx::vulkan::MaterialID aristaeus::gfx::vulkan::MaterialManager::create_material(Material material) {
	size_t material_data_index;
	MaterialID new_material_id = m_current_material_id++;
	auto &core_renderer = utils::get_singleton_safe<CoreRenderer>();
	size_t total_frames = core_renderer.get_total_frames_rendered();

	if (m_initial_materials.contains(material) && m_material_datas[m_material_id_to_data.at(m_initial_materials.at(material))].resources == nullptr) {
		MaterialID base_material_id = m_initial_materials.at(material);

		material_data_index = m_material_id_to_data.at(base_material_id);
		SuperMaterialData &material_data = m_material_datas[material_data_index];

		material_data.data.push_back(material.data);
		material_data.textures.push_back(material.textures);

		assert(material_data.data.size() == material_data.textures.size());

		material_data.materials.insert(std::pair<MaterialID, LocalMaterialID>{new_material_id, material_data.data.size() - 1});
		material_data.added_materials.push_back(new_material_id);
	}
	else {
		bool initial_materials = m_initial_materials.contains(material);

		m_initial_materials.insert({ material, new_material_id });

		m_material_datas.push_back(SuperMaterialData{
			.initial_material = material,
			.data = {material.data},
			.textures = {material.textures},
			.materials = {{new_material_id, 0}},
			.added_materials = {new_material_id}
		});

		material_data_index = m_material_datas.size() - 1;
	}

	m_material_id_to_data.insert({ new_material_id, material_data_index });
	m_queued_material_ids.insert(new_material_id);

	return new_material_id;
}

void aristaeus::gfx::vulkan::MaterialManager::initialize_super_material(MaterialID queued_material_id) {
	auto &core_renderer = utils::get_singleton_safe<CoreRenderer>();

	SuperMaterialData &super_material_data = m_material_datas.at(m_material_id_to_data.at(queued_material_id));

	if (super_material_data.resources != nullptr) {
		// super material was already inited
		m_queued_material_ids.erase(queued_material_id);
		return;
	}

	assert(super_material_data.textures.size() == super_material_data.data.size() &&
		super_material_data.textures.size() == super_material_data.materials.size());


	struct BufferRequirement {
		size_t buffer_size = 0;
		vk::ShaderStageFlags used_by_shader_stages{};
		int num_of_buffers = 0;
		std::vector<std::reference_wrapper<size_t>> buffer_offsets_to_write;
	};

	struct ImageRequirement {
		vk::ShaderStageFlags used_by_shader_stages{};
		int num_of_images = 0;
	};

	// the idea is that if we have some sort of common buffer
	// with the same size(eg a material uniform buffer) 
	// we can bind it such that we have it stored as a big array in one buffer and binding
	// sorted by binding point
	std::vector<BufferRequirement> buffer_reqs;
	std::vector<ImageRequirement> image_reqs;
	std::unordered_map<vk::DescriptorType, size_t> descriptor_counts;

	// (step 1): going through the per material shader datas and textures to figure out details
	// for the descriptor set layout
	for (std::vector<MaterialShaderData> &material_shader_datas : super_material_data.data) {
		for (MaterialShaderData &material_shader_data : material_shader_datas) {
			if (buffer_reqs.size() <= material_shader_data.bind_location) {
				buffer_reqs.resize(material_shader_data.bind_location + 1);

				buffer_reqs[material_shader_data.bind_location] = BufferRequirement{
					.buffer_size = material_shader_data.data_size,
					.used_by_shader_stages = material_shader_data.used_by_shader_stages,
					.num_of_buffers = 1
				};
			}
			else if (buffer_reqs[material_shader_data.bind_location].buffer_size == material_shader_data.data_size) {
				buffer_reqs[material_shader_data.bind_location].num_of_buffers++;
				buffer_reqs[material_shader_data.bind_location].used_by_shader_stages |= material_shader_data.used_by_shader_stages;

				buffer_reqs[material_shader_data.bind_location].buffer_offsets_to_write.push_back(material_shader_data.data_size);
			}
			else {
				throw std::runtime_error("Invalid binding point for buffer which is too small!");
			}

			++descriptor_counts[vk::DescriptorType::eStorageBuffer];
		}
	}

	for (const std::vector<MaterialTexture> &textures : super_material_data.textures) {
		for (const MaterialTexture &texture : textures) {
			if (image_reqs.size() <= texture.bind_location) {
				size_t old_size = image_reqs.size();
				size_t new_size = texture.bind_location + 1;

				image_reqs.resize(new_size);
			}

			image_reqs[texture.bind_location].num_of_images++;
			image_reqs[texture.bind_location].used_by_shader_stages |= texture.used_by_shader_stages;

			++descriptor_counts[vk::DescriptorType::eCombinedImageSampler];
		}
	}

	// (step 2): create descriptor set layout and descriptor sets
	std::vector<vk::DescriptorSetLayoutBinding> material_layout_bindings;

	for (int i = 0; buffer_reqs.size() > i; ++i) {
		if (buffer_reqs[i].num_of_buffers > 0) {
			material_layout_bindings.emplace_back(i, vk::DescriptorType::eStorageBuffer, 1,
												  buffer_reqs[i].used_by_shader_stages);
		}
	}

	for (int i = 0; image_reqs.size() > i; ++i) {
		if (image_reqs[i].num_of_images > 0) {
			material_layout_bindings.emplace_back(i, vk::DescriptorType::eCombinedImageSampler,
												  static_cast<uint32_t>(image_reqs[i].num_of_images),
												  image_reqs[i].used_by_shader_stages);
		}
	}

	std::vector<vk::DescriptorPoolSize> pool_sizes;

	for (std::pair<vk::DescriptorType, size_t> descriptor_pair : descriptor_counts) {
		pool_sizes.push_back({ descriptor_pair.first, static_cast<uint32_t>(descriptor_pair.second * utils::vulkan::MAX_FRAMES_IN_FLIGHT) });
	}

	vk::raii::DescriptorSetLayout descriptor_set_layout{ core_renderer.get_logical_device_ref(), {{}, material_layout_bindings} };
	vk::raii::DescriptorPool descriptor_pool{ core_renderer.get_logical_device_ref(), {{}, utils::vulkan::MAX_FRAMES_IN_FLIGHT, pool_sizes} };

	std::array<vk::DescriptorSetLayout, utils::vulkan::MAX_FRAMES_IN_FLIGHT> descriptor_set_layouts{ *descriptor_set_layout, *descriptor_set_layout };

	vk::DescriptorSetAllocateInfo set_allocate_info{ *descriptor_pool, descriptor_set_layouts };
	std::vector<vk::raii::DescriptorSet> new_sets_vector = core_renderer.get_logical_device_ref().allocateDescriptorSets(set_allocate_info);
	std::array<vk::raii::DescriptorSet, utils::vulkan::MAX_FRAMES_IN_FLIGHT> descriptor_sets_array{ std::move(new_sets_vector[0]),
																								   std::move(new_sets_vector[1]) };

	// (step 3): create graphics pipeline
	GraphicsPipeline::PipelineInfo pipeline_info = super_material_data.initial_material.pipeline_info;
	std::vector<GraphicsPipeline::ShaderInfo> shader_infos = super_material_data.initial_material.shader_infos;

	pipeline_info.descriptor_bindings.push_back(material_layout_bindings); // "material_ubo"
	pipeline_info.descriptor_bindings.push_back(std::vector<vk::DescriptorSetLayoutBinding>{ MeshManager::INSTANCE_STORAGE_BUFFER_BINDING }); // "instance_ubo"

	GraphicsPipeline graphics_pipeline{ core_renderer, shader_infos, pipeline_info, false };

	// (step 4): create storage buffer requested for material
	size_t buffer_per_material_size = 0;
	size_t total_buffer_size = 0;

	for (const BufferRequirement &buffer_req : buffer_reqs) {
		for (int i = 0; buffer_req.buffer_offsets_to_write.size() > i; ++i) {
			buffer_req.buffer_offsets_to_write[i].get() = total_buffer_size + (i * buffer_req.buffer_size);
		}

		total_buffer_size += buffer_req.buffer_size * buffer_req.num_of_buffers;
	}

	BufferRaii storage_buffer = core_renderer.get_memory_manager().create_buffer(core_renderer, 
		utils::vulkan::MAX_FRAMES_IN_FLIGHT * total_buffer_size,
		vk::BufferUsageFlagBits::eStorageBuffer, vk::SharingMode::eExclusive,
		VMA_ALLOCATION_CREATE_HOST_ACCESS_SEQUENTIAL_WRITE_BIT, VMA_MEMORY_USAGE_AUTO,
		"super-material-" + std::to_string(m_material_id_to_data.at(queued_material_id)) + "-storage-buffer");
	std::vector<std::vector<std::unique_ptr<Texture>>> textures;

	// (step 5): create images requested for material
	for (const std::vector<MaterialTexture> &textures_for_material : super_material_data.textures) {
		std::vector<std::unique_ptr<Texture>> material_texture_output;

		for (const MaterialTexture &material_texture : textures_for_material) {
			vk::ImageViewCreateInfo image_view_create_info = AssimpModel::DEFAULT_IMAGE_VIEW_CREATE_INFO;
			vk::SamplerCreateInfo sampler_create_info = AssimpModel::DEFAULT_SAMPLER_CREATE_INFO;

			if(material_texture.image_view_create_info.has_value()) {
				image_view_create_info = *material_texture.image_view_create_info;
			}

			if(material_texture.sampler_create_info.has_value()) {
				sampler_create_info = *material_texture.sampler_create_info;
			}
			else {
				sampler_create_info.maxAnisotropy = core_renderer.get_max_sampler_anisotropy();
			}

			if (material_texture.manual_image == nullptr && !material_texture.null_image) {
				assert(!material_texture.texture_path.empty());

				material_texture_output.push_back(std::make_unique<Texture>(material_texture.texture_path, material_texture.texture_types,
																		    core_renderer, image_view_create_info, sampler_create_info,
																			std::optional<vk::CommandBuffer>{}, false, false, true));
			}
			else {
				material_texture_output.push_back(nullptr);
			}
		}

		textures.push_back(std::move(material_texture_output));
	}

	// (step 6): finish resource creation and unqueue all the materials which belong to this SuperMaterial
	super_material_data.resources = std::make_unique<SuperMaterialResources>(std::move(descriptor_set_layout), std::move(descriptor_pool),
																			 std::move(descriptor_sets_array), ~0, std::move(textures), 
																			 std::move(storage_buffer), std::move(graphics_pipeline));

	// need to wait before we std::move anything
	for (std::vector<std::unique_ptr<Texture>> &textures_for_material : super_material_data.resources->textures) {
		for (std::unique_ptr<Texture> &texture : textures_for_material) {
			if (texture != nullptr) {
				texture->delayed_create_image();
			}
		}
	}

	super_material_data.resources->graphics_pipeline.start_initial_pipeline_creation(core_renderer, false);

	for (std::pair<MaterialID, size_t> queued_material : super_material_data.materials) {
		assert(m_queued_material_ids.contains(queued_material.first));
		m_queued_material_ids.erase(queued_material.first);
	}
}

void aristaeus::gfx::vulkan::MaterialManager::write_material_descriptor_set(SuperMaterialData &super_material_data) {
	assert(super_material_data.resources != nullptr);
	SuperMaterialResources &super_material_resources = *super_material_data.resources;

	auto &core_renderer = utils::get_singleton_safe<CoreRenderer>();

	std::vector<vk::WriteDescriptorSet> descriptor_set_writes;
	vk::raii::DescriptorSet &current_descriptor_set = super_material_resources.descriptor_sets[core_renderer.get_current_frame()];

	size_t per_frame_buffer_size = super_material_resources.storage_buffer->buffer_size / utils::vulkan::MAX_FRAMES_IN_FLIGHT;
	size_t buffer_offset = core_renderer.get_current_frame() * per_frame_buffer_size;
	vk::DescriptorBufferInfo buffer_info{ super_material_resources.storage_buffer->buffer, buffer_offset, per_frame_buffer_size };
	std::vector<vk::DescriptorImageInfo> image_infos;

	size_t current_offset = 0;
	void *buffer_data;

	vmaMapMemory(super_material_resources.storage_buffer->allocator, super_material_resources.storage_buffer->allocation, &buffer_data);
	char *buffer_data_char = &reinterpret_cast<char *>(buffer_data)[buffer_offset];
	std::vector<AssimpModel::MaterialUniformBuffer *> materials;

	std::unordered_map<uint32_t, uint32_t> binding_location_indexes;

	super_material_data.first_image_binding_index.clear();

	for (int i = 0; super_material_data.textures.size() > i; ++i) {
		super_material_data.first_image_binding_index.emplace_back();

		for (int i2 = 0; super_material_data.textures[i].size() > i2; ++i2) {
			MaterialTexture &material_texture = super_material_data.textures[i][i2];

			/*if (!material_texture.dirty) {
				continue;
			}*/

			if (material_texture.manual_image != nullptr) {
				assert(material_texture.manual_image_view != nullptr && material_texture.manual_sampler != nullptr);


				vk::DescriptorImageInfo descriptor_image_info{ **material_texture.manual_sampler,
															   **material_texture.manual_image_view,
															   vk::ImageLayout::eShaderReadOnlyOptimal };

				image_infos.push_back(descriptor_image_info);
			}
			else if (material_texture.null_image) {
				image_infos.push_back(get_null_image_info());
			}
			else if (super_material_resources.textures[i][i2] != nullptr) {
				image_infos.push_back(super_material_resources.textures[i][i2]->get_descriptor_image_info(core_renderer));
			}

			uint32_t bind_index = binding_location_indexes[material_texture.bind_location]++;

			if (!super_material_data.first_image_binding_index.back().contains(material_texture.bind_location)) {
				super_material_data.first_image_binding_index.back().insert({ material_texture.bind_location, bind_index });
			}

			descriptor_set_writes.emplace_back(current_descriptor_set, material_texture.bind_location, bind_index,
				vk::DescriptorType::eCombinedImageSampler, image_infos.back());

			material_texture.dirty = false;
		}
	}

	bool super_material_data_dirty = false;

	for (int i = 0; super_material_data.data.size() > i; ++i) {
		m_current_binded_material_id = super_material_data.added_materials[i];

		for (int i2 = 0; super_material_data.data[i].size() > i2; ++i2) {
			MaterialShaderData &material_shader_data = super_material_data.data[i][i2];

			MaterialShaderData::GeneratorData generator_data =
				material_shader_data.data_generator(material_shader_data.data_generator_arg);

			materials.push_back(reinterpret_cast<AssimpModel::MaterialUniformBuffer*>(generator_data.generated_data));

			if (generator_data.generated_data != nullptr && generator_data.generated_data_size > 0) {
				std::memcpy(&buffer_data_char[current_offset], generator_data.generated_data, generator_data.generated_data_size);
				super_material_data_dirty = true;
			}

			current_offset += generator_data.generated_data_size;
		}
	}

	vmaUnmapMemory(super_material_resources.storage_buffer->allocator, super_material_resources.storage_buffer->allocation);

	if (super_material_data_dirty) {
		// image_infos here serves as an empty array
		std::vector<vk::DescriptorImageInfo> empty_images;
		descriptor_set_writes.emplace_back(*current_descriptor_set, 0, 0, vk::DescriptorType::eStorageBuffer, empty_images, buffer_info);
	}

	for (int i = 0; image_infos.size() > i; ++i) {
		descriptor_set_writes[i].pImageInfo = &image_infos[i];
	}

	if (!descriptor_set_writes.empty()) {
		core_renderer.get_logical_device_ref().updateDescriptorSets(descriptor_set_writes, {});
	}
}

#include "rasterization_gi/reflective_shadow_map.hpp"

vk::CommandBuffer aristaeus::gfx::vulkan::MaterialManager::bind_material(MaterialID material, RenderContextID render_context_id) {
	auto &core_renderer = utils::get_singleton_safe<CoreRenderer>();

	assert(!m_queued_material_ids.contains(material));

	// what are we doing when we bind a material/super material?
	// (0): we grab a command buffer corresponding to our RenderContextID and subpass id from the graphics pipeline.
	// (1): we bind the new graphics pipeline
	// (2): we generate data for all the materials' shader datas within the super material
	// (3): we write only the data that is generated (some generators may not output data if up-to-date) and the images
	// from textures
	// (4): we bind the descriptor set to the last slot in the graphics pipeline.
	// the local material id within the super material should then be passed in through a push constant

	size_t data_index = m_material_id_to_data.at(material);
	SuperMaterialData &super_material_data = m_material_datas.at(data_index);
	assert(super_material_data.resources != nullptr);
	SuperMaterialResources &super_material_resources = *super_material_data.resources;
	
	vk::CommandBuffer command_buffer =
		core_renderer.get_graphics_command_buffer_manager().get_command_buffer_for_thread(render_context_id, super_material_resources.graphics_pipeline);

	vk::raii::DescriptorSet &current_descriptor_set = super_material_resources.descriptor_sets[core_renderer.get_current_frame()];

	if (super_material_resources.last_frame_rendered != core_renderer.get_current_frame()) {
		write_material_descriptor_set(super_material_data);

		super_material_resources.last_frame_rendered = core_renderer.get_current_frame();
	}

	uint32_t material_set = 
		super_material_data.initial_material.material_set_num.has_value() ? *super_material_data.initial_material.material_set_num : 
		super_material_resources.graphics_pipeline.get_pipeline_info().descriptor_bindings.size() - 2;

	command_buffer.bindDescriptorSets(vk::PipelineBindPoint::eGraphics, *super_material_resources.graphics_pipeline.get_pipeline_layout(), 
									  material_set, *current_descriptor_set, {});

	if (m_on_super_material_bind_functions.contains(render_context_id)) {
		m_on_super_material_bind_functions.at(render_context_id)(material);
	}

	return command_buffer;
}

aristaeus::gfx::vulkan::GraphicsPipeline &aristaeus::gfx::vulkan::MaterialManager::get_graphics_pipeline_for_material(MaterialID material) {
	return m_material_datas[m_material_id_to_data.at(material)].resources->graphics_pipeline;
}

// useful for binding common environment sets such as camera or environment ubos
void aristaeus::gfx::vulkan::MaterialManager::call_upon_super_material_binds(RenderContextID render_context_id,
																			 const SuperMaterialBindFunction &super_material_bind_callback) {
	m_on_super_material_bind_functions.insert({ render_context_id, super_material_bind_callback });
}

size_t aristaeus::gfx::vulkan::MaterialManager::get_vertex_binding_size(MaterialID material_id, uint32_t binding) const {
	const SuperMaterialData &super_material = m_material_datas[m_material_id_to_data.at(material_id)];
	
	size_t vertex_size = 0;

	for (int i = 0; super_material.initial_material.pipeline_info.vertex_input_stage.vertexAttributeDescriptionCount > i; ++i) {
		vertex_size += 12;
	}

	return vertex_size;
}

uint32_t aristaeus::gfx::vulkan::MaterialManager::get_first_image_index_within_binding(MaterialID material, uint32_t binding) const {
	const SuperMaterialData &data = m_material_datas[m_material_id_to_data.at(material)];
	LocalMaterialID local_id = data.materials.at(material);

	return data.first_image_binding_index[local_id].contains(binding) ? data.first_image_binding_index[local_id].at(binding) : 0;
}