#version 450 

layout(set = 0, binding = 0) uniform sampler2D unblurred_occlusion_buffer;

layout (location = 0) in vec2 uv_coords;
layout (location = 0) out vec4 color_out;

// determines how much larger slopes within objects are tolerated 
const float k_depth_delta = 0.0078125;

float when_less_than(float x, float y) {
    return max(sign(y - x), 0.0);
}

const int BLUR_KERNEL_SIZE = 4;

void main() {
	vec3 blur_sum = vec3(0);
	vec2 texture_offset = vec2(1) / textureSize(unblurred_occlusion_buffer, 0);

	for(int i = -2; 2 > i; ++i) {
		for(int j = -2; 2 > j; ++j) {
			vec2 current_sample = uv_coords + (vec2(i, j) * texture_offset);

			blur_sum += texture(unblurred_occlusion_buffer, current_sample).r;
		}
	}

	vec3 blur_result = blur_sum / (BLUR_KERNEL_SIZE * BLUR_KERNEL_SIZE);

	color_out = vec4(blur_result, 0);
}