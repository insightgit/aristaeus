//
// Created by bobby on 26/01/23.
//

#include <filesystem>

#include "cube_map_texture.hpp"

const vk::ImageViewCreateInfo aristaeus::gfx::vulkan::CubeMapTexture::DEFAULT_IMAGE_VIEW_CREATE_INFO{ 
                                                                        {}, {}, vk::ImageViewType::eCube,
                                                                        vk::Format::eR8G8B8A8Srgb,
                                                                        {}, {vk::ImageAspectFlagBits::eColor,
                                                                        0, 1, 0, 6} };

aristaeus::gfx::vulkan::CubeMapTexture::CubeMapTexture(const std::array<std::string, 6> &texture_faces,
                                                       const std::optional<vk::SamplerCreateInfo> &sampler_create_info,
                                                       vk::PipelineBindPoint pipeline_bind_point) :
        m_pipeline_bind_point(pipeline_bind_point),
        m_cubemap_image(create_cubemap_image(texture_faces)),
        m_cubemap_image_view(create_image_view()),
        m_cubemap_sampler(utils::get_singleton_safe<CoreRenderer>().get_logical_device_ref(),
                          sampler_create_info.has_value() ? sampler_create_info.value() : get_sampler_info()) {

}

aristaeus::gfx::vulkan::CubeMapTexture::CubeMapTexture(ImageRaii cubemap_image_raii, uint32_t number_of_mip_levels,
                                                       const std::optional<vk::SamplerCreateInfo> &sampler_create_info,
                                                       vk::PipelineBindPoint pipeline_bind_point) :
        m_cubemap_image_size(cubemap_image_raii->image_width, cubemap_image_raii->image_height),
        m_pipeline_bind_point(pipeline_bind_point),
        m_cubemap_image(std::move(cubemap_image_raii)),
        m_cubemap_image_view(create_image_view(number_of_mip_levels)),
        m_cubemap_sampler(utils::get_singleton_safe<CoreRenderer>().get_logical_device_ref(),
                          sampler_create_info.has_value() ? sampler_create_info.value() : get_sampler_info(number_of_mip_levels)),
        m_number_of_mip_levels(number_of_mip_levels) {
}

aristaeus::gfx::vulkan::ImageRaii aristaeus::gfx::vulkan::CubeMapTexture::create_cubemap_image(
        const std::array<std::string, 6> &texture_faces) {
    CoreRenderer &core_renderer = utils::get_singleton_safe<CoreRenderer>();
    std::array<unsigned char *, 6> texture_datas;

    assert(texture_faces.size() == texture_datas.size());

    for(size_t i = 0; texture_faces.size() > i; ++i) {
        int image_height;
        int image_channels;
        int image_width;

        stbi_set_flip_vertically_on_load(false);

        std::ifstream input_file(texture_faces[i].c_str(), std::ios::binary);
        std::vector<unsigned char> file_buffer(std::istreambuf_iterator<char>(input_file), {});
        size_t file_buffer_size = file_buffer.size() * sizeof(unsigned char);

        m_file_hashes[i] = std::pair<std::string, utils::SHA256Digest>{ texture_faces[i], utils::get_sha256_hash(file_buffer.data(), file_buffer_size) };

        unsigned char *pixels = stbi_load_from_memory(file_buffer.data(), file_buffer_size, & image_width, &image_height, &image_channels, STBI_rgb_alpha);

        if(pixels == nullptr) {
            throw std::runtime_error("Couldn't load texture at " + texture_faces[i]);
        }

        if(i == 0) {
            m_cubemap_image_size.y = static_cast<uint32_t>(image_height);
            m_cubemap_image_size.x = static_cast<uint32_t>(image_width);
        } else if(image_height != m_cubemap_image_size.x || image_width != m_cubemap_image_size.y) {
            throw std::runtime_error("image width and height for each cubemap face must be equal");
        }

        texture_datas[i] = pixels;
    }

    ImageRaii image_raii = core_renderer.get_memory_manager().create_image(core_renderer, m_cubemap_image_size.x, m_cubemap_image_size.y,
                                                                           vk::Format::eR8G8B8A8Srgb,vk::ImageTiling::eOptimal,
                                                                           vk::ImageUsageFlagBits::eTransferDst | vk::ImageUsageFlagBits::eSampled,
                                                                           {}, VMA_MEMORY_USAGE_AUTO_PREFER_DEVICE, texture_faces[0] + "-cube_map_texture",
                                                                           texture_datas.size(), vk::ImageCreateFlagBits::eCubeCompatible);

    m_staging_image_buffer = core_renderer.get_memory_manager().create_buffer(core_renderer,
                                                         (6ul * (4 * m_cubemap_image_size.x * m_cubemap_image_size.y)),
                                                         vk::BufferUsageFlagBits::eTransferSrc, vk::SharingMode::eExclusive,
                                                         VMA_ALLOCATION_CREATE_HOST_ACCESS_SEQUENTIAL_WRITE_BIT, VMA_MEMORY_USAGE_AUTO,
                                                         "skybox-staging-image-buffer");

    void *buffer_data;

    vmaMapMemory(m_staging_image_buffer->allocator, m_staging_image_buffer->allocation, &buffer_data);

    size_t image_size = 4ul * m_cubemap_image_size.x * m_cubemap_image_size.y;

    for(size_t i = 0; texture_datas.size() > i; ++i) {
        std::memcpy(reinterpret_cast<void*>(reinterpret_cast<uintptr_t>(buffer_data) + (i * image_size)),
                    texture_datas[i], image_size);

        stbi_image_free(texture_datas[i]);
    }

    vmaUnmapMemory(m_staging_image_buffer->allocator, m_staging_image_buffer->allocation);

    core_renderer.copy_cubemap_buffer_to_image(m_staging_image_buffer, image_raii, image_size,
                                               m_pipeline_bind_point);
    core_renderer.transfer_staging_buffer_for_deletion(std::move(m_staging_image_buffer));

    return image_raii;
}

[[nodiscard]] vk::SamplerCreateInfo aristaeus::gfx::vulkan::CubeMapTexture::get_sampler_info(uint32_t number_of_mip_levels) {
    vk::SamplerCreateInfo sampler_info = DEFAULT_SAMPLER_CREATE_INFO;

    sampler_info.maxAnisotropy =
        utils::get_singleton_safe<CoreRenderer>().get_selected_physical_device_properties().limits.maxSamplerAnisotropy;
    sampler_info.addressModeU = vk::SamplerAddressMode::eClampToBorder;
    sampler_info.addressModeV = sampler_info.addressModeU;
    sampler_info.addressModeW = sampler_info.addressModeU;

    if (number_of_mip_levels > 1) {
        sampler_info.mipmapMode = vk::SamplerMipmapMode::eLinear;
        sampler_info.maxLod = static_cast<float>(number_of_mip_levels);
    }

    return sampler_info;
}
