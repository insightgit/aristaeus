#include "core_renderer.hpp"
#include "render_phase.hpp"

#include <glm/gtx/hash.hpp>

size_t aristaeus::gfx::vulkan::RenderPhase::DontCareImageInfoHasher::operator()(const DontCareImageInfo &dont_care_info) const noexcept {
    const AttachmentInfo &attachment_info = dont_care_info.attachment_info;

    return static_cast<unsigned int>(attachment_info.usage_flags) + (10000 * static_cast<unsigned int>(attachment_info.description.format)) +
        (60000 * static_cast<unsigned int>(attachment_info.description.initialLayout)) +
        (100000 * static_cast<unsigned int>(attachment_info.description.initialLayout)) +
        (200000 * std::hash<glm::uvec2>()(dont_care_info.image_resolution));
}

aristaeus::gfx::vulkan::RenderPhase::RenderPhase(const std::vector<RenderPassInfo> &render_passes, const std::string &render_phase_name) :
    m_signal_semaphores(utils::create_semaphores(CoreRenderer::instance->get_logical_device_ref())), m_render_phase_name(render_phase_name), 
    m_default_sampler(utils::get_singleton_safe<CoreRenderer>().get_logical_device_ref(), utils::get_singleton_safe<CoreRenderer>().get_recommended_sampler_create_info()) {
    for (const RenderPassInfo &render_pass_info : render_passes) {
        add_render_pass(render_pass_info, true, {});
    }
}

void aristaeus::gfx::vulkan::RenderPhase::create_render_pass_attachment(RenderPassData &render_pass_data, const glm::ivec2 &image_resolution, 
                                                                        uint32_t num_of_render_contexts, const std::string &image_category,
                                                                        vk::ImageAspectFlags image_aspect_flags, const AttachmentInfo &attachment_info,
                                                                        bool count_into_stride, int frame) {
    auto &core_renderer = utils::get_singleton_safe<CoreRenderer>();
    SharedImageRaii image;

    if (attachment_info.existing_image.size() > frame && attachment_info.existing_image[frame] != nullptr) {
        if (attachment_info.existing_image[frame]->format != attachment_info.description.format ||
            attachment_info.existing_image[frame]->image_width != image_resolution.x ||
            attachment_info.existing_image[frame]->image_height != image_resolution.y) {
            throw std::runtime_error("Existing image must conform to the image resolution and the specified attachment_info's format!");
        }

        image = attachment_info.existing_image[frame];
    }
    else {
        image = core_renderer.get_memory_manager().create_shared_image(image_resolution.x, image_resolution.y,
            attachment_info.description.format, vk::ImageTiling::eOptimal, attachment_info.usage_flags, {}, VMA_MEMORY_USAGE_AUTO, image_category,
            num_of_render_contexts, render_pass_data.info.is_cube_map ? vk::ImageCreateFlagBits::eCubeCompatible : vk::ImageCreateFlags{});
    }

    vk::ImageViewCreateInfo image_view_create_info(static_cast<vk::ImageViewCreateFlags>(0), {},
                                                   vk::ImageViewType::e2D, attachment_info.description.format, {},
                                                   vk::ImageSubresourceRange(image_aspect_flags, 0, 1, 0, 1));

    if (attachment_info.description.storeOp == vk::AttachmentStoreOp::eDontCare) {
        DontCareImageInfo dont_care_info{ attachment_info, image_resolution };

        assert(m_dont_care_image_views.contains(dont_care_info) == m_dont_care_images.contains(dont_care_info));

        if (!m_dont_care_images.contains(dont_care_info)) {
            m_dont_care_images.insert({ dont_care_info, std::move(image)});

            image_view_create_info.image = m_dont_care_images.at(dont_care_info)->image;

            for (size_t i = 0; num_of_render_contexts > i; ++i) {
                image_view_create_info.subresourceRange.baseArrayLayer = i;

                vk::raii::ImageView attachment_image_view{ core_renderer.get_logical_device_ref(), image_view_create_info };

                m_dont_care_image_views.insert({ dont_care_info, std::move(attachment_image_view) });
            }
        }
    }
    else {
        render_pass_data.resources->images.push_back(std::move(image));

        image_view_create_info.image = render_pass_data.resources->images.back()->image;

        for (size_t i2 = 0; num_of_render_contexts > i2; ++i2) {
            image_view_create_info.subresourceRange.baseArrayLayer = i2;

            render_pass_data.resources->image_views.emplace_back(core_renderer.get_logical_device_ref(), image_view_create_info);

            if (render_pass_data.info.show_attachments_in_imgui_window) {
                vk::DescriptorSet imgui_descriptor_set = ImGui_ImplVulkan_AddTexture(*m_default_sampler, *render_pass_data.resources->image_views.back(),
                                                                                     VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);

                render_pass_data.resources->texture_ids.push_back(static_cast<ImTextureID>(imgui_descriptor_set));
            }
        }

        if (count_into_stride) {
            ++render_pass_data.resources->image_image_views_stride;
        }
    }
}

void aristaeus::gfx::vulkan::RenderPhase::create_render_pass_attachments_for_context_resolution(RenderPassData &render_pass_data,
                                                                                                int frame, bool count_into_stride,
                                                                                                const glm::uvec2 &context_resolution,
                                                                                                uint32_t num_of_render_contexts,
                                                                                                bool &used_swap_chain_image) {
    auto &core_renderer = utils::get_singleton_safe<CoreRenderer>();
    glm::uvec2 swap_chain_resolution{ core_renderer.get_swap_chain_extent().width, core_renderer.get_swap_chain_extent().height };

    for (int i = 0; render_pass_data.info.render_pass_attachments.size() > i; ++i) {
        const AttachmentInfo &attachment_info = render_pass_data.info.render_pass_attachments[i];

        std::string image_category = m_render_phase_name + "-" + render_pass_data.info.name + "-" + std::to_string(i) + "-" + 
                                     std::to_string(frame) + "-(" + std::to_string(context_resolution.x) + ", " + 
                                     std::to_string(context_resolution.y) + ")";
        vk::ImageAspectFlags image_aspect_flags;

        switch (attachment_info.description.format) {
        case vk::Format::eD24UnormS8Uint:
        case vk::Format::eD32SfloatS8Uint:
        case vk::Format::eD32Sfloat:
            image_aspect_flags = vk::ImageAspectFlagBits::eDepth;
            break;
        default:
            image_aspect_flags = vk::ImageAspectFlagBits::eColor;
            break;
        }

        if (!render_pass_data.info.swap_chain_images.empty() && image_aspect_flags == vk::ImageAspectFlagBits::eColor && !used_swap_chain_image &&
            context_resolution == swap_chain_resolution) {
            render_pass_data.resources->image_views.push_back(core_renderer.create_image_view(render_pass_data.info.swap_chain_images[frame],
                attachment_info.description.format, image_aspect_flags));

            if (count_into_stride && frame == 0) {
                ++render_pass_data.resources->image_image_views_stride;
            }

            used_swap_chain_image = true;
        }
        else {
            create_render_pass_attachment(render_pass_data, context_resolution, num_of_render_contexts,
                                          image_category, image_aspect_flags, attachment_info, count_into_stride, frame);
        }
    }
}

void aristaeus::gfx::vulkan::RenderPhase::create_render_pass_attachments(RenderPassData &render_pass_data) {
    CoreRenderer &core_renderer = utils::get_singleton_safe<CoreRenderer>();

    glm::uvec2 image_resolution = render_pass_data.info.resolution.has_value() ? *render_pass_data.info.resolution :
        glm::uvec2{ core_renderer.get_swap_chain_extent().width, core_renderer.get_swap_chain_extent().height };

    std::unordered_map<glm::uvec2, size_t> num_of_context_image_resolutions;
    std::vector<glm::uvec2> context_image_resolution_ordering;
    std::unordered_map<RenderContextID, size_t> render_context_order_within_context_resolution_group;

    bool using_per_context_resolutions = render_pass_data.info.per_context_resolutions.size() >= render_pass_data.info.num_of_render_contexts;

    if (using_per_context_resolutions) {
        for (int i = 0; render_pass_data.info.num_of_render_contexts > i; ++i) {
            render_context_order_within_context_resolution_group.insert({ 
                i, num_of_context_image_resolutions[render_pass_data.info.per_context_resolutions[i]]++ });
        }
    }

    render_pass_data.resources->images.clear();
    render_pass_data.resources->image_views.clear();
    render_pass_data.resources->frame_buffers.clear();

    render_pass_data.info.viewport = vk::Viewport{ 0.0f, 0.0f, static_cast<float>(image_resolution.x), static_cast<float>(image_resolution.y), 0.0f, 1.0f };
    render_pass_data.info.viewport_scissor = vk::Rect2D{ {0, 0}, {image_resolution.x, image_resolution.y} };

    render_pass_data.resources->image_image_views_stride = 0;

    size_t frame_count = render_pass_data.info.swap_chain_images.empty() ? utils::vulkan::MAX_FRAMES_IN_FLIGHT : render_pass_data.info.swap_chain_images.size();

    for (size_t frame = 0; frame_count > frame; ++frame) {
        bool count_into_stride = frame == 0;
        bool used_swap_chain_image = false;

        if (using_per_context_resolutions) {
            for (const std::pair<const glm::uvec2, size_t> per_context_resolution_pair : num_of_context_image_resolutions) {
                if (frame == 0) {
                    context_image_resolution_ordering.push_back(per_context_resolution_pair.first);
                }

                create_render_pass_attachments_for_context_resolution(render_pass_data, frame, count_into_stride, per_context_resolution_pair.first,
                                                                      per_context_resolution_pair.second, used_swap_chain_image);

                if (count_into_stride) {
                    count_into_stride = false;
                }
            }
        }
        else {
            create_render_pass_attachments_for_context_resolution(render_pass_data, frame, count_into_stride, image_resolution,
                                                                  render_pass_data.info.num_of_render_contexts, used_swap_chain_image);
        }
    }

    assert(m_dont_care_images.size() == m_dont_care_image_views.size());

    for (size_t frame = 0; frame_count > frame; ++frame) {
        glm::uvec2 past_context_image_resolution;

        for (size_t render_context = 0; render_pass_data.info.num_of_render_contexts > render_context; ++render_context) {
            std::vector<vk::ImageView> attachments;
            glm::uvec2 context_image_resolution = using_per_context_resolutions ? render_pass_data.info.per_context_resolutions[render_context] : image_resolution;

            size_t image_view_index;

            for (size_t i = 0; render_pass_data.info.render_pass_attachments.size() > i; ++i) {
                if (render_pass_data.info.render_pass_attachments[i].description.storeOp == vk::AttachmentStoreOp::eDontCare) {
                    DontCareImageInfo dont_care_info{ render_pass_data.info.render_pass_attachments[i], context_image_resolution };

                    attachments.push_back(*m_dont_care_image_views.at(dont_care_info));
                }
                else {
                    if (using_per_context_resolutions) {
                        size_t num_of_contexts_before_resolution_group = 0;
                        size_t num_of_contexts_in_resolution_group = num_of_context_image_resolutions.at(context_image_resolution);

                        size_t context_order_num_in_resolution_group = render_context_order_within_context_resolution_group.at(render_context);

                        for (int i2 = 0; context_image_resolution_ordering.size() > i2; ++i2) {
                            if (context_image_resolution_ordering[i2] == context_image_resolution) {
                                break;
                            }
                            else {
                                num_of_contexts_before_resolution_group += num_of_context_image_resolutions.at(context_image_resolution_ordering[i2]);
                            }
                        }

                        image_view_index = (frame * render_pass_data.info.num_of_render_contexts * render_pass_data.resources->image_image_views_stride) +
                                           (num_of_contexts_before_resolution_group * render_pass_data.resources->image_image_views_stride) + 
                                           (num_of_contexts_in_resolution_group * i) + context_order_num_in_resolution_group;
                    }
                    else {
                        image_view_index = (frame * render_pass_data.info.num_of_render_contexts * render_pass_data.resources->image_image_views_stride) +
                            (i * render_pass_data.info.num_of_render_contexts) + render_context;
                    }

                    attachments.push_back(*render_pass_data.resources->image_views[image_view_index]);
                }
            }

            vk::FramebufferCreateInfo frame_buffer_info{ {}, *render_pass_data.resources->render_pass, attachments, context_image_resolution.x,
                                                         context_image_resolution.y, 1 };

            render_pass_data.resources->frame_buffers.emplace_back(core_renderer.get_logical_device_ref(), frame_buffer_info, nullptr);
        }
    }
}

aristaeus::gfx::vulkan::RenderPhase::SubpassInfo aristaeus::gfx::vulkan::RenderPhase::get_default_subpass_info(const RenderPassData &render_pass_data) {
    SubpassInfo subpass_info;

    subpass_info.dependencies.push_back(vk::SubpassDependency{
        VK_SUBPASS_EXTERNAL, 0,
        vk::PipelineStageFlagBits::eColorAttachmentOutput | vk::PipelineStageFlagBits::eEarlyFragmentTests,
        vk::PipelineStageFlagBits::eColorAttachmentOutput | vk::PipelineStageFlagBits::eEarlyFragmentTests,
        {}, vk::AccessFlagBits::eColorAttachmentWrite | vk::AccessFlagBits::eDepthStencilAttachmentWrite
    });
    subpass_info.descriptions.push_back({ {}, vk::PipelineBindPoint::eGraphics });

    m_default_subpass_info_color_attachment_refs[render_pass_data.info.name].clear();

    for (size_t i = 0; render_pass_data.info.render_pass_attachments.size() > i; ++i) {
        vk::ImageLayout final_image_layout = render_pass_data.info.render_pass_attachments[i].description.finalLayout;
        vk::Format image_format = render_pass_data.info.render_pass_attachments[i].description.format;
        bool is_depth_format = image_format == vk::Format::eD24UnormS8Uint || image_format == vk::Format::eD32SfloatS8Uint ||
            image_format == vk::Format::eD32Sfloat;

        switch (final_image_layout) {
        case vk::ImageLayout::ePresentSrcKHR:
        case vk::ImageLayout::eShaderReadOnlyOptimal:
            if (is_depth_format) {
                final_image_layout = vk::ImageLayout::eDepthStencilAttachmentOptimal;
            }
            else {
                final_image_layout = vk::ImageLayout::eColorAttachmentOptimal;
            }
            break;
        default:
            break;
        }

        vk::AttachmentReference attachment_ref{ static_cast<uint32_t>(i), final_image_layout };

        if (is_depth_format) {
            m_default_subpass_info_depth_stencil_attachment_ref[render_pass_data.info.name] = attachment_ref;
        }
        else {
            m_default_subpass_info_color_attachment_refs[render_pass_data.info.name].push_back(attachment_ref);
        }
    }

    subpass_info.descriptions.back().setColorAttachments(m_default_subpass_info_color_attachment_refs[render_pass_data.info.name]);

    if (m_default_subpass_info_depth_stencil_attachment_ref.contains(render_pass_data.info.name)) {
        subpass_info.descriptions.back().pDepthStencilAttachment = &m_default_subpass_info_depth_stencil_attachment_ref[render_pass_data.info.name];
    }

    return subpass_info;
}

bool aristaeus::gfx::vulkan::RenderPhase::add_render_pass(RenderPassInfo render_pass_info, bool add_dependencies,
                                                          const std::unordered_map<std::string, vk::PipelineStageFlags> &child_dependents) {
    CoreRenderer &core_renderer = *CoreRenderer::instance;

    if (m_render_pass_data.contains(render_pass_info.name)) {
        return false;
    }

    render_pass_info.num_of_render_contexts = std::max(render_pass_info.num_of_render_contexts, static_cast<size_t>(1));

    for (const std::pair<const std::string, vk::PipelineStageFlags> &phase_dep : render_pass_info.phase_dependencies) {
        if (!m_dependencies.contains(phase_dep.first) || ((m_dependencies.at(phase_dep.first) & phase_dep.second) == vk::PipelineStageFlags{})) {
            // dependency does not exist!
            if (add_dependencies && m_render_phase_name != phase_dep.first) {
                m_dependencies[phase_dep.first] |= phase_dep.second;
            }
            else {
                return false;
            }
        }
    }

    for (int i = 0; render_pass_info.render_pass_attachments.size() > i; ++i) {
        bool is_depth = false;

        switch (render_pass_info.render_pass_attachments[i].description.format) {
        case vk::Format::eD24UnormS8Uint:
        case vk::Format::eD32SfloatS8Uint:
        case vk::Format::eD32Sfloat:
            is_depth = true;
            break;
        }

        if (render_pass_info.clear_values.size() == i) {
            if (is_depth) {
                render_pass_info.clear_values.push_back(vk::ClearDepthStencilValue{ 1.0f, 0 });
            }
            else {
                render_pass_info.clear_values.push_back(vk::ClearColorValue{ 0.0f, 0.0f, 0.0f, 1.0f });
            }
        }
    }

    std::unordered_map<std::string, vk::PipelineStageFlags> global_dependents = m_dependents;

    for (const std::pair<const std::string, vk::PipelineStageFlags> &child_dependent : child_dependents) {
        global_dependents[child_dependent.first] |= child_dependent.second;
    }

    for (const std::pair<const std::string, vk::PipelineStageFlags> &phase_dep : render_pass_info.phase_dependents) {
        if (!global_dependents.contains(phase_dep.first) || ((global_dependents.at(phase_dep.first) & phase_dep.second) == vk::PipelineStageFlags{})) {
            // dependent does not exist!
            if (m_render_phase_name != phase_dep.first) {
                m_dependents[phase_dep.first] |= phase_dep.second;
            }
            else {
                return false;
            }
        }
    }

    std::string render_pass_name = render_pass_info.name;

    m_render_pass_data.insert(std::move(std::pair<std::string, RenderPassData>{ render_pass_name, std::move(RenderPassData{
        .info = std::move(render_pass_info)
    }) }));

    RenderPassData &render_pass_data = m_render_pass_data.at(render_pass_name);

    SubpassInfo subpass_info;

    if (!render_pass_data.info.render_pass_attachments.empty()) {
        if (render_pass_data.info.subpass_info.has_value()) {
            subpass_info = *render_pass_data.info.subpass_info;
        }
        else {
            subpass_info = get_default_subpass_info(render_pass_data);
        }

        std::vector<vk::AttachmentDescription> attachment_descriptions;

        for(const AttachmentInfo &attachment_info : render_pass_data.info.render_pass_attachments) {
            attachment_descriptions.push_back(attachment_info.description);
        }

        vk::RenderPassCreateInfo render_pass_create_info {
            {}, attachment_descriptions, subpass_info.descriptions, subpass_info.dependencies
        };

        render_pass_data.resources = VulkanRenderPassResources{ .render_pass = vk::raii::RenderPass{core_renderer.get_logical_device_ref(), render_pass_create_info}};

        create_render_pass_attachments(render_pass_data);

        m_num_of_render_contexts += render_pass_data.info.num_of_render_contexts;
    }

    if (render_pass_data.info.one_shot) {
        render_pass_data.one_shot_render_pass_fired_frame = render_pass_info.fire_one_shot_on_creation ? std::optional<size_t>{} : -1;
        render_pass_data.one_shot_contextless_draw_called = false;

        if (render_pass_info.fire_one_shot_on_creation) {
            m_render_phase_active = true;
        }
    }
    else {
        m_render_phase_active = true;
    }

    return true;
}

std::optional<vk::SubmitInfo> aristaeus::gfx::vulkan::RenderPhase::submit_render_phase(const std::vector<std::pair<vk::Semaphore, vk::PipelineStageFlags>> &additional_wait_semaphores) {
    m_num_of_render_contexts_last_rendered = 0;
    
    if (!m_render_phase_active) {
        return {};
    }

    CoreRenderer &core_renderer = *CoreRenderer::instance;
    uint32_t current_frame = core_renderer.get_current_frame();
    SubmitData &current_submit_data = m_submit_datas[current_frame];

    vk::CommandBuffer primary_command_buffer = 
        core_renderer.get_graphics_command_buffer_manager().get_primary_command_buffer(current_frame, m_phase_id.value());

    current_submit_data.primary_command_buffer = primary_command_buffer;

    primary_command_buffer.reset({});
    primary_command_buffer.begin(vk::CommandBufferBeginInfo{});

    current_submit_data.reset();

    for (const std::pair<vk::Semaphore, vk::PipelineStageFlags> &wait_semaphore_pair : additional_wait_semaphores) {
        current_submit_data.wait_semaphores.push_back(wait_semaphore_pair.first);
        current_submit_data.wait_stage_masks.push_back(wait_semaphore_pair.second);
    }

    if (Window::PERFORMANCE_MARKERS_ENABLED) {
        std::string debug_marker_text = "Render Phase " + m_render_phase_name;

        vk::DebugUtilsLabelEXT pre_render_pass_debug_marker{ debug_marker_text.c_str() };

        primary_command_buffer.beginDebugUtilsLabelEXT(pre_render_pass_debug_marker);
    }

    RenderContextID current_render_context_id = m_render_context_id;
    bool all_one_shots = true;
    bool in_default_phase = core_renderer.get_default_rasterization_render_phase_name() == m_render_phase_name;

    assert(m_before_phase_function_calls.size() == m_before_function_args.size());
    assert(m_after_phase_function_calls.size() == m_after_function_args.size());

    for (size_t i = 0; m_before_phase_function_calls.size() > i; ++i) {
        m_before_phase_function_calls[i](m_before_function_args[i]);
    }

    if (in_default_phase) {
        core_renderer.before_default_render_phase();
    }

    // we intentionally wait to set the one_shot_render_pass_fired flag in RenderPassData so
    // that other following render passes know which passes came before them and 
    // get_render_pass_starting_render_context_id works. In other words,
    // You think you just fell out of a coconut tree? 
    // You exist in the context of all in which you live and what came before you.
    std::vector<std::reference_wrapper<RenderPassData>> one_shot_render_passes_fired;

    for(std::pair<const std::string, RenderPassData> &render_pass_data : m_render_pass_data) {
        bool has_render_pass = render_pass_data.second.resources.has_value();
        glm::uvec2 image_resolution = render_pass_data.second.info.resolution.has_value() ? *render_pass_data.second.info.resolution :
            glm::uvec2{ core_renderer.get_swap_chain_extent().width, core_renderer.get_swap_chain_extent().height };
        bool using_per_context_resolution = 
            render_pass_data.second.info.per_context_resolutions.size() >= render_pass_data.second.info.num_of_render_contexts;

        if (render_pass_data.second.info.one_shot) {
            if (render_pass_data.second.one_shot_render_pass_fired_frame.has_value() || !render_pass_data.second.one_shot_contextless_draw_called) {
                current_render_context_id += render_pass_data.second.info.num_of_render_contexts;
                continue;
            }
            else {
                one_shot_render_passes_fired.push_back(render_pass_data.second);
            }
        }
        else {
            all_one_shots = false;
        }

        assert(!has_render_pass || render_pass_data.second.resources.has_value());
        assert(render_pass_data.first == render_pass_data.second.info.name);

        m_current_render_pass = render_pass_data.first;
        m_num_of_render_contexts = render_pass_data.second.info.num_of_render_contexts;

        if (Window::PERFORMANCE_MARKERS_ENABLED) {
            std::string debug_marker_text = "Render Pass " + m_current_render_pass;

            vk::DebugUtilsLabelEXT pre_render_pass_debug_marker{ debug_marker_text.c_str() };

            primary_command_buffer.beginDebugUtilsLabelEXT(pre_render_pass_debug_marker);
        }

        if (render_pass_data.second.info.before_function.has_value()) {
            (*render_pass_data.second.info.before_function)(render_pass_data.second.info.before_function_arg);
        }

        int number_of_render_contexts = has_render_pass ? render_pass_data.second.info.num_of_render_contexts : 1;

        for (int i = 0; number_of_render_contexts > i; ++i) {
            core_renderer.set_current_render_context_id(current_render_context_id);

            if (using_per_context_resolution) {
                image_resolution = render_pass_data.second.info.per_context_resolutions[i];
                render_pass_data.second.info.viewport = vk::Viewport{ 0.0f, 0.0f, static_cast<float>(image_resolution.x),
                                                                      static_cast<float>(image_resolution.y), 0.0f, 1.0f };
                render_pass_data.second.info.viewport_scissor = vk::Rect2D{ {0, 0}, {image_resolution.x, image_resolution.y} };
            }

            if (Window::PERFORMANCE_MARKERS_ENABLED) {
                std::string debug_marker_text = "Render Context " + std::to_string(current_render_context_id) + "/" + std::to_string(i) + 
                    " (" + std::to_string(image_resolution.x) + ", " + std::to_string(image_resolution.y) + ")";

                vk::DebugUtilsLabelEXT pre_render_pass_debug_marker{ debug_marker_text.c_str() };

                primary_command_buffer.beginDebugUtilsLabelEXT(pre_render_pass_debug_marker);
            }

            if (has_render_pass) {
                uint32_t frame_count = render_pass_data.second.info.swap_chain_images.empty() ? current_frame : core_renderer.get_current_swap_chain_image();

                vk::Framebuffer frame_buffer_to_use = *render_pass_data.second.resources->frame_buffers[i + (frame_count * number_of_render_contexts)];

                vk::RenderPassBeginInfo begin_info{ *render_pass_data.second.resources->render_pass, frame_buffer_to_use,
                                                    {{0, 0}, {image_resolution.x, image_resolution.y}}, render_pass_data.second.info.clear_values };

                primary_command_buffer.beginRenderPass(begin_info, vk::SubpassContents::eSecondaryCommandBuffers);

                m_vulkan_render_pass_scope = true;
            }

            size_t subpass_num = render_pass_data.second.info.subpass_info.has_value() ? render_pass_data.second.info.subpass_info->descriptions.size() : 1;

            for (int i = 0; subpass_num > i; ++i) {
                core_renderer.get_graphics_command_buffer_manager().begin_secondary_command_buffers(core_renderer.get_current_frame(), current_render_context_id, i);
            }

            render_pass_data.second.info.draw_function(render_pass_data.second.info.draw_function_arg);

            if (core_renderer.in_default_rasterization_render_pass()) {
                assert(in_default_phase);
                core_renderer.call_event<void*>(std::string{CoreRenderer::DEFAULT_RENDER_PASS_DRAW_EVENT}, nullptr);
            }

            if (m_draw_next_frame_funcs.contains(current_render_context_id)) {
                for (std::function<void()> draw_func : m_draw_next_frame_funcs.at(current_render_context_id)) {
                    draw_func();
                }

                m_draw_next_frame_funcs.erase(current_render_context_id);
            }

            core_renderer.get_graphics_command_buffer_manager().record_all_secondary_command_buffers(current_render_context_id++, subpass_num - 1, { primary_command_buffer });

            if (has_render_pass) {
                primary_command_buffer.endRenderPass();
                m_vulkan_render_pass_scope = false;
            }

            if (Window::PERFORMANCE_MARKERS_ENABLED) {
                primary_command_buffer.endDebugUtilsLabelEXT();
            }
        }

        m_num_of_render_contexts_last_rendered += number_of_render_contexts;

        if (render_pass_data.second.info.after_function.has_value()) {
            (*render_pass_data.second.info.after_function)(render_pass_data.second.info.after_function_arg);
        }

        if (in_default_phase) {
            core_renderer.after_default_render_phase();
        }

        if (Window::PERFORMANCE_MARKERS_ENABLED) {
            primary_command_buffer.endDebugUtilsLabelEXT();
        }
    }

    for (size_t i = 0; m_after_phase_function_calls.size() > i; ++i) {
        m_after_phase_function_calls[i](m_after_function_args[i]);
    }

    std::unordered_set<std::string> new_last_frame_one_shots;

    for (std::reference_wrapper<RenderPassData> &render_pass_data : one_shot_render_passes_fired) {
        if (m_one_shot_render_passes_fired_last_frame.contains(render_pass_data.get().info.name)) {
            render_pass_data.get().one_shot_render_pass_fired_frame = core_renderer.get_total_frames_rendered();
        }
        else {
            new_last_frame_one_shots.insert(render_pass_data.get().info.name);
        }
    }

    m_one_shot_render_passes_fired_last_frame = new_last_frame_one_shots;

    for (const std::pair<const std::string, vk::PipelineStageFlags> &dependency : m_dependencies) {
        current_submit_data.wait_semaphores.push_back(*core_renderer.get_render_phase_semaphore(dependency.first, current_frame));
        current_submit_data.wait_stage_masks.push_back(dependency.second);
    }

    if (Window::PERFORMANCE_MARKERS_ENABLED) {
        primary_command_buffer.endDebugUtilsLabelEXT();
    }

    primary_command_buffer.end();

    if (all_one_shots && m_one_shot_render_passes_fired_last_frame.empty()) {
        m_render_phase_active = false;
    }

    current_submit_data.signal_semaphore = *m_signal_semaphores[current_frame];

    return vk::SubmitInfo{ current_submit_data.wait_semaphores, current_submit_data.wait_stage_masks, current_submit_data.primary_command_buffer, current_submit_data.signal_semaphore };
}

void aristaeus::gfx::vulkan::RenderPhase::on_swap_chain_recreation(const std::vector<vk::Image> &new_swap_chain_images) {
    for (std::pair<const std::string, RenderPassData> &render_pass_data : m_render_pass_data) {
        if (render_pass_data.second.resources.has_value() && !render_pass_data.second.info.resolution.has_value()) {
            render_pass_data.second.resources->frame_buffers.clear();
            render_pass_data.second.resources->images.clear();
            render_pass_data.second.resources->image_views.clear();
        }
    }

    m_dont_care_image_views.clear();
    m_dont_care_images.clear();

    for (std::pair<const std::string, RenderPassData> &render_pass_data_pair : m_render_pass_data) {
        if (render_pass_data_pair.second.resources.has_value() && !render_pass_data_pair.second.resources->images.empty()) {
            continue;
        }

        if (!render_pass_data_pair.second.info.swap_chain_images.empty()) {
            render_pass_data_pair.second.info.swap_chain_images = new_swap_chain_images;
        }

        if(render_pass_data_pair.second.resources.has_value()) {
            create_render_pass_attachments(render_pass_data_pair.second);
        }
    }
}

aristaeus::gfx::vulkan::SharedImageRaii &aristaeus::gfx::vulkan::RenderPhase::get_render_pass_image(const std::string &render_pass_name, 
                                                                                                    uint32_t current_frame) {
    return m_render_pass_data.at(render_pass_name).resources.value().images[current_frame];
}

vk::raii::ImageView &aristaeus::gfx::vulkan::RenderPhase::get_render_pass_image_view(const std::string &render_pass_name, uint32_t current_frame) {
    return m_render_pass_data.at(render_pass_name).resources.value().image_views[current_frame];
}

vk::raii::RenderPass &aristaeus::gfx::vulkan::RenderPhase::get_render_pass(const std::string &render_pass_name) {
    return m_render_pass_data.at(render_pass_name).resources.value().render_pass;
}

std::optional<vk::CommandBufferInheritanceInfo> aristaeus::gfx::vulkan::RenderPhase::get_current_cmd_inheritance_info() {
    if (in_vulkan_render_pass_scope()) {
        RenderContextID render_context_id = utils::get_singleton_safe<CoreRenderer>().get_current_render_context_id();

        return get_cmd_inheritance_info(render_context_id);
    }
    else {
        return {};
    } 
}

std::optional<vk::CommandBufferInheritanceInfo> aristaeus::gfx::vulkan::RenderPhase::get_cmd_inheritance_info(RenderContextID render_context_id) {
    assert(render_context_id >= m_render_context_id);

    CoreRenderer &core_renderer = utils::get_singleton_safe<CoreRenderer>();
    RenderContextID local_render_context_id = render_context_id - get_render_pass_starting_render_context_id(get_current_render_pass());
    RenderContextID prev_render_context_id = 0;

    if (!m_current_render_pass.empty()) {
        RenderPassData &render_pass_data = m_render_pass_data.at(m_current_render_pass);
        VulkanRenderPassResources &resources = render_pass_data.resources.value();

        int number_of_render_contexts = render_pass_data.info.num_of_render_contexts;

        uint32_t frame_count = render_pass_data.info.swap_chain_images.empty() ? core_renderer.get_current_frame() : core_renderer.get_current_swap_chain_image();

        vk::Framebuffer frame_buffer = *resources.frame_buffers[(frame_count * number_of_render_contexts) +
                                                                local_render_context_id];

        return vk::CommandBufferInheritanceInfo{ *resources.render_pass, 0, frame_buffer };
    }
    else {
        for (std::pair<const std::string, RenderPassData>& render_pass_data : m_render_pass_data) {
            RenderContextID next_render_context = render_pass_data.second.info.num_of_render_contexts + prev_render_context_id;

            if (local_render_context_id >= prev_render_context_id && next_render_context > local_render_context_id) {
                RenderPassData &render_pass_data = m_render_pass_data.at(m_current_render_pass);
                VulkanRenderPassResources &resources = render_pass_data.resources.value();

                int number_of_render_contexts = render_pass_data.info.num_of_render_contexts;
                uint32_t frame_count = render_pass_data.info.swap_chain_images.empty() ? core_renderer.get_current_frame() : core_renderer.get_current_swap_chain_image();

                vk::Framebuffer frame_buffer = *resources.frame_buffers[(frame_count * number_of_render_contexts) + local_render_context_id];

                return vk::CommandBufferInheritanceInfo{ *resources.render_pass, 0, frame_buffer };
            }
            else {
                prev_render_context_id = next_render_context;
            }
        }
    }

    return {};
}

void aristaeus::gfx::vulkan::RenderPhase::retoggle_one_shot_render_pass(const std::string &render_pass_name) {
    RenderPassData &render_pass_data = m_render_pass_data.at(render_pass_name);

    assert(render_pass_data.info.one_shot);

    if (render_pass_data.one_shot_render_pass_fired_frame.has_value()) {
        render_pass_data.one_shot_render_pass_fired_frame = {};
        render_pass_data.one_shot_contextless_draw_called = false;
    }

    m_render_phase_active = true;
}

void aristaeus::gfx::vulkan::RenderPhase::update_attachments(const std::string &render_pass_name, const std::vector<AttachmentInfo> &attachment_infos, const std::vector<size_t> &attachment_indices) {
    auto &core_renderer = utils::get_singleton_safe<CoreRenderer>();
    RenderPassData &render_pass_data = m_render_pass_data.at(render_pass_name);
    assert(attachment_indices.size() == attachment_infos.size());
    
    for (int i = 0; attachment_indices.size() > i; ++i) {
        assert(render_pass_data.info.render_pass_attachments.size() > attachment_indices[i]);

        render_pass_data.info.render_pass_attachments[attachment_indices[i]] = attachment_infos[i];
    }

    std::vector<vk::AttachmentDescription> attachment_descriptions;
    SubpassInfo subpass_info;

    if (render_pass_data.info.subpass_info.has_value()) {
        subpass_info = *render_pass_data.info.subpass_info;
    }
    else {
        subpass_info = get_default_subpass_info(render_pass_data);
    }

    for (const AttachmentInfo &attachment_info : render_pass_data.info.render_pass_attachments) {
        attachment_descriptions.push_back(attachment_info.description);
    }

    vk::RenderPassCreateInfo render_pass_create_info{{}, attachment_descriptions, subpass_info.descriptions, subpass_info.dependencies };

    render_pass_data.resources = VulkanRenderPassResources{ .render_pass = vk::raii::RenderPass{core_renderer.get_logical_device_ref(), render_pass_create_info} };

    create_render_pass_attachments(render_pass_data);
}

aristaeus::gfx::vulkan::RenderContextID aristaeus::gfx::vulkan::RenderPhase::get_render_pass_starting_render_context_id(const std::string &render_pass_name) const {
    assert(m_render_pass_data.contains(render_pass_name));

    int current_render_context_id = m_render_context_id;
    
    for (const std::pair<const std::string, RenderPassData> &render_pass : m_render_pass_data) {
        if (render_pass.first == render_pass_name) {
            break;
        }

        current_render_context_id += render_pass.second.info.num_of_render_contexts;
    }

    return current_render_context_id;
}

[[nodiscard]] glm::uvec2 aristaeus::gfx::vulkan::RenderPhase::get_current_resolution() const {
    auto &core_renderer = utils::get_singleton_safe<vulkan::CoreRenderer>();
    std::string current_render_pass = get_current_render_pass();
    RenderContextID local_render_context_id = core_renderer.get_current_render_context_id() - get_render_pass_starting_render_context_id(current_render_pass);

    const RenderPassData &render_pass_data = m_render_pass_data.at(current_render_pass);
    bool using_context_resolutions = render_pass_data.info.per_context_resolutions.size() >= render_pass_data.info.num_of_render_contexts;

    if (using_context_resolutions) {
        return render_pass_data.info.per_context_resolutions[local_render_context_id];
    }
    else {
        return render_pass_data.info.resolution.value();
    }
}

void aristaeus::gfx::vulkan::RenderPhase::contextless_draw() {
    auto &core_renderer = utils::get_singleton_safe<CoreRenderer>();
    RenderContextID current_render_context_id = m_render_context_id;

    m_contextless_draw = true;

    for (std::pair<const std::string, RenderPassData> &data_pair : m_render_pass_data) {
        m_current_render_pass = data_pair.first;

        if (!data_pair.second.info.one_shot || !data_pair.second.one_shot_render_pass_fired_frame.has_value()) {
            for (int i = 0; data_pair.second.info.num_of_render_contexts > i; ++i) {
                core_renderer.set_current_render_context_id(current_render_context_id++);

                core_renderer.m_disallow_command_recording = true;

                if (data_pair.second.info.contextless_draw_function.has_value()) {
                    (*data_pair.second.info.contextless_draw_function)(data_pair.second.info.contextless_draw_function_arg);
                }

                core_renderer.m_disallow_command_recording = false;

                data_pair.second.one_shot_contextless_draw_called = true;
            }
        }
        else {
            current_render_context_id += data_pair.second.info.num_of_render_contexts;
        }
    }

    m_contextless_draw = false;
}

void aristaeus::gfx::vulkan::RenderPhase::add_draw_next_frame(const std::function<void()> &draw_function) {
    m_draw_next_frame_funcs[utils::get_singleton_safe<CoreRenderer>().get_current_render_context_id()].push_back(draw_function);
}

void aristaeus::gfx::vulkan::RenderPhase::imgui_draw(vk::CommandBuffer &command_buffer) {
    auto &core_renderer = utils::get_singleton_safe<CoreRenderer>();

    for (std::pair<const std::string, RenderPassData> &render_pass_data : m_render_pass_data) {
        std::stringstream imgui_text;

        if (render_pass_data.second.info.show_attachments_in_imgui_window) {
            if (render_pass_data.second.info.one_shot) {
                imgui_text << "[one_shot] ";
            }

            imgui_text << "RP " << render_pass_data.second.info.name;

            if (ImGui::TreeNode(imgui_text.str().c_str())) {
                for (int i = 0; render_pass_data.second.resources->texture_ids.size() > i; ++i) {
                    ImVec2 image_size;

                    SharedImageRaii &image_raii = render_pass_data.second.resources->images[i];

                    image_size.x = glm::min<uint32_t>(image_raii->image_width, 256);
                    image_size.y = glm::min<uint32_t>(image_raii->image_height, 256);

                    ImGui::Text("Attachment #%i", i);
                    ImGui::Image(render_pass_data.second.resources->texture_ids[i], image_size);
                }

                ImGui::TreePop();
            }
        }
    }
}
