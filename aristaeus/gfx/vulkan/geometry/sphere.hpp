#ifndef ARISTAEUS_SPHERE_HPP
#define ARISTAEUS_SPHERE_HPP

#include "geometry.hpp"

namespace aristaeus::gfx::vulkan {
	class Sphere : public Geometry {
	public:
		Sphere(CoreRenderer &core_renderer, size_t stack_count, size_t sector_count, float radius,
			   const ModelInfo &model_info) : Geometry(create_geometry(core_renderer, stack_count, sector_count, radius, model_info)) {}
	private:
		Geometry create_geometry(CoreRenderer& core_renderer, size_t stack_count, size_t sector_count, float radius, const ModelInfo &model_info);
	};
}

#endif