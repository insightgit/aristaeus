#version 460

layout (location = 0) out vec4 color_out;

layout (location = 0) in GeomOut {
    int line_type;
} geom_in;

float when_equal_to(float x, float y) {
    return 1.0 - abs(sign(x - y));
}

void main() {
    color_out = (vec4(0.0, 1.0, 0.0, 1.0) * when_equal_to(geom_in.line_type, 0)) +
                (vec4(0.0, 1.0, 0.0, 1.0) * when_equal_to(geom_in.line_type, 1)) +
                (vec4(1.0, 0.0, 0.0, 1.0) * when_equal_to(geom_in.line_type, 2));
}
