//
// Created by bobby on 03/03/23.
//

#include "memory_manager.hpp"

#ifndef NDEBUG
std::vector<VkBuffer> aristaeus::gfx::vulkan::MemoryManager::created_buffers {};
std::mutex aristaeus::gfx::vulkan::MemoryManager::created_buffers_lock;
#endif

aristaeus::gfx::vulkan::MemoryManager::MemoryManager(Window &window, vk::raii::Device &logical_device) :
    m_allocator(window.create_allocator(logical_device)) {
    #ifndef NDEBUG
    get_buffer_purpose(nullptr);
    #endif
}

aristaeus::gfx::vulkan::BufferRaii aristaeus::gfx::vulkan::MemoryManager::create_buffer(
                                                          CoreRenderer &core_renderer, vk::DeviceSize size,
                                                          vk::BufferUsageFlags usage,
                                                          vk::SharingMode sharing_mode,
                                                          VmaAllocationCreateFlagBits allocation_flags,
                                                          VmaMemoryUsage allocation_usage_flags,
                                                          const std::string &allocation_category) {
    VmaAllocationCreateInfo allocation_info {
            .flags = static_cast<VmaAllocationCreateFlags>(allocation_flags),
            .usage = allocation_usage_flags
    };

    vk::BufferCreateInfo buffer_info {{}, size, usage, sharing_mode};

    VkBuffer buffer;
    VmaAllocation buffer_allocation;
    auto buffer_create_info = static_cast<VkBufferCreateInfo>(buffer_info);
    BufferRaii buffer_raii;

    VkResult buffer_creation_result = vmaCreateBuffer(*m_allocator, &buffer_create_info, &allocation_info, &buffer,
                                                      &buffer_allocation, nullptr);

    if(buffer_creation_result != VK_SUCCESS) {
        throw std::runtime_error("Couldn't create and allocate buffer!");
    }

    #ifndef NDEBUG
    {
        std::lock_guard<std::mutex> lock_guard{m_allocated_buffers_lock};
        m_allocated_buffers.insert({ buffer, allocation_category });

        if (!m_alive_buffers_by_size.contains(size)) {
            m_alive_buffers_by_size.insert({ size,  {} });
        }

        m_alive_buffers_by_size[size].insert({ buffer, allocation_category });
    }
    #endif

    buffer_raii.reset(new VmaBuffer {
        .buffer = buffer,
        .buffer_size = size,
        .allocation = buffer_allocation,
        .allocator = *m_allocator,
        .core_renderer = core_renderer
    });

    return buffer_raii;
}


aristaeus::gfx::vulkan::VmaImage *aristaeus::gfx::vulkan::MemoryManager::create_vma_image(uint32_t width, uint32_t height, vk::Format format, 
    vk::ImageTiling tiling, vk::ImageUsageFlags usage, VmaAllocationCreateFlagBits allocation_flags, VmaMemoryUsage allocation_usage_flags,
    const std::string &allocation_category, uint32_t array_layers, vk::ImageCreateFlags flags, uint32_t mip_levels) {
    vk::ImageCreateInfo texture_image_info(flags, vk::ImageType::e2D,
        format, vk::Extent3D(width, height, 1), std::max(mip_levels, 1u),
        std::max(array_layers, 1u), vk::SampleCountFlagBits::e1, tiling, usage,
        vk::SharingMode::eExclusive);

    VmaAllocationCreateInfo allocation_info{
            .flags = static_cast<VmaAllocationCreateFlags>(allocation_flags),
            .usage = allocation_usage_flags
    };

    VkImage image;
    auto image_create_info = static_cast<VkImageCreateInfo>(texture_image_info);
    VmaAllocation image_allocation;
    ImageRaii image_raii;
    VmaAllocator allocator = *m_allocator;

    VkResult image_creation_result = vmaCreateImage(allocator, &image_create_info, &allocation_info, &image,
        &image_allocation, nullptr);

    if (image_creation_result != VK_SUCCESS) {
        throw std::runtime_error("Couldn't create and allocate image!");
    }

    VmaImage *vma_image = new VmaImage{
        .image = image,
        .image_height = height,
        .image_width = width,
        .allocation = image_allocation,
        .allocator = *m_allocator,
        .array_layers = array_layers,
        .mip_levels = mip_levels,
        .format = format
    };

    vma_image->image_layouts.resize(array_layers * mip_levels);

    for (uint32_t i = 0; array_layers * mip_levels > i; ++i) {
        vma_image->image_layouts[i] = vk::ImageLayout::eUndefined;
    }

    #ifndef NDEBUG
    {
        std::lock_guard<std::mutex> lock_guard{ m_allocated_images_lock };
        m_allocated_images.insert({ image, allocation_category });

        glm::vec2 image_size{ width, height };

        if (!m_alive_images_by_size.contains(image_size)) {
            m_alive_images_by_size.insert({ glm::vec2{width, height},  {} });
        }

        m_alive_images_by_size[image_size].insert({ image, allocation_category });
    }
    #endif

    return vma_image;
}

aristaeus::gfx::vulkan::ImageRaii aristaeus::gfx::vulkan::MemoryManager::create_image(CoreRenderer &core_renderer,
                                                         uint32_t width, uint32_t height,
                                                         vk::Format format, vk::ImageTiling tiling,
                                                         vk::ImageUsageFlags usage,
                                                         VmaAllocationCreateFlagBits allocation_flags,
                                                         VmaMemoryUsage allocation_usage_flags, const std::string &allocation_category,
                                                         uint32_t array_layers, vk::ImageCreateFlags flags, uint32_t mip_levels) {
    ImageRaii image_raii;

    image_raii.reset(create_vma_image(width, height, format, tiling, usage, allocation_flags, 
                                      allocation_usage_flags, allocation_category, array_layers, flags, mip_levels));

    return image_raii;
}

aristaeus::gfx::vulkan::SharedImageRaii aristaeus::gfx::vulkan::MemoryManager::create_shared_image(uint32_t width, uint32_t height, 
                                                                                                   vk::Format format, vk::ImageTiling tiling, 
                                                                                                   vk::ImageUsageFlags usage, 
                                                                                                   VmaAllocationCreateFlagBits allocation_flags,
                                                                                                   VmaMemoryUsage allocation_usage_flags, 
                                                                                                   const std::string &allocation_category,
                                                                                                   uint32_t array_layers, 
                                                                                                   vk::ImageCreateFlags flags, 
                                                                                                   uint32_t mip_levels) {  
    return std::shared_ptr<VmaImage>(create_vma_image(width, height, format, tiling, usage, allocation_flags, 
                                      allocation_usage_flags, allocation_category, array_layers, flags, mip_levels), VmaImageDeleter{});
}

#ifndef NDEBUG
std::string aristaeus::gfx::vulkan::MemoryManager::get_buffer_purpose(VkBuffer buffer_handle) {
    if(m_allocated_buffers.contains(buffer_handle)) {
        return m_allocated_buffers.at(buffer_handle);
    } else {
        return "nonexistent";
    }
}

std::string aristaeus::gfx::vulkan::MemoryManager::get_image_purpose(VkImage image_handle) {
    if (m_allocated_images.contains(image_handle)) {
        return m_allocated_images.at(image_handle);
    }
    else {
        return "nonexistent";
    }
}
#endif
