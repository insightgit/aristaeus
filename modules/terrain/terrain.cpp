//
// Created by bobby on 12/30/21.
//

#include <gfx/vulkan/rasterization_lighting_environment.hpp>

#include "terrain.hpp"

aristaeus::gfx::vulkan::Terrain::Terrain(float height_magnitude, float vertex_resolution,
                                 const std::vector<std::vector<float>> &height_map,
                                 const std::string &terrain_texture_path,
                                 int number_of_textures, ObjectType object_type,
                                 PropInfo &prop_info, const std::string &node_name, const std::string &specular_map_path,
                                 uint32_t number_of_patches, std::optional<vk::CommandBuffer> create_command_buffer) :
                                 SceneGraphNode(node_name, std::string{NODE_TYPE}),
                                 m_terrain_pipeline(utils::get_singleton_safe<CoreRenderer>(),
                                                    get_terrain_pipeline_shader_infos(),
                                                    get_terrain_pipeline_pipeline_info(
                                                    *utils::get_singleton_safe<CoreRenderer>().get_default_rasterization_render_pass()), true),
                                 m_specular_map(specular_map_path),
                                 m_number_of_patches(number_of_patches),
                                 m_height_magnitude(height_magnitude),
                                 m_height_map(height_map),
                                 m_height_map_texture(new Texture(m_height_map, std::unordered_set<std::string>{"texture_terrain_height_map"},
                                                                  utils::get_singleton_safe<CoreRenderer>(),
                                                                  get_image_view_create_info(),
                                                                  get_sampler_create_info(),
                                                                  create_command_buffer)),
                                 m_number_of_textures(number_of_textures),
                                 m_multi_texture_mode(false),
                                 m_vertex_resolution(vertex_resolution),
                                 m_ubos(create_model_ubos()),
                                 m_mesh(setup_mesh(terrain_texture_path, create_command_buffer))

                                #ifndef NDEBUG
                                 ,m_terrain_normal_debug_pipeline(utils::get_singleton_safe<CoreRenderer>(),
                                                                 get_terrain_normal_debug_pipeline_shader_infos(),
                                                                 get_terrain_pipeline_pipeline_info(*utils::get_singleton_safe<CoreRenderer>().get_default_rasterization_render_pass()))
                                #endif
    {

    if(height_map.empty()) {
        return;
    }

    if(m_vertex_resolution < 0) {
        m_vertex_resolution = -m_vertex_resolution;
    }

    if(!m_ubo_content_set) {
        m_ubo_content = {
                .terrain_height_magnitude = m_height_magnitude,
                .override_texture = 0,
                .multi_texture_terrain_mode = m_edge_debug_mode ? 2 : (m_multi_texture_mode ? 1 : 0),
                .light_emitter = 0,
                .light_box_color = glm::vec3(0.0f),
                .tessellation_settings = {
                    .mode = 0,
                    .amount = 64,
                    .camera_max = 128,
                    .camera_min = 4,
                }
        };
    }

    m_ubo_content.object_type = static_cast<int>(object_type);

    add_node_group(std::string{ gfx::vulkan::RasterizationLightingEnvironment::FORWARD_RENDER_NODE_GROUP });
}

aristaeus::gfx::vulkan::Terrain::Terrain(float height_magnitude, float vertex_resolution,
                                 const std::vector<std::vector<float>> &height_map,
                                 const std::map<glm::vec4, std::vector<Image>, utils::Vec4Hash> &terrain_texture_paths_height,
                                 int number_of_textures, ObjectType object_type,
                                 PropInfo &prop_info, const std::string &node_name,
                                 const std::unordered_map<Image, glm::vec4> &terrain_texture_paths_slope,
                                 uint32_t number_of_patches, std::optional<vk::CommandBuffer> create_command_buffer) :
                                SceneGraphNode(node_name, std::string{NODE_TYPE}),
                                m_terrain_pipeline(utils::get_singleton_safe<CoreRenderer>(),
                                                   get_terrain_pipeline_shader_infos(),
                                                   get_terrain_pipeline_pipeline_info(
                                                   *utils::get_singleton_safe<CoreRenderer>().get_default_rasterization_render_pass()), true,
                                                   true),
                                m_number_of_patches(number_of_patches),
                                m_height_magnitude(height_magnitude),
                                m_height_map(height_map),
                                m_height_map_texture(new Texture(m_height_map, std::unordered_set<std::string>{"texture_terrain_height_map"},
                                                                 utils::get_singleton_safe<CoreRenderer>(),
                                                                 AssimpModel::DEFAULT_IMAGE_VIEW_CREATE_INFO,
                                                                 get_sampler_create_info(),
                                                                 create_command_buffer)),
                                m_number_of_textures(number_of_textures),
                                m_multi_texture_mode(true),
                                m_vertex_resolution(vertex_resolution),
                                m_ubos(create_model_ubos()),
                                m_mesh(setup_mesh(terrain_texture_paths_height, terrain_texture_paths_slope,
                                                  create_command_buffer))
#ifndef NDEBUG
                                ,m_terrain_normal_debug_pipeline(utils::get_singleton_safe<CoreRenderer>(),
                                                            get_terrain_normal_debug_pipeline_shader_infos(),
                                                            get_terrain_pipeline_pipeline_info(
                                                            *utils::get_singleton_safe<CoreRenderer>().get_default_rasterization_render_pass()))
#endif
{

    if(m_vertex_resolution < 0) {
        m_vertex_resolution = -m_vertex_resolution;
    }

    if(!m_ubo_content_set) {
        m_ubo_content = {
                .terrain_height_magnitude = m_height_magnitude,
                .override_texture = 0,
                .multi_texture_terrain_mode = m_edge_debug_mode ? 2 : (m_multi_texture_mode ? 1 : 0),
                .light_emitter = 0,
                .light_box_color = glm::vec3(0.0f),
                .tessellation_settings = {
                        .mode = 0,
                        .amount = 64,
                        .camera_max = 128,
                        .camera_min = 4,
                }
        };
    }

    m_ubo_content.object_type = static_cast<int>(object_type);

    setup_terrain_infos(terrain_texture_paths_height, terrain_texture_paths_slope);
}

aristaeus::gfx::vulkan::Terrain::Terrain(
                        float height_magnitude, float vertex_resolution, const glm::ivec2 &height_map_size,
                        const std::map<glm::vec4, std::vector<Image>, utils::Vec4Hash> &terrain_texture_paths_height,
                        int number_of_textures, ObjectType object_type,
                        PropInfo &prop_info, const std::string &node_name,
                        const std::unordered_map<Image, glm::vec4> &terrain_texture_paths_slope,
                        uint32_t number_of_patches, std::optional<vk::CommandBuffer> create_command_buffer) :
        SceneGraphNode(node_name, std::string{NODE_TYPE}),
        m_terrain_pipeline(utils::get_singleton_safe<CoreRenderer>(),
                           get_terrain_pipeline_shader_infos(),
                           get_terrain_pipeline_pipeline_info(
                           *utils::get_singleton_safe<CoreRenderer>().get_default_rasterization_render_pass()), true, true),
        m_number_of_patches(number_of_patches),
        m_height_magnitude(height_magnitude),
        m_height_map_size(height_map_size),
        m_number_of_textures(number_of_textures),
        m_multi_texture_mode(true),
        m_vertex_resolution(vertex_resolution),
        m_ubos(create_model_ubos()),
        m_mesh(setup_mesh(terrain_texture_paths_height, terrain_texture_paths_slope, create_command_buffer))
#ifndef NDEBUG
        ,m_terrain_normal_debug_pipeline(utils::get_singleton_safe<CoreRenderer>(),
                                         get_terrain_normal_debug_pipeline_shader_infos(),
                                         get_terrain_pipeline_pipeline_info(
                                         *utils::get_singleton_safe<CoreRenderer>().get_default_rasterization_render_pass()))
#endif
{
    // TODO(Bobby): undup this code
    if(m_vertex_resolution < 0) {
        m_vertex_resolution = -m_vertex_resolution;
    }

    if(!m_ubo_content_set) {
        m_ubo_content = {
                .terrain_height_magnitude = m_height_magnitude,
                .override_texture = 0,
                .multi_texture_terrain_mode = m_edge_debug_mode ? 2 : (m_multi_texture_mode ? 1 : 0),
                .light_emitter = 0,
                .light_box_color = glm::vec3(0.0f),
                .tessellation_settings = {
                        .mode = 0,
                        .amount = 64,
                        .camera_max = 128,
                        .camera_min = 4,
                }
        };
    }

    m_ubo_content.object_type = static_cast<int>(object_type);

    setup_terrain_infos(terrain_texture_paths_height, terrain_texture_paths_slope);
}

void aristaeus::gfx::vulkan::Terrain::setup_terrain_infos(
        const std::map<glm::vec4, std::vector<Image>, utils::Vec4Hash> &terrain_texture_paths_height,
        const std::unordered_map<Image, glm::vec4> &terrain_texture_paths_slope) {
    int terrain_counter = 0;

    m_ubo_content.vertex_resolution = m_vertex_resolution;
    m_ubo_content.terrain_num_of_textures = m_number_of_textures;
    //m_ubo_content.height_map_size = glm::vec2(m_height_map[0].size(), m_height_map.size());

    for(const std::pair<glm::vec4, std::vector<Image>> height_image : terrain_texture_paths_height) {
        m_ubo_content.terrain_texture_infos[terrain_counter].height = height_image.first;

        if(terrain_texture_paths_slope.contains(height_image.second[0])) {
            m_ubo_content.terrain_texture_infos[terrain_counter].slope =
                    terrain_texture_paths_slope.at(height_image.second[0]);
            m_ubo_content.terrain_texture_infos[terrain_counter].mode = TerrainMode::HAS_HEIGHT_SLOPE;
        } else {
            m_ubo_content.terrain_texture_infos[terrain_counter].slope = glm::vec4(100.0f, 100.0f, 100.0f, 100.0f);
            m_ubo_content.terrain_texture_infos[terrain_counter].mode = TerrainMode::HAS_HEIGHT;
        }

        ++terrain_counter;

        if(terrain_counter >= AssimpModel::MAX_NUM_OF_TERRAIN_TEXTURES) {
            break;
        }
    }

    add_node_group(std::string{ gfx::vulkan::RasterizationLightingEnvironment::FORWARD_RENDER_NODE_GROUP });
}

aristaeus::gfx::vulkan::AssimpMesh aristaeus::gfx::vulkan::Terrain::setup_mesh(
        const std::map<glm::vec4, std::vector<Image>, utils::Vec4Hash> &terrain_images_height,
        const std::unordered_map<Image, glm::vec4> &terrain_images_slope,
        std::optional<vk::CommandBuffer> create_command_buffer) {
    CoreRenderer &core_renderer = utils::get_singleton_safe<CoreRenderer>();
    std::vector<glm::vec3> per_quad_normals;
    std::vector<std::shared_ptr<Texture>> textures;
    std::vector<AssimpMesh::TerrainVertex> vertices = generate_patch_vertices();

    for(std::pair<glm::vec4, std::vector<Image>> terrain_image : terrain_images_height) {
        for(const Image &image : terrain_image.second) {
            textures.push_back(std::make_shared<Texture>(image.image_path,
                                                         std::unordered_set<std::string>{image.image_type},
                                                         core_renderer, create_command_buffer));
        }
    }

    AssimpMesh return_value {vertices, textures, false, core_renderer, 0, 0, 0, 0, 0};

    return return_value;
}

std::vector<std::vector<float>>
aristaeus::gfx::vulkan::Terrain::extract_height_map_from_image(const std::string &height_map_texture_path) {
    int height;
    unsigned char *image_data;
    std::vector<std::vector<float>> return_value;
    int width;
    int color_channels;

    image_data = stbi_load(height_map_texture_path.c_str(), &width, &height, &color_channels, 1);

    if(image_data == nullptr) {
        throw std::runtime_error("[TERRAIN] Couldn't load monochrome height map texture " + height_map_texture_path);
    } else if(color_channels != 1) {
        throw std::runtime_error("[TERRAIN] Invalid monochrome height map texture " + height_map_texture_path);
    }

    for(int y = 0; height > y; ++y) {
        std::vector<float> image_row;

        for(int x = 0; width > x; ++x) {
            int index = (y * width) + x;

            image_row.push_back(image_data[index] / 255.0f);
        }

        return_value.push_back(image_row);
    }

    return return_value;
}

void aristaeus::gfx::vulkan::Terrain::draw(const vk::CommandBuffer &command_buffer, vk::raii::PipelineLayout &pipeline_layout, size_t instance_number) {
    if (m_height_map_texture == nullptr && m_height_map_textures.empty()) {
        return;
    }

    // TODO(Bobby): actually make shadow mapping work with terrain
    RenderPhase &render_phase = *utils::get_singleton_safe<CoreRenderer>().get_current_render_phase();
    bool using_shadow_mapping = render_phase.is_render_pass_depth_only(render_phase.get_current_render_pass());
    if (using_shadow_mapping) {
        return;
    }

    CoreRenderer &core_renderer = utils::get_singleton_safe<CoreRenderer>();
    uint32_t current_frame = core_renderer.get_current_frame();
    RenderContextID render_context_id = core_renderer.get_current_render_context_id();

    m_ubos.write_to_descriptor(&m_ubo_content, sizeof(m_ubo_content), current_frame, render_context_id, core_renderer);

    std::reference_wrapper<vk::raii::PipelineLayout> pipeline_layout_to_use = pipeline_layout;

    if(m_using_terrain_pipeline) {
        assert(!m_height_map_textures.empty());
        assert(m_height_map_texture != nullptr);

        m_height_map_textures[m_height_map_textures.size() / 2] = m_height_map_texture;

        m_terrain_pipeline.bind_to_command_buffer(command_buffer, core_renderer);

        pipeline_layout_to_use = m_terrain_pipeline.get_pipeline_layout();
    }

    command_buffer.bindDescriptorSets(vk::PipelineBindPoint::eGraphics, *pipeline_layout_to_use.get(), 2,
                                      *m_ubos.get_descriptor_to_bind(core_renderer, current_frame,
                                                                     render_context_id), {});

    //std::cout << "cpu offset " << offsetof(AssimpModel::ModelUniformBuffer, tessellation_settings) << "\n";
    m_mesh.draw(command_buffer, get_model_matrix(), false, true,
                pipeline_layout_to_use, m_height_map_textures);

#ifndef NDEBUG
    if(m_normal_debug_mode) {
        m_terrain_normal_debug_pipeline.bind_to_command_buffer(command_buffer, core_renderer);

        m_mesh.draw(command_buffer, get_model_matrix(), false, true,
                    m_terrain_normal_debug_pipeline.get_pipeline_layout(), m_height_map_textures, false);
    }
#endif

}

std::vector<aristaeus::gfx::vulkan::GraphicsPipeline::ShaderInfo>
        aristaeus::gfx::vulkan::Terrain::get_terrain_pipeline_shader_infos() {
    return {
        {
            .code = utils::read_file("environments/vulkan_environment_shaders/terrain/terrain.vert.spv"),
            .name = "terrain.vert",
            .stage = vk::ShaderStageFlagBits::eVertex
        },
        {
            .code = utils::read_file("environments/vulkan_environment_shaders/terrain/terrain.tesc.spv"),
            .name = "terrain.tesc",
            .stage = vk::ShaderStageFlagBits::eTessellationControl
        },
        {
            .code = utils::read_file("environments/vulkan_environment_shaders/terrain/terrain.tese.spv"),
            .name = "terrain.tese",
            .stage = vk::ShaderStageFlagBits::eTessellationEvaluation
        },
        {
            .code = utils::read_file("environments/vulkan_environment_shaders/engine/environment_shader.frag.spv"),
            .name = "environment_shader.frag",
            .stage = vk::ShaderStageFlagBits::eFragment
        }
    };
}

#ifndef NDEBUG
std::vector<aristaeus::gfx::vulkan::GraphicsPipeline::ShaderInfo>
aristaeus::gfx::vulkan::Terrain::get_terrain_normal_debug_pipeline_shader_infos() {
    return {
            {
                    .code = utils::read_file("environments/vulkan_environment_shaders/terrain/terrain.vert.spv"),
                    .name = "terrain.vert",
                    .stage = vk::ShaderStageFlagBits::eVertex
            },
            {
                    .code = utils::read_file("environments/vulkan_environment_shaders/terrain/terrain.tesc.spv"),
                    .name = "terrain.tesc",
                    .stage = vk::ShaderStageFlagBits::eTessellationControl
            },
            {
                    .code = utils::read_file("environments/vulkan_environment_shaders/terrain/debug/terrain_normals.tese.spv"),
                    .name = "terrain_normals.tese",
                    .stage = vk::ShaderStageFlagBits::eTessellationEvaluation
            },
            {
                    .code = utils::read_file("environments/vulkan_environment_shaders/engine/debug/color_normals.geom.spv"),
                    .name = "color_normals.geom",
                    .stage = vk::ShaderStageFlagBits::eGeometry
            },
            {
                    .code = utils::read_file("environments/vulkan_environment_shaders/engine/debug/color_normals.frag.spv"),
                    .name = "color_normals.frag",
                    .stage = vk::ShaderStageFlagBits::eFragment
            }
    };
}
#endif

aristaeus::gfx::vulkan::GraphicsPipeline::PipelineInfo
aristaeus::gfx::vulkan::Terrain::get_terrain_pipeline_pipeline_info(vk::raii::RenderPass &render_pass) {
    return {
            .vertex_input_stage = {{}, TERRAIN_BINDING_DESCRIPTION,
                                   TERRAIN_ATTRIBUTE_DESCRIPTIONS},
            .input_assembly_stage = {{}, vk::PrimitiveTopology::ePatchList, VK_FALSE},
            .tessellation_stage = {{}, 4},
            .viewport_stage = {{}, 1, nullptr, 1, nullptr},
            .rasterization_stage = {{}, VK_FALSE, VK_FALSE, vk::PolygonMode::eFill, vk::CullModeFlagBits::eNone,
                                    vk::FrontFace::eCounterClockwise, {}, {}, {}, {}, 1.0f},
            .multisample_stage = {{}, vk::SampleCountFlagBits::e1, VK_FALSE, 1.0f, nullptr, VK_FALSE, VK_FALSE},
            .depth_stencil_stage = {{}, VK_TRUE, VK_TRUE, vk::CompareOp::eLessOrEqual, VK_FALSE, VK_FALSE, {}, {}, 0.0f, 1.0f},
            .blending_stage = {{}, VK_FALSE, vk::LogicOp::eCopy,
                               RasterizationLightingEnvironment::FORWARD_COLOR_BLEND_ATTACHMENT, {0, 0, 0, 0}},

            .dynamic_stage = {{}, RasterizationLightingEnvironment::FORWARD_DYNAMIC_STATE},
            .push_content_ranges = {},

            .descriptor_bindings = {RasterizationLightingEnvironment::FORWARD_DESCRIPTOR_SET_BINDINGS.cbegin(),
                                    RasterizationLightingEnvironment::FORWARD_DESCRIPTOR_SET_BINDINGS.cend()},

            .sub_pass = utils::get_singleton_safe<CoreRenderer>().get_default_rasterization_subpass(),
            .pipeline_cache_path = "terrain_pipeline_cache",
            
            .render_pass = render_pass
    };
}

std::vector<aristaeus::gfx::vulkan::AssimpMesh::TerrainVertex> aristaeus::gfx::vulkan::Terrain::generate_patch_vertices() {
    std::vector<AssimpMesh::TerrainVertex> vertices;

    for(uint32_t z = 0; m_number_of_patches > z; ++z) {
        for (uint32_t x = 0; m_number_of_patches > x; ++x) {
            glm::vec2 height_map_size;

            if (m_height_map.empty()) {
                height_map_size = m_height_map_size.value();
            } else {
                height_map_size = glm::vec2{ m_height_map[0].size(), m_height_map.size() };
            }

            for(int i = 0; 4 > i; ++i) {
                AssimpMesh::TerrainVertex current_vertex {};

                glm::vec2 general_coords {x + (i % 2), z + (i / 2)};

                current_vertex.height_map_texture_coords = general_coords / glm::vec2(m_number_of_patches);

                if(current_vertex.height_map_texture_coords.x > 1.0f ||
                   current_vertex.height_map_texture_coords.x < 0.0f ||
                   current_vertex.height_map_texture_coords.y > 1.0f ||
                   current_vertex.height_map_texture_coords.y < 0.0f) {
                    assert(false);
                }

                vertices.push_back(current_vertex);
            }
        }
    }

    return vertices;
}

aristaeus::gfx::vulkan::AssimpModel::Wave
aristaeus::gfx::vulkan::Terrain::generate_new_wave(const WaveProperties &wave_properties,
                                                   float birth_period) {
    std::uniform_real_distribution<double> real_distribution {0, 1};

    float amplitude = wave_properties.median_amplitude / 2 +
                      (real_distribution(CoreRenderer::mersenne_twister_rng) * (wave_properties.median_amplitude));
    float sharpness = wave_properties.median_sharpness / 2 +
                      (real_distribution(CoreRenderer::mersenne_twister_rng) * (wave_properties.median_sharpness));
    float wavelength = wave_properties.median_wavelength / 2 +
                       (real_distribution(CoreRenderer::mersenne_twister_rng) * (wave_properties.median_wavelength));

    float frequency = 2 / wavelength;
    float sharpness_term = fmin(fmax(sharpness, 1), 0) / (frequency * amplitude * wave_properties.count);

    return {
        .wave_active = 1,
        .amplitude = amplitude,
        .birth_period = birth_period,
        .center_wave = wave_properties.direction,
        .death_period = 1.0f,
        .sharpness = sharpness_term,
        .wavelength = wavelength,
        .t_time = 0.0f,
    };
}

void aristaeus::gfx::vulkan::Terrain::update_waves(const WaveProperties &wave_properties) {
    double elapsed_seconds_count;

    if(m_wave_last_time_point.has_value()) {
        std::chrono::time_point now_time = std::chrono::system_clock::now();
        std::chrono::duration<double> elapsed_seconds = now_time - m_wave_last_time_point.value();

        elapsed_seconds_count = elapsed_seconds.count();
        m_wave_last_time_point = {std::chrono::system_clock::now()};
    } else {
        m_wave_last_time_point = {std::chrono::system_clock::now()};
        elapsed_seconds_count = 0;
    }

    for(int i = 0; m_ubo_content.active_wave_count > i; ++i) {
        // update existing waves
        AssimpModel::Wave &current_wave = m_ubo_content.active_waves[i];

        // could probably simplify this
        // this is probably wrong
        float speed = sqrtf(utils::GRAVITY_CONSTANT * (2 * utils::PI / current_wave.wavelength));
        float phase_constant = (speed * 2) / current_wave.wavelength;
        float wave_lifecycle = (2 * utils::PI / phase_constant) + current_wave.birth_period;
        /*float wave_lifecycle = (2 * utils::PI * current_wave.wavelength);

        wave_lifecycle /= 2 * speed;*/

        current_wave.t_time += elapsed_seconds_count;

        if(current_wave.t_time > wave_lifecycle) {
            // wave is ded: generate a new one and replace it
            // maybe lerp between two waves?
            // have a death and birth period (robust soln)
            // or
            // identify a common point between the waves to jump
            // or
            // is this noticable

            float death_period_t = (current_wave.t_time - wave_lifecycle) / current_wave.death_period;

            if(m_ubo_content.pending_waves[i].wave_active == 0) {
                m_ubo_content.pending_waves[i] = generate_new_wave(wave_properties,
                                                                   m_ubo_content.active_waves[i].death_period);
            }

            if(death_period_t > 1.0f) {
                m_ubo_content.active_waves[i] = m_ubo_content.pending_waves[i];
                m_ubo_content.pending_waves[i].wave_active = 0;
            }
        }
    }

    // generate new waves if needed and spaced out properly
    // initializing new waves
    if(wave_properties.count > m_ubo_content.active_wave_count) {
        for(int i = m_ubo_content.active_wave_count; wave_properties.count > i; ++i) {
            m_ubo_content.active_waves[i] = generate_new_wave(wave_properties, 0.0f);
        }

        m_ubo_content.active_wave_count = wave_properties.count;
    }
}

void aristaeus::gfx::vulkan::Terrain::set_chunked_settings(const std::optional<float> &chunked_smoothing_percent,
                                                           const std::optional<glm::ivec2> &chunk_position) {
    if(chunked_smoothing_percent.has_value() && chunk_position.has_value()) {
        m_ubo_content.chunked_blend_threshold = chunked_smoothing_percent.value();
        m_ubo_content.chunk_position = chunk_position.value() + glm::ivec2(1);
        m_ubo_content.is_chunked_terrain = 1;
    } else {
        m_ubo_content.is_chunked_terrain = 0;
    }
}

float aristaeus::gfx::vulkan::Terrain::get_height_value(const glm::ivec2 &height_map_coords) {
    //throw std::runtime_error("get_height_value not implemented");
    return (m_height_map[height_map_coords.y][height_map_coords.x] - 0.5) * 2;
}

/*float aristaeus::gfx::vulkan::Terrain::get_slope(const glm::vec2 &world_position) {
    assert(m_height_map.size() == m_height_map[0].size());

    glm::vec2 height_map_value =
            ((world_position - glm::vec2(m_mesh.get_position())) / m_vertex_resolution) *
            glm::vec2(m_height_map.size());

    float center_value = m_height_map[height_map_value.y][height_map_value.x];
    float total_slope = 0;

    for(int y = -1; 1 > y; ++y) {
        for(int x = -1; 1 > x; ++x) {
            if(x == 0 && y == 0) {
                continue;
            } else {
                float current_value = m_height_map[height_map_value.y + y][height_map_value.x + x];

                total_slope += current_value - center_value;
            }
        }
    }

    return total_slope / 8;
}*/


