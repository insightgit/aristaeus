#include "class_db.hpp"

#include "../gfx/scene_graph_node.hpp"
#include "../utils.hpp"

#include "../../libraries/include/imgui_stdlib.h"

const std::unordered_set<std::string> aristaeus::core::ClassDB::IGNORED_PROPERTY_NAMES {
	"comment"
};

template<>
aristaeus::core::ClassDB *aristaeus::core::Singleton<aristaeus::core::ClassDB>::instance = nullptr;

void aristaeus::core::ClassDB::register_base_types() {
	register_type<char>(basic_type_json_parse_function<char>());
	register_type<int8_t>(basic_type_json_parse_function<int8_t>(), get_integer_ui_functions<int8_t>(ImGuiDataType_S8));
	register_type<int16_t>(basic_type_json_parse_function<int16_t>(), get_integer_ui_functions<int16_t>(ImGuiDataType_S16));
	register_type<int32_t>(basic_type_json_parse_function<int32_t>(), get_integer_ui_functions<int32_t>(ImGuiDataType_S32));
	register_type<int64_t>(basic_type_json_parse_function<int64_t>(), get_integer_ui_functions<int64_t>(ImGuiDataType_S64));
	register_type<uint8_t>(basic_type_json_parse_function<uint8_t>(), get_integer_ui_functions<uint8_t>(ImGuiDataType_U8));
	register_type<uint16_t>(basic_type_json_parse_function<uint16_t>(), get_integer_ui_functions<uint16_t>(ImGuiDataType_U16));
	register_type<uint32_t>(basic_type_json_parse_function<uint32_t>(), get_integer_ui_functions<uint32_t>(ImGuiDataType_U32));
	register_type<uint64_t>(basic_type_json_parse_function<uint64_t>(), get_integer_ui_functions<uint64_t>(ImGuiDataType_U64));

	register_type<float>(basic_type_json_parse_function<float>(), get_integer_ui_functions<float>(ImGuiDataType_Float));
	register_type<double>(basic_type_json_parse_function<double>(), get_integer_ui_functions<double>(ImGuiDataType_Double));


	UiFunctions<bool> bool_ui_functions{ .parse_function = [](bool &persistent_state, bool display_ui, const std::optional<std::string> &label_hint) {
		nlohmann::json bool_json;

		if(display_ui) {
			std::string label = label_hint.has_value() ? label_hint.value() : "bool value";

			ImGui::Checkbox(label.c_str(), &persistent_state);
		}

		bool_json.push_back(persistent_state);

		return bool_json;
	}, .constructor_function = []() {return false; } };

	register_type<bool>(basic_type_json_parse_function<bool>(), bool_ui_functions);

	UiFunctions<std::string> string_ui_functions{ 
		.parse_function = [](std::string &persistent_state, bool display_ui, const std::optional<std::string> &label_hint) {
			if(display_ui) {
				std::string label = label_hint.has_value() ? label_hint.value() : "string value";

				ImGui::InputText(label.c_str(), &persistent_state);
			}

			nlohmann::json string_json;

			string_json.push_back(persistent_state);

			return string_json;
		}, .constructor_function = []() {return std::string(""); } };

	register_type<std::string>(basic_type_json_parse_function<std::string>(), string_ui_functions);

	register_type<glm::quat>(utils::get_quat_json, get_glm_vector_ui_functions<glm::quat>(ImGuiDataType_Float, 4));
	register_type<glm::vec4>(utils::get_vec4_json, get_glm_vector_ui_functions<glm::vec4>(ImGuiDataType_Float, 4));
	register_type<glm::vec3>(utils::get_vec3_json, get_glm_vector_ui_functions<glm::vec3>(ImGuiDataType_Float, 3));
	register_type<glm::vec2>(utils::get_vec2_json, get_glm_vector_ui_functions<glm::vec2>(ImGuiDataType_Float, 2));
	register_type<glm::ivec4>(utils::get_ivec4_json, get_glm_vector_ui_functions<glm::ivec4>(ImGuiDataType_S32, 4));
	register_type<glm::ivec3>(utils::get_ivec3_json, get_glm_vector_ui_functions<glm::ivec3>(ImGuiDataType_S32, 3));
	register_type<glm::ivec2>(utils::get_ivec2_json, get_glm_vector_ui_functions<glm::ivec2>(ImGuiDataType_S32, 2));

	register_type<glm::mat4>(utils::get_mat_json<glm::mat4>);
	register_type<glm::mat3>(utils::get_mat_json<glm::mat3>);
}

void aristaeus::core::ClassDB::register_type_unsafe(StdTypeInfoRef type_info, ParseFunction parse_function, DeleterFunction deleter_function, std::optional<InternalUiFunctions> ui_functions) {
	if (!m_type_infos.contains(type_info)) {
		m_type_infos.insert({ type_info, TypeInfo{.parse_function = parse_function, .deleter_function = deleter_function, .ui_functions = ui_functions} });
	}
}

void aristaeus::core::ClassDB::parse_properties_on_type(void *type_instance, const ClassInfo &class_info, const nlohmann::json::value_type &node_json) {
	std::unordered_set<std::string> set_properties;
	std::unordered_set<std::string> unset_properties;

	// step 1: set properties on scene_graph_node

	for (std::pair<std::string, PropertyInfo> class_property : class_info.properties) {
		unset_properties.insert(class_property.first);
	}

	for (auto &property_json : node_json.items()) {
		std::string current_property_name = property_json.key();
		if (IGNORED_PROPERTY_NAMES.contains(current_property_name)) {
			continue;
		}
		else if (!class_info.properties.contains(current_property_name)) {
			std::cerr << "Skipping non-existent property " << current_property_name << " for " << class_info.class_type_name << "\n";
		}
		else {
			StdTypeInfoRef property_type = class_info.properties.at(current_property_name).type;
			void *property_instance;
			DeleterFunction deleter_function;

			if (m_type_infos.contains(property_type)) {
				ParseFunction type_parse_function = m_type_infos.at(property_type).parse_function;
				deleter_function = m_type_infos.at(property_type).deleter_function;

				property_instance = type_parse_function(property_json.value());
			}
			else if(m_class_infos.contains(property_type) || m_class_ptr_info_aliases.contains(property_type)) {
				assert(!m_class_ptr_info_aliases.contains(property_type) || m_class_infos.contains(m_class_ptr_info_aliases.at(property_type)));

				ClassInfo property_class_info = m_class_infos.contains(property_type) ? m_class_infos.at(property_type) : m_class_infos.at(m_class_ptr_info_aliases.at(property_type));

				property_instance = property_class_info.constructor_function();
				deleter_function = property_class_info.deleter_function;

                reinterpret_cast<gfx::SceneGraphNode*>(property_instance)->call_event<void*>(
                        std::string{gfx::SceneGraphNode::BEFORE_PARSING_EVENT}, nullptr);

				parse_properties_on_type(property_instance,
					property_class_info, property_json.value());

				for (auto& property_json : property_json.value().items()) {
					std::cout << "property json key: " << property_json.key() << "\n";
				}
			}
			else {
				throw std::runtime_error(property_type.get().name() + std::string(" is not recognized as a registered type or class"));
			}

			class_info.properties.at(current_property_name).setter(type_instance, property_instance);

			if (m_type_infos.contains(property_type)) {
				deleter_function(property_instance);
			}
			else {
				reinterpret_cast<gfx::SceneGraphNode*>(property_instance)->call_event<void*>(
                                                                  std::string{gfx::SceneGraphNode::PARSING_DONE_EVENT},
                                                                  nullptr);
			}

			assert(!set_properties.contains(current_property_name));
			assert(unset_properties.contains(current_property_name));

			set_properties.insert(current_property_name);
			unset_properties.erase(current_property_name);
		}
	}

	// step 2: verify all required properties have been set on scene_graph_node and set all remaining
	// non-required ones to an instance from their default constructor.
	std::vector<std::string> unsatisifed_requirements;

	for (const std::string& required_property : class_info.required_properties) {
		if (unset_properties.contains(required_property)) {
			unsatisifed_requirements.push_back(required_property);
		}
	}

	if (!unsatisifed_requirements.empty()) {
		std::string error_string = "Missing the following required properties for " + std::string(class_info.class_type_name) + ": ";

		for (size_t i = 0; unsatisifed_requirements.size() > i; ++i) {
			error_string += unsatisifed_requirements[i];

			if (i != 0 && i != unsatisifed_requirements.size() - 1) {
				error_string += ", ";
			}
		}

		throw std::runtime_error(error_string);
	}

	for (const std::string &unset_property : unset_properties) {
		std::function<void* ()> default_constructor_function =
			class_info.properties.at(unset_property).default_constructor_function.value();

		void *property_instance = default_constructor_function();

		class_info.properties.at(unset_property).setter(type_instance, property_instance);

		const TypeInfo &type_info = m_type_infos.at(class_info.properties.at(unset_property).type);

		type_info.deleter_function(property_instance);
	}

	// step 3: repeat steps 2 and 3 as needed for parent classes
	for (StdTypeInfoRef type_info_ref : class_info.parent_class_types) {
		if (!m_class_infos.contains(type_info_ref)) {
			throw std::runtime_error(std::string("One of ") + std::string(class_info.class_type_name) + "'s parents, " + std::string(type_info_ref.get().name()) + " is not registered as a class!");
		}
		else {
			parse_properties_on_type(type_instance, m_class_infos.at(type_info_ref), node_json);
		}
	}
}

aristaeus::gfx::SceneGraphNode *aristaeus::core::ClassDB::parse_node(const nlohmann::json::value_type &node_json) {
	std::string node_type = node_json.at("type");

	if (!m_class_type_names.contains(node_type)) {
		throw UnrecognizedNodeTypeException("Need to register class with register_class!");
	}

	const ClassInfo &class_info = m_class_infos.at(m_class_type_names.at(node_type));

	if (!class_info.is_node) {
		throw std::runtime_error("Class " + std::string(class_info.class_type_name) + " is not a node!");
	}

	auto scene_graph_node = static_cast<gfx::SceneGraphNode*>(class_info.constructor_function());

    scene_graph_node->call_event<void*>(std::string{gfx::SceneGraphNode::BEFORE_PARSING_EVENT}, nullptr);

	parse_properties_on_type(scene_graph_node, class_info, node_json);

	scene_graph_node->call_event<void*>(std::string{gfx::SceneGraphNode::PARSING_DONE_EVENT}, nullptr);

	return scene_graph_node;
}

std::vector<std::string_view> aristaeus::core::ClassDB::get_ui_initializable_classes() const {
	std::vector<std::string_view> class_type_names;

	for (const std::pair<const StdTypeInfoRef, ClassInfo> &class_info : m_class_infos) {
		if (class_info.second.is_ui_initializable) {
			class_type_names.push_back(class_info.second.class_type_name);
		}
	}

	return class_type_names;
}

nlohmann::json::value_type aristaeus::core::ClassDB::call_ui_functions(const std::string& node_path, StdTypeInfoRef type_info, bool get_complete_json) {
	ClassInfo& class_info = m_class_infos.at(type_info);
	nlohmann::json return_type;

	if (!class_info.is_node) {
		throw std::runtime_error("Node type specified isn't node");
	}

	if (get_complete_json || ImGui::TreeNode(class_info.class_type_name.data())) {
		if (!m_node_ui_persistent_data.contains(node_path)) {
			std::vector<PersistentData> node_persistent_data;

			for (std::pair<const std::string, PropertyInfo> &property_pair : class_info.properties) {
				InternalUiFunctions &ui_functions = m_type_infos.at(property_pair.second.type).ui_functions.value();
				void *property_persistent_data;

				if (property_pair.second.default_constructor_function.has_value() && ui_functions.persistent_state_type == property_pair.second.type) {
					property_persistent_data = property_pair.second.default_constructor_function.value()();
				}
				else {
					property_persistent_data = ui_functions.constructor_function();
				}

				node_persistent_data.push_back(PersistentData{
					.data = property_persistent_data,
					.data_type = property_pair.second.type,
					.name = property_pair.second.name
				});
			}

			m_node_ui_persistent_data.insert({ node_path, node_persistent_data });
		}

		for (const PersistentData &property_pair : m_node_ui_persistent_data.at(node_path)) {
			nlohmann::json property_json{};

			const TypeInfo &type_info = m_type_infos.at(property_pair.data_type);

			property_json[property_pair.name] = type_info.ui_functions.value().parse_function(property_pair.data, !get_complete_json, property_pair.name);

			return_type.merge_patch(property_json);
		}

		for (StdTypeInfoRef parent_type_info : class_info.parent_class_types) {
			nlohmann::json::value_type parent_json = call_ui_functions(node_path + ": " + parent_type_info.get().name(), parent_type_info, get_complete_json);

			return_type.merge_patch(parent_json);
		}

		if (!get_complete_json) {
			ImGui::TreePop();
		}
	}

	return return_type;
}

std::function<void(aristaeus::core::EventObject&, void*)> aristaeus::core::ClassDB::get_event_callback_wrapper(
        const EventCallbackInfo &event_callback_info) {
    return [event_callback_info](
            core::EventObject &event_object,
            void *argument) {
        gfx::SceneGraphNode &scene_graph_node =
                utils::compile_checked_down_cast<core::EventObject, gfx::SceneGraphNode>(&event_object);

        event_callback_info.callback_function(scene_graph_node, argument);
    };
}
