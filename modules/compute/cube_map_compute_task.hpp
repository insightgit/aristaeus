#ifndef ARISTAEUS_CUBE_MAP_COMPUTE_TASK
#define ARISTAEUS_CUBE_MAP_COMPUTE_TASK

#include <gfx/vulkan/image_buffer_raii.hpp>

#include "compute_task.hpp"

namespace aristaeus::gfx::vulkan::compute {
	class CubeMapComputeTask : public ComputeTask {
	public:
		struct OutputInfo {
			ImageRaii image;
			std::vector<vk::raii::ImageView> image_views;
			vk::raii::Sampler sampler;
		};

		[[nodiscard]] std::optional<OutputInfo> get_output_info() {
			if (get_compute_job_status() == JobStatus::Finished) {
				return std::move(m_output_info);
			}
			else {
				return {};
			}
		}

		vk::DeviceSize get_output_size() override;

		bool load_from_saved_output(vk::CommandBuffer &command_buffer, BufferRaii &buffer, vk::DeviceSize buffer_offset,
                                    const std::vector<glm::ivec3> &file_image_data)override;
		void save_output(vk::CommandBuffer &command_buffer, BufferRaii &buffer, vk::DeviceSize buffer_offset)override;
	protected:
		static const glm::mat4 PROJECTION_MATRIX;

		CubeMapComputeTask(size_t ubo_size, uint32_t number_of_storage_images, uint32_t number_of_ubos,
                           uint32_t number_of_samplers, ComputePipelineInfo compute_pipeline_info,
                           const std::string &compute_task_type, uint32_t number_of_dispatches_done_per_job,
                           const glm::ivec2 &image_size, bool mipmapping_enabled,
                           std::optional<uint32_t> max_num_of_mipmaps = {},
                           const std::optional<std::string> &cache_path = {},
						   const std::vector<std::string> &input_file_paths = {});

        OutputInfo create_output_data(const glm::ivec2 &image_size, const std::string &output_texture_name,
                                      bool mipmapping_enabled, std::optional<uint32_t> max_num_of_mipmaps = {});

		glm::mat4 get_current_view_transformation(int index);
	
		OutputInfo m_output_info;
	};
}

#endif