//
// Created by bobby on 1/21/22.
//

#include "gpu_perlin_noise_procedural_terrain.hpp"
#include "gpu_perlin_noise_terrain.hpp"

#include <gfx/vulkan/lighting_environment.hpp>

#define STB_IMAGE_WRITE_IMPLEMENTATION
#include <stb_image_write.h>


aristaeus::gfx::vulkan::compute::GPUPerlinNoiseProceduralTerrain::GPUPerlinNoiseProceduralTerrain(
        const TerrainInfo &terrain_info, const WaterInfo &water_info, std::mt19937 &rng,
        const glm::ivec2 &height_map_resolution,
        const std::unordered_map<glm::vec2, glm::vec2, utils::Vec2Hash> &predetermined_gradient_vectors,
        std::optional<std::reference_wrapper<GraphicsPipeline>> environment_pipeline,
        bool write_noise_texture, bool draw_water_noise_texture, std::optional<glm::vec2> perlin_noise_start,
        std::optional<vk::CommandBuffer> create_command_buffer) :
        ProceduralTerrain(terrain_info, water_info, rng, height_map_resolution, predetermined_gradient_vectors,
                          environment_pipeline, write_noise_texture, draw_water_noise_texture, perlin_noise_start,
                          create_command_buffer),
        m_perlin_noise_info(GPUPerlinNoise::PerlinNoiseInfo{ {}, get_height_map_resolution(),
                             get_terrain_info().number_of_perlin_octaves, 1.0f, 0.5f,
                             get_terrain_info().perlin_persistence, 2.0f }),
        m_height_map_job(generate_height_map_job(perlin_noise_start)) {
}

std::shared_ptr<aristaeus::gfx::vulkan::compute::GPUPerlinNoise>
aristaeus::gfx::vulkan::compute::GPUPerlinNoiseProceduralTerrain::generate_height_map_job(
                                                                          std::optional<glm::vec2> perlin_noise_start) {

    glm::vec2 random_start;

    if(perlin_noise_start.has_value()) {
        random_start = perlin_noise_start.value();
    } else {
        std::uniform_real_distribution<float> random_num_generator_distribution(0.0f, 1.0f);

        random_start = glm::vec2{ random_num_generator_distribution(m_random_num_generator) };
    }

    m_perlin_noise_info.random_start = random_start;

    std::shared_ptr<GPUPerlinNoise> height_map_job = std::make_shared<GPUPerlinNoise>(m_perlin_noise_info);

    utils::get_singleton_safe<ComputeManager>().add_compute_task(std::static_pointer_cast<ComputeTask>(height_map_job));

    return height_map_job;
}

aristaeus::gfx::vulkan::Terrain
*aristaeus::gfx::vulkan::compute::GPUPerlinNoiseProceduralTerrain::terrain_from_terrain_info(
        const ProceduralTerrain::TerrainInfo &terrain_info,
        std::optional<vk::CommandBuffer> create_command_buffer) {
    if (terrain_info.multi_textured_terrain) {
        TerrainInfo terrain_info = get_terrain_info();

        return new GPUPerlinNoiseTerrain(terrain_info.height_magnitude, terrain_info.vertex_resolution,
                           *m_height_map_job, terrain_info.texture_paths_height, terrain_info.number_of_textures,
                           Terrain::ObjectType::Object, terrain_info.prop_info, get_terrain_node_path(),
                           terrain_info.texture_paths_slope, terrain_info.number_of_patches, create_command_buffer);
    }
    else {
        throw std::runtime_error("non multi textured terrain not implemented for compute yet");
    }
}
