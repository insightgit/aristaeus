//
// Created by bobby on 8/25/21.
//

#ifndef ARISTAEUS_OGL_SCENE_HPP
#define ARISTAEUS_OGL_SCENE_HPP

#include <fstream>
#include <memory>

#include <../../libraries/include/json.hpp>


#include "module_loader.hpp"
#include "../gfx/vulkan/perspective_camera_environment.hpp"
#include "../gfx/vulkan/lighting_environment.hpp"

#include "../gfx/vulkan/geometry/cylinder.hpp"
#include "../gfx/vulkan/geometry/rectangular_prism.hpp"
#include "../gfx/vulkan/geometry/sphere.hpp"
#include "../gfx/vulkan/geometry/torus.hpp"

#include "../gfx/lru_cache_node.hpp"

#include "../engine_module_entry_point.hpp"

#include "../gfx/scene_graph_node.hpp"

#include "../gfx/gui/scene_tree_gui.hpp"

namespace aristaeus::core {
    class SceneLoader : public core::Singleton<SceneLoader>, public core::EventObject {
    public:
        static constexpr std::string_view SCENE_LOADING_EVENT = "on_scene_loaded";

        SceneLoader() {
            add_event<void*>(std::string{SCENE_LOADING_EVENT});
        }

        ~SceneLoader() = default;

        void load_environment_json(const std::string &environment_path, const std::string &environment_name);

        gfx::vulkan::PerspectiveCameraEnvironment init_environment_json(float aspect_ratio, const vk::Viewport &viewport, const vk::Rect2D &viewport_scissor,
                                                                        const std::unordered_set<std::string> &vulkan_device_extensions_loaded);

        std::unordered_set<std::string> get_desired_vulkan_extensions() const;

        [[nodiscard]] std::string get_environment_path_for_file(const std::string &environment_file) const {
            return (std::filesystem::path(m_environment_path) / environment_file).string();
        }

        [[nodiscard]] std::string get_environment_path() const {
            return m_environment_path;
        }

        std::shared_ptr<gfx::SceneGraphNode> &get_scenegraph_node_root() {
            return m_scenegraph_node_root;
        }
    private:
        static constexpr std::string_view PREFAB_NODE_TYPE = "prefab";
        static constexpr size_t PREFAB_CACHE_CAPACITY = 100;

        [[nodiscard]] glm::vec2 get_vec2_json(const nlohmann::json::value_type& vec2_json) const {
            return { vec2_json[0], vec2_json[1] };
        }

        [[nodiscard]] glm::vec3 get_vec3_json(const nlohmann::json::value_type& vec3_json) const {
            return { vec3_json[0], vec3_json[1], vec3_json[2] };
        }

        [[nodiscard]] glm::vec4 get_vec4_json(const nlohmann::json::value_type& vec4_json) const {
            return { vec4_json[0], vec4_json[1], vec4_json[2], vec4_json[3] };
        }

        [[nodiscard]] glm::quat get_quat_json(const nlohmann::json::value_type& quat_json) const {
            return { quat_json[0], quat_json[1], quat_json[2], quat_json[3] };
        }

        void discover_modules(const nlohmann::json::value_type &module_json);

        gfx::vulkan::PerspectiveCameraEnvironment parse_perspective_camera_environment(
                const nlohmann::json::value_type &camera_environment_json, gfx::vulkan::CoreRenderer &core_renderer,
                float aspect_ratio);

        std::vector<glm::vec2> parse_vec2_array(const nlohmann::json::value_type &root_json,
                                                const std::string &json_key);

        gfx::vulkan::LightingEnvironment::Fog parse_fog_info(const nlohmann::json::value_type &fog_json);

        void parse_and_create_prefab(const nlohmann::json::value_type &node_sub_json,
                                     const std::string &node_path);

        void parse_and_create_node(nlohmann::json::value_type &prefab_node_sub_json,
                                   const std::string &prefab_node_path,
                                   const std::string &root_path = std::string{gfx::SceneGraphNode::NODE_CHILD_SEPARATOR});

        size_t get_number_of_point_light_shadows();

        void call_ready_on_scene_init();

        static const std::array<std::string, 14> REQUIRED_WATER_INFO;

        nlohmann::json m_environment_json;
        std::string m_environment_name;
        std::string m_environment_path;

        std::string m_default_lighting_environment;

        core::ClassDB m_class_db;

        std::shared_ptr<gfx::SceneGraphNode> m_scenegraph_node_root;
        ModuleLoader m_module_loader{"runtime_modules"};
        LRUCache<std::string, nlohmann::json> m_prefab_cache{PREFAB_CACHE_CAPACITY, false};
    };
}

#endif //ARISTAEUS_OGL_SCENE_HPP
