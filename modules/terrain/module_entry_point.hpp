#ifndef ARISTAEUS_TERRAIN_MODULE_ENTRY_POINT_HPP
#define ARISTAEUS_TERRAIN_MODULE_ENTRY_POINT_HPP

#include <core/class_db.hpp>

extern "C" void terrain_module_entry_point(const nlohmann::json &module_json);

#endif //ARISTAEUS_TERRAIN_MODULE_ENTRY_POINT_HPP