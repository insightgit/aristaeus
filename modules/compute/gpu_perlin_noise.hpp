//
// Created by bobby on 16/04/23.
//

#ifndef ARISTAEUS_GPU_PERLIN_NOISE_HPP
#define ARISTAEUS_GPU_PERLIN_NOISE_HPP

#include <glm/glm.hpp>
#include <unordered_map>

#include "compute_pipeline.hpp"
#include "single_image_compute_task.hpp"
#include <utils.hpp>
#include <gfx/vulkan/texture.hpp>

namespace aristaeus::gfx::vulkan::compute {
    class GPUPerlinNoise : public SingleImageComputeTask {
    public:
        struct PerlinNoiseInfo {
            alignas(4) glm::vec2 random_start;
            alignas(8) glm::ivec2 resolution;
            alignas(4) int octaves;
            alignas(4) float amplitude;
            alignas(4) float frequency;
            alignas(4) float persistence;
            alignas(4) float frequency_increase;
        };

        GPUPerlinNoise(
                const PerlinNoiseInfo &perlin_noise_input_info,
                const std::unordered_map<glm::vec2, glm::vec2, utils::Vec2Hash> &predetermined_gradient_vectors = {},
                bool is_in_preload_stage = false, const std::optional<std::string> &cache_path = {});

        [[nodiscard]] PerlinNoiseInfo get_perlin_noise_info() {
            return m_ubo_data.perlin_noise_info;
        }

        void dispatch(vk::CommandBuffer &command_buffer)override;
    private:
        static constexpr uint32_t WORK_GROUP_X = 16;
        static constexpr uint32_t WORK_GROUP_Y = 16;
        static constexpr uint32_t WORK_GROUP_SIZE = WORK_GROUP_X * WORK_GROUP_Y * 1; // reference perlin_noise.comp

        static constexpr uint32_t MAX_NUM_OF_PERLIN_WORK_PER_SUBMIT = 10; // TODO(Bobby): put this work into one invocation

        static constexpr uint32_t PERLIN_LUT_PADDING = 16 / sizeof(int);

        struct PerlinNoiseUniformBuffer {
            alignas(16) int perlin_lookup_table[256 * PERLIN_LUT_PADDING];

            // this alignment might be alignas(4) not alignas(16)
            alignas(16) PerlinNoiseInfo perlin_noise_info;
        } m_ubo_data;

        VulkanPipeline::ShaderInfo get_compute_perlin_shader_info() {
            return {
                .code = utils::read_file("environments/vulkan_environment_shaders/compute/perlin_noise.comp.spv"),
                .name = "perlin_noise.comp",
                .stage = vk::ShaderStageFlagBits::eCompute
            };
        }

        PerlinNoiseUniformBuffer get_ubo_data(const PerlinNoiseInfo &perlin_noise_info) {
            assert(utils::PERLIN_LOOKUP_TABLE.size() == 256);

            PerlinNoiseUniformBuffer ubo_data{.perlin_noise_info = perlin_noise_info};

            for (size_t i = 0; utils::PERLIN_LOOKUP_TABLE.size() > i; ++i) {
                ubo_data.perlin_lookup_table[i * PERLIN_LUT_PADDING] = utils::PERLIN_LOOKUP_TABLE[i];
            }

            return ubo_data;
        }

        ComputePipelineInfo get_compute_pipeline_info(bool is_in_preload_stage) {
            return ComputePipelineInfo{ get_compute_perlin_shader_info(), std::string("perlin_compute"), std::vector<vk::PushConstantRange>{}, true, is_in_preload_stage };
        }

        // returns the current dispatch id
        void update_descriptor_set();

        std::unordered_map<glm::vec2, glm::vec2, utils::Vec2Hash> m_predetermined_gradient_vectors;

        std::mutex m_descriptor_count_mutex;
        size_t previous_frame = -1;
        size_t current_descriptor_set_count = 0;
    };
}

#endif //ARISTAEUS_GPU_PERLIN_NOISE_HPP
