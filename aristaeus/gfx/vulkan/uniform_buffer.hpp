//
// Created by bobby on 6/4/22.
//

#ifndef ARISTAEUS_UNIFORM_BUFFER_HPP
#define ARISTAEUS_UNIFORM_BUFFER_HPP

#include <unordered_map>

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "vulkan_raii.hpp"

#include "render_phase.hpp"
#include "image_buffer_raii.hpp"
#include "../../utils.hpp"

namespace aristaeus::gfx::vulkan {
    class CoreRenderer;

    class UniformBuffer {
    public:
        UniformBuffer(CoreRenderer &core_renderer, vk::ShaderStageFlags active_shader_stages, size_t buffer_size);

        vk::raii::DescriptorSet &get_descriptor_to_bind(CoreRenderer &core_renderer, uint32_t frame_number,
                                                        RenderContextID render_context_id);

        void write_to_all_descriptors(const void *data, size_t data_size, RenderContextID render_context_id,
                                      CoreRenderer &core_renderer);

        void write_to_descriptor(const void *data, size_t data_size, uint32_t frame_number,
                                 RenderContextID render_context_id, CoreRenderer &core_renderer);
    private:
        static constexpr vk::DescriptorPoolSize POOL_SIZE {vk::DescriptorType::eUniformBuffer,
                                                           static_cast<uint32_t>(utils::vulkan::MAX_FRAMES_IN_FLIGHT)};

        void copy_data_to_buffer(const void *data, size_t data_size, CoreRenderer &core_renderer);
        vk::raii::DescriptorSetLayout create_descriptor_set_layout(vk::raii::Device &logical_device,
                                                                   vk::ShaderStageFlags active_shader_stages);

        vk::raii::DescriptorPool create_descriptor_pool(vk::raii::Device &logical_device) {
            return {logical_device, {{vk::DescriptorPoolCreateFlagBits::eFreeDescriptorSet},
                     utils::vulkan::MAX_FRAMES_IN_FLIGHT, POOL_SIZE}};
        }

        std::unordered_map<RenderContextID, vk::raii::DescriptorPool> create_init_descriptor_pools(
                vk::raii::Device &logical_device, RenderContextID default_render_context_id) {
            std::unordered_map<RenderContextID, vk::raii::DescriptorPool> return_value;

            return_value.insert({default_render_context_id, create_descriptor_pool(logical_device)});

            return return_value;
        }

        std::array<vk::raii::DescriptorSet, utils::vulkan::MAX_FRAMES_IN_FLIGHT> create_descriptor_sets(
                vk::raii::Device &logical_device, RenderContextID render_context_id);

        std::unordered_map<RenderContextID,
                            std::array<vk::raii::DescriptorSet, utils::vulkan::MAX_FRAMES_IN_FLIGHT>>
                        create_init_descriptor_sets(vk::raii::Device &logical_device,
                                                    RenderContextID default_render_context_id) {
            std::unordered_map<RenderContextID,
                               std::array<vk::raii::DescriptorSet, utils::vulkan::MAX_FRAMES_IN_FLIGHT>> return_value;

            return_value.insert({default_render_context_id, create_descriptor_sets(logical_device, default_render_context_id)});

            return return_value;
        }

        BufferRaii create_buffer_ubo(CoreRenderer& core_renderer);

        void get_ready_for_descriptor_write(RenderContextID render_context_id, CoreRenderer &core_renderer);

        size_t m_buffer_size;

        vk::raii::DescriptorSetLayout m_descriptor_set_layout;
        std::unordered_map<RenderContextID, vk::raii::DescriptorPool> m_descriptor_pools; // TODO(Bobby): Know how many RenderPasses we will have before hand

        std::unordered_map<RenderContextID,
                           std::array<vk::raii::DescriptorSet, utils::vulkan::MAX_FRAMES_IN_FLIGHT>> m_descriptor_sets;

        std::unordered_map<RenderContextID,
                          std::vector<std::pair<vk::WriteDescriptorSet, vk::DescriptorBufferInfo>>>
                          m_pending_descriptor_writes;
        std::unordered_map<RenderContextID, std::array<BufferRaii, utils::vulkan::MAX_FRAMES_IN_FLIGHT>>
            m_pending_descriptor_write_buffers;

        //BufferRaii m_data_buffer;
        //vk::DescriptorBufferInfo m_data_buffer_info;
    };
}

#endif //ARISTAEUS_UNIFORM_BUFFER_HPP
