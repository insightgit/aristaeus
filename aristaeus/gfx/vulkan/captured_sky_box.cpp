//
// Created by bobby on 22/12/22.
//

#include "captured_sky_box.hpp"
#include "lighting_environment.hpp"
#include "perspective_camera_environment.hpp"
#include "sky_box.hpp"
#include "render_phase.hpp"



aristaeus::gfx::vulkan::CapturedSkyBox::CapturedSkyBox(const std::vector<glm::vec3> &initial_capture_points,
                                                       size_t capture_points_capacity,
                                                       const vk::Extent2D &cubemap_resolution,
                                                       bool depth_only) :
                                                        m_capture_points(initial_capture_points),
                                                        m_capture_points_capacity(capture_points_capacity),
                                                        m_cubemap_resolution(cubemap_resolution),
                                                        m_depth_only(depth_only),
                                                        m_cubemap_sampler(CoreRenderer::instance->get_logical_device_ref(),
                                                                          CubeMapTexture::get_sampler_info()),
                                                        m_capture_point_infos(
                                                                construct_render_passes()),
                                                        m_captured_semaphore(
                                                                create_captured_semaphore()) {
    m_capture_points.reserve(m_capture_points_capacity);

    for (size_t i = 0; m_capture_points.size() > i; ++i) {
        m_uncaptured_points.insert(i);
    }

    assert(m_capture_points_capacity >= m_capture_points.size());
}

std::vector<aristaeus::gfx::vulkan::CapturedSkyBox::CapturePointInfo>
        aristaeus::gfx::vulkan::CapturedSkyBox::construct_render_passes() {
    CoreRenderer &core_renderer = utils::get_singleton_safe<CoreRenderer>();
    std::vector<CapturePointInfo> return_value;

    m_captured_sky_box_context_infos.clear();

    m_render_phase_attached_to = {};

    for (size_t i2 = 0; m_capture_points_capacity > i2; ++i2) {
        m_captured_sky_box_context_infos.push_back(CapturedSkyBoxContextInfo{
            .capture_point = i2,
            .sky_box = *this
        });
    }

    for(size_t i2 = 0; m_capture_points_capacity > i2; ++i2) {
        std::vector<RenderPhase::AttachmentInfo> attachment_infos =
            m_depth_only ? core_renderer.get_default_depth_attachment_info() : core_renderer.get_default_color_attachment_info();

        for(RenderPhase::AttachmentInfo &attachment_info : attachment_infos) {
            attachment_info.usage_flags |= vk::ImageUsageFlagBits::eTransferSrc;
        }

        std::vector<vk::ClearValue> clear_values;

        if (!m_depth_only) {
            glm::vec4 clear_color = core_renderer.get_clear_color();

            clear_values.push_back(vk::ClearValue{ std::array<float, 4>{clear_color.x, clear_color.y, clear_color.z, clear_color.w} });
        }

        clear_values.push_back(vk::ClearValue{ vk::ClearDepthStencilValue{ 1.0f, 0 } });

        RenderPhase::RenderPassInfo render_pass_info{
            .before_function = captured_sky_box_predraw_load,
            .before_function_arg = &m_captured_sky_box_context_infos[i2],
            .clear_values = clear_values,
            .contextless_draw_function = captured_sky_box_predraw,
            .draw_function = captured_sky_box_render_callback,
            .draw_function_arg = &m_captured_sky_box_context_infos[i2],
            .name = get_render_pass_name(i2),
            .one_shot = true,
            .fire_one_shot_on_creation = m_capture_points.size() > i2,
            .phase_dependents = std::unordered_map<std::string, vk::PipelineStageFlags>{},
            .phase_dependencies = std::unordered_map<std::string, vk::PipelineStageFlags>{},
            .render_pass_attachments = attachment_infos,
            .resolution = glm::vec2{1000, 1000},
            .num_of_render_contexts = 6,
            .is_cube_map = true
        };

        render_pass_info.phase_dependents.insert(std::pair<std::string, vk::PipelineStageFlags>{ std::string{CoreRenderer::SHADOW_MAPPING_RENDER_PHASE}, vk::PipelineStageFlagBits::eFragmentShader });

        RenderPhase *new_render_phase = &core_renderer.add_render_pass(render_pass_info, !m_captured_sky_box_phase_created, 
                                                                       std::string{ CAPTURED_SKY_BOX_PHASE_NAME });

        if (!m_captured_sky_box_phase_created) {
            m_captured_sky_box_phase_created = true;
        }

        // all the render passes here should have the same dependencies and the same attachments
        if (m_render_phase_attached_to.has_value()) {
            assert(new_render_phase->get_render_phase_name() == *m_render_phase_attached_to);
        }
        else {
            m_render_phase_attached_to = new_render_phase->get_render_phase_name();

            new_render_phase->set_after_callback<CapturedSkyBox>(captured_sky_box_after_phase_callback, this);
            new_render_phase->set_before_callback<CapturedSkyBox>(captured_sky_box_before_phase_callback, this);
        }

        SharedImageRaii &frame_0_image = new_render_phase->get_render_pass_image(render_pass_info.name, 0);
        SharedImageRaii &frame_1_image = new_render_phase->get_render_pass_image(render_pass_info.name, 1);

        assert(frame_0_image->format == frame_1_image->format);

        vk::ImageAspectFlags image_aspect_flags = m_depth_only ? vk::ImageAspectFlagBits::eDepth : vk::ImageAspectFlagBits::eColor;

        vk::ImageViewCreateInfo image_view_create_info{ {}, frame_0_image->image, vk::ImageViewType::eCube, frame_0_image->format, {},  
                                                        {image_aspect_flags, 0, 1, 0, 6}};
        vk::raii::ImageView frame_0_cubemap_image_view{ core_renderer.get_logical_device_ref(), image_view_create_info };

        image_view_create_info.image = frame_1_image->image;

        vk::raii::ImageView frame_1_cubemap_image_view{ core_renderer.get_logical_device_ref(), image_view_create_info };

        return_value.push_back(CapturePointInfo{
            .render_pass_name = render_pass_info.name,
            .image_views = {{std::move(frame_0_cubemap_image_view), std::move(frame_1_cubemap_image_view)}}
        });
    }

    return return_value;
}

void aristaeus::gfx::vulkan::CapturedSkyBox::captured_sky_box_render_callback(void *arg_ptr) {
    CoreRenderer &core_renderer = utils::get_singleton_safe<CoreRenderer>();
    CapturedSkyBoxContextInfo &context_info = *reinterpret_cast<CapturedSkyBoxContextInfo*>(arg_ptr);
    RenderContextID render_context_id = core_renderer.get_current_render_context_id();
    CapturedSkyBox &sky_box = context_info.sky_box;

    vk::CommandBuffer command_buffer =
        core_renderer.get_graphics_command_buffer_manager().get_command_buffer_for_thread(render_context_id);

    if (sky_box.m_capture_points.size() <= context_info.capture_point) {
        return;
    }

    const glm::vec3 &capture_point = sky_box.m_capture_points[context_info.capture_point];
    RenderPhase *current_render_phase = core_renderer.get_current_render_phase();

    assert(current_render_phase != nullptr);
    RenderContextID starting_render_context_id = current_render_phase->get_render_pass_starting_render_context_id(current_render_phase->get_current_render_pass());
    RenderContextID local_render_context_id = (render_context_id - starting_render_context_id) % current_render_phase->get_num_of_render_contexts();

    assert(local_render_context_id < 6);

    float target_pitch_degrees = PITCH_DEGREES[local_render_context_id];
    float target_yaw_degrees = YAW_DEGREES[local_render_context_id];

    glm::vec3 camera_up{ 0, 1, 0 };

    if (target_pitch_degrees == 90) {
        camera_up = glm::vec3{ 0.0f, 0, 1.0f };
    }
    else if (target_pitch_degrees == -90) {
        camera_up = glm::vec3{ 0.0f, 0, -1.0 };
    }

    core_renderer.get_perspective_camera_environment().setup_capture(PerspectiveCameraEnvironment::Capture{
        .aspect_ratio = 1.0f,
        .fov = 90.0f,
        .pitch = target_pitch_degrees,
        .position = capture_point,
        .up = camera_up,
        .yaw = target_yaw_degrees,
        .z_far = 100.0f,
        .z_near = 0.1f,
    });

    core_renderer.get_perspective_camera_environment().draw(command_buffer, core_renderer, core_renderer.get_logical_device_ref());
}

void aristaeus::gfx::vulkan::CapturedSkyBox::captured_sky_box_predraw_load(void *sky_box_ptr) {
    CoreRenderer &core_renderer = utils::get_singleton_safe<CoreRenderer>();

    core_renderer.predraw_load();
}

void aristaeus::gfx::vulkan::CapturedSkyBox::captured_sky_box_predraw(void*) {
    CoreRenderer &core_renderer = utils::get_singleton_safe<CoreRenderer>();

    core_renderer.get_perspective_camera_environment().predraw();
}

void aristaeus::gfx::vulkan::CapturedSkyBox::captured_sky_box_before_phase_callback(CapturedSkyBox *sky_box) {
    assert(sky_box != nullptr);

    std::vector<std::pair<size_t, size_t>> uncaptured_point_array_ranges;

    if (!sky_box->m_first_capture_time) {
        return;
    }

    for (size_t uncaptured_point_index : sky_box->m_uncaptured_points) {
        std::pair<size_t, size_t> my_point_pair{ uncaptured_point_index * 6, (uncaptured_point_index + 1) * 6 };

        bool found_pair = false;

        for (std::pair<size_t, size_t> &array_range : uncaptured_point_array_ranges) {
            if (array_range.second == my_point_pair.first) {
                array_range.second = my_point_pair.second;
            }
            else if (array_range.first == my_point_pair.second) {
                array_range.first = my_point_pair.first;
            }
            else {
                continue;
            }

            found_pair = true;
            break;
        }

        if (!found_pair) {
            uncaptured_point_array_ranges.push_back(my_point_pair);
        }
    }

    sky_box->m_first_capture_time = false;
}
void aristaeus::gfx::vulkan::CapturedSkyBox::captured_sky_box_after_phase_callback(CapturedSkyBox *sky_box) {
    assert(sky_box != nullptr);

    sky_box->m_uncaptured_points.clear();
}

void aristaeus::gfx::vulkan::CapturedSkyBox::capture_needed_points() {
    if (m_capture_points_capacity == 0 || m_capture_points.empty()) {
        return;
    }

    assert(m_capture_point_infos.size() == m_capture_points_capacity);
    assert(m_render_phase_attached_to.has_value());

    RenderPhase *render_phase = utils::get_singleton_safe<CoreRenderer>().get_render_phase(*m_render_phase_attached_to);

    assert(render_phase != nullptr);

    for (size_t uncaptured_point_index : m_uncaptured_points) {
        render_phase->retoggle_one_shot_render_pass(m_capture_point_infos[uncaptured_point_index].render_pass_name);
    }
}

void aristaeus::gfx::vulkan::CapturedSkyBox::capture_points() {
    for (size_t i2 = 0; m_capture_points_capacity > i2; ++i2) {
        m_captured_sky_box_context_infos[i2].sky_box = *this;
    }

    if (m_capture_points_capacity == 0 || m_capture_points.empty()) {
        return;
    }

    force_recapture();

    capture_needed_points();
}

std::vector<vk::DescriptorImageInfo> aristaeus::gfx::vulkan::CapturedSkyBox::get_all_descriptor_image_infos() {
    std::vector<vk::DescriptorImageInfo> descriptor_image_infos;

    for(size_t i = 0; m_capture_points.size() > i; ++i) {
        descriptor_image_infos.push_back(get_descriptor_image_info(i));
    }

    return descriptor_image_infos;
}

void aristaeus::gfx::vulkan::CapturedSkyBox::update_capture_point(const glm::vec3 &capture_point, uint32_t index) {
    assert(m_render_phase_attached_to.has_value());
    assert(index < m_capture_points_capacity);

    RenderPhase *render_phase = utils::get_singleton_safe<CoreRenderer>().get_render_phase(*m_render_phase_attached_to);

    assert(render_phase != nullptr);

    if (m_capture_points.size() < (index + 1)) {
        m_capture_points.resize(std::min(static_cast<size_t>(index + 1), m_capture_points_capacity));
    }
    
    m_capture_points[index] = capture_point;

    render_phase->retoggle_one_shot_render_pass(m_capture_point_infos[index].render_pass_name);

    m_uncaptured_points.insert(index);
}

void aristaeus::gfx::vulkan::CapturedSkyBox::attach_capture_point_to_node(const glm::vec3 &capture_point, gfx::SceneGraphNode &scene_graph_node) {
    std::optional<size_t> capture_point_index;

    for (size_t i = 0; m_capture_points.size() > i; ++i) {
        if (m_capture_points[i] == capture_point) {
            assert(!capture_point_index.has_value());

            capture_point_index = i;
            break;
        }
    }

    if (capture_point_index.has_value()) {
        size_t capture_point_index_value = *capture_point_index;
        std::shared_ptr<gfx::SceneGraphNode> node_ptr = scene_graph_node.get_weak_ptr_to_this().lock();

        assert(node_ptr != nullptr);

        if (m_attached_capture_points.contains(node_ptr)) {
            scene_graph_node.remove_callback_for_event<void*>(std::string{SceneGraphNode::GLOBAL_TRANSFORM_CHANGED_EVENT}, "point_light_global_transform_changed");
        }

        m_attached_capture_points[node_ptr] = capture_point_index_value;

        // TODO(Bobby): raii-ize this add_callback_for_event call
        scene_graph_node.add_callback_for_event<void*, gfx::SceneGraphNode>(
                std::string{SceneGraphNode::GLOBAL_TRANSFORM_CHANGED_EVENT},
                "point_light_captured_sky_box_" + std::to_string(reinterpret_cast<size_t>(this)),
                [this, capture_point_index_value](gfx::SceneGraphNode& node, void*) {
            gfx::SceneGraphNode::Transform transform = node.get_parent_transform() + node.get_transform();

            std::weak_ptr<gfx::SceneGraphNode> node_weak_ptr = node.get_weak_ptr_to_this();

            assert(!node_weak_ptr.expired());

            if (transform.position != m_capture_points[capture_point_index_value]) {
                m_capture_points[capture_point_index_value] = transform.position;
                m_uncaptured_points.insert(capture_point_index_value);
            }
        }, scene_graph_node);
    } else {
        throw std::runtime_error("Couldn't find " + glm::to_string(capture_point) + " in this CapturedSkyBox!");
    }
}

void aristaeus::gfx::vulkan::CapturedSkyBox::force_recapture() {
    m_uncaptured_points.clear();

    for (size_t i = 0; m_capture_points.size() > i; ++i) {
        m_uncaptured_points.insert(i);
    }
}

[[nodiscard]] vk::DescriptorImageInfo aristaeus::gfx::vulkan::CapturedSkyBox::get_descriptor_image_info(size_t capture_point_index) {
    CoreRenderer &core_renderer = utils::get_singleton_safe<CoreRenderer>();

    if (m_uncaptured_points.contains(capture_point_index)) {
        assert(LightingEnvironment::NULL_CUBE_MAP_TEXTURE_INITED);

        return LightingEnvironment::NULL_CUBE_MAP_TEXTURE->get_descriptor_image_info();
    }
    else {
        return { *m_cubemap_sampler, *m_capture_point_infos[capture_point_index].image_views[core_renderer.get_current_frame()], vk::ImageLayout::eShaderReadOnlyOptimal };
    }
}
