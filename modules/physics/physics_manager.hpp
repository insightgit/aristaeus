#ifndef ARISTAEUS_PHYSICS_MANAGER_HPP
#define ARISTAEUS_PHYSICS_MANAGER_HPP

#include <cassert>
#include <iostream>
#include <vector>

#include <PxPhysicsAPI.h>

#include <gfx/vulkan/assimp_mesh.hpp>
#include <gfx/vulkan/graphics_pipeline.hpp>
#include <gfx/scene_graph_node.hpp>
#include <core/singleton.hpp>

#include "physx_obj_deleter.hpp"

namespace aristaeus::physics {
	class PhysicsErrorCallback : public physx::PxErrorCallback
	{
	public:
		virtual void reportError(physx::PxErrorCode::Enum code, const char* message, const char* file, int line) override
		{
			std::string error_name;

			switch (code)
			{
			case physx::PxErrorCode::eNO_ERROR:
				error_name = "no error";
				break;
			case physx::PxErrorCode::eINVALID_PARAMETER:
				error_name = "invalid parameter";
				break;
			case physx::PxErrorCode::eINVALID_OPERATION:
				error_name = "invalid operation";
				break;
			case physx::PxErrorCode::eOUT_OF_MEMORY:
				error_name = "out of memory";
				break;
			case physx::PxErrorCode::eDEBUG_INFO:
				error_name = "info";
				break;
			case physx::PxErrorCode::eDEBUG_WARNING:
				error_name = "warning";
				break;
			case physx::PxErrorCode::ePERF_WARNING:
				error_name = "performance warning";
				break;
			case physx::PxErrorCode::eABORT:
				error_name = "abort";
				break;
			case physx::PxErrorCode::eINTERNAL_ERROR:
				error_name = "internal error";
				break;
			case physx::PxErrorCode::eMASK_ALL:
				error_name = "unknown error";
				break;
			}

			assert(!error_name.empty());
			std::cout << "[PHYSX] error type " << error_name << " on " << file << ":" << line << ": " << message << "\n";

			// in debug builds halt execution for abort codes
			assert(code != physx::PxErrorCode::eABORT);
		}
	};

	struct MaterialInfo {
		physx::PxReal dynamic_friction;
		physx::PxReal static_friction;
		physx::PxReal restitution;

		bool operator==(const MaterialInfo &other) const {
			return dynamic_friction == other.dynamic_friction && static_friction == other.static_friction && 
				restitution == other.restitution;
		}
	};

	enum class GeometryType {
		BOX,
		SPHERE,
		CAPSULE,
		PLANE,
		TRIANGLE_MESH,
		TRIANGLE_MESH_NODE
	};

	struct GeometryInfo {
		GeometryType geom_type;
		std::optional<MaterialInfo> material_info;

		union NumericArg {
			float float_arg;
			uint32_t uint32_arg;
		};

		std::vector<NumericArg> numeric_args;
		std::string string_arg;

		bool operator==(const GeometryInfo &other) const {
			bool numeric_args_equal = other.numeric_args.size() == numeric_args.size();

			if (numeric_args_equal) {
				for (size_t i = 0; other.numeric_args.size() > i; ++i) {
					if (other.numeric_args[i].uint32_arg != numeric_args[i].uint32_arg && 
						other.numeric_args[i].float_arg != numeric_args[i].float_arg) {
						numeric_args_equal = false;
						break;
					}
				}
			}

			return numeric_args_equal && geom_type == other.geom_type && material_info == other.material_info &&
				   string_arg == other.string_arg;
		}
	};

	struct PhysxShape {
		std::shared_ptr<physx::PxShape> shape;
		MaterialInfo material_info;
		std::shared_ptr<physx::PxMaterial> custom_material;
	};
}

namespace std {
	template <>
	struct hash<aristaeus::physics::MaterialInfo>
	{
		size_t operator()(const aristaeus::physics::MaterialInfo &material_info) const
		{
			return (hash<physx::PxReal>()(material_info.dynamic_friction) << 40) | 
				   ((hash<physx::PxReal>()(material_info.static_friction) << 20) & utils::get_bit_mask(20, 20)) |
				   (hash<physx::PxReal>()(material_info.restitution) & utils::get_bit_mask(0, 20));
		};
	};

	template<>
	struct hash<aristaeus::physics::GeometryInfo>
	{
		size_t operator()(const aristaeus::physics::GeometryInfo &geometry_info) const {
			size_t material_info_hash = geometry_info.material_info.has_value() ? hash<aristaeus::physics::MaterialInfo>()(*geometry_info.material_info) : ~(0x0);

			std::vector<uint32_t> u32_numeric_args;

			for (const aristaeus::physics::GeometryInfo::NumericArg &numeric_arg : geometry_info.numeric_args) {
				u32_numeric_args.push_back(numeric_arg.uint32_arg);
			}

			return (static_cast<size_t>(geometry_info.geom_type)) | 
				   ((material_info_hash << 4) & utils::get_bit_mask(4, 20)) | 
				   (utils::Uint32VectorHasher()(u32_numeric_args) << 24 & utils::get_bit_mask(24, 24)) |
				   (hash<std::string>()(geometry_info.string_arg) & utils::get_bit_mask(48, 16));
		}
	};

	template <>
	struct hash<aristaeus::physics::PhysxShape>
	{
		size_t operator()(const aristaeus::physics::PhysxShape& physx_shape) const
		{
			return (hash<aristaeus::physics::MaterialInfo>()(physx_shape.material_info) << 32) | ((reinterpret_cast<size_t>(physx_shape.shape.get()) << 32) >> 32);
		}
	};
}

namespace aristaeus::physics {
    class PhysicsManager : public core::Singleton<PhysicsManager>, public core::EventObject {
	public:              
		static constexpr std::string_view PHYSICS_STEP_FINISHED_EVENT = "physics_step_finished_event";

		static constexpr MaterialInfo DEFAULT_MATERIAL_INFO{
			.dynamic_friction = 0.0f,
			.static_friction = 0.0f,
			.restitution = 0.0f
		};


		PhysicsManager(const nlohmann::json &physics_module_options);

		virtual ~PhysicsManager() = default;

		void add_actor(physx::PxActor &physics_actor, gfx::SceneGraphNode &scene_graph_node);
		void remove_actor(physx::PxActor &physics_actor);

		physx::PxPhysics &get_physics() {
			return *m_physics;
		}

		physx::PxMaterial &get_default_material() {
			return *m_default_material;
		}

		std::shared_ptr<physx::PxMaterial> create_cached_material(const MaterialInfo &material_info);
		std::shared_ptr<PhysxShape> create_cached_shape(const GeometryInfo &geometry_info, gfx::SceneGraphNode *relative_node = nullptr);

		void step();
		void waitForStepResults();

		void debug_render();
	private:
		static constexpr float DEFAULT_TIME_STEP = 1.0f / 60.0f;
		static constexpr glm::vec3 VISUALIZATION_POINT_COLOR = glm::vec3(1, 0, 0);
		static constexpr glm::vec3 VISUALIZATION_LINE_COLOR = glm::vec3(1, 0, 0);

		static constexpr float SDF_DEFAULT_SPACING = 0.01f;
		static constexpr uint32_t SDF_DEFAULT_SUBGRID_SIZE = 4;
		static constexpr uint32_t SDF_BITS_PER_SUBGRID_PIXEL = 8;


		static PhysicsErrorCallback error_callback;
		static physx::PxDefaultAllocator default_allocator_callback;

		static GeometryInfo geometry_info_parse_function(const nlohmann::json &shape_json);

		float m_time_step;
		bool m_visualization_mode;

		physx::PxTriangleMeshGeometry *create_mesh_geometry(const gfx::vulkan::AssimpMesh::MeshGeometryData &mesh_geo_data, 
															std::optional<physx::PxSDFDesc> sdf_info = {});

		gfx::vulkan::GraphicsPipeline::PipelineInfo get_pipeline_info(bool is_point);
		std::vector<gfx::vulkan::GraphicsPipeline::ShaderInfo> get_shader_infos();

		PhysxUniquePtr<physx::PxFoundation> m_foundation;
		
		#if !defined(NDEBUG) && defined(_WIN32)
		PhysxUniquePtr<physx::PxPvd> m_visual_debugger;
		PhysxUniquePtr<physx::PxPvdTransport> m_visual_debugger_transport;
		#endif

		PhysxUniquePtr<physx::PxPhysics> m_physics;
		PhysxUniquePtr<physx::PxDefaultCpuDispatcher> m_cpu_dispatcher; // TODO(Bobby): integrate with Taskflow
		PhysxUniquePtr<physx::PxScene> m_scene;

		PhysxUniquePtr<physx::PxMaterial> m_default_material;

		std::unordered_map<MaterialInfo, std::weak_ptr<physx::PxMaterial>> m_materials_cache;
		std::unordered_map<GeometryInfo, std::weak_ptr<PhysxShape>> m_shape_cache;

		std::chrono::system_clock::time_point m_last_physics_step_time;

		std::unordered_map<physx::PxActor*, std::reference_wrapper<gfx::SceneGraphNode>> m_scene_graph_nodes;

		gfx::vulkan::GraphicsPipeline m_debug_point_pipeline;
		gfx::vulkan::GraphicsPipeline m_debug_line_pipeline;
		std::array<gfx::vulkan::BufferRaii, utils::vulkan::MAX_FRAMES_IN_FLIGHT> m_debug_render_buffers;
	};
}

#endif