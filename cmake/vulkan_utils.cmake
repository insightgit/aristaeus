find_program(GLSLC glslc)

if(GLSLC)
    message(STATUS "glslc ${GLSLC} - found")
else()
    message(FATAL_ERROR "glslc - not found")
endif()


function(add_glsl_shader glsl_shader_path compiler_definitions custom_output_file)
    if ( NOT CMAKE_BUILD_TYPE )
        message(STATUS "Build Type not set, defaulting to Debug..." )
        set( CMAKE_BUILD_TYPE Debug )
    endif()

    if(CMAKE_BUILD_TYPE MATCHES Debug)
        message(STATUS "glsl shader will have debug info")
        set(DEBUG_OPTIONS "-g")
    else()
        set(DEBUG_OPTIONS "-DNDEBUG")
    endif()


    set(COMPILER_DEFINES "")

    foreach(define IN LISTS compiler_definitions)
        set(COMPILER_DEFINES ${COMPILER_DEFINES} -D${define})
    endforeach()

    if(custom_output_file STREQUAL "")
        set(custom_output_file "${CMAKE_CURRENT_BINARY_DIR}/${glsl_shader_path}.spv")
    endif()

    get_filename_component(glsl_shader_dir ${CMAKE_CURRENT_BINARY_DIR}/${glsl_shader_path}.spv DIRECTORY)

    file(MAKE_DIRECTORY ${glsl_shader_dir})

    add_custom_command(OUTPUT ${custom_output_file}
            COMMAND ${GLSLC} ${CMAKE_CURRENT_SOURCE_DIR}/${glsl_shader_path} ${DEBUG_OPTIONS} ${COMPILER_DEFINES} -o ${custom_output_file}
            DEPENDS ${CMAKE_CURRENT_SOURCE_DIR}/${glsl_shader_path}
            VERBATIM)
endfunction()