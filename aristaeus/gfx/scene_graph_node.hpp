#ifndef ARISTAEUS_SCENE_GRAPH_NODE_HPP
#define ARISTAEUS_SCENE_GRAPH_NODE_HPP

#define PX_PHYSX_STATIC_LIB

#include <array>
#include <map>
#include <memory>
#include <optional>
#include <string>
#include <vector>

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/glm.hpp>
#include <glm/gtx/quaternion.hpp>

#include <taskflow/taskflow.hpp>

#include "vulkan/vulkan_raii.hpp"

#include "vulkan/graphics_pipeline.hpp"

#include "../core/event_object.hpp"
#include "../core/class_db.hpp"

#include <imgui.h>
#include "../../libraries/include/ImGuizmo.h"

namespace aristaeus::gfx {
	namespace vulkan {
		class CoreRenderer;
	}

    class SceneGraphNode : public core::EventObject {
		ARISTAEUS_CLASS_HEADER_ENTRY_POINT("scene_graph_node", SceneGraphNode, false, _construct_entry_point)
	public:
		static constexpr char NODE_CHILD_SEPARATOR = '/';
		static constexpr std::string_view NODE_PARENT_SEPARATOR = "..";
        static constexpr std::string_view NODE_NOOP_SEPARATOR = ".";
		static constexpr std::string_view ROOT_NODE_NAME = "root";
		static constexpr std::string_view BLENDED_OBJECT_GROUP = "blended_object";
		
		static constexpr int NON_BLENDED_OBJECT_Z_ORDER_DEFAULT = 0;
		static constexpr int BLENDED_OBJECT_Z_ORDER_RANGE_START = 1;

        static constexpr std::string_view BEFORE_PARSING_EVENT = "before_parsing";
        static constexpr std::string_view PARSING_DONE_EVENT = "parsing_done";
        static constexpr std::string_view ENTER_TREE_EVENT = "enter_tree";
        static constexpr std::string_view EXIT_TREE_EVENT = "exit_tree";
        static constexpr std::string_view TRANSFORM_CHANGED_EVENT = "transform_changed";
        static constexpr std::string_view GLOBAL_TRANSFORM_CHANGED_EVENT = "global_transform_changed";

		static constexpr std::string_view DESCENDENT_NODE_ADDED_EVENT = "descendent_node_added_event";

        // this is different from ENTER_TREE_EVENT because ENTER_TREE_EVENT is called before
        // children if any are added to the node, whereas this is triggered afterwards
        // if it was added as a prefab or in overall scene initialization (Otherwise, it will be called
        // at the same time because we won't know if we have any children or when they might be added).
        // In addition to having a gurantee of added children, all children must have also had this event
        // called on them before their parent.
        static constexpr std::string_view TREE_READY_EVENT = "tree_ready";

		struct Transform {
			glm::vec3 scale;
			glm::vec3 position;
			glm::quat rotation_quat;

			Transform operator+(const Transform &other) const {
				return Transform {
					.scale = other.scale * scale,
					.position = other.position + position,
					.rotation_quat = other.rotation_quat * rotation_quat
				};
			}

			Transform operator-(const Transform &other) const;

			bool operator==(const Transform &other) const {
				return scale == other.scale && position == other.position && rotation_quat == other.rotation_quat;
			}

			void operator+=(const Transform &other) {
				*this = *this + other;
			}

			void operator-=(const Transform &other) {
				*this = *this - other;
			}
		};

		static constexpr Transform IDENTITY_TRANSFORM {
			.scale = glm::vec3{1.0f},
			.position = glm::vec3{0.0f},
			.rotation_quat = glm::quat{0.0f, 0.0f, 0.0f, 1.0f}
		};

		SceneGraphNode(const std::string &so_stuff_will_compile, const std::string& node_type) {
			update_model_matrix(false);
		}

		SceneGraphNode(SceneGraphNode &&other) {
			m_children_nodes = std::move(other.m_children_nodes);
			other.m_children_nodes = {};

			m_parent_node = other.m_parent_node;
			other.m_parent_node = nullptr;

			m_model_matrix = other.m_model_matrix;

			m_rotation_quat = other.m_rotation_quat;
			m_scale = other.m_scale;
			m_position = other.m_position;

			m_node_path = std::move(other.m_node_path);
			m_node_name = std::move(other.m_node_name);

			m_node_groups = std::move(other.m_node_groups);

			m_visible = other.m_visible;
			m_z_order = other.m_z_order;
			m_z_order_map = std::move(other.m_z_order_map);
			m_reaper_list = std::move(other.m_reaper_list);
		}

		virtual ~SceneGraphNode() {
			for (std::pair<const std::string, std::shared_ptr<SceneGraphNode>> &child_node : m_children_nodes) {
				assert(child_node.second != nullptr);
				child_node.second->m_parent_node = nullptr;
				child_node.second->m_node_name = "";
			}
		}

		void add_child(const std::shared_ptr<SceneGraphNode> &child_node);

		bool remove_child(const std::string& scene_graph_node, bool delay_deletion = false, std::optional<uint32_t> current_frame_num = {});

		void remove_all_children(bool delay_deletion = false, std::optional<uint32_t> current_frame_num = {}) {
			std::vector<std::string> all_children_strings;

			for (std::pair<std::string, std::shared_ptr<SceneGraphNode>> node : m_children_nodes) {
				all_children_strings.push_back(node.first);
			}


			for(const std::string &child : all_children_strings) {
				remove_child(child, delay_deletion, current_frame_num);
			}
		}

		bool remove_child(const std::shared_ptr<SceneGraphNode> &scene_graph_node, bool delay_deletion = false, std::optional<uint32_t> current_frame_num = {}) {
			if (scene_graph_node != nullptr) {
				return remove_child(scene_graph_node->get_node_name(), delay_deletion, current_frame_num);
			}
			else {
				return false;
			}
		}

		std::weak_ptr<SceneGraphNode> get_child(std::string node_name) {
			return m_children_nodes.at(node_name);
		}

		bool map_contains(std::string node_name) {
			bool a = false;
			for (auto const& pair : m_children_nodes) {
				if (pair.first == node_name) {
					a = true;
				}
			}
			return a;
		}

		[[nodiscard]] std::weak_ptr<SceneGraphNode> get_weak_ptr_to_this() const {
			return m_weak_ptr_to_this;
		}

		// the path here includes both the child node's name and the path to it.
		[[nodiscard]] std::string get_node_path() const {
			return m_node_path;
		}

		[[nodiscard]] std::string get_node_name() const {
			return m_node_name;
		}

		[[nodiscard]] glm::quat get_rotation_quat() const {
			return m_rotation_quat;
		}

		[[nodiscard]] glm::vec3 get_scale() const {
			return m_scale;
		}

		[[nodiscard]] glm::vec3 get_position() const {
			return m_position;
		}

		[[nodiscard]] glm::mat4 get_model_matrix() const {
			return m_model_matrix;
		}

		[[nodiscard]] glm::mat4 get_global_model_matrix() const {
			return m_global_model_matrix;
		}

		[[nodiscard]] bool is_part_of_node_group(const std::string &node_group) {
			return m_node_groups.contains(node_group);
		}

		[[nodiscard]] std::vector<std::shared_ptr<gfx::SceneGraphNode>> get_children() const {
			std::vector<std::shared_ptr<gfx::SceneGraphNode>> return_value; 
			
			for (const std::pair<const std::string, std::shared_ptr<gfx::SceneGraphNode>> &child_node : m_children_nodes) {
				return_value.push_back(child_node.second);
			}

			return return_value;
		}

		[[nodiscard]] std::unordered_set<std::string> get_node_groups() const {
			return m_node_groups;
		}

		[[nodiscard]] Transform get_parent_transform() const {
			if (m_parent_node == nullptr) {
				return IDENTITY_TRANSFORM;
			}
			else {
				Transform direct_parent_transform = m_parent_node->get_transform();
				Transform parent_parent_transform = m_parent_node->get_parent_transform();

				return direct_parent_transform + parent_parent_transform;
			}
		}

		[[nodiscard]] Transform get_transform() const {
			return {
				.scale = m_scale,
				.position = m_position,
				.rotation_quat = m_rotation_quat
			};
		}

		[[nodiscard]] int get_z_order() const {
			return m_z_order;
		}

		[[nodiscard]] bool is_visible() const {
			return m_visible;
		}

		void set_weak_ptr_to_this(const std::weak_ptr<SceneGraphNode> &weak_ptr_to_this) {
			if (weak_ptr_to_this.lock().get() != this) {
				throw std::runtime_error("Weak ptr to this must equal this!");
			}

			m_weak_ptr_to_this = weak_ptr_to_this;
		}

		void set_scale(const glm::vec3 &scale) {
			m_scale = scale;

			update_model_matrix(true);
		}

		void set_position(const glm::vec3 &position) {
			m_position = position;

			update_model_matrix(true);
		}

		void set_rotation_quat(const glm::quat &rotation_quat) {
			m_rotation_quat = rotation_quat;

			update_model_matrix(true);
		}

		void set_transform(const Transform &transform, bool trigger_events = true);

		void set_model_matrix(const glm::mat4 &model_matrix) {
			// TODO(Bobby): update position, scale, and rotation quat
			m_model_matrix = model_matrix;
		}

		void set_node_groups(const std::unordered_set<std::string> &node_groups) {
			m_node_groups = node_groups;
		}

		void add_node_group(const std::string &node_group) {
			m_node_groups.insert(node_group);
		}

		void delete_node_group(const std::string &node_group) {
			m_node_groups.erase(node_group);
		}

		void set_z_order(int z_order) {
			if (m_z_order != z_order) {
				update_z_order_map(m_z_order, z_order);

				m_z_order = z_order;
			}
		}

		void rename_node(const std::string &node_name) {
			std::string old_node_name = m_node_name;

			m_node_name = node_name;

			if(m_parent_node != nullptr) {
				std::shared_ptr<SceneGraphNode> my_node_shared_ptr =
					m_parent_node->m_children_nodes.at(old_node_name);

				assert(my_node_shared_ptr.get() == this);

				m_parent_node->m_children_nodes.erase(old_node_name);
				m_parent_node->m_children_nodes.insert({m_node_name, my_node_shared_ptr});
			}

			update_node_paths();
		}

		void hide() {
			m_visible = false;

			for (std::pair<const std::string, std::shared_ptr<SceneGraphNode>> &child : m_children_nodes) {
				assert(child.second != nullptr);

				child.second->hide();
			}
		}

		void show() {
			m_visible = true;

			for (std::pair<const std::string, std::shared_ptr<SceneGraphNode>> &child : m_children_nodes) {
				assert(child.second != nullptr);

				child.second->show();
			}
		}

		static std::pair<std::string, std::string> get_parent_node_path_and_child_name(
                                                                                const std::string &node_path_string,
                                                                                bool normalize_node_path_string = true);

        static std::vector<std::string> get_nodes_in_node_path(const std::string &node_path);
        static std::string normalize_node_path(const std::string &node_path);

		static std::string concatenate_node_paths(const std::vector<std::string> &node_paths,
                                                  bool normalize_node_paths = true);

		std::map<std::string, std::shared_ptr<SceneGraphNode>> get_map() const {
			return m_children_nodes;
		}

		std::optional<std::reference_wrapper<SceneGraphNode>> get_node_with_path(const std::string& node_path_string, bool create_nonexistent_nodes);

		// used for operations on the whole scene tree
		void draw_tree(const Transform &parent_matrix,
						std::optional<std::reference_wrapper<tf::Subflow>> task_subflow = {}, std::optional<int> z_order_to_draw = {},
						const std::unordered_set<std::string> &node_groups_needed_to_draw = {}, const std::unordered_set<std::string> &node_groups_to_ignore = {});

		void predraw_load_tree(const vk::CommandBuffer &command_buffer, const glm::vec3 &camera_position,
							   std::optional<std::reference_wrapper<tf::Subflow>> task_subflow = {});

		// used for operating on an indivdual node in the scene tree
		virtual void draw() = 0;

		virtual void predraw_load(const vk::CommandBuffer &command_buffer, const glm::vec3 &camera_position) = 0;

		[[nodiscard]] SceneGraphNode *get_parent() {
			return m_parent_node;
		}

		[[nodiscard]] std::weak_ptr<SceneGraphNode> get_root_node() {
			return m_root_node;
		}
	protected:
		std::map<std::string, std::shared_ptr<SceneGraphNode>> m_children_nodes;

		// this function should only be called ONCE by the ARISTAEUS_CLASS_HEADER
		// macro
		void _construct_entry_point();
	private:
		static constexpr Transform DEFAULT_TRANSFORM {
			.scale = glm::vec3{1.0f},
			.position = glm::vec3{0.0f},
			.rotation_quat = glm::quat{1.0f, 0.0f, 0.0f, 0.0f}
		};

		void update_model_matrix(bool trigger_events);

		void update_node_paths() {
			if(m_parent_node != nullptr) {
				m_node_path = m_parent_node->get_node_path() + NODE_CHILD_SEPARATOR + get_node_name();
			} else {
				m_node_path = get_node_name();
			}

			for (std::pair<const std::string, std::shared_ptr<SceneGraphNode>> &child_node : m_children_nodes) {
				child_node.second->update_node_paths();
			}
		}

		void add_z_order_map(const std::map<int, int> &z_order_map) {
			for (std::pair<int, int> z_order_map_pair : z_order_map) {
				if (m_z_order_map.contains(z_order_map_pair.first)) {
					m_z_order_map[z_order_map_pair.first] += z_order_map_pair.second;
				}
				else {
					m_z_order_map.insert(z_order_map_pair);
				}
			}

			if (m_parent_node != nullptr) {
				m_parent_node->add_z_order_map(z_order_map);
			}
		}

		void update_z_order_map(int old_z_order, int new_z_order) {
			assert(m_z_order_map.contains(old_z_order));

			--m_z_order_map[old_z_order];

			assert(m_z_order_map.at(old_z_order) >= 0);

			if (m_z_order_map.at(old_z_order) == 0) {
				m_z_order_map.erase(old_z_order);
			}

			if (m_z_order_map.contains(new_z_order)) {
				++m_z_order_map[new_z_order];
			}
			else {
				m_z_order_map.insert({ new_z_order, 1 });
			}
			

			if (m_parent_node != nullptr) {
				m_parent_node->update_z_order_map(old_z_order,new_z_order);
			}
		}

		void remove_z_order_map(const std::map<int, int> &z_order_map) {
			for(std::pair<int, int> z_order_map_pair : z_order_map) {
				assert(m_z_order_map.contains(z_order_map_pair.first));

				int usage_count = m_z_order_map.at(z_order_map_pair.first);

				assert(usage_count >= z_order_map_pair.second);

				if (usage_count == z_order_map_pair.second) {
					m_z_order_map.erase(z_order_map_pair.first);
				}
				else {
					m_z_order_map[z_order_map_pair.first] -= z_order_map_pair.second;
				}
			}

			if (m_parent_node != nullptr) {
				m_parent_node->remove_z_order_map(z_order_map);
			}
		}

		void set_root_node_recursively(const std::weak_ptr<gfx::SceneGraphNode> &root_node) {
			std::queue<std::reference_wrapper<SceneGraphNode>> children;

			children.push(*this);

			while (!children.empty()) {
				children.front().get().m_root_node = root_node;

				for (std::shared_ptr<gfx::SceneGraphNode> &child_of_child : children.front().get().get_children()) {
					children.push(*child_of_child);
				}

				children.pop();
			}
		}

		static nlohmann::json transform_ui_function(Transform &persistent_state, bool display_gui, const std::optional<std::string> &label_hint);

		SceneGraphNode *m_parent_node = nullptr;
		std::weak_ptr<SceneGraphNode> m_root_node;

		glm::mat4 m_model_matrix;
		glm::mat4 m_global_model_matrix;

		glm::quat m_rotation_quat{1.0f, 0.0f, 0.0f, 0.0f};
		glm::vec3 m_scale{1.0f};
		glm::vec3 m_position{0.0f};

		std::string m_node_path;
		std::string m_node_name;

		std::unordered_set<std::string> m_node_groups;

		bool m_visible = true;
		int m_z_order = 0; // drawing order: higher z order means it will be drawn later, lower means it will be drawn earlier. The same z-order
						   // will mean that objects will be drawn based on the traversal that the scene tree does
						   // TODO(Bobby): Implement optional relative z-order (children get a higher z order than parents)

		std::map<int, int> m_z_order_map{ {NON_BLENDED_OBJECT_Z_ORDER_DEFAULT, 1} }; // set of z-order for this node and its children (key = z order, value = number of nodes with said z-order

		std::unordered_map<uint32_t, std::vector<std::shared_ptr<SceneGraphNode>>> m_reaper_list;

		std::weak_ptr<SceneGraphNode> m_weak_ptr_to_this;
	};

	class NoOpNode : public SceneGraphNode {
	ARISTAEUS_CLASS_HEADER("no_op", NoOpNode, true);
	public:
		NoOpNode(bool temporary) : m_temporary(temporary) {}
		
		void predraw_load(const vk::CommandBuffer &command_buffer, const glm::vec3 &camera_position)override {}

		void draw()override {}

		bool get_temp() {
			return m_temporary;
		}

		void set_temp(bool temp) {
			m_temporary = temp;
		}
	private:
		bool m_temporary = false;
	};
}

#endif