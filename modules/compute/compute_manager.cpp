#include <gfx/vulkan/core_renderer.hpp>

#include "compute_manager.hpp"

template<>
aristaeus::gfx::vulkan::compute::ComputeManager
    *aristaeus::core::Singleton<aristaeus::gfx::vulkan::compute::ComputeManager>::instance = nullptr;

aristaeus::gfx::vulkan::compute::ComputeManager::ComputeManager(const std::optional<uint32_t> &compute_family_index,
                                                                const std::optional<uint32_t> &queue_index,
													            const std::optional<std::string> &compute_info_path) :
    Singleton(),
	m_async_compute_queue(get_async_compute_queue(compute_family_index, queue_index)),
	m_compute_fences(utils::create_signaled_fences(utils::get_singleton_safe<CoreRenderer>().get_logical_device_ref())),
	m_command_buffer_manager(get_command_buffer_manager(compute_family_index)),
	m_compute_info_path(compute_info_path),
	m_compute_cache_info_json(create_and_verify_compute_cache_json(m_compute_info_path)) {
	m_command_buffer_manager->add_render_phase();
    auto &core_renderer = utils::get_singleton_safe<CoreRenderer>();

    core_renderer.add_callback_for_event<void*, CoreRenderer>(
            std::string{CoreRenderer::DRAW_FRAME_EVENT}, "compute_manager_draw_frame",
            [](CoreRenderer &core_renderer, void*){
        auto &compute_manager = utils::get_singleton_safe<ComputeManager>();

        if (compute_manager.is_async()) {
            // TODO(Bobby): multithread this
            compute_manager.update_submitted_jobs();
            compute_manager.execute_async_compute_jobs();
        } else {
            compute_manager.update_submitted_jobs(utils::get_singleton_safe<CoreRenderer>().get_current_frame());
        }
    }, core_renderer);

    if(!is_async()) {
        RenderPhase::RenderPassInfo render_pass_info{
            .draw_function = [](void*) {
                auto &compute_manager = utils::get_singleton_safe<ComputeManager>();
                auto &core_renderer = utils::get_singleton_safe<CoreRenderer>();

                vk::CommandBuffer current_frame_command_buffer =
                        core_renderer.get_graphics_command_buffer_manager().get_primary_command_buffer(
                                core_renderer.get_current_frame());

                compute_manager.execute_sync_compute(current_frame_command_buffer);
            },
            .name = "sync_compute_render_pass",
            .num_of_render_contexts = 1
        };

        core_renderer.add_render_pass(render_pass_info);
    }

    core_renderer.add_callback_for_event<void*, CoreRenderer>(
    std::string{CoreRenderer::WINDOW_DESTRUCTION_EVENT}, "compute_manager_window_destruction",
    [](CoreRenderer &core_renderer, void*){
        auto &compute_manager = utils::get_singleton_safe<ComputeManager>();

        compute_manager.save_compute_cache_info();
    }, core_renderer);
}

nlohmann::json aristaeus::gfx::vulkan::compute::ComputeManager::create_and_verify_compute_cache_json(
                                                                  const std::optional<std::string> &compute_info_path) {
	if (compute_info_path.has_value()) {
		std::string compute_info_path_string = compute_info_path.value();
		std::ifstream json_input_stream(compute_info_path_string);

		if (!json_input_stream.is_open()) {
			return {};
		}

		std::string compute_info_json_string(std::istreambuf_iterator<char>{json_input_stream}, {});

		nlohmann::json return_value = nlohmann::json::parse(compute_info_json_string);

		return return_value;
	}
	else {
		return {};
	}
}

void aristaeus::gfx::vulkan::compute::ComputeManager::populate_command_buffers(
                                                                      vk::CommandBuffer &command_buffer_to_populate,
                                                                      bool is_sync_compute) {
    CoreRenderer &core_renderer = utils::get_singleton_safe<CoreRenderer>();

    if(is_sync_compute) {
        core_renderer.added_loading_tasks();
    } else {
        command_buffer_to_populate.begin(vk::CommandBufferBeginInfo{});

        m_command_buffer_manager->begin_secondary_command_buffers(core_renderer.get_current_frame(), 0, 0);

        if (Window::PERFORMANCE_MARKERS_ENABLED) {
            vk::DebugUtilsLabelEXT main_render_pass_debug_marker{ "ComputeManager pass" };
            command_buffer_to_populate.beginDebugUtilsLabelEXT(main_render_pass_debug_marker);
        }
    }

	tf::Taskflow compute_taskflow;
	uint32_t current_frame = core_renderer.get_current_frame();

	vk::DeviceSize load_buffer_size = 0;
	std::vector<std::shared_ptr<ComputeTask>> temp_submitted_loaded_compute_tasks;

	auto loading_jobs = compute_taskflow.emplace([&core_renderer, &command_buffer_to_populate, &load_buffer_size, this](tf::Subflow &subflow) {
		if (m_pending_loading_images_data.empty()) {
			return;
		}
		
		// 1st step: we need to load all the images into a shared buffer
		
		size_t buffer_size = 0;

		assert(m_pending_loading_images_data.size() == m_pending_loading_jobs.size());

		std::vector<size_t> compute_task_output_sizes;
		std::vector<unsigned char> image_data;

		std::vector<std::vector<glm::ivec3>> compute_task_stb_image_datas;

		for (size_t i = 0; m_pending_loading_images_data.size() > i; ++i) {
			std::vector<glm::ivec3> compute_task_stb_image_data;
			const std::vector<std::vector<unsigned char>> &loading_images_datas = m_pending_loading_images_data[i];
			size_t compute_task_output_size = 0;

			for (const std::vector<unsigned char> &loading_image_data : loading_images_datas) {
				compute_task_stb_image_data.emplace_back();

				glm::ivec3 &stb_image_data = compute_task_stb_image_data.back();

				stbi_set_flip_vertically_on_load(false);
				
				float *pixels = stbi_loadf_from_memory(loading_image_data.data(), loading_image_data.size(), &stb_image_data.x,
													   &stb_image_data.y, &stb_image_data.z, STBI_rgb_alpha);

				size_t image_size = stb_image_data.x * stb_image_data.y * 4 * sizeof(float);
				size_t working_offset = image_data.size();

				image_data.resize(image_data.size() + image_size);
				std::memcpy(&image_data[working_offset], pixels, image_size);

				compute_task_output_size += image_size;

				stbi_image_free(pixels);
			}

			compute_task_output_sizes.push_back(compute_task_output_size);
			compute_task_stb_image_datas.push_back(compute_task_stb_image_data);
			buffer_size += compute_task_output_sizes.back();
		}

		// 2nd step: we need to create the buffer
		m_loading_job_buffers[core_renderer.get_current_frame()] = core_renderer.get_memory_manager().create_buffer(
                core_renderer, buffer_size,
				vk::BufferUsageFlagBits::eTransferDst | vk::BufferUsageFlagBits::eTransferSrc,
                vk::SharingMode::eExclusive, VMA_ALLOCATION_CREATE_HOST_ACCESS_SEQUENTIAL_WRITE_BIT,
                VMA_MEMORY_USAGE_AUTO, "compute_manager_loading_staging_buffer");

		void *loading_data;

		vmaMapMemory(m_loading_job_buffers[core_renderer.get_current_frame()]->allocator,
                     m_loading_job_buffers[core_renderer.get_current_frame()]->allocation, &loading_data);

		std::memcpy(loading_data, image_data.data(), image_data.size() * sizeof(unsigned char));

		vmaUnmapMemory(m_loading_job_buffers[core_renderer.get_current_frame()]->allocator,
                       m_loading_job_buffers[core_renderer.get_current_frame()]->allocation);

		// 3rd step: we need to load all the images into the buffer
		assert(compute_task_stb_image_datas.size() == compute_task_output_sizes.size());
		assert(compute_task_output_sizes.size() == m_pending_loading_jobs.size());

		size_t working_buffer_offset = 0;

		for (size_t i = 0; m_pending_loading_jobs.size() > i; ++i) {
			bool success = m_pending_loading_jobs[i]->load_from_saved_output(
                                                        command_buffer_to_populate,
                                                        m_loading_job_buffers[core_renderer.get_current_frame()],
                                                        working_buffer_offset, compute_task_stb_image_datas[i]);

			if (success) {
				m_pending_loading_jobs[i]->m_job_status = ComputeTask::JobStatus::SubmittedLoaded;
				m_pending_loading_jobs[i]->m_job_finished_promise.set_value();
			}
			else {
				m_pending_loading_jobs[i]->m_job_status = ComputeTask::JobStatus::Pending;
			}

			working_buffer_offset += compute_task_output_sizes[i];
		}
	});

	auto sort_loaded_jobs = compute_taskflow.emplace([this, &core_renderer, &temp_submitted_loaded_compute_tasks] {
		for (std::shared_ptr<ComputeTask> &pending_loading_job : m_pending_loading_jobs) {
			if (pending_loading_job->m_job_status == ComputeTask::JobStatus::SubmittedLoaded) {
				temp_submitted_loaded_compute_tasks.push_back(std::move(pending_loading_job));
			}
			else {
				assert(pending_loading_job->m_job_status == ComputeTask::JobStatus::Pending);
				m_pending_jobs[core_renderer.get_current_frame()].push_back(std::move(pending_loading_job));
			}
		}

		m_pending_loading_jobs.clear();
		m_pending_loading_images_data.clear();
	});

	auto pending_jobs = compute_taskflow.for_each(m_pending_jobs[current_frame].begin(), m_pending_jobs[current_frame].end(),
		[&](std::shared_ptr<ComputeTask> &compute_task) {
		CommandBufferState current_command_buffer;

		if (is_sync_compute) {
			current_command_buffer = core_renderer.get_graphics_command_buffer_manager().
                    get_command_buffer_state_for_thread(core_renderer.get_current_render_context_id());
		}
		else {
			current_command_buffer = m_command_buffer_manager->get_command_buffer_state_for_thread(0);
		}

		current_command_buffer.begin_if_not_ready(0, 0, true);

		compute_task->dispatch(current_command_buffer.command_buffer);

		compute_task->m_job_status = ComputeTask::JobStatus::Submitted;
		if (!compute_task->m_cache_path.has_value()) {
			compute_task->m_job_finished_promise.set_value();
		}
	});

	auto create_saving_job_staging_buffer = compute_taskflow.emplace([&]() {
		vk::DeviceSize current_buffer_size = 0;

		for(std::shared_ptr<ComputeTask> &compute_task : m_saving_jobs[current_frame]) {
			current_buffer_size += compute_task->get_output_size();
		}

		if (current_buffer_size == 0) {
			m_saving_job_buffers[current_frame] = nullptr;
		}
		else {
			m_saving_job_buffers[current_frame] = core_renderer.get_memory_manager().create_buffer(
                    core_renderer, current_buffer_size, vk::BufferUsageFlagBits::eTransferDst,
                    vk::SharingMode::eExclusive, VMA_ALLOCATION_CREATE_HOST_ACCESS_SEQUENTIAL_WRITE_BIT,
                    VMA_MEMORY_USAGE_AUTO, "compute_manager_saving_staging_buffer");
		}
	});

	vk::DeviceSize working_buffer_offset = 0;

	for (size_t i = 0; m_saving_jobs[current_frame].size() > i; ++i) {
		std::shared_ptr<ComputeTask> saving_job = m_saving_jobs[current_frame][i];

		if (saving_job->m_job_status != ComputeTask::JobStatus::ReadyForSaving) {
			continue;
		}

		auto saving_job_task = compute_taskflow.emplace([current_frame, i, working_buffer_offset, &is_sync_compute,
                                                          &core_renderer, this, saving_job]() {
			CommandBufferState current_command_buffer;

			if (is_sync_compute) {
				current_command_buffer =
                    core_renderer.get_graphics_command_buffer_manager().
                    get_command_buffer_state_for_thread(core_renderer.get_current_render_context_id());
			}
			else {
				current_command_buffer = m_command_buffer_manager->get_command_buffer_state_for_thread(0);
			}

			current_command_buffer.begin_if_not_ready(0, 0, true);

			saving_job->m_job_status = ComputeTask::JobStatus::Saving;

			saving_job->save_output(current_command_buffer.command_buffer, m_saving_job_buffers[current_frame],
                                    working_buffer_offset);
		});

		working_buffer_offset += m_saving_jobs[current_frame][i]->get_output_size();

		saving_job_task.succeed(create_saving_job_staging_buffer);
	}

	loading_jobs.precede(sort_loaded_jobs);
	sort_loaded_jobs.precede(pending_jobs);

	core_renderer.get_task_manager().execute_taskflow(&compute_taskflow).wait();

	m_submitted_jobs[current_frame] = m_pending_jobs[current_frame];

	m_submitted_jobs[current_frame].insert(m_submitted_jobs[current_frame].end(), temp_submitted_loaded_compute_tasks.begin(), temp_submitted_loaded_compute_tasks.end());

	m_pending_jobs[current_frame].clear();

	if (!is_sync_compute) {
        if (Window::PERFORMANCE_MARKERS_ENABLED) {
            command_buffer_to_populate.endDebugUtilsLabelEXT();
        }

		m_command_buffer_manager->record_all_secondary_command_buffers(0, 0);

		command_buffer_to_populate.end();
	}
}

void aristaeus::gfx::vulkan::compute::ComputeManager::execute_async_compute_jobs() {
	if (!m_async_compute_queue.has_value()) {
		throw std::runtime_error("This ComputeManager was not setup for async compute yet execute_async_compute_jobs was called! (Does this device support async compute?)");
	}

    CoreRenderer &core_renderer = utils::get_singleton_safe<CoreRenderer>();

	if (m_pending_jobs[core_renderer.get_current_frame()].empty() && m_saving_jobs[core_renderer.get_current_frame()].empty() && m_pending_loading_jobs.empty()) {
		return;
	}

	// step 1: wait for previous compute jobs to finish up
	uint32_t current_frame = core_renderer.get_current_frame();
	vk::raii::Device &logical_device_ref = core_renderer.get_logical_device_ref();
	vk::CommandBuffer primary_command_buffer =
            m_command_buffer_manager->get_primary_command_buffer(current_frame);

	if (logical_device_ref.waitForFences(*m_compute_fences[current_frame], VK_TRUE, UINT64_MAX) != vk::Result::eSuccess) {
		throw std::runtime_error("Failed to wait for jobs submitted in flight to finish!");
	}

	logical_device_ref.resetFences(*m_compute_fences[current_frame]);

	// step 2: populate primary command buffer with compute jobs
	vk::SubmitInfo submit_compute_jobs{ {}, {}, primary_command_buffer, {}};

	populate_command_buffers(primary_command_buffer, false);

	m_async_compute_queue->submit(submit_compute_jobs, *m_compute_fences[current_frame]);
}

void aristaeus::gfx::vulkan::compute::ComputeManager::execute_sync_compute(
                                      vk::CommandBuffer &command_buffer_to_record_into, bool allowed_with_async_queue) {
	if (!allowed_with_async_queue && m_async_compute_queue.has_value()) {
		throw std::runtime_error("This ComputeManager was not setup for async compute yet submit_sync_compute was called with not allowing for async queues!");
	}

    CoreRenderer &core_renderer = utils::get_singleton_safe<CoreRenderer>();

	if (m_pending_jobs[core_renderer.get_current_frame()].empty()) {
		return;
	}

	populate_command_buffers(command_buffer_to_record_into, true);
}


// see disclaimer in header file for this function
void aristaeus::gfx::vulkan::compute::ComputeManager::update_submitted_jobs(std::optional<uint32_t> current_frame) {
	if (current_frame.has_value() && !m_async_compute_queue.has_value()) {
		for (std::shared_ptr<ComputeTask> &compute_task : m_submitted_jobs[current_frame.value()]) {
			compute_task->m_job_status = ComputeTask::JobStatus::Finished;
            std::cout << "compute manager sync job finished\n";
		}

		m_submitted_jobs[current_frame.value()].clear();
	} else {
		assert(m_async_compute_queue.has_value());

		for (uint32_t i = 0; utils::vulkan::MAX_FRAMES_IN_FLIGHT > i; ++i) {
			vk::Result current_compute_status = m_compute_fences[i].getStatus();

			if (m_async_compute_queue.has_value() && current_compute_status != vk::Result::eSuccess) {
				continue;
			}

			if (m_saving_job_buffers[i] != nullptr) {
				m_saving_job_buffers[i] = nullptr;
			}

			if (m_loading_job_buffers[i] != nullptr) {
				m_loading_job_buffers[i] = nullptr;
			}

			m_saving_jobs[i].erase(std::remove_if(m_saving_jobs[i].begin(), m_saving_jobs[i].end(), [this](std::shared_ptr<ComputeTask> saving_compute_task) {
				if (saving_compute_task->m_job_status == ComputeTask::JobStatus::Finished) {
					std::filesystem::path cache_filesystem_path {saving_compute_task->m_cache_path.value()};
					std::string final_directory_name = cache_filesystem_path.parent_path().filename().string();
					std::unordered_map<std::string, utils::SHA256Digest> input_file_hash_pairs;

					for (const std::string &input_file_path : saving_compute_task->m_input_file_paths) {
						std::filesystem::path filesystem_path = std::filesystem::path(input_file_path);
					
						filesystem_path = std::filesystem::weakly_canonical(filesystem_path).make_preferred();

						input_file_hash_pairs[filesystem_path.string()] = 
							utils::get_sha256_hash_file(input_file_path).value().second;
					}

					m_compute_cache_info_json[final_directory_name] = input_file_hash_pairs;
				}

				return saving_compute_task->m_job_status == ComputeTask::JobStatus::Finished;
			}), m_saving_jobs[i].end());

			for (std::shared_ptr<ComputeTask> &submitted_compute_task : m_submitted_jobs[i]) {
				if (submitted_compute_task->m_cache_path.has_value() &&  
					submitted_compute_task->m_job_status != ComputeTask::JobStatus::SubmittedLoaded) {
					m_saving_jobs[i].push_back(submitted_compute_task);
					submitted_compute_task->m_job_status = ComputeTask::JobStatus::ReadyForSaving;
				}
				else {
					submitted_compute_task->m_job_status = ComputeTask::JobStatus::Finished;
				}
			}

			m_submitted_jobs[i].clear();
		}
	}
}

size_t aristaeus::gfx::vulkan::compute::ComputeManager::add_compute_task(std::shared_ptr<ComputeTask> compute_task) {
	// check for existing compute task
	assert(compute_task != nullptr);

	if (compute_task->m_cache_path.has_value()) {
		std::filesystem::path cache_filesystem_path {std::filesystem::weakly_canonical(compute_task->m_cache_path.value()).make_preferred()};
		std::string final_directory_name = cache_filesystem_path.filename().empty() ? cache_filesystem_path.parent_path().filename().string() :
										   cache_filesystem_path.filename().string();

		bool contains_final_directory_name = m_compute_cache_info_json.contains(final_directory_name);

		bool rerun_needed = false;

		if (!contains_final_directory_name || m_compute_cache_info_json.at(final_directory_name).size() != compute_task->m_input_file_paths.size()) {
			rerun_needed = true;
		}
		else {
			std::unordered_map<std::string, utils::SHA256Digest> predicted_sha256_hash_pairs = m_compute_cache_info_json.at(final_directory_name);

			for (const std::string &input_file : compute_task->m_input_file_paths) {
				std::optional<std::pair<std::vector<unsigned char>, utils::SHA256Digest>> actual_sha256_hash_optional = utils::get_sha256_hash_file(input_file);

				std::string input_file_normalized = std::filesystem::weakly_canonical(input_file).make_preferred().string();

				if (!actual_sha256_hash_optional.has_value() || !predicted_sha256_hash_pairs.contains(input_file_normalized)) {
					// this file doesn't exist: we need to re-run this compute task
					rerun_needed = true;
					break;
				}

				utils::SHA256Digest actual_sha256_hash = actual_sha256_hash_optional.value().second;
				const utils::SHA256Digest &predicted_sha256_hash = predicted_sha256_hash_pairs.at(input_file_normalized);

				assert(predicted_sha256_hash.size() == actual_sha256_hash.size());

				for (size_t i = 0; actual_sha256_hash.size() > i; ++i) {
					if (actual_sha256_hash[i] != predicted_sha256_hash[i]) {
						// the compute task's input file hashes are different: we need to re-run this compute task
						rerun_needed = true;
						break;
					}
				}
			}
		}
			
		if (!rerun_needed) {
			std::vector<std::vector<unsigned char>> target_pending_loading_images_data;

			// TODO(Bobby): support non-cubemap loading!
			for (uint32_t i = 0; compute_task->m_number_of_array_layers * compute_task->m_number_of_mipmaps > i; ++i) {
				std::string output_file_path = compute_task->m_cache_path.value() + std::string("/") + std::to_string(i / 6) + "-" + std::to_string(i % 6) + ".hdr";

				std::ifstream output_file(output_file_path, std::ios::binary);
				std::vector<unsigned char> output_file_buffer(std::istreambuf_iterator<char>(output_file), {});

				target_pending_loading_images_data.push_back(output_file_buffer);
			}

			{
				std::lock_guard<std::mutex> pending_gpu_loading_jobs_lock_guard {m_pending_loading_jobs_lock};
				m_pending_loading_jobs.push_back(compute_task);
				m_pending_loading_images_data.push_back(target_pending_loading_images_data);
			}

			return m_pending_loading_jobs.size();
		}
	}

	uint32_t current_frame = utils::get_singleton_safe<CoreRenderer>().get_current_frame();

	{
		std::lock_guard<std::mutex> pending_gpu_jobs_lock_guard{ m_pending_job_locks[current_frame] };

		m_pending_jobs[current_frame].push_back(compute_task);
	}

	return m_pending_jobs.size() + (MAX_TASKS_PER_FRAME * current_frame);
}


std::optional<vk::raii::Queue> aristaeus::gfx::vulkan::compute::ComputeManager::get_async_compute_queue(
                                                                        std::optional<uint32_t> compute_family_index,
                                                                        std::optional<uint32_t> queue_index) {
	assert(compute_family_index.has_value() == queue_index.has_value());

    CoreRenderer &core_renderer = utils::get_singleton_safe<CoreRenderer>();

	if (queue_index.has_value() && compute_family_index.has_value() &&
        (compute_family_index != core_renderer.get_graphics_queue_family_index() || queue_index.value() != 0)) {
		return core_renderer.get_logical_device_ref().getQueue(compute_family_index.value(), queue_index.value());
	}
	else {
		return {};
	}
}

std::unique_ptr<aristaeus::gfx::vulkan::CommandBufferManager> aristaeus::gfx::vulkan::compute::ComputeManager::
                                              get_command_buffer_manager(std::optional<uint32_t> compute_family_index) {
	if (compute_family_index.has_value()) {
		return std::make_unique<CommandBufferManager>(compute_family_index.value(), true);
	}
	else {
		return nullptr;
	}
}

void aristaeus::gfx::vulkan::compute::ComputeManager::save_compute_cache_info() {
	if (m_compute_info_path.has_value()) {
		std::ofstream output_stream(m_compute_info_path.value());
		output_stream << std::setw(4) << m_compute_cache_info_json << "\n";

		output_stream.close();
	}
}