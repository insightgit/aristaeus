//
// Created by bobby on 09/10/22.
//

#ifndef ARISTAEUS_IMAGE_BUFFER_RAII_HPP
#define ARISTAEUS_IMAGE_BUFFER_RAII_HPP

#include <optional>

#ifdef _WIN32
#include <vma/vk_mem_alloc.h>
#else
#include <vk_mem_alloc.h>
#endif
#include "vulkan_raii.hpp"

namespace aristaeus::gfx::vulkan {
    class CoreRenderer;

    struct VmaBuffer {
        vk::Buffer buffer;
        size_t buffer_size;
        VmaAllocation allocation;
        VmaAllocator allocator;
        CoreRenderer &core_renderer;
    };

    struct VmaImage {
    public:
        struct ImageLayoutRequest {
            // if array layers or mip levels have no value,
            // we default to transitioning all the layers and mips.
            std::optional<uint32_t> starting_array_layer;
            std::optional<uint32_t> starting_mip_level;
            std::optional<uint32_t> array_layers;
            std::optional<uint32_t> mip_levels;
            vk::ImageLayout desired_layout;

            vk::PipelineBindPoint pipeline_type;

            // You shouldn't need to touch these much at all,
            // but just in case they are here. If one of these fields 
            // does not have a value, then we use the default for the
            // given from and to image layouts.
            std::optional<vk::AccessFlags> src_access_flags;
            std::optional<vk::PipelineStageFlags> src_stage;
            std::optional<vk::AccessFlags> dest_access_flags;
            std::optional<vk::PipelineStageFlags> dest_stage;
        };

        vk::Image image;
        uint32_t image_height;
        uint32_t image_width;
        VmaAllocation allocation;
        VmaAllocator allocator;
        uint32_t array_layers;
        uint32_t mip_levels;

        vk::Format format;
        std::vector<vk::ImageLayout> image_layouts;

        bool is_depth_format() const {
            return format == vk::Format::eD24UnormS8Uint || format == vk::Format::eD32SfloatS8Uint || format == vk::Format::eD32Sfloat;
        }

        void transition_image_layout(const vk::CommandBuffer &command_buffer, const ImageLayoutRequest &image_layout_request);
        void transition_image_layout(const vk::CommandBuffer &command_buffer, vk::ImageLayout desired_layout, vk::PipelineBindPoint pipeline_type = vk::PipelineBindPoint::eGraphics,
                                     std::optional<uint32_t> starting_array_layer = {}, std::optional<uint32_t> starting_mip_level = {}, 
                                     std::optional<uint32_t> array_layers = {}, std::optional<uint32_t> mip_levels = {}, 
                                     std::optional<vk::AccessFlags> src_access_flags = {}, std::optional<vk::PipelineStageFlags> src_stage = {}, 
                                     std::optional<vk::AccessFlags> dest_access_flags = {}, std::optional<vk::PipelineStageFlags> dest_stage = {}) {
            transition_image_layout(command_buffer, ImageLayoutRequest{
                .starting_array_layer = starting_array_layer,
                .starting_mip_level = starting_mip_level,
                .array_layers = array_layers,
                .mip_levels = mip_levels, 
                .desired_layout = desired_layout,
                .pipeline_type = pipeline_type,
                .src_access_flags = src_access_flags,
                .src_stage = src_stage,
                .dest_access_flags = dest_access_flags,
                .dest_stage = dest_stage
            });
        }

        void transition_image_layouts(const vk::CommandBuffer &command_buffer, const std::vector<ImageLayoutRequest> &image_layout_requests);
    //private:
        struct SourceAndDestinationImageInfo {
            vk::AccessFlags destination_access_flags;
            vk::PipelineStageFlags destination_stage;
            vk::AccessFlags source_access_flags;
            vk::PipelineStageFlags source_stage;
        };

        SourceAndDestinationImageInfo get_default_src_and_dest_stage(vk::ImageLayout old_image_layout, vk::ImageLayout new_image_layout, vk::PipelineBindPoint pipeline_bind_point);
    };

    struct VmaBufferDeleter {
        void operator()(VmaBuffer *buffer);
    };

    struct VmaImageDeleter {
        void operator()(VmaImage *image);
    };

    typedef std::unique_ptr<VmaBuffer, VmaBufferDeleter> BufferRaii;
    typedef std::unique_ptr<VmaImage, VmaImageDeleter> ImageRaii;
    typedef std::shared_ptr<VmaImage> SharedImageRaii;
}

#endif //ARISTAEUS_IMAGE_BUFFER_RAII_HPP
