//
// Created by bobby on 5/29/22.
//

#ifndef ARISTAEUS_TEXTURE_HPP
#define ARISTAEUS_TEXTURE_HPP

#include <functional>
#include <memory>
#include <string>

#include <stb_image.h>
#include <stb_image_write.h>
#include <taskflow/taskflow.hpp>

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/glm.hpp>

#include "vulkan_raii.hpp"
#include "memory_manager.hpp"

namespace aristaeus::gfx::vulkan {
    class CoreRenderer;

    // Texture class designed for static textures which are loaded once and then never modified again
    // this allows them to be cached and loaded only when they need to be.
    class Texture {
    public:
        static constexpr bool ENABLE_CACHE = true;

        struct TextureHash {
            size_t operator()(const Texture &texture) const noexcept {
                std::hash<std::string> hasher;

                return hasher(texture.get_image_path());
            }

            /*bool operator()(const glm::vec4 &vec_a, const glm::vec4 &vec_b) const noexcept {
                return vec_a.x == vec_b.x && vec_a.y == vec_b.y && vec_a.z == vec_b.z && vec_a.w == vec_b.w;
            }*/
        };

        struct TextureInfo {
            ImageRaii image_texture;
            std::unique_ptr<vk::raii::ImageView> image_texture_view;
            std::unique_ptr<vk::raii::Sampler> texture_sampler;
            tf::Taskflow texture_taskflow;
            bool taskflow_launched;
            vk::ImageLayout current_image_layout;
        };

        static std::unordered_map<std::string, std::weak_ptr<TextureInfo>> texture_cache;
        static std::mutex texture_cache_mutex;

        Texture(const std::string &image_path, const std::unordered_set<std::string> &texture_types, CoreRenderer &core_renderer,
                const vk::ImageViewCreateInfo &image_view_create_info,
                const vk::SamplerCreateInfo &sampler_create_info,
                std::optional<vk::CommandBuffer> create_command_buffer = {}, bool wait_for_creation = false, bool is_compute = false,
                bool delay_creation = false);


        // can't really cache these for now, as they have no file
        Texture(const std::vector<unsigned char> &pixels_vector, vk::Format format, const glm::uvec2 &image_size,
                const std::unordered_set<std::string> &texture_types, CoreRenderer &core_renderer,
                const vk::ImageViewCreateInfo &image_view_create_info,
                const vk::SamplerCreateInfo &sampler_create_info,
                std::optional<vk::CommandBuffer> create_command_buffer = {}, bool wait_for_creation = false, bool is_compute = false,
                bool delay_creation = false);

        Texture(const std::vector<unsigned char> &pixels_vector, vk::Format format, const glm::uvec2 &image_size,
                const std::unordered_set<std::string> &texture_types, CoreRenderer &core_renderer,
                std::optional<vk::CommandBuffer> create_command_buffer = {}, bool wait_for_creation = false, bool is_compute = false);

        // already have loaded image, bypasses caching
        Texture(ImageRaii &image_raii, const std::unordered_set<std::string> &texture_types,
                CoreRenderer &core_renderer, const vk::ImageViewCreateInfo &image_view_create_info,
                const vk::SamplerCreateInfo &sampler_create_info, bool is_compute = false);

        Texture(const std::string &image_path, const std::unordered_set<std::string> &texture_types, CoreRenderer &core_renderer,
                std::optional<vk::CommandBuffer> create_command_buffer = {}, bool wait_for_creation = false, bool is_compute = false);

        std::future<glm::ivec2> get_image_size() {
            return m_image_size_promise.get_future();
        }

        std::future<vk::DeviceSize> get_image_size_bytes() {
            return m_image_size_bytes_promise.get_future();
        }

        [[nodiscard]] std::string get_image_path() const {
            if(m_image_path.has_value()) {
                return m_image_path.value();
            } else {
                return "from_buffer";
            }
        }

        void add_texture_type(const std::string &texture_type) {
            m_texture_types.insert(texture_type);
            
            if (!m_is_compute && texture_type.find("compute") != std::string::npos) {
                m_is_compute = true;
            }
        }

        [[nodiscard]] std::unordered_set<std::string> get_texture_types() const {
            return m_texture_types;
        }

        void delayed_create_image() {
            assert(m_delay_creation);

            if (m_texture_info != nullptr && !m_texture_info->taskflow_launched) {
                create_image_texture();
            }
        }
        void wait_for_creation();

        void transition_pre_compute(const vk::CommandBuffer &command_buffer);
        void transition_post_compute(const vk::CommandBuffer &command_buffer, bool from_transfer_dst = false);

        glm::uvec2 get_image_texture_size() const {
            return { m_texture_info->image_texture->image_width, m_texture_info->image_texture->image_height };
        }

        [[nodiscard]] ImageRaii &get_image_texture() {
            return m_texture_info->image_texture;
        }

        [[nodiscard]] vk::DescriptorImageInfo get_descriptor_image_info(CoreRenderer &core_renderer);
    private:
        // TODO(Bobby): get rid of this after load (except for maybe pixels)
        struct CurrentLoadingData {
            vk::Format texture_format;
            int image_channels;
            int image_height;
            int image_width;
            int mipmapping_levels;
            unsigned char *pixels;
            std::vector<unsigned char> pixels_vector;
            BufferRaii staging_image_buffer;
            vk::DeviceSize image_size;
            std::optional<std::string> image_path;
            std::optional<vk::CommandBuffer> create_command_buffer;
            CoreRenderer *core_renderer;
        } m_current_loading_data{.texture_format = vk::Format::eR8G8B8A8Srgb };

        struct CallbackData {
            std::string image_path;
            Texture *texture;
        };

        void write_hdr_image_save_buffer_to_file(BufferRaii &host_buffer, const std::string &image_path);

        void create_image_texture();

        std::shared_ptr<TextureInfo> create_texture_info(CoreRenderer &core_renderer, ImageRaii image_raii,
                                                         const vk::ImageViewCreateInfo &image_view_create_info,
                                                         const vk::SamplerCreateInfo &sampler_create_info);

        std::promise<glm::ivec2> m_image_size_promise;
        std::promise<vk::DeviceSize> m_image_size_bytes_promise;
        std::optional<std::string> m_image_path;
        std::unordered_set<std::string> m_texture_types;
        bool m_is_compute;

        vk::ImageViewCreateInfo m_image_view_create_info;
        vk::SamplerCreateInfo m_sampler_create_info;

        bool m_wait_for_creation;
        bool m_delay_creation;

        std::shared_ptr<TextureInfo> m_texture_info;
    };
}

#endif //ARISTAEUS_TEXTURE_HPP
