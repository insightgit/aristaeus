#version 450

layout (location = 0) in vec3 position;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec2 texture_coords;
layout (location = 3) in vec4 diffuse_color;
layout (location = 4) in vec4 specular_color;

#define MAX_WATER_HEIGHT_MAP_SIZE_X 256
#define MAX_WATER_HEIGHT_MAP_SIZE_Z 256

/*layout (std430) buffer NewWaterHeightMap {
    float height_map_to_cpu[MAX_WATER_SIZE_Z][MAX_WATER_SIZE_Z];
};*/

const int OBJECT_TYPE_OBJECT = 0;
const int OBJECT_TYPE_GRASS = 1;
const int OBJECT_TYPE_WATER = 2;

struct MaterialConfig {
    int diffuse_maps_active;
    int normal_maps_active;
    int specular_maps_active;
    int use_reflective_map;

    float shininess;
    float reflectivity;
    float refractive_index;
    float refractive_strength;
};

struct Wave {
    int wave_active;
    float amplitude;
    float birth_period;
    vec2 center_wave;
    float death_period;
    float sharpness;
    float wavelength;
    float t_time;
    vec2 padding;
};

const int MAX_NUM_OF_WAVES = 100;

layout (set = 0, binding = 0) uniform PerspectiveCameraUBO {
    mat4 projection;
    mat4 view;
} camera_ubo;

layout (set = 2, binding = 0) uniform ModelUBO {
    int override_texture;
    int multi_texture_terrain_mode;
    int light_emitter;
    int object_type;

    vec3 light_box_color;

    Wave active_waves[MAX_NUM_OF_WAVES];
    Wave pending_waves[MAX_NUM_OF_WAVES];
    vec3 wave_world_position;
    int active_wave_count;
} model_ubo;

layout (set = 3, binding = 0) uniform MeshUBO {
    mat4 model;
    mat3 normal_transform;
    mat4 light_space_matrix;
    MaterialConfig material_config;
} mesh_ubo;

layout (location = 0) out VertexOut {
    vec4 diffuse_color_vector;
    vec4 specular_color_vector;
    vec3 normal_vector;
    mat3 tbn_matrix;
    vec2 texture_coords_out;
    vec3 world_position;
    float vertex_height;
    vec2 height_map_texture_coords;
} vertex_out;

void main() {
    vertex_out.diffuse_color_vector = diffuse_color;
    vertex_out.specular_color_vector = specular_color;

    vertex_out.world_position = (mesh_ubo.model * vec4(position, 1.0)).xyz;
    vertex_out.normal_vector = mesh_ubo.normal_transform * normal;
    vertex_out.texture_coords_out = texture_coords;

    //height_map_to_cpu[int(vertex_out.world_position.z)][int(vertex_out.world_position.x)] = vertex_out.world_position.y;

    gl_Position = camera_ubo.projection * camera_ubo.view * (vec4(vertex_out.world_position, 1.0));
}
