#include "module_entry_point.hpp"

#include "physics_actor_node.hpp"
#include "rigid_body_node.hpp"
#include "static_body_node.hpp"
#include "physics_manager.hpp"

aristaeus::physics::PhysicsManager *_physics_manager_singleton = nullptr;

extern "C" void physics_module_entry_point(const nlohmann::json &module_json) {
    auto &class_db = utils::get_singleton_safe<aristaeus::core::ClassDB>();

    // initialize compute manager singleton
    // we need to do this before registering other classes because
    // the other classes rely on the PhysicsManager registering the GeometryInfo type
    assert(_physics_manager_singleton == nullptr);

    _physics_manager_singleton = new aristaeus::physics::PhysicsManager(module_json);

    class_db.register_abstract_class<aristaeus::physics::PhysicsActorNode>();
    class_db.register_class<aristaeus::physics::RigidBodyNode>();
    class_db.register_class<aristaeus::physics::StaticBodyNode>();
}

extern "C" void physics_module_exit_point(const nlohmann::json & module_json) {
    if (_physics_manager_singleton != nullptr) {
        delete _physics_manager_singleton;
        _physics_manager_singleton = nullptr;
    }
}