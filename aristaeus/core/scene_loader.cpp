//
// Created by bobby on 12/12/21.
//

#include <ranges>

#include "../gfx/vulkan/rasterization_lighting_environment.hpp"

#include "gfx/vulkan/core_renderer.hpp"
#include "scene_loader.hpp"


const std::array<std::string, 14> aristaeus::core::SceneLoader::REQUIRED_WATER_INFO {"noise_amplitude",
                                                                           "number_of_textures", "texture_path",
                                                                           "y_spawn_threshold", "noise_influence",
                                                                           "noise_persistence",
                                                                           "number_of_perlin_octaves",
                                                                           "noise_frequency",
                                                                           "noise_frequency_increase",
                                                                           "shininess", "reflectivity",
                                                                           "refractive_index", "refractive_strength",
                                                                           "transmission"};

template<>
aristaeus::core::SceneLoader* aristaeus::core::Singleton<aristaeus::core::SceneLoader>::instance = nullptr;

void dfs(std::vector<std::string>& vec, std::shared_ptr<aristaeus::gfx::SceneGraphNode> node) {
    if (node == nullptr) return;
    // std::cout << "print 1" << vec.size() << std::endl;
    std::map<std::string, std::shared_ptr<aristaeus::gfx::SceneGraphNode>> map = node->get_map();
    for (std::pair<std::string, std::shared_ptr<aristaeus::gfx::SceneGraphNode>> pair: map)
    {
        // std::cout << "print 2" << vec.size() << std::endl;

        if (pair.second->get_class_type() == "no_op") {
            std::shared_ptr<aristaeus::gfx::NoOpNode> no_op_node = std::dynamic_pointer_cast<aristaeus::gfx::NoOpNode>(pair.second);
            if (no_op_node != nullptr && no_op_node->get_temp()) {
                std::cerr << "[WARN] " << no_op_node->get_node_path() << " temp is " << no_op_node->get_temp() << std::endl;
                vec.emplace_back(no_op_node->get_node_path());
                // std::cout << "print 3" << vec.size() << std::endl;

            }
        }
        dfs(vec, pair.second);
        // std::cout << "print 4" << vec.size() << std::endl;

    }
}

void aristaeus::core::SceneLoader::discover_modules(const nlohmann::json::value_type &module_json) {
    ModuleLoader::ModuleLoadingPolicy loading_policy = ModuleLoader::ModuleLoadingPolicy::EVERYTHING;
    if(module_json.contains("module_loading_policy")) {
        std::string loading_policy_string =
            utils::string_to_lower(module_json.at("module_loading_policy").get<std::string>());

        if(loading_policy_string == "blacklist") {
            loading_policy = ModuleLoader::ModuleLoadingPolicy::BLACKLIST;
        } else if(loading_policy_string == "whitelist") {
            loading_policy = ModuleLoader::ModuleLoadingPolicy::WHITELIST;
        } else if(loading_policy_string == "nothing") {
            loading_policy = ModuleLoader::ModuleLoadingPolicy::NOTHING;
        } else if(loading_policy_string != "everything") {
            throw std::runtime_error("Unknown module loading policy " + loading_policy_string);
        }
    }

    m_module_loader.set_module_loading_policy(loading_policy);

    if(loading_policy == ModuleLoader::ModuleLoadingPolicy::BLACKLIST ||
       loading_policy == ModuleLoader::ModuleLoadingPolicy::WHITELIST) {
        const std::string whitelist_blacklist_field_name =
            loading_policy == ModuleLoader::ModuleLoadingPolicy::BLACKLIST ? "blacklist" : "whitelist";

        if(module_json.contains(whitelist_blacklist_field_name)) {
            std::unordered_set<std::string> whitelist_blacklist;

            for(std::string module_path : module_json.at(whitelist_blacklist_field_name)) {
                whitelist_blacklist.insert(module_path);
            }

            m_module_loader.set_whitelist_blacklist(whitelist_blacklist);
        }
    }

    if(module_json.contains("module_path")) {
        m_module_loader.set_modules_path(module_json.at("module_path").get<std::string>());
    }
}

void aristaeus::core::SceneLoader::call_ready_on_scene_init() {
    std::vector<std::vector<std::reference_wrapper<gfx::SceneGraphNode>>> node_levels;

    std::queue<std::reference_wrapper<gfx::SceneGraphNode>> current_node_level;
    std::queue<std::reference_wrapper<gfx::SceneGraphNode>> next_node_level;

    assert(m_scenegraph_node_root != nullptr);
    current_node_level.emplace(*m_scenegraph_node_root);

    while(!current_node_level.empty()) {
        node_levels.emplace_back();

        while(!current_node_level.empty()) {
            gfx::SceneGraphNode &current_node = current_node_level.front();

            node_levels.back().emplace_back(current_node);

            for(std::shared_ptr<gfx::SceneGraphNode> &child : current_node.get_children()) {
                assert(child != nullptr);

                next_node_level.push(*child);
            }

            current_node_level.pop();
        }

        current_node_level = next_node_level;
        next_node_level = std::queue<std::reference_wrapper<gfx::SceneGraphNode>>{};
    }

    for(int i = static_cast<int>(node_levels.size()) - 1; i >= 0; --i) {
        for(std::reference_wrapper<gfx::SceneGraphNode> node : node_levels[i]) {
            node.get().call_event<void*>(std::string{gfx::SceneGraphNode::TREE_READY_EVENT}, nullptr);
        }
    }

    call_event<void*>(std::string{SCENE_LOADING_EVENT}, nullptr);
}

void aristaeus::core::SceneLoader::load_environment_json(const std::string &environment_path, const std::string &environment_name) {
    m_environment_path = environment_path;
    m_environment_name = "bistro/bistro";

    std::ifstream ifstream(environment_path + "/" + m_environment_name + ".asc.json");
    std::string json_content((std::istreambuf_iterator<char>(ifstream)),
                             (std::istreambuf_iterator<char>() ));

    ifstream.close();

    m_environment_json = nlohmann::json::parse(json_content);

    if (m_environment_json.contains("runtime_modules")) {
        discover_modules(m_environment_json.at("runtime_modules"));
    }
}

std::unordered_set<std::string> aristaeus::core::SceneLoader::get_desired_vulkan_extensions() const {
    std::unordered_set<std::string> desired_vulkan_extensions;

    for (std::pair<const std::string, ModuleLoader::Module> &module_pair : m_module_loader.get_module_candidates()) {
        desired_vulkan_extensions.insert(desired_vulkan_extensions.end(), module_pair.second.required_vulkan_device_extensions.begin());
    }

    return desired_vulkan_extensions;
}

aristaeus::gfx::vulkan::PerspectiveCameraEnvironment aristaeus::core::SceneLoader::init_environment_json(float aspect_ratio, const vk::Viewport &viewport, const vk::Rect2D &viewport_scissor,
                                                                                                         const std::unordered_set<std::string> &vulkan_device_extensions_loaded) {
    std::vector<glm::vec3> capture_points;
    vk::Extent2D capture_resolution{ 1000, 1000 };
    auto &core_renderer = utils::get_singleton_safe<gfx::vulkan::CoreRenderer>();
    gfx::vulkan::LightingEnvironment::Fog fog{ .fog_active = 0 };
    std::optional<std::string> caustic_environment_map_path{};
    float point_light_shadow_bias = 0.25f;

    std::optional<std::pair<glm::ivec2, int32_t>> brdf_convolution_info{};
    gfx::vulkan::PBRMaterialInfo pbr_material_info{};
    int shadow_mode = 1;

    if (m_environment_json["lighting"].contains("point_light_shadow_bias")) {
        point_light_shadow_bias = m_environment_json.at("lighting").at("point_light_shadow_bias").get<float>();
    }

    if (m_environment_json["lighting"].contains("shadow_mode")) {
        shadow_mode = m_environment_json["lighting"].at("shadow_mode").get<int>();
    }

    if (m_environment_json["lighting"].contains("fog") &&
        m_environment_json["lighting"]["fog"].contains("active") &&
        m_environment_json["lighting"]["fog"]["active"]) {
        fog = parse_fog_info(m_environment_json["lighting"]["fog"]);
    }

    if (m_environment_json["lighting"].contains("caustic_environment_map_path")) {
        caustic_environment_map_path =
            m_environment_path + "/" + std::string(m_environment_json["lighting"]["caustic_environment_map_path"]);
    }

    if (m_environment_json["lighting"].contains("captured_skybox") &&
        m_environment_json["lighting"]["captured_skybox"].contains("enabled") &&
        m_environment_json["lighting"]["captured_skybox"]["enabled"] &&
        m_environment_json["lighting"]["captured_skybox"].contains("points")) {
        for (const nlohmann::json::value_type &point_json :
            m_environment_json["lighting"]["captured_skybox"]["points"]) {
            capture_points.push_back(get_vec3_json(point_json));
        }

        if (m_environment_json["lighting"]["captured_skybox"].contains("resolution")) {
            glm::vec2 capture_resolution_vec =
                get_vec2_json(m_environment_json["lighting"]["captured_skybox"]["resolution"]);

            capture_resolution.width = static_cast<uint32_t>(capture_resolution_vec.x);
            capture_resolution.height = static_cast<uint32_t>(capture_resolution_vec.y);
        }
    }

    if (m_environment_json["lighting"].contains("pbr_settings")) {
        nlohmann::json::value_type pbr_settings = m_environment_json["lighting"]["pbr_settings"];

        if (pbr_settings.contains("brdf_samples") && pbr_settings.contains("brdf_map_size")) {
            brdf_convolution_info = { get_vec2_json(pbr_settings.at("brdf_map_size")), pbr_settings.at("brdf_samples") };
        }
        else {
            throw std::runtime_error("No BRDF convolution info was given for a PBR environment!");
        }

        if (pbr_settings.contains("default_pbr_info")) {
            nlohmann::json::value_type default_pbr_info = pbr_settings["default_pbr_info"];

            if (default_pbr_info.contains("albedo")) {
                pbr_material_info.albedo = get_vec4_json(default_pbr_info["albedo"]);
            }

            if (default_pbr_info.contains("roughness")) {
                pbr_material_info.roughness = default_pbr_info["roughness"];
            }

            if (default_pbr_info.contains("metallic")) {
                pbr_material_info.metallic = default_pbr_info["metallic"];
            }

            if (default_pbr_info.contains("ambient")) {
                pbr_material_info.ao = default_pbr_info["ambient"];
            }
        }
    }
    else {
        throw std::runtime_error("No PBR lighting settings were given!");
    }

    bool deferred_priority_renderer = m_environment_json["renderer_priority"] != "priority_forward";
    bool screen_space_interpolation = 
        m_environment_json.contains("screen_space_interpolation") ? m_environment_json.at("screen_space_interpolation").get<bool>() : false;

    gfx::vulkan::PerspectiveCameraEnvironment return_value_environment =
        parse_perspective_camera_environment(m_environment_json["camera"], core_renderer, aspect_ratio);
    std::optional<std::string> default_lighting_env_tag;
    std::unordered_set<std::string> lighting_env_tags;
    size_t num_of_point_light_shadows = get_number_of_point_light_shadows();

    // loading of LightingEnvironments
    std::unique_ptr<gfx::vulkan::LightingEnvironment> lighting_env =
        std::make_unique<gfx::vulkan::RasterizationLightingEnvironment>(pbr_material_info, capture_points, capture_resolution, 
            capture_points.size(), num_of_point_light_shadows, std::optional<vk::Extent2D>{}, std::optional<vk::Viewport>{}, deferred_priority_renderer, false);
    gfx::vulkan::RasterizationLightingEnvironment &rasterization_lighting_env = *static_cast<gfx::vulkan::RasterizationLightingEnvironment*>(lighting_env.get());

    if (caustic_environment_map_path.has_value()) {
        rasterization_lighting_env.set_caustic_environment_map(caustic_environment_map_path);
    }

    rasterization_lighting_env.set_point_light_shadow_bias(point_light_shadow_bias);
    rasterization_lighting_env.set_shadow_mode(shadow_mode);
    rasterization_lighting_env.set_fog(fog);

    std::string lighting_env_tag = lighting_env->get_lighting_env_tag();

    return_value_environment.attach_lighting_environment(lighting_env);

    default_lighting_env_tag = lighting_env_tag;

    lighting_env_tags.insert(lighting_env_tag);

    ImGuiIO &io = ImGui::GetIO();
    io.Fonts->AddFontDefault();
    io.Fonts->Build();

    engine_module_entry_point(m_environment_json.contains("module_options") ? m_environment_json.at("module_options") :
        nlohmann::json{});

    if (m_environment_json.contains("runtime_modules")) {
        m_module_loader.load_module_candidates(vulkan_device_extensions_loaded);
    }

    m_scenegraph_node_root = std::make_shared<gfx::NoOpNode>(false);

    m_scenegraph_node_root->rename_node("root");
    m_scenegraph_node_root->set_weak_ptr_to_this(m_scenegraph_node_root);

    m_scenegraph_node_root->call_event<void *>(std::string{ gfx::SceneGraphNode::ENTER_TREE_EVENT }, nullptr);

    if (m_environment_json.contains("scene_tree") && m_environment_json.at("scene_tree").contains("nodes")) {
        /*bool disallow = false;

        if (m_environment_json.at("scene_tree").contains("disallow_automatic_no_op_nodes")) {
            disallow = m_environment_json.at("scene_tree").at("disallow_automatic_no_op_nodes");
        }*/

        nlohmann::json::value_type nodes_json = m_environment_json.at("scene_tree").at("nodes");

        for (nlohmann::json::iterator node_json = nodes_json.begin(); node_json != nodes_json.end(); ++node_json) {
            parse_and_create_node(node_json.value(), node_json.key());
        }

        // use root to dfs through the tree and look for temporary nodes. if true then we throw error
        std::vector<std::string> vec{};
        dfs(vec, m_scenegraph_node_root);
        std::string error_msg;
        for (size_t i = 0; i < vec.size(); i++) {
            error_msg += vec.at(i);
            if (i != vec.size() - 1) {
                error_msg += ", ";
            }
        }
        if (vec.size() > 0) {
            std::cerr << "[WARN] Temporary Nodes found in Scene Tree when disallowed" << "\n";
            // throw std::runtime_error("Temporary nodes detected at " + error_msg + "!"); // handle exception correctly?
        }
    }
    else {
        throw std::runtime_error("Missing scene_tree and node details in " + m_environment_name + "!");
    }

    assert(default_lighting_env_tag.has_value());
    assert(!lighting_env_tags.empty());
    assert(m_scenegraph_node_root != nullptr);

    m_default_lighting_environment = *default_lighting_env_tag;

    return_value_environment.set_scene_graph_root_node(m_scenegraph_node_root);
    return_value_environment.set_default_lighting_env_tag(m_default_lighting_environment);

    std::cout << "[SCENE] Loaded environment " << m_environment_name << "\n";

    call_ready_on_scene_init();

    return return_value_environment;
}

aristaeus::gfx::vulkan::PerspectiveCameraEnvironment
        aristaeus::core::SceneLoader::parse_perspective_camera_environment(
                const nlohmann::json::value_type &camera_environment_json, aristaeus::gfx::vulkan::CoreRenderer &core_renderer, float aspect_ratio) {
    bool enable_up_down_movement = true;
    float fov = 55.0f;
    float mouse_sensitivity = 1.0f;
    float z_near = 0.1f;
    float z_far = 100.0f;
    gfx::vulkan::PerspectiveCameraEnvironment::DirectionalLightShadowsInfo shadows_info{ .enabled = false };

    if(camera_environment_json.contains("fov")) {
        fov = camera_environment_json.at("fov");
    }

    if(camera_environment_json.contains("z_near")) {
        z_near = camera_environment_json.at("z_near");
        z_near = abs(z_near);
    }

    if(camera_environment_json.contains("z_far")) {
        z_far = camera_environment_json.at("z_far");
        z_far = abs(z_far);
    }

    if(camera_environment_json.contains("mouse_sensitivity")) {
        mouse_sensitivity = camera_environment_json.at("mouse_sensitivity");
    }

    if(camera_environment_json.contains("enable_up_down_movement")) {
        enable_up_down_movement = camera_environment_json.at("enable_up_down_movement");
    }

    if (camera_environment_json.contains("directional_shadows")) {
        nlohmann::json shadows_json = camera_environment_json.at("directional_shadows");

        shadows_info.enabled = shadows_json.at("enabled").get<bool>();
        shadows_info.follow_player_camera = shadows_json.at("follow_player").get<bool>();
        
        for (size_t i = 0; shadows_info.orthographic_matrix.size() > i; ++i) {
            shadows_info.orthographic_matrix[i] = shadows_json.at("orthographic_matrix")[i];
        }
        
        shadows_info.eye_position = utils::get_vec3_json(shadows_json.at("eye_position"));
        shadows_info.target_position = utils::get_vec3_json(shadows_json.at("target_position"));
    }

    gfx::vulkan::PerspectiveCameraEnvironment perspective_camera_env{ core_renderer, fov, aspect_ratio, z_near, z_far, mouse_sensitivity, enable_up_down_movement, shadows_info };

    if (camera_environment_json.contains("start_position")) {
        perspective_camera_env.set_camera_position(utils::get_vec3_json(camera_environment_json.at("start_position")));
    }

    return perspective_camera_env;
}

std::vector<glm::vec2> aristaeus::core::SceneLoader::parse_vec2_array(
        const nlohmann::json::value_type &root_json, const std::string &json_key) {
    std::vector<glm::vec2> return_value;

    if(!root_json.contains(json_key)) {
        return return_value;
    }

    for(const nlohmann::json::value_type &vec2_json : root_json[json_key]) {
        return_value.push_back(get_vec2_json(vec2_json));
    }

    return return_value;
}

aristaeus::gfx::vulkan::LightingEnvironment::Fog
aristaeus::core::SceneLoader::parse_fog_info(const nlohmann::json::value_type &fog_json) {
    if(!fog_json.contains("attenuation") || !fog_json.contains("color") ||
       !fog_json.contains("mode") || !fog_json.contains("start") || !fog_json.contains("end")) {
        std::cerr << "[SCENE] Ignoring incomplete fog info in environment " << m_environment_name << "!\n";

        return {.fog_active = 0};
    }

    nlohmann::json::value_type attenuation_json = fog_json["attenuation"];
    gfx::vulkan::LightingEnvironment::Fog current_fog {.fog_active = 1};

    if(attenuation_json.contains("quad")) {
        current_fog.quadratic_attenuation = attenuation_json["quad"];
    }

    if(attenuation_json.contains("line")) {
        current_fog.linear_attenuation = attenuation_json["line"];
    }

    if(attenuation_json.contains("const")) {
        current_fog.constant_attenuation = attenuation_json["const"];
    }

    current_fog.fog_color = get_vec3_json(fog_json["color"]);
    current_fog.fog_mode = fog_json["mode"];
    current_fog.fog_start = fog_json["start"];
    current_fog.fog_end = fog_json["end"];

    return current_fog;
}

size_t aristaeus::core::SceneLoader::get_number_of_point_light_shadows() {
    if(m_environment_json.contains("lighting") && m_environment_json["lighting"].contains("point_light_shadows") &&
       m_environment_json["lighting"]["point_light_shadows"].contains("enabled") &&
       m_environment_json["lighting"]["point_light_shadows"]["enabled"] && 
       m_environment_json["lighting"]["point_light_shadows"].contains("shadow_limit")) {
        return m_environment_json["lighting"]["point_light_shadows"]["shadow_limit"].get<size_t>();
    } else {
        return 0;
    }
}

void aristaeus::core::SceneLoader::parse_and_create_prefab(const nlohmann::json::value_type &prefab_node_sub_json,
                                                           const std::string &prefab_node_path) {
    if(!prefab_node_sub_json.contains("prefab_file")) {
        throw std::runtime_error("Prefab at " + prefab_node_path + " is incomplete!");
    }

    std::string prefab_file_path = prefab_node_sub_json.at("prefab_file").get<std::string>();

    // TODO(Bobby): Make it if possible so we aren't copying the json value
    std::optional<nlohmann::json> prefab_json = m_prefab_cache.get(prefab_file_path, {});

    if(!prefab_json.has_value()) {
        std::ifstream ifstream(m_environment_path + "/" + prefab_file_path);
        std::string json_content((std::istreambuf_iterator<char>(ifstream)),
                                 (std::istreambuf_iterator<char>() ));

        ifstream.close();

        m_prefab_cache.put({prefab_file_path, nlohmann::json::parse(json_content)}, {});
        prefab_json = m_prefab_cache.get(prefab_file_path, {});
    }

    assert(prefab_json.has_value());

    if(!prefab_json->contains("tree")) {
        throw std::runtime_error("Prefab at " + prefab_node_path + " is incomplete!");
    }

    // TODO(Bobby): Have someway to store prefab metadata in SceneGraphNode
    for (nlohmann::json::iterator node_json = prefab_json->at("tree").begin();
         node_json != prefab_json->at("tree").end(); ++node_json) {
        std::string rel_prefab_path = node_json.key();

        if(prefab_node_sub_json.contains("prefab_overrides") &&
           prefab_node_sub_json.at("prefab_overrides").contains(node_json.key())) {
            nlohmann::json new_node_json_value = node_json.value();

            new_node_json_value.merge_patch(prefab_node_sub_json.at("prefab_overrides").at(node_json.key()));

            parse_and_create_node(new_node_json_value, rel_prefab_path, prefab_node_path);
        } else {
            parse_and_create_node(node_json.value(), rel_prefab_path, prefab_node_path);
        }
    }
}

void aristaeus::core::SceneLoader::parse_and_create_node(nlohmann::json::value_type &node_sub_json,
                                                         const std::string &node_path,
                                                         const std::string &root_path) {
    if (!node_sub_json.contains("type")) {
        std::cerr << "[WARN] Skipping parsing incomplete node!\n";
        return;
    }

    std::string abs_node_path = gfx::SceneGraphNode::concatenate_node_paths({root_path, node_path});

    std::pair<std::string, std::string> parent_node_path_and_node_name =
        gfx::SceneGraphNode::get_parent_node_path_and_child_name(abs_node_path);

    std::shared_ptr<gfx::SceneGraphNode> scene_graph_node;

    bool non_essential_node = false;

    while(scene_graph_node == nullptr) {
        node_sub_json["node_name"] = parent_node_path_and_node_name.second;

        if(node_sub_json.contains("non_essential_node")) {
            non_essential_node = node_sub_json.at("non_essential_node").get<bool>();
        }

        try {
            std::string node_type = node_sub_json.at("type").get<std::string>();

            if(node_type == PREFAB_NODE_TYPE) {
                parse_and_create_prefab(node_sub_json, abs_node_path);
                return;
            } else {
                scene_graph_node.reset(core::ClassDB::instance->parse_node(node_sub_json));
            }
        } catch(ClassDB::UnrecognizedNodeTypeException &unrecognized_node_type_exception) {
            if(node_sub_json.contains("fallback_node") && node_sub_json.at("fallback_node").contains("type")) {
                std::string old_node_type = node_sub_json.at("type");

                node_sub_json = node_sub_json.at("fallback_node");

                std::cout << "Couldn't load node " << abs_node_path << " with type " << old_node_type
                          << ": falling back to fallback node type " << node_sub_json.at("type").get<std::string>()
                          << "\n";
            } else if(non_essential_node) {
                return;
            } else {
                throw unrecognized_node_type_exception;
            }
        }
    }

    scene_graph_node->set_weak_ptr_to_this(scene_graph_node);

    std::optional<std::reference_wrapper<gfx::SceneGraphNode>> parent_node =
            m_scenegraph_node_root->get_node_with_path(parent_node_path_and_node_name.first, true);

    if (parent_node.has_value()) {
        parent_node->get().add_child(scene_graph_node);
        std::cout << "adding child " << scene_graph_node->get_node_name() << " to parent " << parent_node.value().get().get_node_name() << std::endl;
    }
    else {
        throw std::runtime_error(scene_graph_node->get_node_path() + " is not a valid node path for " + scene_graph_node->get_node_name());
    }
}
