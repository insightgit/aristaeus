#ifndef ARISTAEUS_GEOMETRY_HPP
#define ARISTAEUS_GEOMETRY_HPP

#include "../assimp_mesh.hpp"

#include "../image_buffer_raii.hpp"

namespace aristaeus::gfx::vulkan {
	class Geometry {
	public:
		static constexpr float PI = 3.1415926535;

		Geometry(CoreRenderer &core_renderer, const std::vector<ModelVertex> &model_vertices, const std::vector<uint32_t> &indices, 
				 const std::string &mesh_generation_info, const ModelInfo &model_info) :
				m_assimp_mesh(create_assimp_mesh_from_vertices(core_renderer, model_vertices, indices, mesh_generation_info)), 
				m_model_info(model_info) {}

		std::shared_ptr<AssimpMesh> get_produced_assimp_mesh() {
			return std::move(m_assimp_mesh);
		}

		const ModelInfo &get_model_info() const {
			return m_model_info;
		}
	private:
		std::shared_ptr<AssimpMesh> create_assimp_mesh_from_vertices(CoreRenderer &core_renderer, const std::vector<ModelVertex> &model_vertices,
																	 const std::vector<uint32_t> &indices, const std::string &mesh_generation_info) {
			std::vector<glm::vec3> vertices;

			for (const ModelVertex &model_vertex : model_vertices) {
				model_vertex.pack_model_vertex(vertices);
			}

			return std::make_shared<AssimpMesh>(vertices, indices, mesh_generation_info);
		}

		std::shared_ptr<AssimpMesh> m_assimp_mesh;
		ModelInfo m_model_info;
	};
}

#endif