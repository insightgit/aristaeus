#ifndef ARISTAEUS_GPU_PERLIN_NOISE_TERRAIN_HPP
#define ARISTAEUS_GPU_PERLIN_NOISE_TERRAIN_HPP

#include <gfx/vulkan/terrain.hpp>

#include "gpu_perlin_noise.hpp"

namespace aristaeus::gfx::vulkan::compute {
    class GPUPerlinNoiseTerrain : public Terrain {
    public:
        GPUPerlinNoiseTerrain(float height_magnitude, float vertex_resolution, GPUPerlinNoise &height_map_texture_job,
                          const std::map<glm::vec4, std::vector<Image>, utils::Vec4Hash> &terrain_texture_paths_height,
                          int number_of_textures, ObjectType object_type, PropInfo &prop_info,
                          const std::string &node_name,
                          const std::unordered_map<Image, glm::vec4> &terrain_texture_paths_slope = {},
                          uint32_t number_of_patches = 60, std::optional<vk::CommandBuffer> create_command_buffer = {});

        ~GPUPerlinNoiseTerrain() = default;

        void predraw_load(const vk::CommandBuffer &command_buffer, const glm::vec3 &camera_position)override;
    private:
        std::shared_ptr<GPUPerlinNoise> m_height_map_texture_job;
    };
}

#endif //ARISTAEUS_GPU_PERLIN_NOISE_TERRAIN_HPP
