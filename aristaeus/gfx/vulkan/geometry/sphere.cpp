#include "../core_renderer.hpp"

#include "sphere.hpp"

aristaeus::gfx::vulkan::Geometry aristaeus::gfx::vulkan::Sphere::create_geometry(CoreRenderer &core_renderer, size_t stack_count, size_t sector_count, float radius, const ModelInfo &model_info) {
	// based on https://www.songho.ca/opengl/gl_sphere.html
	std::vector<ModelVertex> vertices;
	std::vector<uint32_t> indices;

	std::string mesh_generation_info = "sphere-" + std::to_string(stack_count) + "-" + std::to_string(sector_count);

	float stack_increment = (PI / 2.0f) / stack_count;
	float sector_increment = (2.0f * PI) / sector_count;
	int current_index = 0;

	for (float stack = -PI / 2.0f; PI / 2.0f > stack; stack += stack_increment) {
		for (float sector = 0.0f; 2.0f * PI > sector; sector += sector_increment) {
			ModelVertex vertex;

			vertex.normal = glm::vec3(cosf(stack) * cosf(sector), cosf(stack) * sinf(sector), sinf(stack));

			vertex.position = radius * vertex.normal;
			vertex.texture_coords = glm::vec3((sector / sector_increment) / sector_count, (stack / stack_increment) / stack_count, 0);

			vertex.normal = glm::normalize(-vertex.normal);

			vertices.push_back(vertex);

			if (stack < (PI / 2.0f) - stack_increment) {
				indices.push_back(current_index);
				indices.push_back(current_index + sector_count + 1);
				indices.push_back(current_index + sector_count);
			}

			if (stack != -PI / 2.0f) {
				indices.push_back(current_index);
				indices.push_back(current_index + 1);
				indices.push_back(current_index + sector_count + 1);
			}

			++current_index;
		}
	}

	return { core_renderer, vertices, indices, mesh_generation_info, model_info };
}