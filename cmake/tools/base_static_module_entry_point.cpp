#include <engine_module_entry_point.hpp>

//!PUT_INCLUDES_HERE!

namespace aristaeus {
    void static_module_entry_points(const nlohmann::json &module_options_json) {
        //!PUT_ENTRY_POINTS_HERE!
    }

    void static_module_exit_points(const nlohmann::json &module_options_json) {
        //!PUT_EXIT_POINTS_HERE!
    }

    //!PUT_STATIC_MODULE_LIST_HERE!
}
