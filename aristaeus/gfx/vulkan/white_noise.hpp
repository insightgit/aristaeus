#ifndef ARISTAEUS_WHITE_NOISE_HPP
#define ARISTAEUS_WHITE_NOISE_HPP

#include "core_renderer.hpp"
#include "assimp_model.hpp"

namespace aristaeus::gfx::vulkan {
    class WhiteNoise {
    public:
        WhiteNoise(std::mt19937 &rng) : m_rng(rng) {}

        Texture to_white_noise_texture(const glm::ivec2 &resolution,
                                       std::optional<vk::CommandBuffer> create_command_buffer = {},
                                       bool wait_for_creation = false) {
            auto &core_renderer = utils::get_singleton_safe<CoreRenderer>();
            std::vector<unsigned char> noise;

            std::uniform_real_distribution<float> white_noise_distribution{ 0.0f, 1.0f };

            noise.resize(4 * sizeof(float) * resolution.x * resolution.y);
            float current_noise_offset = 0;

            for (int y = 0; resolution.y > y; ++y) {
                for (int x = 0; resolution.x > x; ++x) {
                    float rand_1 = white_noise_distribution(m_rng);
                    float rand_2 = white_noise_distribution(m_rng);
                    float rand_3 = white_noise_distribution(m_rng);
                    float rand_4 = white_noise_distribution(m_rng);

                    std::memcpy(&noise[current_noise_offset], &rand_1, sizeof(float));
                    std::memcpy(&noise[current_noise_offset + sizeof(float)], &rand_2, sizeof(float));
                    std::memcpy(&noise[current_noise_offset + (2 * sizeof(float))], &rand_3, sizeof(float));
                    std::memcpy(&noise[current_noise_offset + (3 * sizeof(float))], &rand_4, sizeof(float));

                    current_noise_offset += 4 * sizeof(float);
                }
            }
            vk::ImageViewCreateInfo image_view_create_info = AssimpModel::DEFAULT_IMAGE_VIEW_CREATE_INFO;
            vk::SamplerCreateInfo sampler_create_info = AssimpModel::DEFAULT_SAMPLER_CREATE_INFO;

            sampler_create_info.maxAnisotropy = core_renderer.get_max_sampler_anisotropy();

            return Texture{ noise, vk::Format::eR32G32B32A32Sfloat, glm::uvec2{resolution}, 
                            std::unordered_set<std::string>{"texture_white_noise"}, core_renderer,
                            image_view_create_info, sampler_create_info, create_command_buffer, wait_for_creation };
        }
    private:
        std::mt19937 &m_rng;
    };
}

#endif