#ifndef ARISTAEUS_MODEL_INFO_HPP
#define ARISTAEUS_MODEL_INFO_HPP

#include <optional>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/glm.hpp>

namespace aristaeus::gfx::vulkan {
    struct PBRMaterialInfo {
        alignas(4) float roughness;
        alignas(4) float metallic;
        alignas(4) float ao;
        alignas(4) int using_gltf_metallicroughness_texture;

        alignas(8) glm::ivec2 albedo_map_range;
        alignas(8) glm::ivec2 normal_map_range;
        alignas(8) glm::ivec2 metallic_map_range;
        alignas(8) glm::ivec2 roughness_map_range;
        alignas(8) glm::ivec2 ao_map_range;

        alignas(16) glm::vec4 albedo;
        alignas(16) glm::vec3 normal;
        alignas(16) glm::vec3 world_position;
        alignas(16) glm::vec3 tangent;
    };

    struct TextureOverride {
        std::unordered_set<std::string> texture_types;
        std::string texture_path;
    };

    struct MaterialOverride {
        std::string override_name;
        float override_value;
    };

    struct ModelInfo {
        PBRMaterialInfo pbr_material_info;
        bool material_color_override_textures;
        std::string model_path;
        std::string model_node_name;
        float reflectivity;
        float refractive_index;
        float refractive_strength;
        float shininess;
        float transmission;
        std::unordered_map<std::string, std::vector<MaterialOverride>> material_overrides;
        bool use_front_culling_shadow_mapping;
        std::optional<std::string> default_normal_map_path;
        std::optional<std::string> default_displacement_map_path;
        std::vector<TextureOverride> texture_overrides;
        bool debug_normals;
        bool bake_physx_shapes;
    };

    static const ModelInfo DEFAULT_MODEL_INFO = {
            .shininess = -1.0f,
            .use_front_culling_shadow_mapping = true,
            .bake_physx_shapes = true
    };

    struct ModelVertex {
        glm::vec3 position;
        glm::vec3 normal;
        glm::vec3 texture_coords;
        glm::vec3 tangent;

        void pack_model_vertex(std::vector<glm::vec3> &vertex_array) const {
            vertex_array.push_back(position);
            vertex_array.push_back(normal);
            vertex_array.push_back(texture_coords);
            vertex_array.push_back(tangent);
        }
    };
}

#endif