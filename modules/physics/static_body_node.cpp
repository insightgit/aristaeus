#include "static_body_node.hpp"

ARISTAEUS_CLASS_IMPL(aristaeus::physics::StaticBodyNode,
	aristaeus::core::StdTypeInfoRef(typeid(aristaeus::physics::PhysicsActorNode)))

void aristaeus::physics::StaticBodyNode::classdb_register() {
	auto &class_db = utils::get_singleton_safe<core::ClassDB>();

	class_db.register_event_callback_on_construction<physics::StaticBodyNode, void*>(
            std::string{ gfx::SceneGraphNode::PARSING_DONE_EVENT }, "rigid_body_parsing_done",
            [](StaticBodyNode &static_body_node, void*) {
		static_body_node.on_parsing_done();
	});
}

void aristaeus::physics::StaticBodyNode::on_parsing_done() {
	auto &physics_manager = utils::get_singleton_safe<PhysicsManager>();

	m_rigid_static = physics_manager.get_physics().createRigidStatic(scene_transform_to_physx_transform(get_transform()));
	m_actor = m_rigid_static;
    m_actor->setGlobalPose(scene_transform_to_physx_transform(get_parent_transform() + get_transform()));

	call_event<void*>(std::string{ physics::PhysicsActorNode::ACTOR_UPDATED_EVENT }, nullptr);
}