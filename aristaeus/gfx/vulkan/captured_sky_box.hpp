//
// Created by bobby on 22/12/22.
//

#ifndef ARISTAEUS_CAPTURED_SKY_BOX_HPP
#define ARISTAEUS_CAPTURED_SKY_BOX_HPP

#include <vector>

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/glm.hpp>

#include "core_renderer.hpp"
#include "cube_map_texture.hpp"

namespace aristaeus::gfx::vulkan {
    class PerspectiveCameraEnvironment;

    class CapturedSkyBox {
    public:
        // view order: +x, -x, +y, -y, +z, -z
        // pitch, yaw
        // 0.0f, 0.0f
        // 180.0f, 0.0f
        // -90.0f, 0.0f
        // 90.0f, 0.0f
        // 0.0f, -90.0f
        // 0.0f, 90.0f
        static constexpr std::array<float, 6> PITCH_DEGREES {{0.0f, 180.0f, 90.0f, -90.0f, 0.0f, 0.0f}};
        static constexpr std::array<float, 6> YAW_DEGREES {{0.0f, 0.0f, 0.0f, 0.0f, 90.0f, -90.0f}};

        CapturedSkyBox(const std::vector<glm::vec3> &initial_capture_points, size_t capture_point_capacity,
                       const vk::Extent2D &cubemap_resolution, bool depth_only);

        [[nodiscard]] vk::DescriptorImageInfo get_descriptor_image_info(size_t capture_point_index);

        [[nodiscard]] std::vector<vk::DescriptorImageInfo> get_all_descriptor_image_infos();

        void force_recapture();

        std::vector<glm::vec3> get_capture_points() const {
            return m_capture_points;
        }

        size_t get_capture_point_capacity() const {
            return m_capture_points_capacity;
        }

        bool has_been_captured() const {
            return m_uncaptured_points.empty();
        }

        // When a CapturedSkyBox's capture point is attached to a node, a recapture is forced 
        // whenever the node's position changes.
        void attach_capture_point_to_node(const glm::vec3 &capture_point, gfx::SceneGraphNode &scene_graph_node);

        void update_capture_point(const glm::vec3 &capture_point, uint32_t index);

        // capture_points always re-captures all capture points in m_capture_points whereas capture_needed_points
        // only captures the points that need to be captured (e.g. in the event of a position update with an attached
        // capture point, or when the application starts) and is a no-op if there are no points that need to be captured.
        void capture_points();

        void capture_needed_points();
    private:
        struct CapturedSkyBoxContextInfo {
            size_t capture_point;
            std::reference_wrapper<CapturedSkyBox> sky_box;
        };

        struct CapturePointInfo {
            std::string render_pass_name;
            std::array<vk::raii::ImageView, utils::vulkan::MAX_FRAMES_IN_FLIGHT> image_views;
        };

        static constexpr std::string_view CAPTURED_SKY_BOX_PHASE_NAME = "CapturedSkyBoxPhase";

        vk::raii::Semaphore create_captured_semaphore() {
            vk::SemaphoreCreateInfo captured_semaphore_create_info {};
            vk::raii::Semaphore captured_semaphore { CoreRenderer::instance->get_logical_device_ref(),
                                                     captured_semaphore_create_info, nullptr};

            return captured_semaphore;
        }

        [[nodiscard]] std::string get_render_pass_name(size_t capture_point_index) const {
            return "CapturedSkyBox-" + std::to_string(capture_point_index) + "-" + std::to_string(reinterpret_cast<uintptr_t>(this));
        }

        std::vector<CapturePointInfo> construct_render_passes();

        static void captured_sky_box_render_callback(void *arg_ptr);

        static void captured_sky_box_before_phase_callback(CapturedSkyBox *sky_box);
        static void captured_sky_box_predraw_load(void *sky_box_ptr);
        static void captured_sky_box_predraw(void*);

        static void captured_sky_box_after_phase_callback(CapturedSkyBox *sky_box);

        // because of this strong ref, this part will need to be refactored before CapturedSkyBox becomes a node
        std::unordered_map<std::shared_ptr<gfx::SceneGraphNode>, size_t> m_attached_capture_points; // links a scene graph node to a capture point index in m_capture_points
        std::vector<glm::vec3> m_capture_points;

        std::unordered_set<size_t> m_uncaptured_points;
        std::vector<VmaImage::ImageLayoutRequest> m_uncaptured_image_layout_requests;

        size_t m_capture_points_capacity;
        vk::Extent2D m_cubemap_resolution;

        bool m_depth_only;

        // TODO(Bobby): Put all render passes into one image with different layers for each render pass if possible
        // TODO(Bobby): for increased efficency
        // these two vectors are symmetric with m_capture_points
        vk::raii::Sampler m_cubemap_sampler; // shared cubemap sampler settings
        std::vector<CapturedSkyBoxContextInfo> m_captured_sky_box_context_infos;
        std::optional<std::string> m_render_phase_attached_to;
        bool m_captured_sky_box_phase_created = false;
        std::vector<CapturePointInfo> m_capture_point_infos;

        bool m_first_capture_time = true;

        vk::raii::Semaphore m_captured_semaphore;
        bool m_captured;
    };
} // vulkan

#endif //ARISTAEUS_CAPTURED_SKY_BOX_HPP
