#ifndef ARISTAEUS_VULKAN_PIPELINE_HPP
#define ARISTAEUS_VULKAN_PIPELINE_HPP

#include <mutex>
#include <unordered_map>

#include "vulkan_raii.hpp"

namespace aristaeus::gfx::vulkan {
	class VulkanPipeline {
    public:
        struct ShaderInfo {
            std::vector<char> code;
            std::string name;
            vk::ShaderStageFlagBits stage;

            bool operator==(const ShaderInfo& shader_info_b) const noexcept {
                return this->code == shader_info_b.code;
            }
        };
	protected:
        struct ShaderInfoHash {
            size_t operator()(const ShaderInfo& shader_info) const noexcept {
                size_t shader_code_hash = 0;

                for (char code_byte : shader_info.code) {
                    shader_code_hash += code_byte;
                }

                return shader_code_hash;
            }
        };

        typedef std::unordered_map<ShaderInfo, std::weak_ptr<vk::raii::ShaderModule>, ShaderInfoHash> WeakShaderModuleCache;

        static WeakShaderModuleCache shader_module_cache;
        static std::mutex shader_module_cache_mutex;

        std::vector<vk::raii::DescriptorSetLayout> create_descriptor_set_layouts(vk::raii::Device &logical_device,
            const std::vector<std::vector<vk::DescriptorSetLayoutBinding>> &descriptor_set_bindings) {
            std::vector<vk::raii::DescriptorSetLayout> descriptor_set_layouts;

            for (const std::vector<vk::DescriptorSetLayoutBinding> &descriptor_binding : descriptor_set_bindings) {
                descriptor_set_layouts.push_back({ logical_device, {{}, descriptor_binding} });
            }

            return descriptor_set_layouts;
        }

        std::unique_ptr<vk::raii::PipelineLayout> create_pipeline_layout_object(vk::raii::Device &logical_device, const std::vector<vk::PushConstantRange> &push_content_ranges,  
                                                                                const std::vector<vk::raii::DescriptorSetLayout> &descriptor_set_layouts) {
            std::vector<vk::DescriptorSetLayout> descriptor_set_layouts_non_raii{ descriptor_set_layouts.size() };

            for (size_t i = 0; descriptor_set_layouts.size() > i; ++i) {
                descriptor_set_layouts_non_raii[i] = *descriptor_set_layouts[i];
            }

            vk::PipelineLayoutCreateInfo pipeline_layout_info{
                static_cast<vk::PipelineLayoutCreateFlags>(0), descriptor_set_layouts_non_raii, push_content_ranges
            };

            return std::make_unique<vk::raii::PipelineLayout>(logical_device, pipeline_layout_info);
        }
	};
}

#endif