//
// Created by bobby on 03/03/23.
//

#ifndef ARISTAEUS_TASK_MANAGER_HPP
#define ARISTAEUS_TASK_MANAGER_HPP

#include <array>

#include <taskflow/taskflow.hpp>

#include "../../utils.hpp"

namespace aristaeus::gfx::vulkan {
    class TaskManager {
    public:
        TaskManager();

        tf::Future<void> execute_taskflow(tf::Taskflow *taskflow) {
            return m_task_executor.run(*taskflow);
        }

        tf::Future<void> execute_loading_taskflow(tf::Taskflow *loading_taskflow) {
            return m_loader_executor.run(*loading_taskflow);
        }

        void execute_pre_render_taskflow(tf::Taskflow *taskflow, uint32_t current_frame) {
            m_pre_render_tasks[current_frame].insert({taskflow, m_task_executor.run(*taskflow)});
        }

        void wait_for_pre_render_taskflow(tf::Taskflow *taskflow, uint32_t current_frame) {
            if(m_pre_render_tasks[current_frame].contains(taskflow)) {
                m_pre_render_tasks[current_frame][taskflow].wait();
            }
        }

        // not the cleanest thing, get rid of this at some point
        tf::Executor &get_executor() {
            return m_task_executor;
        }

        void wait_and_clear_pre_render_taskflows(uint32_t current_frame);
    private:
        tf::Executor m_task_executor;
        tf::Executor m_loader_executor;
        std::array<std::unordered_map<tf::Taskflow*, tf::Future<void>>, utils::vulkan::MAX_FRAMES_IN_FLIGHT>
            m_pre_render_tasks;
    };
}

#endif //ARISTAEUS_TASK_MANAGER_HPP
