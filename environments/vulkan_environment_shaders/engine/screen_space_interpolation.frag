#version 450

layout (location = 0) in vec2 uv_coords;
layout (location = 0) out vec4 color_out;

layout (input_attachment_index = 0, set = 1, binding = 0) uniform subpassInput position_screen_texture;
layout (input_attachment_index = 1, set = 1, binding = 1) uniform subpassInput normal_screen_texture;

layout(set = 1, binding = 2) uniform sampler2D low_res_position_screen_texture;
layout(set = 1, binding = 3) uniform sampler2D low_res_normal_screen_texture;
layout(set = 1, binding = 4) uniform sampler2D low_res_composition_texture;

layout(std430, set = 1, binding = 5) writeonly buffer OcclusionTestBuffer {
    uint passing_occlusion[];
} occlusion_buffer;

layout (push_constant) uniform OcclusionQueryConstants {
	layout(offset = 16) mat4 screen_space_transform;
    uint quad_index;
} constants;

const float MAX_POSITION_TOLERANCE = 0.5f;
const float MAX_NORMAL_TOLERANCE = 0.05f;

void main() {
    vec3 new_world_position = subpassLoad(position_screen_texture).xyz;
    vec3 new_normal = subpassLoad(normal_screen_texture).xyz;

    vec3 old_world_position = texture(low_res_position_screen_texture, uv_coords).xyz;
    vec3 old_normal = texture(low_res_normal_screen_texture, uv_coords).xyz;

    float world_position_difference = length(new_world_position - old_world_position);
    float normal_difference = length(new_normal - old_normal);

    if(world_position_difference > MAX_POSITION_TOLERANCE || normal_difference > MAX_NORMAL_TOLERANCE) {
        occlusion_buffer.passing_occlusion[constants.quad_index] = 0;
        color_out = vec4(0, 0, 0, 1);
    } else {
        // i take in the new_world_position, but i actually want to interpolate based on screen space positions.
        // therefore, new_world_position must be in clip space, as top_left and bottom_right must also.

        vec3 top_left_corner_color = textureOffset(low_res_composition_texture, uv_coords, ivec2(0, 0)).rgb;
        vec3 top_right_corner_color = textureOffset(low_res_composition_texture, uv_coords, ivec2(1, 0)).rgb;
        vec3 bottom_left_corner_color = textureOffset(low_res_composition_texture, uv_coords, ivec2(0, 1)).rgb;
        vec3 bottom_right_corner_color = textureOffset(low_res_composition_texture, uv_coords, ivec2(1, 1)).rgb;

        vec4 frag_pos_four_vec = constants.screen_space_transform * vec4(new_world_position, 1);
        vec4 top_left_four_vec = constants.screen_space_transform * vec4(textureOffset(low_res_position_screen_texture, uv_coords, ivec2(0, 0)).xyz, 1);
        vec4 bottom_right_four_vec = constants.screen_space_transform * vec4(textureOffset(low_res_position_screen_texture, uv_coords, ivec2(1, 1)).xyz, 1);

        vec2 frag_pos = vec2(frag_pos_four_vec.x / frag_pos_four_vec.w, frag_pos_four_vec.y / frag_pos_four_vec.w);
        vec2 top_left = vec2(top_left_four_vec.x / top_left_four_vec.w, top_left_four_vec.y / top_left_four_vec.w);
        vec2 bottom_right = vec2(bottom_right_four_vec.x / bottom_right_four_vec.w, bottom_right_four_vec.y / bottom_right_four_vec.w);

        vec2 position_difference = bottom_right - top_left;

        vec3 x1_color_interp = (((bottom_right.x - frag_pos.x) / position_difference.x) * top_left_corner_color) + 
                               (((frag_pos.x - top_left.x) / position_difference.x) * top_right_corner_color);
        vec3 x2_color_interp = (((bottom_right.x - frag_pos.x) / position_difference.x) * bottom_left_corner_color) + 
                               (((frag_pos.x - top_left.x) / position_difference.x) * bottom_right_corner_color);

        vec3 y_interp = (((bottom_right.y - frag_pos.y) / position_difference.y) * x1_color_interp) + 
                        (((frag_pos.y - top_left.y) / position_difference.y) * x2_color_interp);

        color_out = vec4(y_interp, 1.0);
    }
}