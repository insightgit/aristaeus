//
// Created by bobby on 5/23/22.
//

#include <limits>

#include <GLFW/glfw3.h>
#include "core_renderer.hpp"
#include "window.hpp"
#include "imgui_impl_vulkan.h"

#ifdef NVIDIA_AFTERMATH
//#include <GFSDK_Aftermath.h>
//#include <GFSDK_Aftermath_GpuCrashDump.h>
#endif

const std::array<std::string, 2> aristaeus::gfx::vulkan::Window::DEVICE_EXTENSIONS = {
        VK_KHR_SWAPCHAIN_EXTENSION_NAME,
        VK_KHR_SHADER_NON_SEMANTIC_INFO_EXTENSION_NAME
};

const std::array<std::string, 1> aristaeus::gfx::vulkan::Window::VALIDATION_LAYERS = {
        "VK_LAYER_KHRONOS_validation"
};


PFN_vkVoidFunction aristaeus::gfx::vulkan::Window::vulkan_function_loader(const char* function_name,
                                                                          void* user_data) {
    auto instance = reinterpret_cast<vk::raii::Instance*>(user_data);

    return instance->getProcAddr(function_name);
}

aristaeus::gfx::vulkan::Window::Window(bool prefer_integrated_gpu, bool prefer_sync_compute, const std::unordered_set<std::string> &desired_vulkan_extensions) :
                                                                     m_prefer_sync_compute(prefer_sync_compute),
                                                                     m_vulkan_instance(create_vulkan_instance()),
                                                                     m_debug_messenger(create_debug_messenger()),
                                                                     m_window_surface(create_window_surface()),
                                                                     m_physical_device(select_physical_device(prefer_integrated_gpu, prefer_sync_compute, desired_vulkan_extensions)),
                                                                     m_queue_indices_info(find_queue_indices(m_physical_device, prefer_sync_compute)) {}

vk::raii::Instance aristaeus::gfx::vulkan::Window::create_vulkan_instance() {
    std::vector<const char *> all_required_extensions;

    vk::ApplicationInfo application_info("Aristaeus", VK_MAKE_VERSION(1, 2, 0), "No Engine",
                                         VK_MAKE_VERSION(1, 2, 0), VK_API_VERSION_1_2);
    vk::InstanceCreateInfo create_info = vk::InstanceCreateInfo({}, &application_info);

    glfwInit();

    glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
    glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE);

    //m_glfw_window.reset(glfwCreateWindow(1920, 1080, "Aristaeus Vulkan renderer", nullptr, nullptr));

    all_required_extensions = get_required_extensions();

    create_info.enabledExtensionCount = static_cast<uint32_t>(all_required_extensions.size());
    create_info.ppEnabledExtensionNames = all_required_extensions.data();

    if(VALIDATION_LAYERS_ENABLED && !check_validation_layer_support()) {
        throw std::runtime_error("Validation layers requested, but not available!");
    } else if(VALIDATION_LAYERS_ENABLED) {
        std::array<const char *, 1> validation_layers_cstrings = {VALIDATION_LAYERS[0].c_str()};

        std::cout << "Validation layers:";

        for(const char *validation_layer : validation_layers_cstrings) {
            std::cout << validation_layer << ", ";
        }

        std::cout << "\n";

        create_info.enabledLayerCount = static_cast<uint32_t>(VALIDATION_LAYERS.size());
        create_info.ppEnabledLayerNames = validation_layers_cstrings.data();

        create_info.pNext = reinterpret_cast<const VkDebugUtilsMessengerCreateInfoEXT*>(&DEBUG_CALLBACK_CREATION_INFO);
    } else {
        create_info.enabledLayerCount = 0;

        create_info.pNext = nullptr;
    }

    try {
        vk::raii::Instance new_vulkan_instance {m_raii_context, create_info};

        // imgui setup
        IMGUI_CHECKVERSION();
        ImGui::CreateContext();

        ImGuiIO &io = ImGui::GetIO();

        io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;
        io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;

        ImGui::StyleColorsLight();

        glfwInit();

        glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
        glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE);

        auto vkGetInstanceProcAddr = m_dynamic_loader.getProcAddress<PFN_vkGetInstanceProcAddr>("vkGetInstanceProcAddr");

        VULKAN_HPP_DEFAULT_DISPATCHER.init(vkGetInstanceProcAddr);
        VULKAN_HPP_DEFAULT_DISPATCHER.init(*new_vulkan_instance);

        m_glfw_window.reset(glfwCreateWindow(1920, 1080, "Aristaeus", nullptr, nullptr));

        ImGui_ImplGlfw_InitForVulkan(m_glfw_window.get(), true);

        ImGui_ImplVulkan_LoadFunctions(vulkan_function_loader, &new_vulkan_instance);

        std::cout << "Created Vulkan instance successfully!\n";

        return new_vulkan_instance;
    } catch(vk::SystemError &system_error) {
        throw std::runtime_error("Couldn't create Vulkan instance!");
    }
}

bool aristaeus::gfx::vulkan::Window::check_validation_layer_support() {
    std::vector<vk::LayerProperties> available_layers = m_raii_context.enumerateInstanceLayerProperties();

    for(size_t i = 0; VALIDATION_LAYERS.size() > i; ++i) {
        bool layer_found = false;

        for(const vk::LayerProperties &layer_properties : available_layers) {
            if(VALIDATION_LAYERS[i] != layer_properties.layerName) {
                layer_found = true;
                break;
            }
        }

        if(!layer_found) {
            return false;
        }
    }

    return true;
}

std::unique_ptr<vk::raii::DebugUtilsMessengerEXT> aristaeus::gfx::vulkan::Window::create_debug_messenger() {
    if(VALIDATION_LAYERS_ENABLED) {
        static const vk::DebugUtilsMessengerCreateInfoEXT debug_callback_creation_info = {
                static_cast<vk::DebugUtilsMessengerCreateFlagsEXT>(0),
                static_cast<vk::DebugUtilsMessageSeverityFlagsEXT>(VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT |
                VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT |
                VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT),
                static_cast<vk::DebugUtilsMessageTypeFlagsEXT>(VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT |
                VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT),
                debug_callback, nullptr
        };


        return std::make_unique<vk::raii::DebugUtilsMessengerEXT>(m_vulkan_instance,
                                                                  debug_callback_creation_info, nullptr);
    } else {
        return {};
    }
}

VkBool32 aristaeus::gfx::vulkan::Window::debug_callback(VkDebugUtilsMessageSeverityFlagBitsEXT message_severity,
                                                        VkDebugUtilsMessageTypeFlagsEXT message_type,
                                                        const VkDebugUtilsMessengerCallbackDataEXT *callback_data,
                                                        void *user_data) {
    std::string message_type_string;
    std::string severity_string = "?";

    switch(message_type) {
        case VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT:
            message_type_string = "general";
            break;
        case VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT:
            message_type_string = "validation";
            break;
        case VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT:
            message_type_string = "performance";
            break;
        default:
            message_type_string = "";
            break;
    }

    switch(message_severity) {
        case VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT:
            severity_string = "verbose";
            break;
        case VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT:
            severity_string = "info";
            break;
        case VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT:
            severity_string = "warning";
            break;
        case VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT: {
            static constexpr std::string_view loader_message_type = "Loader Message";

            if (strncmp(callback_data->pMessageIdName, loader_message_type.data(), loader_message_type.size()) == 0) {
                severity_string = "loader_error";
            } else {
                #ifndef NDEBUG
                if (callback_data->messageIdNumber == 1303270965) {
                    constexpr std::string_view VK_IMAGE_STRING = "VkImage";

                    std::string vulkan_error_message{ callback_data->pMessage };
                    size_t found_image = vulkan_error_message.find(std::string{ VK_IMAGE_STRING });

                    assert(found_image != std::string::npos);
                    std::stringstream hex_conversion_string_stream;

                    for (int i = 0; 16 > i; ++i) {
                        hex_conversion_string_stream << std::hex << vulkan_error_message[found_image + VK_IMAGE_STRING.size() + 3 + i];
                    }

                    uintptr_t vk_image_ptr;
                    hex_conversion_string_stream >> vk_image_ptr;

                    std::string image_purpose = CoreRenderer::instance->get_memory_manager().get_image_purpose(
                                                                               reinterpret_cast<VkImage>(vk_image_ptr));

                    severity_string = "error";

                    // invalid image layout transition  
                    std::cerr << "from validation layer: invalid image layout transition on image " << image_purpose << "\n";
                }
                else {
                    severity_string = "error";
                }
                #endif
            }
            break;
        }
        default:
            break;
    }

    
    std::cerr << "from validation layer (type: " << message_type_string << ", severity: " << severity_string
        << "): " << callback_data->pMessage << "\n";

    // we do not want to abort the Vulkan call that triggered this message
    return VK_FALSE;
}

vk::raii::PhysicalDevice aristaeus::gfx::vulkan::Window::select_physical_device(bool prefer_integrated_gpu, bool prefer_sync_compute, 
                                                                                const std::unordered_set<std::string> &desired_vulkan_extensions) {
    vk::raii::PhysicalDevices physical_devices(m_vulkan_instance);
    std::multimap<uint32_t, size_t> physical_device_candidates;

    if(physical_devices.empty()) {
        throw std::runtime_error("Couldn't find any Vulkan-supported GPUs!");
    }

    for(size_t i = 0; physical_devices.size() > i; ++i) {
        physical_device_candidates.insert(std::make_pair(physical_device_suitability_score(physical_devices[i], prefer_integrated_gpu, 
                                                                                           prefer_sync_compute, desired_vulkan_extensions), i));
    }

    if(physical_device_candidates.rbegin()->first > 0) {
        size_t candidate_index = physical_device_candidates.rbegin()->second;

        std::cout << "Found suitable Vulkan-supported GPU!\n";
        std::cout << "Using GPU " <<
            physical_devices[candidate_index].getProperties().deviceName << "\n";

        std::vector<vk::ExtensionProperties> available_device_extensions =
            physical_devices[candidate_index].enumerateDeviceExtensionProperties();

        for (const vk::ExtensionProperties &device_extension : available_device_extensions) {
            m_loaded_device_extensions.insert(device_extension.extensionName);
        }

        return physical_devices[candidate_index];
    } else {
        throw std::runtime_error("failed to find a suitable Vulkan-supported GPU!");
    }
}

int aristaeus::gfx::vulkan::Window::physical_device_suitability_score(const vk::raii::PhysicalDevice &physical_device,
                                                                      bool prefer_integrated_gpu, bool prefer_sync_compute, 
                                                                      const std::unordered_set<std::string> &desired_vulkan_extensions) {
    int return_value = 0;

    vk::PhysicalDeviceFeatures device_features = physical_device.getFeatures();
    vk::PhysicalDeviceProperties device_properties = physical_device.getProperties();
    SwapChainSupportDetails swap_chain_support_details = get_swap_chain_support(physical_device);

    if(device_properties.deviceType != vk::PhysicalDeviceType::eDiscreteGpu &&
       device_properties.deviceType != vk::PhysicalDeviceType::eIntegratedGpu) {
        return 0;
    }

    return_value += static_cast<int>(device_properties.limits.maxImageDimension2D);

    QueueIndicesInfo queue_indices_info = find_queue_indices(physical_device, prefer_sync_compute);

    if(!queue_indices_info.is_complete()) {
        // no graphics, compute and presentation queues found, this device is not suitable!
        return 0;
    }

    int device_extension_support_score = check_device_extension_support(physical_device, desired_vulkan_extensions);

    if(device_extension_support_score < 0) {
        // No swap chain? <insert megamind meme here>
        return 0;
    }

    return_value += device_extension_support_score;

    if(swap_chain_support_details.surface_formats.empty() || swap_chain_support_details.presentation_modes.empty()) {
        // Swap chain found is not adequate
        return 0;
    }

    if(!device_features.samplerAnisotropy) {
        // Device doesn't support anisotropy in samplers
        return 0;
    }

    if(!device_features.tessellationShader) {
        // device doesn't support tessellation shaders
        return 0;
    }

    if (device_properties.limits.maxPushConstantsSize < 128) {
        // push constant size smaller than supported
        return 0;
    }

    #ifndef NDEBUG
    if(!device_features.geometryShader) {
        return 0;
    }
    #endif

    if (!device_features.fragmentStoresAndAtomics) {
        return 0;
    }

    if(prefer_integrated_gpu && device_properties.deviceType == vk::PhysicalDeviceType::eIntegratedGpu) {
        return_value += 10000;
    }

    if (!prefer_sync_compute && !queue_indices_info.supports_async_compute()) {
        return_value += 10000;
    }

    return return_value;
}

aristaeus::gfx::vulkan::Window::SwapChainSupportDetails
aristaeus::gfx::vulkan::Window::get_swap_chain_support(const vk::raii::PhysicalDevice &physical_device) {
    return {
            .surface_capabilities = physical_device.getSurfaceCapabilitiesKHR(*m_window_surface),
            .surface_formats = physical_device.getSurfaceFormatsKHR(*m_window_surface),
            .presentation_modes = physical_device.getSurfacePresentModesKHR(*m_window_surface)
    };
}

int aristaeus::gfx::vulkan::Window::check_device_extension_support(const vk::raii::PhysicalDevice &physical_device, std::unordered_set<std::string> desired_extensions) {
    std::vector<vk::ExtensionProperties> available_device_extensions =
            physical_device.enumerateDeviceExtensionProperties();
    std::set<std::string> required_device_extensions(DEVICE_EXTENSIONS.begin(), DEVICE_EXTENSIONS.end());

    #ifndef NDEBUG
        required_device_extensions.insert(VK_KHR_SHADER_NON_SEMANTIC_INFO_EXTENSION_NAME);
    #endif

    int desired_extension_sum = 0;

    for(const vk::ExtensionProperties &available_device_extension : available_device_extensions) {
        if (desired_extensions.contains(available_device_extension.extensionName)) {
            desired_extensions.erase(available_device_extension.extensionName);
            desired_extension_sum += 10000;
        }

        required_device_extensions.erase(available_device_extension.extensionName);
    }

    if (!required_device_extensions.empty()) {
        return -1;
    }

    return desired_extension_sum;
}

vk::raii::SurfaceKHR aristaeus::gfx::vulkan::Window::create_window_surface() {
    glfwSetFramebufferSizeCallback(m_glfw_window.get(), frame_buffer_resize_callback);
    glfwSetWindowUserPointer(m_glfw_window.get(), this);

    auto instance_ptr = static_cast<VkInstance>(*m_vulkan_instance);

    create_glfw_window_surface(instance_ptr, m_glfw_window.get(), &m_window_surface_ptr);

    return {m_vulkan_instance, m_window_surface_ptr};
}

aristaeus::gfx::vulkan::Window::QueueIndicesInfo
aristaeus::gfx::vulkan::Window::find_queue_indices(const vk::raii::PhysicalDevice &physical_device, bool prefer_sync_compute) {
    QueueIndicesInfo return_value;
    unsigned int i = 0;
    std::vector<vk::QueueFamilyProperties> queue_families = physical_device.getQueueFamilyProperties();

    for(const vk::QueueFamilyProperties &queue_family : queue_families) {
        if(queue_family.queueFlags & vk::QueueFlagBits::eGraphics && !return_value.graphics_family.has_value()) {
            return_value.graphics_family = i;
        }

        // TODO(Bobby): need to deal with edge case where async compute queue is on another queue family
        if (queue_family.queueFlags & vk::QueueFlagBits::eCompute) {
            if (return_value.graphics_family.has_value() && return_value.graphics_family.value() == i && queue_family.queueCount > 1 && !prefer_sync_compute) {
                return_value.compute_queue_index = 1;
            } else {
                return_value.compute_queue_index = 0;
            }

            return_value.compute_family = i;
        }

        if(physical_device.getSurfaceSupportKHR(i, *m_window_surface) && !return_value.presentation_family.has_value()) {
            return_value.presentation_family = i;
        }

        i++;
    }

    return return_value;
}

vk::raii::Device aristaeus::gfx::vulkan::Window::create_logical_device() {
    static constexpr std::array<float, 2> QUEUE_PRIORITIES{ {GRAPHICS_QUEUE_PRIORITY, COMPUTE_QUEUE_PRIORITY} };

    if(!m_queue_indices_info.is_complete()) {
        throw std::runtime_error("No graphics queue and presentation queue found!");
    }

    bool sync_compute = m_prefer_sync_compute || !m_queue_indices_info.supports_async_compute();

    vk::PhysicalDeviceDescriptorIndexingFeatures descriptor_indexing_features{};
    vk::PhysicalDeviceFeatures2 device_features;
    std::vector<vk::DeviceQueueCreateInfo> queue_create_infos;
    std::vector<const char *> device_extensions_cstring(DEVICE_EXTENSIONS.size());
    std::vector<const char *> validation_layers_cstring(VALIDATION_LAYERS.size());

    device_features.pNext = &descriptor_indexing_features;

    device_features.features.samplerAnisotropy = VK_TRUE;
    device_features.features.tessellationShader = VK_TRUE;
    device_features.features.fragmentStoresAndAtomics = VK_TRUE;

    device_features.features.independentBlend = VK_TRUE;

    descriptor_indexing_features.runtimeDescriptorArray = VK_TRUE;

    for(size_t i = 0; DEVICE_EXTENSIONS.size() > i; ++i) {
        device_extensions_cstring[i] = DEVICE_EXTENSIONS[i].c_str();
    }

    #ifndef NDEBUG
        device_features.features.geometryShader = VK_TRUE;

        device_extensions_cstring.push_back(VK_KHR_SHADER_NON_SEMANTIC_INFO_EXTENSION_NAME);
    #endif

    vk::DeviceCreateInfo logical_device_create_info;
    logical_device_create_info.pNext = &device_features;
    logical_device_create_info.enabledExtensionCount = static_cast<uint32_t>(device_extensions_cstring.size()),
    logical_device_create_info.ppEnabledExtensionNames = device_extensions_cstring.data();

    std::unordered_set<uint32_t> queue_indices { m_queue_indices_info.graphics_family.value(),
                                                 m_queue_indices_info.presentation_family.value()};

    bool compute_queue_accounted = false;

    for (const uint32_t& queue_index : queue_indices) {
        vk::DeviceQueueCreateInfo queue_create_info;
        int queue_count = 1;

        if (!compute_queue_accounted && m_queue_indices_info.compute_family.value() == queue_index) {
            if (m_queue_indices_info.compute_queue_index.value() != 0) {
                ++queue_count;
            }

            compute_queue_accounted = true;
        }

        queue_create_info.queueFamilyIndex = queue_index;
        queue_create_info.queueCount = queue_count;
        queue_create_info.pQueuePriorities = QUEUE_PRIORITIES.data();

        queue_create_infos.push_back(queue_create_info);
    }

    if (!sync_compute && !compute_queue_accounted) {
        vk::DeviceQueueCreateInfo compute_queue_create_info{ {}, m_queue_indices_info.compute_family.value(), 1, & COMPUTE_QUEUE_PRIORITY };

        queue_create_infos.push_back(compute_queue_create_info);
    }

    logical_device_create_info.queueCreateInfoCount = queue_create_infos.size();
    logical_device_create_info.pQueueCreateInfos = queue_create_infos.data();

    if(VALIDATION_LAYERS_ENABLED) {
        for(size_t i = 0; VALIDATION_LAYERS.size() > i; ++i) {
            validation_layers_cstring[i] = VALIDATION_LAYERS[i].c_str();
        }

        logical_device_create_info.enabledLayerCount = static_cast<uint32_t>(VALIDATION_LAYERS.size());
        logical_device_create_info.ppEnabledLayerNames = validation_layers_cstring.data();
    }

    vk::raii::Device logical_device(m_physical_device, logical_device_create_info);

    std::cout << "Created logical device successfully!\n";

    #ifdef NVIDIA_AFTERMATH
        /*AFTERMATH_CHECK_ERROR(GFSDK_Aftermath_EnableGpuCrashDumps(
                GFSDK_Aftermath_Version_API,
                GFSDK_Aftermath_GpuCrashDumpWatchedApiFlags_DX,
                GFSDK_Aftermath_GpuCrashDumpFeatureFlags_Default,   // Default behavior.
                GpuCrashDumpCallback,                               // Register callback for GPU crash dumps.
                ShaderDebugInfoCallback,                            // Register callback for shader debug information.
                CrashDumpDescriptionCallback,                       // Register callback for GPU crash dump description.
                ResolveMarkerCallback,                              // Register callback for marker resolution (R495 or later NVIDIA graphics driver).
                &m_gpuCrashDumpTracker));                           // Set the GpuCrashTracker object as user data passed back by the above callbacks.*/
    #endif
    return logical_device;
}

aristaeus::gfx::vulkan::Window::VmaAllocatorRaii
        aristaeus::gfx::vulkan::Window::create_allocator(vk::raii::Device &logical_device) {
    auto allocator = new VmaAllocator();
    VmaAllocatorRaii allocator_raii;

    VmaVulkanFunctions vulkan_functions = get_vma_vulkan_functions();

    VmaAllocatorCreateInfo allocator_info {
            .physicalDevice = *m_physical_device,
            .device = *logical_device,
            .pVulkanFunctions = &vulkan_functions,
            .instance = *m_vulkan_instance,
            .vulkanApiVersion = VK_API_VERSION_1_0
    };

    if(vmaCreateAllocator(&allocator_info, allocator) != VK_SUCCESS) {
        throw std::runtime_error("Couldn't create VMA allocator!");
    }

    allocator_raii.reset(allocator);

    std::cout << "Created allocator successfully!\n";

    return allocator_raii;
}

VmaVulkanFunctions aristaeus::gfx::vulkan::Window::get_vma_vulkan_functions() {
    return {
        .vkGetInstanceProcAddr = m_dynamic_loader.getProcAddress<PFN_vkGetInstanceProcAddr>("vkGetInstanceProcAddr"),
        .vkGetDeviceProcAddr = m_dynamic_loader.getProcAddress<PFN_vkGetDeviceProcAddr>("vkGetDeviceProcAddr"),
        .vkGetPhysicalDeviceProperties = m_dynamic_loader.getProcAddress<PFN_vkGetPhysicalDeviceProperties>(
                                                                                       "vkGetPhysicalDeviceProperties"),
        .vkGetPhysicalDeviceMemoryProperties =
                m_dynamic_loader.getProcAddress<PFN_vkGetPhysicalDeviceMemoryProperties>(
                                                                                 "vkGetPhysicalDeviceMemoryProperties"),
        .vkAllocateMemory = m_dynamic_loader.getProcAddress<PFN_vkAllocateMemory>("vkAllocateMemory"),
        .vkFreeMemory = m_dynamic_loader.getProcAddress<PFN_vkFreeMemory>("vkFreeMemory"),
        .vkMapMemory = m_dynamic_loader.getProcAddress<PFN_vkMapMemory>("vkMapMemory"),
        .vkUnmapMemory = m_dynamic_loader.getProcAddress<PFN_vkUnmapMemory>("vkUnmapMemory"),
        .vkFlushMappedMemoryRanges = m_dynamic_loader.getProcAddress<PFN_vkFlushMappedMemoryRanges>(
                                                                                        "vkFlushMappedMemoryRanges"),
        .vkInvalidateMappedMemoryRanges = m_dynamic_loader.getProcAddress<PFN_vkInvalidateMappedMemoryRanges>(
                                                                                   "vkInvalidateMappedMemoryRanges"),
        .vkBindBufferMemory = m_dynamic_loader.getProcAddress<PFN_vkBindBufferMemory>("vkBindBufferMemory"),
        .vkBindImageMemory = m_dynamic_loader.getProcAddress<PFN_vkBindImageMemory>("vkBindImageMemory"),
        .vkGetBufferMemoryRequirements =
                m_dynamic_loader.getProcAddress<PFN_vkGetBufferMemoryRequirements>("vkGetBufferMemoryRequirements"),
        .vkGetImageMemoryRequirements = m_dynamic_loader.getProcAddress<PFN_vkGetImageMemoryRequirements>
                                                                                       ("vkGetImageMemoryRequirements"),
        .vkCreateBuffer = m_dynamic_loader.getProcAddress<PFN_vkCreateBuffer>("vkCreateBuffer"),
        .vkDestroyBuffer = m_dynamic_loader.getProcAddress<PFN_vkDestroyBuffer>("vkDestroyBuffer"),
        .vkCreateImage = m_dynamic_loader.getProcAddress<PFN_vkCreateImage>("vkCreateImage"),
        .vkDestroyImage = m_dynamic_loader.getProcAddress<PFN_vkDestroyImage>("vkDestroyImage"),
        .vkCmdCopyBuffer = m_dynamic_loader.getProcAddress<PFN_vkCmdCopyBuffer>("vkCmdCopyBuffer"),


        // vulkan 1.1/1.3 support
        .vkGetBufferMemoryRequirements2KHR = m_dynamic_loader.
                getProcAddress<PFN_vkGetBufferMemoryRequirements2KHR>("vkGetBufferMemoryRequirements2"),
        .vkGetImageMemoryRequirements2KHR = m_dynamic_loader.
                getProcAddress<PFN_vkGetImageMemoryRequirements2KHR>("vkGetImageMemoryRequirements2"),
        .vkBindBufferMemory2KHR = m_dynamic_loader.
                getProcAddress<PFN_vkBindBufferMemory2KHR>("vkBindBufferMemory2"),
        .vkBindImageMemory2KHR = m_dynamic_loader.
                getProcAddress<PFN_vkBindImageMemory2KHR>("vkBindImageMemory2"),
        .vkGetPhysicalDeviceMemoryProperties2KHR = m_dynamic_loader.
                getProcAddress<PFN_vkGetPhysicalDeviceMemoryProperties2KHR>("vkGetPhysicalDeviceMemoryProperties2"),
        .vkGetDeviceBufferMemoryRequirements = m_dynamic_loader.
                getProcAddress<PFN_vkGetDeviceBufferMemoryRequirements>("vkGetDeviceBufferMemoryRequirements"),
        .vkGetDeviceImageMemoryRequirements = m_dynamic_loader.
                getProcAddress<PFN_vkGetDeviceImageMemoryRequirements>("vkGetDeviceImageMemoryRequirements")
    };
}

vk::Format aristaeus::gfx::vulkan::Window::find_supported_image_format(const std::vector<vk::Format> &formats,
                                                                       vk::ImageTiling tiling,
                                                                       vk::FormatFeatureFlags features) {
    for(vk::Format format : formats) {
        vk::FormatProperties format_properties = m_physical_device.getFormatProperties(format);

        if(tiling == vk::ImageTiling::eLinear && (format_properties.linearTilingFeatures & features) == features) {
            return format;
        } else if(tiling == vk::ImageTiling::eOptimal &&
                  (format_properties.optimalTilingFeatures & features) == features) {
            return format;
        }
    }

    throw std::runtime_error("Failed to find supported format!");
}

vk::raii::SwapchainKHR aristaeus::gfx::vulkan::Window::create_swap_chain(vk::raii::Device &logical_device, vk::SwapchainKHR old_swapchain) {
    SwapChainSupportDetails swap_chain_support = get_swap_chain_support(m_physical_device);

    vk::Extent2D chosen_extent = choose_swap_extent(swap_chain_support.surface_capabilities);
    vk::PresentModeKHR chosen_presentation_mode = choose_swap_presentation_mode(swap_chain_support.presentation_modes);
    vk::SurfaceFormatKHR chosen_surface_format = choose_swap_surface_format(swap_chain_support.surface_formats);
    std::array<uint32_t, 2> queue_family_indices_array = {m_queue_indices_info.graphics_family.value(),
                                                          m_queue_indices_info.presentation_family.value()};
    vk::SwapchainCreateInfoKHR swap_chain_create_info {
            {}, *m_window_surface, {}, chosen_surface_format.format,
            chosen_surface_format.colorSpace, chosen_extent, 1,
            vk::ImageUsageFlagBits::eColorAttachment, vk::SharingMode::eExclusive
    };
    uint32_t swap_image_count = swap_chain_support.surface_capabilities.minImageCount + 1;

    if(swap_chain_support.surface_capabilities.maxImageCount > 0 &&
       swap_image_count > swap_chain_support.surface_capabilities.maxImageCount) {
        swap_image_count = swap_chain_support.surface_capabilities.maxImageCount;
    }

    swap_chain_create_info.minImageCount = swap_image_count;

    if(m_queue_indices_info.graphics_family != m_queue_indices_info.presentation_family) {
        swap_chain_create_info.imageSharingMode = vk::SharingMode::eConcurrent;
        swap_chain_create_info.queueFamilyIndexCount = 2;
        swap_chain_create_info.pQueueFamilyIndices = queue_family_indices_array.data();
    }

    swap_chain_create_info.preTransform = swap_chain_support.surface_capabilities.currentTransform;
    swap_chain_create_info.compositeAlpha = vk::CompositeAlphaFlagBitsKHR::eOpaque;

    swap_chain_create_info.presentMode = chosen_presentation_mode;
    swap_chain_create_info.clipped = VK_TRUE;

    swap_chain_create_info.oldSwapchain = old_swapchain;

    m_swap_chain_image_format = chosen_surface_format.format;
    m_swap_chain_extent = chosen_extent;

    vk::raii::SwapchainKHR new_swap_chain {logical_device, swap_chain_create_info, nullptr};

    return new_swap_chain;
}

void aristaeus::gfx::vulkan::Window::update_imgui_input() {
    ImGuiIO &io = ImGui::GetIO();
    double mouse_x, mouse_y;

    glfwPollEvents();

    glfwGetCursorPos(m_glfw_window.get(), &mouse_x, &mouse_y);

    if (!is_capturing_mouse()) {
        io.AddMousePosEvent(mouse_x, mouse_y);
        io.AddMouseButtonEvent(GLFW_MOUSE_BUTTON_LEFT,
                               glfwGetMouseButton(m_glfw_window.get(), GLFW_MOUSE_BUTTON_LEFT) == GLFW_PRESS);
        io.AddMouseButtonEvent(GLFW_MOUSE_BUTTON_RIGHT,
                               glfwGetMouseButton(m_glfw_window.get(), GLFW_MOUSE_BUTTON_RIGHT) == GLFW_PRESS);
    }
}

vk::Extent2D aristaeus::gfx::vulkan::Window::choose_swap_extent(const vk::SurfaceCapabilitiesKHR &capabilities) {
    int window_height;
    int window_width;

    glfwGetFramebufferSize(m_glfw_window.get(), &window_width, &window_height);

    return {
            std::clamp(static_cast<uint32_t>(window_width), capabilities.minImageExtent.width,
                                capabilities.maxImageExtent.width),
            std::clamp(static_cast<uint32_t>(window_height), capabilities.minImageExtent.height,
                                    capabilities.maxImageExtent.height)
    };
}
