#include "scene_graph_node.hpp"

#include "vulkan/core_renderer.hpp"

ARISTAEUS_CLASS_IMPL(aristaeus::gfx::SceneGraphNode, );
ARISTAEUS_CLASS_IMPL(aristaeus::gfx::NoOpNode, typeid(aristaeus::gfx::SceneGraphNode));

void aristaeus::gfx::NoOpNode::classdb_register() {}

void aristaeus::gfx::SceneGraphNode::_construct_entry_point() {
	update_model_matrix(true);
}

aristaeus::gfx::SceneGraphNode::Transform aristaeus::gfx::SceneGraphNode::Transform::operator-(const Transform &other) const {
	// getting the inverse of other's quaternion as specified by walkar
	// on the Math Stack Exchange (https://math.stackexchange.com/a/1375759)
	float inverse_numerator = other.rotation_quat.w - other.rotation_quat.x - other.rotation_quat.y - other.rotation_quat.z;
	float inverse_denominator = powf(other.rotation_quat.w, 2) + powf(other.rotation_quat.x, 2) +
		powf(other.rotation_quat.y, 2) + powf(other.rotation_quat.z, 2);

	return Transform{
		.scale = scale / other.scale,
		.position = position - other.position,
		.rotation_quat = rotation_quat * (inverse_numerator / inverse_denominator)
	};
}


void aristaeus::gfx::SceneGraphNode::classdb_register() {
	assert(core::ClassDB::instance != nullptr);

	auto node_name_setter = [](SceneGraphNode &node, std::string node_name) {
		node.m_node_name = node_name;
	};
	auto node_name_getter = [](SceneGraphNode &node) {
		return node.m_node_name;
	};

	core::ClassDB &class_db = *core::ClassDB::instance;

	class_db.register_type<Transform>([](const nlohmann::json::value_type &json) {
		Transform transform = DEFAULT_TRANSFORM;

		if (json.contains("scale")) {
			transform.scale = utils::get_vec3_json(json.at("scale"));
		}

		if (json.contains("position")) {
			transform.position = utils::get_vec3_json(json.at("position"));
		}

		if (json.contains("rotation_quat")) {
			transform.rotation_quat = utils::get_quat_json(json.at("rotation_quat"));
		}

		return transform;
	}, core::ClassDB::UiFunctions<Transform>{
		.parse_function = [](Transform &persistent_state, bool display_gui, const std::optional<std::string> &label_hint) {
			return transform_ui_function(persistent_state, display_gui, label_hint);
		},
		.constructor_function = [] {
			return DEFAULT_TRANSFORM;
		}
	});


	class_db.register_property<SceneGraphNode, Transform>("transform", &SceneGraphNode::get_transform,
													      [](SceneGraphNode &scene_graph_node, SceneGraphNode::Transform transform){scene_graph_node.set_transform(transform);},
													[]{return DEFAULT_TRANSFORM;});
	class_db.register_property<SceneGraphNode, std::unordered_set<std::string>>("groups", &SceneGraphNode::get_node_groups,
																	            &SceneGraphNode::set_node_groups, []{return std::unordered_set<std::string>{};});
	class_db.register_property<SceneGraphNode, int>("z_order", &SceneGraphNode::get_z_order, &SceneGraphNode::set_z_order,
													[]{return NON_BLENDED_OBJECT_Z_ORDER_DEFAULT;});

	class_db.register_event<SceneGraphNode, void*>(std::string{ BEFORE_PARSING_EVENT });
	class_db.register_event<SceneGraphNode, void*>(std::string{ PARSING_DONE_EVENT });
	class_db.register_event<SceneGraphNode, void*>(std::string{ ENTER_TREE_EVENT });
	class_db.register_event<SceneGraphNode, void*>(std::string{ EXIT_TREE_EVENT });
	class_db.register_event<SceneGraphNode, Transform>(std::string{ TRANSFORM_CHANGED_EVENT });
	class_db.register_event<SceneGraphNode, void*>(std::string{ GLOBAL_TRANSFORM_CHANGED_EVENT });

    class_db.register_event<SceneGraphNode, void*>(std::string{ SceneGraphNode::TREE_READY_EVENT });

	class_db.register_event<SceneGraphNode, std::reference_wrapper<SceneGraphNode>>(std::string{SceneGraphNode::DESCENDENT_NODE_ADDED_EVENT});

	class_db.register_event_callback_on_construction<SceneGraphNode, void*>(std::string{ gfx::SceneGraphNode::ENTER_TREE_EVENT }, "scene_graph_node_enter_tree", [](SceneGraphNode &node, void*) {
		if (node.m_root_node.lock() == nullptr) {
			// if the root node wasn't set, we don't have a parent, and thus we are the root node.
			assert(node.m_parent_node == nullptr);

			node.set_root_node_recursively(node.get_weak_ptr_to_this());
		}

		node.update_model_matrix(true);
	});

	class_db.register_event_callback_on_construction<SceneGraphNode, void *>(std::string{ GLOBAL_TRANSFORM_CHANGED_EVENT }, 
																			 "scene_graph_node_global_transform_changed", 
																			 [](SceneGraphNode &node, void *) {
		node.update_model_matrix(false);
	});

	class_db.register_property<SceneGraphNode, std::string>("node_name", node_name_getter, node_name_setter, {});
}

void aristaeus::gfx::SceneGraphNode::draw_tree(const Transform &parent_transform,
											   std::optional<std::reference_wrapper<tf::Subflow>> task_subflow, std::optional<int> z_order_to_draw,
											   const std::unordered_set<std::string> &node_groups_needed_to_draw, const std::unordered_set<std::string> &node_groups_to_ignore) {

	vulkan::CoreRenderer &core_renderer = utils::get_singleton_safe<vulkan::CoreRenderer>();

	if (!z_order_to_draw.has_value()) {
		for(std::pair<int, int> z_order_pair : m_z_order_map) {
			draw_tree(parent_transform, task_subflow, z_order_pair.first, node_groups_needed_to_draw, node_groups_to_ignore);
		}
	}
	else {
		Transform old_transform = get_transform();
		Transform new_transform = old_transform + parent_transform;
		int z_order_to_draw_int = z_order_to_draw.value();
		bool is_in_node_groups = true;

		for (const std::string &node_group : node_groups_needed_to_draw) {
			assert(!node_groups_to_ignore.contains(node_group));

			if (!m_node_groups.contains(node_group)) {
				is_in_node_groups = false;
				break;
			}
		}

		if (is_in_node_groups) {
			for (const std::string &node_group : node_groups_to_ignore) {
				if (m_node_groups.contains(node_group)) {
					is_in_node_groups = false;
					break;
				}
			}
		}

		if (get_z_order() == z_order_to_draw_int && is_in_node_groups) {
			draw();
		}

		std::string current_node_path = get_node_path();
		size_t beginning_child_node_size = m_children_nodes.size();

		for (std::pair<std::string, std::shared_ptr<SceneGraphNode>> child_node : m_children_nodes) {
			auto lambda = [this, child_node, new_transform, &core_renderer, z_order_to_draw_int, node_groups_needed_to_draw, node_groups_to_ignore]
			(std::optional<std::reference_wrapper<tf::Subflow>> subflow) {
				if (child_node.second != nullptr) {
					child_node.second->draw_tree(new_transform, subflow, z_order_to_draw_int, node_groups_needed_to_draw, node_groups_to_ignore);
				}
			};

			lambda({});

			// handle scene tree changes by skipping drawing everything else for this frame
			if (current_node_path != get_node_path() || beginning_child_node_size != m_children_nodes.size()) {
				break;
			}

			/*if (task_subflow.has_value()) {
				//task_subflow.emplace([lambda](tf::Subflow &subflow) {
				//	lambda(subflow);
				//});
			}
			else {
				lambda({});
			}*/
		}
	}
}

void aristaeus::gfx::SceneGraphNode::predraw_load_tree(const vk::CommandBuffer &command_buffer,
													   const glm::vec3 &camera_position,
                                                       std::optional<std::reference_wrapper<tf::Subflow>> subflow) {
    auto &core_renderer = utils::get_singleton_safe<vulkan::CoreRenderer>();

	if (m_reaper_list.contains(core_renderer.get_current_frame())) {
		m_reaper_list.erase(core_renderer.get_current_frame());
	}
	
	predraw_load(command_buffer, camera_position);

	for (std::pair<std::string, std::shared_ptr<SceneGraphNode>> child_node : m_children_nodes) {
		auto lambda = [this, child_node, command_buffer, camera_position]
		(std::optional<std::reference_wrapper<tf::Subflow>> subflow) {
			if (child_node.second != nullptr) {
				child_node.second->predraw_load_tree(command_buffer, camera_position, subflow);
			}
		};

		lambda({});

		if (subflow.has_value()) {
			subflow->get().emplace([lambda](tf::Subflow& subflow) {
				lambda(subflow);
			});
		}
		else {
			lambda({});
		}
	}
}

std::optional<std::reference_wrapper<aristaeus::gfx::SceneGraphNode>> aristaeus::gfx::SceneGraphNode::get_node_with_path(
        const std::string &node_path_string, bool create_nonexistent_nodes) {
	std::vector<std::string> node_names = get_nodes_in_node_path(node_path_string);
	std::cout << "calling get_node_with_path " << node_path_string << std::endl;

	if (node_names.size() == 1 && node_names[0] == std::string{NODE_CHILD_SEPARATOR}) {
		return *this;
	}

	std::reference_wrapper<SceneGraphNode> rover = *this;

	if (rover.get().get_node_path() == node_path_string){
		return rover;
	}

	for (size_t i = 0; node_names.size() > i; ++i) {
        std::string node_name = node_names[i];

        if(node_name == std::string{NODE_CHILD_SEPARATOR}) {
            // skip the root node name
            assert(i == 0);
            continue;
        }

		if (rover.get().map_contains(node_name) || create_nonexistent_nodes) {
			if (!rover.get().map_contains(node_name)) {
				std::shared_ptr<NoOpNode> temp_node = std::make_shared<NoOpNode>(true);

				temp_node.get()->rename_node(node_name);

				rover.get().add_child(temp_node);
			}

			rover = *rover.get().get_child(node_name).lock().get();
		}
		else {
			// couldn't find node path!
			return {};
		}
	}

	return rover;
}

void aristaeus::gfx::SceneGraphNode::update_model_matrix(bool trigger_events) {
	glm::mat4 translation_matrix = glm::translate(glm::mat4{ 1.0f }, m_position);
	glm::mat4 scale_matrix = glm::scale(glm::mat4{ 1.0f }, m_scale);
	glm::mat4 rotation_matrix = glm::toMat4(m_rotation_quat);

	Transform parent_transform = get_parent_transform();

	glm::mat4 global_translation_matrix = glm::translate(glm::mat4{ 1.0f }, parent_transform.position + m_position);
	glm::mat4 global_scale_matrix = glm::scale(glm::mat4{ 1.0f }, parent_transform.scale * m_scale);
	glm::mat4 global_rotation_matrix = glm::toMat4(parent_transform.rotation_quat * m_rotation_quat);

	glm::mat4 model_matrix = translation_matrix * rotation_matrix * scale_matrix;
	m_global_model_matrix = global_translation_matrix * global_rotation_matrix * global_scale_matrix;

 	set_model_matrix(model_matrix);

	if (trigger_events) {
		call_event<Transform>(std::string{SceneGraphNode::TRANSFORM_CHANGED_EVENT}, Transform{
			.scale = m_scale,
			.position = m_position,
			.rotation_quat = m_rotation_quat
		});
		call_event<void*>(std::string{SceneGraphNode::GLOBAL_TRANSFORM_CHANGED_EVENT}, nullptr);

		std::queue<std::shared_ptr<SceneGraphNode>> children_nodes_queue;

		for (const std::pair<const std::string, std::shared_ptr<SceneGraphNode>> &child_node : m_children_nodes) {
			children_nodes_queue.push(child_node.second);
		}

		while (!children_nodes_queue.empty()) {
			std::shared_ptr<SceneGraphNode> &node = children_nodes_queue.front();

			node->call_event<void *>(std::string{ SceneGraphNode::GLOBAL_TRANSFORM_CHANGED_EVENT }, nullptr);

			for (const std::pair<const std::string, std::shared_ptr<SceneGraphNode>> &child_node : node->m_children_nodes) {
				children_nodes_queue.push(child_node.second);
			}

			children_nodes_queue.pop();
		}
	}
}

void aristaeus::gfx::SceneGraphNode::add_child(const std::shared_ptr<SceneGraphNode> &child_node) {
	if (child_node != nullptr) {
		child_node->m_parent_node = this;
		child_node->update_node_paths();

		add_z_order_map(child_node->m_z_order_map);

		m_children_nodes[child_node->get_node_name()] = child_node;

		child_node->set_root_node_recursively(m_root_node);
		
		child_node->call_event<void*>(std::string{SceneGraphNode::ENTER_TREE_EVENT}, nullptr);
		child_node->call_event<void*>(std::string{SceneGraphNode::GLOBAL_TRANSFORM_CHANGED_EVENT}, nullptr);

		std::queue<gfx::SceneGraphNode*> nodes_just_added_to_tree;

		nodes_just_added_to_tree.push(child_node.get());

		// need to account and notify the parents in the scene tree not just that this child_node was added
		// as a new descendent, but all of the children of child_node and so on.
		while (!nodes_just_added_to_tree.empty()) {
			gfx::SceneGraphNode *descendent_rover = this;
			gfx::SceneGraphNode *new_node_added_to_tree = nodes_just_added_to_tree.front();

			while (descendent_rover != nullptr) {
				descendent_rover->call_event<std::reference_wrapper<SceneGraphNode>>(std::string{ SceneGraphNode::DESCENDENT_NODE_ADDED_EVENT }, *new_node_added_to_tree);

				descendent_rover = descendent_rover->m_parent_node;
			}

			for (std::shared_ptr<gfx::SceneGraphNode> &child_node : new_node_added_to_tree->get_children()) {
				nodes_just_added_to_tree.push(child_node.get());
			}

			nodes_just_added_to_tree.pop();
		}
	}
}

bool aristaeus::gfx::SceneGraphNode::remove_child(const std::string &scene_graph_node, bool delay_deletion, std::optional<uint32_t> current_frame_num) {
	if (m_children_nodes.contains(scene_graph_node)) {
		std::shared_ptr<SceneGraphNode> &child_node = m_children_nodes.at(scene_graph_node);

		child_node->call_event<void*>(std::string{SceneGraphNode::EXIT_TREE_EVENT}, nullptr);

		child_node->m_parent_node = nullptr;
		child_node->m_node_path = m_children_nodes.at(scene_graph_node)->get_node_name();
		child_node->set_root_node_recursively({});

		remove_z_order_map(child_node->m_z_order_map);

		if (delay_deletion) {
			assert(current_frame_num.has_value());

			if (!m_reaper_list.contains(current_frame_num.value())) {
				m_reaper_list.insert({current_frame_num.value(), {}});
			}
					
			m_reaper_list.at(current_frame_num.value()).push_back(child_node);
		}

		m_children_nodes.erase(scene_graph_node);

		return true;
	}
	else {
		return false;
	}
}

void aristaeus::gfx::SceneGraphNode::set_transform(const Transform &transform, bool trigger_events) {
	if (m_position != transform.position || m_scale != transform.scale || m_rotation_quat != transform.rotation_quat) {
		m_position = transform.position;
		m_scale = transform.scale;
		m_rotation_quat = transform.rotation_quat;
		
		update_model_matrix(trigger_events);
	}
}

nlohmann::json aristaeus::gfx::SceneGraphNode::transform_ui_function(Transform &persistent_state, bool display_gui,
                                                                     const std::optional<std::string> &label_hint)
{
	if (display_gui && (!label_hint.has_value() || ImGui::TreeNode(label_hint.value().c_str()))) {
		ImGui::BeginGroup();

		ImGui::InputFloat3("Scale", &persistent_state.scale[0]);
		ImGui::InputFloat3("Position", &persistent_state.position[0]);
		ImGui::InputFloat4("Rotation Quaternion", &persistent_state.rotation_quat[0]);

		if (label_hint.has_value()) {
			ImGui::TreePop();
		}
	}

	nlohmann::json return_value{};

	return_value["scale"] = utils::get_glm_json<glm::vec3>(persistent_state.scale);
	return_value["position"] = utils::get_glm_json<glm::vec3>(persistent_state.position);
	return_value["rotation_quat"] = utils::get_glm_json<glm::quat>(persistent_state.rotation_quat);

	return return_value;
}

std::pair<std::string, std::string> aristaeus::gfx::SceneGraphNode::get_parent_node_path_and_child_name(
                                                                                    const std::string &node_path_string,
                                                                                    bool normalize_node_path_string) {
    int last_separator = -1;

    for (size_t i = 0; node_path_string.size() > i; ++i) {
        if (i != node_path_string.size() - 1 && node_path_string[i] == NODE_CHILD_SEPARATOR) {
            last_separator = i;
        }
    }

    if (last_separator == -1) {
        return { std::string{NODE_CHILD_SEPARATOR}, node_path_string }; // if no / anywhere, then we treat it as a child of root
    }

    std::string parent_node_path = node_path_string.substr(0, last_separator);

    if(parent_node_path.empty()) {
        parent_node_path = std::string{NODE_CHILD_SEPARATOR};
    } else if(normalize_node_path_string) {
        parent_node_path = normalize_node_path(parent_node_path);
    }

    std::string child_node_name;

    if(node_path_string.ends_with(NODE_CHILD_SEPARATOR)) {
        child_node_name = node_path_string.substr(last_separator + 1, node_path_string.size() - last_separator - 2);
    } else {
        child_node_name = node_path_string.substr(last_separator + 1);
    }

    return { parent_node_path, child_node_name };
}

std::vector<std::string> aristaeus::gfx::SceneGraphNode::get_nodes_in_node_path(const std::string &node_path) {
    if (node_path == ROOT_NODE_NAME || node_path == std::string{NODE_CHILD_SEPARATOR}) {
        return std::vector<std::string>{std::string{NODE_CHILD_SEPARATOR}};
    }

    std::vector<std::string> node_names{};

    int last_separator = -1;

    for (size_t i = 0; node_path.size() > i; ++i) {
        if (node_path[i] == NODE_CHILD_SEPARATOR) {
            std::string node_name = node_path.substr(last_separator + 1, i - (last_separator + 1));

            if(node_names.empty() && node_name.empty()) {
                node_names.push_back(std::string{NODE_CHILD_SEPARATOR});
            } else if (node_name != NODE_NOOP_SEPARATOR) {
                if(node_name == ROOT_NODE_NAME && node_names.empty()) {
                    node_names.push_back(std::string{NODE_CHILD_SEPARATOR});
                } else {
                    node_names.push_back(node_name);
                }

                if (node_names.back().empty() && i < node_path.size() - 1) {
                    // invalid path!
                    return {};
                }
            }

            last_separator = i;
        }
        else if (node_path.substr(i, NODE_PARENT_SEPARATOR.size()) == NODE_PARENT_SEPARATOR &&
                 node_path[i + NODE_PARENT_SEPARATOR.size()] == NODE_CHILD_SEPARATOR && !node_names.empty()) {
            node_names.pop_back();
        }
    }

    if (last_separator + 1 < static_cast<int>(node_path.size())) {
        node_names.push_back(node_path.substr(last_separator + 1));

        if(node_names[0] == ROOT_NODE_NAME) {
            node_names[0] = std::string{NODE_CHILD_SEPARATOR};
        }
    }

    return node_names;
}

std::string aristaeus::gfx::SceneGraphNode::normalize_node_path(const std::string &node_path) {
    std::vector<std::string> node_paths = get_nodes_in_node_path(node_path);

    return concatenate_node_paths(node_paths, false);
}

std::string aristaeus::gfx::SceneGraphNode::concatenate_node_paths(const std::vector<std::string> &node_paths,
                                                                   bool normalize_node_paths) {
    assert(!node_paths.empty());

    std::stringstream string_stream;
    bool is_string_stream_empty = true;

    for (size_t i = 0; node_paths.size() > i; ++i) {
        std::string current_node_path = normalize_node_paths ? normalize_node_path(node_paths[i]) : node_paths[i];

        if(!is_string_stream_empty && current_node_path.starts_with(gfx::SceneGraphNode::NODE_CHILD_SEPARATOR)) {
            string_stream << current_node_path.substr(1);
        } else {
            string_stream << current_node_path;
            if(!current_node_path.empty()) {
                is_string_stream_empty = false;
            }
        }

        if (i != node_paths.size() - 1 && current_node_path.back() != NODE_CHILD_SEPARATOR) {
            string_stream << NODE_CHILD_SEPARATOR;
        }
    }

    return string_stream.str();
}
