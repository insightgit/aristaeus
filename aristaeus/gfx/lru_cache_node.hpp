#ifndef ARISTAEUS_LRU_CACHE_NODE
#define ARISTAEUS_LRU_CACHE_NODE

#include "scene_graph_node.hpp"
#include "../core/lru_cache.hpp"

#include "vulkan/core_renderer.hpp"

namespace aristaeus::gfx {
	// We are using multiple inheritance here because composition does not suffice
	// (we expect to use this node as both a LRUCache and a SceneGraphNode), while not pure interfaces, 
	// are sufficently unrelated to each other for it to reduce the danger of multiple inheritance.
	// 
	// LRUCache: data structure, mainly managing memory and having this idea of a cache with a max size
	// SceneGraphNode: node in a scene tree, which although it has a memory managment side with having
	// shared_ptrs, mainly serves to make scene builders' jobs easier through recursive transforms of nodes.
	//
	// Although this is a SceneGraphNode, DO NOT add children thru add_child UNLESS you don't want it to be managed by
	// the LRU cache. Otherwise, use put instead to add a child.
	template<typename K, typename V, typename Hash = std::hash<K>>
	requires(std::is_base_of_v<SceneGraphNode, V>)
	class LRUCacheNode : public SceneGraphNode, public LRUCache<K, std::shared_ptr<V>, Hash>
	{
	public:
		static constexpr std::string_view NODE_TYPE = "lru_cache_node";

		using LRUCache<K, std::shared_ptr<V>, Hash>::update_all_with_dependency;

		LRUCacheNode(const std::string &node_name, size_t capacity, bool delay_deletion) : SceneGraphNode(node_name, std::string{NODE_TYPE}), LRUCache<K, std::shared_ptr<V>, Hash>(capacity, delay_deletion) {}

		void predraw_load(const vk::CommandBuffer &command_buffer, const glm::vec3 &camera_position)override {}

		void draw()override {
			// need to mark all of them as used this frame to keep dependencies current
			// so that data isn't freed while a frame using that data is still in flight
			update_all_with_dependency(utils::get_singleton_safe<vulkan::CoreRenderer>().get_current_frame());
		}
	protected:
		void added_obj(std::shared_ptr<V> &lru_value) override {
			add_child(lru_value);
		}

		void queued_for_deletion(std::shared_ptr<V> &lru_value) override {
			remove_child(lru_value);
		}
	};
}

#endif