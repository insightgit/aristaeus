#include "vulkan_pipeline.hpp"

aristaeus::gfx::vulkan::VulkanPipeline::WeakShaderModuleCache aristaeus::gfx::vulkan::VulkanPipeline::shader_module_cache{};
std::mutex aristaeus::gfx::vulkan::VulkanPipeline::shader_module_cache_mutex{};