//
// Created by bobby on 8/28/21.
//

#ifndef ARISTAEUS_DIRECTIONAL_LIGHT_HPP
#define ARISTAEUS_DIRECTIONAL_LIGHT_HPP

#include <memory>

#include "assimp_model.hpp"
#include "lighting_node.hpp"
#include "perspective_camera_environment.hpp"

#include "../../core/class_db.hpp"
#include "../../utils.hpp"

namespace aristaeus::gfx::vulkan {
    class DirectionalLight : public LightingNode {
    public:
        ARISTAEUS_CLASS_HEADER("directional_light", DirectionalLight, true);

        static constexpr std::string_view NODE_TYPE = "directional_light";
        static constexpr std::string_view SHADOW_MAP_CAPTURE_CHANGE_EVENT = "on_shadow_map_capture_change";
        static constexpr int UBO_DIRECTIONAL_LIGHT_TYPE_ID = 0;

        ~DirectionalLight() = default;

        PerspectiveCameraEnvironment::Capture get_shadow_map_capture() {
            PerspectiveCameraEnvironment::Capture capture = m_shadow_map_capture;

            if (capture.view_direction == USE_CURRENT_STATE) {
                capture.view_direction = m_direction;
            }

            if (capture.position == USE_CURRENT_STATE) {
                capture.position = get_parent_transform().position + get_position();
            }

            return capture;
        }

        [[nodiscard]] bool is_day_night_rotation_paused() const {
            return m_day_night_rotation_paused;
        }

        void resume_day_night_rotation() {
            m_day_night_rotation_paused = false;
        }

        void pause_day_night_rotation() {
            m_day_night_rotation_paused = true;
        }

        void predraw_load(const vk::CommandBuffer &command_buffer, const glm::vec3 &camera_position)override {}

        void draw() override;
    private:
        static constexpr glm::vec3 USE_CURRENT_STATE{ -122431234, 2222, -44342343 };

        static constexpr PerspectiveCameraEnvironment::Capture DEFAULT_SHADOW_MAP_CAPTURE{
            .aspect_ratio = 1.0f,
            .fov = 90.0f,
            .pitch = 0.0f,
            .position = USE_CURRENT_STATE,
            .up = glm::vec3{0, 1, 0},
            .yaw = 0.0f,
            .z_far = 100.0f,
            .z_near = 1.0f,
            .view_direction = USE_CURRENT_STATE,
            .orthographic_capture = {}
        };

        [[nodiscard]] static PerspectiveCameraEnvironment::Capture get_capture_json(const nlohmann::json::value_type &capture_json);

        void update_point_light_ubo();

        void day_night_rotation_time_updated(float new_time_frames);

        glm::vec3 m_diffuse;
        glm::vec3 m_direction;
        glm::vec3 m_base_direction;
        float m_day_night_rotation_time_frames;
        float m_current_rotation_frame_count = 0;
        bool m_day_night_rotation_paused = false;

        PerspectiveCameraEnvironment::Capture m_shadow_map_capture;
        PerspectiveCameraEnvironment::Capture m_base_shadow_map_capture;
    };
}


#endif //ARISTAEUS_POINT_LIGHT_HPP
