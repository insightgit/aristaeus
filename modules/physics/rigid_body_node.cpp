#include "rigid_body_node.hpp"

ARISTAEUS_CLASS_IMPL(aristaeus::physics::RigidBodyNode,
                     aristaeus::core::StdTypeInfoRef(typeid(aristaeus::physics::PhysicsActorNode)))

void aristaeus::physics::RigidBodyNode::classdb_register() {
	auto &class_db = utils::get_singleton_safe<core::ClassDB>();

	class_db.register_event_callback_on_construction<RigidBodyNode, void*>(std::string{ gfx::SceneGraphNode::PARSING_DONE_EVENT }, "rigid_body_parsing_done", [](RigidBodyNode &rigid_body_node, void*) {
		rigid_body_node.on_parsing_done();
	});

	class_db.register_event_callback_on_construction<RigidBodyNode, void*>(std::string{ gfx::SceneGraphNode::ENTER_TREE_EVENT }, "rigid_body_enter_tree", [](RigidBodyNode& rigid_body_node, void*) {
		rigid_body_node.on_enter_tree_rigid_body();
	});
}

void aristaeus::physics::RigidBodyNode::on_parsing_done() {
	auto &physics_manager = utils::get_singleton_safe<PhysicsManager>();

	m_rigid_dynamic = physics_manager.get_physics().createRigidDynamic(scene_transform_to_physx_transform(get_transform()));
	m_actor = m_rigid_dynamic;
    m_actor->setGlobalPose(scene_transform_to_physx_transform(get_parent_transform() + get_transform()));

	call_event<void*>(std::string{ physics::PhysicsActorNode::ACTOR_UPDATED_EVENT }, nullptr);
}

void aristaeus::physics::RigidBodyNode::on_enter_tree_rigid_body() {
	utils::get_singleton_safe<PhysicsManager>().add_callback_for_event<void*, RigidBodyNode>(std::string{ physics::PhysicsManager::PHYSICS_STEP_FINISHED_EVENT }, "rigid_body_" + get_node_path(),
	[](RigidBodyNode& rigid_body_node, void*) {
		gfx::SceneGraphNode::Transform new_transform = rigid_body_node.physx_transform_to_scene_transform(rigid_body_node.m_actor->getGlobalPose());
		gfx::SceneGraphNode::Transform parent_transform = rigid_body_node.get_parent_transform();

		new_transform = new_transform - parent_transform;

		rigid_body_node.set_transform(new_transform);
	}, *this);
}