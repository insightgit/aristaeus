//
// Created by bobby on 1/21/22.
//

#ifndef ARISTAEUS_PROCEDURAL_TERRAIN_HPP
#define ARISTAEUS_PROCEDURAL_TERRAIN_HPP

#include <cmath>

#include <chrono>
#include <map>
#include <memory>
#include <random>
#include <unordered_map>

#include <gfx/vulkan/point_light.hpp>
#include <gfx/vulkan/core_renderer.hpp>
#include <gfx/vulkan/perlin_noise.hpp>
#include "wave_properties.hpp"

#include "prop_info.hpp"
#include "terrain_image.hpp"
#include "terrain.hpp"

namespace aristaeus::gfx::vulkan {
    class ProceduralTerrain : public SceneGraphNode {
    public:
        static constexpr std::string_view NODE_TYPE = "procedural_terrain";

        static constexpr vk::PipelineColorBlendAttachmentState WATER_COLOR_BLEND_ATTACHMENT =
                {VK_TRUE, vk::BlendFactor::eSrcAlpha, vk::BlendFactor::eOneMinusSrcAlpha, vk::BlendOp::eAdd,
                 vk::BlendFactor::eOne, vk::BlendFactor::eOne, vk::BlendOp::eAdd,
                 vk::ColorComponentFlagBits::eR | vk::ColorComponentFlagBits::eG | vk::ColorComponentFlagBits::eB |
                 vk::ColorComponentFlagBits::eA};

        struct TerrainInfo {
            float height_magnitude;
            float height_map_pos_ratio;
            int number_of_perlin_octaves;
            int number_of_textures;
            bool multi_textured_terrain;
            std::map<glm::vec4, std::vector<Image>, utils::Vec4Hash> texture_paths_height;
            std::unordered_map<Image, glm::vec4> texture_paths_slope;
            float perlin_persistence;
            std::string specular_map_path;
            std::string texture_path;
            bool use_terrain_shader;
            float vertex_resolution;
            int number_of_patches;

            bool use_grass_shader;
            PropInfo prop_info;
        };

        struct WaterInfo {
            bool draw_water;
            glm::vec3 noise_amplitude;
            glm::vec3 noise_frequency;
            glm::vec3 noise_frequency_increase;
            glm::vec3 noise_influence;
            glm::vec3 noise_persistence;
            float noise_period;
            unsigned long noise_range;
            glm::ivec3 number_of_perlin_octaves;
            int number_of_textures;
            float refractive_index;
            float refractive_strength;
            float reflectivity;
            float shininess;
            std::string texture_path;
            float transmission;
            float y_spawn_threshold;
            int number_of_dynamic_sky_box_spots;
            float dynamic_sky_box_resolution_scale;
            WaveProperties wave_properties;
            int tessellation_amount;
            int number_of_patches;
        };

        struct GrassInfo {
            bool draw_grass;
            glm::vec4 grass_texture_value; // contains a vector with 1.0f to the one component that
            // must be used and 0.0f to everything else.
            float grass_texture_threshold;

            int oscilation_octaves;
            float oscilation_amplitude;
            float oscilation_persistence;
            float oscilation_scale;
            float oscilation_frequency;
            float oscilation_frequency_increase;

            std::string grass_texture_path;
            std::string grass_texture_specular_path;

            float grass_size;
        };

        ProceduralTerrain(const TerrainInfo &terrain_info, const WaterInfo &water_info,
                          std::mt19937 &rng, const glm::ivec2 &height_map_resolution,
                          const std::unordered_map<glm::vec2, glm::vec2, utils::Vec2Hash> &predetermined_gradient_vectors,
                          std::optional<std::reference_wrapper<GraphicsPipeline>> environment_pipeline,
                          bool write_noise_texture = false, bool draw_water_noise_texture = true,
                          std::optional<glm::vec2> perlin_noise_start = {},
                          std::optional<vk::CommandBuffer> create_command_buffer = {});

        ProceduralTerrain(const TerrainInfo &terrain_info, const WaterInfo &water_info,
                    const glm::ivec2 &height_map_resolution, const std::vector<std::vector<float>> &perlin_noise_map,
                    std::optional<std::reference_wrapper<GraphicsPipeline>> environment_pipeline,
                    std::optional<vk::CommandBuffer> create_command_buffer = {});

        [[nodiscard]] GrassInfo get_grass_info() {
            return m_grass_info;
        }

        [[nodiscard]] virtual float get_height_map_value(int x, int y) const {
            return m_cpu_generated_height_map[y][x];
        }

        [[nodiscard]] virtual float get_height_map_value_normalized(float x, float y) const {
            glm::ivec2 height_map_int_coords{std::min(static_cast<int>(x * m_height_map_resolution.x),
                                                       m_height_map_resolution.x - 1),
                                              std::min(static_cast<int>(y * m_height_map_resolution.y),
                                                       m_height_map_resolution.y - 1)};

            return m_cpu_generated_height_map[height_map_int_coords.y][height_map_int_coords.x];
        }

        void set_chunked_settings(const std::optional<float> &chunked_smoothing_percent,
                                  const std::optional<glm::ivec2> &chunked_position);

        [[nodiscard]] virtual glm::vec2 get_perlin_gradient_vector(const glm::vec2 &coords) {
            return m_cpu_perlin_noise.value().get_perlin_gradient_vector(coords);
        }

        std::shared_ptr<Texture> get_terrain_height_map_texture();

        void set_grass_info(const GrassInfo &grass_info) {
            m_grass_info = grass_info;
        }

        void refresh_with_new_terrain_info(const TerrainInfo &new_terrain_info, const int seed,
                                           const glm::ivec2 &height_map_resolution,
                                           std::optional<vk::CommandBuffer> create_command_buffer,
                                           std::optional<glm::vec2> perlin_noise_start = {});

        [[nodiscard]] AssimpModel::TessellationSettings get_tessellation_settings() const;

        [[nodiscard]] int get_num_of_patches() const;

        // just for ease of debugging
        #ifndef NDEBUG
        void set_terrain_position(const glm::ivec2 &terrain_position) {
            if (m_terrain != nullptr) {
                m_terrain->set_terrain_purpose("base-" +glm::to_string(terrain_position));
            }

            if (m_water_terrain != nullptr) {
                m_water_terrain->set_terrain_purpose("water-" + glm::to_string(terrain_position));
            }
        }
        #endif

        void set_height_map_textures(const std::vector<std::shared_ptr<Texture>> &height_map_textures) {
            std::vector<std::shared_ptr<Texture>> new_height_map_textures;

            if (height_map_textures.empty()) {
                new_height_map_textures.push_back(m_terrain->get_height_map_texture());
            }
            else {
                assert(height_map_textures.size() == 8);

                for (size_t i = 0; height_map_textures.size() + 1 > i; ++i) {
                    if (i == (height_map_textures.size() + 1) / 2) {
                        new_height_map_textures.push_back(m_terrain->get_height_map_texture());
                    }
                    else if (i > (height_map_textures.size() + 1) / 2) {
                        new_height_map_textures.push_back(height_map_textures[i - 1]);
                    }
                    else {
                        new_height_map_textures.push_back(height_map_textures[i]);
                    }
                }
            }

            m_height_map_textures = new_height_map_textures;

            if (m_terrain != nullptr) {
                m_terrain->set_height_map_textures(m_height_map_textures);
            }

            if (m_water_terrain != nullptr) {
                m_water_terrain->set_height_map_textures(m_height_map_textures);
            }
        }

        void set_tessellation_settings(const AssimpModel::TessellationSettings &tessellation_settings);

        void set_num_of_patches(int num_of_patches, CoreRenderer &core_renderer);

        void set_test_color(std::optional<glm::vec3> test_color);
        bool using_terrain_shader() const;

        void toggle_edge_debug_mode();
        #ifndef NDEBUG
        void toggle_normal_debug_mode();
        #endif

        void set_environment_pipeline(GraphicsPipeline &environment_pipeline) {
            m_environment_pipeline = environment_pipeline;
        }

        virtual void predraw_load(const vk::CommandBuffer &command_buffer, const glm::vec3 &camera_position) override {}

        void draw_imgui(CoreRenderer &core_renderer);

        void draw(const vk::CommandBuffer &command_buffer, vk::raii::PipelineLayout &pipeline_layout, size_t instance_number)override;
    protected:
        std::mt19937 m_random_num_generator;

        [[nodiscard]] glm::ivec2 get_height_map_resolution() const {
            return m_height_map_resolution;
        }

        [[nodiscard]] std::string get_terrain_node_path() const {
            return m_terrain_node_path;
        }

        [[nodiscard]] TerrainInfo get_terrain_info() const {
            return m_terrain_info;
        }
    private:
        struct PixelData {
            uint8_t r;
            uint8_t g;
            uint8_t b;
        };

        // should match respective defines in Aristaeus_Gfx_Terrain_Water_Shader.vert
        static constexpr int MAX_WATER_HEIGHTMAP_SIZE_X = 256;
        static constexpr int MAX_WATER_HEIGHTMAP_SIZE_Z = 256;


        vk::SamplerCreateInfo get_sampler_create_info(CoreRenderer &core_renderer) const {
            vk::SamplerCreateInfo sampler_create_info = AssimpModel::DEFAULT_SAMPLER_CREATE_INFO;

            sampler_create_info.maxAnisotropy = core_renderer.get_max_sampler_anisotropy();

            return sampler_create_info;
        }

        std::vector<GraphicsPipeline::ShaderInfo> get_water_pipeline_shader_infos() {
            return {
                    {
                            .code = utils::read_file("environments/vulkan_environment_shaders/terrain/terrain.vert.spv"),
                            .name = "terrain.vert",
                            .stage = vk::ShaderStageFlagBits::eVertex
                    },
                    {
                            .code = utils::read_file("environments/vulkan_environment_shaders/terrain/terrain.tesc.spv"),
                            .name = "terrain.tesc",
                            .stage = vk::ShaderStageFlagBits::eTessellationControl
                    },
                    {
                            .code = utils::read_file("environments/vulkan_environment_shaders/terrain/terrain_water.tese.spv"),
                            .name = "terrain_water.tese",
                            .stage = vk::ShaderStageFlagBits::eTessellationEvaluation
                    },
                    {
                            .code = utils::read_file("environments/vulkan_environment_shaders/engine/environment_shader.frag.spv"),
                            .name = "environment_shader.frag",
                            .stage = vk::ShaderStageFlagBits::eFragment
                    }
            };
        }

        void imgui_vec4(const std::string &base_label, glm::vec4 &target_vec4);

        GraphicsPipeline::PipelineInfo get_water_pipeline_info(vk::raii::RenderPass &render_pass);

        void setup_water_terrain();

        float bilinearly_interperlate(const glm::vec2 &coords,
                                      const std::vector<std::vector<float>> &height_map) const;

        virtual Terrain *terrain_from_terrain_info(const TerrainInfo &terrain_info,
                                                    std::optional<vk::CommandBuffer> create_command_buffer);

        std::vector<std::vector<float>> generate_height_map_cpu_perlin_noise(const glm::ivec2 &height_map_resolution,
                                                                             std::optional<glm::ivec2> perlin_noise_start = {});
        std::shared_ptr<Terrain> generate_water_terrain(CoreRenderer &core_renderer,
                                                        std::optional<vk::CommandBuffer> create_command_buffer);

        bool m_draw_water_noise_texture;
        glm::ivec2 m_height_map_resolution;
        glm::vec2 m_quad_vertices[12] = {
                // vertex then            tex coord
                glm::vec2(-0.2f, 0.2f),  glm::vec2(0.0f, 1.0f),
                glm::vec2(-0.2f, -0.2f),   glm::vec2(0.0f, 0.0f),
                glm::vec2(0.2f, -0.2f),   glm::vec2(1.0f, 0.0f),

                glm::vec2(-0.2f, 0.2f),   glm::vec2(0.0f, 1.0f),
                glm::vec2(0.2f, -0.2f),    glm::vec2(1.0f, 0.0f),
                glm::vec2(0.2f, 0.2f),   glm::vec2(1.0f, 1.0f)
        };
        std::mt19937::result_type m_seed;
        GrassInfo m_grass_info;
        TerrainInfo m_terrain_info;
        WaterInfo m_water_info;
        bool m_use_cpu_perlin_noise;
        bool m_write_noise_texture;

        std::unordered_map<glm::vec2, glm::vec2, utils::Vec2Hash> m_predetermined_gradient_vectors;

        GraphicsPipeline m_water_pipeline;

        // cpu perlin noise variables
        std::optional<PerlinNoise> m_cpu_perlin_noise;
        std::vector<std::vector<float>> m_cpu_generated_height_map;

        int m_last_start;
        long m_start_time = std::chrono::duration_cast<std::chrono::milliseconds>(
                std::chrono::system_clock::now().time_since_epoch()).count();
        std::string m_terrain_node_path;
        std::string m_water_terrain_node_path;

        std::vector<std::shared_ptr<Texture>> m_height_map_textures;

        std::shared_ptr<Terrain> m_terrain;
        std::shared_ptr<Terrain> m_water_terrain;
        std::unordered_map<unsigned long, unsigned int> m_water_terrain_noise_textures;

        std::optional<std::reference_wrapper<GraphicsPipeline>> m_environment_pipeline;

        std::vector<std::vector<float>> m_generated_height_map_water;

        // if optional is empty, we haven't started getting the wave time point yet
        std::optional<std::chrono::system_clock::time_point> m_wave_last_time_point;

        vk::RenderPass m_current_render_pass;
    };
}

#endif //ARISTAEUS_PROCEDURAL_TERRAIN_HPP
