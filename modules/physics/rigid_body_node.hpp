#ifndef ARISTAEUS_RIGID_BODY_NODE_HPP
#define ARISTAEUS_RIGID_BODY_NODE_HPP

#include "physics_actor_node.hpp"
#include "physics_manager.hpp"

namespace aristaeus::physics {
	class RigidBodyNode : public PhysicsActorNode {
    ARISTAEUS_CLASS_HEADER("rigid_body", RigidBodyNode, true);
	private:
		void draw() {};

		void predraw_load(const vk::CommandBuffer &command_buffer, const glm::vec3 &camera_position) {};

		void on_parsing_done();
		void on_enter_tree_rigid_body();

		physx::PxRigidDynamic *m_rigid_dynamic;
	};
}

#endif