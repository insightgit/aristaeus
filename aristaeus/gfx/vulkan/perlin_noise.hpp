//
// Created by bobby on 16/04/23.
//

#ifndef ARISTAEUS_PERLIN_NOISE_HPP
#define ARISTAEUS_PERLIN_NOISE_HPP

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/glm.hpp>
#include <unordered_map>

#include "core_renderer.hpp"
#include "../../utils.hpp"
#include "texture.hpp"

namespace aristaeus::gfx::vulkan {
    class PerlinNoise {
    public:
        PerlinNoise(std::mt19937 &rng,
                    const std::unordered_map<glm::vec2, glm::vec2, utils::Vec2Hash> &predetermined_gradient_vectors = {}) :
            m_rng(rng),
            m_predetermined_gradient_vectors(predetermined_gradient_vectors) {}

        void add_predetermined_gradient_vector(const glm::vec2 &key, const glm::vec2 &value) {
            m_predetermined_gradient_vectors[key] = value;
        }

        glm::vec2 get_perlin_gradient_vector(const glm::vec2 &coords);

        float perlin(float frequency, const glm::vec2 &coords);

        float perlin_with_octaves(float amplitude, float frequency, int number_of_octaves,
                                  float persistence, glm::vec2 xy_coords);

        Texture to_perlin_noise_texture(float amplitude, float frequency, int number_of_octaves,
                                        float persistence, const glm::ivec2 &height_map_resolution,
                                        CoreRenderer &core_renderer,
                                        std::optional<vk::CommandBuffer> create_command_buffer = {},
                                        bool wait_for_creation = false);
    private:
        // Source: http://riven8192.blogspot.com/2010/08/calculate-perlinnoise-twice-as-fast.html
        float perlin_gradient_vector(int hash, const glm::vec2 &location_vector);
        float perlin_gradient_vector(glm::ivec2 gradient_vector, const glm::vec2 &location_vector) {
            /*if (gradient_vector.x % 128 == 0 || gradient_vector.x % 127 == 0) {
                gradient_vector.x = 0;
            }
            if (gradient_vector.y % 128 == 0 || gradient_vector.y % 127 == 0) {
                gradient_vector.y = 0;
            }*/

            return perlin_gradient_vector(perlin_hash(gradient_vector), location_vector);
        }

        float random_gradient_vector(const glm::vec2 &location_vector) const {
            std::uniform_real_distribution<float> random_num_generator_distribution(0.0f, 1.0f);

            glm::vec2 random_vector = glm::vec2(random_num_generator_distribution(m_rng),
                                                random_num_generator_distribution(m_rng));

            return glm::dot(random_vector, location_vector);
        }

        float perlin_fade(float to_be_faded) {
            return (6 * pow(to_be_faded, 5)) - (15 * pow(to_be_faded, 4)) + (10 * pow(to_be_faded, 3));
        }
        int perlin_hash(glm::ivec2 coords) {
            // z is a constant here (0)
            while(coords.x < 0) {
                coords.x += 255;
            }

            while(coords.y < 0) {
                coords.y += 255;
            }

            coords.x = coords.x % 255;
            coords.y = coords.y % 255;

            int x_y_index = (utils::PERLIN_LOOKUP_TABLE[coords.x] + coords.y) % 255;

            return utils::PERLIN_LOOKUP_TABLE[utils::PERLIN_LOOKUP_TABLE[x_y_index] + 0];
        }

        std::mt19937 &m_rng;
        std::unordered_map<glm::vec2, glm::vec2, utils::Vec2Hash> m_predetermined_gradient_vectors;
    };
}

#endif //ARISTAEUS_PERLIN_NOISE_HPP
