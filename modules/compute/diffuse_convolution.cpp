#include "diffuse_convolution.hpp"

aristaeus::gfx::vulkan::compute::DiffuseConvolution::DiffuseConvolution(
                                            CubeMapTexture &cube_map_texture,
                                            const std::vector<std::string> &skybox_texture_paths,
                                            const glm::ivec2 &samples, bool specular_reflection_mode,
                                            const std::optional<std::string> &cache_path) :
    CubeMapComputeTask(0, 1, 0, 1, ComputePipelineInfo{ get_compute_shader_info(),
                                                        std::string("compute_diffuse_convolution"),
                                                        {vk::PushConstantRange{vk::ShaderStageFlagBits::eCompute, 0,
                                                                               sizeof(ConvolutionPushConstants)}},
                                                                               true, false },
                       "compute_diffuse_convolution", 6 * get_number_of_mip_maps(cube_map_texture),
                       cube_map_texture.get_image_size() * glm::ivec2{4, 4}, specular_reflection_mode,
                       specular_reflection_mode ? std::optional<uint32_t>{get_number_of_mip_maps(cube_map_texture)} :
                       std::optional<uint32_t>{}, cache_path, skybox_texture_paths),
    m_push_constants({.samples = samples}), m_original_environment_map(cube_map_texture),
    m_specular_reflection_mode(specular_reflection_mode) {
    m_push_constants.image_size = m_original_environment_map.get_image_size();
}


void aristaeus::gfx::vulkan::compute::DiffuseConvolution::update_descriptor_set(const glm::ivec2 &image_size,
                                                                                uint32_t index) {
    vk::DescriptorImageInfo storage_image_info{ *m_output_info.sampler, *m_output_info.image_views.at(index),
                                                vk::ImageLayout::eGeneral };

    vk::WriteDescriptorSet storage_image_set_write{ *m_descriptor_sets[index], 0, 0, vk::DescriptorType::eStorageImage,
                                                    storage_image_info };

    vk::DescriptorImageInfo sampler_image_info = m_original_environment_map.get_descriptor_image_info();
    vk::WriteDescriptorSet sampler_descriptor_set_write{ *m_descriptor_sets[index], 1, 0,
                                                         vk::DescriptorType::eCombinedImageSampler, sampler_image_info,
                                                         {} };

    utils::get_singleton_safe<CoreRenderer>().get_logical_device_ref().
        updateDescriptorSets({ storage_image_set_write, sampler_descriptor_set_write }, {});
}

void aristaeus::gfx::vulkan::compute::DiffuseConvolution::send_dispatch_to_vulkan(vk::CommandBuffer &command_buffer, int index) {
    command_buffer.pushConstants(*m_compute_pipeline.get_pipeline_layout(), vk::ShaderStageFlagBits::eCompute, 0,
                                 sizeof(ConvolutionPushConstants), static_cast<void*>(&m_push_constants));
    update_descriptor_set(m_push_constants.image_size, index);

    m_compute_pipeline.bind_to_command_buffer(command_buffer);
    command_buffer.bindDescriptorSets(vk::PipelineBindPoint::eCompute, *m_compute_pipeline.get_pipeline_layout(), 0,
                                      *m_descriptor_sets[index], {});

    command_buffer.dispatch(m_push_constants.image_size.x / WORK_GROUP_X, m_push_constants.image_size.x / WORK_GROUP_Y,
                            1);
}

void aristaeus::gfx::vulkan::compute::DiffuseConvolution::dispatch(vk::CommandBuffer &command_buffer) {
    //tf::Taskflow new_taskflow;

    if (m_push_constants.image_size.x % WORK_GROUP_X != 0 || m_push_constants.image_size.y % WORK_GROUP_Y != 0) {
        throw std::runtime_error("convolution res must be a multiple of (" + std::to_string(WORK_GROUP_X) + "," +
                                 std::to_string(WORK_GROUP_Y) + ")");
    }

    uint32_t number_of_mipmaps = m_output_info.image->mip_levels;

    m_output_info.image->transition_image_layout(command_buffer, vk::ImageLayout::eGeneral,
                                                 vk::PipelineBindPoint::eCompute);

    for (uint32_t array_layer = 0; 6 > array_layer; ++array_layer) {
        int view_transformation_to_select = array_layer;

        // +x, -x, +y, -y, +z, -z
        switch (array_layer) {
        case 0:
            view_transformation_to_select = 5;
            break;
        case 1:
            view_transformation_to_select = 4;
            break;
        case 2:
            view_transformation_to_select = 3;
            break;
        case 3:
            view_transformation_to_select = 2;
            break;
        case 4:
            view_transformation_to_select = 0;
            break;
        case 5:
            view_transformation_to_select = 1;
            break;
        }

        m_push_constants.face_up = glm::vec3{ 0, -1, 0 };

        m_push_constants.current_view_transformation = get_current_view_transformation(view_transformation_to_select);

        if (m_specular_reflection_mode) {
            glm::ivec2 base_image_size = m_push_constants.image_size;
            m_push_constants.importance_sample_roughness = 0.0f;

            for (uint32_t mipmap = 0; number_of_mipmaps > mipmap; ++mipmap) {
                send_dispatch_to_vulkan(command_buffer, (array_layer * number_of_mipmaps) + mipmap);
                m_push_constants.importance_sample_roughness += 1.0f / number_of_mipmaps;
                m_push_constants.image_size /= glm::ivec2(2);
            }

            m_push_constants.image_size = base_image_size;
        }
        else {
            m_push_constants.importance_sample_roughness = -1.0f;
            send_dispatch_to_vulkan(command_buffer, array_layer);
        }
    }

    if (!m_cache_path.has_value()) {
        m_output_info.image->transition_image_layout(command_buffer, vk::ImageLayout::eShaderReadOnlyOptimal,
                                                     vk::PipelineBindPoint::eCompute);
    }
}