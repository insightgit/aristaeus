//
// Created by bobby on 23/02/23.
//

#ifndef ARISTAEUS_CHUNKED_TERRAIN_HPP
#define ARISTAEUS_CHUNKED_TERRAIN_HPP

#include <coroutine>

#include <core/class_db.hpp>

#include <gfx/lru_cache_node.hpp>

#include "procedural_terrain.hpp"

namespace aristaeus::gfx::vulkan {
    class ChunkedTerrain : public SceneGraphNode {
        ARISTAEUS_CLASS_HEADER("chunked_terrain", ChunkedTerrain, true);
    public:
        ChunkedTerrain(CoreRenderer &core_renderer,
                       const ProceduralTerrain::TerrainInfo &terrain_info,
                       const ProceduralTerrain::WaterInfo &water_info, const std::string &node_name,
                       const std::mt19937::result_type &seed, const glm::ivec2 &height_map_resolution,
                       float draw_distance, float smoothing_percent, size_t max_loaded_terrains);

        void predraw_load(const vk::CommandBuffer &command_buffer, const glm::vec3 &camera_position)override;

        void toggle_edge_debug_mode();

        #ifndef NDEBUG
        void toggle_normal_debug_mode();
        #endif

        float get_smoothed_height_map_value(glm::vec2 height_map_coords, glm::ivec2 current_chunk_position,
                                            uint32_t current_frame);
        float get_smoothed_height_slope(glm::vec2 height_map_coords, glm::ivec2 current_chunk_position,
                                        uint32_t current_frame);

        [[nodiscard]] glm::vec3 get_chunk_world_position(const glm::vec2 &chunk_position) const {
            return {(m_default_terrain_info.vertex_resolution - 1) * chunk_position.x, 0,
                    (m_default_terrain_info.vertex_resolution - 1) * chunk_position.y};
        }

        void toggle_test_mode() {
            m_test_mode = !m_test_mode;
        }

        [[nodiscard]] float get_height_magnitude() const {
            return m_default_terrain_info.height_magnitude;
        }

        [[nodiscard]] float get_vertex_resolution() const {
            return m_default_terrain_info.vertex_resolution;
        }

        void draw(const vk::CommandBuffer &command_buffer, vk::raii::PipelineLayout &pipeline_layout,
                  size_t instance_number = 1)override;

        void draw_imgui(CoreRenderer &core_renderer, const glm::vec3 &camera_position);
    protected:
        void on_parsing_done();
    private:
        class TaskflowObserver : public tf::ObserverInterface {
        public:
            TaskflowObserver() {}

            TaskflowObserver(tf::Task &needed_chunks, tf::Task &optional_chunks) :
                m_needed_chunks(needed_chunks), m_optional_chunks(optional_chunks) {}

            bool has_needed_chunks_finished() const {
                return m_needed_chunks_finished;
            }

            bool has_optional_chunks_finished() const {
                return m_optional_chunks_finished;
            }

            void set_up(size_t num_workers) override final {}
            void on_entry(tf::WorkerView w, tf::TaskView tv) override final {}

            void on_exit(tf::WorkerView w, tf::TaskView tv) override final {
                /*if (tv.name() == m_needed_chunks.value().get().name()) {
                    m_needed_chunks_finished = true;
                }

                if(tv.name() == m_optional_chunks.value().get().name()) {
                    m_optional_chunks_finished = true;
                }*/
            }
        private:
            std::optional<std::reference_wrapper<tf::Task>> m_needed_chunks;
            std::optional<std::reference_wrapper<tf::Task>> m_optional_chunks;
            bool m_needed_chunks_finished = false;
            bool m_optional_chunks_finished = false;
        };

        struct TerrainInfo {
            std::shared_ptr<ProceduralTerrain> loaded_terrain;
            std::shared_ptr<ChunkPropLoader> prop_loader;
        };

        void create_loading_tasks(const glm::ivec2 &chunk_camera_position);

        ProceduralTerrain *load_terrain_in(const glm::ivec2 &current_chunk_position);

        glm::ivec2 get_camera_chunk_position(const glm::vec3 &camera_position);

        std::vector<std::shared_ptr<Texture>> get_height_map_textures(uint32_t current_frame_num,
                                                                      const glm::ivec2 &current_chunk_position);

        aristaeus::gfx::vulkan::GraphicsPipeline::PipelineInfo get_height_patch_pipeline_pipeline_info(vk::raii::RenderPass &render_pass);

        static nlohmann::json water_info_ui_parse(ProceduralTerrain::WaterInfo &persistent_data, bool display_gui, const std::optional<std::string> &label_name);
        static nlohmann::json terrain_info_ui_parse(ProceduralTerrain::TerrainInfo &persistent_data, bool display_gui, const std::optional<std::string> &label_name);

        ProceduralTerrain::TerrainInfo m_default_terrain_info;
        ProceduralTerrain::WaterInfo m_default_water_info;
        glm::ivec2 m_height_map_resolution;
        std::mt19937::result_type m_seed;
        float m_draw_distance;
        float m_smoothing_percent;
        size_t m_max_loaded_terrains;

        // TODO(Bobby): fix template issues so ProceduralTerrain can be on the stack
        std::shared_ptr<LRUCacheNode<glm::vec2, ProceduralTerrain, utils::Vec2Hash>> m_loaded_terrains;
        std::array<tf::Taskflow, utils::vulkan::MAX_FRAMES_IN_FLIGHT> m_terrain_loading_taskflows;
        tf::Future<void> m_terrain_loading_taskflow_future;
        std::shared_ptr<TaskflowObserver> m_terrain_taskflow_observer;
        std::atomic<size_t> m_terrain_loading_counter;

        tf::Task m_needed_chunks_task;
        tf::Task m_optional_chunks_task;
        std::array<glm::ivec2, utils::vulkan::MAX_FRAMES_IN_FLIGHT> m_previous_chunk_position {{{100000, 100000},
                                                                                                {100000, 100000}}};

        glm::vec2 m_origin_random_start;
        glm::vec3 m_predraw_load_position;
        bool m_test_mode = false;

        std::mt19937 m_rng;

        std::unique_ptr<GraphicsPipeline> m_height_patch_pipeline;
        std::unordered_map<glm::vec2, AssimpMesh, utils::Vec2Hash> m_height_patch_model;
    };
}


#endif //ARISTAEUS_CHUNKED_TERRAIN_HPP
