//
// Created by bobby on 12/30/21.
//

#ifndef ARISTAEUS_TERRAIN_HPP
#define ARISTAEUS_TERRAIN_HPP

#include <string>
#include <vector>

#include <glm/gtx/transform.hpp>
#include <stb_image.h>

#include <gfx/vulkan/assimp_model.hpp>
#include <gfx/vulkan/sky_box.hpp>

#include "chunk_prop_loader.hpp"
#include "prop_info.hpp"
#include "wave_properties.hpp"
#include "terrain_image.hpp"

namespace aristaeus::gfx::vulkan {
    class Terrain : public SceneGraphNode {
    public:
        static constexpr float NOT_IN_TERRAIN = -1000000.0f;

        static constexpr std::string_view NODE_TYPE = "terrain";

        static constexpr vk::VertexInputBindingDescription TERRAIN_BINDING_DESCRIPTION
                {0, sizeof(AssimpMesh::TerrainVertex), vk::VertexInputRate::eVertex};
        static constexpr std::array<vk::VertexInputAttributeDescription, 1> TERRAIN_ATTRIBUTE_DESCRIPTIONS {{
            {0, 0, vk::Format::eR32G32Sfloat, offsetof(AssimpMesh::TerrainVertex, height_map_texture_coords)}
        }};

        enum class ObjectType {
            Object = 0,
            Grass = 1,
            Water = 2,
            Water_Gerstner = 3
        };

        struct WaveInfo {
            AssimpModel::Wave active_waves[AssimpModel::MAX_NUM_OF_WAVES];
            AssimpModel::Wave pending_waves[AssimpModel::MAX_NUM_OF_WAVES];
            int active_wave_count;
        };

        Terrain(float height_magnitude, float vertex_resolution, const std::vector<std::vector<float>> &height_map,
                const std::string &terrain_texture_path, int number_of_textures,
                ObjectType object_type, PropInfo &prop_info, const std::string &node_name,
                const std::string &specular_map_path = "", uint32_t number_of_patches = 60,
                std::optional<vk::CommandBuffer> create_command_buffer = {});

        Terrain(float height_magnitude, float vertex_resolution, const std::string &height_map_texture_path,
                const std::string &terrain_texture_path, int number_of_textures,
                ObjectType object_type, PropInfo &prop_info, const std::string &node_name,
                const std::string &specular_map_path = "", uint32_t number_of_patches = 60,
                std::optional<vk::CommandBuffer> create_command_buffer = {}) :
                Terrain(height_magnitude, vertex_resolution, extract_height_map_from_image(height_map_texture_path),
                        terrain_texture_path, number_of_textures, object_type, prop_info, node_name,
                        specular_map_path, number_of_patches, create_command_buffer) {}

        Terrain(float height_magnitude, float vertex_resolution, const std::vector<std::vector<float>> &height_map,
                const std::map<glm::vec4, std::vector<Image>, utils::Vec4Hash> &terrain_texture_paths_height,
                int number_of_textures, ObjectType object_type, PropInfo &prop_info, const std::string &node_name,
                const std::unordered_map<Image, glm::vec4> &terrain_texture_paths_slope = {},
                uint32_t number_of_patches = 60,
                std::optional<vk::CommandBuffer> create_command_buffer = {});

        Terrain(float height_magnitude, float vertex_resolution, const std::string &height_map_texture_path,
                const std::map<glm::vec4, std::vector<Image>, utils::Vec4Hash> &terrain_texture_paths_height,
                int number_of_textures, ObjectType object_type, PropInfo &prop_info, const std::string &node_name,
                const std::unordered_map<Image, glm::vec4> &terrain_texture_paths_slope = {},
                uint32_t number_of_patches = 60,
                std::optional<vk::CommandBuffer> create_command_buffer = {}) :
                Terrain(height_magnitude, vertex_resolution, extract_height_map_from_image(height_map_texture_path),
                        terrain_texture_paths_height, number_of_textures, object_type, prop_info, node_name,
                        terrain_texture_paths_slope, number_of_patches, create_command_buffer) {}

        ~Terrain() = default;

        [[nodiscard]] bool using_terrain_shader() const {
            return m_use_terrain_shader;
        }

        float get_height_value(const glm::ivec2 &height_map_coords);
        //float get_slope(const glm::vec2 &world_position);

        [[nodiscard]] float get_height_magnitude() const {
            return m_height_magnitude;
        }

        [[nodiscard]] float get_vertex_resolution() const {
            return m_vertex_resolution;
        }

        [[nodiscard]] std::vector<std::vector<float>> get_height_map() const {
            return m_height_map;
        }

        [[nodiscard]] AssimpModel::TessellationSettings get_tessellation_settings() const {
            return m_ubo_content.tessellation_settings;
        }

        [[nodiscard]] int get_num_of_patches() {
            return m_number_of_patches;
        }

        [[nodiscard]] std::shared_ptr<Texture> get_height_map_texture() {
            return m_height_map_texture;
        }

        [[nodiscard]] float get_height_map_slope(unsigned int z, unsigned int x) const;

        [[nodiscard]] WaveInfo get_wave_info() const {
            WaveInfo wave_info{
                .active_wave_count = m_ubo_content.active_wave_count
            };

            for(int i = 0; AssimpModel::MAX_NUM_OF_WAVES > i; ++i) {
                wave_info.active_waves[i] = m_ubo_content.active_waves[i];
                wave_info.pending_waves[i] = m_ubo_content.pending_waves[i];
            }

            return wave_info;
        }

        void set_wave_info(const WaveInfo &wave_info) {
            for(int i = 0; AssimpModel::MAX_NUM_OF_WAVES > i; ++i) {
                m_ubo_content.active_waves[i] = wave_info.active_waves[i];
                m_ubo_content.pending_waves[i] = wave_info.pending_waves[i];
            }
            m_ubo_content.active_wave_count = wave_info.active_wave_count;
        }

        void set_model_ubo(const AssimpModel::ModelUniformBuffer &model_uniform_buffer) {
            m_ubo_content = model_uniform_buffer;
            m_ubo_content_set = true;
        }

        void set_wave_world_position(const glm::vec3 &wave_world_position) {
            m_ubo_content.wave_world_position = wave_world_position;
        }

        // just for ease of debugging
        #ifndef NDEBUG
        void set_terrain_purpose(const std::string &terrain_purpose) {
            m_terrain_purpose = terrain_purpose;
        }
        #endif

        void set_reflectivity(const float reflectivity) {
            m_mesh.set_reflectivity(reflectivity);
        }

        void set_refractive_index(const float refractive_index) {
            m_mesh.set_refractive_index(refractive_index);
        }

        void set_refractive_strength(const float refractive_strength) {
            m_mesh.set_refractive_strength(refractive_strength);
        }

        void set_shininess(const float shininess) {
            m_mesh.set_shininess(shininess);
        }

        void set_transmission(const float transmission) {
            m_mesh.set_transmission(transmission);
        }

        void set_tessellation_settings(const AssimpModel::TessellationSettings &tessellation_settings) {
            m_ubo_content.tessellation_settings = tessellation_settings;
        }

        void set_chunked_settings(const std::optional<float> &chunked_smoothing_percent,
                                  const std::optional<glm::ivec2> &chunk_position);

        void set_num_of_patches(int num_of_patches, CoreRenderer &core_renderer) {
            m_number_of_patches = num_of_patches;

            m_mesh.change_terrain_vertices(core_renderer, generate_patch_vertices());
        }

        void set_test_color(std::optional<glm::vec3> test_color) {
            if(test_color.has_value()) {
                m_ubo_content.light_emitter = 1;
                m_ubo_content.light_box_color = test_color.value();
            } else {
                m_ubo_content.light_emitter = 0;
            }
        }

        void toggle_edge_debug_mode() {
            m_edge_debug_mode = !m_edge_debug_mode;

            m_ubo_content.multi_texture_terrain_mode = m_edge_debug_mode ? 2 : (m_multi_texture_mode ? 1 : 0);
        }

        #ifndef NDEBUG
        void toggle_normal_debug_mode() {
            m_normal_debug_mode = !m_normal_debug_mode;

            if(m_normal_debug_mode) {
                std::cout << "normal debug mode is on\n";
            } else {
                std::cout << "normal debug mode is off\n";
            }
        }
        #endif

        void set_using_terrain_pipeline(bool using_terrain_pipeline) {
            m_using_terrain_pipeline = using_terrain_pipeline;
        }

        void set_height_map_textures(const std::vector<std::shared_ptr<Texture>> &height_map_textures) {
            m_height_map_textures = height_map_textures;
        }

        void set_light_space_matrix(const glm::mat4 &light_space_matrix) {
            m_light_space_matrix = light_space_matrix;
        }

        virtual void predraw_load(const vk::CommandBuffer &command_buffer, const glm::vec3 &camera_position) override {};

        void draw(const vk::CommandBuffer &command_buffer, vk::raii::PipelineLayout &pipeline_layout, size_t instance_number = 1)override;

        void setup_terrain_infos(
                const std::map<glm::vec4, std::vector<Image>, utils::Vec4Hash> &terrain_texture_paths_height,
                const std::unordered_map<Image, glm::vec4> &terrain_texture_paths_slope);

        void update_waves(const WaveProperties &wave_properties);
    protected:
        Terrain(float height_magnitude, float vertex_resolution, const glm::ivec2 &height_map_size,
                const std::map<glm::vec4, std::vector<Image>, utils::Vec4Hash> &terrain_texture_paths_height,
                int number_of_textures, ObjectType object_type,
                PropInfo &prop_info, const std::string &node_name,
                const std::unordered_map<Image, glm::vec4> &terrain_texture_paths_slope = {},
                uint32_t number_of_patches = 60, std::optional<vk::CommandBuffer> create_command_buffer = {});

        void set_height_map_texture(const std::shared_ptr<Texture> &height_map_texture) {
            m_height_map_texture = height_map_texture;
        }
    private:
        static constexpr vk::DescriptorPoolSize POOL_SIZE {vk::DescriptorType::eUniformBuffer,
                                                           utils::vulkan::MAX_FRAMES_IN_FLIGHT};
        static constexpr AssimpModel::ModelUniformBuffer DEFAULT_MODEL_UBO {.override_texture = 0,
                                                                            .multi_texture_terrain_mode = 1};

        AssimpModel::Wave generate_new_wave(const WaveProperties &wave_properties,
                                            float birth_period);

        std::vector<std::vector<float>> extract_height_map_from_image(const std::string &height_map_texture_path);

        vk::raii::DescriptorSetLayout create_terrain_descriptor_set_layout(vk::raii::Device &logical_device);

        std::array<vk::raii::DescriptorSet, utils::vulkan::MAX_FRAMES_IN_FLIGHT>
            create_terrain_descriptor_sets(vk::raii::Device &logical_device);

        AssimpMesh setup_mesh(const std::map<glm::vec4, std::vector<Image>, utils::Vec4Hash> &terrain_images_height,
                              const std::unordered_map<Image, glm::vec4> &terrain_images_slope,
                              std::optional<vk::CommandBuffer> create_command_buffer);

        AssimpMesh setup_mesh(const std::string &diffuse_map_path,
                              std::optional<vk::CommandBuffer> create_command_buffer) {
            assert(!m_multi_texture_mode);

            Image image = {
                    .image_path = diffuse_map_path,
                    .image_type = "texture_diffuse"
            };
            std::map<glm::vec4, std::vector<Image>, utils::Vec4Hash> table;

            table.insert({glm::vec4(0.0f), {image}});

            return setup_mesh(table, {}, create_command_buffer);
        }

        vk::SamplerCreateInfo get_sampler_create_info() const {
            vk::SamplerCreateInfo sampler_create_info = AssimpModel::DEFAULT_SAMPLER_CREATE_INFO;

            sampler_create_info.addressModeU = vk::SamplerAddressMode::eClampToEdge;
            sampler_create_info.addressModeV = vk::SamplerAddressMode::eClampToEdge;

            sampler_create_info.maxAnisotropy = utils::get_singleton_safe<CoreRenderer>().get_max_sampler_anisotropy();

            return sampler_create_info;
        }

        vk::ImageViewCreateInfo get_image_view_create_info() const {
            vk::ImageViewCreateInfo image_view_create_info = AssimpModel::DEFAULT_IMAGE_VIEW_CREATE_INFO;

            image_view_create_info.format = vk::Format::eR32Sfloat;

            return image_view_create_info;
        }

        UniformBuffer create_model_ubos() {
            CoreRenderer &core_renderer = utils::get_singleton_safe<CoreRenderer>();
            size_t data_size = sizeof(DEFAULT_MODEL_UBO);

            UniformBuffer ubos{core_renderer, vk::ShaderStageFlagBits::eVertex | vk::ShaderStageFlagBits::eFragment |
                               vk::ShaderStageFlagBits::eTessellationEvaluation | vk::ShaderStageFlagBits::eTessellationControl,
                               data_size};

            ubos.write_to_all_descriptors(&DEFAULT_MODEL_UBO, data_size, core_renderer.get_current_render_context_id(),
                                          core_renderer);

            return ubos;
        }

        std::vector<AssimpMesh::TerrainVertex> generate_patch_vertices();

        std::optional<ChunkPropLoader> get_chunk_prop_loader(PropInfo &prop_info, CoreRenderer &core_renderer,
                                                             std::shared_ptr<std::mt19937> &rng);

        std::vector<GraphicsPipeline::ShaderInfo> get_terrain_pipeline_shader_infos();

        #ifndef NDEBUG
        std::vector<GraphicsPipeline::ShaderInfo> get_terrain_normal_debug_pipeline_shader_infos();
        #endif

        aristaeus::gfx::vulkan::GraphicsPipeline::PipelineInfo get_terrain_pipeline_pipeline_info(vk::raii::RenderPass &render_pass);

        #ifndef NDEBUG
        std::string m_terrain_purpose;
        #endif

        GraphicsPipeline m_terrain_pipeline;

        std::string m_specular_map;
        uint32_t m_number_of_patches;

        float m_height_magnitude;
        std::optional<glm::ivec2> m_height_map_size; // used when the child class is responsible
                                                     // for providing the height map
        std::vector<std::vector<float>> m_height_map;
        std::shared_ptr<Texture> m_height_map_texture;
        int m_number_of_textures;
        bool m_mesh_active = false;
        bool m_multi_texture_mode;
        bool m_use_grass_shader;
        bool m_use_terrain_shader;
        float m_vertex_resolution;

        std::vector<std::shared_ptr<Texture>> m_height_map_textures;
        bool m_using_terrain_pipeline = true;
        glm::mat4 m_light_space_matrix;

        bool m_ubo_content_set = false;
        AssimpModel::ModelUniformBuffer m_ubo_content;
        UniformBuffer m_ubos;

        AssimpMesh m_mesh;

        // if optional is empty, we haven't started getting the wave time point yet
        std::optional<std::chrono::system_clock::time_point> m_wave_last_time_point;

        double m_time_count = 0.0f;
        bool m_edge_debug_mode = false;
        #ifndef NDEBUG
        GraphicsPipeline m_terrain_normal_debug_pipeline;
        bool m_normal_debug_mode = false;
        #endif
    };
}

#endif //ARISTAEUS_TERRAIN_HPP
