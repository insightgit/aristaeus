//
// Created by bobby on 06/04/24.
//

#ifndef ARISTAEUS_COMPUTE_MODULE_ENTRY_POINT_HPP
#define ARISTAEUS_COMPUTE_MODULE_ENTRY_POINT_HPP

#include <core/class_db.hpp>

#include "compute_manager.hpp"
#include "ibl_sky_box.hpp"

extern "C" void compute_module_entry_point(const nlohmann::json &module_json);

extern "C" void compute_module_exit_point(const nlohmann::json &module_json);

#endif //ARISTAEUS_MODULE_ENTRY_POINT_HPP
