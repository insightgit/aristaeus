#version 450

layout (location = 0) in vec3 position;

layout (set = 0, binding = 0) uniform PerspectiveCameraUBO {
    mat4 projection;
    mat4 view;
} camera_ubo;

layout (location = 0) out vec3 texture_coords;

void main() {
    texture_coords = vec3(position.x, position.y, position.z);

    vec4 new_position = camera_ubo.projection * camera_ubo.view * vec4(position, 1.0);

    gl_Position = new_position.xyww;
}

