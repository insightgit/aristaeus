#ifndef ARISTAEUS_COMPUTE_TASK_HPP
#define ARISTAEUS_COMPUTE_TASK_HPP

#include <filesystem>
#include <future>
#include <unordered_set>

#include "compute_pipeline.hpp"

#include <gfx/vulkan/assimp_model.hpp>
#include <gfx/vulkan/image_buffer_raii.hpp>

#include <utils.hpp>

namespace aristaeus::gfx::vulkan::compute {
    class ComputeTask {
    public:
        enum class JobStatus {
            Pending,
            Loading,
            Submitted,
            SubmittedLoaded,
            ReadyForSaving,
            Saving,
            Finished
        };

        void save_hdr_image(const std::string &image_path, aristaeus::gfx::vulkan::ImageRaii &image,
                            const std::optional<vk::CommandBuffer> &command_buffer,
                            BufferRaii &image_staging_buffer, bool skip_image_staging_buffer_creation = false, 
                            std::optional<vk::DeviceSize> buffer_offset = {},
                            const std::optional<std::reference_wrapper<utils::SHA256Digest>> &sha256_digest = {});

        [[nodiscard]] std::string get_compute_task_type() const {
            return m_compute_task_type;
        }

        [[nodiscard]] JobStatus get_compute_job_status() const {
            return m_job_status;
        }

        [[nodiscard]] std::future<void> get_compute_job_future() {
            return m_job_finished_promise.get_future();
        }

        [[nodiscard]] std::vector<std::string> get_output_filenames() const;

        virtual vk::DeviceSize get_output_size() = 0;

        virtual void dispatch(vk::CommandBuffer &command_buffer) = 0;

        // file_image_data ivec3: (width, height, image channels)
        virtual bool load_from_saved_output(vk::CommandBuffer &command_buffer, BufferRaii &buffer,
                                            vk::DeviceSize buffer_offset, const std::vector<glm::ivec3> &file_image_data) = 0;
        virtual void save_output(vk::CommandBuffer &command_buffer, BufferRaii &buffer, vk::DeviceSize buffer_offset) = 0;
    protected:
        struct ComputePipelineInfo {
            VulkanPipeline::ShaderInfo compute_shader_info;
            std::string pipeline_cache_path;
            std::vector<vk::PushConstantRange> push_content_ranges;
            bool start_constructing_pipeline;
            bool is_in_preload_stage;
        };

        ComputeTask(size_t ubo_size, uint32_t number_of_storage_images,
                    uint32_t number_of_ubos, uint32_t number_of_samplers, ComputePipelineInfo compute_pipeline_info,
                    const std::string &compute_task_type, uint32_t number_of_array_layers,
                    uint32_t number_of_mipmaps, uint32_t number_of_dispatches_done_per_job,
                    const std::optional<std::string> &cache_path = {},
                    const std::vector<std::string> &input_file_paths = {});

        typedef std::pair<std::vector<vk::DescriptorSetLayoutBinding>, std::vector<vk::DescriptorPoolSize>> DescriptorInfo;

        Texture create_output_texture(const glm::ivec2 &image_size, const vk::Format &image_format,
                                      const std::string &image_description);

        DescriptorInfo m_descriptor_set_info;
        ComputePipeline m_compute_pipeline;
        vk::raii::DescriptorSetLayout m_descriptor_set_layout;
        vk::raii::DescriptorPool m_descriptor_pool;
        std::vector<vk::raii::DescriptorSet> m_descriptor_sets;
        std::vector<BufferRaii> m_ubos;
        std::optional<std::string> m_cache_path;
        std::vector<std::string> m_input_file_paths;
    private:
        struct CallbackData {
            std::string image_path;
            aristaeus::gfx::vulkan::ImageRaii& image_raii;
            aristaeus::gfx::vulkan::BufferRaii& image_staging_buffer;
            std::optional<vk::DeviceSize> buffer_offset;
            std::optional<std::reference_wrapper<utils::SHA256Digest>> sha256_digest;
            ComputeTask &compute_task;
        };

        static void postrender_saving_callback(void* data);

        JobStatus m_job_status;
        std::promise<void> m_job_finished_promise;

        uint32_t m_number_of_array_layers;
        uint32_t m_number_of_mipmaps;

        DescriptorInfo generate_descriptor_set_info(uint32_t number_of_storage_images, uint32_t number_of_ubos,
                                                    uint32_t number_of_samplers,
                                                    uint32_t number_of_dispatches_done_per_job);

        std::vector<vk::raii::DescriptorSet> create_descriptor_sets(vk::raii::Device &logical_device,
                                                                    uint32_t number_of_descriptor_sets);

        std::vector<BufferRaii> create_ubos(size_t ubo_size, uint32_t number_of_ubos);

        vk::raii::DescriptorPool create_descriptor_pool(vk::raii::Device& logical_device, uint32_t number_of_sets);

        std::string m_compute_task_type;

        friend class ComputeManager; // ComputeManager updates and manages JobStatus and related promise
    };
}

#endif
