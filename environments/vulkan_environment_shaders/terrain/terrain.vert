#version 450

layout (location = 0) in vec2 in_height_map_texture_coords;

layout (location = 0) out OutTerrainVertex {
    vec2 height_map_texture_coords;
    vec4 transformed_position;
} out_vertex;

layout (set = 0, binding = 0) uniform PerspectiveCameraUBO {
    mat4 projection;
    mat4 view;
} camera_ubo;

struct TessellationSettings {
    int mode;
    int amount;

    int camera_max;
    int camera_min;
};

struct Wave {
    int wave_active;
    float amplitude;
    float birth_period;
    vec2 center_wave;
    float death_period;
    float sharpness;
    float wavelength;
    float t_time;
    vec2 padding;
};

const int MAX_NUM_OF_WAVES = 100;

layout (set = 2, binding = 0) uniform ModelUBO {
    float terrain_height_magnitude;

    int override_texture;
    int multi_texture_terrain_mode;
    int light_emitter;
    int object_type;
    vec3 light_box_color;

    Wave active_waves[MAX_NUM_OF_WAVES];
    Wave pending_waves[MAX_NUM_OF_WAVES];
    vec3 wave_world_position;
    int active_wave_count;

    TessellationSettings tessellation_settings;
    float vertex_resolution;
    float terrain_num_of_textures;
} model_ubo;

layout (set = 3, binding = 0) uniform MeshUBO {
// only needed variables declared
    mat4 model;
    mat3 normal_transform;
} mesh_ubo;

void main() {
    vec2 current_coords = vec2(
        (-model_ubo.vertex_resolution / 2.0f) + (model_ubo.vertex_resolution * in_height_map_texture_coords.x),
        (-model_ubo.vertex_resolution / 2.0f) + (model_ubo.vertex_resolution * in_height_map_texture_coords.y));

    vec3 position = vec3(current_coords.x, 0, current_coords.y);

    out_vertex.height_map_texture_coords = in_height_map_texture_coords;
    out_vertex.transformed_position = camera_ubo.projection * camera_ubo.view * mesh_ubo.model * vec4(position, 1.0);

    gl_Position = vec4(position, 1.0);
}