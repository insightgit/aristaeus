#version 450
#extension GL_EXT_nonuniform_qualifier : enable

layout (location = 0) in vec3 position;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec2 texture_coords;
layout (location = 3) in vec3 tangent;


layout (set = 0, binding = 0) uniform PerspectiveCameraUBO {
    mat4 projection;
    mat4 view;
    mat4 light_space_matrix;
    vec3 camera_front;
    vec3 camera_up;
    vec3 camera_right;
    vec3 camera_position;
} camera_ubo;

struct Instance {
    mat4 model;
    mat3 normal_transform;
    uint material_index;
};

layout (std430, set = 3, binding = 0) readonly buffer InstanceUBO {
    Instance instances[];
} instance_ubo;

layout (location = 0) out VertexOut {
    vec3 normal_vector;
    mat3 tbn_matrix;
    vec2 texture_coords_out;
    vec3 world_position;

    float instance_index;
} vertex_out;

void main() {
    uint instance_index = gl_InstanceIndex;
    Instance instance = instance_ubo.instances[instance_index];

    vec3 bittangent_vector = cross(normal, tangent);
    vec3 tangent_vector_transformed = normalize(vec3(instance.model * vec4(tangent, 0.0)));
    vec3 bittangent_vector_transformed = normalize(vec3(instance.model * vec4(bittangent_vector, 0.0)));
    vec3 normal_vector_transformed = normalize(vec3(instance.model * vec4(normal, 0.0)));

    mat3 tbn_matrix = mat3(tangent_vector_transformed, bittangent_vector_transformed, normal_vector_transformed);

    vertex_out.world_position = (instance.model * vec4(position, 1.0)).xyz;
    vertex_out.normal_vector = normalize(normal);
    vertex_out.tbn_matrix = tbn_matrix;
    vertex_out.texture_coords_out = texture_coords;
    vertex_out.instance_index = instance_index;

    gl_Position = camera_ubo.projection * camera_ubo.view * vec4(vertex_out.world_position, 1.0);
}