#version 460

layout (quads, fractional_odd_spacing, ccw) in;

layout (location = 0) in vec2 height_map_texture_coords[];

const int OBJECT_TYPE_OBJECT = 0;
const int OBJECT_TYPE_GRASS = 1;
const int OBJECT_TYPE_WATER = 2;
const int OBJECT_TYPE_WATER_GERSTNER = 3;

const float GRAVITY_CONSTANT = 9.8;
const float PI = 3.141592653589793238;

struct TessellationSettings {
    int mode;
    int amount;

    int camera_max;
    int camera_min;
};

struct Wave {
    int wave_active;
    float amplitude;
    float birth_period;
    vec2 center_wave;
    float death_period;
    float sharpness;
    float wavelength;
    float t_time;
    vec2 padding;
};

layout (location = 0) out VertexOut {
    vec4 diffuse_color_vector;
    vec4 specular_color_vector;
    vec3 normal_vector;
    mat3 tbn_matrix;
    vec2 texture_coords_out;
    vec3 world_position;
    float vertex_height;

    vec2 height_map_texture_coords;
} tessellation_out;

layout (set = 0, binding = 0) uniform PerspectiveCameraUBO {
    mat4 projection;
    mat4 view;
} camera_ubo;

layout (set = 1, binding = 1) uniform PerlinNoiseUBO {
    float system_time;
    int perlin_lookup_table[256];
} perlin_noise_ubo;

const int MAX_NUM_OF_WAVES = 100;

layout (set = 2, binding = 0) uniform ModelUBO {
    float terrain_height_magnitude;

    int override_texture;
    int multi_texture_terrain_mode;
    int light_emitter;
    int object_type;

    vec3 light_box_color;

    Wave active_waves[MAX_NUM_OF_WAVES];
    Wave pending_waves[MAX_NUM_OF_WAVES];
    int active_wave_count;

    TessellationSettings tessellation_settings;
    float vertex_resolution;
    float terrain_num_of_textures;
    int is_chunked_terrain;
    float chunked_blend_threshold;
    ivec2 chunk_position;
} model_ubo;

layout (set = 3, binding = 0) uniform MeshUBO {
    // only needed variables declared
    mat4 model;
    mat3 normal_transform;
} mesh_ubo;
// origin is 4, others are the height maps surrounding the terrain
// TODO(Bobby): Move this to the model set
layout (set = 3, binding = 6) uniform sampler2D height_map[9];

float perlin_fade(float to_be_faded) {
    return to_be_faded * to_be_faded * to_be_faded * (to_be_faded * (to_be_faded * 6 - 15) + 10);
}

// source: https://theorangeduck.com/page/avoiding-shader-conditionals
float when_less_than(float x, float y) {
    return max(sign(y - x), 0.0);
}

float when_greater_equal_than(float x, float y) {
    return 1.0 - when_less_than(x, y);
}

float when_equal_to(float x, float y) {
    return 1.0 - abs(sign(x - y));
}

float when_not_equal_to(float x, float y) {
    return 1.0 - when_equal_to(x, y);
}

float or(float x, float y) {
    return min(x + y, 1.0);
}

float perlin_gradient_dot(int hash, vec2 direction_vector) {
    int h = hash & 15;                      // CONVERT LO 4 BITS OF HASH CODE
    float u = (direction_vector.x * when_less_than(h, 8)) + (direction_vector.y * when_greater_equal_than(h, 8));
    //float u = h < 8 ? direction_vector.x : direction_vector.y;                 // INTO 12 GRADIENT DIRECTIONS.
    //float v = h < 4 ? direction_vector.y : h == 12||h==14 ? direction_vector.x : 0.0;

    float v = (when_less_than(h, 4) * direction_vector.y) + (or(when_equal_to(h, 12), when_equal_to(h, 14)) * direction_vector.x);

    float return_value = (when_equal_to(h&1, 0) * u) + (when_not_equal_to(h&1, 0) * -u);

    return_value += (when_equal_to(h&2, 0) * v) + (when_not_equal_to(h&2, 0) * -v);

    return return_value;

    //return ((h&1) == 0 ? u : -u) + ((h&2) == 0 ? v : -v);
}

int perlin_hash(ivec2 location_coords) {
    // z is a constant here (0)
    while(location_coords.x < 0) {
        location_coords.x += 255;
    }

    while(location_coords.y < 0) {
        location_coords.y += 255;
    }

    location_coords.x = location_coords.x % 255;
    location_coords.y = location_coords.y % 255;

    int x_y_index = (perlin_noise_ubo.perlin_lookup_table[location_coords.x] + location_coords.y) % 255;

    return perlin_noise_ubo.perlin_lookup_table[perlin_noise_ubo.perlin_lookup_table[x_y_index] + 0];
}

float perlin(vec2 coords) {
    vec2 direction_pos = vec2(coords.x - floor(coords.x), coords.y - floor(coords.y));
    ivec2 location_pos = ivec2(int(coords.x) & 255, int(coords.y) & 255);
    vec2 fade_pos;

    fade_pos.x = perlin_fade(direction_pos.x);
    fade_pos.y = perlin_fade(direction_pos.y);

    float x1_x2_lerp = mix(perlin_gradient_dot(perlin_hash(location_pos), direction_pos),
    perlin_gradient_dot(perlin_hash(location_pos + ivec2(1, 0)),
    direction_pos - vec2(1, 0)), fade_pos.x);
    float x3_x4_lerp = mix(perlin_gradient_dot(perlin_hash(location_pos + ivec2(0, 1)), direction_pos - vec2(0, 1)),
    perlin_gradient_dot(perlin_hash(location_pos + ivec2(1, 1)), direction_pos - vec2(1, 1)),
    fade_pos.x);

    return max(mix(x1_x2_lerp, x3_x4_lerp, fade_pos.y), 0.0);
}

float perlin_octaves(vec2 coords, int octaves, float amplitude, float persistence, float frequency_increase) {
    float frequency = 1.0f;
    float max_value = 0.0f;
    float total_result = 0.0f;

    for(int i = 0; octaves > i; ++i) {
        max_value += amplitude;
        total_result += amplitude * perlin(coords * frequency);

        amplitude *= persistence;
        frequency *= frequency_increase;
    }

    return total_result / max_value;
}

vec2 interoplate_tex_height_map_coords() {
    vec2 texture_coord_first_u = mix(height_map_texture_coords[0], height_map_texture_coords[1], gl_TessCoord.x);
    vec2 texture_coord_bottom_u = mix(height_map_texture_coords[2], height_map_texture_coords[3], gl_TessCoord.x);

    return mix(texture_coord_first_u, texture_coord_bottom_u, gl_TessCoord.y);
}


vec2 interoplate_tex_coords(vec2 height_map_texture_coords) {
    vec2 tex_coords;

    vec2 texture_coord_base = vec2(model_ubo.vertex_resolution / model_ubo.terrain_num_of_textures,
                                   model_ubo.vertex_resolution / model_ubo.terrain_num_of_textures);

    vec2 current_coords = vec2((
        -model_ubo.vertex_resolution / 2.0f) + (model_ubo.vertex_resolution * height_map_texture_coords.x),
        (-model_ubo.vertex_resolution / 2.0f) + (model_ubo.vertex_resolution * height_map_texture_coords.y));
    tex_coords = current_coords / texture_coord_base;

    tex_coords.x = tex_coords.x - floor(tex_coords.x);
    tex_coords.y = tex_coords.y - floor(tex_coords.y);

    return tex_coords;
}

vec4 interoplate_position_coords() {
    vec4 position_first_u = mix(gl_in[0].gl_Position, gl_in[1].gl_Position, gl_TessCoord.x);
    vec4 position_bottom_u = mix(gl_in[2].gl_Position, gl_in[3].gl_Position, gl_TessCoord.x);

    vec4 position_v = mix(position_first_u, position_bottom_u, gl_TessCoord.y);

    position_v.w = 1.0;

    return position_v;
}

float get_smoothed_height_map_value(vec2 height_map_coords) {
    // TODO(Bobby): Do diagnols if smoothstep works

    // UNCOMMENT THIS IF IT IS NOT A CHUNKED TERRAIN!
    /*if(model_ubo.is_chunked_terrain == 0) {
        return height_map_value;
    }*/

    vec2 pixel_diff = vec2(1.0 / model_ubo.vertex_resolution);

    height_map_coords += when_equal_to(height_map_coords.x, 0) * vec2(pixel_diff.x, 0);
    height_map_coords += when_equal_to(height_map_coords.y, 0) * vec2(0, pixel_diff.y);

    // TODO(Bobby): this should really be pixel_diff.x instead of pixel_diff.y
    height_map_coords -= when_equal_to(height_map_coords.x, 1) * vec2(0, pixel_diff.y);
    height_map_coords -= when_equal_to(height_map_coords.y, 1) * vec2(0, pixel_diff.y);

    float height_map_value = texture(height_map[4], height_map_coords).r;

    // TODO(Bobby): when smoothing is better, get rid of these conditionals
    // top edge
    if(height_map_coords.y >= 1 - model_ubo.chunked_blend_threshold) {
        vec2 top_tex_coords = vec2(height_map_coords.x, 0);
        float scaling_value =
            mix(1 - model_ubo.chunked_blend_threshold, 1.0f, height_map_coords.y);

        if(model_ubo.multi_texture_terrain_mode == 2) {
            tessellation_out.diffuse_color_vector = mix(vec4(0, 1, 0, 1), vec4(0), 1 - scaling_value);
        }

        height_map_value = height_map_value * scaling_value +
                           texture(height_map[7], top_tex_coords).r * (1 - scaling_value);
    }

    // right edge
    if(height_map_coords.x <= model_ubo.chunked_blend_threshold) {
        vec2 right_tex_coords = vec2(1, height_map_coords.y);
        float scaling_value =
            mix(0, model_ubo.chunked_blend_threshold, height_map_coords.x);

        if(model_ubo.multi_texture_terrain_mode == 2) {
            tessellation_out.diffuse_color_vector = mix(vec4(0, 1, 1, 1), vec4(0), scaling_value);
        }

        height_map_value = height_map_value * (1 - scaling_value) +
                           texture(height_map[5], right_tex_coords).r * scaling_value;
    }

    return height_map_value;
}

vec3 get_height_map_normal(vec2 height_map_texture_coords) {
    // adapted from https://stackoverflow.com/a/5282364
    /*float height_map_unit = 1 / model_ubo.vertex_resolution;

    vec2 x0_coords = height_map_texture_coords - vec2(0.1 * height_map_unit, 0);
    vec2 x1_coords = height_map_texture_coords + vec2(0.1 * height_map_unit, 0);

    vec2 y0_coords = height_map_texture_coords - vec2(0, 0.1 * height_map_unit);
    vec2 y1_coords = height_map_texture_coords + vec2(0, 0.1 * height_map_unit);


    float map_x0 = get_smoothed_height_map_value(x0_coords);
    float map_x1 = get_smoothed_height_map_value(x1_coords);

    float map_y0 = get_smoothed_height_map_value(y0_coords);
    float map_y1 = get_smoothed_height_map_value(y1_coords);

    map_x0 = (map_x0 - 0.5) * 2;
    map_x0 = -map_x0;
    map_x1 = (map_x1 - 0.5) * 2;
    map_x1 = -map_x1;
    map_y0 = (map_y0 - 0.5) * 2;
    map_y0 = -map_y0;
    map_y1 = (map_y1 - 0.5) * 2;
    map_y1 = -map_y1;

    // the spacing of the grid in same units as the height map
    float eps = height_map_unit;

    // plug into the formulae above:
    vec3 normal = normalize(vec3((map_x0 - map_x1)/(2*eps), (map_y0 - map_y1)/(2*eps), 1));*/

    const float height_map_unit = 1 / model_ubo.vertex_resolution;
    float sobel_filter[9];
    vec3 normal = vec3(0.0f);

    for(int y = 0; 3 > y; ++y) {
        for(int x = 0; 3 > x; ++x) {
            vec2 offset = vec2(x, y) * height_map_unit;

            sobel_filter[(y * 3) + x] = get_smoothed_height_map_value(height_map_texture_coords + height_map_unit);
        }
    }

    normal.x = /*scale **/
        -(sobel_filter[8]-sobel_filter[6]+2*(sobel_filter[5]-sobel_filter[3])+sobel_filter[2]-sobel_filter[0]);
    normal.y = /*scale **/
        -(sobel_filter[0]-sobel_filter[6]+2*(sobel_filter[1]-sobel_filter[7])+sobel_filter[2]-sobel_filter[8]);
    normal.z = 1.0;
    normal = normalize(normal);

    return normal;
}

void main() {
    // need to differentiate noise texture coord from non noise texture coord!!!!!!

    // noise texture coord calculation
    // because gl_TessCoord is surface and not location specific, we are interpolating over 0 to 1
    // where 1 is the far edge and 0 is the near edge

    // normal calculation
    vec4 patch_u_vector = gl_in[1].gl_Position - gl_in[0].gl_Position;
    vec4 patch_v_vector = gl_in[2].gl_Position - gl_in[0].gl_Position;

    // position calculation
    tessellation_out.height_map_texture_coords = interoplate_tex_height_map_coords();
    tessellation_out.normal_vector = vec3(0, -1, 0)/*normalize(cross(patch_v_vector.xyz, patch_u_vector.xyz))*/;

    //tessellation_out.normal_vector.y = -tessellation_out.normal_vector.y;

    vec4 position = interoplate_position_coords();

    tessellation_out.diffuse_color_vector = vec4(0.0f);
    tessellation_out.specular_color_vector = vec4(0.0f);

    float height =
    get_smoothed_height_map_value(tessellation_out.height_map_texture_coords) * model_ubo.terrain_height_magnitude;

    height = (height - 0.5) * 2;

    //height = -height;

    position += vec4(height * tessellation_out.normal_vector, 0.0);

    vec2 next_tangent_height = tessellation_out.height_map_texture_coords + vec2(1 / model_ubo.vertex_resolution, 0);
    //vec2 next_bittangent_height = tessellation_out.height_map_texture_coords + vec2(0, 1 / model_ubo.vertex_resolution);

    //tessellation_out.normal_vector = get_height_map_normal(tessellation_out.height_map_texture_coords);

    vec3 tangent = normalize(vec3(get_smoothed_height_map_value(next_tangent_height) - height));
    vec3 bittangent = /*normalize(vec3(get_smoothed_height_map_value(next_bittangent_height) - height))*/
    normalize(cross(tessellation_out.normal_vector, tangent));

    tessellation_out.tbn_matrix = mat3(tangent, bittangent, tessellation_out.normal_vector);

    tessellation_out.world_position = (mesh_ubo.model * position).xyz;
    tessellation_out.vertex_height = position.y;
    tessellation_out.texture_coords_out = interoplate_tex_coords(tessellation_out.height_map_texture_coords);

    gl_Position = mesh_ubo.model * position;
}