//
// Created by bobby on 5/25/22.
//

#include "scene.hpp"

aristaeus::core::Scene::Scene(int argc, char **argv) :
        m_environment_loading_info(get_environment_loading_info(argc, argv)),
        m_window(construct_window(argc, argv)),
        m_construction_swap_chain_info(create_swap_chain_info()),
        m_construction_viewport(
                0.0f, 0.0f,
                static_cast<float>(m_construction_swap_chain_info.swap_chain_extent.width),
                static_cast<float>(m_construction_swap_chain_info.swap_chain_extent.height),
                0.0f, 1.0f),
        m_construction_viewport_scissor(
                {0, 0},
                m_construction_swap_chain_info.swap_chain_extent),
        m_mesh_manager(false),
        m_renderer(create_core_renderer(argc, argv)) {
    m_mesh_manager.setup_null_texture();
    std::cout << "loaded scene successfully!\n";
}

aristaeus::gfx::vulkan::Window aristaeus::core::Scene::construct_window(int argc, char **argv) {
    bool prefer_integrated_gpu = true;
    bool prefer_sync_compute = false;

    for(int i = 0; argc > i; ++i) {
        if (prefer_integrated_gpu && prefer_sync_compute) {
            break;
        } else if(strncmp(PREFER_INTEGRATED_GPU_STRING.data(), argv[i], PREFER_INTEGRATED_GPU_STRING.length()) == 0) {
            prefer_integrated_gpu = true;
        } else if (strncmp(PREFER_SYNC_COMPUTE_GPU_STRING.data(), argv[i], PREFER_SYNC_COMPUTE_GPU_STRING.length()) == 0) {
            prefer_sync_compute = true;
        }
    }

    m_scene_loader.load_environment_json(m_environment_loading_info.environment_path, m_environment_loading_info.environment_name);

    return gfx::vulkan::Window{ prefer_integrated_gpu, prefer_sync_compute, m_scene_loader.get_desired_vulkan_extensions() };
}

void aristaeus::core::Scene::render() {
    m_renderer.add_callback_for_event<void *, gfx::vulkan::MeshManager>(std::string{ gfx::vulkan::CoreRenderer::DRAW_FRAME_EVENT }, 
                                                                        "mesh_manager_draw_frame", [](gfx::vulkan::MeshManager &manager, void*) {
        manager.prepare_for_render();
    }, m_mesh_manager);

    while(!m_renderer.should_close_window()) {
        m_renderer.draw_frame();
    }
}

aristaeus::gfx::vulkan::CoreRenderer aristaeus::core::Scene::create_core_renderer(int argc, char **argv) {
    std::vector<glm::vec4> clear_colors{ {0.0859375, 0.7734375, 0.984375, 0} };

    return gfx::vulkan::CoreRenderer{ m_window, clear_colors, m_construction_swap_chain_info.logical_device,
            m_construction_swap_chain_info.swap_chain,m_construction_swap_chain_info.swap_chain_extent,
            m_construction_swap_chain_info.swap_chain_image_format, m_environment_loading_info, "compute_info.json"};
}

aristaeus::gfx::vulkan::CoreRenderer::EnvironmentLoadingInfo aristaeus::core::Scene::get_environment_loading_info(int argc, char** argv) {
    std::string environment_name{DEFAULT_STARTING_ENVIRONMENT};

    for (int i = 0; argc > i; ++i) {
        size_t string_length = strlen(argv[i]);

        if (strncmp(CUSTOM_ENVIRONMENT_OPTION_STRING.data(), argv[i], CUSTOM_ENVIRONMENT_OPTION_STRING.size()) == 0 && string_length > CUSTOM_ENVIRONMENT_OPTION_STRING.size()) {
            environment_name = &argv[i][CUSTOM_ENVIRONMENT_OPTION_STRING.size()];
            break;
        }
    }

    return { m_scene_loader, "../environments/", environment_name, static_cast<float>(m_construction_swap_chain_info.swap_chain_extent.width) /
            static_cast<float>(m_construction_swap_chain_info.swap_chain_extent.height), m_construction_viewport, m_construction_viewport_scissor };
}
